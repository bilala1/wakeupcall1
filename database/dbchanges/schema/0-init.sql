CREATE DATABASE  IF NOT EXISTS `wakeuc_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wakeuc_db`;
-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: wakeuc_db
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `faqs_categories`
--

DROP TABLE IF EXISTS `faqs_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs_categories` (
  `faqs_categories_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_faqs_categories_id` mediumint(8) unsigned NOT NULL,
  `faqs_categories_name` varchar(100) NOT NULL,
  `faqs_categories_status` enum('active','inactive') NOT NULL,
  `faqs_categories_visible` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`faqs_categories_id`),
  KEY `join_faqs_categories_id` (`join_faqs_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `actions_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_certificates_id` int(10) unsigned NOT NULL,
  `actions_name` varchar(255) NOT NULL,
  `actions_datetime` datetime NOT NULL,
  `actions_actio_msds` varchar(15) NOT NULL COMMENT 'Id of MSDS in the Actio system',
  PRIMARY KEY (`actions_id`)
) ENGINE=MyISAM AUTO_INCREMENT=353 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claims__OLD`
--

DROP TABLE IF EXISTS `claims__OLD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claims__OLD` (
  `claims_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_hotels_id` int(11) NOT NULL,
  `join_carriers_id` int(11) NOT NULL,
  `claims_name` varchar(255) NOT NULL,
  `claims_file` varchar(100) NOT NULL,
  `claims_filename` varchar(255) NOT NULL,
  `claims_type` enum('Workers Comp','Property/General Liability/3rd Party Property','Auto') NOT NULL,
  `claims_sendby` enum('fax') NOT NULL,
  `claims_datetime` datetime NOT NULL,
  PRIMARY KEY (`claims_id`),
  KEY `join_hotels_id` (`join_hotels_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_topics`
--

DROP TABLE IF EXISTS `forums_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_topics` (
  `topics_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_forums_id` mediumint(8) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `topics_title` varchar(255) NOT NULL,
  `topics_last_posts_id` mediumint(8) unsigned NOT NULL,
  `topics_last_posts_date` datetime NOT NULL,
  `topics_createdate` datetime NOT NULL,
  `topics_status` enum('active','locked') NOT NULL,
  `topics_views` mediumint(8) unsigned NOT NULL,
  `topics_stickied` tinyint(1) NOT NULL DEFAULT '0',
  `topics_approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`topics_id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corporations`
--

DROP TABLE IF EXISTS `corporations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporations` (
  `corporations_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_forums_id` int(11) NOT NULL,
  `corporations_name` varchar(255) NOT NULL,
  `corporations_address` varchar(255) NOT NULL,
  `corporations_city` varchar(255) NOT NULL,
  `corporations_state` varchar(255) NOT NULL,
  `corporations_zip` varchar(255) NOT NULL,
  `corporations_num_locations` int(11) NOT NULL,
  `corporations_admin_at_corporation` tinyint(1) NOT NULL DEFAULT '0',
  `corporations_separate_from_hotels` tinyint(1) NOT NULL DEFAULT '0',
  `corporations_active` tinyint(1) NOT NULL,
  `corporations_nextbill` datetime NOT NULL,
  `corporations_delete_datetime` datetime DEFAULT NULL COMMENT 'Date when corporation was marked as deleted',
  PRIMARY KEY (`corporations_id`),
  KEY `join_members_id` (`join_members_id`),
  KEY `join_forums_id` (`join_forums_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums`
--

DROP TABLE IF EXISTS `forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums` (
  `forums_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_forums_id_parent` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forums_name` varchar(255) NOT NULL,
  `forums_createdate` datetime NOT NULL,
  `forums_desc` text NOT NULL,
  `forums_topics` mediumint(8) unsigned NOT NULL,
  `forums_posts` mediumint(8) unsigned NOT NULL,
  `forums_status` enum('active','locked') NOT NULL DEFAULT 'active',
  `forums_public_permission_view` tinyint(1) NOT NULL DEFAULT '0',
  `forums_public_permission_reply` tinyint(1) NOT NULL DEFAULT '0',
  `forums_public_permission_new_topic` tinyint(1) NOT NULL DEFAULT '0',
  `forums_topic_approval` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if a new topic needs approval',
  `forums_post_approval` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if a new post needs approval',
  PRIMARY KEY (`forums_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `staff_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_hotels_id` int(10) unsigned NOT NULL,
  `staff_datetime` datetime NOT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `join_hotels_id` (`join_hotels_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Table of staff members for hotels';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `surveys_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `surveys_title` varchar(255) NOT NULL,
  `surveys_status` enum('active','inactive') NOT NULL,
  `surveys_datetime` datetime NOT NULL,
  PRIMARY KEY (`surveys_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `vendors_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `vendors_name` varchar(100) NOT NULL,
  `vendors_contact_name` varchar(255) NOT NULL,
  `vendors_email` varchar(255) NOT NULL,
  `vendors_phone` varchar(100) NOT NULL,
  `vendors_street` varchar(255) NOT NULL,
  `vendors_services` varchar(255) NOT NULL,
  PRIMARY KEY (`vendors_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `albums_photos`
--

DROP TABLE IF EXISTS `albums_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums_photos` (
  `albums_photos_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_albums_id` mediumint(8) unsigned NOT NULL,
  `join_albums_photos_id` mediumint(8) unsigned NOT NULL,
  `albums_photos_name` varchar(100) NOT NULL,
  `albums_photos_desc` longtext NOT NULL,
  `albums_photos_specs` longtext NOT NULL,
  `albums_photos_filename` varchar(127) NOT NULL,
  `albums_photos_datestamp` datetime NOT NULL,
  `join_global_members_id` mediumint(8) unsigned NOT NULL,
  `albums_photos_published` tinyint(1) NOT NULL DEFAULT '0',
  `albums_photos_sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`albums_photos_id`),
  KEY `join_albums_photos_id` (`join_albums_photos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_table_encryption_decryption_thinkhr`
--

DROP TABLE IF EXISTS `test_table_encryption_decryption_thinkhr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_table_encryption_decryption_thinkhr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encrypted_value` text NOT NULL COMMENT 'don''t forget to unserialize() this ',
  `decrypted_value` text NOT NULL,
  `response_value` text NOT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=232 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members_settings`
--

DROP TABLE IF EXISTS `members_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_settings` (
  `members_settings_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `members_settings_infobox_my_files` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_certificates` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_claims` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_corporate_forum` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_my_account` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_hr` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_empl_law` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_webinars` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_training` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_message_forum` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_doc_library` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_msds` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_news` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_addit_services` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_faq` tinyint(1) NOT NULL DEFAULT '1',
  `members_settings_infobox_tutorials` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`members_settings_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=latin1 COMMENT='Settings per member';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `articles_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `join_articles_categories_id` int(10) unsigned NOT NULL,
  `join_articles_tags` varchar(255) NOT NULL,
  `join_user_groups` varchar(255) DEFAULT NULL,
  `articles_title` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_category` varchar(255) NOT NULL,
  `articles_tags` varchar(255) NOT NULL,
  `articles_subtitle` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_teaser` text CHARACTER SET latin1,
  `articles_body` text CHARACTER SET latin1,
  `articles_rating` decimal(8,2) NOT NULL,
  `articles_views` int(10) unsigned NOT NULL DEFAULT '0',
  `articles_byline` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_newsdate` datetime DEFAULT NULL,
  `articles_entrydate` date DEFAULT NULL,
  `articles_footnote` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_copyright` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_order` int(11) DEFAULT NULL,
  `articles_page_url` varchar(60) NOT NULL,
  `articles_page_title` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_page_description` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_page_keywords` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `articles_active` tinyint(1) NOT NULL DEFAULT '0',
  `articles_allowed_comments` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`articles_id`),
  KEY `join_articles_category_id` (`join_articles_categories_id`),
  KEY `join_articles_tags` (`join_articles_tags`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_property`
--

DROP TABLE IF EXISTS `claim_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_property` (
  `claim_property_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_claims_id` int(10) unsigned NOT NULL,
  `claim_property_loss_type` varchar(255) NOT NULL,
  PRIMARY KEY (`claim_property_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_notifications`
--

DROP TABLE IF EXISTS `admin_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_notifications` (
  `admin_notifications_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_notifications_subject` varchar(255) NOT NULL,
  `admin_notifications_message` text NOT NULL,
  `admin_notifications_datetime` datetime NOT NULL,
  `admin_notifications_read` int(11) NOT NULL,
  PRIMARY KEY (`admin_notifications_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `albums_photos_bak`
--

DROP TABLE IF EXISTS `albums_photos_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums_photos_bak` (
  `albums_photos_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_albums_id` mediumint(8) unsigned NOT NULL,
  `join_albums_photos_id` mediumint(8) unsigned NOT NULL,
  `albums_photos_name` varchar(100) NOT NULL,
  `albums_photos_desc` longtext NOT NULL,
  `albums_photos_specs` longtext NOT NULL,
  `albums_photos_filename` varchar(127) NOT NULL,
  `albums_photos_datestamp` datetime NOT NULL,
  `join_global_members_id` mediumint(8) unsigned NOT NULL,
  `albums_photos_published` tinyint(1) NOT NULL DEFAULT '0',
  `albums_photos_sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`albums_photos_id`),
  KEY `join_albums_photos_id` (`join_albums_photos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=376 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `notifications_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `notifications_forum_update` tinyint(1) NOT NULL DEFAULT '1',
  `notifications_news_update` tinyint(1) NOT NULL DEFAULT '1',
  `notifications_blog_update` tinyint(1) NOT NULL DEFAULT '1',
  `notifications_additional_services` tinyint(1) NOT NULL DEFAULT '1',
  `notifications_webinars_update` tinyint(1) NOT NULL DEFAULT '1',
  `notifications_training_update` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`notifications_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chemicals`
--

DROP TABLE IF EXISTS `chemicals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chemicals` (
  `chemicals_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `chemicals_name` varchar(100) NOT NULL,
  `chemicals_manuf` varchar(80) NOT NULL,
  `chemicals_file` varchar(100) NOT NULL,
  PRIMARY KEY (`chemicals_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workcomps`
--

DROP TABLE IF EXISTS `workcomps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workcomps` (
  `workcomps_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `workcomps_name` varchar(100) NOT NULL,
  `workcomps_email` varchar(255) NOT NULL,
  PRIMARY KEY (`workcomps_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_topics_visits`
--

DROP TABLE IF EXISTS `forums_topics_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_topics_visits` (
  `forums_topics_visits_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_forums_topics_id` int(10) unsigned NOT NULL,
  `join_members_id` int(10) unsigned NOT NULL,
  `forums_topics_visits_datetime` datetime NOT NULL,
  PRIMARY KEY (`forums_topics_visits_id`),
  KEY `join_forums_topics_id` (`join_forums_topics_id`,`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1 COMMENT='Tracking member''s visits to a forum''s topics';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `albums_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `albums_parent` mediumint(9) NOT NULL DEFAULT '0',
  `albums_name` varchar(100) NOT NULL,
  `albums_icon` varchar(100) NOT NULL,
  PRIMARY KEY (`albums_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_comments`
--

DROP TABLE IF EXISTS `articles_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_comments` (
  `articles_comments_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_articles_id` int(10) unsigned NOT NULL,
  `join_members_id` int(10) unsigned NOT NULL,
  `articles_comments_author` varchar(160) NOT NULL,
  `articles_comments_email` varchar(255) NOT NULL,
  `articles_comments_title` varchar(60) NOT NULL,
  `articles_comments_body` text NOT NULL,
  `articles_comments_post_date` datetime NOT NULL,
  `articles_comments_rating` decimal(8,2) NOT NULL,
  PRIMARY KEY (`articles_comments_id`),
  KEY `article_id` (`join_articles_id`,`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `global_tags`
--

DROP TABLE IF EXISTS `global_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_tags` (
  `global_tags_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `global_tags_name` text NOT NULL,
  `global_tags_datetime` datetime NOT NULL,
  `global_tags_forums` mediumint(6) unsigned NOT NULL,
  `global_tags_articles` mediumint(6) unsigned NOT NULL,
  PRIMARY KEY (`global_tags_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_general_liab`
--

DROP TABLE IF EXISTS `claim_general_liab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_general_liab` (
  `claim_general_liab_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_claims_id` int(10) unsigned NOT NULL,
  `claim_general_liab_claimant` varchar(255) NOT NULL,
  `claim_general_liab_loss_area` varchar(255) NOT NULL,
  `claim_general_liab_loss_type` varchar(255) NOT NULL,
  PRIMARY KEY (`claim_general_liab_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_groups`
--

DROP TABLE IF EXISTS `forums_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_groups` (
  `groups_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `groups_name` varchar(100) NOT NULL,
  `groups_datetime` datetime NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL COMMENT 'founder',
  `groups_type` enum('open','restricted','closed') NOT NULL,
  `groups_desc` longtext NOT NULL,
  PRIMARY KEY (`groups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webinars`
--

DROP TABLE IF EXISTS `webinars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinars` (
  `webinars_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webinars_name` varchar(128) NOT NULL,
  `webinars_description` text NOT NULL,
  `webinars_connection_info` text NOT NULL,
  `webinars_link` varchar(250) NOT NULL,
  `webinars_datetime` datetime NOT NULL,
  `webinars_file1` varchar(255) NOT NULL,
  `webinars_file1_name` varchar(255) NOT NULL,
  `webinars_file2` varchar(255) NOT NULL,
  `webinars_file_name` varchar(255) NOT NULL,
  `webinars_file_recording` varchar(255) NOT NULL,
  `webinars_keywords` text NOT NULL,
  PRIMARY KEY (`webinars_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_watchdogs`
--

DROP TABLE IF EXISTS `articles_watchdogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_watchdogs` (
  `watchdogs_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_articles_id` mediumint(8) unsigned NOT NULL,
  `watchdogs_datestamp` datetime NOT NULL,
  PRIMARY KEY (`watchdogs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `settings_name` varchar(255) NOT NULL,
  `settings_value` text NOT NULL,
  PRIMARY KEY (`settings_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` tinyint(3) unsigned NOT NULL,
  `countries_code` varchar(3) NOT NULL,
  `countries_name` varchar(60) NOT NULL,
  `countries_order` smallint(3) NOT NULL,
  PRIMARY KEY (`countries_id`),
  KEY `continent_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_auto`
--

DROP TABLE IF EXISTS `claim_auto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_auto` (
  `claim_auto_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_claims_id` int(10) unsigned NOT NULL,
  `claim_auto_claimant` varchar(255) NOT NULL,
  `claim_auto_nature` varchar(255) NOT NULL,
  PRIMARY KEY (`claim_auto_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `admins_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `admins_username` varchar(100) NOT NULL,
  `admins_password` varchar(255) NOT NULL,
  `admins_email` varchar(255) NOT NULL,
  `admins_active` enum('active','inactive') NOT NULL,
  `admins_firstname` varchar(255) NOT NULL,
  `admins_lastname` varchar(255) NOT NULL,
  PRIMARY KEY (`admins_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_x_global_tags`
--

DROP TABLE IF EXISTS `articles_x_global_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_x_global_tags` (
  `join_articles_id` int(11) NOT NULL,
  `join_global_tags_id` int(11) NOT NULL,
  PRIMARY KEY (`join_articles_id`,`join_global_tags_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members_logins`
--

DROP TABLE IF EXISTS `members_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_logins` (
  `members_logins_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `members_logins_datetime` datetime NOT NULL,
  PRIMARY KEY (`members_logins_id`)
) ENGINE=MyISAM AUTO_INCREMENT=918 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `payments_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `payments_amount` decimal(10,2) NOT NULL,
  `payments_datetime` datetime NOT NULL,
  `payments_transaction_id` varchar(255) NOT NULL,
  PRIMARY KEY (`payments_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bills` (
  `bills_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `bills_amount` decimal(10,2) NOT NULL,
  `bills_type` enum('yearly','location') NOT NULL,
  `bills_description` varchar(255) NOT NULL,
  `bills_datetime` datetime NOT NULL,
  `bills_paid` tinyint(1) NOT NULL DEFAULT '0',
  `join_discount_codes_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`bills_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_posts_files`
--

DROP TABLE IF EXISTS `forums_posts_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_posts_files` (
  `forums_posts_files_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_posts_id` mediumint(8) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `forums_posts_files_datetime` datetime NOT NULL,
  `forums_posts_files_original_filename` varchar(100) NOT NULL,
  `forums_posts_files_filename` varchar(100) NOT NULL,
  `forums_posts_files_size` varchar(30) NOT NULL,
  `forums_posts_files_ext` varchar(25) NOT NULL,
  PRIMARY KEY (`forums_posts_files_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chemicals_x_members`
--

DROP TABLE IF EXISTS `chemicals_x_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chemicals_x_members` (
  `chemicals_x_members_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_chemicals_id` mediumint(8) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`chemicals_x_members_id`),
  KEY `join_chemicals_id` (`join_chemicals_id`,`join_members_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `newsletters_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsletters_text` text NOT NULL,
  `newsletters_date` datetime NOT NULL,
  PRIMARY KEY (`newsletters_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_categories`
--

DROP TABLE IF EXISTS `articles_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_categories` (
  `articles_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `articles_categories_name` varchar(60) NOT NULL,
  `articles_categories_description` text NOT NULL,
  `articles_categories_order` int(11) NOT NULL,
  `articles_categories_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`articles_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_posts_reported`
--

DROP TABLE IF EXISTS `forums_posts_reported`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_posts_reported` (
  `posts_reported_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_posts_id` mediumint(8) NOT NULL,
  `posts_reported_datetime` datetime NOT NULL,
  `join_report_reasons_id` smallint(4) unsigned NOT NULL,
  `posts_reported_desc` longtext NOT NULL,
  `posts_reported_status` enum('open','pending','closed') NOT NULL DEFAULT 'open',
  PRIMARY KEY (`posts_reported_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_posts`
--

DROP TABLE IF EXISTS `forums_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_posts` (
  `posts_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_topics_id` mediumint(8) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `posts_postdate` datetime NOT NULL,
  `posts_tags` text,
  `posts_body` longtext NOT NULL,
  `posts_enabled` text NOT NULL,
  `posts_modified_join_members_id` mediumint(8) NOT NULL,
  `posts_modified_reason` text NOT NULL,
  `posts_modified_date` datetime NOT NULL,
  `posts_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `posts_approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`posts_id`),
  KEY `join_topics_id` (`join_topics_id`),
  KEY `join_global_members_id` (`join_members_id`),
  KEY `posts_modified_join_global_members_id` (`posts_modified_join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `survey_options`
--

DROP TABLE IF EXISTS `survey_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_options` (
  `survey_options_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `survey_options_name` varchar(255) NOT NULL,
  `survey_options_color` varchar(255) NOT NULL,
  `join_surveys_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`survey_options_id`),
  KEY `join_surveys_id` (`join_surveys_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webinars_x_members`
--

DROP TABLE IF EXISTS `webinars_x_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webinars_x_members` (
  `webinars_x_members_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_webinars_id` int(11) NOT NULL,
  `join_members_id` int(11) NOT NULL,
  `webinars_x_members_datetime` datetime NOT NULL,
  PRIMARY KEY (`webinars_x_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claims_files`
--

DROP TABLE IF EXISTS `claims_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claims_files` (
  `claims_files_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_claims_id` int(11) NOT NULL,
  `claims_files_file` char(18) NOT NULL,
  `claims_files_filename` varchar(250) NOT NULL,
  PRIMARY KEY (`claims_files_id`),
  KEY `join_claims_id` (`join_claims_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `documents_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_library_categories_id` mediumint(8) unsigned NOT NULL,
  `join_chemicals_id` int(10) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `documents_title` varchar(255) NOT NULL,
  `documents_datetime` datetime NOT NULL,
  `documents_file` varchar(255) NOT NULL,
  `documents_filename` varchar(255) NOT NULL,
  `documents_type` set('submitted','personal','library') NOT NULL,
  `documents_status` enum('active','inactive') NOT NULL,
  `documents_keywords` text NOT NULL,
  `documents_additional` text NOT NULL,
  `documents_requestable` tinyint(1) NOT NULL,
  `documents_freetrial` tinyint(1) NOT NULL COMMENT 'Free trial access',
  PRIMARY KEY (`documents_id`),
  KEY `join_library_categories_id` (`join_library_categories_id`,`join_members_id`),
  FULLTEXT KEY `documents_title` (`documents_title`,`documents_keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=644 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount_codes`
--

DROP TABLE IF EXISTS `discount_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_codes` (
  `discount_codes_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discount_codes_code` varchar(25) NOT NULL,
  `discount_codes_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `discount_codes_start` date NOT NULL,
  `discount_codes_end` date NOT NULL,
  `discount_codes_amount` decimal(12,2) NOT NULL,
  `discount_codes_percent` decimal(5,2) NOT NULL,
  PRIMARY KEY (`discount_codes_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bad_words`
--

DROP TABLE IF EXISTS `bad_words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bad_words` (
  `bad_words_id` int(11) NOT NULL AUTO_INCREMENT,
  `bad_words_word` varchar(32) NOT NULL,
  PRIMARY KEY (`bad_words_id`)
) ENGINE=MyISAM AUTO_INCREMENT=463 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `carriers`
--

DROP TABLE IF EXISTS `carriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriers` (
  `carriers_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `carriers_name` varchar(100) NOT NULL,
  `carriers_contact` varchar(150) NOT NULL,
  `carriers_email` varchar(255) NOT NULL,
  `carriers_phone` varchar(50) NOT NULL,
  `carriers_fax` varchar(50) NOT NULL,
  `carriers_policy_number` varchar(255) NOT NULL,
  `carriers_address` varchar(255) NOT NULL,
  `carriers_coverages_list` varchar(100) DEFAULT NULL COMMENT 'list of coverages ',
  `carriers_coverages_other` varchar(255) NOT NULL,
  PRIMARY KEY (`carriers_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `survey_responses`
--

DROP TABLE IF EXISTS `survey_responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_responses` (
  `survey_responses_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_survey_options_id` mediumint(8) unsigned NOT NULL,
  `join_surveys_id` mediumint(8) unsigned NOT NULL,
  `survey_responses_datetime` datetime NOT NULL,
  PRIMARY KEY (`survey_responses_id`),
  KEY `join_members_id` (`join_members_id`,`join_survey_options_id`),
  KEY `join_surveys_id` (`join_surveys_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `faqs_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_faqs_categories_id` mediumint(8) unsigned NOT NULL,
  `faqs_question` text NOT NULL,
  `faqs_answer` text NOT NULL,
  `faqs_question_datetime` datetime NOT NULL,
  `faqs_answer_datetime` datetime NOT NULL,
  `faqs_published` tinyint(1) NOT NULL,
  `faqs_keywords` text NOT NULL,
  `faqs_order` int(11) NOT NULL,
  PRIMARY KEY (`faqs_id`),
  KEY `join_members_id` (`join_members_id`,`join_faqs_categories_id`),
  FULLTEXT KEY `faqs_question` (`faqs_question`,`faqs_answer`,`faqs_keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `certificates_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_vendors_id` mediumint(8) unsigned DEFAULT NULL,
  `certificates_name` varchar(100) NOT NULL,
  `certificates_project_name` varchar(255) NOT NULL,
  `certificates_type` enum('regular','special') NOT NULL,
  `certificates_file` varchar(100) NOT NULL,
  `certificates_filename` varchar(255) NOT NULL,
  `certificates_expire` date NOT NULL,
  `certificates_email_days` int(4) unsigned NOT NULL,
  `certificates_coverages` text NOT NULL,
  `certificates_send_copy` tinyint(1) NOT NULL DEFAULT '0',
  `certificates_remind_member` tinyint(1) NOT NULL DEFAULT '1',
  `certificates_remind_member_date_last` date NOT NULL COMMENT 'A date when last reminder was sent',
  `certificates_remind_vendor` tinyint(1) NOT NULL DEFAULT '1',
  `certificates_remind_text` text NOT NULL,
  PRIMARY KEY (`certificates_id`),
  KEY `join_members_id` (`join_members_id`,`join_vendors_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotels` (
  `hotels_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_corporations_id` int(11) NOT NULL,
  `hotels_name` varchar(255) NOT NULL,
  `hotels_address` varchar(255) NOT NULL,
  `hotels_city` varchar(255) NOT NULL,
  `hotels_state` varchar(255) NOT NULL,
  `hotels_zip` varchar(255) NOT NULL,
  `hotels_industry_type` varchar(255) NOT NULL,
  `hotels_num_rooms` int(11) NOT NULL,
  `hotels_franchise` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_active` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_delete_datetime` datetime DEFAULT NULL COMMENT 'When action to delete was made',
  PRIMARY KEY (`hotels_id`),
  KEY `join_corporations_id` (`join_corporations_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `events_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `events_startdate` datetime NOT NULL,
  `events_enddate` datetime NOT NULL,
  `events_allday` tinyint(1) DEFAULT NULL,
  `events_homepage_order` int(11) NOT NULL DEFAULT '0',
  `events_name` text NOT NULL,
  `events_city` varchar(100) NOT NULL,
  `events_state` varchar(50) NOT NULL,
  `events_zip` varchar(10) NOT NULL,
  `events_lon` float(11,6) NOT NULL,
  `events_lat` float(11,6) NOT NULL,
  `events_desc` longtext NOT NULL,
  `events_desc_short` text NOT NULL,
  `events_image_filename` varchar(255) NOT NULL,
  PRIMARY KEY (`events_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terms` (
  `terms_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `terms_acronym` varchar(100) NOT NULL,
  `terms_name` varchar(255) NOT NULL,
  `terms_datetime` datetime NOT NULL,
  PRIMARY KEY (`terms_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members_x_documents`
--

DROP TABLE IF EXISTS `members_x_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_x_documents` (
  `members_x_documents_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_documents_id` mediumint(8) unsigned NOT NULL,
  `join_members_library_categories_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`members_x_documents_id`),
  KEY `join_members_id` (`join_members_id`,`join_documents_id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads` (
  `downloads_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `downloads_type` enum('file','msds','certificate','forum file') NOT NULL COMMENT 'file = my files and document library',
  `join_id` int(10) unsigned NOT NULL,
  `join_members_id` int(10) unsigned NOT NULL,
  `downloads_datetime` datetime NOT NULL,
  PRIMARY KEY (`downloads_id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `members_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `members_firstname` varchar(100) NOT NULL,
  `members_lastname` varchar(100) NOT NULL,
  `members_title` varchar(40) NOT NULL COMMENT 'position / title',
  `members_phone` varchar(20) NOT NULL,
  `members_fax` varchar(20) NOT NULL,
  `members_email` varchar(255) NOT NULL,
  `members_password` varchar(255) NOT NULL,
  `members_last_login` datetime DEFAULT NULL,
  `members_lastsession` varchar(64) NOT NULL COMMENT 'id of a session',
  `members_datetime` datetime NOT NULL COMMENT 'Date and time when a person registered ',
  `members_membership_datetime` datetime NOT NULL COMMENT 'When a person became a member',
  `members_status` enum('free','member','exempt','locked') NOT NULL,
  `members_forum_signature` text NOT NULL,
  `members_type` enum('hotel','corporation','staff') NOT NULL DEFAULT 'hotel',
  `members_verified_code` char(32) NOT NULL,
  `members_billing_firstname` varchar(255) NOT NULL,
  `members_billing_lastname` varchar(255) NOT NULL,
  `members_billing_addr1` varchar(150) NOT NULL,
  `members_billing_addr2` varchar(150) NOT NULL,
  `members_billing_city` varchar(100) NOT NULL,
  `members_billing_state` varchar(100) NOT NULL,
  `members_billing_zip` varchar(25) NOT NULL,
  `members_billing_amt` decimal(6,2) NOT NULL COMMENT 'billing amount',
  `members_card_name` varchar(255) NOT NULL,
  `members_card_num` varchar(100) NOT NULL,
  `members_card_type` varchar(100) NOT NULL,
  `members_card_expire` date NOT NULL,
  `members_hr_agreement_yn` tinyint(1) NOT NULL COMMENT 'Whether a person agreed to HR agreement',
  `members_hr_agreement_datetime` datetime DEFAULT NULL COMMENT 'date and time when person agreed to HR agreement',
  `members_elaw_agreement_yn` int(11) NOT NULL DEFAULT '0',
  `members_elaw_agreement_datetime` datetime DEFAULT NULL,
  `members_locked_datetime` datetime NOT NULL,
  `members_show_startup_video` int(11) NOT NULL DEFAULT '1',
  `members_last_visit` datetime NOT NULL,
  PRIMARY KEY (`members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claims`
--

DROP TABLE IF EXISTS `claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claims` (
  `claims_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_hotels_id2` int(10) unsigned NOT NULL,
  `join_carriers_id` int(10) unsigned NOT NULL,
  `claims_type` enum('Workers Comp','General Liability / Guest Property','Auto','Your Property') NOT NULL,
  `claims_datetime` datetime NOT NULL,
  `claims_datetime_submitted` datetime NOT NULL,
  `claims_filed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`claims_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members_library_categories`
--

DROP TABLE IF EXISTS `members_library_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_library_categories` (
  `members_library_categories_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `members_library_categories_name` varchar(255) NOT NULL,
  PRIMARY KEY (`members_library_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_watchdogs`
--

DROP TABLE IF EXISTS `forums_watchdogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_watchdogs` (
  `watchdogs_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  `join_topics_id` mediumint(8) unsigned NOT NULL,
  `watchdogs_datestamp` datetime NOT NULL,
  PRIMARY KEY (`watchdogs_id`),
  KEY `join_members_id` (`join_members_id`),
  KEY `join_topics_id` (`join_topics_id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_workers_comp`
--

DROP TABLE IF EXISTS `claim_workers_comp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_workers_comp` (
  `claim_workers_comp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_claims_id` int(10) unsigned NOT NULL,
  `claim_workers_comp_employee_name` varchar(255) NOT NULL,
  `claim_workers_comp_department` varchar(255) NOT NULL,
  `claim_workers_comp_injury_type` varchar(255) NOT NULL,
  `claim_workers_comp_body_parts` text NOT NULL,
  PRIMARY KEY (`claim_workers_comp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_reasons`
--

DROP TABLE IF EXISTS `report_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_reasons` (
  `report_reasons_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `report_reasons_name` varchar(150) NOT NULL,
  PRIMARY KEY (`report_reasons_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_groups_x_forums`
--

DROP TABLE IF EXISTS `forums_groups_x_forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_groups_x_forums` (
  `forums_groups_x_forums_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `join_groups_id` mediumint(9) NOT NULL,
  `join_forums_id` mediumint(9) NOT NULL,
  `forums_groups_x_forums_permission_view` tinyint(1) NOT NULL DEFAULT '0',
  `forums_groups_x_forums_permission_reply` tinyint(1) NOT NULL DEFAULT '0',
  `forums_groups_x_forums_permission_new_topic` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`forums_groups_x_forums_id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `irmi_terms`
--

DROP TABLE IF EXISTS `irmi_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `irmi_terms` (
  `irmi_terms_id` int(11) NOT NULL AUTO_INCREMENT,
  `irmi_terms_name` varchar(255) NOT NULL,
  `irmi_terms_url` varchar(255) NOT NULL,
  PRIMARY KEY (`irmi_terms_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3207 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_tags`
--

DROP TABLE IF EXISTS `articles_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_tags` (
  `articles_tags_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `articles_tags_total_articles` int(11) NOT NULL,
  `articles_tags_name` varchar(255) NOT NULL,
  `articles_tags_description` text NOT NULL,
  PRIMARY KEY (`articles_tags_id`),
  KEY `articles_tags_name` (`articles_tags_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workcomps_x_members`
--

DROP TABLE IF EXISTS `workcomps_x_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workcomps_x_members` (
  `workcomps_x_members_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_workcomps_id` mediumint(8) unsigned NOT NULL,
  `join_members_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`workcomps_x_members_id`),
  KEY `join_workcomps_id` (`join_workcomps_id`,`join_members_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `library_categories`
--

DROP TABLE IF EXISTS `library_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_categories` (
  `library_categories_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_library_categories_id` mediumint(8) unsigned NOT NULL,
  `library_categories_name` varchar(255) NOT NULL,
  `library_categories_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`library_categories_id`),
  KEY `join_library_categories_id` (`join_library_categories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forums_groups_x_members`
--

DROP TABLE IF EXISTS `forums_groups_x_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_groups_x_members` (
  `forums_groups_x_members_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `join_groups_id` mediumint(9) NOT NULL,
  `join_members_id` int(11) NOT NULL,
  PRIMARY KEY (`forums_groups_x_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `definitions`
--

DROP TABLE IF EXISTS `definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `definitions` (
  `definitions_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `definitions_word` varchar(100) NOT NULL,
  `definitions_description` text NOT NULL,
  `definitions_pending` tinyint(1) NOT NULL DEFAULT '0',
  `join_members_id` int(11) DEFAULT NULL COMMENT 'an id of a members who requested to add this defnition ot the db',
  `definitions_requested_datetime` datetime DEFAULT NULL COMMENT 'date and time when this term was requested',
  PRIMARY KEY (`definitions_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3899 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `testimonial_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `details` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `zorder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-18 13:30:39
