--
-- Schema Sync 0.9.1 Patch Script
-- Created: Thu, Dec 06, 2012
-- Server Version: 5.5.28-0ubuntu0.12.04.2
-- Apply To: 127.0.0.1/wakeupcall_9_17
--

USE `wakeuc_db`;
ALTER DATABASE `wakeuc_db` CHARACTER SET=utf8 COLLATE=utf8_general_ci;
CREATE TABLE `discount_code_logs` ( `discount_code_logs_id` int(10) unsigned NOT NULL AUTO_INCREMENT, `discount_codes_logs_code` varchar(25) NOT NULL, `join_discount_codes_id` int(10) DEFAULT NULL, `discount_code_logs_percent` decimal(5,2) DEFAULT '0.00', `discount_code_logs_amount` decimal(12,2) DEFAULT '0.00', `discount_code_logs_dateused` date NOT NULL, `discount_code_logs_final_balance` decimal(12,2) NOT NULL, `join_members_id` int(10) DEFAULT NULL, `discount_code_logs_members_firstname` varchar(100) DEFAULT NULL, `discount_code_logs_members_lastname` varchar(100) NOT NULL, PRIMARY KEY (`discount_code_logs_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `franchises` ( `franchises_id` int(10) unsigned NOT NULL AUTO_INCREMENT, `join_members_id` int(10) unsigned NOT NULL, `join_albums_photos_id` int(10) NOT NULL, `franchises_name` varchar(45) NOT NULL, `franchises_code` varchar(45) NOT NULL, `franchises_active` tinyint(1) NOT NULL DEFAULT '0', `franchises_delete_datetime` datetime DEFAULT NULL, `franchises_pct_discount` int(3) NOT NULL DEFAULT '0', `franchises_amount_discount` int(3) NOT NULL DEFAULT '0', PRIMARY KEY (`franchises_id`), UNIQUE KEY `franchises_code_UNIQUE` (`franchises_code`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `albums` ADD COLUMN `albums_root_folder` varchar(45) NOT NULL DEFAULT '/data/albums' AFTER `albums_icon`, ADD UNIQUE INDEX `albums_root_folder_UNIQUE` (`albums_root_folder`) USING BTREE;
ALTER TABLE `discount_codes` ADD COLUMN `discount_codes_max_uses` int(10) NOT NULL DEFAULT '0' AFTER `discount_codes_percent`, ADD COLUMN `discount_codes_uses` int(10) NOT NULL AFTER `discount_codes_max_uses`;
ALTER TABLE `hotels` ADD COLUMN `join_franchises_id` int(11) NULL AFTER `hotels_delete_datetime`;
ALTER TABLE `members` MODIFY COLUMN `members_fax` varchar(20) NULL AFTER `members_phone`;
