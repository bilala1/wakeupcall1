--
-- Schema Sync 0.9.1 Revert Script
-- Created: Thu, Dec 06, 2012
-- Server Version: 5.5.28-0ubuntu0.12.04.2
-- Apply To: 127.0.0.1/wakeupcall_9_17
--

USE `wakeupcall_9_17`;
ALTER DATABASE `wakeupcall_9_17` CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;
DROP TABLE `discount_code_logs`;
DROP TABLE `franchises`;
ALTER TABLE `albums` DROP COLUMN `albums_root_folder`, DROP INDEX `albums_root_folder_UNIQUE`;
ALTER TABLE `discount_codes` DROP COLUMN `discount_codes_max_uses`, DROP COLUMN `discount_codes_uses`;
ALTER TABLE `hotels` DROP COLUMN `join_franchises_id`;
ALTER TABLE `members` MODIFY COLUMN `members_fax` varchar(20) NOT NULL AFTER `members_phone`;
