alter table `claim_workers_comp` CHANGE column `claim_workers_comp_department` `claim_workers_comp_employee_department` varchar(255) not null;

alter table `claim_workers_comp` add column `claim_workers_comp_event_department` varchar(255) not null;
alter table `claim_workers_comp` add column `claim_workers_comp_job_title` varchar(255) not null;
alter table `claim_workers_comp` add column `claim_workers_comp_event_type` enum('Injury', 'Illness') not null default 'Injury';
alter table `claim_workers_comp` add column `claim_workers_comp_injury_type_other` varchar(255) null;
alter table `claim_workers_comp` add column `claim_workers_comp_illness_type` varchar(255) null;
alter table `claim_workers_comp` add column `claim_workers_comp_illness_type_other` varchar(255) null;
alter table `claim_workers_comp` add column `claim_workers_comp_medical_costs` decimal(8, 2) null;
alter table `claim_workers_comp` add column `claim_workers_comp_classification` enum('G', 'H', 'I', 'J') not null default 'J';
alter table `claim_workers_comp` add column `claim_workers_comp_datetime_away_start` datetime null;
alter table `claim_workers_comp` add column `claim_workers_comp_datetime_away_end` datetime null;

alter table `claims` add column `claims_claim_number` varchar(255) null;
alter table `claims` add column `claims_description` varchar(255) not null;
alter table `claims` add column `claims_datetime_notified` datetime null;