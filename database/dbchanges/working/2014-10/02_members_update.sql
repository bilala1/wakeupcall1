alter table members
add members_stripe_id varchar(50);

alter table members
add members_billing_type varchar(10);

update members
set members_billing_type = 'yearly';