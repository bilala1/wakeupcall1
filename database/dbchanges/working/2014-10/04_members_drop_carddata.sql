alter table members
drop members_card_num;

alter table members
drop members_billing_firstname;

alter table members
drop members_billing_lastname;

alter table members
drop members_billing_addr1;

alter table members
drop members_billing_addr2;

alter table members
drop members_billing_city;

alter table members
drop members_billing_state;

alter table members
drop members_billing_zip;

alter table members
drop members_card_name;

alter table members
drop members_card_amount;

alter table members
drop members_card_type;

alter table members
drop members_card_expire;