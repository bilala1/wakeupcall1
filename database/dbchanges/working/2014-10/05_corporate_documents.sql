ALTER TABLE members_x_documents ADD COLUMN `documents_is_corporate_shared` tinyint(1) NOT NULL DEFAULT '0';

UPDATE members_x_documents, documents
   SET members_x_documents.documents_is_corporate_shared = documents.documents_is_corporate_shared
 WHERE documents.documents_id = members_x_documents.join_documents_id;

ALTER TABLE documents DROP COLUMN `documents_is_corporate_shared`;
