delimiter $$

CREATE TABLE `contracts` (
  `contracts_id` int(11) NOT NULL AUTO_INCREMENT,
  `contracts_contracted_party_name` varchar(200) NOT NULL,
  `contracts_contact_name` varchar(200) NOT NULL,
  `contracts_contact_phone` varchar(20) NOT NULL,
  `contracts_contact_email` varchar(200) NOT NULL,
  `contracts_effective_date` datetime NOT NULL,
  `contracts_expiration_date` datetime NULL,
  `contracts_description` varchar(200) NULL,
  `contracts_additional_details` varchar(2000) NOT NULL,
  `contracts_status` enum('Active', 'Month to Month', 'Expired', 'Inactive')  NOT NULL DEFAULT 'Active',
  `contracts_transition_status_to` enum('Month to Month', 'Expired', 'Inactive')  NULL,
  `contracts_expiration_notify_lead_days` int(8) NOT NULL,
  `contracts_expired_notification` int(1) NOT NULL DEFAULT 1,
  `contracts_hidden` int(1) NOT NULL DEFAULT 0,
  `contracts_location_can_view` int(1) NOT NULL DEFAULT 1,
  `contracts_location_can_edit` int(1) NOT NULL DEFAULT 1,
  `contracts_all_locations` int(1) NOT NULL DEFAULT 0,
  `join_accounts_id` int(10) unsigned NOT NULL,
  `contracts_remind_member_date_last` datetime NULL,
  PRIMARY KEY (`contracts_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$


CREATE TABLE `contracts_x_licensed_locations` (
  `contracts_x_licensed_locations_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_contracts_id` int(11) NOT NULL,
  `join_licensed_locations_id` int(11) NOT NULL,
  PRIMARY KEY (`contracts_x_licensed_locations_id`),
  KEY `join_contracts_id` (`join_contracts_id`),
  KEY `join_licensed_locations_id` (`join_licensed_locations_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$


CREATE TABLE `contracts_files` (
  `contracts_files_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_contracts_id` int(11) NOT NULL,
  `contracts_files_file` char(18) NOT NULL,
  `contracts_files_filename` varchar(250) NOT NULL,
  PRIMARY KEY (`contracts_files_id`),
  KEY `join_contracts_id` (`join_contracts_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$


CREATE TABLE `contracts_history` (
  `contracts_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contracts_history_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_contracts_id` int(10) unsigned NOT NULL,
  `contracts_history_description` varchar(2000) NOT NULL,
  PRIMARY KEY (`contracts_history_id`),
  KEY `join_contracts_id` (`join_contracts_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$


CREATE TABLE `contracts_additional_recipients` (
  `contracts_additional_recipients_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_contracts_id` int(10) unsigned NOT NULL,
  `contracts_additional_recipients_email` varchar(200) NOT NULL,
  PRIMARY KEY (`contracts_additional_recipients_id`),
  KEY `join_contracts_id` (`join_contracts_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$


ALTER TABLE `members_settings` ADD COLUMN `members_settings_infobox_contracts` int(1) NOT NULL DEFAULT 1$$

