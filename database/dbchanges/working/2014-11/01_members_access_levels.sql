delimiter $$

CREATE TABLE `members_access_levels` (
  `members_access_level_id` mediumint(8) unsigned NOT NULL,
  `members_access_level_name` varchar(100) NOT NULL,
  `members_access_level_path` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1$$

INSERT INTO `members_access_levels` (`members_access_level_id`, `members_access_level_name`, `members_access_level_path`) VALUES
(1, 'Certificate Training', '/members/my-documents/certificates/index.php'),
(2, 'Claims Reporting', '/members/claims/index.php'),
(3, 'Contracts Management', '/members/contracts/index.php'),
(4, 'Employment Law', '/members/employmentlaw/terms.php'),
(5, 'HR', '/members/hr/index.php'),
(6, 'Message Forum', '/members/forums/view-forum.php')$$

ALTER TABLE `members_access_levels` ADD PRIMARY KEY (`members_access_level_id`)$$

ALTER TABLE `members_access_levels` MODIFY `members_access_level_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7$$
