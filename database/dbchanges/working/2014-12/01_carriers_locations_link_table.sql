CREATE TABLE `licensed_locations_x_carriers` (
  `join_licensed_locations_id` int,
  `join_carriers_id` int,
  PRIMARY KEY (`join_licensed_locations_id`, `join_carriers_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1466 DEFAULT CHARSET=latin1;


DELIMITER //

CREATE PROCEDURE process_carriers (INOUT output varchar(8000))
BEGIN
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_carrier_id INTEGER DEFAULT 0;
	DECLARE v_member_id INTEGER DEFAULT 0;
	DECLARE v_locations_id INTEGER DEFAULT 0;
	DECLARE v_corporation_id INTEGER DEFAULT 0;

	DECLARE v_new_members_id INTEGER DEFAULT 0;

	DECLARE carriers_cursor CURSOR
	FOR
	SELECT carriers_id, join_members_id FROM carriers;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET v_finished = 1;

	OPEN carriers_cursor;
 
	get_carrier: LOOP
	 
		FETCH carriers_cursor INTO v_carrier_id, v_member_id;

		IF v_finished = 1 THEN
			LEAVE get_carrier;
		END IF;
		
		set v_corporation_id = (SELECT corporations_id from corporations where join_members_id = v_member_id);
		set v_locations_id = (select licensed_locations_id from licensed_locations where join_members_id = v_member_id);
		
		IF v_locations_id THEN
			INSERT INTO licensed_locations_x_carriers(join_licensed_locations_id, join_carriers_id) VALUES(v_locations_id, v_carrier_id);
		END IF;
		
		IF v_corporation_id <> 0 THEN
			ITERATE get_carrier;
		END IF;

		
		set v_corporation_id = (select join_corporations_id from licensed_locations where join_members_id = v_member_id);
		set v_new_members_id = (select join_members_id from corporations where corporations_id = v_corporation_id);

		IF v_new_members_id THEN
			UPDATE carriers SET join_members_id = v_new_members_id WHERE carriers_id = v_carrier_id;
		END IF;

	END LOOP get_carrier;
	CLOSE carriers_cursor;
END//

SET @output = ""//
CALL process_carriers(@output)//
SELECT @output//

DROP PROCEDURE process_carriers//



