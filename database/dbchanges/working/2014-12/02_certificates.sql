ALTER TABLE `certificates` ADD COLUMN `certificates_coverage_other` varchar(50) null;

ALTER TABLE `vendors` ADD COLUMN `vendors_hidden` tinyint(1) NOT NULL DEFAULT '0';
