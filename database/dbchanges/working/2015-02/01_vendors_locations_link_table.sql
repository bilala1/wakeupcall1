CREATE TABLE `vendors_x_licensed_locations` (
  `join_vendors_id` int,
  `join_licensed_locations_id` int,
  PRIMARY KEY (`join_licensed_locations_id`, `join_vendors_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1466 DEFAULT CHARSET=latin1;

DELIMITER //

CREATE PROCEDURE process_vendors (INOUT output varchar(8000))
BEGIN
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_vendor_id INTEGER DEFAULT 0;
	DECLARE v_member_id INTEGER DEFAULT 0;
	DECLARE v_locations_id INTEGER DEFAULT 0;
	DECLARE v_corporation_id INTEGER DEFAULT 0;

	DECLARE v_new_members_id INTEGER DEFAULT 0;

	DECLARE vendors_cursor CURSOR
	FOR
	SELECT vendors_id, join_members_id FROM vendors;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET v_finished = 1;

	OPEN vendors_cursor;
 
	get_vendor: LOOP
	 
		FETCH vendors_cursor INTO v_vendor_id, v_member_id;

		IF v_finished = 1 THEN
			LEAVE get_vendor;
		END IF;
		
		set v_corporation_id = (SELECT corporations_id from corporations where join_members_id = v_member_id);
		set v_locations_id = (select licensed_locations_id from licensed_locations where join_members_id = v_member_id);
		
		IF v_locations_id THEN
			INSERT INTO vendors_x_licensed_locations(join_licensed_locations_id, join_vendors_id) VALUES(v_locations_id, v_vendor_id);
		END IF;
		
		IF v_corporation_id <> 0 THEN
			ITERATE get_vendor;
		END IF;

		
		set v_corporation_id = (select join_corporations_id from licensed_locations where join_members_id = v_member_id);
		set v_new_members_id = (select join_members_id from corporations where corporations_id = v_corporation_id);

		IF v_new_members_id THEN
			UPDATE vendors SET join_members_id = v_new_members_id WHERE vendors_id = v_vendor_id;
		END IF;

	END LOOP get_vendor;
	CLOSE vendors_cursor;
END//

SET @output = ""//
CALL process_vendors(@output)//
SELECT @output//

DROP PROCEDURE process_vendors//

ALTER TABLE vendors ADD COLUMN `vendors_all_locations` boolean default 0//
ALTER TABLE vendors ADD COLUMN `vendors_location_can_view` boolean default 1//
ALTER TABLE vendors ADD COLUMN `vendors_location_can_edit` boolean default 1//

ALTER TABLE vendors CHANGE `join_members_id` `join_accounts_id` mediumint(8) unsigned NOT NULL//


