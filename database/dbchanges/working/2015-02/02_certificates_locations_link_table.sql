DELIMITER //

ALTER TABLE certificates ADD COLUMN `certificates_hidden` boolean default 0//

ALTER TABLE certificates ADD COLUMN `certificates_location_can_view` boolean default 1//
ALTER TABLE certificates ADD COLUMN `certificates_location_can_edit` boolean default 1//

ALTER TABLE certificates ADD COLUMN `join_licensed_locations_id` int(11) not null default 0//

CREATE PROCEDURE process_certificates (INOUT output varchar(8000))
BEGIN
	DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_certificate_id INTEGER DEFAULT 0;
	DECLARE v_member_id INTEGER DEFAULT 0;
	DECLARE v_locations_id INTEGER DEFAULT 0;
	DECLARE v_corporation_id INTEGER DEFAULT 0;

	DECLARE v_new_members_id INTEGER DEFAULT 0;

	DECLARE certificates_cursor CURSOR
	FOR
	SELECT certificates_id, join_members_id FROM certificates;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET v_finished = 1;

	OPEN certificates_cursor;
 
	get_certificate: LOOP
	 
		FETCH certificates_cursor INTO v_certificate_id, v_member_id;

		IF v_finished = 1 THEN
			LEAVE get_certificate;
		END IF;
		
		set v_corporation_id = (SELECT corporations_id from corporations where join_members_id = v_member_id);
		set v_locations_id = (select licensed_locations_id from licensed_locations where join_members_id = v_member_id);
		
		IF v_locations_id THEN
			UPDATE certificates set join_licensed_locations_id = v_locations_id where certificates_id = v_certificate_id;
		END IF;
		
		IF v_corporation_id <> 0 THEN
			ITERATE get_certificate;
		END IF;

		
		set v_corporation_id = (select join_corporations_id from licensed_locations where join_members_id = v_member_id);
		set v_new_members_id = (select join_members_id from corporations where corporations_id = v_corporation_id);

		IF v_new_members_id THEN
			UPDATE certificates SET join_members_id = v_new_members_id WHERE certificates_id = v_certificate_id;
		END IF;

	END LOOP get_certificate;
	CLOSE certificates_cursor;
END//

SET @output = ""//
CALL process_certificates(@output)//
SELECT @output//

DROP PROCEDURE process_certificates//

ALTER TABLE certificates CHANGE `join_members_id` `join_accounts_id` mediumint(8) unsigned NOT NULL//

ALTER TABLE certificates ADD COLUMN `certificates_remind_corporate_admin` boolean default 0//

ALTER TABLE certificates ADD COLUMN `certificates_remind_other` varchar(50) NULL//
