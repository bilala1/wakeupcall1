ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_osha_include bool default 1;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_osha_privacy bool default 0;