ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_datetime_restricted_start datetime;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_datetime_restricted_end datetime;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_classification_G bit default 0;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_classification_H bit default 0;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_classification_I bit default 0;

ALTER TABLE claim_workers_comp
ADD COLUMN claim_workers_comp_classification_J bit default 0;