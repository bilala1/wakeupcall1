DELIMITER //

CREATE PROCEDURE process_workers_comp (INOUT output varchar(8000))
BEGIN
        DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE v_claim_workers_comp_id INTEGER DEFAULT 0;
        DECLARE v_claim_workers_comp_datetime_away_start datetime;
        DECLARE v_claim_workers_comp_datetime_away_end datetime;
	DECLARE v_claim_workers_comp_classification enum('G','H','I','J')  DEFAULT 'J';

	DECLARE workers_comp_cursor CURSOR
	FOR
	SELECT claim_workers_comp_id, claim_workers_comp_classification, claim_workers_comp_datetime_away_start, claim_workers_comp_datetime_away_end FROM claim_workers_comp;

	DECLARE CONTINUE HANDLER
	FOR NOT FOUND SET v_finished = 1;

	OPEN workers_comp_cursor;
 
	get_workers_comp: LOOP
	 
		FETCH workers_comp_cursor INTO v_claim_workers_comp_id, v_claim_workers_comp_classification, v_claim_workers_comp_datetime_away_start, v_claim_workers_comp_datetime_away_end;

		IF v_finished = 1 THEN
			LEAVE get_workers_comp;
		END IF;
		
                UPDATE claim_workers_comp SET claim_workers_comp_datetime_restricted_start = v_claim_workers_comp_datetime_away_start WHERE claim_workers_comp_id = v_claim_workers_comp_id; 
                UPDATE claim_workers_comp SET claim_workers_comp_datetime_restricted_end = v_claim_workers_comp_datetime_away_end WHERE claim_workers_comp_id = v_claim_workers_comp_id; 

                IF v_claim_workers_comp_classification = 'G' THEN
                    UPDATE claim_workers_comp SET claim_workers_comp_classification_G = 1 WHERE claim_workers_comp_id = v_claim_workers_comp_id;
                END IF;

                		
                IF v_claim_workers_comp_classification = 'H' THEN
                    UPDATE claim_workers_comp SET claim_workers_comp_classification_H = 1 WHERE claim_workers_comp_id = v_claim_workers_comp_id; 
                END IF;

                		
                IF v_claim_workers_comp_classification = 'I' THEN
                    UPDATE claim_workers_comp SET claim_workers_comp_classification_I = 1 WHERE claim_workers_comp_id = v_claim_workers_comp_id; 
                END IF;

                		
                IF v_claim_workers_comp_classification = 'J' THEN
                    UPDATE claim_workers_comp SET claim_workers_comp_classification_J = 1 WHERE claim_workers_comp_id = v_claim_workers_comp_id; 
                END IF;

	END LOOP get_workers_comp;
	CLOSE workers_comp_cursor;
END//

SET @output = ""//
CALL process_workers_comp(@output)//
SELECT @output//

DROP PROCEDURE process_workers_comp//




