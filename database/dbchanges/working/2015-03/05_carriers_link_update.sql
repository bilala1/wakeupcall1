ALTER TABLE licensed_locations_x_email_recipients 
ADD COLUMN always_email bit DEFAULT 0;

UPDATE licensed_locations_x_email_recipients 
SET always_email = 1 WHERE join_licensed_locations_id > -1;