CREATE TABLE `claims_faxes` (
  `claims_faxes_id` int NOT NULL AUTO_INCREMENT,
  `join_claims_id` int NOT NULL,
  `claims_faxes_submission_id` varchar(50) NOT NULL,
  `claims_faxes_submitted_datetime` datetime NOT NULL,
  `claims_faxes_status` int NOT NULL,
  `claims_faxes_submitted_by` int NOT NULL,
  PRIMARY KEY (`claims_faxes_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `claims_faxes_files` (
  `claims_faxes_files_id` int NOT NULL AUTO_INCREMENT,
  `join_claims_faxes_id` int NOT NULL,
  `join_claims_files_id` int NOT NULL,
  PRIMARY KEY (`claims_faxes_files_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `claims` CHANGE COLUMN `claims_status` `claims_status` enum('Submitted - Open','Submission Failed','Submitted - Closed','Record Only - Not submitted','First Aid Only - Not submitted') NOT NULL DEFAULT 'Record Only - Not submitted';
