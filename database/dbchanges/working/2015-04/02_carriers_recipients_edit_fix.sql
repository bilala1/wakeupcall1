ALTER TABLE licensed_locations_x_email_recipients 
ADD COLUMN location_can_edit bit DEFAULT 0;

ALTER TABLE licensed_locations_x_carriers 
ADD COLUMN location_can_edit bit DEFAULT 0;

ALTER TABLE carriers
ADD COLUMN carriers_all_recieve bit DEFAULT 0;