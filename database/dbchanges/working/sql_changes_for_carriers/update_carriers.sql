alter table carriers add column carriers_hidden boolean not null default 0;
alter table carriers add column carriers_recipient boolean not null default 0;
alter table carriers add column carriers_effective_start_date datetime;
alter table carriers add column carriers_effective_end_date datetime;

update carriers set carriers_effective_end_date = '1972-01-01';
update carriers set carriers_effective_start_date = '1972-01-01';