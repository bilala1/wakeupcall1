CREATE TABLE `claims_notes` (
	`claims_notes_id` INT NOT NULL AUTO_INCREMENT,
	`join_claims_id` INT NOT NULL,
	`claims_notes_subject` varchar(150),
	`claims_notes_note` TEXT,
	`claims_notes_date` TIMESTAMP,
	PRIMARY KEY (`claims_notes_id`),
	KEY `join_claims_id` (`join_claims_id`)
);

INSERT INTO claims_notes(join_claims_id, claims_notes_subject, claims_notes_note) values(0, "asdf", "asdf");