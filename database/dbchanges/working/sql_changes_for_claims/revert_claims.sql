ALTER TABLE `claims` DROP COLUMN `claims_datetime_originally_submitted`;
ALTER TABLE `claims` DROP COLUMN `claims_datetime_closed`;
ALTER TABLE `claims` DROP COLUMN `claims_amount_paid`;
ALTER TABLE `claims` DROP COLUMN `claims_amount_reserved`;
ALTER TABLE `claims` DROP COLUMN `claims_notes`;
ALTER TABLE `claims` DROP COLUMN `claims_status`;
ALTER TABLE `claims` DROP COLUMN `claims_hidden`;

