ALTER TABLE `claims` ADD COLUMN `claims_datetime_closed` DATETIME NULL;
ALTER TABLE `claims` ADD COLUMN `claims_amount_paid` DECIMAL(8, 2) NOT NULL DEFAULT 0;
ALTER TABLE `claims` ADD COLUMN `claims_amount_reserved` DECIMAL(8, 2) NOT NULL DEFAULT 0;
ALTER TABLE `claims` ADD COLUMN `claims_status` enum('Submitted - Open', 'Submitted - Closed', 'Record Only - Not submitted', 'First Aid Only - Not submitted') NOT NULL DEFAULT 'Record Only - Not submitted';
ALTER TABLE `claims` ADD COLUMN `claims_hidden` BOOLEAN NOT NULL DEFAULT 0;
ALTER TABLE `claims` MODIFY COLUMN `claims_datetime_submitted` DATETIME NULL;

update `claims` set claims_status = 'Submitted - Open' where claims_filed = 1;

/* Added 7/21 */
update `claims` set claims_datetime_submitted = null where claims_filed = 0;

ALTER TABLE `claims` MODIFY COLUMN `claims_status` enum( 'Submitted - Open', 'Submitted - Closed', 'Record Only - Not submitted', 'First Aid Only - Not submitted') NOT NULL DEFAULT 'Record Only - Not submitted';
update claims set claims_status = 'Record Only - Not submitted' where claims_status = '';