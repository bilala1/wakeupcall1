
-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Alter Discount Codes

ALTER TABLE `discount_codes` ADD COLUMN `discount_codes_type` VARCHAR(255) NOT NULL DEFAULT 'billing';

delimiter $$

CREATE TABLE `discount_code_types` (
  `discount_code_types_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_code_types_type` varchar(255) NOT NULL,
  PRIMARY KEY (`discount_code_types_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1$$

INSERT INTO `discount_code_types` (discount_code_types_type) VALUES ('billing');
INSERT INTO `discount_code_types` (discount_code_types_type) VALUES ('trial');