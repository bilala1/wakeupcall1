DELIMITER $$
DROP PROCEDURE IF exists `sp_update_corp_locations`$$

CREATE PROCEDURE sp_update_corp_locations()
BEGIN
	DECLARE done int;
	DECLARE a CHAR(16);
	DECLARE v_last_id INT(11) default 0;
	DECLARE v_join_members_id int(11);
	DECLARE v_corporations_id int(11);
	DECLARE v_corporations_name VARCHAR(255);
	DECLARE v_corporations_address VARCHAR(255);
	DECLARE v_corporations_city VARCHAR(255);
	DECLARE v_corporations_state VARCHAR(255);
	DECLARE v_corporations_zip VARCHAR(255);
	DECLARE v_corporations_active tinyint(1);
	DECLARE cur1 CURSOR FOR 
	select join_members_id, corporations_id,  corporations_name, corporations_address, corporations_city, corporations_state, corporations_zip, corporations_active 
    from corporations;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur1;

	read_loop: LOOP
    FETCH cur1 INTO v_join_members_id, v_corporations_id,  v_corporations_name, v_corporations_address, v_corporations_city, v_corporations_state, v_corporations_zip, v_corporations_active;
    
		IF done then
		LEAVE read_loop;
		END IF;

		 insert into licensed_locations (join_members_id, join_corporations_id, licensed_locations_name, licensed_locations_address, licensed_locations_city, licensed_locations_state, licensed_locations_zip, licensed_locations_active)
         VALUES (v_join_members_id, v_corporations_id, v_corporations_name, v_corporations_address, v_corporations_city, v_corporations_state, v_corporations_zip, v_corporations_active);
         -- VALUES (v_join_members_id, -1, 'test', 'test', 'test', 'test', 'test', 0);

		-- insert into licensed_locations 
		-- (join_members_id, join_corporations_id, licensed_locations_name, licensed_locations_address, licensed_locations_city, licensed_locations_state, licensed_locations_zip, /*old_hotels_industry_type, old_hotels_num_rooms, old_hotels_franchise,*/ licensed_locations_active /*, join_franchises_id*/)
        -- VALUES (v_join_members_id, v_join_corporations_id, v_corporations_name, v_corporations_address, v_corporations_city, v_corporations_state, v_corporations_zip, /* , "NA", 0, 0,*/ v_corporations_active /*,0 */);
        --  VALUES (v_join_members_id, v_join_corporations_id, v_corporations_name, v_corporations_address, v_corporations_city, v_corporations_state, v_corporations_zip, "NA", 0, 0, v_corporations_active,0);        


		set v_last_id = last_insert_id();
		insert into corporate_locations (corporate_locations_num_rooms, corporate_locations_active, join_licensed_locations_id) 
		values (0, v_corporations_active, v_last_id);


	END LOOP;

  CLOSE cur1;
END$$
DELIMITER ;