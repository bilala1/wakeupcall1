
delimiter $$

CREATE TABLE `licensed_locations` (
  `licensed_locations_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_corporations_id` int(11) NOT NULL,
  `licensed_locations_name` varchar(255) NOT NULL,
  `licensed_locations_address` varchar(255) NOT NULL,
  `licensed_locations_city` varchar(255) NOT NULL,
  `licensed_locations_state` varchar(255) NOT NULL,
  `licensed_locations_zip` varchar(255) NOT NULL,
  `old_hotels_industry_type` varchar(255) NOT NULL,
  `old_hotels_num_rooms` int(11) NOT NULL,
  `old_hotels_franchise` tinyint(1) NOT NULL DEFAULT '0',
  `licensed_locations_active` tinyint(1) NOT NULL DEFAULT '0',
  `licensed_locations_delete_datetime` datetime DEFAULT NULL COMMENT 'When action to delete was made',
  `join_franchises_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`licensed_locations_id`),
  KEY `join_corporations_id` (`join_corporations_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1$$