drop table if exists `corporationsold`;
-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Corporations Table

delimiter $$;

CREATE TABLE `corporationsold` (
  `corporations_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_forums_id` int(11) NOT NULL,
  `corporations_name` varchar(255) NOT NULL,
  `corporations_address` varchar(255) NOT NULL,
  `corporations_city` varchar(255) NOT NULL,
  `corporations_state` varchar(255) NOT NULL,
  `corporations_zip` varchar(255) NOT NULL,
  `corporations_num_locations` int(11) NOT NULL,
  `corporations_admin_at_corporation` tinyint(1) NOT NULL DEFAULT '0',
  `corporations_separate_from_hotels` tinyint(1) NOT NULL DEFAULT '0',
  `corporations_active` tinyint(1) NOT NULL,
  `corporations_nextbill` datetime NOT NULL,
  `corporations_delete_datetime` datetime DEFAULT NULL COMMENT 'Date when corporation was marked as deleted',
  PRIMARY KEY (`corporations_id`),
  KEY `join_members_id` (`join_members_id`),
  KEY `join_forums_id` (`join_forums_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1$$;

INSERT INTO `corporationsold` SELECT * FROM `corporations`;

-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Hotels Table

delimiter $$

CREATE TABLE `hotelsold` (
  `hotels_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_corporations_id` int(11) NOT NULL,
  `hotels_name` varchar(255) NOT NULL,
  `hotels_address` varchar(255) NOT NULL,
  `hotels_city` varchar(255) NOT NULL,
  `hotels_state` varchar(255) NOT NULL,
  `hotels_zip` varchar(255) NOT NULL,
  `hotels_industry_type` varchar(255) NOT NULL,
  `hotels_num_rooms` int(11) NOT NULL,
  `hotels_franchise` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_active` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_delete_datetime` datetime DEFAULT NULL COMMENT 'When action to delete was made',
  `join_franchises_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`hotels_id`),
  KEY `join_corporations_id` (`join_corporations_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1$$

INSERT INTO `hotelsold` SELECT * FROM `hotels`;

-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Franchises Table

delimiter $$

CREATE TABLE `franchisesold` (
  `franchises_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_albums_photos_id` int(10) NOT NULL,
  `franchises_name` varchar(45) NOT NULL,
  `franchises_code` varchar(45) NOT NULL,
  `franchises_active` tinyint(1) NOT NULL DEFAULT '0',
  `franchises_delete_datetime` datetime DEFAULT NULL,
  `franchises_pct_discount` int(3) NOT NULL DEFAULT '0',
  `franchises_amount_discount` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`franchises_id`),
  UNIQUE KEY `franchises_code_UNIQUE` (`franchises_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8$$

INSERT INTO `franchisesold` SELECT * FROM `franchises`;


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Members Table

delimiter $$

CREATE TABLE `membersold` (
  `members_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `members_firstname` varchar(100) NOT NULL,
  `members_lastname` varchar(100) NOT NULL,
  `members_title` varchar(40) NOT NULL COMMENT 'position / title',
  `members_phone` varchar(20) NOT NULL,
  `members_fax` varchar(20) DEFAULT NULL,
  `members_email` varchar(255) NOT NULL,
  `members_password` varchar(255) NOT NULL,
  `members_last_login` datetime DEFAULT NULL,
  `members_lastsession` varchar(64) NOT NULL COMMENT 'id of a session',
  `members_datetime` datetime NOT NULL COMMENT 'Date and time when a person registered ',
  `members_membership_datetime` datetime NOT NULL COMMENT 'When a person became a member',
  `members_status` enum('free','member','exempt','locked') NOT NULL,
  `members_forum_signature` text NOT NULL,
  `members_type` enum('hotel','corporation','staff') NOT NULL DEFAULT 'hotel',
  `members_verified_code` char(32) NOT NULL,
  `members_billing_firstname` varchar(255) NOT NULL,
  `members_billing_lastname` varchar(255) NOT NULL,
  `members_billing_addr1` varchar(150) NOT NULL,
  `members_billing_addr2` varchar(150) NOT NULL,
  `members_billing_city` varchar(100) NOT NULL,
  `members_billing_state` varchar(100) NOT NULL,
  `members_billing_zip` varchar(25) NOT NULL,
  `members_billing_amt` decimal(6,2) NOT NULL COMMENT 'billing amount',
  `members_card_name` varchar(255) NOT NULL,
  `members_card_num` varchar(100) NOT NULL,
  `members_card_type` varchar(100) NOT NULL,
  `members_card_expire` date NOT NULL,
  `members_hr_agreement_yn` tinyint(1) NOT NULL COMMENT 'Whether a person agreed to HR agreement',
  `members_hr_agreement_datetime` datetime DEFAULT NULL COMMENT 'date and time when person agreed to HR agreement',
  `members_elaw_agreement_yn` int(11) NOT NULL DEFAULT '0',
  `members_elaw_agreement_datetime` datetime DEFAULT NULL,
  `members_locked_datetime` datetime NOT NULL,
  `members_show_startup_video` int(11) NOT NULL DEFAULT '1',
  `members_last_visit` datetime NOT NULL,
  PRIMARY KEY (`members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=138 DEFAULT CHARSET=latin1$$

INSERT INTO `membersold` SELECT * FROM `members`;

-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Staff Table

delimiter $$;

CREATE TABLE `staffold` (
  `staff_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_hotels_id` int(10) unsigned NOT NULL,
  `staff_datetime` datetime NOT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `join_hotels_id` (`join_hotels_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Table of staff members for hotels'$$;

INSERT INTO `staffold` SELECT * FROM `staff`;


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy Claims Table

delimiter $$;

CREATE TABLE `claimsold` (
  `claims_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `join_members_id` int(10) unsigned NOT NULL,
  `join_hotels_id2` int(10) unsigned NOT NULL,
  `join_carriers_id` int(10) unsigned NOT NULL,
  `claims_type` enum('Workers Comp','General Liability / Guest Property','Auto','Your Property') NOT NULL,
  `claims_datetime` datetime NOT NULL,
  `claims_datetime_submitted` datetime NOT NULL,
  `claims_filed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`claims_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1$$;

INSERT INTO `claimsold` SELECT * FROM `claims`;


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Copy hotels to Licensed Locations For staging

drop table `licensed_locations`;

delimiter $$

CREATE TABLE `licensed_locations` (
  `licensed_locations_id` int(11) NOT NULL AUTO_INCREMENT,
  `join_members_id` int(11) NOT NULL,
  `join_corporations_id` int(11) NOT NULL,
  `hotels_name` varchar(255) NOT NULL,
  `hotels_address` varchar(255) NOT NULL,
  `hotels_city` varchar(255) NOT NULL,
  `hotels_state` varchar(255) NOT NULL,
  `hotels_zip` varchar(255) NOT NULL,
  `hotels_industry_type` varchar(255) NOT NULL,
  `hotels_num_rooms` int(11) NOT NULL,
  `hotels_franchise` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_active` tinyint(1) NOT NULL DEFAULT '0',
  `hotels_delete_datetime` datetime DEFAULT NULL COMMENT 'When action to delete was made',
  `join_franchises_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`licensed_locations_id`),
  KEY `join_corporations_id` (`join_corporations_id`),
  KEY `join_members_id` (`join_members_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1$$;

INSERT INTO `licensed_locations` SELECT * FROM `hotels`;

ALTER TABLE `licensed_locations` DROP COLUMN `hotels_franchise` , DROP COLUMN `hotels_num_rooms` , DROP COLUMN `hotels_industry_type` ;
ALTER TABLE `licensed_locations` CHANGE COLUMN `hotels_name` `licensed_locations_name` VARCHAR(255) NOT NULL  , CHANGE COLUMN `hotels_address` `licensed_locations_address` VARCHAR(255) NOT NULL  , CHANGE COLUMN `hotels_city` `licensed_locations_city` VARCHAR(255) NOT NULL  , CHANGE COLUMN `hotels_state` `licensed_locations_state` VARCHAR(255) NOT NULL  , CHANGE COLUMN `hotels_zip` `licensed_locations_zip` VARCHAR(255) NOT NULL  , CHANGE COLUMN `hotels_active` `licensed_locations_active` TINYINT(1) NOT NULL DEFAULT '0'  , CHANGE COLUMN `hotels_delete_datetime` `licensed_locations_delete_datetime` DATETIME NULL DEFAULT NULL COMMENT 'When action to delete was made'  ;
ALTER TABLE `licensed_locations` CHANGE COLUMN `join_franchises_id` `join_franchises_id` INT(11) UNSIGNED NOT NULL  ;

-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Alter hotels

ALTER TABLE `hotels` DROP COLUMN `join_franchises_id` , DROP COLUMN `hotels_franchise` , DROP COLUMN `hotels_zip` , DROP COLUMN `hotels_state` , DROP COLUMN `hotels_city` , DROP COLUMN `hotels_address` , DROP COLUMN `hotels_name` , DROP COLUMN `join_corporations_id` , DROP COLUMN `join_members_id` , ADD COLUMN `join_licensed_locations_id` INT(11) NOT NULL DEFAULT 0  AFTER `hotels_delete_datetime` 
, DROP INDEX `join_members_id` 
, DROP INDEX `join_corporations_id` ;

UPDATE `hotels` SET `hotels_industry_type` = 'Hotel - Full Service/Resort' where `hotels_industry_type` in ('Hotel - Full Service', 'Resort');
DELETE FROM `hotels` where `hotels_industry_type` not in ('Hotel - Full Service/Resort', 'Hotel - Limited Service', 'Spa - Destination Spa');

-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Alter Franchises

ALTER TABLE `franchises` ADD COLUMN `join_corporations_id` INT(10) UNSIGNED NOT NULL  AFTER `franchises_amount_discount` ;


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Create Corporate Locations

delimiter $$

CREATE TABLE `corporate_locations` (
  `corporate_locations_id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_locations_num_rooms` int(11) NOT NULL,
  `corporate_locations_active` tinyint(1) NOT NULL DEFAULT '0',
  `corporate_locations_delete_datetime` datetime DEFAULT NULL COMMENT 'When action to delete was made',
  `join_licensed_locations_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`corporate_locations_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1$$


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Alter Staff for LicensedLocations

ALTER TABLE `staff` CHANGE COLUMN `join_hotels_id` `join_licensed_locations_id` INT(10) UNSIGNED NOT NULL  ;


-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- Alter Claims for LicensedLocations

ALTER TABLE `claims` CHANGE COLUMN `join_hotels_id2` `join_licensed_locations_id` INT(10) UNSIGNED NOT NULL  ;
