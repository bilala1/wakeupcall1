<?php

use Phinx\Migration\AbstractMigration;

class Accounts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     */
    public function change()
    {

        // set up accounts
        // we can move payment info here later if it should be at an account level
        // at this point do we still need corporation?  Just for corporate bulletin really.
        $accounts = $this->table('accounts', array('id' => 'accounts_id'));
        $accounts
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('accounts_type', 'enum', array('values' => 'single,multi', 'null' => false, 'default' => 'single'))
            ->create();


        // create corporate accounts
        $this->execute("INSERT INTO accounts (accounts_id, join_members_id, accounts_type)
                        SELECT join_members_id, join_members_id, 'multi' from corporations;");

        // create non-corporate accounts
        $this->execute("INSERT INTO accounts(accounts_id, join_members_id, accounts_type)
                        SELECT join_members_id, join_members_id, 'single' from licensed_locations WHERE join_corporations_id = 0;");

        // Add account id to corporations
        $corporations = $this->table('corporations');
        $corporations
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->save();
        $this->execute("UPDATE corporations SET join_accounts_id = join_members_id");

        // add account id to licensed_locations, remove corporations_id
        $licensed_locations = $this->table('licensed_locations');
        $licensed_locations
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->save();

        // add non-corporate location account_id
        $this->execute("UPDATE licensed_locations SET join_accounts_id = join_members_id WHERE join_corporations_id = 0;");

        // add corporate location account_id
        $this->execute("UPDATE licensed_locations ll SET join_accounts_id = (
                          SELECT c.join_members_id FROM corporations c
                          WHERE c.corporations_id = ll.join_corporations_id
                        ) WHERE join_corporations_id > 0;");

        // remove corporations_id
        $licensed_locations
            ->removeColumn('join_corporations_id')
            ->save();

        // add account id to members
        $members = $this->table('members');
        $members
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->addColumn('members_last_licensed_locations_id', 'integer')
            // Add new admin and user values to enum
            ->changeColumn('members_type', 'enum', array('values' => 'corporation,hotel,staff,admin,user'))
            ->save();


        // add account id to corporate admins
        $this->execute("UPDATE members SET join_accounts_id = members_id WHERE members_type = 'corporation';");

        // add account id to location admins
        $this->execute("UPDATE members SET join_accounts_id = (
                          SELECT ll.join_accounts_id FROM licensed_locations ll
                          WHERE ll.join_members_id = members_id
                        ), members_last_licensed_locations_id = (
                          SELECT ll.licensed_locations_id FROM licensed_locations ll
                          WHERE ll.join_members_id = members_id
                        ) WHERE members_type = 'hotel';");


        // add account id to staff
        $this->execute("UPDATE members SET join_accounts_id = (
                          SELECT ll.join_accounts_id FROM licensed_locations ll
                          INNER JOIN staff s ON ll.licensed_locations_id = s.join_licensed_locations_id
                          WHERE s.join_members_id = members_id
                        ), members_last_licensed_locations_id = (
                          SELECT s.join_licensed_locations_id FROM staff s
                          WHERE s.join_members_id = members_id
                        ) WHERE members_type = 'staff';");

        // Corporate admins become account admins
        $this->execute("UPDATE members SET members_type = 'admin' where members_type = 'corporation'");

        // Other users
        $this->execute("UPDATE members SET members_type = 'user' where members_type != 'admin'");

        // These were the location admins for single location memberships - make them account admins
        $this->execute("UPDATE members SET members_type = 'admin' where members_id = join_accounts_id");

        // Remove the old member types
        $members
            ->changeColumn('members_type', 'enum', array('values' => 'admin,user', 'default' => 'user'))
            ->save();

        // repurpose staff table as licensed_locations_x_members
        $licensed_locations_x_members = $this->table('staff');
        $licensed_locations_x_members
            ->rename('licensed_locations_x_members')
            ->renameColumn('staff_id', 'licensed_locations_x_members_id')
            ->addColumn('licensed_locations_x_members_role', 'enum', array('values' => 'admin,staff'))
            ->update();

        // set staff role
        $this->execute("UPDATE licensed_locations_x_members SET licensed_locations_x_members_role = 'staff';");

        // add location admins
        $this->execute("INSERT INTO licensed_locations_x_members (join_licensed_locations_id, join_members_id, licensed_locations_x_members_role)
                        SELECT licensed_locations_id, join_members_id, 'admin' FROM licensed_locations;");

        //Fix permissions table column names and set them up based on location
        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->renameColumn('members_access_level_id', 'members_access_levels_id')
            ->renameColumn('members_access_level_name', 'members_access_levels_name')
            ->renameColumn('members_access_level_path', 'members_access_levels_type')
            ->addColumn('members_access_level_scope',  'enum', array('values' => 'account,location', 'null' => false, 'default' => 'account'))
            ->update();

// Original values
//            +-------------------------+---------------------------+----------------------------------------------+
//            | members_access_level_id | members_access_level_name | members_access_level_path                    |
//            +-------------------------+---------------------------+----------------------------------------------+
//            |                       1 | Certificate Tracking      | /members/my-documents/certificates/index.php |
//            |                       2 | Claims Reporting          | /members/claims/index.php                    |
//            |                       3 | Contracts Management      | /members/contracts/index.php                 |
//            |                       4 | Employment Law            | /members/employmentlaw/terms.php             |
//            |                       5 | HR                        | /members/hr/index.php                        |
//            |                       6 | Message Forum             | /members/forums/view-forum.php               |
//            +-------------------------+---------------------------+----------------------------------------------+

        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'certificates', members_access_level_scope='location' WHERE members_access_levels_name = 'Certificate Tracking';");
        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'claims', members_access_level_scope='location' WHERE members_access_levels_name = 'Claims Reporting';");
        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'contracts', members_access_level_scope='location' WHERE members_access_levels_name = 'Contracts Management';");
        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'elaw', members_access_level_scope='account' WHERE members_access_levels_name = 'Employment Law';");
        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'hr', members_access_level_scope='account' WHERE members_access_levels_name = 'HR';");
        $this->execute("UPDATE members_access_levels SET members_access_levels_type = 'forum', members_access_level_scope='account' WHERE members_access_levels_name = 'Message Forum';");

        //Fix other permissions table column names and set them up based on location
        $members_access = $this->table('members_access');
        $members_access
            ->renameColumn('members_access_level_id', 'join_members_access_levels_id')
            ->update();

        $licensed_locations_x_members_access = $this->table('licensed_locations_x_members_access', array('id' => 'licensed_locations_x_members_access_id'));
        $licensed_locations_x_members_access
            ->addColumn('join_licensed_locations_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('join_members_access_levels_id', 'integer', array('null' => false))
            ->addIndex(array('join_licensed_locations_id', 'join_members_id', 'join_members_access_levels_id'), array('unique' => true))
            ->create();

        // Create location level permissions
        $this->execute("INSERT INTO licensed_locations_x_members_access (join_licensed_locations_id, join_members_id, join_members_access_levels_id)
                        SELECT join_licensed_locations_id, ma.join_members_id, join_members_access_levels_id
                        FROM members_access ma
                        INNER JOIN members_access_levels mal on mal.members_access_levels_id = ma.join_members_access_levels_id
                        INNER JOIN licensed_locations_x_members ll on ma.join_members_id = ll.join_members_id
                        WHERE members_access_level_scope='location'");

        // Delete location level permissions from account level table
        $this->execute("DELETE FROM members_access WHERE join_members_access_levels_id IN
                        (
                            SELECT members_access_levels_id from members_access_levels
                            WHERE members_access_level_scope='location'
                        )");
    }


    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}