<?php

use Phinx\Migration\AbstractMigration;

class ClaimsSubmitType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('claims');
        $table->addColumn("claims_submit_type", 'string', array('limit' => 5))
            ->update();
        $this->execute("UPDATE claims SET claims_submit_type = 'fax'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('claims');
        $table->removeColumn("claims_submit_type");
    }
}