<?php

use Phinx\Migration\AbstractMigration;

class ClaimStatusCode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {

        // Create the claim status code table
        $claims_status_code = $this->table('claims_status_codes', array('id' => false,
            'primary_key' => array('claims_status_codes_id')));
        $claims_status_code->addColumn('claims_status_codes_id', 'char', array('limit' => 12))
            ->addColumn('claims_status_code_description', 'string', array('limit' => 50))
            ->addIndex(array('claims_status_codes_id'), array('unique' => true))
            ->save();

        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('PENDING', 'Submission Pending')");
        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('SUBMIT_OPEN', 'Submitted - Open')");
        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('SUBMIT_CLOSE', 'Submitted - Closed')");
        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('RECORD_ONLY', 'Record Only - Not submitted')");
        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('FIRST_AID', 'First Aid Only - Not submitted')");
        $this->execute("insert into claims_status_codes(claims_status_codes_id, claims_status_code_description)
                values('FAILED', 'Submission Failed')");

        $this->table('claims')->addColumn('join_claims_status_codes_id', 'char', array('limit' => 12))
            ->addForeignKey('join_claims_status_codes_id', 'claims_status_codes', 'claims_status_codes_id')
            ->update();

        $this->execute("update claims set join_claims_status_codes_id = 'PENDING' where claims_status = 'Submission Pending'");
        $this->execute("update claims set join_claims_status_codes_id = 'SUBMIT_OPEN' where claims_status = 'Submitted - Open'");
        $this->execute("update claims set join_claims_status_codes_id = 'SUBMIT_CLOSE' where claims_status = 'Submitted - Closed'");
        $this->execute("update claims set join_claims_status_codes_id = 'RECORD_ONLY' where claims_status = 'Record Only - Not submitted'");
        $this->execute("update claims set join_claims_status_codes_id = 'FIRST_AID' where claims_status = 'First Aid Only - Not submitted'");
        $this->execute("update claims set join_claims_status_codes_id = 'FAILED' where claims_status = 'Submission Failed'");
        $this->execute("update claims set join_claims_status_codes_id = 'RECORD_ONLY' where join_claims_status_codes_id is null or join_claims_status_codes_id = ''");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('claims_status_codes');
        $this->table('claims')->removeColumn('join_claims_status_codes_id')->update();
    }
}