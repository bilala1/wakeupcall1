<?php

use Phinx\Migration\AbstractMigration;

class ClaimsAccountRefactor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('claims_join_licensed_location_backup')
            ->addColumn('join_claims_id','integer')
            ->create();
        $this->execute("insert claims_join_licensed_location_backup (join_claims_id) select claims_id from claims where claims.join_licensed_locations_id = 0");
        $this->execute("update claims
            join licensed_locations on claims.join_members_id =  licensed_locations.join_members_id
            set claims.join_licensed_locations_id = licensed_locations.licensed_locations_id
            where claims.join_licensed_locations_id = 0");

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("update claims
            join claims_join_licensed_location_backup on claims.claims_id =  claims_join_licensed_location_backup.join_claims_id
            set claims.join_licensed_locations_id = 0");
        $this->dropTable('claims_join_licensed_location_backup');
    }
}