<?php

use Phinx\Migration\AbstractMigration;

class MigrateCorporateStaff extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //Make corporate staff admins
        $this->execute("update members set members_type = 'admin'
                        where exists (
                            select 1 from licensed_locations_x_members ll
                            inner join corporate_locations cl on ll.join_licensed_locations_id = cl.join_licensed_locations_id
                            where members_id = ll.join_members_id)");

        //Remove location-specific permissions for admins
        $this->execute("delete from licensed_locations_x_members
                        where join_members_id in (
                            select members_id from members where members_type = 'admin')");

        $this->execute("delete from licensed_locations_x_members_access
                        where join_members_id in (
                            select members_id from members where members_type = 'admin')");

        $this->execute("delete from members_access
                        where join_members_id in (
                            select members_id from members where members_type = 'admin')");

    }
    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}