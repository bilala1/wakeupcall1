<?php

use Phinx\Migration\AbstractMigration;

class LicensedLocationPhone extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     */
    public function change()
    {
        $licensed_locations = $this->table('licensed_locations');
        $licensed_locations
            ->addColumn('licensed_locations_phone', 'string', array('limit' => 20, 'null' => false))
            ->addColumn('licensed_locations_fax', 'string', array('limit' => 20))
            ->save();

        $this->execute("
        UPDATE licensed_locations
        INNER JOIN members on join_members_id = members_id
            SET licensed_locations_phone = members_phone,
                licensed_locations_fax = members_fax");
    }
}