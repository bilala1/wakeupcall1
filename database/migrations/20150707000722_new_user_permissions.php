<?php

use Phinx\Migration\AbstractMigration;

class NewUserPermissions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->changeColumn('members_access_level_scope', 'enum', array('values' => 'admin,account,location', 'null' => false, 'default' => 'account'))
            ->update();

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_level_scope, members_access_levels_name)
                        VALUES ('manageUsers', 'location', 'Manage Users');");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_level_scope, members_access_levels_name)
                        VALUES ('editLocation', 'location', 'Edit Location');");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_level_scope, members_access_levels_name)
                        VALUES ('locationAdmin', 'location', 'Location Admin');");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_level_scope, members_access_levels_name)
                        VALUES ('manageAdmins', 'admin', 'Manage Account Admins');");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_level_scope, members_access_levels_name)
                        VALUES ('shareDocuments', 'account', 'Share Documents');");

        $this->execute("INSERT INTO licensed_locations_x_members_access (join_members_access_levels_id, join_licensed_locations_id, join_members_id)
                        SELECT members_access_levels_id, join_licensed_locations_id, join_members_id
                        FROM licensed_locations_x_members
                        INNER JOIN members_access_levels on members_access_levels_type = 'locationAdmin'
                        WHERE licensed_locations_x_members_role = 'admin';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DELETE FROM licensed_locations_x_members_access
                        WHERE EXISTS(
                            SELECT 1 from members_access_levels
                            WHERE members_access_levels_type = 'locationAdmin'
                            AND members_access_levels_id = join_members_access_levels_id);");

        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'manageUsers';");
        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'editLocation';");
        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'locationAdmin';");
        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'manageAdmins';");
        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'shareDocuments';");

        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->changeColumn('members_access_level_scope', 'enum', array('values' => 'account,location', 'null' => false, 'default' => 'account'))
            ->update();
    }
}