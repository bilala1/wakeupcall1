<?php

use Phinx\Migration\AbstractMigration;

class AccessColumnRename extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->renameColumn('members_access_level_scope', 'members_access_levels_scope')
            ->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->renameColumn('members_access_levels_scope', 'members_access_level_scope')
            ->update();
    }
}