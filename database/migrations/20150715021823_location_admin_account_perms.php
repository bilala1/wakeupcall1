<?php

use Phinx\Migration\AbstractMigration;

class LocationAdminAccountPerms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO members_access (join_members_id, join_members_access_levels_id)
                        SELECT ma.members_id, mal.members_access_levels_id
                        FROM members ma
                        INNER JOIN members_access_levels mal on mal.members_access_levels_scope = 'account'
                        INNER JOIN licensed_locations_x_members_access llxma on ma.members_id = llxma.join_members_id
                        INNER JOIN members_access_levels maladmin on maladmin.members_access_levels_id = llxma.join_members_access_levels_id
                        WHERE maladmin.members_access_levels_type='locationAdmin'");

        $this->execute("DELETE FROM members_access
                         WHERE join_members_access_levels_id in (
                            SELECT members_access_levels_id
                            from members_access_levels
                            where members_access_levels_type = 'shareDocuments')");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DELETE FROM members_access
                         WHERE join_members_id in (
                            SELECT join_members_id
                            from licensed_locations_x_members_access
                            inner join members_access_levels on join_members_access_levels_id = members_access_levels_id
                            where members_access_levels_type = 'locationAdmin')
                          AND join_members_access_levels_id in (
                            SELECT members_access_levels_id
                            from members_access_levels
                            where members_access_levels_scope = 'account')");
    }
}