<?php

use Phinx\Migration\AbstractMigration;

class AccountDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $accounts = $this->table('accounts');
        $accounts
            ->addColumn('accounts_name', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('accounts_address', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('accounts_city', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('accounts_state', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('accounts_zip', 'string', array('limit' => 255, 'null' => false))
            ->save();

        $this->execute("UPDATE accounts JOIN licensed_locations on accounts_id = join_accounts_id
                        SET accounts_name = licensed_locations_name,
                            accounts_address = licensed_locations_address,
                            accounts_city = licensed_locations_city,
                            accounts_state = licensed_locations_state,
                            accounts_zip = licensed_locations_zip
                        WHERE accounts_type = 'single'");
        
        $this->execute("UPDATE accounts JOIN corporations on accounts_id = join_accounts_id
                        SET accounts_name = corporations_name,
                            accounts_address = corporations_address,
                            accounts_city = corporations_city,
                            accounts_state = corporations_state,
                            accounts_zip = corporations_zip
                        WHERE accounts_type = 'multi'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $accounts = $this->table('accounts');
        $accounts
            ->removeColumn('accounts_name')
            ->removeColumn('accounts_address')
            ->removeColumn('accounts_city')
            ->removeColumn('accounts_state')
            ->removeColumn('accounts_zip')
            ->save();

    }
}