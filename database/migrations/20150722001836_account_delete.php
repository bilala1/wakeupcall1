<?php

use Phinx\Migration\AbstractMigration;

class AccountDelete extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $accounts = $this->table('accounts');
        $accounts
            ->addColumn('accounts_delete_datetime', 'datetime', array('null' => true, 'default' => null))
            ->save();

        $this->execute("UPDATE accounts JOIN licensed_locations on accounts_id = join_accounts_id
                        SET accounts_delete_datetime = licensed_locations_delete_datetime
                        WHERE accounts_type = 'single'");

        $this->execute("UPDATE accounts JOIN corporations on accounts_id = join_accounts_id
                        SET accounts_delete_datetime = corporations_delete_datetime
                        WHERE accounts_type = 'multi'");

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $accounts = $this->table('accounts');
        $accounts
            ->removeColumn('accounts_delete_datetime', 'datetime')
            ->save();
    }
}