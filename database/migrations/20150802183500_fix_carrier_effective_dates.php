<?php

use Phinx\Migration\AbstractMigration;

class FixCarrierEffectiveDates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
         $this->execute("UPDATE carriers set carriers_effective_start_date = null, carriers_effective_end_date = null where carriers_effective_start_date = '1972-01-01 00:00:00'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
         $this->execute("UPDATE carriers set carriers_effective_start_date = '1972-01-01 00:00:00', carriers_effective_end_date = '1972-01-01 00:00:00' where carriers_effective_start_date is null");

    }
}
