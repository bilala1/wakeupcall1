<?php

use Phinx\Migration\AbstractMigration;

class AddAccountIdToCarriers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('carriers');
        $table->addColumn('join_accounts_id', 'integer', array('null' => false, 'after' => 'carriers_id') );
        $table->update();
        $this->execute("UPDATE carriers SET join_accounts_id=join_members_id;");
        $table->removeColumn('join_members_id');

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('carriers');
        $table->addColumn('join_members_id', 'integer', array('null' => false, 'after' => 'carriers_id') );
        $table->update();
        $this->execute("UPDATE carriers SET join_members_id=join_accounts_id;");
        $table->removeColumn('join_accounts_id');
    }
}