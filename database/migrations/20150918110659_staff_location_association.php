<?php

use Phinx\Migration\AbstractMigration;

class StaffLocationAssociation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name)
                        VALUES ('associated', 'location', 'Works here');");

        $this->execute("INSERT INTO licensed_locations_x_members_access (join_members_access_levels_id, join_licensed_locations_id, join_members_id)
                        SELECT members_access_levels_id, join_licensed_locations_id, join_members_id
                         FROM members_access_levels, licensed_locations_x_members
                         inner join members on join_members_id = members_id
                         where members_type = 'user'
                         and licensed_locations_x_members_role = 'staff'
                         and members_access_levels_type = 'associated';
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DELETE FROM licensed_locations_x_members_access
                        WHERE EXISTS(
                            SELECT 1 from members_access_levels
                            WHERE members_access_levels_type = 'associated'
                            AND members_access_levels_id = join_members_access_levels_id);");

        $this->execute("DELETE FROM members_access_levels where members_access_levels_type = 'associated';");

    }
}