<?php

use Phinx\Migration\AbstractMigration;

class FixColumnCarriersAllRecieve extends AbstractMigration
{


    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('carriers');
        // can't use renameColumn because of an error with boolean defaults, need to add, drop and update
        $table->addColumn('carriers_all_receive', 'boolean', array('default' => 0));
        $table->update();
        $this->execute('update carriers set carriers_all_receive = carriers_all_recieve');
        $table->removeColumn('carriers_all_recieve');
        $table->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('carriers');
        // can't use renameColumn because of an error, need to add, drop and update
        $table->addColumn('carriers_all_recieve', 'boolean', array('default' => 0));
        $table->update();
        $this->execute('update carriers set carriers_all_recieve = carriers_all_receive');
        $table->removeColumn('carriers_all_receive');
        $table->update();

    }
}