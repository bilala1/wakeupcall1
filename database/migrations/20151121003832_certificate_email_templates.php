<?php

use Phinx\Migration\AbstractMigration;

class CertificateEmailTemplates extends AbstractMigration
{

    public function up()
    {
        $certs_email_templates = $this->table('certificates_email_templates', array('id' => 'certificates_email_templates_id'))->create();
        
        $certs_email_templates = $this->table('certificates_email_templates');
        $certs_email_templates->addColumn('join_accounts_id', 'integer', array('null' => false));
        $certs_email_templates->addForeignKey('join_accounts_id', 'accounts', 'accounts_id');
        $certs_email_templates->save();
                
        $certs_email_templates->addColumn('join_licensed_locations_id', 'integer', array('null' => false));
        $certs_email_templates->save();
        
        $certs_email_templates->addColumn('email_html', 'string', array('limit' => 16777215));
        $certs_email_templates->save();
        
        

        $licensed_locations_x_email_permissions = $this->table('licensed_locations_x_email_permissions', array('id' => 'licensed_locations_x_email_permissions_id'))->create();
        $licensed_locations_x_email_permissions = $this->table('licensed_locations_x_email_permissions');

        $licensed_locations_x_email_permissions->addColumn('join_licensed_locations_id', 'integer', array('null' => false));
        $licensed_locations_x_email_permissions->save();
        
        $licensed_locations_x_email_permissions->addColumn('location_can_override_certificates_email', 'boolean', array('null' => false));
        $licensed_locations_x_email_permissions->save();
    }

    
    public function down()
    {
        $this->dropTable('licensed_locations_x_email_permissions');
        $this->dropTable('certificates_email_templates');
    }
}
