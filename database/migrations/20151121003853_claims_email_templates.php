<?php

use Phinx\Migration\AbstractMigration;

class ClaimsEmailTemplates extends AbstractMigration
{
    public function up()
    {
        $claims_email_templates = $this->table('claims_email_templates', array('id' => true,
            'primary_key' => array('claims_email_templates_id')))->create();
        
        $claims_email_templates = $this->table('claims_email_templates');
        
        $claims_email_templates->addColumn('join_accounts_id', 'integer', array('null' => false));
        $claims_email_templates->addForeignKey('join_accounts_id', 'accounts', 'accounts_id');
        $claims_email_templates->save();
        
        $claims_email_templates->addColumn('join_licensed_locations_id', 'integer', array('null' => false));
        $claims_email_templates->save();
        
        $claims_email_templates->addColumn('email_html', 'string', array('limit' => 16777215));
        $claims_email_templates->save();
        
        $claims_fax_templates = $this->table('claims_fax_templates', array('id' => true,
            'primary_key' => array('claims_fax_templates_id')));
        
        $claims_fax_templates = $this->table('claims_fax_templates');
        
        $claims_fax_templates->addColumn('join_accounts_id', 'integer', array('null' => false));
        $claims_fax_templates->addForeignKey('join_accounts_id', 'accounts', 'accounts_id');
        
        $claims_fax_templates->addColumn('join_licensed_locations_id', 'integer', array('null' => false));
        $claims_fax_templates->save();
        
        $claims_fax_templates->addColumn('email_html', 'string', array('limit' => 16777215));
        $claims_fax_templates->save();
        
        $licensed_locations_x_email_permissions = $this->table('licensed_locations_x_email_permissions');
    
        $licensed_locations_x_email_permissions->addColumn('location_can_override_claims_email', 'boolean', array('null' => false));
        $licensed_locations_x_email_permissions->addColumn('location_can_override_claims_fax', 'boolean', array('null' => false));
        $licensed_locations_x_email_permissions->save();
    }
    
    public function down()
    {
        $licensed_locations_x_email_permissions = $this->table('licensed_locations_x_email_permissions');
    
        $licensed_locations_x_email_permissions->removeColumn('location_can_override_claims_email');
        $licensed_locations_x_email_permissions->removeColumn('location_can_override_claims_fax');
        
        $this->dropTable('claims_email_templates');
        $this->dropTable('claims_fax_templates');

    }
}
