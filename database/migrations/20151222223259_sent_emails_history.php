<?php

use Phinx\Migration\AbstractMigration;

class SentEmailsHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $claims_email_templates = $this->table('sent_emails_history', array('id' => 'sent_emails_history_id'))->create();
        
        $claims_email_templates = $this->table('sent_emails_history');
        
        $claims_email_templates->addColumn('join_accounts_id', 'integer');
        $claims_email_templates->addForeignKey('join_accounts_id', 'accounts', 'accounts_id');
               
        $claims_email_templates->addColumn('join_licensed_locations_id', 'integer', array('null' => false));
               
        $claims_email_templates->addColumn('sent_emails_date', 'datetime', array('null' => false));
              
        $claims_email_templates->addColumn('sent_emails_desc', 'string', array('limit' => 30));
                
        $claims_email_templates->addColumn('sent_emails_html', 'string', array('limit' => 16777215));
        $claims_email_templates->save();
    }
}