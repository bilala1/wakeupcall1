<?php

use Phinx\Migration\AbstractMigration;

class AccessLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $access_log = $this->table('access_logs', array('id' => 'access_logs_id'));
        $access_log
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->addColumn('access_logs_path', 'string', array('null' => false, 'length' => 255))
            ->addColumn('access_logs_query', 'string', array('null' => true, 'length' => 255))
            ->addColumn('access_logs_method', 'string', array('null' => false, 'length' => 8))
            ->addColumn('access_logs_datetime', 'datetime', array('null' => false))
            ->create();
    }
}
