<?php

use Phinx\Migration\AbstractMigration;

class ExpirationEmails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $accounts = $this->table('certificates');
        $accounts
            ->addColumn('certificates_remind_vendor_expired', 'boolean')
            ->addColumn('certificates_remind_location_expired', 'boolean')
            ->addColumn('certificates_remind_admin_expired', 'boolean')
            ->addColumn('certificates_remind_other_expired', 'string', array('limit' => 50))
            ->update();

        $this->query('UPDATE certificates SET
          certificates_remind_vendor_expired = certificates_remind_vendor,
          certificates_remind_location_expired = certificates_send_copy,
          certificates_remind_admin_expired = certificates_remind_corporate_admin,
          certificates_remind_other_expired = certificates_remind_other
          where certificates_remind_member = 1');
    }
}