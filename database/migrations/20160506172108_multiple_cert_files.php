<?php

use Phinx\Migration\AbstractMigration;

class MultipleCertFiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $cert_files = $this->table('certificates_files', array('id' => 'certificates_files_id'));
        $cert_files
            ->addColumn('join_certificates_id', 'integer', array('null' => false))
            ->addColumn('certificates_files_file', 'string', array('null' => false, 'length' => 255))
            ->addColumn('certificates_files_filename', 'string', array('null' => false, 'length' => 255))
            ->addColumn('certificates_files_datetime', 'datetime', array('null' => true))
            ->addColumn('certificates_files_active', 'boolean', array('null' => false, 'default' => 0))
            ->create();

        $this->execute(
            'insert into certificates_files
              (join_certificates_id, certificates_files_file, certificates_files_filename, certificates_files_datetime, certificates_files_active)
              SELECT certificates_id, certificates_file, certificates_filename, null, true
              FROM certificates
              WHERE certificates_file != ""');
    }
}
