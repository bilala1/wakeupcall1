<?php

use Phinx\Migration\AbstractMigration;

class AccountFeatureFlags extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $accounts_members_access = $this->table('account_features', array('id' => 'account_features_id'));
        $accounts_members_access
            ->addColumn('account_features_type', 'string', array('null' => false, 'length' => 100))
            ->addColumn('account_features_name', 'string', array('null' => false, 'length' => 100))
            ->create();

        $accounts_members_access = $this->table('accounts_x_account_features', array('id' => 'accounts_x_account_features_id'));
        $accounts_members_access
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->addColumn('join_account_features_id', 'integer', array('null' => false))
            ->addColumn('accounts_x_account_features_enabled', 'boolean', array('null' => false, 'default' => false))
            ->create();

        foreach(array(
            'claims' => 'Claims Management',
            'certificates' => 'Certificate Tracking',
            'contracts' => 'Contracts Management',
            'elaw' => 'Employment Law',
            'hr' => 'HR',
            'forum' => 'Message Forum'
                ) as $type=>$name) {
            $this->query(
                "INSERT INTO account_features(account_features_type, account_features_name)
                  VALUES ('$type', '$name')"
            );
        }
    }
}
