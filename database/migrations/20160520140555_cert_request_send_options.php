<?php

use Phinx\Migration\AbstractMigration;

class CertRequestSendOptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $certificates = $this->table('certificates');
        $certificates
            ->addColumn('certificates_request_cert_to_email', 'string', array('length' => 255, 'null' => true))
            ->addColumn('certificates_request_cert_to_address', 'boolean', array('null' => false))
            ->addColumn('certificates_request_cert_to_fax', 'boolean', array('null' => false))
            ->update();

        $this->execute('UPDATE certificates SET certificates_request_cert_to_fax = 1, certificates_request_cert_to_address = 1');
    }
}
