<?php

use Phinx\Migration\AbstractMigration;

class DropFaxTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims_fax_templates = $this->table('claims_fax_templates');
        $claims_fax_templates->drop();

        $licensed_locations_x_email_permissions = $this->table('licensed_locations_x_email_permissions');
        $licensed_locations_x_email_permissions->removeColumn('location_can_override_claims_fax');
        $licensed_locations_x_email_permissions->update();
    }
}
