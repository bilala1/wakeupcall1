<?php

use Phinx\Migration\AbstractMigration;

class UpdateDefaultEmailTemplates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('DELETE from default_email_templates');

        $default_email_templates = $this->table('default_email_templates');

        $seed_data = array(
            array(
                'email_template_description' => 'Certificates_Request',
                'email_html' => '<table width="100%" cellspacing="0" cellpadding="10" bgcolor="#ffffff"><tbody><tr><td valign="top"><table width="550" cellspacing="0" cellpadding="0"><tbody><tr><td width="20">&nbsp;</td><td>To {:vendors_name},<br /><br /><p>{:location_name} requires all of our business associates to provide certificates of insurance to protect us and you in our business together. You are required to provide a current certificate of insurance for the following lines of coverage for {:location_name}:</p><p>{:certificates_coverages}</p><p>Please submit the required certificate(s) as soon as possible by:</p>{:send_certificate_to}<p>If you have any questions, please contact us.</p><p>Sincerely,</p><p>{:location_name}</p></td><td width="20">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>',
            ),
            array(
                'email_template_description' => 'Claims_Email',
                'email_html' => '<table width="100%" cellspacing="0" cellpadding="10" bgcolor="#ffffff"><tbody><tr><td valign="top"><table width="550" cellspacing="0" cellpadding="0"><tbody><tr><td colspan="3"><img src="http://wakeupcall.net/images/logo.png" alt="" /></td></tr><tr><td width="20">&nbsp;</td><td><p>Dear {:carriers_name},</p><p>Please accept this as notice of a claim for location: {:claim_location}</p><p>Policy number: {:policy_number}</p><p>Please see the attached file(s) for more information.</p><p>Thank you,</p><p>{:members_firstname}&nbsp;{:members_lastname}</p><h2>WAKEUP CALL</h2></td><td width="20">&nbsp;</td></tr><tr><td colspan="3">&nbsp;</td></tr><tr><td colspan="3">Copyright &copy; 2016 WAKEUP CALL</td></tr></tbody></table></td></tr></tbody></table>',
            )
        );

        $default_email_templates->insert($seed_data);
        $default_email_templates->save();
    }
}
