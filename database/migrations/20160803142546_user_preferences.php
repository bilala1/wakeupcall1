<?php

use Phinx\Migration\AbstractMigration;

class UserPreferences extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        // set up accounts
        // we can move payment info here later if it should be at an account level
        // at this point do we still need corporation?  Just for corporate bulletin really.
        $userPrefs = $this->table('user_prefs', array('id' => 'user_prefs_id'));
        $userPrefs
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('user_prefs_cert_expire_lead_days', 'integer', array('null' => false, 'default' => '14'))
            ->addColumn('user_prefs_contract_expire_lead_days', 'integer', array('null' => false, 'default' => '60'))
            ->addColumn('user_prefs_claims_recent_incident_days', 'integer', array('null' => false, 'default' => '30'))
            ->create();

    }
}
