<?php

use Phinx\Migration\AbstractMigration;

class AdditionalInsured extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $certificates_table = $this->table('certificates');

        $certificates_table->addColumn('certificates_include_additional_insured_request', 'boolean', array('default' => 0));
        $certificates_table->addColumn('certificates_default_location_name', 'string', array('limit' => 255,'null' => true));
        $certificates_table->addColumn('certificate_additional_request_text', 'string', array('null' => true));
        $certificates_table->update();
    }
}
