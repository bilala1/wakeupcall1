<?php

use Phinx\Migration\AbstractMigration;

class CertificateRemindMembers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $accounts = $this->table('certificate_remind_members', array('id' => 'certificate_remind_members_id'));
        $accounts
            ->addColumn('join_certificates_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('certificates_remind_before_expiry', 'boolean')
            ->addColumn('certificates_remind_after_expiry', 'boolean')
            ->create();

    }
}
