<?php

use Phinx\Migration\AbstractMigration;

class MigrateCertificatesRemindMembers extends AbstractMigration
{
    /**
     * Run during migration
     */
    public function up()
    {
        $certificates = $this->query('
            select *
            from certificates c
            left join licensed_locations ll on licensed_locations_id = join_licensed_locations_id
            where
              certificates_remind_admin_expired = 1
              OR certificates_remind_location_expired = 1
              OR certificates_send_copy = 1
              OR certificates_remind_corporate_admin = 1
            ');

        foreach ($certificates as $certificate) {
            $certificates_id = $certificate['certificates_id'];

            $notify_user_expired = 0;
            $notify_admin_expired = 0;

            if ($certificate['certificates_remind_member']) {
                $notify_user_expired = $certificate['certificates_remind_location_expired'];
                $notify_admin_expired = $certificate['certificates_remind_admin_expired'];
            }

            if ($certificate['certificates_send_copy'] || $notify_user_expired) {
                $location_admins = $this->query('
                    SELECT members_id FROM members
                    INNER JOIN licensed_locations_x_members_access ON members_id = join_members_id
                    INNER JOIN members_access_levels ON join_members_access_levels_id = members_access_levels_id
                    WHERE
                    members_access_levels_type = "locationAdmin"
                    AND join_licensed_locations_id = ' . $certificate['join_licensed_locations_id']);


                foreach ($location_admins as $admin) {
                    $this->query('
                        INSERT INTO certificate_remind_members (join_certificates_id, join_members_id, certificates_remind_before_expiry, certificates_remind_after_expiry)
                        VALUES (' . $certificate['certificates_id'] . ',' . $admin['members_id'] . ',' . $certificate['certificates_send_copy'] . ',' . $notify_user_expired . ')');
                }
            }

            if ($certificate['certificates_remind_corporate_admin'] || $notify_admin_expired) {
                $account_admins = $this->query('
                    SELECT members_id FROM members
                    WHERE
                    members_type = "admin"
                    AND join_accounts_id = ' . $certificate['join_accounts_id']);

                foreach ($account_admins as $admin) {
                    $this->query('
                        INSERT INTO certificate_remind_members (join_certificates_id, join_members_id, certificates_remind_before_expiry, certificates_remind_after_expiry)
                        VALUES (' . $certificate['certificates_id'] . ',' . $admin['members_id'] . ',' . $certificate['certificates_send_copy'] . ',' . $notify_user_expired . ')');
                }
            }
        }
    }

    /**
     * Run during rollback
     */
    public function down()
    {
        $this->query('DELETE FROM certificate_remind_members');
    }
}
