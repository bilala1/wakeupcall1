<?php

use Phinx\Migration\AbstractMigration;

class AddEmailTemplateName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $certificates_email_templates = $this->table('certificates_email_templates');
        $certificates_email_templates
            ->addColumn('join_certificates_id', 'integer', array('null' => true))
            ->addColumn('certificates_email_templates_name', 'string', array('after' => 'certificates_email_templates_id','null' => true,'default' => 'No Name'))
            ->update();
        $certificates = $this->table('certificates');
        $certificates
            ->addColumn('join_certificates_email_templates_id', 'integer', array('null' => true))
            ->update();
    }
}
