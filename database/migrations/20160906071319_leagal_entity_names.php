<?php

use Phinx\Migration\AbstractMigration;

class LeagalEntityNames extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $legal_entity_names = $this->table('legal_entity_names', array('id' => 'legal_entity_names_id'));
        $legal_entity_names
            ->addColumn('legal_entity_name', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->addColumn('join_locations_id', 'integer', array('null' => true))
            ->addColumn('legal_entity_names_hidden', 'boolean', array('default' => 0))
            ->save();
    }
}
