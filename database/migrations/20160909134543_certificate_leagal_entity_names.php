<?php

use Phinx\Migration\AbstractMigration;

class CertificateLeagalEntityNames extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
       $certificates = $this->table('certificates');
       $certificates
            ->removeColumn('certificates_default_location_name')
            ->removeColumn('certificate_additional_request_text')
            ->update();
        $legal_entity_names = $this->table('certificates_x_legal_entity_names', array('id' => 'certificates_x_legal_entity_names_id'));
        $legal_entity_names
            ->addColumn('join_legal_entity_names_id', 'integer', array('null' => false))
            ->addColumn('join_certificates_id', 'integer', array('null' => false))
            ->save();
    }
}
