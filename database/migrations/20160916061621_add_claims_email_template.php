<?php

use Phinx\Migration\AbstractMigration;

class AddClaimsEmailTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims_email_templates = $this->table('claims_email_templates');
        $claims_email_templates
            ->renameColumn('id', 'claims_email_templates_id')
            ->addColumn('join_claims_id', 'integer', array('null' => true))
            ->addColumn('claims_email_templates_name', 'string', array('after' => 'claims_email_templates_id','null' => true,'default' => 'No Name'))
            ->addColumn('claims_email_templates_default', 'boolean', array('default' => 0))
            ->update();
        $claims = $this->table('claims');
        $claims
            ->addColumn('join_claims_email_templates_id', 'integer', array('null' => true))
            ->update();
    }
}
