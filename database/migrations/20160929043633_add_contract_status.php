<?php

use Phinx\Migration\AbstractMigration;

class AddContractStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $contracts = $this->table('contracts');
        $contracts
            ->changeColumn('contracts_status', 'enum', array('values' => 'Active,Month to Month,Expired,Inactive,Autorenew-Annual'))
            ->changeColumn('contracts_transition_status_to', 'enum', array('values' => 'Month to Month,Expired,Inactive,Autorenew-Annual'))
            ->save();

    }
}
