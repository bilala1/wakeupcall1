<?php

use Phinx\Migration\AbstractMigration;

class ContractRemindMembers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $remindmembers = $this->table('contract_remind_members', array('id' => 'contract_remind_members_id'));
        $remindmembers
            ->addColumn('join_contracts_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('contracts_remind_expiry', 'boolean')
            ->create();
    }
}
