<?php

use Phinx\Migration\AbstractMigration;

class CertificateCoverages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $certificate_coverages = $this->table('certificate_coverages', array('id' => 'certificate_coverages_id'));
        $certificate_coverages
            ->addColumn('join_certificates_id', 'integer', array('null' => false))
            ->addColumn('certificate_coverages_name', 'string', array('null' => false))
            ->addColumn('certificate_coverages_shortname', 'string', array('null' => true))
            ->addColumn('certificate_coverages_amount_1', 'decimal', array('null' => true))
            ->addColumn('certificate_coverages_amount_2', 'decimal', array('null' => true))
            ->addColumn('certificate_coverages_amount_3', 'decimal', array('null' => true))
            ->create();
    }
}
