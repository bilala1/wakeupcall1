<?php

use Phinx\Migration\AbstractMigration;

class MigrateCertificateCoverages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $certificates =  $this->query('SELECT certificates_id,certificates_coverages,certificates_coverage_other from certificates');
        foreach($certificates as $certificate){
            $coverages = explode(',', $certificate['certificates_coverages']);
            $certificate_id = $certificate['certificates_id'];
            $other_coverage = $certificate['certificates_coverage_other'];
            if($other_coverage!='')    $coverages[] = $other_coverage;
            foreach($coverages as $coverage){
                if($coverage){
                    preg_match('#\((.*?)\)#', $coverage, $match);
                    $short_name = count($match) > 1 ? $match[1] : '';

                     $this->query("INSERT INTO certificate_coverages (join_certificates_id,certificate_coverages_name,"
                            . "certificate_coverages_shortname) values($certificate_id,'".addslashes($coverage)."','".addslashes($short_name)."')");
                }
            }

        }
    }
    /**
     * Run during rollback
     */
    public function down()
    {
        $this->query('DELETE FROM certificate_coverages');
    }
}
