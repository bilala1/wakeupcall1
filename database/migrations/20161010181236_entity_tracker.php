<?php

use Phinx\Migration\AbstractMigration;

class EntityTracker extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $entities = $this->table('entities', array('id' => 'entities_id'));
        $entities
            ->addColumn('join_accounts_id', 'integer', array('null' => false))
            ->addColumn('entities_name', 'string', array('null' => false))
            ->addColumn('entities_delete_datetime', 'datetime', array('null' => true))
            ->addColumn('entities_hidden', 'boolean', array('null' => false, 'default' => '0'))
            ->create();

        $entitiesNotes = $this->table('entities_notes', array('id' => 'entities_notes_id'));
        $entitiesNotes
            ->addColumn('join_entities_id', 'integer', array('null' => false))
            ->addColumn('entities_notes_subject', 'string', array('null' => false, 'length' => 255))
            ->addColumn('entities_notes_note', 'string', array('null' => false))
            ->addColumn('entities_notes_datetime', 'datetime', array('null' => false))
            ->create();

        $entitiesFiles = $this->table('entities_files', array('id' => 'entities_files_id'));
        $entitiesFiles
            ->addColumn('join_entities_id', 'integer', array('null' => false))
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->create();

        $entitiesBusiness = $this->table('business_entities', array('id' => 'business_entities_id'));
        $entitiesBusiness
            ->addColumn('join_entities_id', 'integer', array('null' => false))
            ->addColumn('join_licensed_locations_id', 'integer', array('null' => false))
            ->create();

        $entitiesItems = $this->table('entities_items', array('id' => 'entities_items_id'));
        $entitiesItems
            ->addColumn('join_entities_id', 'integer', array('null' => false))
            ->addColumn('entities_items_name', 'string', array('null' => false, 'length' => 255))
            ->create();

        $entitiesItemsFiles = $this->table('entities_items_files', array('id' => 'entities_items_files_id'));
        $entitiesItemsFiles
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->create();

        $entitiesHistory = $this->table('entities_history', array('id' => 'entities_history_id'));
        $entitiesHistory
            ->addColumn('join_entities_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('entities_history_datetime', 'datetime', array('null' => false))
            ->addColumn('entities_history_description', 'string', array('null' => false))
            ->create();

        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels
            ->addColumn('members_access_levels_sensitive', 'boolean', array('null' => false, 'default' => '0'))
            ->update();

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name, members_access_levels_sensitive)
                        VALUES ('business_entities', 'location', 'Business Tracker', true);");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name, members_access_levels_sensitive)
                        VALUES ('personnel_entities', 'location', 'Personnel Tracker', true);");

    }
}
