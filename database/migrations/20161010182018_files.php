<?php

use Phinx\Migration\AbstractMigration;

class Files extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create new files table
        $files = $this->table('files', array('id' => 'files_id'));
        $files
            ->addColumn('files_category', 'string', array('null' => false, 'limit' => 31))
            ->addColumn('files_storage_type', 'enum', array('values' => array('local','s3'), 'null' => false, 'default' => 'local'))
            ->addColumn('files_name', 'string', array('null' => false, 'limit' => 255))
            ->addColumn('files_location', 'string', array('null' => false, 'limit' => 255))
            ->addColumn('files_size', 'integer')
            ->addColumn('files_datetime', 'datetime', array('null' => true))
            ->create();

        // migrate existing files
        
        // My Documents/Document Library
        $documents = $this->table('documents');
        $documents
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->update();
        $this->execute("INSERT INTO files (files_category, files_storage_type, files_name, files_location, files_datetime)
                        SELECT 'documents', 'local', documents_filename, CONCAT('library/', documents_file), documents_datetime
                        FROM documents");
        $this->execute("UPDATE documents, files set documents.join_files_id = files_id
                        WHERE files_name = documents_filename
                          AND files_category = 'documents'");

        // Claims
        $claims_files = $this->table('claims_files');
        $claims_files
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->update();
        $this->execute("INSERT INTO files (files_category, files_storage_type, files_name, files_location)
                        SELECT 'claims', 'local', claims_files_filename, CONCAT('claims/', claims_files_file)
                        FROM claims_files");
        $this->execute("UPDATE claims_files, files set claims_files.join_files_id = files_id
                        WHERE files_name = claims_files_filename
                          AND files_category = 'claims'");

        // Certificates
        $certificates = $this->table('certificates_files');
        $certificates
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->update();
        $this->execute("INSERT INTO files (files_category, files_storage_type, files_name, files_location, files_datetime)
                        SELECT 'certificates', 'local', certificates_files_filename, CONCAT('certificates/', certificates_files_file), certificates_files_datetime
                        FROM certificates_files");
        $this->execute("UPDATE certificates_files, files set certificates_files.join_files_id = files_id
                        WHERE files_name = certificates_files_filename
                          AND files_category = 'certificates'");

        // Contracts
        $contracts_files = $this->table('contracts_files');
        $contracts_files
            ->addColumn('join_files_id', 'integer', array('null' => false))
            ->update();
        $this->execute("INSERT INTO files (files_category, files_storage_type, files_name, files_location, files_datetime)
                        SELECT 'contracts', 'local', contracts_files_filename, CONCAT('contracts/', contracts_files_file), contracts_files_datetime
                        FROM contracts_files");
        $this->execute("UPDATE contracts_files, files set contracts_files.join_files_id = files_id
                        WHERE files_name = contracts_files_filename
                          AND files_category = 'contracts'");
    }
}
