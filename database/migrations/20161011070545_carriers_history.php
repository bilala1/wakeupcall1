<?php

use Phinx\Migration\AbstractMigration;

class CarriersHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $carriers_history = $this->table('carriers_history', array('id' => 'carriers_history_id'))->create();
        
        $carriers_history = $this->table('carriers_history');
        
        $carriers_history->addColumn('carriers_history_datetime','datetime', array('null' => false));
        $carriers_history->addColumn('join_members_id', 'integer');
        $carriers_history->addColumn('join_carriers_id', 'integer');
        $carriers_history->addColumn('carriers_history_description', 'string', array('limit' => 2000));
        $carriers_history->save();
               
    }
}
