<?php

use Phinx\Migration\AbstractMigration;

class MigrateContractRemindMembers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $contracts = $this->query("select * from contracts c
        where contracts_expired_notification= 1");

        foreach($contracts as $contract){
            $emailMembers = $this->query('
                    select m.*
                    from contracts c
                    inner join members m on c.join_accounts_id = m.join_accounts_id
                    where m.members_type = \'admin\' and contracts_id = '.$contract['contracts_id']);
            if($contract['contracts_all_locations'] == 1) {
                    $locationAdmins = $this->query('
                    select m.*
                from contracts c
                inner join members m on c.join_accounts_id = m.join_accounts_id
                inner join licensed_locations_x_members_access ll on m.members_id = ll.join_members_id
                inner join members_access_levels on join_members_access_levels_id = members_access_levels_id
                inner join licensed_locations on ll.join_licensed_locations_id = licensed_locations_id
                where members_access_levels_type = \'locationAdmin\' and licensed_locations_delete_datetime IS NULL
                and contracts_id = '.$contract['contracts_id']); 
            } else {
                $locationAdmins = $this->query('
                select llm.*
                from contracts c
                inner join contracts_x_licensed_locations cxll on c.contracts_id = cxll.join_contracts_id
                inner join licensed_locations_x_members_access ll on cxll.join_licensed_locations_id = ll.join_licensed_locations_id
                inner join members llm on ll.join_members_id = llm.members_id
                inner join members_access_levels on join_members_access_levels_id = members_access_levels_id
                inner join licensed_locations on cxll.join_licensed_locations_id = licensed_locations_id
                where members_access_levels_type = \'locationAdmin\' and licensed_locations_delete_datetime IS NULL
                and contracts_id ='.$contract['contracts_id']);
            }
            foreach($emailMembers as $emailMember){
                $admin_users[$emailMember['members_id']] = $emailMember['members_id'];
                $this->query( "INSERT INTO contract_remind_members (join_contracts_id, join_members_id, contracts_remind_expiry)"
                . " VALUES(".$contract['contracts_id'].",".$emailMember['members_id'].",1".")");
            }
            foreach($locationAdmins as $locationAdmin){
                $location_users[$locationAdmin['members_id']] = $locationAdmin['members_id'];
                $this->query("INSERT INTO contract_remind_members (join_contracts_id, join_members_id, contracts_remind_expiry)"
                . " VALUES(".$contract['contracts_id'].",".$locationAdmin['members_id'].",1".")");
            }

        }
    }
}
