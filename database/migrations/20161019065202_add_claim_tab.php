<?php

use Phinx\Migration\AbstractMigration;

class AddClaimTab extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims = $this->table('claims');
        $claims
                ->changeColumn('claims_type', 'enum', array('values' => 'Workers Comp,General Liability / Guest Property,Auto,Your Property,Labor Law,Other'))
                ->save();
        
        $claim_employment = $this->table('claim_employment', array('id' => 'claim_employment_id'));
        $claim_employment
            ->addColumn('claim_employment_claimant', 'string', array('limit' => 255, 'null' => false))
            ->addColumn('claim_employment_job_title', 'string', array('limit' => 255,'null' => false))
            ->addColumn('claim_employment_department', 'string', array('limit' => 255,'null' => false))
            ->addColumn('claim_employment_incident_location', 'string', array('limit' => 500,'null' => false))
            ->addColumn('join_claims_id', 'integer', array('null' => false))
            ->save();
        $claim_employment_involved_parties = $this->table('claim_employment_involved_parties', array('id' => 'claim_employment_involved_parties_id'));
        $claim_employment_involved_parties
                ->addColumn('claim_employment_involved_parties_name', 'string', array('limit' => 255, 'null' => false))
                ->addColumn('join_claims_id', 'integer', array('null' => false))
                ->save();
    }
}
