<?php

use Phinx\Migration\AbstractMigration;

class AddClaimsCarriers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims_carriers = $this->table('claims_x_carriers', array('id' => 'claims_x_carriers_id'));
        $claims_carriers
            ->addColumn('join_claims_id', 'integer', array('null' => false))
            ->addColumn('join_carriers_id', 'integer', array('null' => false))
            ->addColumn('claims_x_carriers_claim_number', 'string', array('null' => true))
            ->addColumn('claims_x_carriers_datetime_submitted', 'datetime', array('null' => true))
            ->addColumn('claims_x_carriers_datetime_closed', 'datetime', array('null' => true))
            ->addColumn('claims_x_carriers_amount_paid', 'decimal', array('null' => true))
            ->addColumn('claims_x_carriers_amount_reserved', 'decimal', array('null' => true))
            ->addColumn('claims_x_carriers_workers_comp_medical_costs', 'decimal', array('null' => true))
            ->addColumn('join_claims_status_codes_id', 'string', array('null' => true))
            ->create();
    }
}
