<?php

use Phinx\Migration\AbstractMigration;

class MigrateClaimsCarriers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $claims = $this->query('select * from claims where join_carriers_id!=0 ');
        foreach ($claims as $claim) {
            $carrier_id = $claim['join_carriers_id'];
            $claims_id = $claim['claims_id'];
            $claim_number = $claim['claims_claim_number'] ? "'" . $claim['claims_claim_number'] . "'" : 'null';
            $datetime_submitted = $claim['claims_datetime_submitted'] ? "'" . $claim['claims_datetime_submitted'] . "'" : 'null';
            $datetime_closed = $claim['claims_datetime_closed'] ? "'" . $claim['claims_datetime_closed'] . "'" : 'null';
            $amount_paid = $claim['claims_amount_paid'] ? $claim['claims_amount_paid'] : 0;
            $amount_reserved = $claim['claims_amount_reserved'] ? $claim['claims_amount_reserved'] : 0;
            $status_code = $claim['join_claims_status_codes_id'] ? $claim['join_claims_status_codes_id'] : null;
            $this->query("INSERT INTO claims_x_carriers (join_carriers_id, join_claims_id, claims_x_carriers_claim_number, claims_x_carriers_datetime_submitted, 
                    claims_x_carriers_datetime_closed, claims_x_carriers_amount_paid, claims_x_carriers_amount_reserved, join_claims_status_codes_id) values(
                    $carrier_id, $claims_id, $claim_number, $datetime_submitted, $datetime_closed, $amount_paid, $amount_reserved, '$status_code')");
        }
    }

    public function down()
    {
        $this->query('DELETE FROM claims_x_carriers');
    }
}
