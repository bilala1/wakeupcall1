<?php

use Phinx\Migration\AbstractMigration;

class LocationEmailReceipients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $carriers = $this->table('carriers');
        $carriers
            ->addColumn('carriers_notify_on_save_all', 'boolean', array('default' => 0))
            ->addColumn('carriers_notify_on_submission_all', 'boolean', array('default' => 0))
            ->update();
        $email_recipients = $this->table('licensed_locations_x_email_recipients');
        $email_recipients
                ->addColumn('always_email_on_save', 'boolean', array('default' => 0))
                ->addColumn('always_email_on_submit', 'boolean', array('default' => 0))
                ->update();
        
    }
}
