<?php

use Phinx\Migration\AbstractMigration;

class AdjusterDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims_x_carriers = $this->table('claims_x_carriers');
        $claims_x_carriers
            ->addColumn('claims_x_carriers_adjusters_name', 'string', array('null' => true))
            ->addColumn('claims_x_carriers_adjusters_email', 'string', array('null' => true))
            ->addColumn('claims_x_carriers_adjusters_phone', 'string', array('null' => true))
            ->update();
    }
}
