<?php

use Phinx\Migration\AbstractMigration;

class AddSensitiveData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name, members_access_levels_sensitive)
                        VALUES ('workers_comp', 'account', 'Workers Comp Claims', true);");

        $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name, members_access_levels_sensitive)
                        VALUES ('labor_claims', 'account', 'Labor Claims', true);");
        
         $this->execute("INSERT INTO members_access_levels (members_access_levels_type, members_access_levels_scope, members_access_levels_name, members_access_levels_sensitive)
                        VALUES ('other_claims', 'account', 'Other Claims', true);");
    }
}
