<?php

use Phinx\Migration\AbstractMigration;

class AlterEntityItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $entity_items = $this->table('entities_items');
        $entity_items
            ->addColumn('entities_items_details', 'string', array('null' => true))
            ->addColumn('entities_items_effective_date', 'date', array('null' => true))
            ->update();
        $entity_items_notes = $this->table('entities_items_notes', array('id' => 'entities_items_notes_id'));
        $entity_items_notes
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('entities_items_notes_subject', 'string', array('null' => true))
            ->addColumn('entities_items_notes_note', 'text', array('null' => true))
            ->addColumn('entities_items_notes_datetime', 'datetime', array('null' => false))
            ->create();
        $entity_items_history = $this->table('entities_items_history', array('id' => 'entities_items_history_id'));
        $entity_items_history
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->addColumn('entities_items_history_datetime', 'datetime', array('null' => false))
            ->addColumn('entities_items_history_description', 'string', array('length' => 2000))
            ->create();
    }
}
