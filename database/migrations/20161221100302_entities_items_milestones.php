<?php

use Phinx\Migration\AbstractMigration;

class EntitiesItemsMilestones extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $entities_items_milestones = $this->table('entities_items_milestones', array('id' => 'entities_items_milestones_id'));
        $entities_items_milestones
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('entities_items_milestones_date', 'date', array('null' => false))
            ->addColumn('entities_items_milestones_description', 'string', array('null' => true))
            ->create();
    }
}
