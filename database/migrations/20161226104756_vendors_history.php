<?php

use Phinx\Migration\AbstractMigration;

class VendorsHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $vendors_history = $this->table('vendors_history', array('id' => 'vendors_history_id'))->create();
        
        $vendors_history = $this->table('vendors_history');
        
        $vendors_history->addColumn('vendors_history_datetime','datetime', array('null' => false));
        $vendors_history->addColumn('join_members_id', 'integer');
        $vendors_history->addColumn('join_vendors_id', 'integer');
        $vendors_history->addColumn('vendors_history_description', 'string', array('length' => 2000));
        $vendors_history->save();
    }
}
