<?php

use Phinx\Migration\AbstractMigration;

class ChangeFieldLength extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claim_employment = $this->table('claim_employment');
        $claim_employment
            ->changeColumn('claim_employment_incident_location', 'string', array('length' => 500))
            ->save();
        
        $carriers_history = $this->table('carriers_history');
        $carriers_history
            ->changeColumn('carriers_history_description', 'string', array('length' => 2000))
            ->save();
        
        $certificates_email_templates = $this->table('certificates_email_templates');
        $certificates_email_templates
            ->changeColumn('email_html', 'text', array('length' => 16777215))
            ->save();
        
        $claims_email_templates = $this->table('claims_email_templates');
        $claims_email_templates
            ->changeColumn('email_html', 'text', array('length' => 16777215))
            ->save();
        
        $sent_emails_history = $this->table('sent_emails_history');
        $sent_emails_history
            ->changeColumn('sent_emails_html', 'text', array('length' => 16777215))
            ->save();
        
        $claims_email_templates = $this->table('claims_email_templates');
        $claims_email_templates
            ->changeColumn('email_html', 'text', array('length' => 16777215))
            ->save();
    }
}
