<?php

use Phinx\Migration\AbstractMigration;

class AddDocumentsToLocation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $members_x_documents = $this->table('members_x_documents');
        $members_x_documents->addColumn('join_accounts_id', 'integer', array('null'=>true));
        $members_x_documents->addColumn('members_x_documents_all_locations', 'boolean', array('default' => 0));
        $members_x_documents->update();
        
        $members_x_documents_x_licensed_locations = $this->table('members_x_documents_x_licensed_locations', array('id' => 'members_x_documents_x_licensed_locations_id'));
        $members_x_documents_x_licensed_locations
            ->addColumn('join_members_x_documents_id', 'integer', array('null' => false))
            ->addColumn('join_licensed_locations_id', 'integer', array('null' => false))
            ->create();
        
        $this->query('UPDATE members_access_levels set members_access_levels_scope = "location" WHERE members_access_levels_type="shareDocuments" ');
        $this->query('UPDATE members_x_documents, members SET members_x_documents.join_accounts_id = members.join_accounts_id where join_members_id = members_id');

    }
}
