<?php

use Phinx\Migration\AbstractMigration;

class MigrateSharedDocumentsPerLocation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $accesses = $this->query(
            "SELECT * FROM members_access_levels
              JOIN members_access ON members_access_levels_id = join_members_access_levels_id
              WHERE members_access_levels_type = 'shareDocuments'"
        );
        
        foreach($accesses as $access){
            $members_access_levels_id = $access['members_access_levels_id'];
            $members_id = $access['join_members_id'];
            $locations = $this->query(
                "SELECT DISTINCT llxm.join_licensed_locations_id FROM licensed_locations_x_members_access llxm
                  WHERE join_members_id = $members_id"
            );

            foreach($locations as $location){
                $location_id = $location['join_licensed_locations_id'];
                $this->query(
                    "INSERT INTO licensed_locations_x_members_access (join_licensed_locations_id, join_members_id, join_members_access_levels_id)
                      VALUES ($location_id, $members_id, $members_access_levels_id)"
                );
            }

        }

        $members = $this->query('
        SELECT members_id, members_type, members_x_documents_id
        FROM members
        JOIN members_x_documents ON (join_members_id = members_id) WHERE documents_is_corporate_shared = 1
         ');
        foreach($members as $member){
            $members_x_documents_id = $member['members_x_documents_id'];
            if($member['members_type'] == 'admin'){
                //set alllocation
                $this->query(
                    "UPDATE members_x_documents set members_x_documents_all_locations = 1
                      WHERE members_x_documents_id = $members_x_documents_id"
                );
            }
            if($member['members_type'] == 'user'){
                //set locations
                $members_id = $member['members_id'];
                $locations = $this->query(
                    "SELECT DISTINCT(join_licensed_locations_id) FROM licensed_locations_x_members_access llxm
                        INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
                        WHERE llxm.join_members_id = $members_id
                        AND licensed_locations_delete_datetime IS NULL"
                );

                foreach($locations as $locations){
                    $location_id = $locations['join_licensed_locations_id'];
                    $this->query(
                        "INSERT INTO members_x_documents_x_licensed_locations (join_members_x_documents_id, join_licensed_locations_id)
                          VALUES ($members_x_documents_id, $location_id)");
                }

            }
        }
    }
}
