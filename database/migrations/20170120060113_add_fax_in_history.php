<?php

use Phinx\Migration\AbstractMigration;

class AddFaxInHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sent_emails_history = $this->table('sent_emails_history');
        $sent_emails_history
            ->addColumn('sent_emails_communication_type', 'enum', array('values' => 'email,fax', 'null' => false, 'default' => 'email'))
            ->update();
    }
}
