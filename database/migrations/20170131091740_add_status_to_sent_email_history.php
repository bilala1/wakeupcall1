<?php

use Phinx\Migration\AbstractMigration;

class AddStatusToSentEmailHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sent_emails_history = $this->table('sent_emails_history');
        $sent_emails_history
            ->addColumn('sent_emails_history_status', 'enum', array('values' => 'success,pending,fail', 'null' => true,'default' => null))
            ->addColumn('sent_emails_history_status_reason', 'string', array('length' => 255, 'null' => true))
            ->addColumn('sent_emails_history_submission_id', 'string', array('length' => 255, 'null' => true))
            ->update();
    }
}
