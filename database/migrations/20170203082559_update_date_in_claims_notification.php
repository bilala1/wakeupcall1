<?php

use Phinx\Migration\AbstractMigration;

class UpdateDateInClaimsNotification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $email_html = '<table width="100%" cellspacing="0" cellpadding="10" bgcolor="#ffffff"><tbody><tr><td valign="top"><table width="550" cellspacing="0" cellpadding="0"><tbody><tr><td colspan="3"><img src="http://wakeupcall.net/images/logo.png" alt="" /></td></tr><tr><td width="20">&nbsp;</td><td><p>Date: {:todays_date}</p><p>Dear {:carriers_name},</p><p>Please accept this as notice of a claim for location: {:claim_location}</p><p>Policy number: {:policy_number}</p><p>Claimant: {:claimant}</p><p>Date of Incident: {:date_of_incident}</p><p>Date Notified: {:date_notified}</p><p>Brief Description: {:brief_description}</p><p>Please see the attached file(s) for more information.</p><p>Thank you,</p><p>{:members_firstname}&nbsp;{:members_lastname}</p><h2>WAKEUP CALL</h2></td><td width="20">&nbsp;</td></tr><tr><td colspan="3">&nbsp;</td></tr><tr><td colspan="3">Copyright &copy; 2016 WAKEUP CALL</td></tr></tbody></table></td></tr></tbody></table>';
        $this->execute("update default_email_templates set email_html= '$email_html' WHERE email_template_description='Claims_Email'");
    }
}
