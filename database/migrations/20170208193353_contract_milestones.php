<?php

use Phinx\Migration\AbstractMigration;

class ContractMilestones extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $contracts_milestones_remind_days = $this->table('contracts_milestones_remind_days', array('id' => 'contracts_milestones_remind_days_id'));
        $contracts_milestones_remind_days
            ->addColumn('join_contracts_id', 'integer', array('null' => false))
            ->addColumn('join_contracts_milestones_id', 'integer', array('null' => false))
            ->addColumn('contracts_milestones_remind_days', 'integer', array('null' => false))
            ->create();
        $contracts_milestones_remind_members = $this->table('contracts_milestones_remind_members', array('id' => 'contracts_milestones_remind_members_id'));
        $contracts_milestones_remind_members
            ->addColumn('join_contracts_id', 'integer', array('null' => false))
            ->addColumn('join_contracts_milestones_id', 'integer', array('null' => false))
            ->addColumn('join_members_id', 'integer', array('null' => false))
            ->create();
        $contracts_milestones_additional_recipients = $this->table('contracts_milestones_additional_recipients', array('id' => 'contracts_milestones_additional_recipients_id'));
        $contracts_milestones_additional_recipients
            ->addColumn('join_contracts_id', 'integer', array('null' => false))
            ->addColumn('join_contracts_milestones_id', 'integer', array('null' => false))
            ->addColumn('contracts_milestones_additional_recipients_email', 'string', array('null' => false))
            ->create();
    }
}
