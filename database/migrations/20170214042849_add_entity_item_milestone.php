<?php

use Phinx\Migration\AbstractMigration;

class AddEntityItemMilestone extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $entities_items_milestones_remind_days = $this->table('entities_items_milestones_remind_days', array('id' => 'entities_items_milestones_remind_days_id'));
        $entities_items_milestones_remind_days
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('join_entities_items_milestones_id', 'integer', array('null' => false))
            ->addColumn('entities_items_milestones_remind_days', 'integer', array('null' => false))
            ->create();
        $entities_items_milestones_remind_members = $this->table('entities_items_milestones_remind_members');
        $entities_items_milestones_remind_members
            ->addColumn('join_entities_items_milestones_id', 'integer', array('null' => false))
            ->update();
        $entities_items_additional_recipients = $this->table('entities_items_milestones_additional_recipients', array('id' => 'entities_items_milestones_additional_recipients_id'));
        $entities_items_additional_recipients
            ->addColumn('join_entities_items_id', 'integer', array('null' => false))
            ->addColumn('join_entities_items_milestones_id', 'integer', array('null' => false))
            ->addColumn('entities_items_milestones_additional_recipients_email', 'string', array('null' => false))
            ->create();
        $this->dropTable('entities_items_additional_recipients');
    }
}
