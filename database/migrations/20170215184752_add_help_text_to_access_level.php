<?php

use Phinx\Migration\AbstractMigration;

class AddHelpTextToAccessLevel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $members_access_levels = $this->table('members_access_levels');
        $members_access_levels->addColumn('members_access_levels_help_text', 'string', array('length'=>1000,'null'=>true));
        $members_access_levels->update();
        $this->execute("UPDATE members_access_levels set members_access_levels_help_text='This permission allows users access to Labor claims for any locations where they have the Claims Tracking permission.' WHERE members_access_levels_type='labor_claims' ");
        $this->execute("UPDATE members_access_levels set members_access_levels_help_text='This permission allows users access to \'Other\' claims for any locations where they have the Claims Tracking permission.'  WHERE members_access_levels_type='other_claims' ");
        $this->execute("UPDATE members_access_levels set members_access_levels_help_text='This permission allows users access to Worker\'s\ Compensation claims for any locations where they have the Claims Tracking permission.' WHERE members_access_levels_type='workers_comp'");
    }
}
