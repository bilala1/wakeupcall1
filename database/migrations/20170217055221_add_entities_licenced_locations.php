<?php

use Phinx\Migration\AbstractMigration;

class AddEntitiesLicencedLocations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $business_entities = $this->table('business_entities');
        $business_entities->addColumn('business_entities_all_locations', 'boolean', array('default' => 0));
        $business_entities->addColumn('join_accounts_id', 'integer', array('null' => false));
        $business_entities->update();
        
        $entities_x_licensed_location = $this->table('business_entities_x_licensed_locations', array('id' => 'entities_x_licensed_locations_id'));
        $entities_x_licensed_location
            ->addColumn('join_business_entities_id', 'integer', array('null' => false))
            ->addColumn('join_licensed_locations_id', 'integer', array('null' => false))
            ->create();
    }
}
