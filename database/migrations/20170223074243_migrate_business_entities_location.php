<?php

use Phinx\Migration\AbstractMigration;

class MigrateBusinessEntitiesLocation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $business_entities =  $this->query('SELECT * from business_entities where join_licensed_locations_id != 0');
        foreach($business_entities as $business_entity){
            $join_licensed_locations_id = $business_entity['join_licensed_locations_id'];
            $business_entity_id = $business_entity['business_entities_id'];
            $this->execute(
                    "INSERT INTO business_entities_x_licensed_locations (join_business_entities_id,join_licensed_locations_id) "
            . "VALUES($business_entity_id,$join_licensed_locations_id)"
                );
        }
        
//        $business_entities = $this->table('business_entities');
//        $business_entities->removeColumn('join_licensed_locations_id');
//        $business_entities->update();
    }
}
