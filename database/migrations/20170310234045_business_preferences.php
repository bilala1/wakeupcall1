<?php

use Phinx\Migration\AbstractMigration;

class BusinessPreferences extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $user_prefs = $this->table('user_prefs');
        $user_prefs->addColumn('user_prefs_business_entities_milestones_lead_days', 'integer', array('default' => 30));
        $user_prefs->update();
    }
}
