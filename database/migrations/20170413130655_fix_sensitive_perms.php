<?php

use Phinx\Migration\AbstractMigration;

class FixSensitivePerms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $members = $this->query("SELECT distinct members_id from members m
         LEFT JOIN licensed_locations_x_members_access ma ON (m.members_id  = ma.join_members_id)
         LEFT JOIN members_access_levels mal ON (ma.join_members_access_levels_id = mal.members_access_levels_id)
         WHERE m.members_type = 'admin' or mal.members_access_levels_type='claims' or mal.members_access_levels_type='locationAdmin'");
        $sensitive_access = $this->query("SELECT * from members_access_levels where members_access_levels_scope = 'account' and members_access_levels_sensitive=1 ");
        $sensitive_ids = array();
        foreach($sensitive_access as $access){
            $sensitive_ids[] = $access['members_access_levels_id'];
        }
        foreach($members as $member){
            $members_id = $member['members_id'];
            foreach($sensitive_ids as $sensitive_id){
                try {
                    $this->query("INSERT INTO members_access(join_members_id,join_members_access_levels_id) VALUES ($members_id,$sensitive_id)");
                } catch(Exception $e) {
                    //duplicate record, ignore
                }
            }
        }
    }
}
