<?php

use Phinx\Migration\AbstractMigration;

class AddUsageSummary extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $access_logs = $this->table('access_logs');
        $access_logs->renameColumn('processed', 'access_logs_processed');
        $access_logs->save();

        $daily_summarys = $this->table('daily_summarys', array('id' => 'daily_summarys_id'));

        $daily_summarys->addColumn('daily_summarys_date', 'date', array('null' => false));
        $daily_summarys->addColumn('join_accounts_id', 'integer', array('null' => false));
        $daily_summarys->addColumn('join_members_id', 'integer', array('null' => false));
        $daily_summarys->addColumn('daily_summarys_total', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_certs', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_files', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_claims', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_contracts', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_library', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_forums', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_other', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_dashboard', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_hr', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_elaw', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_businesses', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_webinars', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_search', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->addColumn('daily_summarys_category_news', 'integer', array('null' => false, 'default'=>0));
        $daily_summarys->create();

    }
}
