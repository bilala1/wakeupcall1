<?php

use Phinx\Migration\AbstractMigration;

class AccountStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $accounts = $this->table('accounts');

        $accounts->addColumn('accounts_is_locked', 'string', array('null' => false, 'default' => false));
        $accounts->save();

        $this->query('update accounts, members set accounts_is_locked = true where join_members_id = members_id and members_status = \'locked\'');
    }
}
