<?php

use Phinx\Migration\AbstractMigration;

class DocumentLibraryLinks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $documents = $this->table('documents');
        $documents->addColumn('documents_format', 'enum', array('values' => 'file,url,hr360', 'null' => false, 'default' => 'file'));
        $documents->save();
    }
}
