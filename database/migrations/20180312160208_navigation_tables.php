<?php

use Phinx\Migration\AbstractMigration;

class NavigationTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // set up accounts
        // we can move payment info here later if it should be at an account level
        // at this point do we still need corporation?  Just for corporate bulletin really.
        $siteFeatures = $this->table('site_features', array('id' => 'site_features_id'));
        $siteFeatures
            ->addColumn('site_features_code', 'string', array('null' => false))
            ->addColumn('site_features_name', 'string', array('null' => false))
            ->addColumn('site_features_active', 'boolean', array('null' => false))
            ->create();

        $this->query("INSERT INTO site_features (site_features_code, site_features_name, site_features_active) VALUES 
            ('dashboard', 'Dashboard', 1), 
            ('files', 'My Files', 1), 
            ('certificates', 'Certificate Tracking', 1), 
            ('claims', 'Claim Tracking', 1),
            ('contracts', 'Contract Tracking', 1),
            ('business_entities', 'Business Tracking', 1),
            ('hr', 'HR', 1),
            ('elaw', 'Employment Law', 1),
            ('training', 'Training', 0),
            ('webinars', 'Webinars', 0),
            ('forum', 'Forums', 0),
            ('doclibrary', 'Document Library', 1),
            ('sds', 'SDS Library', 1),
            ('definitions', 'Definitions', 1),
            ('services', 'Additional Services', 0),
            ('sitenews', 'Site News', 1),
            ('hospitalitynews', 'Hospitality News', 1),
            ('hrnews', 'HR News', 1),
            ('contact', 'Contact Page', 1),
            ('tutorials', 'Tutorials', 1),
            ('faqs', 'FAQs', 1)
            ");
    }
}