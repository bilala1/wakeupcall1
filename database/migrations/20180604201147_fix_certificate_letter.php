<?php

use Phinx\Migration\AbstractMigration;

class FixCertificateLetter extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $email_html = "<table width=100% cellspacing=0 cellpadding=10 bgcolor=#ffffff><tbody><tr><td valign=top><table width=550 cellspacing=0 cellpadding=0><tbody><tr><td width=20>&nbsp;</td><td>
                <p>To {:vendors_name},</p>
                <p>{:location_name} requires all of our business associates, vendors and suppliers
                provide certificates of insurance for our protection in case of an incident involving
                our property or our guests. We ask that you provide a current certificate of insurance
                for the following lines of coverage:</p>
                <p>{:certificates_coverages}</p>
                <p>{:additional_insured}</p>
                <p>Please submit the required certificate(s) as soon as possible by one of the following:</p>
                {:send_certificate_to}
                <p>If you have any questions, please don\'t hesitate to contact us.</p>
                <p>Sincerely,</p>
                <p>{:location_name}</p>
                </td><td width=20>&nbsp;</td></tr></tbody></table></td></tr></tbody></table>";
        $this->execute("update default_email_templates set email_html= '$email_html' WHERE email_template_description='Certificates_Request'");
    }
}
