<?php

use Phinx\Migration\AbstractMigration;

class ChangeGlClaimType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $claims = $this->table('claims');
        $claims
            ->changeColumn('claims_type', 'enum', array('values' => 'Workers Comp,General Liability / Guest Property,General Liability / Property of Others,Auto,Your Property,Labor Law,Other'))
            ->save();

        $this->execute("update claims set claims_type='General Liability / Property of Others' where claims_type='General Liability / Guest Property'");

        $claims = $this->table('claims');
        $claims
            ->changeColumn('claims_type', 'enum', array('values' => 'Workers Comp,General Liability / Property of Others,Auto,Your Property,Labor Law,Other'))
            ->save();

    }
}
