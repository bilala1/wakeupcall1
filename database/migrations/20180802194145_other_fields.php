<?php

use Phinx\Migration\AbstractMigration;

class OtherFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('claim_workers_comp');
        $table
            ->addColumn('claim_workers_comp_body_parts_other', 'string', array('null' => true))
            ->save();

        $table = $this->table('claim_general_liab');
        $table
            ->addColumn('claim_general_liab_loss_area_other', 'string', array('null' => true))
            ->addColumn('claim_general_liab_loss_type_other', 'string', array('null' => true))
            ->save();

        $table = $this->table('claim_auto');
        $table
            ->addColumn('claim_auto_nature_other', 'string', array('null' => true))
            ->save();

        $table = $this->table('claim_property');
        $table
            ->addColumn('claim_property_loss_type_other', 'string', array('null' => true))
            ->save();
    }
}
