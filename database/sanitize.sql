-- Script to reset emails and passwords for development or staging databases.
-- The password is "password"

update members
 set members_email=concat('dummy+', members_id, '@curiousdog.com'),
     members_password = '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
     members_verified_code = '';

update vendors
 set vendors_email=concat('dummy+', vendors_id, '@curiousdog.com')
WHERE vendors_email is not null and vendors_email != '';

update carriers
 set carriers_email=concat('dummy+', carriers_id, '@curiousdog.com')
WHERE carriers_email is not null and carriers_email != '';

update contracts_additional_recipients
 set contracts_additional_recipients_email=concat('dummy+', contracts_additional_recipients_id, '@curiousdog.com');

update certificates
 set certificates_remind_other=concat('dummy+', certificates_id, '@curiousdog.com')
WHERE certificates_remind_other is not null and certificates_remind_other != '';

update certificates
 set certificates_remind_other_expired=concat('dummy+', certificates_id, '@curiousdog.com')
WHERE certificates_remind_other_expired is not null and certificates_remind_other_expired != '';

update entities_items_milestones_additional_recipients
 set entities_items_milestones_additional_recipients_email=concat('dummy+', join_entities_items_milestones_id, '@curiousdog.com')
WHERE entities_items_milestones_additional_recipients_email is not null and entities_items_milestones_additional_recipients_email != '';

update contracts_milestones_additional_recipients
 set contracts_milestones_additional_recipients_email=concat('dummy+', join_contracts_milestones_id, '@curiousdog.com')
WHERE contracts_milestones_additional_recipients_email is not null and contracts_milestones_additional_recipients_email != '';

update claims_x_carriers
 set claims_x_carriers_adjusters_email=concat('dummy+', claims_x_carriers_id, '@curiousdog.com')
WHERE claims_x_carriers_adjusters_email is not null and claims_x_carriers_adjusters_email != '';

update certificates
 set certificates_request_cert_to_email=concat('dummy+', certificates_id, '@curiousdog.com')
WHERE certificates_request_cert_to_email is not null and certificates_request_cert_to_email != '';

update newsletter_subscribers
 set email_address=concat('dummy+', newsletter_subscribers_id, '@curiousdog.com')
WHERE email_address is not null and email_address != '';
