#!/usr/bin/env bash

CUR_BRANCH=`git rev-parse --abbrev-ref HEAD`
SOURCE_BRANCH=staging
TARGET_BRANCH=staging-deployed
git checkout ${SOURCE_BRANCH}
git pull

RELEASE_ID=`date +%Y-%m-%d-%H-%M-%S`
RELEASE_DIR=release/staging/${RELEASE_ID}

mkdir -p ${RELEASE_DIR}
mkdir -p ${RELEASE_DIR}/backup
mkdir -p ${RELEASE_DIR}/deploy

echo 'Changes:' > ${RELEASE_DIR}/CHANGELOG
git log --oneline --no-merges ${TARGET_BRANCH}..${SOURCE_BRANCH} >> ${RELEASE_DIR}/CHANGELOG
echo 'Changed files:' >> ${RELEASE_DIR}/CHANGELOG
git diff --name-only ${TARGET_BRANCH} ${SOURCE_BRANCH} >> ${RELEASE_DIR}/CHANGELOG

#################### BACKUP #######################
# Make folders for backup
git diff --name-only ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | grep ^src/webapp/.*/.* | sed -r 's@^src/webapp/(.*)/[^/]*$@'${RELEASE_DIR}'/backup/\1@g' | xargs mkdir -p

# Make FTP commands for backup
git diff --name-only --diff-filter=MRCD ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | sed -r 's@^src/webapp/(.*)$@get /var/www/vhosts/staging.wakeupcall.net/\1 '${RELEASE_DIR}'/backup/\1@g' > ${RELEASE_DIR}/backup.ftp

#################### DEPLOY #######################
# Make folders for deploy
git diff --name-only --diff-filter=MRCA ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | grep ^src/webapp/.*/.* | sed -r 's@^src/webapp/(.*)/[^/]*$@'${RELEASE_DIR}'/deploy/\1@g' | xargs mkdir -p

# Copy files for deploy
git diff --name-only --diff-filter=MRCA ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | sed -r 's@^src/webapp/(.*)$@src/webapp/\1 '${RELEASE_DIR}'/deploy/\1@g' | xargs -n 2 cp

# Make FTP commands for upload
git diff --name-only --diff-filter=MRCA ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | sed -r 's@^src/webapp/(.*)$@put '${RELEASE_DIR}'/deploy/\1 /var/www/vhosts/staging.wakeupcall.net/\1@g' > ${RELEASE_DIR}/upload.ftp

# Make FTP commands for delete
git diff --name-only --diff-filter=D ${TARGET_BRANCH} ${SOURCE_BRANCH} src/webapp | sed -r 's@^src/webapp/(.*)$@rm /var/www/vhosts/staging.wakeupcall.net/\1@g' > ${RELEASE_DIR}/delete.ftp

gedit ${RELEASE_DIR}/CHANGELOG ${RELEASE_DIR}/backup.ftp ${RELEASE_DIR}/upload.ftp ${RELEASE_DIR}/delete.ftp &
#sftp -b ${RELEASE_DIR}/backup.ftp wakeup@216.70.106.180 
sftp -i ~/.ssh/WUCEC2Keypair.pem ec2-user@52.89.231.38

git checkout ${CUR_BRANCH}

echo ''
echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
echo '! If things went well, make sure to merge `'${SOURCE_BRANCH}'` to `'${TARGET_BRANCH}'`!'
echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

