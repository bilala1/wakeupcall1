Create EC2 instance based on Amazon Linux AMI
Add port 80 and 22 (HTTP/SSH) rules
Install LAMP per: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html
Lock down Apache per: https://benchmarks.cisecurity.org/tools2/apache/CIS_Apache_HTTP_Server_Benchmark_v3.0.0.pdf (may need to access via http://benchmarks.cisecurity.org/en-us/?route=downloads.show.single.apache.300)
- comment out DAV modules in /etc/httpd/conf.modules.d/00-dav.conf
- comment out status/info modules in 00-base.conf

-------
Ended up starting over with a base AMI image with just httpd and php70

provision MySQL server - free tier eligible
provision S3 bucket
For keys, go to My Organiztion -> Users -> select a user -> security credentials
load data
ftp app to site
update .env
install composer
composer install dependencies
make sure composer.json vendor directories are correct

see if it runs?

had to change AllowOverride to All in /etc/httpd/conf/httpd.conf to get the rewrite stuff to work? Needs FileInfo

DB won't connect wtf this worked last time
Security group settings for inbound connections to db

Had to allow short tags (<?) in php.ini
Updated PHPExcel to new version
sudo yum install php70-zip to get the zip archive extension needed by phpexcel

Followed same process for regular db + wp db

Did a bunch of virtual host configuration which I think I will check into src/apache - the file goes in /etc/conf.d

had to install modssl, used: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/SSL-on-an-instance.html
sudo yum install -y mod24_ssl

Was able to re-use certs from old server

changed max file upload size in php.ini to 128m
playing with permissions in WP trying to fix images
changed ownership of wp-content/uploads to apache:apache still didn't work
re-uploaded images that didn't work, now they work
Ended up finding more images that didn't work, but also migrated from separate db instance to a different DB in the same RDS instance, and everything started working so... ?
sudo yum install php70-gd for osha report
Changed crons to run at 8 and 20 until I can do something with time zones or stop using cron. crontab is in src/apache
Change group for all site files to apache so uploads and junk work, until we actually move everything to S3: chgrp -R apache .
