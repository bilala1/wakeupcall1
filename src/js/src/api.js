
import {create} from 'apisauce'
import moment from 'moment';
import camelCase from 'camelcase'

// define the api
const server = create({
    baseURL: '/API',
    headers: {'Accept': 'application/json'},
    withCredentials: true
});

function arrayToJs(array, stripPrefix) {
    return array.map(obj => toJs(obj, stripPrefix));
}

function convertKeyName(key, stripPrefix) {
    stripPrefix = stripPrefix && new RegExp('^' + stripPrefix + '_');
    return camelCase(stripPrefix ? key.replace(stripPrefix, '') : key);
}

function toJs(obj, stripPrefix) {
    stripPrefix = stripPrefix && new RegExp('^' + stripPrefix + '_');
    let newObj = {};
    for (let k of Object.keys(obj)) {
        let newKey = camelCase(stripPrefix ? k.replace(stripPrefix, '') : k);
        newObj[newKey] = obj[k];
    }
    return newObj;
}

function processDates(objects, dateProps) {
    objects.forEach(obj => {
        dateProps.forEach(prop => {
            if (obj[prop]) {
                if (obj[prop] != '0000-00-00') {
                    obj[prop] = moment(obj[prop]);
                } else {
                    obj[prop] = null;
                }
            }
        });
    });
}

let certificates, certificatesPromise,
    claims, claimsPromise,
    contracts, contractsPromise,
    preferences, preferencesPromise,
    locations, locationsPromise,
    permissions, permissionsPromise,
    businesses, businessesPromise;

export let getCertificates = function getCertificates(force) {
    if (certificates && !force) {
        return Promise.resolve(certificates);
    }
    if(!certificatesPromise) {
        certificatesPromise = server.get('certificates.php').then(data => {
            certificatesPromise = null;
            certificates = arrayToJs(data.data, 'certificates');
            processDates(certificates, ['expire']);
            return certificates;
        });
    }
    return certificatesPromise;
}

export let getClaims = function getClaims(force) {
    if (claims && !force) {
        return Promise.resolve(claims);
    }
    if(!claimsPromise) {
        claimsPromise = server.get('claims.php').then(data => {
            claimsPromise = null;
            claims = arrayToJs(data.data, 'claims');
            processDates(claims, ['datetime', 'datetimeNotified', 'datetimeSubmitted']);
            return claims;
        });
    }
    return claimsPromise;
}

export let getContracts = function getContracts(force) {
    if (contracts && !force) {
        return Promise.resolve(contracts);
    }
    if(!contractsPromise) {
        contractsPromise = server.get('contracts.php').then(data => {
            contractsPromise = null;
            contracts = arrayToJs(data.data, 'contracts');
            processDates(contracts, ['expirationDate']);
            contracts.forEach(contract => {
                if(contract.licensedLocationsIds) {
                    var ids = contract.licensedLocationsIds.split(',');
                    ids = ids.map(id => parseInt(id, 10));
                    contract.licensedLocationsIds = ids;
                }
            });
            return contracts;
        });
    }
    return contractsPromise;
}

export let getBusinesses = function getBusinesses(force) {
    if (businesses && !force) {
        return Promise.resolve(businesses);
    }
    if(!businessesPromise) {
        businessesPromise = server.get('entities/businesses.php').then(data => {
            businessesPromise = null;
            businesses = arrayToJs(data.data, 'business_entities');
            businesses.forEach(business => {
                business.trackedItems = arrayToJs(business.trackedItems, 'entities_items');
                processDates(business.trackedItems, ['effectiveDate']);
                business.trackedItems.forEach(item => {
                    item.milestones = arrayToJs(item.milestones, 'entities_items_milestones');
                    processDates(item.milestones, ['date']);
                })
            })
            processDates(businesses, ['datetime', 'datetimeNotified', 'datetimeSubmitted']);
            return businesses;
        });
    }
    return businessesPromise;
}

export let getPreferences = function getPreferences(force) {
    if (preferences && !force) {
        return Promise.resolve(preferences);
    }
    if(!preferencesPromise) {
        preferencesPromise = server.get('userPrefs.php').then(convertPrefsFromServer).then(prefs => {
            preferencesPromise = null;
            return prefs;
        });
    }
    return preferencesPromise;
}

export let setPreference = function setPreference(key, value) {
    //update local copy for immediate UI feedback, then post to server and reload prefs from the response
    preferences[convertKeyName(key, 'user_prefs')] = value;
    value = convertPrefForServer(key, value);
    return server.post('userPrefs.php', { key, value }).then(convertPrefsFromServer);
}

let convertPrefsFromServer = data => {
    preferences = toJs(data.data, 'user_prefs');
    var dashboardLocationFilter = preferences.dashboardLocationFilter;
    if(!dashboardLocationFilter) {
        preferences.dashboardLocationFilter = null;
    } else {
        preferences.dashboardLocationFilter = dashboardLocationFilter.split(',').map(num => parseInt(num, 10));
    }
    return preferences;
}

let convertPrefForServer = (key, value) => {
    switch(key) {
        case 'user_prefs_dashboard_location_filter':
            return value ? value.join(',') : '';

        default:
            return value;
    }
}

export let getLocations = function getLocations(force) {
    if (locations && !force) {
        return Promise.resolve(locations);
    }
    if(!locationsPromise) {
        locationsPromise = server.get('locations.php').then(data => {
            locationsPromise = null;
            locations = arrayToJs(data.data, 'licensed_locations');
            return locations;
        });
    }
    return locationsPromise;
}

export let getPermissions = function getPermissions(force) {
    if (permissions && !force) {
        return Promise.resolve(permissions);
    }
    if(!permissionsPromise) {
        permissionsPromise = server.get('permissions.php').then(data => {
            permissionsPromise = null;
            permissions = data.data;
            return permissions;
        });
    }
    return permissionsPromise;
}

const api = {getCertificates, getClaims, getContracts, getBusinesses, getPreferences, setPreference, getLocations, getPermissions};

export default api;