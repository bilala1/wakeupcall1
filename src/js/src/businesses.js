import React from 'react';
import moment from 'moment';
import { Bar } from 'react-chartjs';

import Loading from './loading';
import Popup from './popup';
import api from './api';
import styles from './styles';

export default class Businesses extends React.Component {
    constructor() {
        super();
        this.state = {};
        api.getBusinesses().then(data => this.setState({businesses: data}));
        this.setExpiringSoon = this.setExpiringSoon.bind(this);
    }

    componentDidMount() {
        var root = this.refs.root,
            style = window.getComputedStyle(root);

        var width = root.clientWidth - parseInt(style.paddingLeft) - parseInt(style.paddingRight);
        this.setState({innerWidth: width});
    }

    setExpiringSoon(event) {
        let newValue = event.target.value;
        try {
            newValue = parseInt(newValue);
            this.props.setPreference('user_prefs_business_entities_milestones_lead_days', newValue);
        } catch (e) {
            // ?
        }
    }

    render() {
        let loading = !(this.state.businesses && this.props.preferences);
        return (
            <div className="businesses card" ref="root">
                {loading || <Popup key="popup">
                    <h3 key="heading">Business Tracking Options</h3>
                    <label key="label">Threshold for upcoming milestones</label>
                    <select key="select" onChange={this.setExpiringSoon}
                            value={this.props.preferences.businessEntitiesMilestonesLeadDays}>
                        {
                            [30, 60, 90].map(days => <option value={days} key={days}>{days} days</option>)
                        }
                    </select>
                </Popup>}
                <h2 key="heading">Business Tracking</h2>
                {loading ?
                    <Loading key="loading"/> : this.renderBusinesses(this.state.businesses, this.props.preferences.businessEntitiesMilestonesLeadDays || 30)}
            </div>
        );
    }

    renderBusinesses(businesses, milestoneLeadDays) {
        businesses = businesses.filter(business => business.status != 'Inactive');

        if (this.props.preferences.dashboardLocationFilter) {
            businesses = businesses.filter(business => this.props.preferences.dashboardLocationFilter.indexOf(business.joinLicensedLocationsId) > -1);
        }

        let milestones = [];
        businesses.forEach(business =>
        business.trackedItems && business.trackedItems.forEach(trackedItem =>
        trackedItem.milestones && trackedItem.milestones.forEach(milestone => milestones.push({
            milestone,
            trackedItem,
            business
        }))));

        let today = moment().startOf('day');
        let upcomingMoment = moment().add('day', milestoneLeadDays).startOf('day');

        milestones = milestones.filter(milestone => !today.isAfter(milestone.milestone.date) && upcomingMoment.isAfter(milestone.milestone.date));
        let notices = this.getNotices(milestones, milestoneLeadDays);

        return (
            <div>
                {notices}
            </div>
        );
    }

    getNotices(milestones, milestoneLeadDays) {
        let notices = [];
        let today = moment().startOf('day');
        milestones.sort((a, b) => a.milestone.date - b.milestone.date);
        milestones.forEach(milestone =>
            notices.push(
                <div className="blurb" key={"notice-"+milestone.id}>
                    Milestone "{milestone.milestone.description}" for tracked item "{milestone.trackedItem.name}" for business {milestone.business.entitiesName} is
                    coming up in {milestone.milestone.date.diff(today, 'day')} days.
                    <a href={`/members/entities/items/edit.php?entities_id=${milestone.business.entitiesId}&entities_items_id=${milestone.trackedItem.id}`}>Go</a>
                </div>)
        );
        let noticeLength = notices.length;
        if (noticeLength > 7) {
            notices.splice(6);
        }
        if(noticeLength == 0) {
            notices.push(
                <div key="notice-none">No milestones in the next {milestoneLeadDays} days.</div>
            );
        }
        return notices;
    }
}

/*
 [{"entities_id":1,
 "join_accounts_id":85,
 "entities_name":"Burnett",
 "entities_hidden":0,
 "business_entities_id":1,
 "join_entities_id":1,
 "join_licensed_locations_id":72,
 "licensed_locations_id":72,
 "join_members_id":98,
 "licensed_locations_name":"Burnett",
 "licensed_locations_address":"1041 Market Street",
 "licensed_locations_city":"San Diego",
 "licensed_locations_state":"CA",
 "licensed_locations_zip":"92101",
 "licensed_locations_active":1,
 "join_franchises_id":0,
 "licensed_locations_phone":"(619) 940-6604",
 "licensed_locations_fax":"",
 "tracked_items":[{"entities_items_id":1,
 "join_entities_id":1,
 "entities_items_name":"Tracked item",
 "entities_items_details":"oh man these deeeeeeeets wasssup west syde oh man these deeeeeeeets wasssup west syde oh man these deeeeeeeets wasssup west syde oh man these deeeeeeeets wasssup west syde oh man these deeeeeeeets wasssup west syde oh man these deeeeeeeets wasssup west sy",
 "entities_items_effective_date":"2016-12-01",
 "milestones":[{"entities_items_milestones_id":5,
 "join_entities_items_id":1,
 "entities_items_milestones_date":"2017-01-31",
 "entities_items_milestones_description":"Hey"},
 {"entities_items_milestones_id":6,
 "join_entities_items_id":1,
 "entities_items_milestones_date":"2017-02-09",
 "entities_items_milestones_description":"Ho"}]},
 {"entities_items_id":2,
 "join_entities_id":1,
 "entities_items_name":"Another thing",
 "entities_items_details":"Wow man this just is really cool",
 "entities_items_effective_date":"2016-12-01",
 "milestones":[]}]}]
 */
