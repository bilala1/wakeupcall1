import React from 'react';
import moment from 'moment';
import { Bar } from 'react-chartjs';

import Loading from './loading';
import Popup from './popup';
import api from './api';
import styles from './styles';

export default class Certificates extends React.Component {
    constructor() {
        super();
        this.state = {};
        api.getCertificates().then(data => this.setState({certificates: data}));
        this.setExpiringSoon = this.setExpiringSoon.bind(this);
    }

    componentDidMount() {
        var root = this.refs.root,
            style = window.getComputedStyle(root);

        var width = root.clientWidth - parseInt(style.paddingLeft) - parseInt(style.paddingRight);
        this.setState({innerWidth: width});
    }

    setExpiringSoon(event) {
        let newValue = event.target.value;
        try {
            newValue = parseInt(newValue);
            this.props.setPreference('user_prefs_cert_expire_lead_days', newValue);
        } catch (e) {
            // ?
        }
    }

    render() {
        let loading = !(this.state && this.state.certificates && this.props.preferences);
        return (
            <div className="certificates card" ref="root">
                {loading || <Popup key="popup">
                    <h3 key="heading">Certificate Options</h3>
                    <label key="label">Threshold for pending expiration</label>
                    <select key="select" onChange={this.setExpiringSoon} value={this.props.preferences.certExpireLeadDays}>
                        {
                            [7, 14, 30].map(days => <option value={days} key={days}>{days} days</option>)
                        }
                    </select>
                </Popup>}
                <h2 key="heading">Certificates of Insurance</h2>
                {loading ? <Loading key="loading"/> : this.renderCerts(this.state.certificates, this.props.preferences.certExpireLeadDays)}
            </div>
        );
    }

    renderCerts(certs, expireLeadDays) {
        if(this.props.preferences.dashboardLocationFilter) {
            certs = certs.filter(cert => this.props.preferences.dashboardLocationFilter && this.props.preferences.dashboardLocationFilter.indexOf(cert.joinLicensedLocationsId) > -1);
        }

        let today = moment().startOf('day');
        let expiringMoment = moment().add('day', expireLeadDays).startOf('day');
        let numCerts = certs.length;
        let expired = certs.filter(cert => cert.expire && today.isAfter(cert.expire));
        let expiring = certs.filter(cert => cert.expire && !today.isAfter(cert.expire) && expiringMoment.isAfter(cert.expire));
        let notExpiring = certs.filter(cert => !cert.expire || !expiringMoment.isAfter(cert.expire));
        let notices = this.getNotices(expired, expiring);
        let chartData = {
                labels: ['More than ' + expireLeadDays + ' days', expireLeadDays + ' days or less', 'Expired'],
                datasets: [
                    {
                        backgroundColor: [styles.chartColors.normal, styles.chartColors.warning, styles.chartColors.bad],
                        borderColor: [styles.chartColors.normalBorder, styles.chartColors.warningBorder, styles.chartColors.badBorder],
                        borderWidth: 1,
                        label: 'Certificates',
                        data: [notExpiring.length, expiring.length, expired.length]
                    }
                ]
            },
            chartOptions = {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fixedStepSize: 1
                        }
                    }]
                }
            };

        return (
            <div>
                {notices}
                <h3 key="label">By Expiration Date</h3>
                <Bar key="chart" data={chartData} options={chartOptions} width={this.state.innerWidth} height={300}/>
                <a key="view" href="/members/my-documents/certificates/index.php" className="jump">View Certificates</a>
            </div>
        );
    }

    getNotices(expiredCerts, expiringCerts) {
        let notices = [];
        let today = moment().startOf('day');
        expiredCerts.sort((a, b) => a.expire - b.expire);
        expiredCerts.forEach(cert => {
            notices.push(
                <div className="blurb error" key={"notice-"+cert.id}>
                    The certificate for vendor "{cert.vendorsName}" expired {cert.expire.from(today)}
                    <a href={`/members/my-documents/certificates/edit.php?certificates_id=${cert.id}`}>Go</a>
                </div>);
        });
        let noticeLength = notices.length;
        if (noticeLength > 4) {
            notices.splice(3);
            notices.push(
                <div className="blurb error" key="notice-extra">... and {noticeLength - 3} more expired certificates
                    <a href="/members/my-documents/certificates/index.php">Go</a>
                </div>);
        }
        return notices;
    }
}

/*
 certificates_coverages: 'Property (Prop)'
 certificates_email_days: 0
 certificates_expire: '2016-04-30'
 certificates_file: '56f988cfb6d52.doc'
 certificates_filename: 'Corporate Bulletin error.doc'
 certificates_files_active: 1
 certificates_files_file: '56f988cfb6d52.doc'
 certificates_files_filename: 'Corporate Bulletin error.doc'
 certificates_files_id: 345
 certificates_hidden: 0
 certificates_id: 516
 certificates_location_can_edit: 0
 certificates_location_can_view: 0
 certificates_name: ''
 certificates_project_name: ''
 certificates_remind_admin_expired: 0
 certificates_remind_corporate_admin: 0
 certificates_remind_location_expired: 0
 certificates_remind_member: 1
 certificates_remind_member_date_last: '2016-06-06'
 certificates_remind_other: ''
 certificates_remind_other_expired: ''
 certificates_remind_text: ''
 certificates_remind_vendor: 1
 certificates_remind_vendor_expired: 1
 certificates_request_cert_to_address: 1
 certificates_request_cert_to_fax: 1
 certificates_send_copy: 0
 certificates_type: 'regular'
 join_accounts_id: 514
 join_certificates_id: 516
 join_licensed_locations_id: 745
 join_vendors_id: 693
 licensed_locations_name: 'Amazing'
 vendors_all_locations: 1
 vendors_contact_name: 'Kristina'
 vendors_email: 'cmarastigeorg+693@curiousdog.com'
 vendors_hidden: 0
 vendors_id: 693
 vendors_location_can_edit: 0
 vendors_location_can_view: 0
 vendors_name: 'Luke'
 vendors_phone: ''
 vendors_services: ''
 vendors_street: ''
 */
