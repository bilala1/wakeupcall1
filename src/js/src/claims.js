import React from 'react';
import moment from 'moment';
import { Bar } from 'react-chartjs';

import Loading from './loading';
import Popup from './popup';
import api from './api';
import styles from './styles';

export default class Claims extends React.Component {
    constructor() {
        super();
        this.state = {};
        api.getClaims().then(data => this.setState({claims: data}));
        this.setRecentIncident = this.setRecentIncident.bind(this);
    }

    componentDidMount() {
        var root = this.refs.root,
            style = window.getComputedStyle(root);

        var width = root.clientWidth - parseInt(style.paddingLeft) - parseInt(style.paddingRight);
        this.setState({innerWidth: width});
    }

    fixChartLegend() {
        const legend = this;
        legend.lineWidths = legend.lineWidths.map(x => legend.width);
    }

    componentDidUpdate() {
        const chart = this.refs.chart && this.refs.chart.getChart();
        if(chart) {
            chart.legend.afterFit = this.fixChartLegend;
            this.fixChartLegend.call(chart.legend);
        }
    }

    setRecentIncident(event) {
        let newValue = event.target.value;
        try {
            newValue = parseInt(newValue);
            this.props.setPreference('user_prefs_claims_recent_incident_days', newValue);
        } catch (e) {
            // ?
        }
    }

    render() {
        let loading = !(this.state && this.state.claims && this.props.preferences);
        return (
            <div className="claims card" ref="root">
                {loading || <Popup key="popup">
                    <h3 key="heading">Claim Options</h3>
                    <label key={"label"}>Threshold for recent incidents</label>
                    <select key="select" onChange={this.setRecentIncident} value={this.props.preferences.claimsRecentIncidentDays}>
                        {
                            [7, 14, 30, 90].map(days => <option value={days} key={days}>{days} days</option>)
                        }
                    </select>
                </Popup>}
                <h2 key="heading">Claims</h2>
                {loading ? <Loading key="loading"/> : this.renderClaims(this.state.claims, this.props.preferences.claimsRecentIncidentDays)}
            </div>
        );
    }

    renderClaims(claims, recentIncidentDays) {
        claims = claims.filter(claim => claim.status != 'Inactive');

        if(this.props.preferences.dashboardLocationFilter) {
            claims = claims.filter(claim => this.props.preferences.dashboardLocationFilter.indexOf(claim.joinLicensedLocationsId) > -1);
        }

        let today = moment().startOf('day');
        let recentMoment = moment().subtract('day', recentIncidentDays).startOf('day');

        let notices = this.getNotices(claims);

        let labelsByStatus = [
            'Submitted - Open',
            'Record Only - Not submitted',
            'First Aid Only - Not submitted'
        ];
        let countsByStatus = labelsByStatus.map(label => claims.reduce((count, claim) => count + (claim.statusCodeDescription == label ? 1 : 0), 0));
        //Update labels to be more user friendly
        labelsByStatus = [
            'Open',
            'Record Only',
            'First Aid Only'
        ]

        let chartByStatusData = {
                labels: labelsByStatus,
                datasets: [
                    {
                        backgroundColor: styles.chartColors.normal,
                        borderColor: styles.chartColors.normalBorder,
                        borderWidth: 1,
                        label: 'Claims',
                        data: countsByStatus
                    }
                ]
            },
            chartByStatusOptions = {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fixedStepSize: 1
                        }
                    }]
                }};

        let labelsByType = [];
        if(this.props.permissions.indexOf('workers_comp') > -1) {
            labelsByType.push('Workers Comp');
        }
        labelsByType.push('Your Property');
        labelsByType.push('Auto');
        labelsByType.push('General Liability / Guest Property');
        if(this.props.permissions.indexOf('labor_claims') > -1) {
            labelsByType.push('Labor Law');
        }
        if(this.props.permissions.indexOf('other_claims') > -1) {
            labelsByType.push('Other');
        }
        let openByType = labelsByType.map(label => claims.reduce((count, claim) => {
            if (claim.type == label &&
                claim.statusCodeDescription == 'Submitted - Open') {
                ++count;
            }
            return count;
        }, 0));
        let recentByType = labelsByType.map(label => claims.reduce((count, claim) => {
            if (claim.type == label &&
                claim.statusCodeDescription != 'Submitted - Closed' &&
                claim.statusCodeDescription != 'Submitted - Open' &&
                claim.datetimeNotified &&
                claim.datetimeNotified.isAfter(recentMoment)) {
                ++count;
            }
            return count;
        }, 0));
        let submittedByType = labelsByType.map(label => claims.reduce((count, claim) => {
            if (claim.type == label &&
                claim.statusCodeDescription == 'Submitted - Open' &&
                claim.datetimeSubmitted &&
                claim.datetimeSubmitted.isAfter(recentMoment)) {
                ++count;
            }
            return count;
        }, 0));
        labelsByType = labelsByType.map(label => {
            switch(label) {
                case 'General Liability / Guest Property':
                    return 'General Liability';
                default:
                    return label;
            }
        });

        let chartByTypeData = {
                labels: labelsByType,
                datasets: [
                    {
                        backgroundColor: styles.chartColors.warning,
                        borderColor: styles.chartColors.warningBorder,
                        borderWidth: 1,
                        label: 'Open Claims',
                        data: openByType
                    },
                    {
                        backgroundColor: styles.chartColors.good,
                        borderColor: styles.chartColors.goodBorder,
                        borderWidth: 1,
                        label: 'Submitted last ' + recentIncidentDays + ' days',
                        data: submittedByType
                    },
                    {
                        backgroundColor: styles.chartColors.normal,
                        borderColor: styles.chartColors.normalBorder,
                        borderWidth: 1,
                        label: 'Incidents last ' + recentIncidentDays + ' days based on date notified',
                        data: recentByType
                    }
                ]
            },
            chartByTypeOptions = {
                legend: {
                    labels: {
                        boxWidth: 45
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fixedStepSize: 1
                        }
                    }]
                }
            };

        return (
            <div key="body">
                {notices}
                <h3 key={'label1'}>Open and Last {recentIncidentDays} Days</h3>
                <Bar key={'chart1'} data={chartByTypeData} options={chartByTypeOptions} width={this.state.innerWidth} height={350} ref="chart"/>
                <h3 key={'label2'}>Total of All Lines of Coverage</h3>
                <Bar key={'chart2'} data={chartByStatusData} options={chartByStatusOptions} width={this.state.innerWidth}
                     height={250}/>
                <a key={'view'} href="/members/claims/index.php" className="jump">View Claims</a>
            </div>
        );
    }

    getNotices(claims) {
        let notices = [],
            failedClaims = claims.filter(claim => claims.status == 'Submission Failed');

        failedClaims.forEach(claim => {
            notices.push(
                <div className="blurb error" key={"notice-"+claim.id}>
                    A claim submission has failed
                    <a href="/members/claims/edit.php?claims_id={claim.id}">Go</a>
                </div>);
        });
        if (notices.length > 3) {
            notices.splice(3);
        }
        return notices;
    }
}
/*
 'Record Only - Not submitted'
 'Submitted - Open'
 'Submitted - Closed'
 'First Aid Only - Not submitted'

 'Your Property'
 'Workers Comp'
 'Auto'
 'General Liability / Guest Property'
 'Other'

 */
/*
 Number of open claims

 b. Number of incidents in the last time period (week, month,6 months, year)submitted or

 not

 c. Number of claims submitted in last time period

 d. Claims closed in the last month

 e. Number of claims per type ( WC, GL, Auto, Property, other)
 */
/*
 carriers_address: ""
 carriers_all_locations: 0
 carriers_all_receive: 0
 carriers_contact: ""
 carriers_coverages_list: "Broker"
 carriers_coverages_other: ""
 carriers_effective_end_date: "2016-03-31 00:00:00"
 carriers_effective_start_date: "2016-03-01 00:00:00"
 carriers_email: "cmarastigeorg+283@curiousdog.com"
 carriers_fax: "(785) 000-0000"
 carriers_hidden: 0
 carriers_id: 283
 carriers_name: "Aleksandr"
 carriers_phone: ""
 carriers_policy_number: "D1258"
 carriers_recipient: 0
 carriers_recipient_all: 0
 claim_workers_comp_body_parts: "Ankle"
 claim_workers_comp_classification: ""
 claim_workers_comp_classification_G: 0
 claim_workers_comp_classification_H: 1
 claim_workers_comp_classification_I: 0
 claim_workers_comp_classification_J: 0
 claim_workers_comp_datetime_away_start: "2016-03-01 00:00:00"
 claim_workers_comp_employee_department: "Bar"
 claim_workers_comp_employee_name: "ghj 1"
 claim_workers_comp_event_department: "Bar"
 claim_workers_comp_event_type: "Injury"
 claim_workers_comp_id: 500
 claim_workers_comp_illness_type_other: ""
 claim_workers_comp_injury_type: "Came in contact with"
 claim_workers_comp_injury_type_other: ""
 claim_workers_comp_job_title: "guard"
 claim_workers_comp_medical_costs: "0.00"
 claim_workers_comp_osha_include: 1
 claim_workers_comp_osha_privacy: 0
 claims_amount_paid: "0.00"
 claims_amount_reserved: "0.00"
 claims_claim_number: ""
 claims_datetime: "2016-03-01 00:00:00"
 claims_datetime_notified: "2016-03-10 00:00:00"
 claims_datetime_submitted: "2016-03-28 00:00:00"
 claims_description: ""
 claims_filed: 1
 claims_hidden: 0
 claims_id: 1009
 claims_status: "Record Only - Not submitted"
 claims_status_code_description: "Submitted - Open"
 claims_status_codes_id: "SUBMIT_OPEN"
 claims_submit_type: "fax"
 claims_type: "Workers Comp"
 join_accounts_id: 514
 join_carriers_id: 283
 join_claims_status_codes_id: "SUBMIT_OPEN"
 join_franchises_id: 0
 join_licensed_locations_id: 745
 join_members_id: 676
 licensed_locations_active: 1
 licensed_locations_address: "10tika"
 licensed_locations_city: "Togliatti"
 licensed_locations_fax: ""
 licensed_locations_id: 745
 licensed_locations_name: "Amazing"
 licensed_locations_phone: ""
 licensed_locations_state: "FL"
 licensed_locations_zip: "12578"
 */
