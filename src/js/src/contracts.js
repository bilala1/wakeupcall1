import React from 'react';
import moment from 'moment';
import { Bar } from 'react-chartjs';

import Loading from './loading';
import Popup from './popup';
import api from './api';
import styles from './styles';

export default class Contracts extends React.Component {
    constructor() {
        super();
        this.state = {};
        api.getContracts().then(data => this.setState({contracts: data}));
        this.setExpiringSoon = this.setExpiringSoon.bind(this);
    }

    componentDidMount() {
        var root = this.refs.root,
            style = window.getComputedStyle(root);

        var width = root.clientWidth - parseInt(style.paddingLeft) - parseInt(style.paddingRight);
        this.setState({innerWidth: width});
    }

    setExpiringSoon(event) {
        let newValue = event.target.value;
        try {
            newValue = parseInt(newValue);
            this.props.setPreference('user_prefs_contract_expire_lead_days', newValue);
        } catch (e) {
            // ?
        }
    }

    render() {
        let loading = !(this.state.contracts && this.props.preferences);
        return (
            <div className="contracts card" ref="root">
                {loading || <Popup key="popup">
                    <h3 key="heading">Contract Options</h3>
                    <label key="label">Threshold for pending expiration</label>
                    <select key="select" onChange={this.setExpiringSoon} value={this.props.preferences.contractExpireLeadDays}>
                        {
                            [30, 60, 90].map(days => <option value={days} key={days}>{days} days</option>)
                        }
                    </select>
                </Popup>}
                <h2 key="heading">Contract Management</h2>
                {loading ? <Loading key="loading"/> : this.renderContracts(this.state.contracts, this.props.preferences.contractExpireLeadDays)}
            </div>
        );
    }

    renderContracts(contracts, expireLeadDays) {
        contracts = contracts.filter(contract => contract.status != 'Inactive');

        if(this.props.preferences.dashboardLocationFilter) {
            contracts = contracts.filter(contract => !contract.licensedLocationsIds || _.intersection(contract.licensedLocationsIds, this.props.preferences.dashboardLocationFilter).length > 0);
        }

        let today = moment().startOf('day');
        let expiringMoment = moment().add('day', expireLeadDays).startOf('day');
        let numContracts = contracts.length;
        let expired = contracts.filter(contract => contract.status == 'Expired');
        let expiring = contracts.filter(contract => contract.expirationDate && expiringMoment.isAfter(contract.expirationDate) && contract.status == 'Active');
        let notExpiring = contracts.filter(contract => (!contract.expirationDate || !expiringMoment.isAfter(contract.expirationDate)) && contract.status == 'Active');
        let notices = this.getNotices(expired, expiring);

        let chartByStatusData = {
                labels: ['More than ' + expireLeadDays + ' days', expireLeadDays + ' days or less', 'Expired'],
                datasets: [
                    {
                        backgroundColor: [styles.chartColors.normal, styles.chartColors.warning, styles.chartColors.bad],
                        borderColor: [styles.chartColors.normalBorder, styles.chartColors.warningBorder, styles.chartColors.badBorder],
                        borderWidth: 1,
                        label: 'Contracts',
                        data: [notExpiring.length, expiring.length, expired.length]
                    }
                ]
            },
            chartByStatusOptions = {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fixedStepSize: 1
                        }
                    }]
                }
            };

        let labelsByType = [
            'Active',
            'Autorenew-Annual',
            'Month to Month',
            'Expired'
        ];
        let countsByType = labelsByType.map(label => contracts.reduce((count, contract) => count + (contract.status == label ? 1 : 0), 0));
        labelsByType[1] = 'Auto Renew - Annual';
        let chartByTypeData = {
                labels: labelsByType,
                datasets: [
                    {
                        backgroundColor: [styles.chartColors.good, styles.chartColors.good, styles.chartColors.normal, styles.chartColors.bad],
                        borderColor: [styles.chartColors.goodBorder, styles.chartColors.goodBorder, styles.chartColors.normalBorder, styles.chartColors.badBorder],
                        borderWidth: 1,
                        label: 'Contracts',
                        data: countsByType
                    }
                ]
            },
            chartByTypeOptions = {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fixedStepSize: 1
                        }
                    }]
                }};

        return (
            <div>
                {notices}
                <h3 key="label1">By Expiration Date</h3>
                <Bar key="chart1" data={chartByStatusData} options={chartByStatusOptions} width={this.state.innerWidth} height={300}/>
                <h3 key="label2">By Status</h3>
                <Bar key="chart2" data={chartByTypeData} options={chartByTypeOptions} width={this.state.innerWidth} height={250}/>
                <a key="view" href="/members/contracts/index.php" className="jump">View Contracts</a>
            </div>
        );
    }

    getNotices(expiredContracts, expiringContracts) {
        let notices = [];
        let today = moment().startOf('day');
        expiredContracts.sort((a, b) => a.expirationDate - b.expirationDate);
        expiredContracts.forEach(contract => {
            notices.push(
                <div className="blurb error" key={"notice-"+contract.id}>
                    The contract with party "{contract.contractedPartyName}" expired {contract.expirationDate.from(today)}
                    <a href={`/members/contracts/edit.php?contracts_id=${contract.id}`}>Go</a>
                </div>);
        });
        let noticeLength = notices.length;
        if(noticeLength > 4) {
            notices.splice(3);
            notices.push(
                <div className="blurb error" key="notice-extra">... and {noticeLength - 3} more expired contracts
                    <a href="/members/contracts/index.php">Go</a>
                </div>);
        }
        return notices;
    }
}

/*
 contracts_additional_details: "xxxxx"
 contracts_all_locations: 1
 contracts_contact_email: ""
 contracts_contact_name: ""
 contracts_contact_phone: ""
 contracts_contracted_party_name: "Nick"
 contracts_description: ""
 contracts_effective_date: "2016-03-01 00:00:00"
 contracts_expiration_date: "2016-03-31 00:00:00"
 contracts_expiration_notify_lead_days: 30
 contracts_expired_notification: 0
 contracts_hidden: 0
 contracts_id: 243
 contracts_location_can_edit: 1
 contracts_location_can_view: 1
 contracts_status: "Month to Month"
 contracts_transition_status_to: "Month to Month"
 join_accounts_id: 514
 licensed_locations_count: 0
 licensed_locations_name: "All Locations"
 */
