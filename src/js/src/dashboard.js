require('./dashboard.css');
require('../node_modules/react-grid-layout/css/styles.css');
require('../node_modules/react-resizable/css/styles.css');

import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import reactGridLayout from 'react-grid-layout';

let ReactGridLayout = reactGridLayout.WidthProvider(reactGridLayout);

import Claims from './claims';
import Certificates from './certificates';
import Contracts from './contracts';
import Businesses from './businesses';
import Preferences from './preferences';
import Loading from './loading';
import api from './api';
import DimensionHandler from './dimensionHandler';

function getlayout(cards = [{key: 'loading'}]) {
    let w;
    switch (cards.length) {
        case 0:
        case 1:
            w = 6;
            break;
        case 2:
            w = 3;
            break;
        default:
            w = 2;
            break;
    }
    let index = 0, numCols = 6 / w;
    return cards.map((card, i) => {
        return {
            x: (index++ % numCols) * w,
            y: Math.floor(index / numCols),
            w: w,
            h: 8,
            i: card.key,
            static: false,
            useCSSTransforms: false
        };
    });
}

const margin = 10,
    rowHeight = 1;

export class Dashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            layout: getlayout()
        };
        api.getPreferences().then(preferences => this.setState({preferences}));
        api.getPermissions().then(permissions => {
            this.setState({
                permissions: permissions,
                layout: getlayout(this.getCards(permissions))
            });
        });
        this.setPreference = this.setPreference.bind(this);
        this.onLayoutChange = this.onLayoutChange.bind(this);
    }

    setPreference(name, value) {
        //hack in a special case where we don't actually want to save an empty location filter but we
        //do want to update the UI. Sorry :(
        if (name == 'user_prefs_dashboard_location_filter' && value && value.length == 0) {
            var preferences = api.getPreferences(false).then(preferences => {
                preferences.dashboardLocationFilter = [];
                this.setState({preferences});
            });
            return;
        }

        // set the pref immediately, but also update our state when the new prefs are loaded
        api.setPreference(name, value).then(preferences => this.setState({preferences}));
        api.getPreferences().then(preferences => this.setState({preferences}));
    }

    onLayoutChange(newLayout) {
        console.log('onLayoutChange: '+JSON.stringify(newLayout));
        //TODO save this
    }

    onHeightChanged(key, newHeight) {
        const newGridHeight = Math.ceil((newHeight + margin) / (rowHeight + margin));
        if (this.heightStateChange) {
            this.widgetHeightMap[key] = newGridHeight;
        } else {
            this.heightStateChange = true;
            this.widgetHeightMap = {};
            this.widgetHeightMap[key] = newGridHeight;
            setTimeout(() => {
                this.heightStateChange = false;
                let layout = this.state.layout.map(item => {
                    item = _.cloneDeep(item);
                    if(this.widgetHeightMap[item.i]) {
                        item.h = this.widgetHeightMap[item.i];
                    }
                    return item;
                });
                this.setState({layout});
            }, 1000);
        }
    }

    getCards(permissions = this.state.permissions) {
        if (!permissions) {
            return [
                <div key="loading">
                    <DimensionHandler onHeightChanged={this.onHeightChanged.bind(this, 'loading')}>
                        <Loading/>
                    </DimensionHandler>
                </div>
            ];
        }

        let cards = permissions.reduce((arr, perm) => {
            switch (perm) {
                case 'certificates':
                    arr.push(<div key="certs">
                        <DimensionHandler onHeightChanged={this.onHeightChanged.bind(this, 'certs')}>
                            <Certificates key="certs" preferences={this.state.preferences}
                                          setPreference={this.setPreference}/>
                        </DimensionHandler>
                    </div>);
                    break;

                case 'claims':
                    arr.push(<div key="claims">
                        <DimensionHandler key="claims" onHeightChanged={this.onHeightChanged.bind(this, 'claims')}>
                            <Claims key="claims" permissions={permissions} preferences={this.state.preferences}
                                    setPreference={this.setPreference}/>
                        </DimensionHandler>
                    </div>);
                    break;

                case 'contracts':
                    arr.push(<div key="contracts">
                        <DimensionHandler key="contracts"
                                          onHeightChanged={this.onHeightChanged.bind(this, 'contracts')}>
                            <Contracts key="contracts" preferences={this.state.preferences}
                                       setPreference={this.setPreference}/>
                        </DimensionHandler>
                    </div>);
                    break;

                case 'business_entities':
                    arr.push(<div key="businesses">
                        <DimensionHandler key="businesses"
                                          onHeightChanged={this.onHeightChanged.bind(this, 'businesses')}>
                            <Businesses key="businesses" preferences={this.state.preferences}
                                        setPreference={this.setPreference}/>
                        </DimensionHandler>
                    </div>);
                    break;
            }
            return arr;
        }, []);

        return cards;
    }

    render() {
        let cards = this.getCards(),
            numCards = cards.length;
        return (
            <div className="dashboardRoot">
                <Preferences key="prefs" preferences={this.state.preferences} setPreference={this.setPreference}/>
                <h1>Dashboard</h1>
                <div className={'dashboard'} key="cardHolder">
                    <ReactGridLayout
                        className="layout"
                        rowHeight={rowHeight}
                        margin={[margin, margin]}
                        cols={6}
                        layout={this.state.layout}
                        onLayoutChange={this.onLayoutChange}
                        isResizable={false}
                        useCSSTransforms={false}
                        // WidthProvider option
                        measureBeforeMount={true}>
                        {cards}
                    </ReactGridLayout>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Dashboard/>, document.querySelector("#content"));
