import React from "react";

import SizeMe, {SizeMeProps} from "react-sizeme";

class DimensionHandleElement extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (nextProps.size.height !== this.props.size.height
            && this.props.onHeightChanged) {
            this.props.onHeightChanged(nextProps.size.height);
        }
        if (nextProps.size.width !== this.props.size.width
            && this.props.onWidthChanged) {
            this.props.onWidthChanged(nextProps.size.width);
        }
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}
const options = { monitorHeight: true };

const Dimension = SizeMe(options)(DimensionHandleElement);

class DimensionHandler extends React.Component {
    render() {
        // The dimensions of the div below is measured by Dimensions, and must be there.
        return (
            <div>
                <Dimension onHeightChanged={this.props.onHeightChanged} onWidthChanged={this.props.onWidthChanged}>
                    {this.props.children}
                </Dimension>
            </div>
        );
    }
}

export default DimensionHandler;