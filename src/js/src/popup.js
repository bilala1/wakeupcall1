import React from 'react';

export default class Popup extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    close() {
        this.setState({open: false});
    }

    open() {
        this.setState({open: true});
    }

    toggle() {
        this.setState({open: !this.state.open});
    }

    render() {
        return (
            <div className={'popup ' + (this.state.open ? 'popup-open ' : '') + (this.props.displayTitleText ? 'with-text ' : '')}>
                <div className="overlay" onClick={this.close}></div>
                <div className="trigger" onClick={this.toggle} title={this.props.title || 'Options'}>{this.props.displayTitleText ? this.props.title : ''}</div>
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
