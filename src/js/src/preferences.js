import React from 'react';
import moment from 'moment';
import { Bar } from 'react-chartjs';
import _ from 'lodash';

import Loading from './loading';
import api from './api';
import Popup from './popup';

export default class Claims extends React.Component {
    constructor() {
        super();
        this.state = {};
        api.getLocations().then(data => this.setState({locations: data}));
        this.setLocationFilter = this.setLocationFilter.bind(this);
    }

    setLocationFilter(event) {
        let newValue = parseInt(event.target.value),
            locationFilter = this.props.preferences.dashboardLocationFilter,
            newFilter = _.clone(locationFilter),
            addedCheck = event.target.checked;

        if(newValue == 0) {
            // If adding a check, clear the filter. If removing, set filter as empty array
            newFilter = addedCheck ? null : [];
        } else if(addedCheck) {
            newFilter.push(newValue);
            if(newFilter.length == this.state.locations.length) {
                newFilter = null;
            }
        } else {
            if(!newFilter) {
                // If all locations, create an array w/all locations so one can be removed.
                newFilter = this.state.locations.map(l => l.id);
            }
            newFilter = _.without(newFilter, newValue);
        }
        if(!_.eq(locationFilter, newFilter)) {
            this.props.setPreference('user_prefs_dashboard_location_filter', newFilter);
        }
    }

    render() {
        let loading = !(this.state && this.state.locations && this.props.preferences);
        return (
            <div className="prefs">
                { loading ? null : this.renderPrefs(this.state.locations) }
            </div>
        );
    }

    renderPrefs(locations) {
        if (locations.length > 1) {
            const locationFilter = this.props.preferences.dashboardLocationFilter,
                allLocations = !locationFilter;
            return [
                <Popup displayTitleText={true} title="Configure">
                    <h3>Location Filter</h3>
                    <label><input type="checkbox"
                                  value={0}
                                  key={0}
                                  onChange={this.setLocationFilter}
                                  checked={allLocations}/>
                        All Locations</label>
                    {
                        locations.map(location =>
                            <label><input type="checkbox"
                                          value={location.id}
                                          key={location.id}
                                          onChange={this.setLocationFilter}
                                          checked={allLocations || locationFilter.indexOf(location.id) > -1}/>
                                {location.name}</label>
                        )
                    }
                </Popup>
            ];
        }
    }
}