import Chart from 'chart.js';

Chart.defaults.global.hover.mode = 'x-axis';
Chart.defaults.bar.hover.mode = 'x-axis';
Chart.defaults.global.tooltips.mode = 'x-axis';
Chart.defaults.bar.scales.yAxes[0].ticks =  {
            beginAtZero: true
        };

const styles = {
    chartColors: {
        normal: 'rgba(0, 93, 170, .95)',
        normalBorder: 'rgba(0, 93, 170, 0)',
        good: 'rgba(64, 170, 64, .95)',
        goodBorder: 'rgba(64, 170, 64, 0)',
        bad: 'rgba(170, 64, 64, .95)',
        badBorder: 'rgba(170, 64, 64, 0)',
        warning: 'rgba(170, 133, 64, .95)',
        warningBorder: 'rgba(170, 133, 64, 0)'
    }
};

export default styles;