<?php


class accounts
{
    static $member = null;
    static $account = null;

    function getMainAdmin($accountId)
    {
        return accounts::$member;
    }

    function get_by_id($accountId)
    {
        return accounts::$account;
    }
}

class licensed_locations
{

    static $locations = null;

    function get_for_account($accountId)
    {
        return licensed_locations::$locations;
    }
}

class DiscountCodes
{

    static $discountCode = null;

    function getByIdIfValid($discountCodesId)
    {
        return DiscountCodes::$discountCode;
    }
}

class BillingTest extends PHPUnit_Framework_TestCase
{
    function BillingTest()
    {
        if (!defined(YEARLY_COST)) {
            define('YEARLY_COST', 3000);
        }
    }

    public function testGetBillingInfoSingleAccount()
    {
        accounts::$member = array(
            'members_billing_type' => 'yearly'
        );
        accounts::$account = array(
            'accounts_type' => 'single'
        );
        licensed_locations::$locations = array();
        DiscountCodes::$discountCode = null;

        $billingInfo = billing::calculateNextBillForAccount(1);
        $this->assertEquals(YEARLY_COST, $billingInfo['bills_amount']);
    }

    public function testGetBillingInfoSingleAccountWithDiscount()
    {
        accounts::$member = array(
            'members_billing_type' => 'yearly'
        );
        accounts::$account = array(
            'accounts_type' => 'single',
            'join_discount_codes_id' => 8
        );
        licensed_locations::$locations = array();
        DiscountCodes::$discountCode = array(
            'discount_codes_id' => 8,
            'discount_codes_amount' => 1000,
            'discount_codes_max_uses' => 0
        );

        $billingInfo = billing::calculateNextBillForAccount(1);
        $this->assertEquals(YEARLY_COST - 1000, $billingInfo['bills_amount']);
        $this->assertEquals(8, $billingInfo['discount_codes_id']);
    }

    public function testGetBillingInfoMultiAccountWith3Locations()
    {
        accounts::$member = array(
            'members_billing_type' => 'yearly'
        );
        accounts::$account = array(
            'accounts_type' => 'multi'
        );
        licensed_locations::$locations = array(
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            )
        );
        DiscountCodes::$discountCode = null;

        $billingInfo = billing::calculateNextBillForAccount(1);
        $this->assertEquals(YEARLY_COST * 2, $billingInfo['bills_amount']);
    }

    public function testGetBillingInfoMultiAccountWith3LocationsWithDiscount()
    {
        accounts::$member = array(
            'members_billing_type' => 'yearly'
        );
        accounts::$account = array(
            'accounts_type' => 'multi',
            'join_discount_codes_id' => 8
        );
        licensed_locations::$locations = array(
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            )
        );
        DiscountCodes::$discountCode = array(
            'discount_codes_id' => 8,
            'discount_codes_amount' => 1000,
            'discount_codes_max_uses' => 0
        );

        $billingInfo = billing::calculateNextBillForAccount(1);
        $this->assertEquals((YEARLY_COST - 1000) * 2, $billingInfo['bills_amount']);
        $this->assertEquals(8, $billingInfo['discount_codes_id']);
    }

    public function testGetBillingInfoMultiAccountWith5LocationsWithPartialDiscount()
    {
        accounts::$member = array(
            'members_billing_type' => 'yearly'
        );
        accounts::$account = array(
            'accounts_type' => 'multi',
            'join_discount_codes_id' => 8
        );
        licensed_locations::$locations = array(
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            ),
            array(
                'licensed_locations_delete_datetime' => null
            )
        );
        DiscountCodes::$discountCode = array(
            'discount_codes_id' => 8,
            'discount_codes_amount' => 1000,
            'discount_codes_max_uses' => 2,
            'discount_codes_uses' => 0
        );

        $billingInfo = billing::calculateNextBillForAccount(1);
        $this->assertEquals((YEARLY_COST - 1000) * 2 + YEARLY_COST * 2, $billingInfo['bills_amount']);
        $this->assertEquals(8, $billingInfo['discount_codes_id']);
    }
}
