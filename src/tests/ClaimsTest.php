<?php

/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 11/15/15
 * Time: 9:27 AM
 */
class ClaimsTest extends PHPUnit_Extensions_Database_TestCase
{

    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;
    /**
     * Returns the test database connection.
     *
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected function getConnection()
    {
        define('DB_USER', $GLOBALS['DB_USER']);
        define('DB_PASSWD', $GLOBALS['DB_PASSWD']);
        define('DB_NAME', $GLOBALS['DB_NAME']);
        define('DB_SERVER', $GLOBALS['DB_SERVER']);
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASSWD);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, DB_NAME);
        }

        return $this->conn;
    }

    /**
     * Returns the test dataset.
     *
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createFlatXmlDataSet('src/tests/fixtures/claimsFixture.xml');
    }

    public function testGetClaim()
    {
        $target = claims::get_claim(1);
        $this->assertEquals(1,$target['claims_id']);
    }
}