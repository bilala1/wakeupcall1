<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 11/9/15
 * Time: 6:37 AM
 */

function autoload_class_multiple_directory($class_name)
{

    # List all the class directories in the array.
    $array_paths = array(
        'src/webapp/models/',
        'src/webapp/library/'
    );

    foreach($array_paths as $path)
    {
        $file = sprintf('%s/%s.php', $path, $class_name);
        if(is_file($file))
        {
            include_once $file;
        }

    }
}
spl_autoload_register(function ($class) {
    autoload_class_multiple_directory($class);
});