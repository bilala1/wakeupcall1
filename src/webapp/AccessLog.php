<?php

class AccessLog
{

    public static $filterUrls = array(
        '/albums/random-image.php',
        '/crons'
    );

    static function RecordCurrentAccess()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $uri = explode('?', $uri);
        $path = $uri[0];
        $query = $_SERVER['QUERY_STRING'];
        $method = $_SERVER['REQUEST_METHOD'];

        foreach(AccessLog::$filterUrls as $filterUrl) {
            if (strpos($path, $filterUrl) === 0) {
                return;
            }
        }

        $access_logs_table = new mysqli_db_table('access_logs');
        $access_logs_table->insert(array(
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'join_accounts_id' => ActiveMemberInfo::GetAccountId(),
            'access_logs_path' => $path,
            'access_logs_query' => $query,
            'access_logs_method' => $method,
            'access_logs_datetime' => SQL('NOW()'),
        ));

        return $access_logs_table->last_id();
    }
}