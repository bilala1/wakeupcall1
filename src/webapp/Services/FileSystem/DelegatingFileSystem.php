<?php

namespace Services\FileSystem;

use Services\Services;

class DelegatingFileSystem implements FileSystem
{
    protected $localFileSystem;
    protected $s3FileSystem;
    protected $delegateFileSystem;

    public function __construct($rootDir)
    {
        $this->localFileSystem = new LocalFileSystem($rootDir);
        $this->s3FileSystem = new S3FileSystem();
        if(FILE_SYSTEM_PROVIDER == 's3') {
            $this->delegateFileSystem = $this->s3FileSystem;
        } else {
            $this->delegateFileSystem = $this->localFileSystem;
        }
    }

    public function migrateLocalFileToS3($file) {
        return $this->s3FileSystem->migrateLocalFileToS3($file);
    }

    public function storeFile($uploadFile, $category)
    {
        return $this->delegateFileSystem->storeFile($uploadFile, $category);
    }

    public function storeFileContents($filename, $contents, $category)
    {
        return $this->delegateFileSystem->storeFileContents($filename, $contents, $category);
    }

    public function getFileDownloadUrl($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        if($file['files_storage_type'] == 's3') {
            return $this->s3FileSystem->getFileDownloadUrl($file);
        } else {
            return $this->localFileSystem->getFileDownloadUrl($file);
        }
    }

    public function getFileContents($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        if($file['files_storage_type'] == 's3') {
            return $this->s3FileSystem->getFileContents($file);
        } else {
            return $this->localFileSystem->getFileContents($file);
        }
    }

    public function deleteFile($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        if($file['files_storage_type'] == 's3') {
            return $this->s3FileSystem->deleteFile($file);
        } else {
            return $this->localFileSystem->deleteFile($file);
        }
    }


}