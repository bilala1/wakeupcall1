<?php
/**
 * Created by PhpStorm.
 * User: jpatula
 * Date: 4/21/16
 * Time: 12:54 PM
 */

namespace Services\FileSystem;


/*
 * The FileSystem interface represents physical storage of files - possibly on a local filesystem, possibly on a remote
 * server, etc.
 *
 * A successful response from a function call indicates that if the database entry exists, the file will also exist.
 * This means that for a creation, the file must be successfully saved before the database entry will be created.
 * For a delete, the database entry must be successfully deleted before the file will be deleted. This could result
 * in a file being stored without a matching database entry, but it will not result in a database entry with no file
 * being stored.
 */
interface FileSystem
{
    /*
     * @param $file An entry from $_FILES
     * @param $category The category of the file
     * @return The database ID of the file, or FALSE in case of failure
     */
    public function storeFile($uploadFile, $category);

    /*
     * @param $filename The filename for the file
     * @param contents The contents of the file
     * @param $category The category of the file
     * @return The database ID of the file, or FALSE in case of failure
     */
    public function storeFileContents($filename, $contents, $category);

    /*
     * @param $fileID The database ID of the file to generate a download link for
     * @return The URL the file can be downloaded from, or FALSE in case of failure
     */
    public function getFileDownloadUrl($fileId);

    /*
     * @param $fileID The database ID of the file to retrieve
     * @return The contents of the file, or FALSE in case of failure
     */
    public function getFileContents($fileId);

    /*
     * @param $fileID The database ID of the file to delete
     * @return TRUE for success, or FALSE in case of failure
     */
    public function deleteFile($fileId);
}