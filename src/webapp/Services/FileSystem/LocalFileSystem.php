<?php

namespace Services\FileSystem;

use Services\Services;

class LocalFileSystem implements FileSystem
{
    protected $rootDir;
    protected $directories = array(
        'albums' => 'albums',
        'certificates' => 'certificates',
        'claims' => 'claims',
        'contracts' => 'contracts',
        'documents' => 'library',
        'webinars' => 'webinars',
        'entities' => 'entities'
    );

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function getFullPath($file)
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . $file['files_location'];
    }

    public function storeFile($uploadFile, $category)
    {
        if (!array_key_exists('name', $uploadFile)) {
            Services::$logger->addError('LocalFileSystem->storeFile(): $uploadFile should be passed through files::fixUploadFileArray() first');
            return false;
        }
        if (!array_key_exists($category, $this->directories)) {
            Services::$logger->addError('LocalFileSystem->storeFile(): Unknown category - where to store?',
                array('category' => $category));
            return false;
        }

        $filename = uniqid() . '.' . \file::get_extension($uploadFile['name']);
        $filePath = $this->directories[$category] . DIRECTORY_SEPARATOR . $filename;
        $fullPath = $this->rootDir . DIRECTORY_SEPARATOR . $filePath;

        //move uploaded file into correct directory
        if (!@move_uploaded_file($uploadFile['tmp_name'], $fullPath)) {
            $lastErr = error_get_last();
            Services::$logger->addError('LocalFileSystem->storeFile(): Could not move uploaded file to path: '.$lastErr['message'],
                array('path' => $fullPath));
            return false;
        }

        $files = new \mysqli_db_table('files');
        $fileEntry = array(
            'files_category' => $category,
            'files_storage_type' => 'local',
            'files_name' => $uploadFile['name'],
            'files_location' => $filePath,
            'files_size' => $uploadFile['size'],
            'files_datetime' => date('Y-m-d H:i:s')
        );
        $files->insert($fileEntry);
        $fileEntry['files_id'] = $files->last_id();

        return $fileEntry;
    }

    public function storeFileContents($filename, $contents, $category)
    { 
        if (!array_key_exists($category, $this->directories)) {
            Services::$logger->addError('LocalFileSystem->storeFileContents(): Unknown category - where to store?',
                array('category' => $category));
            return false;
        }
        $filePath = $this->directories[$category] . DIRECTORY_SEPARATOR . $filename;
        $fullPath = $this->rootDir . DIRECTORY_SEPARATOR . $filePath;

        $size = count($contents);
        file_put_contents($fullPath, $contents);

        $files = new \mysqli_db_table('files');
        $fileEntry = array(
            'files_category' => $category,
            'files_storage_type' => 'local',
            'files_name' => $filename,
            'files_location' => $filePath,
            'files_size' => $size,
            'files_datetime' => date('Y-m-d H:i:s')
        );
        $files->insert($fileEntry);
        $fileEntry['files_id'] = $files->last_id();
        return $fileEntry;
    }

    public function updateNullDateTime($fileId)
    {
        $files = new \mysqli_db_table('files');
        if (!is_array($fileId)) {
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        $fullPath = $this->getFullPath($file);

        if (file_exists($fullPath)) {
            $datetime = filemtime($fullPath);
            $file['files_datetime'] = date('Y-m-d H:i:s', $datetime);
            $files->update($file, $fileId);
            return $file['files_datetime'];
        }
        return false;
    }

    public function getFileDownloadUrl($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        $fullPath = $this->getFullPath($file);
        $symPath = '/syms/' . \files::unique_sym_name($file['files_name']);
        $fullSymPath = $this->rootDir . $symPath;

        Services::$logger->addDebug('LocalFileSystem->getFileDownloadUrl(): Creating symlink for file',
            array('id' => $fileId, 'path' => $fullPath, 'symPath' => $fullSymPath));

        // Create symlink
        $ln = link($fullPath, $fullSymPath);
        $grp = chgrp($fullSymPath, 'psacln');
        $own = chown($fullSymPath, 'wakeup');

        if (!$ln) {
            Services::$logger->addError('LocalFileSystem->getFileDownloadUrl(): Error creating symlink',
                array('id' => $fileId, 'path' => $fullPath, 'symPath' => $fullSymPath));
            return false;
        } elseif (!$grp || !$own) {
            // If the group/owner change failed, the download could still potentially succeed, since the link was ok
            Services::$logger->addWarning('LocalFileSystem->getFileDownloadUrl(): Error changing symlink ownership/group',
                array('id' => $fileId, 'symPath' => $fullSymPath));
        }

        // The /data thing is a hack. Sorry. The plan is to only use this locally anyways.
        return '/data' . $symPath;
    }

    public function getFileContents($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        $fullPath = $this->getFullPath($file);

        $contents = file_get_contents($fullPath);

        if ($contents == false) {
            Services::$logger->addError('LocalFileSystem->getFileContents(): Could not retrieve file contents',
                array('id' => $fileId, 'path' => $fullPath));
        }
        return $contents;
    }

    public function deleteFile($fileId)
    {
        $files = new \mysqli_db_table('files');
        if (!is_array($fileId)) {
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        if (!$file['files_id']) {
            Services::$logger->addError('LocalFileSystem->deleteFile(): Could not load entry for file ID',
                array('id' => $fileId));
            return false;
        }

        $fullPath = $this->getFullPath($file);

        Services::$logger->addDebug('LocalFileSystem->deleteFile(): Deleting file',
            array('id' => $fileId, 'path' => $fullPath));

        $files->delete($fileId);
        $success = !$files->error();

        if (!$success) {
            Services::$logger->addError('LocalFileSystem->deleteFile(): Could not delete file from database - aborting file system delete',
                array('id' => $fileId, 'path' => $fullPath));
            return false;
        }

        $success = unlink($fullPath);
        if (!$success) {
            Services::$logger->addError('LocalFileSystem->deleteFile(): Could not delete physical file',
                array('id' => $fileId, 'path' => $fullPath));
        }

        return true;
    }


}