<?php

namespace Services\FileSystem;

use GuzzleHttp\Psr7;

use Aws\S3\S3Client;

use Services\Services;

class S3FileSystem implements FileSystem
{
    protected $s3Client;

    public function __construct()
    {

        $options = array(
            'region' => AWS_REGION,
            'version' => '2006-03-01',
            'signature' => 'v4',
            'credentials' => array(
                'key' => AWS_ACCESS_KEY_ID,
                'secret' => AWS_SECRET_ACCESS_KEY
            )
        );
        $this->s3Client = new S3Client($options);
        //$this->s3Client = S3Client::factory($options);
    }

    public function migrateLocalFilesToS3()
    {

    }

    public function migrateLocalFileToS3($file)
    {
        $localFs = new LocalFileSystem(SITE_PATH . DIRECTORY_SEPARATOR . 'data');

        $filename = uniqid() . '.' . \file::get_extension($file['files_name']);
        $filePath = $file['files_category'] . '/' . $filename;
        $localPath = $localFs->getFullPath($file);
        $stream = fopen($localPath, 'rb');

        if (!$stream) {
            Services::$logger->addError('S3FileSystem->migrateLocalFileToS3(): Could not open local file',
                array('$localPath' => $localPath));
            return false;
        }

        $response = $this->s3Client->upload(AWS_BUCKET, $filePath, $stream, 'private', array(
            'params' => array(
                'ContentType' => Psr7\mimetype_from_filename($file['files_name']) ?: 'application/octet-stream',
                'ContentDisposition' => 'attachment; filename="' . $file['files_name'] . '"'
            )
        ));

        if ($response['@metadata']['statusCode'] >= 400) {
            Services::$logger->addError('S3FileSystem->migrateLocalFileToS3(): Could not move local file to S3',
                array(
                    '$localPath' => $localPath,
                    '$filePath' => $filePath,
                    '$bucket' => AWS_BUCKET
                ));
            return false;
        }

        $files = new \mysqli_db_table('files');
        $file['files_storage_type'] = 's3';
        $file['files_location'] = $filePath;
        $file['files_size'] = filesize($localPath);
        $files->update($file, $file['files_id']);
        $success = !$files->error();

        if (!$success) {
            Services::$logger->addError('S3FileSystem->migrateLocalFileToS3(): Could not update file in database - aborting file system delete',
                array('id' => $file['files_id']));
            return false;
        }

        if (!unlink($localPath)) {
            Services::$logger->addWarning('S3FileSystem->migrateLocalFileToS3(): Could not delete local file',
                array('$localPath' => $localPath));
        }

        return true;
    }

    public function storeFile($uploadFile, $category)
    {
        if (!array_key_exists('name', $uploadFile)) {
            Services::$logger->addError('S3FileSystem->storeFile(): $uploadFile should be passed through files::fixUploadFileArray() first');
            return false;
        }
        if (!$category) {
            Services::$logger->addError('S3FileSystem->storeFile(): Unknown category - where to store?',
                array('$category' => $category));
            return false;
        }

        $filename = uniqid() . '.' . \file::get_extension($uploadFile['name']);
        $filePath = $category . '/' . $filename;
        $stream = fopen($uploadFile['tmp_name'], 'rb');

        $response = $this->s3Client->upload(AWS_BUCKET, $filePath, $stream, 'private', array(
            'params' => array(
                'ContentType' => Psr7\mimetype_from_filename($filename) ?: 'application/octet-stream',
                'ContentDisposition' => 'attachment; filename="' . $uploadFile['name'] . '"'
            )
        ));

        if ($response['@metadata']['statusCode'] >= 400) {
            Services::$logger->addError('S3FileSystem->storeFile(): Could not move uploaded file to S3',
                array(
                    '$filePath' => $filePath,
                    '$bucket' => AWS_BUCKET
                ));
            return false;
        }

        $files = new \mysqli_db_table('files');
        $fileEntry = array(
            'files_category' => $category,
            'files_storage_type' => 's3',
            'files_name' => $uploadFile['name'],
            'files_location' => $filePath,
            'files_size' => $uploadFile['size'],
            'files_datetime' => date('Y-m-d H:i:s')
        );
        $files->insert($fileEntry);
        $fileEntry['files_id'] = $files->last_id();

        return $fileEntry;
    }

    public function storeFileContents($filename, $contents, $category)
    {
        if (!$category) {
            Services::$logger->addError('S3FileSystem->storeFileContents(): Unknown category - where to store?',
                array('$category' => $category));
            return false;
        }

        $uniqueName = uniqid() . '.' . \file::get_extension($filename);
        $filePath = $category . '/' . $uniqueName;

        $response = $this->s3Client->upload(AWS_BUCKET, $filePath, $contents, 'private', array(
            'params' => array(
                'ContentType' => Psr7\mimetype_from_filename($filename) ?: 'application/octet-stream',
                'ContentDisposition' => 'attachment; filename="' . $filename . '"'
            )
        ));

        if ($response['@metadata']['statusCode'] >= 400) {
            Services::$logger->addError('S3FileSystem->storeFileContents(): Could not upload contents to S3',
                array(
                    '$filePath' => $filePath,
                    '$bucket' => AWS_BUCKET
                ));
            return false;
        }

        $files = new \mysqli_db_table('files');
        $fileEntry = array(
            'files_category' => $category,
            'files_storage_type' => 's3',
            'files_name' => $filename,
            'files_location' => $filePath,
            'files_size' => count($contents),
            'files_datetime' => date('Y-m-d H:i:s')
        );
        $files->insert($fileEntry);
        $fileEntry['files_id'] = $files->last_id();

        return $fileEntry;
    }

    public function getFileDownloadUrl($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        return $this->getTemporaryLink($file['files_location']);
    }

    public function getFileContents($fileId)
    {
        if (!is_array($fileId)) {
            $files = new \mysqli_db_table('files');
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }
        try {
            $response = $this->s3Client->getObject(array(
                'Bucket' => AWS_BUCKET,
                'Key' => $file['files_location']
            ));

            if ($response == false) {
                Services::$logger->addError('S3FileSystem->getFileContents(): Could not retrieve file contents',
                    array('id' => $fileId, 'key' => $file['files_locations']));
                $contents = null;
            } else {
                $contents = $response['Body'];
            }
        } catch (Exception $ex) {
            Services::$logger->addError('S3FileSystem->getFileContents(): Could not retrieve file contents: '.$ex->getMessage(),
                array('id' => $fileId, 'key' => $file['files_locations']));
            $contents = null;
        }

        return $contents;
    }

    public function deleteFile($fileId)
    {
        $files = new \mysqli_db_table('files');

        if (!is_array($fileId)) {
            $file = $files->get($fileId);
        } else {
            $file = $fileId;
            $fileId = $file['files_id'];
        }

        if (!$file['files_id']) {
            Services::$logger->addError('S3FileSystem->deleteFile(): Could not load entry for file ID',
                array('id' => $fileId));
            return false;
        }

        Services::$logger->addDebug('S3FileSystem->deleteFile(): Deleting file',
            array('id' => $fileId, 'key' => $file['files_locations']));

        $files->delete($fileId);
        $success = !$files->error();

        if (!$success) {
            Services::$logger->addError('S3FileSystem->deleteFile(): Could not delete file from database - aborting file system delete',
                array('id' => $fileId, 'key' => $file['files_locations']));
            return false;
        }

        //204
        $response = $this->s3Client->deleteObject(array(
            'Bucket' => AWS_BUCKET,
            'Key' => $file['files_location']
        ));

        if ($response['@metadata']['statusCode'] >= 400) {
            Services::$logger->addError('S3FileSystem->storeFile(): Could not delete file from S3',
                array('key' => $file['files_locations'], 'bucket' => AWS_BUCKET));
        }

        return true;
    }

    /**
     * https://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-presigned-url.html
     */

    private function getTemporaryLink($path, $expires = 1)
    {

        $cmd = $this->s3Client->getCommand('GetObject', [
            'Bucket' => AWS_BUCKET,
            'Key' => $path
        ]);

        $request = $this->s3Client->createPresignedRequest($cmd, '+' . $expires . ' minutes');

        return (string)$request->getUri();
    }
}