<?php

namespace Services;


class HrProvider
{

    public function getLinkInfo($url)
    {
        $db = \mysqli_db::init();
        $agreement = $db->fetch_singlet('SELECT members_hr_agreement_yn FROM members WHERE members_id = ?',
            array(\ActiveMemberInfo::GetMemberId()));

        $newWindow = true;

        if ($agreement != 1) {
            $redirectUrl = BASEURL . '/members/hr/index.php';
            if ($url) {
                $redirectUrl .= '?url=' . urlencode($url);
            }
            $newWindow = false;
        } else {
            $redirectUrl = 'http://www.hr360.com/autologin.aspx?GUID=3e64529e-8af7-498e-a737-8413631ade91';
            if ($url) {
                $redirectUrl .= '&ReturnURL=' . urlencode($url);
            }
        }

        return array(
            'url' => $redirectUrl,
            'newWindow' => $newWindow
        );
    }
}