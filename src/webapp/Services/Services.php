<?php

namespace Services;

use \Monolog\Logger;
use \Monolog\Handler;

class Services
{
    /*
     * @var \Psr\Logger\LoggerInterface The Logger service
     */
    public static $logger;

    /*
     * @var FileSystem\FileSystem The FileSystem service
     */
    public static $fileSystem;

    public static $hrProvider;

    static function configure()
    {
        Services::configureLogger();
        Services::configureFileSystem();
        Services::configureHr();
    }

    private static function configureLogger()
    {
        $logger = new Logger('wuc');

        if (DEPLOYMENT != 'production') {
            // To add a file log to development, set the file path below.  Make sure the place the log will live has
            // lax permissions (chmod 777 logs). If a problem comes up, the WhatFailureGroupHandler makes sure the exception doesn't
            // stop the app from working.
            $fileHandler = new Handler\StreamHandler(SITE_PATH . '/logs/wuc.log', Logger::DEBUG);
            $ignoreFailures = new Handler\WhatFailureGroupHandler(array($fileHandler));
            $logger->pushHandler($ignoreFailures);
        } else {
            // Where do we want to keep production logs?  How do we want to configure this junk
        }
        Services::$logger = $logger;
    }

    private static function configureFileSystem()
    {
        Services::$fileSystem = new FileSystem\DelegatingFileSystem(SITE_PATH . DIRECTORY_SEPARATOR . 'data');
    }

    private static function configureHr()
    {
        Services::$hrProvider = new HrProvider();
    }
}