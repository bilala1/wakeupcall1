<?php
/**
 * Created by PhpStorm.
 * User: jpatula
 * Date: 5/8/17
 * Time: 9:13 PM
 */

include ACTIONS . '/admin/protect.php';
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');
$db = mysqli_db::init();

$wheres = array();
$params = array();
$accountsId = 0;

if (isset($_POST['accounts_id'])) {
    $accountsId = $_POST['accounts_id'];
    if ($accountsId > 0) {
        $wheres[] = 'join_accounts_id = ?';
        $params[] = $accountsId;
    }
}

if ($accountsId == 0) {
    $wheres[] = 'accounts_is_test = 0';
}

$startDate = (array_key_exists('start_date',$_REQUEST) && strtotime($_REQUEST['start_date']) != false)
    ? $_REQUEST['start_date']
    : date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 day'));

$endDate = (array_key_exists('end_date',$_REQUEST) && strtotime($_REQUEST['end_date']) != false)
    ? $_REQUEST['end_date']
    : date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -30 days'));

$wheres[] = 'daily_summarys_date >= STR_TO_DATE(?, \'%m/%d/%Y\')';
$params[] = $startDate;
$wheres[] = 'daily_summarys_date <= STR_TO_DATE(?, \'%m/%d/%Y\')';
$params[] = $endDate;

$query = "SELECT daily_summarys.* FROM daily_summarys inner join accounts on join_accounts_id = accounts_id" . strings::where($wheres);

$usage = $db->fetch_all($query, $params);

echo json_encode($usage, JSON_NUMERIC_CHECK);
