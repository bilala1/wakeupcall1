<?php

///This class is the base class for an API request in our defined format.
class IRequest
{
    ///public request name
    public $request = NULL;
    
    ///public data array
    public $data = NULL;
    
    //protected error string
    protected $error = NULL;
    
    ///gets wether or not the current request is valid.
    ///returns TRUE = valid.
    public function is_valid()
    {
        if($this->request) return true;
        
        return false;
    }
    
    ///returns a serialized json string.
    ///returns serialized json string, NULL error.
    public function serialize()
    {
        $value = json_encode($this);
        
        if(!$value)
        {
            $this->error = json_last_error_msg();
            return NULL;
        }
        
        return $value;
    }
    
    ///returns a serialized json string.
    ///returns TRUE = valid.
    public function deserialize($json_string)
    {
        $json_array = json_decode($json_string, true);
        
        if(!$json_array['request'])
        {
            $this->error = json_last_error_msg();
            return false;
        }
        
        $this->request = $json_array['request'];
        $this->data = $json_array['data'];
        
        if(!$this->request)
        {       
            $this->error = "ERROR: Null Request";
            return false;
        }
        
        return true;
    }
    
    //if serialize or deserialize fail you can get an error message here!
    public function get_error()
    {
        return $this->error;
    }
}

