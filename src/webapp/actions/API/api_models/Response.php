<?php

class IResponse
{
    ///public request name
    public $response = NULL;
    
    ///public data array
    public $data = NULL;
    
    ///public message denoting error or success
    public $message = NULL;
        
    //protected error string
    protected $error = NULL;
    
    public function is_valid()
    {
        if($this->request) return true;
        
        return false;
    }
    
    public function serialize()
    {
        $value = json_encode($this);
        
        if(!$value)
        {
            $this->error = son_last_error_msg();
            return NULL;
        }
        
        return $value;
    }
    
    public function deserialize($json_string)
    {
        $json_array = json_decode($json_string, true);
        
        if(!$json_array['response'])
        {
            $this->error = son_last_error_msg();
            return false;
        }
        
        $this->response = $json_array['response'];
        $this->message = $json_array['message'];
        $this->data = $json_array['data'];
        
        if($this->request)
        {       
            $this->error = "ERROR: Null Response";
            return false;
        }
        
        return false;
    }
}