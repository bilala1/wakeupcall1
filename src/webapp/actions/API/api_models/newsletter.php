<?php
include(SITE_PATH . '/' . ACTIONS . '/API/api_models/Request.php');
include(SITE_PATH . '/' . ACTIONS . '/API/api_models/Response.php');

class NewsletterRequest extends IRequest
{
    private $EmailAddress = NULL;
    
    public function is_valid()
    {
        //validate request type
        if($this->request != "subscribe" &&
           $this->request != "unsubscribe") return false;
        
        //validate email address
        if(!$this->EmailAddress) return false;
        
        return parent::is_valid();
    }
    
    public function serialize()
    {
        //serialize e-mail into data array.
        $this->data = array("email" => $this->EmailAddress);
        
        return parent::serialize();
    }
    
    public function deserialize($json_array)
    {       
        if(!parent::deserialize($json_array)) return false;
        
        //deserialize e-mail into data array
        if($this->data['email'])
        {
            $this->EmailAddress = $this->data['email'];          
        }
        else 
        {
            $this->error = "Email address cannot be NULL";
            return false;
        }
        
        return $this->is_valid();
    }
    
    ///Sets an e-mail address to the passed in parameter
    ///PARAM: email_address - email to set
    public function set_email($email_address)
    {
        $this->EmailAddress = $email_address;
    }

    //Gets the current email address value
    //returns: EMAIL ADDRESS
    public function get_email()
    {
        return $this->EmailAddress;
    }
}