<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/31/15
 * Time: 8:07 PM
 */

// TODO: refactor this to use jesse's API classes
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

include(ACTIONS . '/members/protect.php');

$carriers_id = $_REQUEST['carriers_id'];

//TODO: should only return this claim if owned by the user's account.

$carrier = carriers::get_carrier($carriers_id);

echo json_encode($carrier);