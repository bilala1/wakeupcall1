<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/31/15
 * Time: 8:07 PM
 */

// TODO: refactor this to use jesse's API classes
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

include(ACTIONS . '/members/protect.php');

$licensed_locations_id = $_REQUEST['licensed_locations_id'];
$is_recipients_only = $_REQUEST['is_recipients_only'] == 'true';
$carriers = array();
if ( is_numeric($licensed_locations_id)) {
    $carriers = carriers::get_carriers_for_location($licensed_locations_id, $is_recipients_only);
}

echo json_encode($carriers);