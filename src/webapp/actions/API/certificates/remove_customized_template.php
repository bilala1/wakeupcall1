<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$access = UserPermissions::UserCanAccessScreen('certificates');
$certificate_id = $_REQUEST['certificates_id'];
$certificate = certificates::get_certificate($_REQUEST['certificates_id']);
$permissions = UserPermissions::UserObjectPermissions('certificates', $certificate);
if(!$access || ($certificate && !$permissions['edit'])) {
    http::redirect(BASEURL .'/members/my-documents/certificates/index.php', array('notice' => 'You don\'t have permission to edit that certificate'));
}

if($certificate_id){
    $templates = certificates_email_templates::get_template_for_certificate($certificate_id);
    $certificates_email_templates_id = $templates['certificates_email_templates_id'];
    certificates_email_templates::delete_by_id($certificates_email_templates_id);
    certificates::remove_certificates_email_templates($certificate_id);
    $default_template =  certificates_email_templates::getDefaultTemplate();
    $default_template_id="0";
    if($default_template){
        $default_template_id = $default_template['certificates_email_templates_id'];
    }
    echo $default_template_id;
}