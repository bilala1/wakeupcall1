<?php

include(ACTIONS . '/members/protect.php');

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$paging = new paging(500, $_REQUEST['page']);
$startDate = date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 year'));
$endDate = date('n/j/Y', time());
$claims = claims::get_paged_claims_list($paging, $startDate, $endDate, null, false, true, ActiveMemberInfo::GetMemberId(), null);
echo json_encode($claims);