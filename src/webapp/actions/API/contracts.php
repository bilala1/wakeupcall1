<?php

include(ACTIONS . '/members/protect.php');

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$paging = new paging(500, $_REQUEST['page']);
$contracts = contracts::get_paged_contracts_list($paging);
echo json_encode($contracts);