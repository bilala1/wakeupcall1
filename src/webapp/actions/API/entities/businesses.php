<?php

    include(ACTIONS . '/members/protect.php');

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$paging = new paging(500, $_REQUEST['page']);
$businesses = entities::get_paged_businesses_list($paging, null, false, null);

foreach($businesses as &$business) {
    $items = entities_items::get_entities_items_by_entity_id($business['entities_id']);
    foreach($items as &$item) {
        $milestones = entities_items::get_entities_items_milestones($item['entities_items_id']);
        $item['milestones'] = $milestones;
    }
    $business['tracked_items'] = $items;
}

echo json_encode($businesses);