<?php

include(ACTIONS . '/members/protect.php');

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$db = mysqli_db::init();

$locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());

echo json_encode($locations);