<?php

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

echo '{"maintenanceMode": "'.SITE_NAME.' is currently undergoing maintenance - please try again shortly."}';