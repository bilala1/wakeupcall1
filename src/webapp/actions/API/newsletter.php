<?php
include(SITE_PATH . '/' . ACTIONS . '/API/api_models/newsletter.php');
        
function response_fail($message)
{
    $response = new IResponse();
    $response->response = "failure";
    $response->message = $message;
    $response->data = NULL;
    
    return $response->serialize();
}

function response_success($message)
{
    $response = new IResponse();
    $response->response = "success";
    $response->message = $message;
    $response->data = NULL;
    
    return $response->serialize();
}

function unsubscribe($email_addr)
{
    if($email_addr && filter_var($email_addr, FILTER_VALIDATE_EMAIL))
    {
        $db = mysqli_db::init();
        $db->query('DELETE FROM newsletter_subscribers WHERE email_address = ?', array($email_addr));
        
        echo response_success("User Unsubscribed!");
    }
    else
    {
        echo response_fail("E-Mail address is not valid.");
    }
}

function subscribe($email_addr)
{
    if($email_addr && filter_var($email_addr, FILTER_VALIDATE_EMAIL))
    {
        $db = mysqli_db::init();
        $db->query('INSERT INTO newsletter_subscribers(email_address) VALUES(?)', array($email_addr));
        
        echo response_success("User Subscribed!");
    }
    else
    {
        echo response_fail("E-Mail address is not valid.");
    }
}

if ($_POST['request']) 
{  
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
        
    $request = new NewsletterRequest;
    $request->deserialize($_POST['request']);
    
    if($request->is_valid())
    {
        $request_type = $request->request;
        $request_data = $request->get_email();

        if($request_type === 'subscribe')
        {
            subscribe($request_data);
        }
        else if($request_type === 'unsubscribe')
        {
            unsubscribe($request_data);
        }
    }
    else
    {
        $error = "invalid request: ". $request->get_error();
        echo response_fail($error);
    }
}
?>