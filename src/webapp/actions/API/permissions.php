<?php

include(ACTIONS . '/members/protect.php');

header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$permissions = UserPermissions::AllPermissionTypesForUser(ActiveMemberInfo::GetMemberId());
echo json_encode($permissions);