<?php

include(ACTIONS . '/members/protect.php');

define('FRAMEWORK_NOVIEW', true);

header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
    case 'PUT':
        $request = json_decode(file_get_contents('php://input'), true);
        $preference = $request["key"];
        $value = $request["value"];

        if ($preference) {
            UserPreferences::setForMember(ActiveMemberInfo::GetMemberId(), $preference, $value);
            $prefs = UserPreferences::getForMember(ActiveMemberInfo::GetMemberId());
            echo json_encode($prefs);
        } else {
            $error = "Invalid request: " . $request->get_error();
            echo response_fail($error);
        }
        break;

    case 'OPTIONS':
        break;

    case 'GET':
    default:
        $prefs = UserPreferences::getForMember(ActiveMemberInfo::GetMemberId());
        echo json_encode($prefs);
        break;
}
