<?php
/**
 * Created by NetBeans.
 * User: Vidya
 * Date: 10/08/16
 * Time: 2:10 PM
 */
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$licensed_locations_id = $_REQUEST['licensed_locations_id'];
$permission = $_REQUEST['permission'];
if(!$licensed_locations_id && !ActiveMemberInfo::IsAccountMultiLocation()){
    $licensed_locations_id = ActiveMemberInfo::SingleLocationUserLocationId();
}
UserPermissions::UserCanAccessObject('licensed_locations', $licensed_locations_id);

$users = UserPermissions::UsersWithLocationPermission($permission,$licensed_locations_id);
$jsonFormatted = array();
foreach($users as $user) {
    $jsonFormatted[] = array(
        'id' => $user['members_id'],
        'firstName' => $user['members_firstname'],
        'lastName' => $user['members_lastname'],
        'email' => $user['members_email'],
        'permission' => $user['permission'],
    );
}
echo json_encode($jsonFormatted);
?>