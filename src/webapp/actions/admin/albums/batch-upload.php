<?php

$db = mysqli_db::init();
$albums_table = new mysqli_db_table('albums');
$album = $db->fetch_one('select * from albums where albums_id = ?', array($_REQUEST['join_photos_albums_id']));

if(empty($album['albums_root_folder']))
{
    $dir = SITE_PATH .'/data/albums/';
}
else
{
    $dir = SITE_PATH .$album['albums_root_folder']."/";
}

if (!is_dir($dir)) {
	mkdir($dir);
}
$allowed_ext = array('jpg', 'jpeg');

$files = array();
if (is_array($_FILES['photos']['name'])) {
	echo 'we have an array';
	for($i = 0; $i < count($_FILES['photos']['name']); ++$i) {
		$files[] = array(
			'name' => $_FILES['photos']['name'][$i],
			'type' => $_FILES['photos']['type'][$i],
			'tmp_name' => $_FILES['photos']['tmp_name'][$i],
			'size' => $_FILES['photos']['size'][$i],
			'error' => $_FILES['photos']['error'][$i]
		);
	}
} else {
	echo 'not an array';
	$files[] =  $_FILES['photos'];
}

$next_sort = $db->fetch_singlet('SELECT MAX(albums_photos_sort) + 1 FROM albums_photos WHERE join_photos_albums_id=?', array($_REQUEST['join_photos_albums_id']));
foreach($files as $file) {
	if ($file['error'] !== UPLOAD_ERR_OK) {
		continue;
	}
	$filename = uniqid('alb_photo_') . '.jpg';
	
	$image = new image($file['tmp_name']);
	$image->save_original($dir . $filename.'.org.jpg');
	
	$album_table = new mysqli_db_table('albums_photos');
	$album_table->insert(array(
		'join_albums_id' => $_REQUEST['join_photos_albums_id'],
		'albums_photos_name' => $file['name'],
		'albums_photos_filename' => $filename,
		'albums_photos_datestamp' => SQL('UTC_TIMESTAMP'),
		'albums_photos_sort' => $next_sort++
	));
	
	/*
	$ext = strtolower(array_pop(explode('.', $file['name'])));
	$filename = files::uniqname($dir, $ext);
	$thumb_name = files::uniqname($dir, 'jpeg');
	$thumb_big_name = $thumb_name . '.big.jpeg';
	$result = files::upload($file, $filename, $dir, null, $allowed_ext, MAX_UPLOAD_SIZE);

	if ($result === true) {
		$img = image::load($dir . $filename);
		image::square_crop($img, 100, $dir . $thumb_name);
		image::scale($img, null, 210, $dir . $thumb_big_name);
	
		$photo_table = new mysqli_db_table('photos');
		$photo_table->insert(array(
			'join_photos_albums_id' => $join_photos_albums_id,
			'join_members_id' => ActiveMemberInfo::GetMemberId(),
			'photos_filename' => $file['name'],
			'photos_path' => $relative_dir . $filename,
			'photos_thumb_path' => $relative_dir . $thumb_name
		));
		$photos_id = $photo_table->last_id();
		
		$photo_ids []= $photos_id;
	}
	*/
	//echo 'file uploaded!<br/>';
}

http::redirect(FULLURL . '/admin/albums/edit.php?albums_id='.$_REQUEST['join_photos_albums_id'].'&notice=Files+Uploaded!');
?>