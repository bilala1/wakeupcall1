<?php
include_once 'protect.php';

permissions::check('Album Admin Access');

$album = new mysqli_db_table('albums');
$album->delete($_REQUEST['albums_id']);

http::redirect(FULLURL . '/admin/albums/list.php?notice=' . urlencode('Your album has been removed'));
?>