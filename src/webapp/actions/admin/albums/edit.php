<?php

include_once ACTIONS . '/admin/protect.php';

permissions::check('Album Admin Access');

$db = mysqli_db::init();
$albums_table = new mysqli_db_table('albums');


if (!empty($_REQUEST['albums_id'])) {
    //$bills_table = new mysqli_db_table('bills');
    $album = $db->fetch_one('select * from albums where albums_id = ?', array($_REQUEST['albums_id']));

    if(empty($album['albums_root_folder']))
    {
        $photo_directory = '/data/albums/';
    }
    else
    {
        $photo_directory = $album['albums_root_folder']."/";
    }
}
/*
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

$where = array();
$params = array();

switch($_REQUEST['order']) {
    default:
        $_REQUEST['by'] = 'DESC';
        $_REQUEST['order'] = 'albums_photos_datestamp';
    case 'albums_photos_datestamp':
        $order = 'albums_photos_datestamp';
    break;
    case 'albums_photos_name':
        $order = 'albums_photos_name';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order . ' ' . $by;

$where[] = 'join_albums_id = ?';
$params[] = $_REQUEST['albums_id'];

$photos = $db->fetch_all('
    SELECT *, DATE_FORMAT(albums_photos_datestamp, "'.DATE_FORMAT_FULL_SQL.'") AS albums_photos_datestamp
    FROM albums_photos
    '.strings::where($where).'
    ORDER BY '.$orderby, $params);

$albums = new mysqli_db_table('albums');
$album  = $albums->get((int) $_REQUEST['albums_id']);

if($_POST){
    $data = $_POST;

    if($_FILES['albums_icon']['size']){
        $filename = uniqid('alb_') . '.jpg';
        $dir      = SITE_PATH .'/images/albums/';

        $data['albums_icon'] = $filename;

        $image = new image($_FILES['albums_icon']['tmp_name']);
        $image->resize($dir . $filename, 640)
            ->resize($dir . $filename.'.icon.jpg', 120);

        @unlink($dir . $album['albums_icon']);
    }

    if($album){
        unset($data['albums_id']);

        $albums->update($data, $album['albums_id']);
        $album_id = $album['albums_id'];
    }
    else {
        $albums->insert($data);
        $album_id = $albums->last_id();
    }

    http::redirect(BASEURL .'/admin/albums/edit.php?albums_id='.$album_id.'&notice='.urlencode('Album saved!'));
}

$total = $db->fetch_singlet('
    SELECT COUNT(*)
    FROM albums_photos
    '.strings::where($where), $params);
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/albums/list.php');

$album_list = html::db_to_select_array(albums::get_all_albums(),'albums_id','albums_name','-none-');
if($_REQUEST['albums_id']){
    unset($album_list[$_REQUEST['albums_id']]);
}

*/
if (!empty($_POST['photo'])) {
	$updates = array();
	$params = array();
	
	ksort($_POST['photo']);
	$i = 0;
	
	foreach($_POST['photo'] as $id) {
		$updates[] = 'WHEN albums_photos_id = ? THEN ?';
		$params[] = $id;
		$params[] = $i++;
	}
	
	$db->query('
		UPDATE albums_photos SET albums_photos_sort = (
			CASE 
				' . implode("\n", $updates) . '
				ELSE albums_photos_id
			END
		)',
		$params
	);
}
elseif($_POST)//Update album
{
    $data = $_POST;
    $errors = array();

    if (empty($data['albums_name'])) {
        $errors['albums_name'] = 'Please enter an album name';
    }elseif (preg_match('/^[0-9A-Za-z/', trim($data['albums_name']))) {
        $errors['albums_name'] = 'Name can only have letters and numbers and no spaces.';
    }

    $validate = new validate(array());
    $validate
        ->setAllOptional();

    $errors = $validate->test();

    if(empty($errors)){
        $album['albums_name'] = $data['albums_name'];
        $album['albums_root_folder'] = $data['albums_root_folder'];
        if(empty($data['albums_id'])){

            $data['albums_parent']= 0;
            $data['albums_icon']= "";
            //Create new
            $albums_table->insert($data);
            $albums_id = $albums_table->last_id();

            $notice = 'Album added';
        }else{

            $albums_table->update($data, $data['albums_id']);
            $albums_id = $data['albums_id'];


            if(!$notice){
                $notice = 'Album updated';
            }
        }

        http::redirect('edit.php?albums_id=' . $albums_id . '&notice=' . urlencode($notice));
    }else{
        //so user input is not lost
        $album = array_merge($album, $data);
    }
}


function get_display_date($order) {
	if ($order > 365) {
		return 'never';
	}
	static $total = null;
	if ($total === null) {
		$db = mysqli_db::init();
		$total = $db->fetch_singlet('SELECT COUNT(*) FROM albums_photos');
	}


	$today = date('z');
	
	$display_day = $order;
	$days_in_this_year = date('L') ? 366 : 365;
	while ($display_day < $today) {
		$display_day = $display_day + $total;
	}
	
	if ($display_day >= $days_in_this_year) {
		return date('m/d/Y', strtotime(date('Y') + 1 . '-01-01 + ' . ($order) . ' DAYS'));
	} else {
		return date('m/d/Y', strtotime(date('Y') . '-01-01 + ' . $display_day . ' DAYS'));
	}
	
}

$today = date('m/d/Y');
$photos = $db->fetch_all('SELECT * FROM albums_photos WHERE join_albums_id = ? ORDER BY albums_photos_sort ASC', array($album['albums_id']));
$date_list = array();

for($i = 0; $i < count($photos); ++$i) {
	$date_list[$i] = $photos[$i]['display_date'] = get_display_date($photos[$i]['albums_photos_sort']);
}
?>
