<?php
include_once ACTIONS . '/admin/protect.php';

permissions::check('Album Admin Access');

$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

$where = array();
$params = array();

switch($_REQUEST['order']) {
    default:
        $_REQUEST['by'] = 'ASC';
        $_REQUEST['order'] = 'albums_name';
    case 'albums_name':
        $order = 'a.albums_name';
    break;
    case 'photos':
        $order = 'photos';
    break;
}
    
$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order . ' ' . $by;

$albums = $db->fetch_all('
    SELECT a.*, COUNT(DISTINCT albums_photos_id) AS photos
    FROM albums AS a
    LEFT JOIN albums_photos AS ap ON ap.join_albums_id = a.albums_id
    '.strings::where($where).'
    GROUP BY a.albums_id
    ORDER BY '.$orderby, $params);

$total = $db->fetch_singlet('SELECT COUNT(*) FROM albums');
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/albums/list.php');

?>
