<?php
//Don't want to delete on accident!

permissions::check('Album Admin Access');

$photo = new mysqli_db_table('albums_photos');
$photo->delete($_REQUEST['albums_photos_id']);

//Need to resort the photos
$db = mysqli_db::init();
$photos = $db->fetch_singlets('SELECT albums_photos_id FROM albums_photos ORDER BY albums_photos_sort ASC');

$updates = array();
$params = array();

foreach($photos as $sort_order => $id) {
	$updates[] = 'WHEN albums_photos_id = ? THEN ?';
	$params[] = $id;
	$params[] = $sort_order;
}

$db->query('
	UPDATE albums_photos SET albums_photos_sort = (
		CASE 
			' . implode("\n", $updates) . '
			ELSE albums_photos_id
		END
	)',
	$params
);

http::redirect(FULLURL . '/admin/albums/edit.php?albums_id='.$_REQUEST['albums_id'].'&notice=' . urlencode('Your photo has been removed'));
?>