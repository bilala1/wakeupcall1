<?php
include_once ACTIONS . '/admin/protect.php';

permissions::check('Album Admin Access');

$db = mysqli_db::init();
$photos = new mysqli_db_table('albums_photos');

if(isset($_REQUEST['albums_id'])){
    $albums = new mysqli_db_table('albums');
    $album  = $albums->get((int) $_REQUEST['albums_id']);
}else{
    $photo  = $photos->get((int) $_REQUEST['albums_photos_id']);
    $album  = albums::get_by_photo($_REQUEST['albums_photos_id']);
}

if(empty($album['albums_root_folder']))
{
    $directory = SITE_PATH . '/data/albums/';
}
else
{
    $directory = SITE_PATH . $album['albums_root_folder']."/";
}

$other_photos = $db->fetch_all('SELECT * FROM albums_photos WHERE join_albums_photos_id = ?', array($_REQUEST['albums_photos_id']));

if($_POST){
    $data = $_POST;
    $data['join_albums_photos_id'] = 0;
    
    $errors = array();
    if(empty($data['albums_photos_name'])){
        $errors[] = 'You must give the photo a name';
    }
    if(!$photo && !$_FILES['albums_photos_filename']['size']){
        $errors[] = 'You must provide a photo';
    }
    
    if(empty($errors)){
        if($_FILES['albums_photos_filename']['size']){
            $filename = uniqid('alb_photo_') . '.jpg';
            $dir      = $directory;
            
            $data['albums_photos_filename'] = $filename;
            
            $image = new image($_FILES['albums_photos_filename']['tmp_name']);
            $image->save_original($dir . $filename.'.org.jpg');
            
            @unlink($dir . $photo['albums_photos_filename'].'.org.jpg');
        }
        else {
            unset($data['albums_photos_filename']);
        }
        
        if($photo){
            unset($data['albums_photos_id']);
            
            $data['albums_photos_filename'] = $filename ? $filename : $db->fetch_singlet('SELECT albums_photos_filename FROM albums_photos WHERE albums_photos_id = ?', array($photo['albums_photos_id']));
            
            $photos->update($data, $photo['albums_photos_id']);
            $albums_photos_id = $photo['albums_photos_id'];
            $notice = 'Photo Saved';
        }
        else {
            $data['albums_photos_datestamp'] = SQL('UTC_TIMESTAMP');

            $photos->insert($data);
            $albums_photos_id = $photos->last_id();
            $notice = 'Photo created';
        }
        
        http::redirect(BASEURL .'/admin/albums/photos/edit.php?albums_photos_id='.$albums_photos_id.'&notice='.urlencode($notice));
    }else {
        $photo = $data;
    }
    
}

$join_albums_id = $photo['join_albums_id'];
$join_albums_id = $_REQUEST['albums_id'] ? $_REQUEST['albums_id'] : $join_albums_id;
?>
