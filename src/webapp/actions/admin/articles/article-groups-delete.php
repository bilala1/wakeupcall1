<?php
include_once 'protect.php';

$article_group = new mysqli_db_table('article_groups');
$article_group->delete($_REQUEST['article_group_id']);

http::redirect(FULLURL . '/admin/article-groups.php?notice=' . urlencode('Your article group has been removed'));
?>
