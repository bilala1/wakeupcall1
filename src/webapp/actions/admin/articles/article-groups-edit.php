<?php
include_once 'protect.php';
if(!empty($_POST)) 
{
    $data = $_POST;

    if($data['article_group_id'])
    {
        $article_groups = new mysqli_db_table('article_groups');
        $article_groups->update($data, $data['article_group_id']);
        $article_group_id = $data['article_group_id'];
    }
    else
    {
        $article_groups = new mysqli_db_table('article_groups');
        $article_groups->insert($data);
        $article_group_id = $article_groups->last_id();
    }
    http::redirect(FULLURL . '/admin/article-groups-edit.php?article_group_id=' . $article_group_id . '&notice=' . urlencode('Your article group as been saved'));
}

$article_groups = new mysqli_db_table('article_groups');
$article_group = $article_groups->get($_REQUEST['article_group_id']);

array_map(array('html', 'entities_fix'), $article_group);

$notice = urldecode($_REQUEST['notice']);
?>
