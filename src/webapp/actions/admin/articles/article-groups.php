<?php
include_once 'protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Get Articles
//------------------------------------------------------------------------------
$where = array();
$params = array();
//----------------------------------------------------------------------
//-- Where
//----------------------------------------------------------------------
if(!empty($_REQUEST['term'])) {
    $where[]  = 'name LIKE ?';
    $params[] = '%' . $_REQUEST['term'] . '%';
}
$where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';
$article_groups = $db->fetch_all('SELECT *
    FROM article_groups
    ' . $where . '
    ORDER BY zorder, name
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    $params);
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM article_groups ' . $where, $params);
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/article-groups.php');
//------------------------------------------------------------------------------
//-- Get Notice
//------------------------------------------------------------------------------
$notice = urldecode($_REQUEST['notice']);
?>
