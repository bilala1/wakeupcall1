<?php
include_once ACTIONS . '/admin/protect.php';

if(!articles::category_has_articles($_REQUEST['articles_categories_id'])){
    $articles_categories = new mysqli_db_table('articles_categories');
    $articles_categories->delete($_REQUEST['articles_categories_id']);
    $notice = 'Your article category has been removed';
}else{
    $notice = 'This category cannot be deleted because it contains articles';
}

http::redirect(FULLURL . '/admin/articles/categories/list.php?notice=' . urlencode($notice));
?>
