<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
if(!empty($_POST)) 
{
    $data = $_POST;
    if($data['articles_categories_id'])
    {
        $articles_categories = new mysqli_db_table('articles_categories');
        $articles_categories->update($data, $data['articles_categories_id']);
        $articles_categories_id = $data['articles_categories_id'];
    }
    else
    {
        $articles_categories = new mysqli_db_table('articles_categories');
        $articles_categories->insert($data);
        $articles_categories_id = $articles_categories->last_id();
    }
    
    $params = array();
    $params['notice'] = 'Your article category has been saved';
    $params['articles_categories_id'] = $articles_categories_id;
    http::redirect(FULLURL . '/admin/articles/categories/edit.php?' . http::build_query($params));
}

$articles_categories = new mysqli_db_table('articles_categories');
$articles_category = $articles_categories->get($_REQUEST['articles_categories_id']);

$notice = urldecode($_REQUEST['notice']);
?>
