<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Get Articles
//------------------------------------------------------------------------------
$where = array();
$params = array();
//----------------------------------------------------------------------
//-- Where
//----------------------------------------------------------------------
if(!empty($_REQUEST['term'])) 
{
    $where[]  = 'articles_categories_name LIKE ?';
    $params[] = '%' . $_REQUEST['term'] . '%';
}
if(!empty($_REQUEST['article_group_id'])) 
{
    $where[]  = '(FIND_IN_SET(?, article_groups))';
    $params[] = $_REQUEST['article_group_id'];
}
$where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';
//------------------------------------------------------------------------------
//-- Order
//------------------------------------------------------------------------------
switch($_REQUEST['order']) {
    default:
        $_REQUEST['by'] = 'ASC';
        $_REQUEST['order'] = 'articles_categories_name';
    case 'articles_categories_name':
        $order = 'articles_categories.articles_categories_name';
    break;
    
    case 'total_articles':
        $order = 'total_articles';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order . ' ' . $by;

//------------------------------------------------------------------------------
//-- Rows
//------------------------------------------------------------------------------
$categories = $db->fetch_all('SELECT articles_categories.*, COUNT(DISTINCT articles_id) AS total_articles
    FROM articles_categories
    LEFT JOIN articles ON articles.join_articles_categories_id = articles_categories.articles_categories_id
    ' . $where . '
    GROUP BY articles_categories.articles_categories_id
    ORDER BY ' . $orderby . '
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    $params);
    
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(articles_categories.articles_categories_id) FROM articles_categories ' . $where, $params);
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/articles.php');
//------------------------------------------------------------------------------
//-- Get Notice
//------------------------------------------------------------------------------
$notice = urldecode($_REQUEST['notice']);
?>
