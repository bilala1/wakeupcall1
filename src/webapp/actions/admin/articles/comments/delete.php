<?php
include_once 'protect.php';

$articles_comments = new mysqli_db_table('articles_comments');
$articles_comments->delete($_REQUEST['articles_comments_id']);

http::redirect(FULLURL . '/admin/articles/comments/list.php?notice=' . urlencode('Your article comment has been removed'));
?>
