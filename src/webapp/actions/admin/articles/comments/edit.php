<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
if(!empty($_POST)) 
{
    $data = $_POST;
    
    //--------------------------------------------------------------------------
    //-- UTF-8 Issues
    //--------------------------------------------------------------------------
	$data['body'] = utf8_encode(html::entities_fix($data['body']));
	$data['teaser'] = utf8_encode(html::entities_fix($data['teaser']));

    //--------------------------------------------------------------------------
    //-- Format time to UTC
    //--------------------------------------------------------------------------
    $data['articles_comments_post_date'] = times::to_mysql_utc($data['publish_date'] . ' ' . $data['publish_time']);
    
    //-- Update / Insert
    $params = array();
    if($data['articles_comments_id'])
    { //strings::pre_dump($data); exit;
        $articles_comments = new mysqli_db_table('articles_comments');
        $articles_comments->update($data, $data['articles_comments_id']);
        $articles_comments_id = $data['articles_comments_id'];
        $params['notice'] = 'Your article comment has been updated';
    }
    else
    {
        $articles_comments = new mysqli_db_table('articles_comments');
        $articles_comments->insert($data);
        $articles_comments_id = $articles_comments->last_id();
        $params['notice'] = 'Your article comment has been created';
    }
    
    $params['articles_comments_id'] = $articles_comments_id;
    http::redirect(FULLURL . '/admin/articles/comments/edit.php?' . http::build_query($params));
}

$articles_comments = new mysqli_db_table('articles_comments');
$article_comment = $articles_comments->get($_REQUEST['articles_comments_id']);
$articles = $db->fetch_all('SELECT * FROM articles ORDER BY articles_title');

$notice = urldecode($_REQUEST['notice']);

$article_comment['post_date'] = strtotime($article_comment['post_date']);
$article_comment['post_date'] = $article_comment['post_date'] ? $article_comment['post_date'] : strtotime('NOW');
?>
