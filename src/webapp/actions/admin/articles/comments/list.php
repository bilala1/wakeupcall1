<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

//------------------------------------------------------------------------------
//-- Get Articles
//------------------------------------------------------------------------------
$where = array();
$params = array();

//----------------------------------------------------------------------
//-- Where
//----------------------------------------------------------------------
if(!empty($_REQUEST['articles_id'])) {
    $where[]  = 'join_articles_id = ?';
    $params[] = $_REQUEST['articles_id'];
}
if(!empty($_REQUEST['articles_comments_title'])) {
    $where[]  = 'articles_comments_title LIKE ?';
    $params[] = '%' . $_REQUEST['articles_comments_title'] . '%';
}
if(!empty($_REQUEST['articles_comments_body'])) {
    $where[]  = 'articles_comments_body LIKE ?';
    $params[] = '%' . $_REQUEST['articles_comments_body'] . '%';
}
$where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';

//------------------------------------------------------------------------------
//-- Order
//------------------------------------------------------------------------------
switch($_REQUEST['order']) {
    default:
        $_REQUEST['order'] = 'articles_comments_post_date';
        $_REQUEST['by'] = 'DESC';
    case 'articles_comments_post_date':
        $order = 'articles_comments_post_date';
    break;
    
    case 'articles_comments_author':
        $order = 'articles_comments_author';
    break;
    
    case 'articles_comments_email':
        $order = 'articles_comments_email';
    break;
    
    case 'articles_comments_title':
        $order = 'articles_comments_title';
    break;
    
    case 'articles_comments_body':
        $order = 'articles_comments_body';
    break;
}
$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order . ' ' . $by;

$article_comments = $db->fetch_all('
    SELECT articles_comments.*
    FROM articles_comments 
    ' . $where . '
    ORDER BY ' . 
    $orderby . '
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    $params);
    
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM articles_comments ' . $where, $params);
$pages = ceil($total / $per_page);

//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/articles/comments/list.php');

//------------------------------------------------------------------------------
//-- Get Notice
//------------------------------------------------------------------------------
$notice = urldecode($_REQUEST['notice']);
?>
