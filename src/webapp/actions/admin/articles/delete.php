<?php
include_once 'protect.php';

//delete article comments
$db = mysqli_db::init();
$db->query('delete from articles_comments where join_articles_id = ?', array($_REQUEST['articles_id']));

//delete article
$article = new mysqli_db_table('articles');
$article->delete($_REQUEST['articles_id']);

http::redirect(FULLURL . '/admin/articles/list.php?notice=' . urlencode('The article has been removed'));
?>