<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

if(!empty($_POST)) 
{
    $data = $_POST;
	$data['articles_body'] = utf8_encode(html::entities_fix($data['articles_body']));
	$data['articles_teaser'] = utf8_encode(html::entities_fix($data['articles_teaser']));

    //--------------------------------------------------------------------------
    //-- Format time to UTC
    //--------------------------------------------------------------------------
    $data['articles_newsdate'] = times::to_mysql_utc($data['publish_date'] . ' ' . $data['publish_time']);
    
    //--------------------------------------------------------------------------
    //-- Update / Insert
    //--------------------------------------------------------------------------
    /*if(is_array($data['join_articles_tags']))
    {
        $tag_names = array();
        foreach($data['join_articles_tags'] as $articles_tags_id)
        {
            $tag_names []= $db->fetch_singlet('SELECT articles_tags_name FROM articles_tags WHERE articles_tags_id = ?', array($articles_tags_id));
        }
        $data['join_articles_tags'] = implode(',', $data['join_articles_tags']);
        $data['articles_tags'] = implode(',', $tag_names);
    }*/
    
    if($data['join_articles_categories_id'])
    {
        $articles_categories_name = $db->fetch_singlet('SELECT articles_categories_name FROM articles_categories WHERE articles_categories_id = ?', array($data['join_articles_categories_id']));
        $data['articles_category'] = $articles_categories_name;
    }
    
    if($data['articles_id'])
    {
        $articles = new mysqli_db_table('articles');
        $data['join_articles_tags'] = implode(',', tags::add_tags($data['join_articles_tags']));
        $articles->update($data, $data['articles_id']);
        tags::recalculate_articles();
        $articles_id = $data['articles_id'];
        $notice = 'Your article has been updated';
    }
    else
    {
        $articles = new mysqli_db_table('articles');
        $data['join_articles_tags'] = implode(',', tags::add_tags($data['join_articles_tags']));
        $articles->insert($data);
        tags::recalculate_articles();
        $articles_id = $articles->last_id();
        $notice = 'Your article has been created';
        
        
    }

    //--------------------------------------------------------------------------
    //-- Recalculate tag cloud
    //--------------------------------------------------------------------------
    //article_tags::calculate_tag_depth();
    
    http::redirect(FULLURL . '/admin/articles/edit.php?articles_id=' . $articles_id . '&notice=' . urlencode($notice));
}

$articles = new mysqli_db_table('articles');
$article = $articles->get($_REQUEST['articles_id']);
$article_categories = article_categories::get_ordered();

$article['published'] = strtotime($article['newsdate']);
$article['published'] = $article['published'] ? $article['published'] : strtotime('NOW');

$notice = urldecode($_REQUEST['notice']);
?>
