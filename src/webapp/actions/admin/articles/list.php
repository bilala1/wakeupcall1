<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Get Article Groups
//------------------------------------------------------------------------------
$article_groups = article_categories::get_ordered();
//------------------------------------------------------------------------------
//-- Get Articles
//------------------------------------------------------------------------------
$where = array();
$params = array();
//----------------------------------------------------------------------
//-- Where
//----------------------------------------------------------------------
if(!empty($_REQUEST['term'])) 
{
    $where[]  = '(articles_title LIKE ? OR articles_subtitle LIKE ?)';
    $params[] = '%' . $_REQUEST['term'] . '%';
    $params[] = '%' . $_REQUEST['term'] . '%';
}
if(!empty($_REQUEST['article_group_id'])) 
{
    $where[]  = '(FIND_IN_SET(?, article_groups))';
    $params[] = $_REQUEST['article_group_id'];
}
if(!empty($_REQUEST['join_articles_categories_id'])) 
{
    $where[]  = 'join_articles_categories_id = ?';
    $params[] = $_REQUEST['join_articles_categories_id'];
}
$where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';
//------------------------------------------------------------------------------
//-- Order
//------------------------------------------------------------------------------
switch($_REQUEST['order']) {
    case 'articles_newsdate':
        $order = 'articles.articles_newsdate';
    break;
    
    case 'articles_title':
        $order = 'articles.articles_title';
    break;
    
    case 'articles_teaser':
        $order = 'articles.articles_teaser';
    break;
    
    case 'articles_views':
        $order = 'articles.articles_views';
    break;
    
    case 'total_comments':
        $order = 'total_comments';
    break;
    
    default:
        $_REQUEST['order'] = 'articles_newsdate';
        $_REQUEST['by'] = 'DESC';
    case 'articles_newsdate':
        $order = 'articles.articles_order, articles.articles_newsdate';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order . ' ' . $by;

//------------------------------------------------------------------------------
//-- Rows
//------------------------------------------------------------------------------
$articles = $db->fetch_all('SELECT articles.*, COUNT(articles_comments_id) AS total_comments
    FROM articles
    LEFT JOIN articles_comments ON articles_comments.join_articles_id = articles.articles_id
    ' . $where . '
    GROUP BY articles.articles_id
    ORDER BY ' . $orderby . '
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    $params);
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM articles ' . $where, $params);
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/articles/list.php');
//------------------------------------------------------------------------------
//-- Get Notice
//------------------------------------------------------------------------------
$notice = urldecode($_REQUEST['notice']);
?>
