<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

if(!empty($_POST)) 
{
    $data = $_POST;
    if(!empty($data['rss_items_publish_date'])) 
    {
        $data['rss_items_publish_date'] = date('Y-m-d H:i:s', strtotime($data['rss_items_publish_date']));
    }
    else
    {
        $data['rss_items_publish_date'] = date('Y-m-d H:i:s');
    }
    
    $rss_items = new mysqli_db_table('rss_items');
    $item = $rss_items->get($_REQUEST['rss_items_id']);
    
    $errors = array();
    if(empty($data['rss_items_link']))
    {
        $errors['rss_items_link'] = 'Please fill in a link for the item';
    }
    
    if(empty($data['rss_items_title']))
    {
        $errors['rss_items_title'] = 'Please fill in a title for the item';
    }
    
    if(empty($data['rss_items_description']))
    {
        $errors['rss_items_description'] = 'Please fill in a description for the item';
    }
    
    if(empty($errors))
    {
        if($data['rss_items_id'])
        {
            $rss_items = new mysqli_db_table('rss_items');
            $rss_items->update($data, $data['rss_items_id']);
            $rss_items_id = $data['rss_items_id'];
        }
        else
        {
            $rss_items = new mysqli_db_table('rss_items');
            $rss_items->insert($data);
            $rss_items_id = $rss_items->last_id();
        }
        $notice = 'Your rss item has been saved';
    }
    else 
    {
        $notice = 'You have a few errors, please review them below';
    }
    
    unset($data['errors']);
    unset($data['notices']);
    $params = array(
        'rss_items_id' => $rss_items_id,
        'notices' => $notice,
        'errors'  => $errors
    );
    $params = array_merge($data, $params);
    http::redirect(FULLURL . '/admin/rss/items-edit.php?' . http::build_query($params));
}
if($_REQUEST['rss_items_id'])
{
    $rss_items = new mysqli_db_table('rss_items');
    $item = $rss_items->get($_REQUEST['rss_items_id']);
}
else
{
    $item = $_REQUEST;
}

$notice = $_REQUEST['notices'];
$errors = $_REQUEST['errors'];
?>
