<?php
include ACTIONS .'/admin/protect.php';

$carrier = new mysqli_db_table('carriers');
$carrier->delete($_REQUEST['carriers_id']);

http::redirect(FULLURL . '/admin/carriers/list.php?notice=' . urlencode('Your carrier has been removed'));
?>