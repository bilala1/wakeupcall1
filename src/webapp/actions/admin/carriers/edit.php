<?php
include ACTIONS . '/admin/protect.php';

$carriers_table = new mysqli_db_table('carriers');
$carrier = $carriers_table->get($_REQUEST['carriers_id']);

if($_POST){
    $data = $_POST;
    $errors = array();
    
    $validate = new validate($data);
    $validate
        ->setOptional('carriers_id');
    
    $errors = $validate->test();
    
    if(empty($errors)){
        $data['join_members_id'] = 0;
        
        if(empty($data['carriers_id'])){
            //Create new
            $carriers_table->insert($data);
            $carriers_id = $carriers_table->last_id();
            
            $notice = 'carrier added';
        }else{
            //Update
            $carriers_table->update($data, $data['carriers_id']);
            $carriers_id = $data['carriers_id'];
            
            $notice = 'carrier updated';
        }
        
        http::redirect(BASEURL . '/admin/carriers/edit.php?', array(
            'carriers_id' => $carriers_id,
            'notice'  => $notice
        ));
    }else{
        //so user input is not lost
        $carrier = array_merge((array)$carrier, $data);
    }
}
?>