<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

//----------------------------------------------------------------------
//-- Pagination Settings
//----------------------------------------------------------------------
$paging = new paging(100);

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
list($wheres, $params) = $db->build_wheres_params('carriers', $_REQUEST);

$wheres[] = 'join_members_id = 0';

//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
$orderby = $db->orderby('carriers_name');

//----------------------------------------------------------------------
//-- List
//----------------------------------------------------------------------
$carriers = $db->fetch_all('
    SELECT SQL_CALC_FOUND_ROWS *
    FROM carriers'.
    strings::where($wheres) .
    $orderby .
    $paging->get_limit(),
    $params
);

$paging->set_total($db->found_rows());

?>
