<?php

if (isset($_REQUEST['id'])) {
	$db = mysqli_db::init();
	$db->query('DELETE FROM discount_codes WHERE discount_codes_id = ?', array($_REQUEST['id']));
	http::redirect('list.php?msg=Discount+Code+Deleted');
} else {
	http::redirect('list.php');
}

?>