<?php
include_once ACTIONS . '/admin/protect.php';
$page_title = 'Admin - Coupons';
$page_name = 'coupons';
$sub_name = 'history';

$db = mysqli_db::init();

if (!empty($_REQUEST['id'])) {
	$db = mysqli_db::init();
	$code = $db->fetch_one('SELECT * FROM discount_codes WHERE discount_codes_id = ?', array($_REQUEST['id']));
	if ($code['discount_codes_amount'] > 0) {
		$code['value'] = '$' . $code['discount_codes_amount'];
	} else {
		$code['value'] = $code['discount_codes_percent'] . '%';
	}
}

$code_type_names = $db->fetch_all('SELECT discount_code_types_type FROM discount_code_types');
foreach($code_type_names as $code_type_name) {
    $code_types[$code_type_name["discount_code_types_type"]] = $code_type_name["discount_code_types_type"];
}

if (!empty($_POST)) {
	$code = $_POST;
	$errors = array();
	if (empty($code['discount_codes_code'])) {
		$errors['discount_codes_code'] = 'Please enter a code';
	}
	if (empty($code['value'])) {
        if($code['discount_codes_type'] != 'trial') {
		    $errors['value'] = 'Please enter a value';
        }
	} else {
		if (preg_match('/^\$[0-9\.]+$/', trim($code['value']))) {
			$code['discount_codes_amount'] = str_replace('$', '', $code['value']);
			$code['discount_codes_percent'] = '';
			if ($code['discount_codes_amount'] < 0) {
				$errors['value'] = 'Please enter an amount greater than 0';
			}
		} else if (preg_match('/^[0-9\.]+%$/', trim($code['value']))) {
			$code['discount_codes_percent'] = str_replace('%', '', $code['value']);
			$code['discount_codes_amount'] = '';
			if ($code['discount_codes_percent'] > 100 || $code['discount_codes_percent'] < 0) {
				$errors['value'] = 'Please enter a percent between 0 and 100';
			}
		} else {
			$errors['value'] = 'Please enter in the format "$xx.xx" or "xx%"';
		}
	}
	if (count($errors) == 0) {
		if (!empty($code['discount_codes_start'])) {
			$code['discount_codes_start'] = date_convert($code['discount_codes_start']);
		}
		if (!empty($code['discount_codes_end'])) {
			$code['discount_codes_end'] = date_convert($code['discount_codes_end']);
		}		
		$table = new mysqli_db_table('discount_codes');
		if (isset($_REQUEST['id'])) {
			$table->update($code, $_REQUEST['id']);
			http::redirect('list.php?notice=Discount+Code+Updated');
			exit;
		} else {
			$table->insert($code);
			http::redirect('list.php?notice=Discount+Code+Added');
			exit;
		}
		
	}
}

// m[m]/d[d]/yyyy to yyyy-mm-dd
function date_convert($date) {
	$date = explode('/', $date);
	return "{$date[2]}-{$date[0]}-{$date[1]}";
}
?>