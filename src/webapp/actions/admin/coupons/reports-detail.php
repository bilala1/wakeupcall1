<?php
$page_title = 'Admin | Coupons';
$page_name = 'coupons';
$sub_name = 'reports';

if (empty($_REQUEST['id'])) {
	http::redirect('reports.php');
	exit;
}

$db = mysqli_db::init();
$codes = $db->fetch_all('
	SELECT discount_codes.*, bills.*, members.*, accounts_name, DATE_FORMAT(bills.bills_datetime, "%m/%d/%Y") AS bills_datetime_formatted FROM discount_codes
	LEFT JOIN bills ON (discount_codes_id = join_discount_codes_id)
	LEFT JOIN members ON (join_members_id = members_id)
	LEFT JOIN accounts ON (members_id = accounts.join_members_id)
	WHERE discount_codes_id = ?',
	array($_REQUEST['id'])
);

//echo $db->debug();
