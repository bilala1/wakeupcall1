<?php

$page_title = 'Admin | Reports';
$page_name = 'coupons';
$sub_name = 'reports';

$db = mysqli_db::init();

if (!empty($_REQUEST['order'])) {
	$orderby = strings::orderby($_REQUEST['order']);
} else {
	$orderby = 'discount_codes_id ASC';
}

$codes = $db->fetch_all('
	SELECT discount_codes_id, discount_codes_code, 
	COUNT(bills_id) AS discount_codes_count
	FROM discount_codes
	LEFT JOIN bills ON (discount_codes_id = join_discount_codes_id)
	GROUP BY discount_codes_id ORDER BY ' . $orderby
);

?>