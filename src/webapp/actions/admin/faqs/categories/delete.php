<?php

$db = mysqli_db::init();

$children = $db->fetch_singlet('SELECT COUNT(*) FROM faqs_categories WHERE join_faqs_categories_id = ?', array($_REQUEST['faqs_categories_id']));
$faqs = $db->fetch_singlet('SELECT COUNT(*) FROM faqs WHERE join_faqs_categories_id = ?', array($_REQUEST['faqs_categories_id']));

if ($children == 0 && $faqs == 0) {
	$db->query('DELETE FROM faqs_categories WHERE faqs_categories_id = ?', array($_REQUEST['faqs_categories_id']));
	$notice = 'FAQ Category successfully deleted.';
} else {
	$notice = 'The FAQ Category was not deleted because it contains sub-categories or FAQs.';
}

http::redirect(FULLURL . '/admin/faqs/categories/list.php', array('notice' => $notice));

?>
