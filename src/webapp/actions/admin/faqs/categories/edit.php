<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$categories = $db->fetch_all('SELECT * FROM faqs_categories WHERE faqs_categories_id != ?', array((int) $_REQUEST['faqs_categories_id']));
if($_REQUEST['faqs_categories_id']){
    $category   = $db->fetch_one('SELECT * FROM faqs_categories WHERE faqs_categories_id = ?', array($_REQUEST['faqs_categories_id']));
}

if(!$category and $_REQUEST['join_faqs_categories_id']){
    $category['join_faqs_categories_id'] = $_REQUEST['join_faqs_categories_id'];
}

if($_POST){
    $faqs_categories = new mysqli_db_table('faqs_categories');
    $data = $_POST;
    
    $validate = new validate($data);
    $validate->setOptional('join_faqs_categories_id');
    $validate->setOptional('faqs_categories_id');
    
    $errors = $validate->test();
    
    if(!$errors){
        if($_REQUEST['faqs_categories_id']){
            $faqs_categories->update($data, $category['faqs_categories_id']);
            $notice = 'Category Updated';
            $faqs_categories_id = $category['faqs_categories_id'];
        }
        else {
            $faqs_categories->insert($data);
            $notice = 'Category Created';
            $faqs_categories_id = $faqs_categories->last_id();
        }
        
        http::redirect(BASEURL .'/admin/faqs/categories/edit.php?'.http_build_query(array(
            'notice' => $notice,
            'faqs_categories_id' => $faqs_categories_id
        )));
    }else{
        //so user input is not lost
        $category = array_merge((array)$category, (array)$data);
    }
}

?>
