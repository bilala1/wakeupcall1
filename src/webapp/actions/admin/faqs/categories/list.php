<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$categories = faqs_categories::get_categories_tree();


$have_children = array();
foreach ($categories as &$category) {
	if ($category['num_faqs'] > 0 || in_array($category['faqs_categories_id'],array(8,9,17))) {
		$category['cannot_delete'] = true;
	}

	if ($category['join_faqs_categories_id'] !== 0) {
		$have_children[] = $category['join_faqs_categories_id'];
	}
	unset($category);
}

foreach ($categories as &$category) {
	if (in_array($category['faqs_categories_id'], $have_children)) {
		$category['cannot_delete'] = true;
	}
	unset($category);
}


?>
