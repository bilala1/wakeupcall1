<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$faq = $db->fetch_one('SELECT * FROM faqs WHERE faqs_id = ?', array($_REQUEST['faqs_id']));
$categories = faqs_categories::get_categories_tree(0);

if($_POST){

    $faqs = new mysqli_db_table('faqs');
    $data = $_POST;
    
    $validate = new validate($data);
    $validate->setOptional('faqs_keywords');
    $validate->setOptional('faqs_id');
    $validate->setOptional('faqs_published');
    
    $errors = $validate->test();
    
    if(!$errors){        
        $data['faqs_keywords'] = explode(',', $data['faqs_keywords']);
        $data['faqs_keywords'] = array_unique($data['faqs_keywords']);
        $data['faqs_keywords'] = implode(',', $data['faqs_keywords']); // These aligned perfectly... weird

        if($faq){            
            if($data['faqs_answer'] != $faq['faqs_answer']){
                $data['faqs_answer_datetime'] = SQL('NOW()');
            }
        
            $faqs->update($data, $faq['faqs_id']);
            $faqs_id = $faq['faqs_id'];
            $notice = 'FAQ updated';
        }    
        else {
            $data['faqs_question_datetime'] = SQL('NOW()');
            $data['faqs_answer_datetime'] = SQL('NOW()');
            
            $faqs->insert($data);
            $notice = 'FAQ added';
            $faqs_id = $faqs->last_id();
        }
        
        http::redirect(BASEURL .'/admin/faqs/edit.php?'.http_build_query(array(
            'faqs_id' => $faqs_id,
            'notice'  => $notice
        )));
    }else{     
        //so user input is not lost
        $faq = array_merge((array)$faq, (array)$data);
    }
}

?>
