<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

//if re-ordering
if($_POST){
    $data = $_POST;
    
    $faqs_table = new mysqli_db_table('faqs');
    
    foreach($data['faqs_orders'] as $faqs_id => $faqs_order){
        $faqs_table->update(array('faqs_order' => $faqs_order), $faqs_id);
    }
    
    unset($data['faqs_orders']);
    
    $queries = array_merge($data, array('notice' => 'the FAQ\'s have been re-ordered'));
    
    http::redirect('list.php', $queries);
}


$where = $params = array();

if ($_GET['category'] && !empty($_GET['category'])) {
	$where[] = 'f.join_faqs_categories_id = ?';
	$params[] = $_GET['category'];
}

if($_GET['q'] && !empty($_GET['q'])){
    $where  []= '(faqs_question LIKE ? OR faqs_answer LIKE ?)';
    $params []= '%' . $_GET['q'] . '%';
    $params []= '%' . $_GET['q'] . '%';
}

if($_GET['submitted']){
    $where []= 'm.members_id IS NOT NULL';
}

if($_GET['published']){
    $where []= 'f.faqs_published = 1';
}


switch($_REQUEST['order']){
    default:
    case "faqs_order":
        $order = 'faqs_order';
        $_REQUEST['by'] = 'ASC';
    break;
    
    case "faqs_question_datetime":
        $order = 'faqs_question_datetime';
    break;

    case "faqs_answer_datetime":
        $order = 'faqs_answer_datetime';
    break;

    case "faqs_categories_name":
        $order = 'faqs_categories_name';
    break;

    case "member":
        $order = 'members_lastname';
    break;

    case "faqs_question":
        $order = 'faqs_question';
    break;

    case "faqs_published":
        $order = 'faqs_published';
    break;
}

$orderby = strings::orderby($order);

$faqs = $db->fetch_all('
    SELECT *
    FROM faqs AS f
    LEFT JOIN members AS m ON m.members_id = f.join_members_id
    LEFT JOIN faqs_categories as fc on fc.faqs_categories_id = f.join_faqs_categories_id
    '.strings::where($where).'
    ORDER BY '.$orderby,
    $params
);

//pre($db->debug());

?>
