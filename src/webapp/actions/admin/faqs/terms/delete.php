<?php

include ACTIONS .'/admin/protect.php';

$db = mysqli_db::init();

if(preg_match('/^[0-9]{1,10}$/',$_REQUEST['definitions_id'])){
    
    $db->query('DELETE FROM definitions WHERE definitions_id = ?', array($_REQUEST['definitions_id']));
    $notice = 'Definition was deleted';
}
else {
    $notice = 'An error occured';
}
http::redirect(BASEURL .'/admin/faqs/terms/list.php?'.http_build_query(array(
    'notice' => $notice
)));


?>