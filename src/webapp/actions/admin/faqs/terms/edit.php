<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$term = $db->fetch_one('SELECT * FROM definitions WHERE definitions_id = ?', array($_REQUEST['definitions_id']));

if($_POST){
    $data  = $_POST;
    $definitions = new mysqli_db_table('definitions');
    
    $validate = new validate($data);
    $validate->setOptional('definitions_id');
    
    $errors = $validate->test();
    
    if(!$errors){
        if($term){
            $definitions->update($data,$data['definitions_id']);
            $notice = 'Definition updated';
            $definitions_id = $term['definitions_id'];

        }
        else {
            $definitions->insert($data);
            $notice = 'Definition added';
            $definitions_id = $definitions->last_id();
        }
        
        http::redirect(BASEURL .'/admin/faqs/terms/edit.php?'.http_build_query(array(
            'definitions_id' => $definitions_id,
            'notice' => $notice
        )));
    }
    else {
        $term = $data;
    }
}

?>
