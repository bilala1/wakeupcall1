<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

//----------------------------------------------------------------------
//-- Pagination Settings
//----------------------------------------------------------------------
$paging = new paging(20,$_REQUEST['page']);
$paging->set_firstlast(true);


//------------------------------------------------------------------------------
//-- WHERE
//------------------------------------------------------------------------------
$where = $params = array();
if($_REQUEST['search_string']){
    if(preg_match('/^[a-zA-Z0-9]+$/',$_REQUEST['search_string'])){
        $where  []= '(definitions_word LIKE ("%'.$_REQUEST['search_string'].'%") OR definitions_description LIKE ("%'.$_REQUEST['search_string'].'%"))';
    }
    else{
        $notice = 'ERROR: You must use only alphanumeric characters';
    }
}
if($_REQUEST['pending']){
    $where[] = 'definitions_pending = 1';
}

//------------------------------------------------------------------------------
//-- Order By
//------------------------------------------------------------------------------
switch($_REQUEST['order']){
    default:
    case "definitions_description":
        $order = 'definitions_description';
    break;
    
    case "definitions_word":
        $order = 'definitions_word';
    break;

    case "members_lastname":
        $order = 'members_lastname';
    break;

    case "definitions_requested_datetime":
        $order = 'definitions_requested_datetime';
    break;
}
$orderby = strings::orderby($order);

//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$paging->set_total($db->fetch_singlet('SELECT COUNT(*)
    FROM definitions '.strings::where($where)));

//------------------------------------------------------------------------------
//-- Get Definitions
//------------------------------------------------------------------------------
$definitions  = $db->fetch_all('SELECT * 
                                FROM definitions AS d
                                LEFT JOIN members AS m ON (d.join_members_id = m.members_id)
                                '.strings::where($where).' ORDER BY '.$orderby.' '.$paging->get_limit());
#echo $db->debug();
?>
