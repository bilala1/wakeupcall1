<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

$db->query('DELETE FROM bad_words WHERE bad_words_id = ?', array($_REQUEST['bad_words_id']));

http::redirect(FULLURL . '/admin/forums/badwords/list.php', array('notice' => 'Bad word has been deleted.'));

?>