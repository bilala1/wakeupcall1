<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

if (!empty($_REQUEST['bad_words_id'])) {
	$bad_word = $db->fetch_one('SELECT * FROM bad_words WHERE bad_words_id = ?', array($_REQUEST['bad_words_id']));
}

if ($_POST) {
	$errors = array();
	$data = $_POST;

	$validate = new validate($data);

	$errors = $validate
				->setUnique('bad_words_word', 'bad_words', 'Bad Word')
				->test();

	if (empty($errors)) {
		$bad_words_table = new mysqli_db_table('bad_words');
		if (isset($bad_word)) {
			$bad_words_table->update($data, $bad_word['bad_words_id']);
			$notice = "Bad word has been updated.";
		} else {
			$bad_words_table->insert($data);
			$notice = "Bad word has been saved.";
		}
		http::redirect(FULLURL . '/admin/forums/badwords/list.php', array('notice' => $notice));
	}
}

?>