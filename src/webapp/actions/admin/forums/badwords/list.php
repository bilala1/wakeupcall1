<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

$wheres = $params = array();

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 15;
$max_pages = 5; // only should 5 pages left or right

if (!empty($_REQUEST['term'])) {
	$wheres[] = 'bad_words_word LIKE ?';
	$params[] = '%' . $_REQUEST['term'] . '%';
}

$where = strings::where($wheres);

$bad_words = $db->fetch_all('
	SELECT * FROM bad_words
	'.$where.'
	ORDER BY bad_words_word ASC
	LIMIT ' . ($current_page * $per_page) . ', ' . $per_page.'
', $params
);

//------------------------------------------------------------------------------
//-- Get Total Words
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM bad_words ' . $where, $params);
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/admin/forums/badwords/list.php');

?>