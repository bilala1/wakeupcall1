<?php
include_once ACTIONS . '/admin/protect.php';

$posts_id = $_REQUEST['posts_id'];
$db = mysqli_db::init();

//reset last forum post
$topics_id = $db->fetch_singlet('SELECT join_topics_id FROM forums_posts WHERE posts_id = ?', array($posts_id));

forums::delete_post($posts_id);
$notice = 'Forum post deleted';

$last_posts_id = $db->fetch_singlet('SELECT MAX(posts_id) FROM forums_posts WHERE join_topics_id = ?', array($topics_id));
$db->query('UPDATE forums_topics SET topics_last_posts_id = ? WHERE topics_id = ?', array($last_posts_id, $topics_id));

http::redirect(BASEURL .'/admin/forums/view-topic.php', array('topics_id' => $topics_id, 'notice' => 'The post has been deleted'));

?>
