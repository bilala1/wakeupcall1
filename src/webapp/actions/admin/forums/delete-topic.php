<?php

include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

$topics_id = $_REQUEST['topics_id'];

$topic = $db->fetch_one('SELECT * FROM forums_topics WHERE topics_id = ?', array($topics_id));

// Go through the posts and delete the files for them as well...
$posts = $db->fetch_all('SELECT * FROM forums_posts WHERE join_topics_id = ?', array($topics_id));

foreach($posts as $post){
    $files = $db->fetch_singlets('SELECT forums_posts_files_filename FROM forums_posts_files WHERE join_posts_id = ?', array($post['posts_id']));
    
    foreach($files as $file){
        @unlink(SITE_PATH .'/data/'.$file);
    }
    
    $db->query('DELETE FROM forums_posts_files WHERE join_posts_id = ?', array($post['posts_id']));
    $db->query('DELETE FROM forums_posts_reported WHERE join_posts_id = ?', array($post['posts_id']));
}

$db->query('DELETE FROM forums_posts WHERE join_topics_id = ?', array($topics_id));
$db->query('DELETE FROM forums_topics WHERE topics_id = ?', array($topics_id));

http::redirect(BASEURL .'/admin/forums/view-forum.php?forums_id='.$topic['join_forums_id']);

?>
