<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

//first delete child forums
$children_ids = $db->fetch_singlets('select forums_id from forums where join_forums_id_parent = ?', array($_REQUEST['forums_id']));
foreach($children_ids as $child_id){
    framework::call_action('admin/forums/delete', array('forums_id' => $child_id));
}

//get topics in forum
$topic_ids = $db->fetch_singlets('select topics_id from forums_topics where join_forums_id = ?', array($_REQUEST['forums_id']));

//delete posts in each topic
$db->query('delete from forums_posts where join_topics_id in (' . $db->filter_in($topic_ids) . ')');

//delete topics
$db->query('delete from forums_topics where join_forums_id = ?', array($_REQUEST['forums_id']));

//delete forum
$db->query('delete from forums where forums_id = ?', array($_REQUEST['forums_id']));

http::redirect(FULLURL . '/admin/forums/list.php?notice=' . urlencode('Your forum has been removed'));
?>
