<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

$forums = new forums;
$groups_x_forums = new mysqli_db_table('forums_groups_x_forums');

if($_REQUEST['forums_id']){
    $forum = $forums->get($_REQUEST['forums_id']);
    
    $forums->sync_groups($_REQUEST['forums_id']);
    
    $groups = $forums->get_groups($_REQUEST['forums_id']);
}else{
    //default permission checkboxes for new forum
    $forum['forums_default_permission_view'] = 1;
    $forum['forums_default_permission_reply'] = 1;
    $forum['forums_default_permission_new_topic'] = 1;
    $forum['forums_topic_approval'] = 1;
    $forum['forums_post_approval'] = 1;
    
    $groups = groups::get_groups();
    foreach($groups as &$group){
        $group['forums_groups_x_forums_permission_view'] = $forum['forums_default_permission_view'];
        $group['forums_groups_x_forums_permission_reply'] = $forum['forums_default_permission_reply'];
        $group['forums_groups_x_forums_permission_new_topic'] = $forum['forums_default_permission_new_topic'];
    }
    unset($group);
}

if($_POST){
    $data = $_POST;
    
    foreach($groups as &$group){
        $id = $group['groups_id'];
        
        $group['forums_groups_x_forums_permission_view'] = $data['forums_groups_x_forums_permission_view'][$id];
        $group['forums_groups_x_forums_permission_reply'] = $data['forums_groups_x_forums_permission_reply'][$id];
        $group['forums_groups_x_forums_permission_new_topic'] = $data['forums_groups_x_forums_permission_new_topic'][$id];
    }
    unset($group);
    
    $data['parent_id'] = $data['parent_id'] ? $data['parent_id'] : 0;
    
    if($forum['forums_id']){ // Editing...
        $forums->update($data, $forum['forums_id']);
        $forums_id = $forum['forums_id'];
        
        //update group permissions
        foreach($groups as $group){
            groups::update_permission($forums_id,$group['groups_id'],$group);
        }
        
        $notice = 'Your forum has been updated';
    }
    else {
        $forums->insert(array(
            'join_forums_id_parent' => 0, //$data['join_forums_id_parent']
            'forums_name' => $data['forums_name'],
            'forums_createdate' => SQL('UTC_TIMESTAMP()'),
            'forums_desc' => $data['forums_desc'],
            'forums_public_permission_view' => $data['forums_public_permission_view']
        ));
        $forums_id = $forums->last_id();
        
        //create group permissions
        foreach($groups as $group){
            $group['join_groups_id'] = $group['groups_id'];
            $group['join_forums_id'] = $forums_id;
            $groups_x_forums->insert($group);
        }
        
        $notice = 'Your forum has been added';
    }

    http::redirect(BASEURL .'/admin/forums/edit.php?forums_id='.$forums_id.'&notice='.urlencode($notice));
}

$forum_list = html::db_to_select_array(forums::get_all_forums(),'forums_id','forums_name','-none-');
if($_REQUEST['forums_id']){
    unset($forum_list[$_REQUEST['forums_id']]);
}
?>
