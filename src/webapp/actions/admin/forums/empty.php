<?php
/*************
 *
 *  This file deletes all topics and posts for a given forum,
 *  thereby "emptying" the forum.
 *
 *************/

$db = mysqli_db::init();

if(isset($_REQUEST['forums_id'])) {
    $forums_id = $_REQUEST['forums_id'];
    
    $db->query("DELETE ft.*,fp.*,fpf.*,fpr.* FROM forums_topics AS ft
                LEFT JOIN forums_posts AS fp ON fp.join_topics_id = ft.topics_id
                LEFT JOIN forums_posts_files AS fpf ON fpf.join_posts_id = fp.posts_id
                LEFT JOIN forums_posts_reported AS fpr ON fpr.join_posts_id = fp.posts_id
                WHERE ft.join_forums_id = ?",
                array($forums_id));
    #echo $db->debug();exit;
                
    http::redirect(FULLURL . '/admin/forums/list.php?notice=' . urlencode('The forum has been emptied.'));
}
?>
