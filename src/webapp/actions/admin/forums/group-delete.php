<?php
include_once 'protect.php';

$forums_groups = new mysqli_db_table('forums_groups');
$forums_groups->delete($_REQUEST['groups_id']);

http::redirect(FULLURL . '/admin/forums/groups.php?notice=' . urlencode('Your group has been removed'));
?>
