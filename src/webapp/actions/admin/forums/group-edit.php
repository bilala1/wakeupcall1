<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

if($_REQUEST['groups_id']){
    $group = groups::by_id($_REQUEST['groups_id']);
    //$members = members::by_group($_REQUEST['groups_id']);
    
    groups::sync_forums($_REQUEST['groups_id']);
    
    $forums = groups::get_forums_levels_permissions(0,$_REQUEST['groups_id']);
}else{
    $forums_model = new forums;
    $forums = $forums_model->get_forums_levels(0);
    foreach($forums as &$forum){
        $forum['forums_groups_x_forums_permission_view'] = $forum['forums_default_permission_view'];
        $forum['forums_groups_x_forums_permission_reply'] = $forum['forums_default_permission_reply'];
        $forum['forums_groups_x_forums_permission_new_topic'] = $forum['forums_default_permission_new_topic'];
    }
    unset($forum);
}

if($_POST){
    $data = $_POST;
    
    //strings::pre_dump($data); exit;
    
    $groups_table = new mysqli_db_table('forums_groups');
    $groups_x_forums = new mysqli_db_table('forums_groups_x_forums');
    
    $group_details = array(
        'groups_name' => $data['groups_name'],
        'groups_desc' => $data['groups_desc']);
    
    foreach($forums as &$forum){
        $id = $forum['forums_id'];
        
        $forum['forums_groups_x_forums_permission_view'] = $data['forums_groups_x_forums_permission_view'][$id];
        $forum['forums_groups_x_forums_permission_reply'] = $data['forums_groups_x_forums_permission_reply'][$id];
        $forum['forums_groups_x_forums_permission_new_topic'] = $data['forums_groups_x_forums_permission_new_topic'][$id];
    }
    unset($forum);
    
    if($data['groups_id'] == 0){
        //add new group
        $group_details['groups_datetime'] = SQL('UTC_TIMESTAMP()');
        $groups_table->insert($group_details);
        $groups_id = $groups_table->last_id();
        
        //add group permissions
        foreach($forums as $forum){
            $forum['join_groups_id'] = $groups_id;
            $forum['join_forums_id'] = $forum['forums_id'];
            $groups_x_forums->insert($forum);
        }
        
        $notice = 'This group has been added';
    }else{
        //update group
        $groups_table->update($group_details, $data['groups_id']);
        $groups_id = $data['groups_id'];
        
        //update group permissions
        foreach($forums as $forum){
            groups::update_permission($forum['forums_id'],$groups_id,$forum);
        }
        
        $notice = 'This group has been updated';
    }

    http::redirect(BASEURL .'/admin/forums/group-edit.php?groups_id='.$groups_id.'&notice='.urlencode($notice));
}

?>
