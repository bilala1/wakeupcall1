<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Where
//------------------------------------------------------------------------------
$where  = array();
$params = array();

switch($_REQUEST['order']){
    default:
        $_REQUEST['order'] = 'groups_name';
        $_REQUEST['by'] = 'ASC';
    case "groups_name":
        $order = 'groups_name';
    break;

    case "groups_datetime":
        $order = 'groups_datetime';
    break;

    case "groups_members":
        $order = 'groups_members';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order .' '. $by;

$groups = $db->fetch_all('SELECT g.*, COUNT(DISTINCT gxg.join_members_id) AS groups_members
    FROM forums_groups AS g
    LEFT JOIN forums_groups_x_members AS gxg ON gxg.join_groups_id = g.groups_id
    '.strings::where($where).'
    GROUP BY g.groups_id
    ORDER BY '.$orderby, $params);
?>
