<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Where
//------------------------------------------------------------------------------
$where  = array();
$params = array();

switch($_REQUEST['order']){
    default:
        $_REQUEST['order'] = 'forums_name';
        $_REQUEST['by'] = 'ASC';
    case "forums_name":
        $order = 'f.forums_name';
    break;

    case "forums_date":
        $order = 'f.forums_createdate';
    break;

    case "forums_topics":
        $order = 'forums_topics';
    break;
}

$where  []= 'f.join_forums_id_parent = ?';
$params []= (int) $_REQUEST['parent_id'];

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order .' '. $by;

/*$forums = $db->fetch_all('SELECT f.*, COUNT(DISTINCT t.topics_id) AS forums_topics, COUNT(DISTINCT f2.forums_id) AS children FROM forums AS f
    LEFT JOIN forums_topics AS t ON t.join_forums_id = f.forums_id
    LEFT JOIN forums AS f2 ON f2.join_forums_id_parent = f.forums_id
    '.strings::where($where).'
    GROUP BY f.forums_id
    ORDER BY '.$orderby, $params);*/

$forums = forums::get_forums_levels((int) $_REQUEST['parent_id'], 0, $orderby);

$forum_model = new forums;
$forum_path = $_REQUEST['parent_id'] ? $forum_model->build_trail($forum_model->get_forum_breadcrumb_admin($_REQUEST['parent_id'])) : '';
?>
