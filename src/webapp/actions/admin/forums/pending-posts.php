<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

//------------------------------------------------------------------------------
//-- Where
//------------------------------------------------------------------------------
$where  = array();
$params = array();

switch($_REQUEST['order']){
    case "members_email":
        $order = 'members_email';
    break;
    
    case "topics_title":
        $order = 'topics_title';
    break;
    
    default:
        $_REQUEST['order'] = 'post_date';
        $_REQUEST['by'] = 'ASC';
    case "post_date":
        $order = 'posts_postdate';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order .' '. $by;

$posts = $db->fetch_all('
    SELECT *
    FROM forums_posts
    JOIN members ON members_id = join_members_id
    JOIN forums_topics ON topics_id = join_topics_id
    WHERE
        posts_approved = 0 and
        topics_approved = 1
    ORDER BY '.$orderby);
?>
