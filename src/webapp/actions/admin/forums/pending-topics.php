<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

//------------------------------------------------------------------------------
//-- Where
//------------------------------------------------------------------------------
$where  = array();
$params = array();

switch($_REQUEST['order']){
    case "members_email":
        $order = 'members_email';
    break;
    
    case "topics_title":
        $order = 'topics_title';
    break;
    
    default:
        $_REQUEST['order'] = 'topics_createdate';
        $_REQUEST['by'] = 'ASC';
    case "topics_createdate":
        $order = 'topics_createdate';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order .' '. $by;

$topics = $db->fetch_all('
    SELECT *
    FROM forums_topics as ft
    JOIN members ON members_id = ft.join_members_id
    WHERE topics_approved = 0
    ORDER BY '.$orderby);

//group by and order by to get first post of topic
/*$topics = $db->fetch_all('
    SELECT *
    FROM forums_topics as ft
    JOIN members ON members_id = ft.join_members_id
    JOIN forums_posts ON join_topics_id = topics_id
    WHERE topics_approved = 0
    GROUP BY topics_id
    ORDER BY '.$orderby.' posts_postdate');*/
?>
