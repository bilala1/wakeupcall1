<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

$topics_id = $db->fetch_singlet('SELECT join_topics_id FROM forums_posts WHERE posts_id = ?', array($_REQUEST['posts_id']));

switch($_REQUEST['mode']){
    case "approve":
        forums::approve_post($_REQUEST['posts_id']);
        $notice = 'Forum post approved';
        http::redirect(BASEURL .'/admin/forums/pending-posts.php?notice='.urlencode($notice));
    break;
    
    case "hidepost":
        $db->query('UPDATE forums_posts SET posts_hidden = 1 WHERE posts_id = ?', array($_REQUEST['posts_id']));
        $notice = 'Forum post hidden';
    break;
    
    case "deletepost":
        //delete post
        forums::delete_post($_REQUEST['posts_id']);
        $notice = 'Forum post deleted';
        
        //reset last forum post
        $last_posts_id = $db->fetch_singlet('SELECT posts_id FROM forums_posts WHERE join_topics_id = ? ORDER BY posts_postdate desc', array($topics_id));
        $db->query('UPDATE forums_topics SET topics_last_posts_id = ? WHERE topics_id = ?', array($last_posts_id, $topics_id));
    break;
    
    case "suspendmember":
        $members_id = $db->fetch_singlet('SELECT join_members_id FROM forums_posts WHERE posts_id = ?', array($_REQUEST['posts_id']));
        $db->query('UPDATE members SET members_suspended = 1 WHERE members_id = ?', array($members_id));
        $notice = 'Member Suspended';
    break;
}

http::redirect(BASEURL .'/admin/forums/view-topic.php?topics_id='.$topics_id.'&notice='.urlencode($notice));

?>
