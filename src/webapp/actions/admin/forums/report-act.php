<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

switch($_REQUEST['mode']){
    case "ignore":
    
    $db->query('UPDATE forums_posts_reported SET posts_reported_status = "closed" WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    $notice = 'Report ignored';
    
    break;
    
    case "hidepost":
    
    $posts_id = $db->fetch_singlet('SELECT join_posts_id FROM forums_posts_reported WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    $db->query('UPDATE forums_posts SET posts_hidden = 1 WHERE posts_id = ?', array($posts_id));
    $notice = 'Forum post hidden';
    
    break;
    
    case "deletepost":
    
    //delete post
    $posts_id = $db->fetch_singlet('SELECT join_posts_id FROM forums_posts_reported WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    forums::delete_post($posts_id);
    $notice = 'Forum post deleted';
    
    //reset last forum post
    $topics_id = $db->fetch_singlet('SELECT join_topics_id FROM forums_posts_reported WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    $last_posts_id = $db->fetch_singlet('SELECT posts_id FROM forums_posts WHERE join_topics_id = ? ORDER BY posts_postdate desc', array($topics_id));
    $db->query('UPDATE forums_topics SET topics_last_posts_id = ? WHERE topics_id = ?', array($last_posts_id, $topics_id));
    
    $db->query('UPDATE forums_posts_reported SET posts_reported_status = "closed" WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    
    break;
    
    case "suspendmember":
    
    $members_id = $db->fetch_singlet('SELECT join_members_id FROM forums_posts_reported WHERE posts_reported_id = ?', array($_REQUEST['fpr_id']));
    $db->query('UPDATE members SET members_suspended = 1 WHERE members_id = ?', array($members_id));
    $notice = 'Member Suspended';
    
    break;
}

http::redirect(BASEURL .'/admin/forums/reports.php?notice='.urlencode($notice));

?>
