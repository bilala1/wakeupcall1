<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

//------------------------------------------------------------------------------
//-- Where
//------------------------------------------------------------------------------
$where  = array();
$params = array();

switch($_REQUEST['order']){
    case "topics_name":
        $order = 't.topics_title';
    break;

    case "post_date":
        $order = 'fp.posts_postdate';
    break;

    default:
        $_REQUEST['order'] = 'report_date';
        $_REQUEST['by'] = 'DESC';
    case "report_date":
        $order = 'fpr.posts_reported_datetime';
    break;
    
    case "status":
        $order = 'fpr.posts_reported_status';
    break;
    
    case "reason":
        $order = 'rr.report_reasons_name';
    break;
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = $order .' '. $by;

$reports = $db->fetch_all('SELECT fpr.posts_reported_id, fpr.posts_reported_status, t.topics_title, fp.posts_postdate, fpr.posts_reported_datetime, rr.report_reasons_name
    FROM forums_posts_reported AS fpr
    LEFT JOIN forums_posts As fp ON fp.posts_id = fpr.join_posts_id
    LEFT JOIN report_reasons AS rr ON rr.report_reasons_id = fpr.join_report_reasons_id
    LEFT JOIN forums_topics AS t ON t.topics_id = fp.join_topics_id
    WHERE posts_reported_status != "closed"
    GROUP BY fpr.posts_reported_id
    ORDER BY '.$orderby);
?>
