<?php
include_once ACTIONS . '/admin/protect.php';

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5;

$db = mysqli_db::init();

$forums = new forums;

$topics     = $forums->get_topics($_REQUEST['forums_id'], ($current_page * $per_page), $per_page);
$breadcrumb = $forums->build_trail($forums->get_forum_breadcrumb($_REQUEST['forums_id']));
$forum      = $forums->get_forum($_REQUEST['forums_id']);

$total = $forums->get_topics_total($_REQUEST['forums_id']);
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages);

?>
