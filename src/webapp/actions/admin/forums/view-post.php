<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
$forums = new forums;

if($_POST){
    $data = $_POST;
        
    if($data['submit'] == 'Approve Post'){
        http::redirect(BASEURL .'/admin/forums/post-act.php?mode=approve&posts_id='.$data['posts_id']);
    }else{
        //strings::pre_dump($_POST); exit;
        $posts  = new mysqli_db_table('forums_posts');
        
        $enabled = serialize(array(
            'bbcode' => $data['enable_bbcode'],
            'signature' => $data['enable_signature']
        ));
        
        $posts->update(array(
            'posts_modified_join_members_id' => ActiveMemberInfo::GetMemberId(),
            'posts_modified_reason' => $data['posts_modified_reason'],
            'posts_modified_date' => SQL('UTC_TIMESTAMP()'),
            'posts_tags' => implode(',', tags::add_tags($data['posts_tags'])),
            'posts_body' => str_replace('&nbsp;', ' ', $data['posts_body']),
            'posts_enabled' => $enabled,
        ), $data['posts_id']);
        
        http::redirect(BASEURL .'/admin/forums/view-post.php', array('posts_id' => $data['posts_id'], 'notice' => 'Post has been updated'));
    }
}

$post = $db->fetch_one('
    SELECT *
    FROM forums_posts
    JOIN members ON members_id = join_members_id
    WHERE posts_id = ?', array($_REQUEST['posts_id']));
$post['files'] = $forums->get_files($post['posts_id']);

$posts_enabled = unserialize($post['posts_enabled']);
?>
