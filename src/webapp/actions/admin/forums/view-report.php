<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

if($_POST){
    $posts  = new mysqli_db_table('forums_posts');
    $data   = $_POST;
    
    $enabled = serialize(array(
        'bbcode' => $data['enable_bbcode'],
        'signature' => $data['enable_signature']
    ));
    
    $posts->update(array(
        'posts_modified_join_members_id' => ActiveMemberInfo::GetMemberId(),
        'posts_modified_reason' => $data['posts_modified_reason'],
        'posts_modified_date' => SQL('UTC_TIMESTAMP()'),
        'posts_tags' => implode(',', tags::add_tags($data['posts_tags'])),
        'posts_body' => $data['posts_body'],
        'posts_enabled' => $enabled,
    ), $data['posts_id']);
    
    
    http::redirect(BASEURL .'/admin/forums/view-report.php?posts_reported_id='.$data['posts_reported_id'].'&notice='.urlencode('Post has been updated'));
}

$report = $db->fetch_one('SELECT *, m.members_email AS reporter_username, m2.members_email AS reported_username 
    FROM forums_posts_reported AS fpr
        LEFT JOIN forums_posts As fp ON fp.posts_id = fpr.join_posts_id
        LEFT JOIN report_reasons AS rr ON rr.report_reasons_id = fpr.join_report_reasons_id
        LEFT JOIN forums_topics AS t ON t.topics_id = fp.join_topics_id
        LEFT JOIN members AS m ON m.members_id = fpr.join_members_id
        LEFT JOIN members AS m2 ON m2.members_id = fp.join_members_id
            WHERE fpr.posts_reported_id = ?
            GROUP BY fpr.posts_reported_id', array($_REQUEST['posts_reported_id']));
            
if($report['posts_reported_status'] != 'pending' and $report['posts_reported_status'] != 'closed'){
    $db->query('UPDATE forums_posts_reported 
        SET posts_reported_status = ? 
        WHERE posts_reported_id = ?', array('pending', $_REQUEST['posts_reported_id']));
}

$posts_enabled = unserialize($report['posts_enabled']);
?>
