<?php
include_once ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5;

$forums = new forums;

$total = $forums->get_posts_total($_REQUEST['topics_id']);
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages);

if($_REQUEST['action'] == 'approve'){
    forums::approve_topic($_REQUEST['topics_id']);
}

$topic = $forums->get_topic($_REQUEST['topics_id']);
$posts = $forums->get_posts($_REQUEST['topics_id'], ($current_page * $per_page), $per_page, true);

?>
