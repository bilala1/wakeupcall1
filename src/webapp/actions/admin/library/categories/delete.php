<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

if (empty($_REQUEST['library_categories_id'])) {
	http::redirect(HTTP_FULLURL . '/admin');
}

$cats = array($_REQUEST['library_categories_id']);
$subcats = get_subcats($_REQUEST['library_categories_id'], $db);
if (!empty($subcats)) {
	$cats = array_merge($cats, $subcats);
}
$cats = implode(',', $cats);

//Delete categories
$db->query("DELETE FROM library_categories WHERE library_categories_id IN ({$cats})");

$files = $db->fetch_all("
	SELECT documents_id, documents_file FROM documents 
	WHERE join_library_categories_id IN ({$cats})"
);

$delete_list = array();
$file_path = SITE_PATH . '/data/library/';
foreach($files as $file) {
	if (file_exists($file_path . $file['documents_file'])) {
		unlink($file_path . $file['documents_file']); //Delete permissions? may throw a notice if it can't delete
	}
	$delete_list[] = $file['documents_id'];
}

//Delete files / member library records
$db->query("DELETE FROM documents WHERE join_library_categories_id IN ({$cats})");
$db->query('DELETE FROM members_x_documents WHERE join_documents_id IN (' . implode(', ', $delete_list) . ')');
	

http::redirect(HTTP_FULLURL . '/admin/library/categories/list.php');


function get_subcats($parent_id, $db) {
	$output = array();
	$subcats = $db->fetch_singlets('SELECT library_categories_id FROM library_categories WHERE join_library_categories_id = ?', array($parent_id));
	if (!empty($subcats)) {
		foreach($subcats as $subcat) {
			$output[] = $subcat;
			$children = get_subcats($subcat, $db);
			if (!empty($children)) {
				$output = array_merge($output, $children);
			}
		}
		return $output;
	} else {
		return false;
	}
}


?>