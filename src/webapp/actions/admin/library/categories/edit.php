<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$categories = $db->fetch_all('SELECT * FROM library_categories WHERE library_categories_id != ?', array((int) $_REQUEST['library_categories_id']));
if($_REQUEST['library_categories_id']){
    $category   = $db->fetch_one('SELECT * FROM library_categories WHERE library_categories_id = ?', array($_REQUEST['library_categories_id']));
}

if(!$category and $_REQUEST['join_library_categories_id']){
    $category['join_library_categories_id'] = $_REQUEST['join_library_categories_id'];
}

if($_POST){
    $library_categories = new mysqli_db_table('library_categories');
    $data = $_POST;
    
    $validate = new validate($data);
    $validate->setOptional('join_library_categories_id');
    $validate->setOptional('library_categories_id');
    
    $errors = $validate->test();
    
    if(!$errors){
        if($_REQUEST['library_categories_id']){
            $library_categories->update($data, $category['library_categories_id']);
            $notice = 'Category Updated';
            $library_categories_id = $category['library_categories_id'];
        }
        else {
            $library_categories->insert($data);
            $notice = 'Category Created';
            $library_categories_id = $library_categories->last_id();
        }
        
        http::redirect(BASEURL .'/admin/library/categories/edit.php?'.http_build_query(array(
            'notice' => $notice,
            'library_categories_id' => $library_categories_id
        )));
    }else{
        //so user input is not lost
        $category = array_merge((array)$category, (array)$data);
    }
}

?>
