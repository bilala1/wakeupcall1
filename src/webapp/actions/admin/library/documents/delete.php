<?php

use \Services\Services;

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';
if($_REQUEST['documents_id']){
    $documents = new mysqli_db_table('documents');
    $mxd       = new mysqli_db_table('members_x_documents');

    $document  = $documents->get($_REQUEST['documents_id']);

    switch($document['documents_format']) {
        case 'file':
            Services::$fileSystem->deleteFile($document['join_files_id']);
            break;
    }

    $documents->delete($_REQUEST['documents_id']);
    $mxd->where('join_documents_id = '.$db->filter($_REQUEST['documents_id']))->delete();
}
http::redirect(BASEURL .'/admin/library/documents/list.php');

?>
