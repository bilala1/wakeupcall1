<?php
use Services\Services;

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$document   = $db->fetch_one('SELECT * FROM documents left JOIN files ON join_files_id = files_id WHERE documents_id = ?', array($_REQUEST['documents_id']));
$categories = library_categories::get_categories_tree(0);
$documentTypes = array('file'=>'File', 'url'=>'Web Link', 'hr360'=>'HR 360 Link');

if($document) {
    $originalDocFormat = $document['documents_format'];
    switch($originalDocFormat) {
        case 'url':
        case 'hr360':
            $document['documents_link'] = $document['documents_file'];
            break;
    }
}
$errors = array();

if($_POST){
    $documents = new mysqli_db_table('documents');
    $data = $_POST;
    #pre($_FILES);exit;
    $validate = new validate($data);
    $validate->setOptional('documents_id');
    $validate->setOptional('documents_freetrial');
    $validate->setOptional('documents_keywords');
    $validate->setOptional('join_chemicals_id');

    $format = $data['documents_format'];

    $validate
        ->setExtensions('xls','xlt','xlsx','xlb','xlam','xlsb','xlsm','xltm','xlsx',
        'doc','ppt','pdf','docx', 'pptx',
        'jpg','png', 'gif','bmp','txt', 'rtf')
        ->setMimeTypes(
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'application/msword',
            'application/zip',
            'application/vnd.ms-office',
            'text/plain',
            'application/pdf',
            'image/jpeg',
            'image/png',
            'image/bmp',
            'image/gif',
            'application/mspowerpoint',
            'application/vnd.ms-powerpoint',
            'application/x-mspowerpoint',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'application/vnd.ms-powerpoint.template.macroEnabled.12 potm',
            'application/vnd.openxmlformats-officedocument.presentationml.template',
            'application/vnd.ms-powerpoint.addin.macroEnabled.12',
            'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.ms-excel.addin.macroEnabled.12',
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'application/vnd.ms-excel.sheet.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel.template.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'application/vnd.ms-excel',
            'application/vnd.ms-excel.addin.macroEnabled.12',
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'application/vnd.ms-excel.sheet.macroEnabled.12',
            'application/vnd.ms-excel.template.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/x-excel',
            'application/x-msexcel'
        );

    switch($format) {
        case 'url':
        case 'hr360':
            $data['documents_file'] = $data['documents_link'];
            $validate->setOptional('documents_file');
            break;

        case 'file':
            $validate->setOptional('documents_link');
            $files = $_FILES['documents_file'];
            if(is_array($files)) {
                $files = files::fixUploadFileArray($files);
            } else {
                $files = array($files);
            }

            $fileFound = false;
            foreach($files as $file) {
                if(!$document['documents_id'] && $file['size']) {
                    $validate->setFile($file, 'documents_file');
                    $fileFound = true;
                }
            }
            break;
    }
    $errors = $validate->test();

    if($format == 'file' && !$fileFound) {
        $errors['documents_file'] = 'Please upload a file';
    }
    if(!$errors){
        $uploaddir = SITE_PATH . DOCUMENT_DIR;
        
        $data['documents_keywords'] = explode(',', $data['documents_keywords']);
        $data['documents_keywords'] = array_unique($data['documents_keywords']);
        $data['documents_keywords'] = implode(',', $data['documents_keywords']); // These aligned perfectly... weird

        $titles = explode('|',$data['documents_title']);

        if(!$document['documents_id'] or count($files) > 1){
            $data['documents_type'] = 'library';
            if($format == 'file') {
                $counter = 0;
                foreach ($files as $file) {
                    $fileEntry = Services::$fileSystem->storeFile($file, 'documents');

                    $data['documents_title'] = $titles[$counter];
                    $data['join_files_id'] = $fileEntry['files_id'];

                    $counter++;

                    $documents->insert($data);
                    $notice = 'Document(s) added';
                }
            } else {
                $data['documents_datetime'] = sql('NOW()');
                $documents->insert($data);
            }
            http::redirect(BASEURL .'/admin/library/documents/list.php?' . http_build_query(array('notice' => $notice)));
        } else {
            if($document['documents_type'] == 'submitted'){
                $data['documents_type'] = $data['approve_action'] == 'Approve' ? 'library' : 'personal';
            }

            if($format == 'file') {
                $file = $_FILES['documents_file'];
                if ($file['name']) {
                    $fileEntry = Services::$fileSystem->storeFile($file, 'documents');
                    $data['join_files_id'] = $fileEntry['files_id'];
                    $data['documents_datetime'] = sql('NOW()');
                    // delete old doc
                    Services::$fileSystem->deleteFile($document);
                }
            } else {
                if($document['documents_filename'] != $data['documents_filename']) {
                    $data['documents_datetime'] = sql('NOW()');
                }
            }
            $documents->update($data, $_REQUEST['documents_id']);
            $documents_id = $_REQUEST['documents_id'];
            $notice = 'Document updated';

            if($document['documents_status'] == 'active' && $data['documents_status'] == 'inactive') {
                $memberEmails = $db->fetch_singlets(
                    'SELECT members_email FROM members
                      INNER JOIN members_x_documents ON join_members_id = members_id
                      WHERE join_documents_id = ?',
                    array($document['documents_id'])
                );
                foreach($memberEmails as $memberEmail) {
                    mail::send_template(
                        CONCIERGE_EMAIL,
                        $memberEmail,
                        'Document Inactivated - ' . SITE_NAME,
                        'document-inactivated.php',
                        $document
                    );
                }
            }
        }
        
    
        http::redirect(BASEURL .'/admin/library/documents/edit.php?' . http_build_query(array(
            'notice' => $notice,
            'documents_id' => $documents_id
        )));
    }
    else {
        $document = array_merge((array) $document, $data);
    }
}

?>
