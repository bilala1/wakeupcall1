<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
$wheres = array();
$params = array();
$fields = documents::describe();
foreach($fields as $field){
    $field_name = $field['Field'];
    $field_value = $_REQUEST[$field_name];
    if(isset($field_value) && $field_value != ''){
        if(stristr($field_name, 'join') || $field['Type'] == 'tinyint(1)'){
            $wheres[] = $field_name.' = ?';
            $params[] = $field_value;
        }else{
            $wheres[] = $field_name.' like ?';
            $params[] = '%'.$field_value.'%';
        }
    }
}

//custom wheres
if(!empty($_REQUEST['category'])){
    $wheres[] = 'd.join_library_categories_id = ?';
    $params[] = $_REQUEST['category'];
}

$wheres[] = 'documents_type = "library"';

//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
//default order
if(!isset($_REQUEST['order'])){
    $_REQUEST['order'] = 'documents_title';
    $_REQUEST['by'] = 'ASC';
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = ' ORDER BY ' . $db->escape_column($_REQUEST['order']) . ' ' . $by;

//------------------------------------------------------------------------------
//-- Get List
//------------------------------------------------------------------------------
$documents = $db->fetch_all('
    SELECT * 
    FROM documents AS d
    LEFT JOIN files ON join_files_id = files_id
    LEFT JOIN library_categories as lc on lc.library_categories_id = d.join_library_categories_id
    '.strings::where($wheres).'
    GROUP BY d.documents_id
    '.$orderby, 
    $params
);
//echo $db->debug();
?>
