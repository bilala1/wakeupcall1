<?php

require_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

if($_REQUEST['join_member_id'] && $_REQUEST['downloads_type'] && $_REQUEST['join_id']) {
    
    // ----------------------------------------------------------------------------------------
    // -- file info
    // ----------------------------------------------------------------------------------------
    switch($_REQUEST['downloads_type']):
        case 'file':
            $fields = ' doc.documents_title AS file_name, ';
            $left_join = ' LEFT JOIN documents AS doc ON(doc.documents_id = d.join_id) ';
            break;
        case 'msds':
            $fields = ' chem.documents_title AS file_name, ';
            $left_join = ' LEFT JOIN chemicals AS chem ON(chem.chemicals_id = d.join_id) ';            
            break;
        case 'forum file':
            $fields = ' fpf.forums_posts_files_original_filename AS file_name, ';
            $left_join = ' LEFT JOIN forums_posts_files AS fpf ON(fpf.forums_posts_files_id = d.join_id) ';             
            break;
        case 'certificate':
            $fields = ' cert.certificates_name AS file_name, ';
            $left_join = ' LEFT JOIN certificates AS cert ON(cert.certificates_id = d.join_id) ';              
            break;
    endswitch;
    
    $orderby = strings::orderby('downloads_datetime');
    
    $downloads_per_member = $db->fetch_all('SELECT '.$fields.' downloads_datetime 
                                          FROM downloads AS d
                                          LEFT JOIN members AS m ON(d.join_members_id = m.members_id)
                                          '.$left_join.'
                                          WHERE d.join_members_id = ?
                                          AND downloads_type = ?
                                          AND join_id = ? 
                                          ORDER BY '.$orderby.'
                                          ', array($_REQUEST['join_member_id'],$_REQUEST['downloads_type'],$_REQUEST['join_id']));  

    // ----------------------------------------------------------------------------------------
    // -- member info
    // ----------------------------------------------------------------------------------------
    $member = $db->fetch_one('SELECT CONCAT_WS(" ",members_firstname,	members_lastname) AS name, members_type 
                                        FROM members WHERE members_id = ?', array($_REQUEST['join_member_id']));
    switch($member['members_type']):
        case 'hotel':
            //todo test licensed locations
            $corporation = $db->fetch_singlet('SELECT corporations_name FROM corporations AS c
                                               LEFT JOIN licensed_locations AS ll ON(ll.join_corporations_id = c.corporations_id)
                                               WHERE ll.join_members_id = ? ',array($_REQUEST['join_member_id']));
            if($corporation){
                $member['firm']  = $corporation.' >> ';
            }
            $member['firm'] = $member['firm'].'Hotel '.$db->fetch_singlet('SELECT licensed_locations_name FROM licensed_locations WHERE join_members_id = ? ',array($_REQUEST['join_member_id']));
            break;
        case 'corporation':
            $member['firm'] = 'Corporation '.$db->fetch_singlet('SELECT corporations_name FROM corporations WHERE join_members_id = ? ',array($_REQUEST['join_member_id']));
            break;
        case 'staff':
            //todo test licensed locations
            $corporation = $db->fetch_singlet('SELECT corporations_name FROM corporations AS c
                                               LEFT JOIN licensed_locations AS ll ON(h.join_corporations_id = c.corporations_id)
                                               LEFT JOIN staff AS s ON(s.join_licensed_locations_id = ll.licensed_locations_id)
                                               WHERE s.join_members_id = ? ',array($_REQUEST['join_member_id']));
            if($corporation){
                $member['firm']  = $corporation.' >> ';
            }       
            $member['firm'] = $member['firm'].'Hotel '.$db->fetch_singlet('SELECT licensed_locations_name FROM licensed_locations AS ll
                                                           LEFT JOIN staff AS s ON(s.join_licensed_locations_id = ll.licensed_locations_id)
                                                           WHERE s.join_members_id = ? ',array($_REQUEST['join_member_id']));
            break;
    endswitch;
        
}


?>