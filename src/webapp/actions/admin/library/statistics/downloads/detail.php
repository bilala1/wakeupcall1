<?php

include ACTIONS .'/admin/protect.php';

//if no type or join id set, rerirect to list
if(!$_REQUEST['downloads_type'] || !$_REQUEST['join_id']){
    http::redirect('list.php');
}

$db = mysqli_db::init();

$where = $params = array();

if($_REQUEST['date_start']&& $_REQUEST['date_end']){
    $where  []= 'downloads_datetime BETWEEN ? AND ?';
    $params []= date('Y-m-d H:i:s', strtotime($_REQUEST['date_start']));
    $params []= date('Y-m-d H:i:s', strtotime($_REQUEST['date_end']));
}

if($_REQUEST['downloads_type']){
    $where  []= 'downloads_type = ?';
    $params []= $_REQUEST['downloads_type'];
}
if($_REQUEST['join_id']){
    $where  []= 'join_id = ?';
    $params []= $_REQUEST['join_id'];
}
    
switch($_REQUEST['order']){
    case "name":
        $order = 'name';
    break;      
    case "join_members_id":
        $order = 'join_members_id';
    break;
    case "cnt":
        $order = 'cnt';
    break;    
    
    default:
    case "downloads_type":
        $order = 'downloads_type';
    break;
}

$orderby = strings::orderby($order);

//todo test licensed locations
$documents = $db->fetch_all('
    SELECT d.join_members_id, `downloads_type` , `join_id` , COUNT( `join_id` ) AS cnt, c.corporations_name, ll.licensed_locations_name, ll2.licensed_locations_name,
        (IF(c.corporations_name IS NOT NULL,c.corporations_name,IF(ll.licensed_locations_name IS NOT NULL,ll.licensed_locations_name,ll2.licensed_locations_name))) AS name,
        (SELECT documents_title FROM documents WHERE documents_id = join_id AND downloads_type = "file") as file_title,
        (SELECT chemicals_name FROM chemicals WHERE chemicals_id = join_id AND downloads_type = "msds") as chemical_title,
        (SELECT certificates_name FROM certificates WHERE certificates_id = join_id AND downloads_type = "certificate") as certificate_title,
        (SELECT forums_posts_files_original_filename FROM forums_posts_files WHERE forums_posts_files_id = join_id AND downloads_type = "forum file") as forum_post_title
    FROM `downloads` AS d
    LEFT JOIN corporations AS c ON(c.join_members_id = d.join_members_id)
    LEFT JOIN licensed_locations AS ll ON(ll.join_members_id = d.join_members_id)
    LEFT JOIN staff AS s ON(s.join_members_id = d.join_members_id)
    LEFT JOIN licensed_locations AS ll2 ON(ll2.licensed_locations_id = s.join_licensed_locations_id)
    '.strings::where($where).'
    GROUP BY d.join_members_id , `join_id`
    '.$having.'
    ORDER BY '.$orderby, $params
);

//pre($documents);
//pre($db->debug());




?>
