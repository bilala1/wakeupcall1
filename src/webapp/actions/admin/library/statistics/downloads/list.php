<?php

include ACTIONS .'/admin/protect.php';

$db = mysqli_db::init();

$where = $params = array();

if($_REQUEST['date_start']&& $_REQUEST['date_end']){
    $where  []= 'downloads_datetime BETWEEN ? AND ?';
    $params []= date('Y-m-d H:i:s', strtotime($_REQUEST['date_start']));
    $params []= date('Y-m-d H:i:s', strtotime($_REQUEST['date_end']));
}

//set default to display library docs
if(!$_REQUEST['type']){
    $_REQUEST['type'] = 'file';
}

if($_REQUEST['type']){
    if($_REQUEST['type'] != 'all'){
        $where  []= 'downloads_type = ?';
        $params []= $_REQUEST['type'];
    }
}


if($_REQUEST['name']){
    $having = 'HAVING (file_title LIKE ? OR chemical_title LIKE ? OR certificate_title LIKE ? OR forum_post_title LIKE ?)';
    $params []= '%'.$_REQUEST['name'].'%';
    $params []= '%'.$_REQUEST['name'].'%';
    $params []= '%'.$_REQUEST['name'].'%';
    $params []= '%'.$_REQUEST['name'].'%';
}

switch($_REQUEST['order']){
    case "join_id":
        $order = 'join_id';
    break;
    case "cnt":
        $order = 'cnt';
    break;    
    
    default:
    case "downloads_type":
        $order = 'downloads_type';
    break;
}

$orderby = strings::orderby($order);

$documents = $db->fetch_all('
    SELECT `downloads_type` , `join_id` , COUNT( `join_id` ) AS cnt,
        (SELECT documents_title FROM documents WHERE documents_id = join_id AND downloads_type = "file") as file_title,
        (SELECT chemicals_name FROM chemicals WHERE chemicals_id = join_id AND downloads_type = "msds") as chemical_title,
        (SELECT certificates_name FROM certificates WHERE certificates_id = join_id AND downloads_type = "certificate") as certificate_title,
        (SELECT forums_posts_files_original_filename FROM forums_posts_files WHERE forums_posts_files_id = join_id AND downloads_type = "forum file") as forum_post_title
    FROM `downloads`
    '.strings::where($where).'
    GROUP BY `downloads_type` , `join_id`
    '.$having.'
    ORDER BY '.$orderby, $params
);

//pre($db->debug());




?>
