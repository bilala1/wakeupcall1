<?php

$db = mysqli_db::init();

if($_POST){
    $username = $_POST['admins_username'];
    $password = hash('sha256', $_POST['admins_password']);
    
    $admin = $db->fetch_one('SELECT * FROM admins WHERE admins_username = ? AND admins_password = ?', array($username, $password));
    
    if($admin){
        $_SESSION['admins_id']        = $admin['admins_id'];
        $_SESSION['admins_firstname'] = $admin['admins_firstname'];
        $_SESSION['admins_lastname']  = $admin['admins_lastname'];
        
        http::redirect($_POST['redirect']);
    }
    else {
        $errors[] = 'Username and password incorrect.';
    }
}

?>
