<?php
include ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();
$accounts_table = new mysqli_db_table('accounts');
$corporations_table = new mysqli_db_table('corporations');

$accounts_id = $_REQUEST['accounts_id'];
$account = $db->fetch_one('select *,
                             (SELECT COUNT(1) from licensed_locations ll
                              INNER JOIN corporate_locations on join_licensed_locations_id = licensed_locations_id
                              WHERE ll.join_accounts_id = a.accounts_id
                              AND licensed_locations_delete_datetime IS NULL
                              ) as `corporate_office_count`,
                             (SELECT COUNT(1) from licensed_locations ll
                              WHERE NOT EXISTS(SELECT 1 from corporate_locations WHERE join_licensed_locations_id = licensed_locations_id)
                              AND ll.join_accounts_id = a.accounts_id
                              AND licensed_locations_delete_datetime IS NULL
                              ) as `location_count`,
                             (SELECT MAX(bills_datetime + INTERVAL 1 YEAR) FROM bills
                              WHERE bills_type = \'yearly\' AND bills.join_members_id = a.join_members_id
                              ) as `next_bill`
                           from accounts a
                           left join members m on m.members_id = a.join_members_id
                           left join corporations c on a.accounts_id = c.join_accounts_id
                           left join discount_codes dc on a.join_discount_codes_id = dc.discount_codes_id
                           WHERE accounts_id = ? ', array($accounts_id));

$admins = $db->fetch_all('SELECT members_id, members_firstname, members_lastname FROM members
                           WHERE members_type = \'admin\'
                             AND join_accounts_id = ?
                           ORDER BY members_lastname', array($accounts_id));

$bills = $db->fetch_all('select * from bills left join discount_codes on bills.join_discount_codes_id = discount_codes.discount_codes_id where join_members_id = ?', array($account['members_id']));

$childAccounts = accounts::getChildAccounts($account['accounts_id']);

$accountFeatures = $db-> fetch_all(
    'SELECT af.*, COALESCE(acc.accounts_x_account_features_enabled, 1) AS accounts_x_account_features_enabled FROM account_features af
      LEFT JOIN accounts_x_account_features acc ON account_features_id = join_account_features_id AND acc.join_accounts_id = ?;',
    array($accounts_id));

if($account) {
    if($account['accounts_type'] == 'single') {
        $accountTypeOptions = array('single' => 'Single');
    } else {
        $accountTypeOptions = array('multi' => 'Multi (Corporation)', 'parent' => 'Parent');
    }
} else {
    $account = array();
    $accountTypeOptions = array('single' => 'Single', 'multi' => 'Multi (Corporation)', 'parent' => 'Parent');
}

if($_POST){
    $data = $_POST;
    $errors = array();
    
    $validate = new validate($data);
    $validate
        ->setOptional('join_accounts_id_parent')
        ->setOptional('discount_codes_code')
        ;

    if(!$account) {
        $validate
            ->setMinLength('members_password', 6)
            ->setMatch('members_password', 'members_password2')
            ->setUnique('members_email', 'members')
            ->setEmail('members_email')
            ->setMatch('members_email', 'members_email2');
    }
    
    $errors = $validate->test();

    if(count($data['account_features']) != count($accountFeatures)) {
        $errors['account_features'] = 'There was an error processing account features.';
    }
    
    if(empty($errors)){

        if($data['discount_codes_code']) {
            $data['join_discount_codes_id'] = $db->fetch_singlet('select discount_codes_id from discount_codes where discount_codes_code = ? and discount_codes_type = \'billing\'',
                array($data['discount_codes_code']));

            if(!$data['join_discount_codes_id']) {
                $data['join_discount_codes_id'] = null;
            }
        } else {
            $data['join_discount_codes_id'] = null;
        }

        //Update
        if($data['accounts_delete']){ // if checkbox for delete is checked
            if($account['accounts_delete_datetime']){ // if delete date is already set, keep it
                $data['accounts_delete_datetime'] = $account['accounts_delete_datetime'];
            }
            else { // if delete checkbox was checked now, set to current date
                $data['accounts_delete_datetime'] = SQL('NOW()');
            }
        }
        else if($account['accounts_delete_datetime']){ // RENEW: if delete checkbox is not checked, set value to NULL

            //-------------------------------------------------------------------------------
            //-- bill only if renewal was made after scheduled billing date
            //-------------------------------------------------------------------------------
            // get member's for this account
            $members_id = $account['join_members_id'];
            //If the previous yearly fee would have included this hotel, then charge for it now
            $bill_date = $db->fetch_one('SELECT DATE_ADD( bills_datetime, INTERVAL + 1 year ) AS next_bill_date,  bills_datetime AS last_bill_date
                                                  FROM bills
                                                  WHERE join_members_id = ? and bills_type = "yearly"
                                                  ORDER BY last_bill_date DESC limit 1', array($members_id));
            //if their payment date is past due, bill them
            if(strtotime('now') > strtotime($bill_date['next_bill_date'])){

                $bill_description = 'Admin Renewed Yearly subscription: '.$account['accounts_name'].' ('.date(DATE_FORMAT, strtotime($bill_date['next_bill_date'])).' - '.date(DATE_FORMAT, strtotime($bill_date['next_bill_date'].' + 1 year')).')';

                list($approved, $transaction_error) = members::process_payment($members_id, YEARLY_COST, $bill_description);

                if($approved){
                    //Create bill
                    $bills_table = new mysqli_db_table('bills');
                    $bills_table->insert(array(
                        'join_members_id' => $members_id,
                        'bills_amount' => YEARLY_COST,
                        'bills_type' => 'yearly',
                        'bills_description' => $bill_description,
                        'bills_datetime' => $bill_date['next_bill_date'],
                        'bills_paid' => 1
                    ));

                    $notice  = 'Account Updated: account was renewed and billed';
                    // should no longer be marked as deleted
                    $data['accounts_delete_datetime'] = SQL('NULL');
                }
                else { // if payment could not be processed
                    $notice  = 'Account Updated: account could not be renewed because payment could not be processed: '.$transaction_error;
                    $data['accounts_delete_datetime'] = $account['accounts_delete_datetime'];
                }

            }
            else { // if payment period has not expired
                $notice  = 'Account Updated: account was renewed.';
                $data['accounts_delete_datetime'] = SQL('NULL');
            }
            //-------------------------------------------------------------------------------
        }

        if($account) {
            $accounts_table->update($data, $data['accounts_id']);
            $accounts_id = $data['accounts_id'];
            if (array_key_exists('corporations_id', $account)) {
                $corporations_table->update($data, $account['corporations_id']);
            }

            if ($account['accounts_is_locked'] != $data['accounts_is_locked']) {
                members::set_members_status($account['join_members_id'], $data['accounts_is_locked'] == 1 ? 'locked' : 'member');
            }

            if (!$notice) {
                $notice = 'Account updated';
            }
        } else {
            $members_table = new mysqli_db_table('members');
            $notifications_table = new mysqli_db_table('notifications');
            $members_settings_table = new mysqli_db_table('members_settings');
            $forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');

            // Create account
            $member = arrays::filter_keys($data, array(
                'members_firstname',
                'members_lastname',
                'members_title',
                'members_phone',
                'members_fax',
                'members_email'
            ));

            $member = array_merge($member, array(
                'members_status' => 'member',
                'members_datetime' => SQL('NOW()'),
                'members_type' => 'admin',
                'members_verified_code' => '',
                'members_password' => hash('sha256', $data['members_password']),
                'members_billing_type' => 'yearly'
            ));
            $members_table->insert($member);
            $members_id = $members_table->last_id();

            $data['join_members_id'] = $members_id;
            $accounts_id = accounts::create($data);
            members::join_account($members_id, $accounts_id);
            $data['join_accounts_id'] = $accounts_id;

            $notifications_table->insert(array('join_members_id' => $members_id));

            $forums_groups_x_members_table->insert(array(
                'join_groups_id' => 6,      //Message Forum
                'join_members_id' => $members_id
            ));

            // collapsable yellow information boxes
            $members_settings_table->insert(array('join_members_id' => $members_id));

            $bills_table = new mysqli_db_table('bills');
            $bills_table->insert(array(
                'join_members_id' => $members_id,
                'bills_amount' => 0,
                'bills_type' => 'yearly',
                'bills_description' => SITE_NAME . ' Admin create new account',
                'bills_datetime' => SQL('NOW()'),
                'bills_paid' => 1
            ));

            if($data['accounts_type'] != 'single'){
                $data['corporations_name']   =   $data['accounts_name'];
                $data['corporations_address']=   $data['accounts_address'];
                $data['corporations_city']   =   $data['accounts_city'];
                $data['corporations_state']  =   $data['accounts_state'];
                $data['corporations_zip']    =   $data['accounts_zip'];
                $corporations_id = corporations::create($data);

                //create corp location
                $corp_loc = array();
                $corp_loc['join_members_id']           =   $members_id;
                $corp_loc['join_accounts_id']          =   $accounts_id;
                $corp_loc['join_corporations_id']      =   $corporations_id;
                $corp_loc['licensed_locations_name']   =   $data['accounts_name'];
                $corp_loc['licensed_locations_address']=   $data['accounts_address'];
                $corp_loc['licensed_locations_city']   =   $data['accounts_city'];
                $corp_loc['licensed_locations_state']  =   $data['accounts_state'];
                $corp_loc['licensed_locations_zip']    =   $data['accounts_zip'];
                $corp_loc['licensed_locations_active'] =   1;

                $location_id = licensed_locations::create($corp_loc);

                $corp_loc = array();
                $corp_loc['corporate_locations_active']          =   1;
                $corp_loc['join_licensed_locations_id']          =   $location_id;
                corporate_locations::create($corp_loc);

                forums::create_corporate_forum($data['join_members_id']);
            }

            if (!$notice) {
                $notice = 'Account created';
            }
        }

        foreach($accountFeatures as $accountFeature) {
            //feature level unchanged
            if ($accountFeature['accounts_x_account_features_enabled'] ==
                $data['account_features'][$accountFeature['account_features_type']]
            ) {
                continue;
            }

            if (1 == $data['account_features'][$accountFeature['account_features_type']]) {
                //feature level added
                $db->query(
                    'DELETE FROM accounts_x_account_features
                      WHERE join_accounts_id = ?
                        AND join_account_features_id = ?',
                    array($accounts_id, $accountFeature['account_features_id'])
                );
            } else {
                //feature level removed
                $db->query(
                    'INSERT INTO accounts_x_account_features (join_accounts_id, join_account_features_id, accounts_x_account_features_enabled)
                      VALUES (?, ?, 0)',
                    array($accounts_id, $accountFeature['account_features_id'])
                );
            }
        }
        
        http::redirect('edit.php?accounts_id=' . $accounts_id . '&notice=' . urlencode($notice));
    }else{
        //so user input is not lost
        $account = array_merge($account, $data);
    }
}
