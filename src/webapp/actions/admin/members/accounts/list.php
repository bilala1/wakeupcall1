<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$where = $params = array();

switch($_REQUEST['order']){
    default:
    case "name":
        $order = 'a.accounts_name';
        break;

    case "type":
        $order = 'a.accounts_type';
        break;

    case "next_bill":
        $order = 'next_bill';
        break;

    case "num_locations":
        $order = 'a.num_locations';
        break;

    case "parent_accounts_name":
        $order = 'account_parent.accounts_name';
        break;

    case "is_locked":
        $order = 'a.accounts_is_locked';
        break;
}

if(!isset($_REQUEST['by'])) {
    $_REQUEST['by'] = 'ASC';
}

$orderby = strings::orderby($order);

$accounts = $db->fetch_all("
    SELECT a.*, dc.*,
      (select count(1) from licensed_locations
       where join_accounts_id = a.accounts_id
        and licensed_locations_delete_datetime is null
        and not exists(select 1 from corporate_locations where join_licensed_locations_id = licensed_locations_id)) as num_locations,
      (select max(bills_datetime + INTERVAL 1 YEAR) from bills where bills_type = 'yearly' and bills.join_members_id = a.join_members_id) as next_bill,
      account_parent.accounts_name as parent_accounts_name,
      IF(a.accounts_type = 'parent', (SELECT COUNT(1) FROM accounts ca
       WHERE ca.join_accounts_id_parent = a.accounts_id), '') as `num_children`
    FROM accounts AS a
    left join discount_codes dc on a.join_discount_codes_id = dc.discount_codes_id
    left join accounts account_parent on a.join_accounts_id_parent = account_parent.accounts_id
    " . strings::where($where) . "
    GROUP BY a.accounts_id
    ORDER BY " . $orderby,
    $params
);

?>