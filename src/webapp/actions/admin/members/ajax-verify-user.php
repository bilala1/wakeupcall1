<?php

include ACTIONS .'/admin/protect.php';
include LIBS . '/http_response_code.php';
if ( isset($_POST['member_id'])) {
    $members = new mysqli_db_table('members');
    $member = $members->get($_POST['member_id']);
    if ($member ) {
        $members->update(array(
            'members_verified_code' => ''
        ), $member['members_id']);
        return;
    }
}
http_response_code(404);

