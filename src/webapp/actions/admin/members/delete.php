<?php
include_once ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

$members = new mysqli_db_table('members');
if($_REQUEST['members_id'])
{
    if($_REQUEST['members_id'] == FORUM_FORMER_MEMBER){
        //Former Member cannot be deleted
        http::redirect(FULLURL . '/admin/members/list.php?notice=' . urlencode('Cannot delete this member.'));
    }

    members::delete($_REQUEST['members_id']);
    
    http::redirect(FULLURL . '/admin/members/list.php?notice=' . urlencode('The member has been deleted.'));
}


?>
