<?php

include ACTIONS .'/admin/protect.php';
require_once(LIBS . "/stripe_lib/stripe_include.php");

$notifications_table = new mysqli_db_table('notifications');
$members_settings_table = new mysqli_db_table('members_settings');

$db = mysqli_db::init();

$member = $db->fetch_one('SELECT *
        FROM members
        INNER JOIN accounts on join_accounts_id = accounts_id
        WHERE members_id = ?', array($_REQUEST['members_id']));


$member = members::get_customer_credit_card_data($member);

$acc_perms = $db->fetch_all('SELECT members_access_levels_name FROM members_access
                             INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
                             WHERE join_members_id = ?', array($_REQUEST['members_id']));

$loc_perms = $db->fetch_all('SELECT licensed_locations_id, licensed_locations_name, GROUP_CONCAT(members_access_levels_name SEPARATOR \', \') as perms
                             FROM licensed_locations_x_members_access llma
                             INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
                             INNER JOIN licensed_locations ON licensed_locations_id = join_licensed_locations_id
                             WHERE llma.join_members_id = ?
                             GROUP BY licensed_locations_name', array($_REQUEST['members_id']));

//TODO accounts what to do about franchise
$franchise = $db->fetch_one('select f.* from franchises f inner join licensed_locations ll on ll.join_franchises_id = f.franchises_id where ll.licensed_locations_id = ?',
    array($licensed_location['licensed_locations_id']));

$groups_ids = $db->fetch_singlets('SELECT join_groups_id from forums_groups_x_members where join_members_id = ?', array($_REQUEST['members_id']));

$bills = $db->fetch_all('select * from bills left join discount_codes on bills.join_discount_codes_id = discount_codes.discount_codes_id where join_members_id = ?', array($_REQUEST['members_id']));

//from enum field
$statuses = array(
    'free'   => 'Free',
    'member' => 'Member',
    'exempt' => 'Exempt',
    'locked' => 'Locked'
);

//from enum field
$types = array(
    'admin'   => 'Account Admin',
    'user' => 'Regular User'
);

if($_POST){
    $members = new mysqli_db_table('members');
    $data = $_POST;
    
    $validate = new validate($data);
    $validate
        ->setAllOptional()
        ->setEmail('members_email', 'Email');
    
    $errors = $validate->test();
//    pre($data['members_email']); exit;

    if($member && $member['members_email'] !=  $data['members_email']){

        if(!validate::is_unique('members_email', 'members', $data['members_email'])){
            $errors['members_email'] = 'Email is already in use';
        }
        else{
            //Set verification again
            $data['members_verified_code'] = md5(uniqid(rand()));
        }
    }

    if(!$errors){
        if(stristr($data['members_card_num'], '*')){
            unset($data['members_card_num']);
        }
        else {
            if($data['members_card_num']  && !empty($data['members_card_num']))
            {
                try
                {
                    $token = Stripe_Token::create(array( "card" => array( "number" => $data['members_card_num'],
                        //"cvc" => $_POST['members_card_cvc'],
                        "brand" => $data['members_card_type'],
                        "exp_month" => $data['members_card_expire_month'],
                        "exp_year" => $data['members_card_expire_year'],
                        "address_line1" =>  $data['members_billing_addr1'],
                        "address_line2" =>  $data['members_billing_addr2'],
                        "address_city" =>  $data['members_billing_city'],
                        "address_state" =>  $data['members_billing_state'],
                        "address_zip" =>  $data['members_billing_zip']
                    )));

                    if(!$member['members_stripe_id'])
                    {
                        $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                            "card" => $token->id));

                        $data['members_stripe_id'] = $customer->id;
                    }
                    else
                    {
                        $customer = Stripe_Customer::retrieve($member['members_stripe_id']);
                        $customer->description = $member['members_email'];
                        $customer->card = $token->id;
                        $customer->save();
                    }
                }
                catch(Exception $e)
                {

                }
            }
        }
        
        if(empty($data['members_password'])){
            unset($data['members_password']);
        }
        else {
            $data['members_password'] = hash('sha256', $data['members_password']);
        }

        
        //Forum Groups
        members::set_forum_groups($_REQUEST['members_id'], (array)$data['groups_ids']);

        $data['members_hr_agreement_yn'] = $data['members_hr_agreement_yn'] == '1' ? 1 : 0;
        $data['members_elaw_agreement_yn'] = $data['members_elaw_agreement_yn'] == '1' ? 1 : 0;

        if($member){
            $members->update($data, $member['members_id']);
            $members_id = $member['members_id'];
        }
        else {  
            $data['members_datetime'] = SQL('NOW()');
            $members->insert($data);
            
            $members_id = $members->last_id();
            
            $notifications_table->insert(array('join_members_id' => $members_id));
            $members_settings_table->insert(array('join_members_id' => $members_id));
        }
        //members::set_groups($members_id, $data['groups']);
        
        //Exempt - don't allow sub users to be set as exempt (must be whole account)
        if($data['exempt'] && $member['members_id'] == $member['join_members_id']){
            members::set_members_status($members_id, 'exempt');
        }
        
        http::redirect(BASEURL .'/admin/members/edit.php?'.http_build_query(array(
            'members_id' => $members_id,
            'notice' => 'Member information updated'
        )));
    }else{
        $member = array_merge((array)$member, (array)$data);
    }
}

?>
