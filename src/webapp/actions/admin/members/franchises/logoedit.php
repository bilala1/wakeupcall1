<?php
include_once ACTIONS . '/admin/protect.php';

permissions::check('Album Admin Access');

$db = mysqli_db::init();
$photos = new mysqli_db_table('albums_photos');
$albums = new mysqli_db_table('albums');
$franchises_table = new mysqli_db_table('franchises');

$franchise = $db->fetch_one('select * from franchises  where franchises_id = ?', array($_REQUEST['franchises_id']));
$album = $db->fetch_one('select * from albums  where albums_name = ?', array("logos"));

$photo  = $photos->get((int) $franchise['join_albums_photos_id']);

if(empty($album['albums_root_folder']))
{
    $directory = SITE_PATH . '/data/albums/';
}
else
{
    $directory = SITE_PATH . $album['albums_root_folder']."/";
}

if (!is_dir($directory)) {
    mkdir($directory);
}


if($_POST){
    $data = $_POST;
    $data['join_albums_id'] = $album['albums_id'];
    
    $errors = array();
    if(empty($data['albums_photos_name'])){
        $errors[] = 'You must give the photo a name';
    }
    if(!$photo && !$_FILES['albums_photos_filename']['size']){
        $errors[] = 'You must provide a photo';
    }
    
    if(empty($errors)){
        if($_FILES['albums_photos_filename']['size']){
            $filename = uniqid('alb_photo_') . '.jpg';
            $dir      = $directory;
            
            $data['albums_photos_filename'] = $filename;
            
            $image = new image($_FILES['albums_photos_filename']['tmp_name']);
            $image->save_original($dir . $filename.'.org.jpg');
            
            @unlink($dir . $photo['albums_photos_filename'].'.org.jpg');
        }
        else {
            unset($data['albums_photos_filename']);
        }
        
        if($photo){
            unset($data['albums_photos_id']);
            
            $data['albums_photos_filename'] = $filename ? $filename : $db->fetch_singlet('SELECT albums_photos_filename FROM albums_photos WHERE albums_photos_id = ?', array($photo['albums_photos_id']));
            
            $photos->update($data, $photo['albums_photos_id']);
            $franchise['join_albums_photos_id'] = $photo['albums_photos_id'];
            $franchises_table->update($franchise, $franchise['franchises_id']);

            $albums_photos_id = $photo['albums_photos_id'];
            $notice = 'Photo Saved';
        }
        else {
            //TODO, need to require album_id
            $data['albums_photos_datestamp'] = SQL('UTC_TIMESTAMP');
            $data['join_album_id'] = $album['albums_id'];
            $photos->insert($data);
            $albums_photos_id = $photos->last_id();

            $franchise['join_albums_photos_id'] = $albums_photos_id;
            $franchises_table->update($franchise, $franchise['franchises_id']);

            $notice = 'Photo created';
        }
        
        http::redirect(BASEURL .'/admin/members/franchises/edit.php?franchises_id='.$franchise['franchises_id'].'&notice='.urlencode($notice));
    }else {
        $photo = $data;
    }
    
}

$join_albums_id = $photo['join_albums_id'];
$join_albums_id = $_REQUEST['albums_id'] ? $_REQUEST['albums_id'] : $join_albums_id;
?>
