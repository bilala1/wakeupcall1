<?php
include ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();
$bills_table = new mysqli_db_table('bills');
$licensed_locations_table = new mysqli_db_table('licensed_locations');
$licensed_location = licensed_locations::get_by_id_with_details($_REQUEST['licensed_locations_id']);

$account = accounts::get_by_id($licensed_location['join_accounts_id']);

$unkeyed_members = $db->fetch_all('SELECT DISTINCT(members_id), members_firstname, members_lastname
        FROM members m
        INNER JOIN licensed_locations_x_members_access llxm on m.members_id = llxm.join_members_id
        WHERE llxm.join_licensed_locations_id = ?', array($_REQUEST['licensed_locations_id']));

$members = array();
foreach($unkeyed_members as $member) {
    $members[$member['members_id']] = $member;
}

if($_POST){
    $data = $_POST;
    $errors = array();
    
    $validate = new validate($data);

    $validate
        ->setOptional('join_franchises_id')
        ->setOptional('licensed_locations_fax')
        ->setOptional('licensed_locations_delete_datetime')
        ->setTitles(array(
            'join_accounts_id' => 'account'
        ));

    $errors = $validate->test();

    if(empty($data['licensed_locations_id'])){
        if(!in_array($data['licensed_locations_full_type'], array_keys(licensed_locations::get_location_types()))) {
            $errors['licensed_locations_full_type'] = 'Please select a location type';
        }
        $data['licensed_location_type'] = licensed_locations::get_type_from_full_type($data['licensed_locations_full_type']);
    }

    if(empty($errors)){

        if(!empty($data['join_franchises_id']))
        {

        }

        if(empty($data['licensed_locations_id'])){
            //Create new
            $data['licensed_locations_delete_datetime'] = SQL('NULL');
            $licensed_locations_id = licensed_locations::create($data);

            $data['join_licensed_locations_id'] = $licensed_locations_id;
            if($data['licensed_locations_type'] == 'Corporate Office') {
                corporate_locations::create($data);
            } else if($data['licensed_locations_type'] == 'Hotel') {
                hotels::create($data);
            }

            $notice = 'Location added';
        }else{
            //Update
            if($data['licensed_locations_delete']){ // if checkbox for delete is checked
                if($licensed_location['licensed_locations_delete_datetime']){ // if delete date is already set, keep it
                    $data['licensed_locations_delete_datetime'] = $licensed_location['licensed_locations_delete_datetime'];
                }
                else { // if delete checkbox was chekced now, set to current date
                    $data['licensed_locations_delete_datetime'] = SQL('NOW()');
                }
            }
            else if($licensed_location['licensed_locations_delete_datetime']){ // RENEW: if delete checkbox is not checked, set value to NULL
                
                // --------------------------------------------------------------------------------
                // -- if licensed_location belongs to a corporation
                // --------------------------------------------------------------------------------
                if((int)$licensed_location['join_corporations_id']>0){
                    //todo test licensed locations
                    $corp_members_id = $db->fetch_singlet('SELECT c.join_members_id FROM licensed_locations AS ll JOIN corporations AS c ON(ll.join_corporations_id = c.corporations_id) WHERE h.licensed_locations_id = ?',array($licensed_location['licensed_locations_id']));
                    //If the previous yearly fee would have included this licensed location, then charge for it now
                    $last_bill_date = $db->fetch_singlet('select bills_datetime from bills where join_members_id = ? and bills_type = "yearly" order by bills_datetime desc limit 1', array($corp_members_id));
                    if(strtotime($licensed_location['licensed_locations_delete_datetime']) < strtotime($last_bill_date)){
                        $bill_description = 'Admin Renewed location: '.$licensed_location['licensed_locations_name'].' ('.date(DATE_FORMAT, strtotime($last_bill_date)).' - '.date(DATE_FORMAT, strtotime($last_bill_date.' + 1 year')).')';

                        list($approved, $transaction_error) = members::process_payment($corp_members_id, YEARLY_COST, $bill_description);

                        if($approved){
                            //Create bill
                            $bills_table->insert(array(
                                'join_members_id' => $corp_members_id,
                                'bills_amount' => YEARLY_COST,
                                'bills_type' => 'location',
                                'bills_description' => $bill_description,
                                'bills_datetime' => SQL('NOW()'),
                                'bills_paid' => 1
                            ));
                            
                            //mark licensed_location as NOT deleted
                            $data['licensed_locations_delete_datetime'] = SQL('NULL');
                        }
                        else { // if payment could not be processed
                            $notice  = 'Location Updated: Location could not be renewed because payment could not be processed: '.$transaction_error;
                            $data['licensed_locations_delete_datetime'] = $licensed_location['licensed_locations_delete_datetime'];
                        }
                    }else{
                        $notice  = 'Location Updated: Location was renewed.';
                        //mark licensed location as NOT deleted
                        $data['licensed_locations_delete_datetime'] = SQL('NULL');
                    }
                }
                //---------------------------------------------------------------------------------
                // if it is an independent hotel
                //---------------------------------------------------------------------------------
                else {
                    //If the previous yearly fee would have included this hotel, then charge for it now
                    $bill_date = $db->fetch_one('SELECT DATE_ADD( bills_datetime, INTERVAL + 1 year ) AS next_bill_date,  bills_datetime AS last_bill_date
                                                          FROM bills 
                                                          WHERE join_members_id = ? and bills_type = "yearly" 
                                                          ORDER BY last_bill_date DESC limit 1', array($licensed_location['join_members_id']));
                    
                    //if their payment date is past due, bill them
                    if(strtotime('now') > strtotime($bill_date['next_bill_date'])){
                        list($approved, $transaction_error) = members::process_payment($licensed_location['join_members_id'], YEARLY_COST, $bill_description);
                        $bill_description = 'Admin Renewed Yearly subscription: '.$licensed_location['licensed_locations_name'].' ('.date(DATE_FORMAT, strtotime($bill_date['next_bill_date'])).' - '.date(DATE_FORMAT, strtotime($bill_date['next_bill_date'].' + 1 year')).')';
                        if($approved){
                                //record bill
                                $bills_table->insert(array(
                                    'join_members_id' => $licensed_location['join_members_id'],
                                    'bills_amount' => YEARLY_COST,
                                    'bills_type' => 'yearly',
                                    'bills_description' => $bill_description,
                                    'bills_datetime' => $bill_date['next_bill_date'],
                                    'bills_paid' => 1
                                ));

                                $notice  = 'Location Updated: Location was renewed and billed';
                                //mark licensed location as NOT deleted
                                $data['licensed_locations_delete_datetime'] = SQL('NULL');
                        }
                        else { // if payment could not be processed
                            $notice  = 'Location Updated: Location could not be renewed because payment could not be processed: '.$transaction_error;
                            $data['licensed_locations_delete_datetime'] = $licensed_location['licensed_locations_delete_datetime'];
                        }
                    }
                    else {
                        $notice  = 'Location Updated: Location was renewed.';
                        //mark licensed location as NOT deleted
                        $data['licensed_locations_delete_datetime'] = SQL('NULL');
                    }

                }//end -- if independent hotel


            }// end -- if renewing a hotel

            $licensed_locations_table->update($data, $data['licensed_locations_id']);
            $licensed_locations_id = $data['licensed_locations_id'];

            if(!$notice){
                $notice = 'Location updated';
            }
        }
        
        http::redirect('edit.php?licensed_locations_id=' . $licensed_locations_id . '&notice=' . urlencode($notice));
    }else{
        //so user input is not lost
        $licensed_location = array_merge($licensed_location, $data);
    }
}
?>