<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$where = $params = array();

switch($_REQUEST['order']){
    case "accounts_name":
        $order = 'accounts_name';
        break;
    case "franchises_name":
        $order = 'franchises_name';
        break;
    default:
        if(!array_key_exists($_REQUEST, 'by')) {
            $_REQUEST['by'] = 'ASC';
        }
    case "name":
        $order = 'licensed_locations_name';
    break;
}

$orderby = strings::orderby($order);

//todo test licensed locations
$licensed_locations = $db->fetch_all('
    SELECT *
    FROM licensed_locations AS ll
    INNER JOIN accounts ON ll.join_accounts_id = accounts_id
    LEFT JOIN franchises ON franchises_id = ll.join_franchises_id
    ' . strings::where($where) . '
    GROUP BY ll.licensed_locations_id
    ORDER BY ' . $orderby,
    $params
);

?>
