<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$where = $params = array();

if($_REQUEST['corporations_id']){
    $where  []= 'c.corporations_id = ?';
    $params []= $_REQUEST['corporations_id'];
}

if($_REQUEST['corporations_id'] == 'none'){
    $where[] = 'c.corporations_id is null';
}

if($_REQUEST['name']){
    $where  []= '(m.members_firstname LIKE ? OR m.members_lastname LIKE ?)';
    $params []= '%'.$_REQUEST['name'];
    $params []= '%'.$_REQUEST['name'];
}

// exclude Former Member from query
$where[] = 'm.members_id != '.FORUM_FORMER_MEMBER;

switch($_REQUEST['order']){
    default:
    case "name":
        $order = 'members_firstname';
    break;

    case "type":
        $order = 'members_type';
        break;

    case "company":
        $order = 'accounts_name';
    break;

    case "franchises_name":
        $order = 'franchises_name';
    break;

    case "status":
        $order = 'members_status';
    break;

    case "date":
        $order = 'members_datetime';
    break;

	case 'members_last_login':
		$order = 'members_last_login';
	break;
}

$orderby = strings::orderby($order);

$members = $db->fetch_all('
    SELECT m.*, a.*
    FROM members AS m
    INNER JOIN accounts AS a ON m.join_accounts_id = a.accounts_id
    LEFT JOIN corporations c ON c.join_accounts_id = a.accounts_id
    '.strings::where($where).'
    GROUP BY m.members_id
    ORDER BY '.$orderby, $params
);
//pre($db->debug());
//pre($members);


?>
