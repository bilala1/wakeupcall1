<?php

include ACTIONS .'/admin/protect.php';

$db = mysqli_db::init();

if($_POST){

    //assign POST to post variable, so that if errors occur, variables for the form are not list
    $post = $_POST;

	if (http::is_ajax()) {
		$post = $_POST['checkboxes'];
	}


    //---------------------------------------------------------------------------------------------
    // -- where
    //---------------------------------------------------------------------------------------------
    $ors = array();
    if((int)$post['notifications_forum_update']==1){
        $ors[]    = 'notifications_forum_update = 1';
    }
    if((int)$post['notifications_news_update']==1){
        $ors[]    = 'notifications_news_update = 1';
    }
    if((int)$post['notifications_blog_update']==1){
        $ors[]    = 'notifications_blog_update = 1';
    }
    if((int)$post['notifications_additional_services']==1){
        $ors[]    = 'notifications_webinars_update = 1';
    }
    if((int)$post['notifications_webinars_update']==1){
        $ors[]    = 'notifications_webinars_update = 1';
    }
    if((int)$post['notifications_training_update']==1){
        $ors[]    = 'notifications_training_update = 1';
    }

    if($ors){
        $where  []= '(' . implode(' OR ', $ors) . ')';
    }

    //---------------------------------------------------------------------------------------------
    // -- emails
    //---------------------------------------------------------------------------------------------

    // Corporate locations
        //where
        $members_corporate_locations_where = $where ;
        $members_corporate_locations_where[] = 'members_status = "member"';
        $members_corporate_locations_where[] = 'members_type =  "corporation"';
        //query
        $members_corporate_locations = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($members_corporate_locations_where).'',array());

    // Hotels
        //where
        $members_single_locations_where = $where ;
        $members_single_locations_where[] = 'members_status = "member"';
        $members_single_locations_where[] = 'members_type = "hotel"';
        //query
        $members_single_locations = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($members_single_locations_where).'',array());

    //if their last bill covers time period that was ended
        //where
        $members_not_rejoined_where = $where;
        $members_not_rejoined_where[] = 'members_id IN (SELECT join_members_id AS mid
                                                                      FROM `bills`
                                                                      GROUP BY mid
                                                                      HAVING DATE_ADD((SELECT bills_datetime
                                                                                       FROM bills
                                                                                       WHERE join_members_id = mid
                                                                                       AND `bills_type` = "yearly"
                                                                                       ORDER BY `bills_datetime` DESC LIMIT 1), INTERVAL 1 YEAR) < NOW())';
        //query
        $members_not_rejoined = $db->fetch_singlets('SELECT members_email
                                                     FROM members AS m
                                                     LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                     '.strings::where($members_not_rejoined_where).'',array());

    // currently active trial members
        //where
    //todo test licensed locations
        $trial_current_where = $where ;
        $trial_current_where[] = 'members_status = "free"';
        $trial_current_where[] = '(members_type = "corporation"
                                        OR
                                       (members_type = "hotel"
                                        AND members_id IN(SELECT join_members_id FROM licensed_locations WHERE join_corporations_id = 0)))';
        //query
        $trial_current = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($trial_current_where).'',array());

    // trial member who did not join - whose trial has expired
        //where
        $trial_not_joined_where = $where;
        $trial_not_joined_where[] = 'members_status = "locked"';
        $trial_not_joined_where[] = 'DATE(members_datetime) < DATE_SUB(CURDATE(),INTERVAL '.(int)FREE_MEMBER_PERIOD.' DAY)';
        //query
        $trial_not_joined = $db->fetch_singlets('SELECT members_email
                                                   FROM members AS m
                                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                   '.strings::where($trial_not_joined_where).'',array());

    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // - Notifications
/*
    $notifications_forum_update = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_forum_update = 1 ');

    $notifications_news_update = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_news_update = 1 ');

    $notifications_blog_update = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_blog_update = 1 ');

    $notifications_additional_services = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_additional_services = 1 ');

    $notifications_webinars_update = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_webinars_update = 1 ');

    $notifications_training_update = $db->fetch_singlets('SELECT members_email
                                                       FROM members AS m
                                                       JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                       WHERE notifications_training_update = 1 ');
*/
    //---------------------------------------------------------------------------------------------
    //validation
    //---------------------------------------------------------------------------------------------
	if (!http::is_ajax() && !isset($_POST['check_members_count'])) {
		if(strlen($post['message_title']) <3){
			$errors['message_title'] = 'Please enter title of a message';
		}
		if(strlen($post['message_content']) <3){
			$errors['message_content'] = 'Please enter email message to be sent';
		}
		// if no checkbox was selected nor person selected
		if( !$post['members_corporate_locations'] &&
			!$post['members_single_locations'] &&
			!$post['members_not_rejoined'] &&
			!$post['trial_current'] &&
			!$post['trial_not_joined'] &&
			!$post['member_selected']
			/*    &&
			!$post['notifications_forum_update'] &&
			!$post['notifications_news_update'] &&
			!$post['notifications_blog_update'] &&
			!$post['notifications_additional_services'] &&
			!$post['notifications_webinars_update'] &&
			!$post['notifications_training_update']     */
				) {
				$errors['checkboxes'] = 'Please select at least one checkbox';
		}
	} else {
		$errors = false;
	}
    //---------------------------------------------------------------------------------------------

    if(!$errors){

        //-----------------------------------------------------------------------------------------
        //decide whom to send this email
        //-----------------------------------------------------------------------------------------
        $all_members = array();

		if (empty($_POST['member_selected'])) {
			//members [paid]
			if($post['members_corporate_locations']){ // if corporate
				if($members_corporate_locations){
					$all_members = $members_corporate_locations;
				}
			}
			if($post['members_single_locations']){ // if single locations
				if($members_single_locations){
					$all_members = array_merge($all_members,$members_single_locations);
				}
			}
			if($post['members_not_rejoined']){ // if not rejoined
				if($members_not_rejoined){
					$all_members = array_merge($all_members,$members_not_rejoined);
				}
			}
			//trial members
			if($post['trial_current']){ //
				if($trial_current){
					$all_members = array_merge($all_members,$trial_current);
				}
			}
			if($post['trial_not_joined']){ //
				if($trial_not_joined){
					$all_members = array_merge($all_members,$trial_not_joined);
				}
			}

	/*
				//---- notifications
				if($post['notifications_forum_update']){ // forum updates
					if($notifications_forum_update){
						$all_members = array_merge($all_members,$notifications_forum_update);
					}
				}
				if($post['notifications_news_update']){ // news updates
					if($notifications_news_update){
						$all_members = array_merge($all_members,$notifications_news_update);
					}
				}
				if($post['notifications_blog_update']){ // blog updates
					if($notifications_blog_update){
						$all_members = array_merge($all_members,$nnotifications_blog_update);
					}
				}
				if($post['notifications_additional_services']){ // additional services updates
					if($notifications_additional_services){
						$all_members = array_merge($all_members,$notifications_additional_services);
					}
				}
				if($post['notifications_webinars_update']){ // webinars updates
					if($notifications_webinars_update){
						$all_members = array_merge($all_members,$notifications_webinars_update);
					}
				}
				if($post['notifications_training_update']){ // training updates
					if($notifications_training_update){
						$all_members = array_merge($all_members,$notifications_training_update);
					}
				}
	*/
			//-----------------------------------------------------------------------------------------
			// get all members who should receive email
			//-----------------------------------------------------------------------------------------
			//remove duplicates from an array
			$all_members = array_unique($all_members);
		} else {
			$all_members = $db->fetch_singlets('
				SELECT members_email FROM members
				WHERE members_id = ?
			', array($_POST['member_selected']));
		}


		if (http::is_ajax() && isset($_POST['check_members_count'])) {
			echo count($all_members); exit;
		}

        //-----------------------------------------------------------------------------------------
        if($all_members){

            //-- Pull the email into a buffer
            ob_start();
            include(SITE_PATH . '/' . VIEWS . '/emails/mass-email.php');
            $message = ob_get_clean();

            //-- Mail to customer
            //$from = SALES_EMAIL;

			$from = $post['from_email'];

            //$to = 'test@wakeupcall.net';
            $subject = $post['message_title'] . ' | ' . SITE_NAME;

            //-- Set Zend Load Path
            $paths = array(
                realpath(SITE_PATH. '/library'),
                '.'
            );
            set_include_path(implode(PATH_SEPARATOR, $paths));

            //-- Load Zend Mail
            require_once 'Zend/Mail.php';

            // loop through all recepients and send them emails
            foreach($all_members as $to){
//uncomment when testing
//                if(in_array($to, array('test@wakeupcall.net','test@wakeupcall.net'))){ ///-------------
                $mail = new Zend_Mail('utf');
                $mail->setBodyHtml($message);
                $mail->setSubject($subject);
                $mail->addTo($to);
                $mail->setFrom($from);

                if($_FILES['attachment0']){ // if at least one attachment
                    //-- Attachments
                    foreach (range(0, 4) as $i) {
                        $file = $_FILES['attachment' . $i];
                        if (empty($file['name'])) {
                            continue;
                        }

                        $attachment = $mail->createAttachment(
                            file_get_contents($file['tmp_name'])
                        );

                        $attachment->type        = files::get_mime_type($file['tmp_name']) ?: $file['type'];
                        $attachment->disposition = Zend_Mime::DISPOSITION_INLINE;
                        $attachment->encoding    = Zend_Mime::ENCODING_BASE64;
                        $attachment->filename    = $file['name'];
                    }
                }
                $mail->send();
//                } //----------------
            }

            $notice = 'Email was sent';
            http::redirect(BASEURL .'/admin/members/mass-email.php?'.http_build_query(array('notice'  => $notice)));
        }

    }// end -- if no errors


}// end -- if form submitted


//---------------------------------------------------------------------------------------------
    // -- emails
    //---------------------------------------------------------------------------------------------

    // Corporate locations
        //where
        $members_corporate_locations_where = $where ;
        $members_corporate_locations_where[] = 'members_status = "member"';
        $members_corporate_locations_where[] = 'members_type =  "corporation"';
        //query
        $members_corporate_locations = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($members_corporate_locations_where).'',array());

    // Hotels
        //where
        $members_single_locations_where = $where ;
        $members_single_locations_where[] = 'members_status = "member"';
        $members_single_locations_where[] = 'members_type = "hotel"';
        //query
        $members_single_locations = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($members_single_locations_where).'',array());
    //if their last bill covers time period that was ended
        //where
        $members_not_rejoined_where = $where;
        $members_not_rejoined_where[] = 'members_id IN (SELECT join_members_id AS mid
                                                                      FROM `bills`
                                                                      GROUP BY mid
                                                                      HAVING DATE_ADD((SELECT bills_datetime
                                                                                       FROM bills
                                                                                       WHERE join_members_id = mid
                                                                                       AND `bills_type` = "yearly"
                                                                                       ORDER BY `bills_datetime` DESC LIMIT 1), INTERVAL 1 YEAR) < NOW())';
        //query
        $members_not_rejoined = $db->fetch_singlets('SELECT members_email
                                                     FROM members AS m
                                                     LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                     '.strings::where($members_not_rejoined_where).'',array());

    // currently active trial members
        //where
//todo test licensed locations
        $trial_current_where = $where ;
        $trial_current_where[] = 'members_status = "free"';
        $trial_current_where[] = '(members_type = "corporation"
                                        OR
                                       (members_type = "hotel"
                                        AND members_id IN(SELECT join_members_id FROM licensed_locations WHERE join_corporations_id = 0)))';
        //query
        $trial_current = $db->fetch_singlets('SELECT members_email
                                   FROM members AS m
                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                   '.strings::where($trial_current_where).'',array());

    // trial member who did not join - whose trial has expired
        //where
        $trial_not_joined_where = $where;
        $trial_not_joined_where[] = 'members_status = "locked"';
        $trial_not_joined_where[] = 'DATE(members_datetime) < DATE_SUB(CURDATE(),INTERVAL '.(int)FREE_MEMBER_PERIOD.' DAY)';
        //query
        $trial_not_joined = $db->fetch_singlets('SELECT members_email
                                                   FROM members AS m
                                                   LEFT JOIN notifications as n ON (m.members_id = n.join_members_id)
                                                   '.strings::where($trial_not_joined_where).'',array());



?>
