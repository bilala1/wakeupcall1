<?php
$db = mysqli_db::init();

$val = $_REQUEST['value'];

$wheres = $params = array();

if (!empty($val)) {
	$wheres[] = '(members_firstname LIKE ? OR members_lastname LIKE ?)';
	$params[] = '%' . $val . '%';
	$params[] = '%' . $val . '%';
}

$where = strings::where($wheres);
$members = $db->fetch_all('
	SELECT members_id,
		CONCAT(members_firstname, " ", members_lastname) as name,
		members_type
	FROM members
	'.$where.'
', $params);

$json_members = array();
foreach ($members as $member) {
	$json_members[] = array(
		'title' => $member['name'] . ' (' . $member['members_type'] . ')',
		'id' => $member['members_id']
	);
}

echo json_encode($json_members); exit;
?>