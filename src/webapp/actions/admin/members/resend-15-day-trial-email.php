<?php

//members::delete_member_from_think_hr(202);
//members::create_member_from_think_hr(202);
include ACTIONS .'/admin/protect.php';

$notifications_table = new mysqli_db_table('notifications');
$members_settings_table = new mysqli_db_table('members_settings');

$db = mysqli_db::init();

$member = $db->fetch_one('SELECT *
        FROM members WHERE members_id = ?', array($_REQUEST['members_id']));
$member = members::get_customer_credit_card_data($member);


//Send email
ob_start();
include VIEWS . '/emails/register-verify.php';
$message = ob_get_contents();
ob_clean();
mail::send(MEMBERSHIP_EMAIL, $member['members_email'], '15 Day Free Trial Welcome | '.SITE_NAME, $message);

http::redirect('/admin/members/edit.php?members_id='.$member['members_id'].'&notice=Trial+member+verification+email+resent.');

?>