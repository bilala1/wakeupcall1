<?php

//members::delete_member_from_think_hr(202);
//members::create_member_from_think_hr(202);
include ACTIONS .'/admin/protect.php';

$notifications_table = new mysqli_db_table('notifications');
$members_settings_table = new mysqli_db_table('members_settings');

$db = mysqli_db::init();

$member = members::get_by_id($_REQUEST['members_id']);
$account = accounts::get_by_id($member['join_accounts_id']);

//Send email
$template = $account['accounts_type'] == 'single' ? 'full-membership-welcome-individual-verification.php' : 'full-membership-welcome-corporate-verification.php';
mail::send_template(
    $from     = MEMBERSHIP_EMAIL,
    $to       = $member['members_email'],
    $subject  = 'Full Membership Welcome | '.SITE_NAME,
    $template,
    $vars     = get_defined_vars()
);
//ob_start();
//include VIEWS . '/emails/' . $template;
//$message = ob_get_contents();
//ob_clean();
//mail::send(MEMBERSHIP_EMAIL, $member['members_email'], '15 Day Free Trial Welcome | '.SITE_NAME, $message);

http::redirect('/admin/members/edit.php?members_id='.$member['members_id'].'&notice=Joined+member+verification+email+resent.');

?>