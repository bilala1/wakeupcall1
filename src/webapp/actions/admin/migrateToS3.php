<?php

include ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
$limit = 50;
if (array_key_exists('limit', $_REQUEST)) {
    $limit = $_REQUEST['limit'];
}

$localFs = new \Services\FileSystem\LocalFileSystem(SITE_PATH . DIRECTORY_SEPARATOR . 'data');
$files = $db->fetch_all('SELECT * FROM files WHERE files_storage_type = "local" limit ?', array($limit));
foreach ($files as $file) {
    $localPath = $localFs->getFullPath($file);

    if(!file_exists($localPath)) {
        $result = "Not Found";
    } else {
        $result = \Services\Services::$fileSystem->migrateLocalFileToS3($file);
        $result = $result ? "Success" : "Failure";
    }
    echo(sprintf('%s - ID: %s, Location: %s, Name: %s<br>', $result, $file['files_id'],
        $file['files_location'], $file['files_name']));
    flush();
}