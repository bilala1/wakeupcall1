<?php

$db = mysqli_db::init();
include ACTIONS . '/admin/protect.php';

$notifications = $db->fetch_all('
	SELECT * FROM admin_notifications
	ORDER BY admin_notifications_datetime DESC
');

if ($_REQUEST['ajax'] == 'delete_notif') {
	$db->query('DELETE FROM admin_notifications WHERE admin_notifications_id = ?', array($_REQUEST['notifications_id']));
}

?>
