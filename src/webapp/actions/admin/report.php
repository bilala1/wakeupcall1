<?php

include ACTIONS .'/admin/protect.php';
$db = mysqli_db::init();

$logs = $db->fetch_all("
    SELECT accounts_name, m1.members_firstname, m1.members_lastname, access_logs_path, DATE(access_logs_datetime) as access_logs_datetime, al.join_members_id FROM access_logs al
    inner join members m1 on al.join_members_id = m1.members_id
    inner join accounts a on al.join_accounts_id = accounts_id
    inner join members m2 on a.join_members_id = m2.members_id
    where (m2.members_status = 'member' or accounts_id in (50, 230, 399, 429, 460)) and accounts_id not in (450, 451)
    and al.join_members_id != 0
");

$cats = array(
    '/members/index' => 'dashboard',
    '/members/my-documents/certificates' => 'certs',
    '/members/vendors' => 'certs',
    '/members/my-documents' => 'files',
    '/members/claims' => 'claims',
    '/members/contracts' => 'contracts',
    '/members/documents' => 'library',
    '/members/forums' => 'forums',
    '/members/entities' => 'businesses',
    '/members/hr' => 'hr',
    '/members/employmentlaw' => 'elaw',
    '/members/webinars' => 'webinars',
    '/members/search' => 'search',
    '/members/news' => 'news',
    '/members' => 'other'
);

$ignores = array(
    '/admin/',
    '/API/'
);

$byUser = array();
$byAccount = array();

foreach($logs as $log) {
    $ignored=false;
    foreach($ignores as $ignore) {
        if (strpos($log['access_logs_path'], $ignore) === 0) {
            $ignored = true;
            break;
        }
    }
    if($ignored) {
        continue;
    }
    foreach($cats as $prefix => $cat) {
        if (strpos($log['access_logs_path'], $prefix) === 0) {
            $log['category'] = $cat;
            break;
        }
    }
    if(!array_key_exists('category', $log)) {
        continue;
    }
    if(!isset($byUser[$log['join_members_id']])) {
        $byUser[$log['join_members_id']] = array(
            'account' => $log['accounts_name'],
            'member' => $log['members_firstname'] . ' ' . $log['members_lastname'],
            'first use' => $log['access_logs_datetime'],
            'dashboard' => 0,
            'certs' => 0,
            'files' => 0,
            'claims' => 0,
            'contracts' => 0,
            'businesses' => 0,
            'library' => 0,
            'forums' => 0,
            'hr' => 0,
            'elaw' => 0,
            'webinars' => 0,
            'search' => 0,
            'news' => 0,
            'other' => 0
        );
    }
    $byUser[$log['join_members_id']][$log['category']] = $byUser[$log['join_members_id']][$log['category']] + 1;
}



$csv = fopen('/home/jpatula/Development/wakeupcall/src/webapp/data/records.csv', 'w');
fputcsv($csv, array(
    'Account',
    'Member',
    'First Access',
    'Dashboard',
    'Certs',
    'My/Shared Files',
    'Claims',
    'Contracts',
    'Business Tracking',
    'Document Library',
    'Forums/Corp Bulletin',
    'HR',
    'E-Law',
    'Webinars',
    'Search',
    'News',
    'Other'));
foreach($byUser as $user) {
    fputcsv($csv, $user);
}
fclose($csv);

/*
Other cool queries:

select accounts_name, licensed_locations_name,
	(select count(1) from claims where join_licensed_locations_id = licensed_locations_id) as num_claims,
	(select count(1) from certificates where join_licensed_locations_id = licensed_locations_id) as num_certs,
	(select count(1) from contracts c left join contracts_x_licensed_locations on contracts_id = join_contracts_id
		where c.join_accounts_id = accounts_id and (contracts_all_locations = 1 or join_licensed_locations_id = licensed_locations_id)) as num_contracts
 from licensed_locations
 inner join accounts a on join_accounts_id = accounts_id
 inner join members on a.join_members_id = members_id
 where (members_status = 'member' or accounts_id in (50, 230, 399, 429, 460)) and accounts_id not in (450, 451)
 order by accounts_name, licensed_locations_name;

select accounts_name, m.members_firstname, m.members_lastname,
   (SELECT COALESCE(datediff(NOW(), min(members_logins_datetime)) + 1, 0) from members_logins where join_members_id = m.members_id) as total_days,
   (select count(distinct DATE(members_logins_datetime)) from members_logins where join_members_id = m.members_id) as days_logged_in
from members m
inner join accounts on accounts_id = join_accounts_id
inner join members m2 on join_members_id = m2.members_id
 where (m2.members_status = 'member' or accounts_id in (50, 230, 399, 429, 460)) and accounts_id not in (450, 451)
 order by accounts_name, members_lastname, members_firstname;



 */