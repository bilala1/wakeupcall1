<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$wheres = array();
$params = array();

if($_GET['from'] || $_GET['to']){
    $actions_name_where = '(actions_name like "MSDS Search:%" or actions_name like "MSDS Download:%" or actions_name like "SDS Search:%" or actions_name like "SDS Download:%" )';
    
    if($_GET['from'] && $_GET['to']){
        $from = date('Y-m-d', strtotime($_GET['from']));
        $to = date('Y-m-d', strtotime($_GET['to']));
        
        $wheres[] = 'DATE(actions_datetime) between ? and ?';
        $params[] = $from;
        $params[] = $to;
    }
    else if($_GET['from']){
        $from = date('Y-m-d', strtotime($_GET['from']));
        $to = $db->fetch_singlet('select DATE(actions_datetime) from actions '.strings::where((array)$actions_name_where).' order by actions_datetime desc limit 1');
        
        $wheres[] = 'DATE(actions_datetime) >= ?';
        $params[] = $from;
    }
    else if($_GET['to']){
        $wheres[] = 'DATE(actions_datetime) <= ?';
        $from = $db->fetch_singlet('select DATE(actions_datetime) from actions '.strings::where((array)$actions_name_where).' order by actions_datetime asc limit 1');
        $to = date('Y-m-d', strtotime($_GET['to']));
        
        $params[] = $to;
    }
    
    $wheres[] = $actions_name_where;
    $actions = $db->fetch_all('
        select *, DATE(actions_datetime) as date
        from actions'.
        strings::where($wheres),
        $params
    );
    
    $total_searches = 0;
    $total_downloads = 0;
    $days = array();
    $add = 0;
    do{
        $day = date(DATE_FORMAT, strtotime($from.' +'.$add.' day'));
        $mysql_day = date('Y-m-d', strtotime($day));
        
        $num_searches = 0;
        $num_downloads = 0;
        foreach((array)$actions as $action){
            if($action['date'] == $mysql_day){
                if(strpos($action['actions_name'], 'MSDS Search:') !== false
                    || strpos($action['actions_name'], 'SDS Search:') !== false){
                    $num_searches++;
                }else{
                    $num_downloads++;
                }
            }
        }
        
        $days[] = array(
            'date' => $day,
            'num_searches' => $num_searches,
            'num_downloads' => $num_downloads
        );
        
        $total_searches += $num_searches;
        $total_downloads += $num_downloads;
        $add++;
        
        if($add > 5000){
            echo 'stupid do while';
            exit;
        }
    }while(strtotime($day) <= strtotime($to));
}

?>
