<?php
$db = mysqli_db::init();
include ACTIONS . '/admin/protect.php';

$wheres = $params = array();

if ($_GET['from']) {
	$wheres[] = 'DATE(members_logins_datetime) >= ?';
	$params[] = date('Y-m-d', strtotime($_GET['from']));
}

if ($_GET['to']) {
	$wheres[] = 'DATE(members_logins_datetime) <= ?';
	$params[] = date('Y-m-d', strtotime($_GET['to']));
}

$where = strings::where($wheres);
$members_rows = $db->fetch_all('
	SELECT m.*, COUNT(*) AS login_count
	FROM members_logins AS ml
	LEFT JOIN members AS m ON ml.join_members_id = m.members_id
	'.$where.'
	GROUP BY m.members_id
	ORDER BY members_lastname ASC
', $params);
?>