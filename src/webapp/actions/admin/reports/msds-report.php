<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$entities = array();
// the reason I did't use group by has to do with new_download...
// if new_download value is set to 0 in at least one of the rows, antire group won't be displayed.



//$query = "SELECT /*COUNT(*) AS cnt, */  a.*, members_type, members_firstname, members_lastname, 
/*                                    IF(members_type = 'corporation',join_members_id,
                                        IF(members_type = 'hotel',join_members_id,
                                            (SELECT join_members_id FROM hotels WHERE hotels_id = (SELECT join_hotels_id FROM staff AS s WHERE s.join_members_id = a.join_members_id LIMIT 1)
                                            )
                                        )
                                    ) AS entity_id,

                                    (
                                        SELECT CONCAT_WS(' ',members_firstname, members_lastname) FROM members WHERE members_id = entity_id
                                    ) AS entity_members_name,

                                    IF(members_type = 'corporation',join_members_id,
                                        IF(members_type = 'hotel',(SELECT c.join_members_id FROM corporations AS c JOIN hotels AS h ON (h.join_corporations_id = c.corporations_id) WHERE h.join_members_id = a.join_members_id),
                                            (SELECT c.join_members_id FROM corporations AS c LEFT JOIN hotels AS h ON(h.join_corporations_id = c.corporations_id)
                                             WHERE hotels_id = (SELECT join_hotels_id FROM staff AS s WHERE s.join_members_id = a.join_members_id LIMIT 1)
                                             ))) AS corporation_id,

                                    (SELECT bills_datetime FROM bills WHERE join_members_id = corporation_id AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1) as last_bill, 
                                    DATE_ADD((SELECT bills_datetime FROM bills WHERE join_members_id = corporation_id AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1),INTERVAL 1 YEAR) AS next_bill,

                                    (
                                        SELECT COUNT(*) AS cnt2 
                                        FROM actions AS a2 
                                        WHERE (
                                          a2.join_members_id IN(
                                            IF(members_type = 'corporation', corporation_id,
                                              IF(members_type = 'hotel', entity_id,
                                                (SELECT s2.join_members_id FROM staff AS s2 WHERE join_hotels_id = entity_id)
                                              )
                                            )
                                          )
                                        )
                                        AND actions_name LIKE('MSDS Download%')
                                        AND DATE(actions_datetime) < ?
                                        AND DATE(actions_datetime) > (SELECT bills_datetime FROM bills WHERE join_members_id = corporation_id AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1)
                                    ) AS previous_downloads,
                                    (
                                    IF(a.actions_datetime > (SELECT DATE(bills_datetime) FROM bills WHERE join_members_id = corporation_id AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1), 1,0)

                                    ) AS new_download

                                                                    FROM `actions` AS a
                                                                    JOIN members AS m ON(m.members_id = a.join_members_id)
                                                                    WHERE actions_name LIKE('MSDS Download%')
                                                                    AND (DATE(actions_datetime) >= ? AND DATE(actions_datetime) <= ?)

                                    #GROUP BY entity_id
                                    HAVING  previous_downloads < 2  AND  new_download = 1 
                                    ORDER BY entity_id DESC ";

$msds_downloads = $db->fetch_all($query,array(date('Y-m-d',strtotime($_REQUEST['from'])),date('Y-m-d',strtotime($_REQUEST['from'])),date('Y-m-d',strtotime($_REQUEST['to']))));
*/

//todo test licensed locations
$query = "SELECT /*COUNT(*) AS cnt, */  a.*, members_type, members_firstname, members_lastname, 
                                    IF(members_type = 'corporation',join_members_id,
                                        IF(members_type = 'hotel',join_members_id,
                                            (SELECT join_members_id FROM licensed_locations WHERE licensed_locations_id = (SELECT join_licensed_locations_id FROM staff AS s WHERE s.join_members_id = a.join_members_id LIMIT 1)
                                            )
                                        )
                                    ) AS entity_id,
                                    
                                    (SELECT CONCAT_WS(' ',members_firstname, members_lastname) FROM members WHERE members_id = entity_id
                                    ) AS entity_members_name,

                                    IF(members_type = 'corporation',join_members_id,
                                        IF(members_type = 'hotel',(SELECT c.join_members_id FROM corporations AS c JOIN licensed_locations AS ll ON (ll.join_corporations_id = c.corporations_id) WHERE ll.join_members_id = a.join_members_id),
                                            (SELECT c.join_members_id FROM corporations AS c LEFT JOIN licensed_locations AS ll ON(ll.join_corporations_id = c.corporations_id)
                                             WHERE licensed_locations_id = (SELECT join_licensed_locations_id FROM staff AS s WHERE s.join_members_id = a.join_members_id LIMIT 1)
                                            )
                                        )
                                    ) AS corporation_id,
                                    
                                    (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1) 
                                    as last_bill, 
                                    
                                    DATE_ADD((SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1),INTERVAL 1 YEAR
                                    ) AS next_bill,

                                    ( SELECT COUNT(*) AS cnt2 
                                      FROM actions AS a2 
                                      WHERE (
                                          a2.join_members_id IN(
                                              IF(members_type = 'corporation', corporation_id,
                                                  IF(members_type = 'hotel', entity_id,
                                                  (SELECT s2.join_members_id FROM staff AS s2 WHERE join_licensed_locations_id = entity_id)
                                                  )
                                              )
                                          )
                                      )
                                      AND ( actions_name LIKE('MSDS Download%') OR actions_name LIKE('SDS Download%') )
                                      AND DATE(actions_datetime) < ?
                                      AND DATE(actions_datetime) > 
                                        IF(? > (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1), 
                                            (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1), 
                                            (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' AND DATE(bills_datetime) < ? ORDER BY bills_datetime DESC LIMIT 1)
                                        )
                                    ) AS previous_downloads,
                                    
                                    (IF(
                                        a.actions_datetime >
                                            IF((? > (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1)
                                              AND 
                                              ((SELECT COUNT(*) FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' )>1)), 
                                                (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' ORDER BY bills_datetime DESC LIMIT 1), 
                                                IF(
                                                  ((SELECT COUNT(*) FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' AND DATE(bills_datetime) < ?)>0),
                                                  (SELECT bills_datetime FROM bills WHERE join_members_id = IF(corporation_id,corporation_id,entity_id) AND bills_type = 'yearly' AND DATE(bills_datetime) < ? ORDER BY bills_datetime DESC LIMIT 1), ?
                                                )
                                            )
                                        , 1,0)
                                    ) AS new_download

                                    FROM `actions` AS a
                                    JOIN members AS m ON(m.members_id = a.join_members_id)
                                    WHERE ( actions_name LIKE('MSDS Download%') OR actions_name LIKE('SDS Download%') )
                                    AND (DATE(actions_datetime) >= ? AND DATE(actions_datetime) <= ?)
                                    #GROUP BY entity_id
                                    HAVING  previous_downloads < 2  AND  new_download = 1 
                                    ORDER BY entity_id DESC";

$msds_downloads = $db->fetch_all($query,array(
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['from'])),
                                                date('Y-m-d',strtotime($_REQUEST['to'])))
                                );
//pre($db->debug());
$totals = array();
//pre($msds_downloads);
foreach($msds_downloads as &$download){
    if(array_key_exists($download['entity_id'], $totals)){
        $totals[$download['entity_id']] += 1;
    }
    else {
        $totals[$download['entity_id']] = 1;
    }
    unset($download);
}
//remove single rows
foreach($msds_downloads as $k => &$download){
    //remove if (a) number of records is one and (b) if no previous downloads (before date range selected and after billing began)
    if($totals[$download['entity_id']]== 1 && $download['previous_downloads']== 0){
        unset($msds_downloads[$k]);
    }
}

$entities = array();



//pre($entities);

?>