<?php
/**
 * Created by PhpStorm.
 * User: jpatula
 * Date: 5/3/17
 * Time: 8:34 AM
 */

include ACTIONS . '/admin/protect.php';
$db = mysqli_db::init();
$now = new DateTime();
$now->sub(new DateInterval('P1D'));
$start = new DateTime();
$start->sub(new DateInterval('P30D'));

$filters = array(
    'start_date' => $start->format('n/j/Y'),
    'end_date' => $now->format('n/j/Y')
);

$allAccounts = $db->fetch_all("SELECT * FROM accounts order by accounts_name");

$accounts = array(0 => 'All Accounts');
$selectedAccount = $allAccounts[0]['accounts_id'];
foreach($allAccounts as $account) {
    $accounts[$account['accounts_id']] = $account['accounts_name'];
}
