<?php

include ACTIONS .'/admin/protect.php';
$db = mysqli_db::init();

$fileIds = $db->fetch_singlets('SELECT files_id FROM files WHERE files_datetime is null');
foreach($fileIds as $fileId) {
    $result = \Services\Services::$fileSystem->updateNullDateTime($fileId);
    echo("Datetime for file $fileId is now $result<br>");
}