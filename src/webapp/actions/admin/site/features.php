<?php

include ACTIONS .'/admin/protect.php';

$siteFeaturesTable = new mysqli_db_table('site_features');

$db = mysqli_db::init();

if($_POST){
    $featuresArray = $_POST['site_features_active'];
    foreach($featuresArray as $id => $active) {
        $siteFeaturesTable->update(array('site_features_active'=>$active), $id);
    }
    $notice = "Active features updated";
}

$siteFeatures = $db->fetch_all('SELECT * FROM site_features');