<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$db->query('DELETE FROM surveys WHERE surveys_id = ?', array($_REQUEST['surveys_id']));
$db->query('DELETE FROM survey_responses WHERE join_surveys_id = ?', array($_REQUEST['surveys_id']));
$db->query('DELETE FROM survey_options WHERE join_surveys_id = ?', array($_REQUEST['surveys_id']));

@unlink(SITE_PATH .'/images/surveys/survey_'.$_REQUEST['surveys_id'].'.png');

http::redirect(BASEURL .'/admin/surveys/list.php');

?>
