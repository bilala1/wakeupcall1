<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$survey  = $db->fetch_one('SELECT * FROM surveys WHERE surveys_id = ?', array($_REQUEST['surveys_id']));
$options = $db->fetch_all('SELECT so.*, COUNT(DISTINCT sr.survey_responses_id) AS votes FROM survey_options AS so
    LEFT JOIN survey_responses AS sr ON sr.join_survey_options_id = so.survey_options_id
        WHERE so.join_surveys_id = ?
        GROUP BY so.survey_options_id', 
    array($_REQUEST['surveys_id'])
);

$total_votes = 0;
foreach($options as $option){
    $total_votes += $option['votes'];
}

if($_POST){
    $data = $_POST;
    
    $surveys        = new mysqli_db_table('surveys');
    $survey_options = new mysqli_db_table('survey_options');
    
    if($survey){
        $old_options     = $db->fetch_singlets('SELECT survey_options_id FROM survey_options WHERE join_surveys_id = ?', array($survey['surveys_id']));
        $current_options = array_keys((array) $data['options']);
        
        // Delete the options left over
        foreach(array_diff($old_options, $current_options) as $survey_options_id){
            $db->query('DELETE FROM survey_options WHERE survey_options_id = ?', array($survey_options_id));
            $db->query('DELETE FROM survey_responses WHERE join_survey_options_id = ?', array($survey_options_id));
        }
        
        foreach($data['options'] as $key => $value){
            $survey_options->update(array(
                'survey_options_name' => $value,
                'survey_options_color' => $data['options_color'][$key]
            ), $key);
        }
        
        $surveys->update($data, $survey['surveys_id']);
        $surveys_id = $survey['surveys_id'];
        $notice = 'Survey updated';
    }
    else {
        $data['surveys_datetime'] = SQL('NOW()');
        $surveys->insert($data);
        $surveys_id = $surveys->last_id();
        $notice = 'Survey created';
    }
    
    
    foreach((array) $data['new_options'] as $key => $value){
        if(!$value){ continue; }
        
        $survey_options->insert(array(
            'survey_options_name' => $value,
            'survey_options_color' => $data['new_colors'][$key],
            'join_surveys_id' => $surveys_id
        ));
    }
    
    surveys::create_chart($surveys_id);
    
    http::redirect(BASEURL .'/admin/surveys/edit.php?'.http_build_query(array(
        'surveys_id' => $surveys_id,
        'notice' => $notice
    )));
}
    
        
?>
