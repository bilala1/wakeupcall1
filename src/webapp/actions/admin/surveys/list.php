<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$where = $params = array();

switch($_REQUEST['order']){
    default:
    case "title":
    
    $order = 'surveys_title';
    
    break;
    
    case "status":
    
    $order = 'surveys_status';
    
    break;
    
    case "date":
    
    $order = 'surveys_datetime';
    
    break;
    
    case "responses":
    
    $order = 'responses';
    
    break;
}

$orderby = strings::orderby($order);

$surveys = $db->fetch_all('SELECT s.*, COUNT(DISTINCT sr.survey_responses_id) AS responses FROM surveys AS s
    LEFT JOIN survey_responses AS sr ON sr.join_surveys_id = s.surveys_id
    '.strings::where($where).'
    GROUP BY s.surveys_id
    ORDER BY '.$orderby, $params
);

?>
