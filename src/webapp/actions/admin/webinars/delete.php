<?php
include_once 'protect.php';

//delete article
$webinars_table = new mysqli_db_table('webinars');
$webinars_table->delete($_REQUEST['webinars_id']);

http::redirect(FULLURL . '/admin/webinars/list.php?notice=' . urlencode('The webinar has been removed'));
?>