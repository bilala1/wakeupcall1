<?php

$db = mysqli_db::init();
include ACTIONS .'/admin/protect.php';

$webinar = $db->fetch_one('SELECT * FROM webinars WHERE webinars_id = ?', array($_REQUEST['webinars_id']));

$attendees = $db->fetch_all('
	select * from webinars_x_members 
	join members on members_id = join_members_id
	WHERE join_webinars_id = ?
	order by members_firstname ', 
	array($_REQUEST['webinars_id'])
);

if($_POST){
    $webinars = new mysqli_db_table('webinars');
    $data = $_POST;
    
    $validate = new validate($data);
    $validate->setOptional('webinars_keywords');
    $validate->setOptional('webinars_id');
    $validate->setOptional('webinars_connection_info');
    
    $data['webinars_keywords'] = explode(',', $data['webinars_keywords']);
    $data['webinars_keywords'] = array_unique($data['webinars_keywords']);
    $data['webinars_keywords'] = implode(',', $data['webinars_keywords']); // These aligned perfectly... weird
    
    $validate
        ->setExtensions('doc', 'docx', 'pdf', 'flv')
        ->setMimeTypes(
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'video/x-flv'
        );
    
    if($_FILES['webinars_file_recording']['size']){
        $validate->setFile($_FILES['webinars_file_recording'], 'webinars_file_recording');
    }
    if($_FILES['webinars_file1']['size']){
        $validate->setFile($_FILES['webinars_file1'], 'webinars_file1');
    }
    if($_FILES['webinars_file2']['size']){
        $validate->setFile($_FILES['webinars_file2'], 'webinars_file2');
    }
    
    if(!$_FILES['webinars_file1']['size'] && !$webinar['webinars_file1']){
        $validate->setOptional('webinars_file1_name');
    }
    if(!$_FILES['webinars_file2']['size'] && !$webinar['webinars_file2']){
        $validate->setOptional('webinars_file2_name');
    }
    
    $errors = $validate->test();
    
    if(!$errors){
        $uploaddir = SITE_PATH . WEBINAR_DIR;
        
        if($_FILES['webinars_file_recording']['size']){
            $filename = file::uniqname($uploaddir, file::get_extension($_FILES['webinars_file_recording']['name']));
            @move_uploaded_file($_FILES['webinars_file_recording']['tmp_name'], $uploaddir . $filename);
            
            $data['webinars_file_recording'] = $filename;
        }
        if($_FILES['webinars_file1']['size']){
            $filename = file::uniqname($uploaddir, file::get_extension($_FILES['webinars_file1']['name']));
            @move_uploaded_file($_FILES['webinars_file1']['tmp_name'], $uploaddir . $filename);
            
            $data['webinars_file1'] = $filename;
        }
        if($_FILES['webinars_file2']['size']){
            $filename = file::uniqname($uploaddir, file::get_extension($_FILES['webinars_file2']['name']));
            @move_uploaded_file($_FILES['webinars_file2']['tmp_name'], $uploaddir . $filename);
            
            $data['webinars_file2'] = $filename;
        }
        
        if($data['date']){
            $data['webinars_datetime'] = times::to_mysql_utc(date('Y-m-d', strtotime($data['date'])) . ' ' . date('H:i:s', strtotime($data['time'])));
		}
		
        if($webinar){
            $webinars->update($data, $webinar['webinars_id']);
            $webinars_id = $webinar['webinars_id'];
            $notice = 'webinar updated';
        }    
        else {
            //$data['webinars_datetime'] = SQL('NOW()');
            
            $webinars->insert($data);
            $webinars_id = $webinars->last_id();
            $notice = 'webinar added';
        }
        
        http::redirect(BASEURL .'/admin/webinars/edit.php?'.http_build_query(array(
            'webinars_id' => $webinars_id,
            'notice'  => $notice
        )));
    }else{
        //so user input is not lost
        $webinar = array_merge((array)$webinar, (array)$data);
    }
}

?>
