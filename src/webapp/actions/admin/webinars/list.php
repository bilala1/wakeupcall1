<?php
include ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();
$wheres = array();
$params = array();

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 20;
$max_pages = 10;     // only show 5 pages left or right

//------------------------------------------------------------------------------
//-- build wheres
//------------------------------------------------------------------------------
if($_REQUEST['type'] == 'past'){
    $wheres[] = 'webinars_datetime <= ?';
    $params[] = times::to_mysql_utc(date(DATE_FORMAT_FULL));
}else{
    $wheres[] = 'webinars_datetime > ?';
    $params[] = times::to_mysql_utc(date(DATE_FORMAT_FULL));
}

//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
//default order
if(!isset($_REQUEST['order'])){
    $_REQUEST['order'] = 'webinars_datetime';
    $_REQUEST['by'] = 'DESC';
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = ' ORDER BY ' . $db->escape_column($_REQUEST['order']) . ' ' . $by;

//------------------------------------------------------------------------------
//-- Get List
//------------------------------------------------------------------------------
$webinars = $db->fetch_all(
    'SELECT SQL_CALC_FOUND_ROWS *, (SELECT COUNT(*) FROM webinars_x_members WHERE join_webinars_id = w.webinars_id) AS participants
    FROM webinars AS w
    ' .
    strings::where($wheres) .
    $orderby .
    ' LIMIT ' . ($current_page * $per_page) . ', ' . $per_page, $params);
//echo $db->debug();
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$total = $db->found_rows();
$total_pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $total_pages);
?>

