<?php
include ACTIONS . '/admin/protect.php';

$db = mysqli_db::init();

$members = $db->fetch_all('SELECT m.* FROM members AS m
                           WHERE members_id IN 
                                (
                                    SELECT join_members_id
                                    FROM webinars_x_members
                                    WHERE join_webinars_id = ?
                                )
                           ', array($_REQUEST['webinars_id']));

foreach ($members as &$member){
    $member['contact_info'] = members::get_contact_info($member['members_id']);
    unset($member);
}

$webinar = $db->fetch_one('SELECT * FROM webinars WHERE webinars_id = ?', array($_REQUEST['webinars_id']));



?>