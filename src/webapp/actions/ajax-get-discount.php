<?

include "coupon_comparer.php";

$db = mysqli_db::init();

$comparer = new CouponComparer();

// AJAX --
if ($_REQUEST['ajax'] == 'coupon-pricing') {
    $result = $comparer->GetDiscountCodePricing($db, $_REQUEST['discount_code']);

    if(empty($result)){
        die('fail');
    }
    else {
        die(json_encode($result));
    }
} // - AJAX


// AJAX --
if ($_REQUEST['ajax'] == 'franchise-pricing') {
    $result = $comparer->GetFranchiseCodePricing($db, $_REQUEST['franchise_code']);

    if(empty($result)){
        die('fail');
    }
    else {
        die(json_encode($result));
    }
} // - AJAX


//die('........');
//
//$db = mysqli_db::init();
//
//$code = $db->fetch_one('
//	SELECT * FROM discount_codes WHERE discount_codes_code = ?
//	AND discount_codes_start <= NOW()
//	AND discount_codes_end >= NOW()',
//	array($_REQUEST['code'])
//);
//
//var_dump($code);
//
//if (empty($code)) {
//	die('fail');
//} else {
//	$result = array();
//	if ($code['discount_codes_amount'] > 0) {
//		$result['discount'] = money_format('%n', $code['discount_codes_amount']);
//		$result['total'] = money_format('%n', $_REQUEST['total'] - $code['discount_codes_amount']);
//	} else {
//		$result['discount'] = $code['discount_codes_percent'] . '%';
//		$result['total'] = money_format('%n', $_REQUEST['total'] * (1 - $code['discount_codes_percent'] / 100));
//	}
//	die(json_encode($result));
//}

?>