<?php
    $db = mysqli_db::init();
    
    if ($_REQUEST['value']) {
        $term = $_REQUEST['value'] . '%';

        //todo test licensed locations
        //$retVal = $db-fetch_all('SELECT distinct licensed')


        $retVal = $db->fetch_all('SELECT distinct licensed_locations_name AS title FROM licensed_locations WHERE licensed_locations_name LIKE ?', array($term));
        $corporations = $db->fetch_all('select distinct corporations_name as title from corporations where corporations_name like ?', array($term));
        $accounts = $db->fetch_all('select distinct accounts_name as title from accounts where accounts_name like ?', array($term));


        $retVal = array_merge($retVal, $corporations, $accounts);
        
        $retVal = array_unique($retVal);
        
        echo json_encode($retVal);
    }
    else {
        if($_REQUEST['hotel_name']) {

            //todo test licensed locations
            $retVal = $db->fetch_singlet('SELECT COUNT(*) FROM licensed_locations WHERE licensed_locations_name = ?',
                                            array(trim($_REQUEST['hotel_name'])));
            
            if(!$retVal){
                $retVal = $db->fetch_singlet('SELECT COUNT(*) FROM corporations WHERE corporations_name = ?',
                                                array(trim($_REQUEST['hotel_name'])));
            }
        }
        
        if($retVal) {
            echo json_encode('Already in db');
        }
        else {
            echo json_encode('Not in db');
        }
    }
?>
