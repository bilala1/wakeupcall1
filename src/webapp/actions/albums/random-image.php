<?
//Loops through all of the photos, giving a different one every day until the loop repeats
$db = mysqli_db::init();
$directory = SITE_PATH .'/data/albums/';
$members_id = ActiveMemberInfo::GetMemberId();
$show_pics = $db->fetch_singlet('SELECT DATE(NOW()) > members_datetime
								FROM members WHERE members_id = ?',
								array(ActiveMemberInfo::GetMemberId()));
if($show_pics) {
    if($_SESSION['day'] != date('z') || !$_SESSION['albums_photos_filename']){
        $db = mysqli_db::init();

        //Filter on albums 13.  Wouldn't normally hardcode ID, but this will be relatively static and also
        //The header is called often. So, would rather not have extra call.
        $total_albums_photos = $db->fetch_singlet('select count(*) from albums_photos where join_albums_id=13');

        $_SESSION['day'] = date('z');
        
        $mod = $_SESSION['day'] % $total_albums_photos;

        $_SESSION['albums_photos_filename'] = $db->fetch_singlet('
            select albums_photos_filename
            from albums_photos
            where join_albums_id=13
            order by albums_photos_sort
            limit ?, 1',
            array($mod)
        );
    }

    $filename = $_SESSION['albums_photos_filename'].'.org.jpg';
}
else {
    $filename = 'transparent.png';
}
#echo $directory.$filename;

header('Content-type:'.file::get_mime_type($directory.$filename));

readfile($directory.$filename);

?>
