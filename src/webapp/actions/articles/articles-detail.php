<?php
$db = mysqli_db::init();

//------------------------------------------------------------------------------
//-- article
//------------------------------------------------------------------------------
$article = $db->fetch_one('SELECT *, DATE_FORMAT(newsdate, "%M %d, %Y") AS newsdate 
    FROM articles WHERE active = 1 AND article_id = ?', array($_REQUEST['article_id']));
if(empty($article['page_url']))
{
    $params['article_id'] = $article['article_id'];
}
$article['page_url'] = $article['page_url'] ? $article['page_url'] : 'articles-detail.php';

if(!empty($params))
{
    $article['page_url'] .= '?' . http::build_query($params);
}

/*******************************************************************************
 * Pagination Settings
 ******************************************************************************/
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 5;
$max_pages = 5; // only should 5 pages left or right
/*******************************************************************************
 * article Comments
 ******************************************************************************/
$article_comments = $db->fetch_all('SELECT article_comments.*
    FROM article_comments 
    WHERE article_id = ? 
    ORDER BY post_date DESC
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page, 
    array($_REQUEST['article_id']));
/*******************************************************************************
 * Get Total Comments
 ******************************************************************************/
$total = $db->fetch_singlet('SELECT COUNT(*) FROM article_comments 
    WHERE article_id = ?', array($_REQUEST['article_id'])) + 1;
$pages = ceil($total / $per_page);
/*******************************************************************************
 * Comments Pagination
 ******************************************************************************/

if($pages > 1)
{
    $pages = html::paginate(
        $_GET, $current_page, $max_pages, $pages, FULLURL . '/' . $article['page_url']
    );
}
else 
{
    $pages = false;
}

foreach($article_comments as &$article_comment)
{
    $article_comment['body'] = html::bbcode_format($article_comment['body']);
    $article_comment['body'] = nl2br($article_comment['body']);
    
    unset($article_comment);
}
/*******************************************************************************
 * Post Comments
 ******************************************************************************/
if(!empty($_POST))
{
    $data = $_POST;
    $data['post_date'] = SQL('NOW()');
    $data['body'] = strip_tags($data['body']);
    if(empty($data['title']))
    {
        $errors['title'] = 'Please fill in a topic title';
    }
    if(empty($data['body']))
    {
        $errors['body'] = 'Please fill in a topic body';
    }
    
    if(empty($errors)) 
    {
        $posts = new mysqli_db_table('article_comments');
        $posts->insert($data);
        $article_comment_id = $posts->last_id();
        
        $redirect = FULLURL . '/' . $article['page_url'];
        $redirect .= strstr($redirect, '?') ? '&' : '?';
        $redirect .= 'notice=' . urlencode('Your comment as been posted') . '#comments';
        
        http::redirect($redirect);
    }    
}
?>
