<?php
$db = mysqli_db::init();

$article_group = $db->fetch_one('SELECT * FROM article_groups WHERE active = 1 AND article_group_id = ?', array($_REQUEST['article_group_id']));

$articles = $db->fetch_all('SELECT *, DATE_FORMAT(newsdate, "%M %d, %Y") AS newsdate 
    FROM articles WHERE active = 1 AND FIND_IN_SET(?, article_groups)', array($_REQUEST['article_group_id']));
    
foreach($articles as &$article) 
{
    $params = array();
    if(empty($article['page_url']))
    {
        $params['article_id'] = $article['article_id'];
    }
    $params['article_group_id'] = $_REQUEST['article_group_id'];
    $article['page_url'] = $article['page_url'] ? $article['page_url'] : 'articles-detail.php';
    $article['page_url'] .= '?' . http::build_query($params);
    unset($article);
}
?>
