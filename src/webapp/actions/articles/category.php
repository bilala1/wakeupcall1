<?php
//-- Category
$articles_categories = new article_categories();
$result = $articles_categories->search(array(
    'has_posts' => true
));
$category = $articles_categories->get($_REQUEST['articles_categories_id']);

//-- articles
$article_model = new articles;
$result = $article_model->search(array(
    'p' => $_REQUEST['p'],
    'perpg' => $_REQUEST['perpg'],
    'articles_categories_id' => $_REQUEST['articles_categories_id']
));
$articles = $result['articles'];
$pages = $result['pages'];
?>
