<?php
$db = mysqli_db::init();

//------------------------------------------------------------------------------
//-- article
//------------------------------------------------------------------------------
if(isset($_GET['previewing_article']))
{       
    // previewing
    $article = $_POST;
}
else
{
    $article = $db->fetch_one('SELECT *, DATE_FORMAT(articles_newsdate, "%M %d, %Y") AS articles_newsdate 
        FROM articles WHERE articles_active = 1 AND articles_id = ?', array($_REQUEST['articles_id']));
}

if(empty($article['articles_page_url']))
{
    $params['articles_id'] = $article['articles_id'];
}
$article['articles_page_url'] = $article['articles_page_url'] ? $article['articles_page_url'] : 'articles/detail.php';

if(!empty($params))
{
    $article['articles_page_url'] .= '?' . http::build_query($params);
}
//------------------------------------------------------------------------------
//-- Add View Count to article
//------------------------------------------------------------------------------
if(!isset($_GET['previewing_article'])){
    $db->query('UPDATE articles SET articles_views = articles_views + 1 WHERE articles_id = ?', array($_REQUEST['articles_id']));
}

/*******************************************************************************
 * Pagination Settings
 ******************************************************************************/
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 2;
$max_pages = 5; // only should 5 pages left or right
/*******************************************************************************
 * article Comments
 ******************************************************************************/
$article_comments = $db->fetch_all('SELECT articles_comments.*
    FROM articles_comments 
    WHERE join_articles_id = ? 
    ORDER BY articles_comments_post_date DESC
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page, 
    array($_REQUEST['articles_id']));
/*******************************************************************************
 * Get Total Comments
 ******************************************************************************/
$total = $db->fetch_singlet('SELECT COUNT(*) FROM articles_comments 
    WHERE join_articles_id = ?', array($_REQUEST['articles_id'])) + 1;
$pages = ceil($total / $per_page);
/*******************************************************************************
 * Comments Pagination
 ******************************************************************************/
if($pages > 1)
{
    $pages = html::paginate(
        $_GET, $current_page, $max_pages, $pages, FULLURL . '/' . $article['articles_page_url'] . '#comments'
    );
}
else 
{
    $pages = false;
}

foreach($article_comments as &$article_comment)
{
    $article_comment['body'] = html::bbcode_format($article_comment['body']);
    $article_comment['body'] = nl2br($article_comment['body']);
    
    unset($article_comment);
}
/*******************************************************************************
 * Post Comments
 ******************************************************************************/
if(!empty($_POST) && !isset($_GET['previewing_article']))
{
    $data = $_POST;
    $data['articles_comments_post_date'] = SQL('UTC_TIMESTAMP()');
    $data['articles_comments_body'] = strip_tags($data['articles_comments_body']);
    $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
    $data['join_articles_id'] = $data['articles_id'];
    if(empty($data['articles_comments_title']))
    {
        $errors['title'] = 'Please fill in a topic title';
    }
    if(empty($data['articles_comments_body']))
    {
        $errors['body'] = 'Please fill in a topic body';
    }
    
    if(empty($errors)) 
    {
        $posts = new mysqli_db_table('articles_comments');
        $posts->insert($data);
        $article_comment_id = $posts->last_id();
        points::addpoints(5, ActiveMemberInfo::GetMemberId(), 'article comment', array('articles_comments_id'=>$article_comment_id));
        articles::recalc_rating($data['articles_id']);
        
        $redirect = FULLURL . '/' . $article['articles_page_url'];
        $redirect .= strstr($redirect, '?') ? '&' : '?';
        $redirect .= 'notice=' . urlencode('Your comment as been posted') . '#comments';
        
        // Check watchdogs for people watching this article
        $dogs = $db->fetch_all('
            SELECT m.*
            FROM articles_watchdogs AS w
            LEFT JOIN members AS m ON m.members_id = w.join_members_id
            WHERE
                m.members_id != ? AND
                m.members_status != "locked" AND
                w.join_articles_id = ?
            GROUP BY m.members_id', array(ActiveMemberInfo::GetMemberId(), $data['articles_id']));
        
        foreach($dogs as $member)
        {
            $members_email = $member['members_email'];

            ob_start();
            include(VIEWS .'/emails/articles-comments-new.php');
            $message = ob_get_contents();
            ob_end_clean();

            mail::send(
                $from = SITE_EMAIL,
                $to = $members_email,
                SITE_NAME . ' | New Comment To A Watched article',
                $message = $message
            );
        }
        http::redirect($redirect);
    }
    else
    {
        $anchor = 'comment-form';
        //display errors, without redirecting to save comment values
    }
}

// Is this member watching this post?
$watching = $db->fetch_singlet('SELECT COUNT(*) FROM articles_watchdogs 
    WHERE join_members_id = ?
    AND join_articles_id = ?', array(ActiveMemberInfo::GetMemberId(), $article['articles_id']));

$comment_permission = !!ActiveMemberInfo::GetMemberId();
?>
