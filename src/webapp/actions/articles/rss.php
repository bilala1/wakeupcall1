<?php
//header('Content-type: application/rss+xml; charset="utf-8"');
header('Content-Type: text/xml');

$db = mysqli_db::init();

$items = array();

$articles = $db->fetch_all('SELECT * FROM articles WHERE articles_active = 1 ORDER BY articles_newsdate DESC LIMIT 10');
foreach($articles as $a)
{
    $items[strtotime($a['articles_newsdate']) . '-' . $a['articles_id']] = $a;
}

krsort($items);

$rss_link        = HTTP_FULLURL . '/articles/rss.php';
$rss_title       = SITE_NAME;
$rss_description = SITE_HEAD;
$rss_copyright   = SITE_HEAD;
$rss_generator   = SITE_NAME . ' RSS Builder';

$doc = new DOMDocument('1.0', 'utf-8');
$doc->formatOutput = true;

/*
 * Rss
 */
$rss = $doc->createElement('rss');
$rss = $doc->appendChild($rss);
$rss->setAttribute('version', '2.0');

/*
 * Channel
 */
$channel = $doc->createElement('channel');
$channel = $rss->appendChild($channel);

/*
 * Title
 */
$title = $doc->createElement('title');
$title = $channel->appendChild($title);
$title->appendChild($doc->createTextNode($rss_title));

/*
 * Link
 */
$link = $doc->createElement('link');
$link = $channel->appendChild($link);
$link->appendChild($doc->createTextNode($rss_link));

/*
 * Description
 */
$description = $doc->createElement('description');
$description = $channel->appendChild($description);
$description->appendChild($doc->createTextNode($rss_description));

/*
 * Language
 */
$language = $doc->createElement('language');
$language = $channel->appendChild($language);
$language->appendChild($doc->createTextNode('en-us'));

/*
 * Publish Date
 */
$pubdate = $doc->createElement('pubDate');
$pubdate = $channel->appendChild($pubdate);
$pubdate->appendChild($doc->createTextNode(date('D, j M Y H:i:s T')));

/*
 * Last Build Date
 */
$lastbuilddate = $doc->createElement('lastBuildDate');
$lastbuilddate = $channel->appendChild($lastbuilddate);
$lastbuilddate->appendChild($doc->createTextNode(date('D, j M Y H:i:s T')));

/*
 * Copyright
 */
$copyright = $doc->createElement('copyright');
$copyright = $channel->appendChild($copyright);
$copyright->appendChild($doc->createTextNode($rss_copyright));

/*
 * Generator
 */
$generator = $doc->createElement('generator');
$generator = $channel->appendChild($generator);
$generator->appendChild($doc->createTextNode($rss_generator));

foreach($items as $i)
{
    render_article_item($doc, $channel, $i);
}

echo $doc->saveXML();

function render_article_item(&$doc, &$channel, $article)
{
    /*
     * Item
     */
    $item = $doc->createElement('item');
    $item = $channel->appendChild($item);
    
    /*
     * Title
     */
    $title = $doc->createElement('title');
    $title = $item->appendChild($title);
    $title->appendChild($doc->createTextNode($article['articles_title']));  
    
    /*
     * Link
     */
    $params = array();
    if(empty($article['articles_page_url']))
    {
        $params['articles_id'] = $article['articles_id'];
    }
    $article['page_url'] = $article['articles_page_url'] ? $article['articles_page_url'] : 'articles-detail.php';
    $article['page_url'] .= !empty($params) ? '?' . http::build_query($params) : ''; 
    $link = $doc->createElement('link');
    $link = $item->appendChild($link);
    $link->appendChild($doc->createTextNode(HTTP_FULLURL . '/articles/' . $article['page_url']));   
    
    /*
     * Description
     */
    $article['articles_teaser'] .= '...';
    $description = $doc->createElement('description');
    $description = $item->appendChild($description);
    $description->appendChild($doc->createCDATASection($article['articles_teaser']));  
    
    /*
     * Publish Date
     */
    $pubdate = $doc->createElement('pubDate');
    $pubdate = $item->appendChild($pubdate);
    $pubdate->appendChild($doc->createTextNode(date('D, j M Y H:i:s T', strtotime($article['articles_newsdate']))));  
    
    /*
     * Guid
     */
    $guid = $doc->createElement('guid');
    $guid = $item->appendChild($guid);
    $guid->appendChild($doc->createTextNode(HTTP_FULLURL . '/articles/' . $article['articles_page_url']));   
}


?>
