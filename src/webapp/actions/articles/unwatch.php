<?php
$articles_id = (int) $_REQUEST['articles_id'];
$db = mysqli_db::init();

if(ActiveMemberInfo::GetMemberId()){
    $watchdogs = new mysqli_db_table('articles_watchdogs');
    $watchdogs->where('join_articles_id = ' . $db->filter($articles_id))
        ->where('join_members_id = ' . $db->filter(ActiveMemberInfo::GetMemberId()))
        ->delete();
}

http::redirect(BASEURL .'/articles/detail.php?articles_id=' . $articles_id);
?>
