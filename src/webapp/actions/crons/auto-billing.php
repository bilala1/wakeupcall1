<?
//members who are due for billing are billed

$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');
$bills_table = new mysqli_db_table('bills');
$corporations_table = new mysqli_db_table('corporations');

//get members that have an unpaid bill
$bills = $db->fetch_all('
    select *
    from members as m
    inner join bills as b on b.join_members_id = m.members_id
    inner join accounts as a on a.accounts_id = m.join_accounts_id
    where
        members_status != "locked" and members_status != "exempt" and
        (bills_type = "yearly" or bills_type = "monthly") and
        bills_paid = 0'
);



foreach($bills as $bill){
    //this is for debugging it will skip all but selected members
    if($_POST['user_email'] && $_POST['user_email'] != $bill['members_email']) {
        continue;
    }

    if(!!$bill['join_accounts_id_parent']) {
        $bill = array_merge(
            $bill,
            $db->fetch_one('
              SELECT m.* FROM members m
              INNER JOIN accounts ON join_members_id = members_id
              WHERE accounts_id = ?
            ', array($bill['join_accounts_id_parent']))
        );
    }

    list($approved, $transaction_error) = members::process_payment($bill['members_id'], $bill['bills_amount'], $bill['bills_description']);

    if($approved)
    {
        //update bill as paid
        $bills_table->update(array('bills_paid' => 1), $bill['bills_id']);
        $bill['last4'] = members::get_credit_last4($bill['members_stripe_id']);

        $locations = $db->fetch_all('select *
        from licensed_locations
        left join corporate_locations cl on licensed_locations_id = cl.join_licensed_locations_id
        where join_accounts_id = ? and
        licensed_locations_delete_datetime is null',
            array($bill['accounts_id']));

        $template = 'payment-receipt.php';
        $member_data = $bill;
        $member_locations = $locations;
        mail::send_template(
            $from     = MEMBERSHIP_EMAIL,
            $to       = $bill['members_email'],
            $subject  = 'Payment Receipt | '.SITE_NAME,
            $template = $template,
            $vars     = get_defined_vars()
        );

        $template = 'admin-auto-rewelcome-notification.php';
        mail::send_template(
            $from     = ADMIN_EMAIL,
            $to       = ADMIN_EMAIL,
            $subject  = 'Membership renewed for: ' . $bill['accounts_name'],
            $template = $template,
            $vars     = get_defined_vars()
        );
    }else{
        echo 'Error processing payment for account id '.$bill['accounts_id'].':'.$transaction_error.'<br />';
        
        //email admin. 
        notify::notify_admin_member_account_expired_payment_failed($bill['members_id']);
        
        //Send payment failed email to member
        mail::send_template(
            $from     = MEMBERSHIP_EMAIL,
            $to       = $bill['members_email'],
            $subject  = 'Credit Card Did Not Go Through for Renewal | '.SITE_NAME,
            $template = 'payment-failed.php',
            $vars     = get_defined_vars()
        );

        // Lock members and remove from Think HR
        $db->query("UPDATE members
                    SET members_status = 'locked',
                        members_elaw_agreement_yn = 0,
                        members_elaw_agreement_datetime = NULL
                    WHERE join_accounts_id = ?", array($bill['accounts_id']));

        $members = $db->fetch_all('SELECT members_id FROM members WHERE members_hr_agreement_yn = 1 AND join_accounts_id = ?', array($bill['accounts_id']));

        foreach($members as $member) {
            members::delete_member_from_think_hr($member['members_id']);
        }
    }
}

?>
