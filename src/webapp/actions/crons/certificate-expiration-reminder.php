<?
function getAllMembers()
{
    $db = mysqli_db::init();
    $members = $db->fetch_all('SELECT members_id,members_email,join_accounts_id,members_type from members
                                WHERE members_status != "locked"');
    return $members;
}

function getAllOtherEmails()
{
    $db = mysqli_db::init();
    $otherEmails = $db->fetch_all('
    SELECT distinct -1 as members_id, other_email as members_email, -1 as join_accounts_id, "user" as members_type FROM
    (select certificates_remind_other_expired as other_email from certificates
    UNION
    select certificates_remind_other as other_email from certificates) emails
    WHERE NOT EXISTS(select 1 from members where members_email = other_email)
    AND other_email is not NULL AND other_email != ""');
    return $otherEmails;
}

function getExpiredCertificates($member)
{
    $db = mysqli_db::init();
    $certificates = $db->fetch_all('
            select DATEDIFF(c.certificates_expire,CURDATE()) as days,m.*,c.*,v.*,ll.* FROM certificates c
            JOIN vendors v on v.vendors_id = c.join_vendors_id
            JOIN licensed_locations ll on licensed_locations_id = c.join_licensed_locations_id
            JOIN accounts on c.join_accounts_id = accounts_id
            LEFT JOIN certificate_remind_members crm on c.certificates_id = crm.join_certificates_id
            LEFT JOIN members m on crm.join_members_id = m.members_id
            WHERE accounts_is_locked = 0
            AND c.certificates_hidden = 0
            AND licensed_locations_delete_datetime IS NULL
            AND c.certificates_expire <= CURDATE()
            AND
            (
                (
                    m.members_id = ?
                    AND crm.certificates_remind_after_expiry = 1
                )
                OR c.certificates_remind_other_expired = ?
            )
            order by days
        ', array($member['members_id'], $member['members_email']));
    return $certificates;
}

function getExpiringCertificates($member)
{
    $db = mysqli_db::init();
    $certificates = $db->fetch_all('
            select DATEDIFF(c.certificates_expire,CURDATE()) as days,m.*,c.*,v.*,ll.* FROM certificates c
            JOIN vendors v on v.vendors_id = c.join_vendors_id
            JOIN licensed_locations ll on licensed_locations_id = c.join_licensed_locations_id
            JOIN accounts on c.join_accounts_id = accounts_id
            LEFT JOIN certificate_remind_members crm on c.certificates_id = crm.join_certificates_id
            LEFT JOIN members m on crm.join_members_id = m.members_id
            WHERE accounts_is_locked = 0
            AND c.certificates_hidden = 0
            AND licensed_locations_delete_datetime IS NULL
            AND c.certificates_expire >= CURDATE()
            AND DATEDIFF(c.certificates_expire,CURDATE()) <= certificates_email_days
            AND
            (
                (
                    m.members_id = ?
                    AND crm.certificates_remind_before_expiry = 1
                )
                OR c.certificates_remind_other = ?
            )
            order by days
        ', array($member['members_id'], $member['members_email']));
    return $certificates;
}

$members = array_merge(getAllMembers(), getAllOtherEmails());
foreach ($members as $member) {
    $expiredCertificates = getExpiredCertificates($member);
    $expiringCertificates = getExpiringCertificates($member);

    if(empty($expiredCertificates) && empty($expiringCertificates)) {
        continue;
    }
    $member_email = $member['members_email'];

    ob_start();
    include(VIEWS . '/emails/certificate-expiration.php');
    $message = ob_get_contents();
    ob_end_clean();

    mail::send(
        $from = AUTO_EMAIL,
        $to = $member_email,
        $subject = 'Certificate Of Insurance Expirations',
        $message = $message
    );
}