<?php
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

$client = new SoapClient('https://ws.interfax.net/dfs.asmx?wsdl');
$username = 'wakeupcall'; // Insert your InterFAX username here
$password = 'wakeup01'; // Insert your InterFAX password here

$params = new stdClass();
$params->Username = $username;
$params->Password = $password;
$params->MaxItems = 1;
$params->TotalCount = '';
$params->ListSize = '';
$params->ResultCode = '';

$claims = new mysqli_db_table('claims');
$claims_files = new mysqli_db_table('claims_files');
$claims_faxes = new mysqli_db_table('claims_faxes');
$claims_faxes_files = new mysqli_db_table('claims_faxes_files');

$faxes = $claims_faxes->fetch_all(
    'SELECT * FROM claims_faxes
     inner join claims on join_claims_id = claims_id
     inner join carriers on join_carriers_id = carriers_id
     inner join members on claims_faxes_submitted_by = members_id
     WHERE claims_faxes_status < 0');

foreach ($faxes as $fax) {
    $params->LastTransactionID = $fax['claims_faxes_submission_id'] + 1;

    $query_result = $client->FaxStatusEx($params);
    $fax_status = $query_result->FaxStatusExResult->FaxItemEx->Status;
    
    if($fax_status == 0) $status = "success";
    if($fax_status < 0 ) $status = "pending";
    if($fax_status > 0 ) $status = "fail";
    $submission_id = $fax['claims_faxes_submission_id'];
    $db = mysqli_db::init();
    $db->query("UPDATE sent_emails_history set sent_emails_history_status = ? where sent_emails_history_submission_id =  ?",array($status,$submission_id));

    $claims_faxes->update(array('claims_faxes_status' => $fax_status), $fax['claims_faxes_id']);
    if ($fax_status < 0) {
        //Still processing
        continue;
    }

    $fax['fax_item'] = $query_result->FaxStatusExResult->FaxItemEx;

    if ($fax_status == 0) {
        // Get the claim - if it's not one of the submitted statuses, change it to submitted - open;
        $claim = $claims->get($fax['claims_id']);
        $claims_update_array = array('claims_filed' => 1);

        if ($claim['join_claims_status_codes_id'] != claims::$SUBMIT_OPEN
            && $claim['join_claims_status_codes_id'] != claims::$SUBMIT_CLOSE) {
            $claims_update_array['join_claims_status_codes_id'] = claims::$SUBMIT_OPEN;
        }
        $claims->update($claims_update_array, $fax['join_claims_id']);

        //Look into 'CompletionTime' property
        //Update submitted files
        $files = $claims_faxes_files->fetch_all('SELECT * FROM claims_faxes_files WHERE join_claims_faxes_id = ?', array($fax['claims_faxes_id']));
        $claims_files_update = array('claims_files_submitted' => $fax['claims_faxes_submitted_datetime']);
        foreach ($files as $file) {
            $claims_files->update($claims_files_update, $file['join_claims_files_id']);
        }

        claims_history::create_claim_entry($fax['members_id'], $fax['claims_id'], 'Claim fax submission successfully sent.');

        mail::send_template(
            CONCIERGE_EMAIL,
            $fax['members_email'],
            'Successful Fax Submission to ' . $fax['carriers_fax'],
            'fax-sent.php',
            $fax);

    } else if ($fax_status > 0) {
        //error oh noes!
        $claims->update(array('join_claims_status_codes_id' => claims::$FAILED), $fax['join_claims_id']);

        claims_history::create_claim_entry($fax['members_id'], $fax['claims_id'], 'Claim fax submission <b>failed</b> to send to '.$fax['carriers_name']);
        mail::send_template(
            CONCIERGE_EMAIL,
            $fax['members_email'],
            'Failed Fax Submission to ' . $fax['carriers_fax'],
            'fax-failed.php',
            $fax);
    }
    
    
}
?>
