<?
//Emails a contract expiration reminder to the member's email

$days_before = array(180, 120, 90, 60, 30, 7, 0);

$db = mysqli_db::init();

$contracts_table = new mysqli_db_table('contracts');

$contracts =  $db->fetch_all('select *, DATEDIFF(c.contracts_expiration_date,CURDATE()) as days from contracts c'
        . ' WHERE DATEDIFF(c.contracts_expiration_date,CURDATE()) = 0');
foreach ($contracts as $contract) {
    if(($contract['contracts_status']=='Active' && $contract['contracts_transition_status_to'] == 'Autorenew-Annual')
       || ($contract['contracts_status']== 'Autorenew-Annual') ){
        
       $expiration_date = $contract['contracts_expiration_date'];
       $new_expiration_date =  date('Y-m-d H:i:s', strtotime('+1 year', strtotime($expiration_date)));
       $db->query('UPDATE contracts
                SET contracts_expiration_date = ?
                WHERE contracts_id = ?', array($new_expiration_date, $contract['contracts_id']));

        $newContract = $db->fetch_one('select * from contracts where contracts_id = ?', array($contract['contracts_id']));
        contracts_history::contract_updated_entry(null, $newContract, $contract);
    }
}
foreach ($days_before as $db4) {
    $contracts = $db->fetch_all('
        select * from contracts c
        inner join accounts a on c.join_accounts_id = a.accounts_id
        inner join members m on a.join_members_id = m.members_id
        where
        contracts_status = \'active\' and
        contracts_hidden = 0 and
        (contracts_expiration_date - INTERVAL ' . (int)$db4 . ' DAY) = CURDATE() and
        contracts_expiration_date >= CURDATE() and
        contracts_expiration_notify_lead_days >= ' . (int)$db4
    );

    foreach ($contracts as $contract) {
        //Need to verify it's not only for a deleted location
        if($contract['contracts_all_locations'] != 1) {
            $llCount = $db->fetch_singlet('SELECT COUNT(*) FROM contracts_x_licensed_locations
              INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
              WHERE licensed_locations_delete_datetime IS NULL
                AND join_contracts_id = ?', array($contract['contracts_id']));
            if($llCount == 0) {
                continue;
            }
        }

        if ($db4 == 0) {
            $db->query('UPDATE contracts
                SET contracts_remind_member_date_last = CURDATE(),
                contracts_status = contracts_transition_status_to
                WHERE contracts_id = ?', array($contract['contracts_id']));

            $transition_phrase = '';
            switch ($contract['contracts_transition_status_to']) {
                case 'Month to Month':
                    $transition_phrase = ' and has changed to a Month-to-Month contract';
                    break;
                case 'Inactive':
                    $transition_phrase = ' and has become inactive';
            };

            $newContract = $db->fetch_one('select * from contracts where contracts_id = ?', array($contract['contracts_id']));
            contracts_history::contract_updated_entry(null, $newContract, $contract);
        }

        $emailRecipients = contracts::getNotificationEmailsForContract($contract);
        $licensed_locations = $db->fetch_all('
            select *
            from licensed_locations ll
            inner join contracts_x_licensed_locations cxll on ll.licensed_locations_id = cxll.join_licensed_locations_id
            where cxll.join_contracts_id = ?', array($contract['contracts_id']));

        foreach ($emailRecipients as $location) {
            ob_start();
            if ($db4 > 0) {
                include(VIEWS . '/emails/contract-expiration-reminder.php');
                $subject = 'Contract Expiring Soon';
            } else {
                include(VIEWS . '/emails/contract-expired.php');
                $subject = 'Contract Expired';
            }
            $message = ob_get_contents();
            ob_end_clean();
            //echo $message;

            try {
                mail::send(
                    $from = AUTO_EMAIL,
                    $to = $location['members_email'],
                    $subject = $subject . ' | ' . SITE_NAME,
                    $message = $message
                );
            } catch(Exception $ex) {
                //Don't let a single failed mail break everyone... but what to do?  Email site admin?

            }
        }

        $db->query('UPDATE contracts
                SET contracts_remind_member_date_last = CURDATE()
                WHERE contracts_id = ?', array($contract['contracts_id']));
    }
}

//Change status of expired contracts
$contracts = $db->fetch_all('
    SELECT *
    FROM contracts
    WHERE
        contracts_status = \'Active\'
        AND contracts_expiration_date <= CURDATE()'
);

foreach ($contracts as $contract) {
    $contracts_table->update(array('contracts_status' => $contract['contracts_transition_status_to']), $contract['contracts_id']);

    $newContract = $db->fetch_one('select * from contracts where contracts_id = ?', array($contract['contracts_id']));
    contracts_history::contract_updated_entry(null, $newContract, $contract);
}

//Nag members about expired contracts
$contracts = $db->fetch_all('
    SELECT *
    FROM contracts c
    inner join accounts a on c.join_accounts_id = a.accounts_id
    inner join members m on a.join_members_id = m.members_id
    WHERE
        contracts_expired_notification = 1
        AND c.contracts_hidden = 0
        AND contracts_expiration_date <= CURDATE()
        AND contracts_status != \'Inactive\'
        AND contracts_remind_member_date_last <= CURDATE() - INTERVAL 1 WEEK'
);

foreach ($contracts as $contract) {
    //Need to verify it's not only for a deleted location
    if($contract['contracts_all_locations'] != 1) {
        $llCount = $db->fetch_singlet('SELECT COUNT(*) FROM contracts_x_licensed_locations
              INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
              WHERE licensed_locations_delete_datetime IS NULL
                AND join_contracts_id = ?', array($contract['contracts_id']));
        if($llCount == 0) {
            continue;
        }
    }

    $emailRecipients = contracts::getNotificationEmailsForContract($contract);
    $licensed_locations = $db->fetch_all('
            select *
            from licensed_locations ll
            inner join contracts_x_licensed_locations cxll on ll.licensed_locations_id = cxll.join_licensed_locations_id
            where cxll.join_contracts_id = ?', array($contract['contracts_id']));

    foreach ($emailRecipients as $location) {
        ob_start();
        include(VIEWS . '/emails/contract-expired.php');
        $message = ob_get_clean();

        try {
            mail::send(
                $from = AUTO_EMAIL, // used to be SALES_EMAIL
                $to = $location['members_email'],
                $subject = 'Contract Expired | ' . SITE_NAME,
                $message = $message
            );
        } catch(Exception $ex) {
            //Don't let a single failed mail break everyone... but what to do?  Email site admin?

        }
    }

    // update 
    $db->query('UPDATE contracts 
                SET contracts_remind_member_date_last = CURDATE()
                WHERE contracts_id = ?', array($contract['contracts_id']));
}

?>
