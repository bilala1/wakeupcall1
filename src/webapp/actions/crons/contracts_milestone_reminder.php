<?
$db = mysqli_db::init();

//crone for contracts milestone starts

$contracts_milestones = $db->fetch_all(
    "SELECT *, DATEDIFF(contracts_milestones_date,CURDATE()) as num_days from contracts_milestones cm
      INNER JOIN contracts_milestones_remind_days cmr ON cmr.join_contracts_milestones_id = cm.contracts_milestones_id
      INNER JOIN contracts on cm.join_contracts_id = contracts_id
      INNER JOIN accounts ON join_accounts_id = accounts_id
      WHERE contracts_hidden = 0
        AND accounts_is_locked = 0
        AND DATEDIFF(contracts_milestones_date,CURDATE()) = cmr.contracts_milestones_remind_days");

foreach ($contracts_milestones as $contracts_milestone) {
    $remind_days = $contracts_milestone['num_days'];
    $contracts_id = $contracts_milestone['join_contracts_id'];
    $contracts_milestone_id = $contracts_milestone['contracts_milestones_id'];
    $emailRecipients = contracts::getNotificationEmailsForContractsMilestones($contracts_milestone['contracts_milestones_id']);
    foreach ($emailRecipients as $emailRecipient) {
        ob_start();
        include(VIEWS . '/emails/contracts_milestone_reminder.php');
        $subject = 'Contracts Milestone Reached';

        $message = ob_get_contents();
        ob_end_clean();
        //echo $message;

        try {
            mail::send(
                $from = AUTO_EMAIL,
                $to = $emailRecipient['members_email'],
                $subject = $subject . ' | ' . SITE_NAME,
                $message = $message
            );
        } catch (Exception $ex) {
            //Don't let a single failed mail break everyone... but what to do?  Email site admin?

        }
    }
}


//crone for contracts milestone ends
?>
