<?
//Create yearly bills for members

$db = mysqli_db::init();
$bills_table = new mysqli_db_table('bills');
$bill_description = 'Yearly subscription';

//----- CORPORATIONS

//get corporations that have not had a yearly or monthly bill for the appropriate interval
$corp_members = $db->fetch_all('
    select *
    from members as m
    join accounts as a on a.join_members_id = m.members_id
    where
        members_status = "member" and
        accounts_delete_datetime is null and
        members_id not in
            (select join_members_id
             from bills
             where
                (bills_type = "yearly" and
                bills_datetime > NOW() - INTERVAL 1 YEAR) or
                (bills_type = "monthly" and
                bills_datetime > NOW() - INTERVAL 1 MONTH))'
);

//create yearly bill for these members
foreach($corp_members as $member){

    //this is for debugging it will skip all but selected members
    if($_POST['user_email'] && $_POST['user_email'] != $member['members_email'])
        continue;

    $billing_info = billing::calculateNextBillForAccount($member['accounts_id']);

    if($billing_info['discount_codes_id']) {
        $db->query('UPDATE discount_codes SET discount_codes_uses = discount_codes_uses + ? WHERE discount_codes_id = ?',
            array($billing_info['num_discounted'], $billing_info['discount_code']));
    }

    $bills_paid = 0;
    if($billing_info['bills_amount'] == 0) {
        $bills_paid = 1;
    }

    //record bill
    $bills_table->insert(array(
        'join_members_id' => $member['members_id'],
        'bills_amount' => $billing_info['bills_amount'],
        'bills_type' => $member['members_billing_type'],
        'bills_description' => $billing_info['bills_description'],
        'bills_datetime' => SQL('NOW()'),
        'bills_paid' => $bills_paid,
        'join_discount_codes_id' => $billing_info['discount_codes_id']
    ));

    $bill = $bills_table->get($bills_table->last_id());

    //send out notification e-mails
    $member = array_merge($member, members::get_customer_credit_card_data($member), $billing_info);
    $member['next_bills_datetime'] = $bill['bills_datetime'];
    $template = 'full-membership-auto-rewelcome-corporation.php';

    if(!$bills_paid) {
        mail::send_template(
            $from = MEMBERSHIP_EMAIL,
            $to = $member['members_email'],
            $subject = 'Continuing Membership | ' . SITE_NAME,
            $template = $template,
            $vars = get_defined_vars()
        );
    }
}

?>