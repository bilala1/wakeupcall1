<?php
//this script deletes files and db info. per member

$db = mysqli_db::init();

$delete_date = date('Y-m-d',strtotime('now - '.DELETE_ENTITY_DAYS.' days'));

// ---------------------------------------------------------------------------------------------
// - accounts to delete
//----------------------------------------------------------------------------------------------
$accounts_to_delete = $db->fetch_singlets('SELECT accounts_id
                                     FROM accounts
                                     WHERE DATE(accounts_delete_datetime) <= ?',array($delete_date));
//if($accounts_to_delete){
//    foreach($accounts_to_delete as $account_id){
//        accounts::delete_account($account_id);
//    }
//}


// ---------------------------------------------------------------------------------------------
// - licensed_locations to delete
//----------------------------------------------------------------------------------------------
$licensed_locations_to_delete = $db->fetch_singlets('SELECT licensed_locations_id
                                     FROM licensed_locations
                                     WHERE DATE(ll.licensed_locations_delete_datetime) <= ?',array($delete_date));
//if($licensed_locations_to_delete){
//    foreach($licensed_locations_to_delete as $licensed_locations_id){
//        licensed_locations::delete($licensed_locations_id);
//    }
//}
