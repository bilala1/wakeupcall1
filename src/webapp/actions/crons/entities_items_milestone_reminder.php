<?
$db = mysqli_db::init();

//crone for entities items milestone starts

$entities_items_milestones = $db->fetch_all(
    "SELECT *, DATEDIFF(entities_items_milestones_date,CURDATE()) as num_days from entities_items_milestones em
      INNER JOIN entities_items_milestones_remind_days emr ON emr.join_entities_items_milestones_id = em.entities_items_milestones_id
      INNER JOIN entities_items on em.join_entities_items_id = entities_items_id
      INNER JOIN entities on join_entities_id = entities_id
      INNER JOIN accounts ON join_accounts_id = accounts_id
      WHERE DATEDIFF(entities_items_milestones_date,CURDATE()) = emr.entities_items_milestones_remind_days
        AND accounts_is_locked = 0
        AND entities_hidden = 0");

foreach ($entities_items_milestones as $entities_items_milestone) {
    $remind_days = $entities_items_milestone['num_days'];
    $entities_items_id = $entities_items_milestone['join_entities_items_id'];
    $entities_items_milestone_id = $entities_items_milestone['entities_items_milestones_id'];
    $emailRecipients = entities_items::getNotificationEmailsForEntityItemMilestone($entities_items_milestone['entities_items_milestones_id']);

    foreach ($emailRecipients as $emailRecipient) {
        ob_start();
        include(VIEWS . '/emails/entities_items_milestone_reminder.php');
        $subject = 'Tracked Item Milestone Reached';

        $message = ob_get_contents();
        ob_end_clean();
        //echo $message;

        try {
            mail::send(
                $from = AUTO_EMAIL,
                $to = $emailRecipient['members_email'],
                $subject = $subject . ' | ' . SITE_NAME,
                $message = $message
            );
        } catch (Exception $ex) {
            //Don't let a single failed mail break everyone... but what to do?  Email site admin?

        }
    }
}


//crone for entities items milestone ends
?>
