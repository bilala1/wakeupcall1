<?
//Member gets another email if their free membership has been expired for 2 weeks, 2 months, 4 months

$durations = array('2 WEEK', '2 MONTH', '4 MONTH');     //must be SQL safe!

$db = mysqli_db::init();

foreach($durations as $duration){
    $members = $db->fetch_all('
        SELECT *
        FROM members
        WHERE
            DATE(members_datetime) = DATE_SUB(DATE_SUB(CURDATE(), INTERVAL '.$duration.'), INTERVAL 15 DAY) 
            AND members_status = "free"'
    );
    
    $template = stristr($duration, 'MONTH') ? 'free-membership-expired-reminder-months.php' : 'free-membership-expired-reminder.php';

    foreach((array)$members as $member){
        mail::send_template(
            $from     = MEMBERSHIP_EMAIL,
            $to       = $member['members_email'],
            $subject  = 'Free Membership Expired | ' . SITE_NAME,
            $template = $template,
            $vars     = get_defined_vars()
        );
    }
}

?>