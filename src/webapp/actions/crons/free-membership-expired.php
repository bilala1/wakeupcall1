<?php

//After a member has been a 'free' member for 15 days, they get sent an email with a "benefits of full membership" 

$db = mysqli_db::init();

$members = $db->fetch_all('SELECT *
                           FROM members
                           WHERE members_status = "free" AND DATE(members_datetime) <= DATE_SUB(CURDATE(),INTERVAL '.(int)FREE_MEMBER_PERIOD.' DAY)');

if($members){

    foreach($members as $member){

        //set new stataus
        //$db->query('UPDATE members SET members_status = "locked" WHERE members_id = ? ', array($member['members_id']));
        members::set_members_status($member['members_id'], 'locked');

        if($member['members_hr_agreement_yn'] == '1') {
            members::delete_member_from_think_hr($member['members_id']);
        }
        if($member['members_elaw_agreement_yn'] == '1') {
            // Update the member table to make sure they have to re-agree and be re-created in the future.
            $db->query('UPDATE members
                            SET members_elaw_agreement_yn = 0, members_elaw_agreement_datetime = NULL
                            WHERE members_id = ? ', array($member['members_id']));
        }

        //send email
        //email members
        ob_start();
        include(VIEWS . '/emails/free-membership-expired.php');
        $message = ob_get_contents();
        ob_end_clean();

        mail::send(
            $from = MEMBERSHIP_EMAIL,
            $to = $member['members_email'],
            $subject =  'End of 15 Day Free Trial | ' . SITE_NAME,
            $message = $message
        );
        
        //notify and admin about each person whose account has expired
        notify::notify_admin_member_free_trial_expired($member['members_id']);
    }
}


?>