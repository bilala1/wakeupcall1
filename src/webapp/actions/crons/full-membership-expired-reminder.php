<?
//Member gets another email if their full membership has been expired for 1 month


$durations = array('1 MONTH', '3 MONTH');     //must be SQL safe!

$db = mysqli_db::init();

foreach($durations as $duration){
    $members = $db->fetch_all('
        SELECT *
        FROM members AS m
        JOIN bills AS b ON (m.members_id = b.join_members_id)
        WHERE
            DATE(b.bills_datetime) = DATE_SUB(CURDATE(), INTERVAL '.$duration.') 
            AND members_status = "locked"
            AND bills_paid = 0'
    );
    
    $template = stristr($duration, '3 MONTH') ? 'full-membership-expired-reminder-months.php' : 'full-membership-expired-reminder.php';

    foreach((array)$members as $member){
        mail::send_template(
            $from     = MEMBERSHIP_EMAIL,
            $to       = $member['members_email'],
            $subject  = 'Full Membership Expired | ' . SITE_NAME,
            $template = $template,
            $vars     = get_defined_vars()
        );
    }
}

?>