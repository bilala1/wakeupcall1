<?php
    $db = mysqli_db::init();

    function SendReminder($members)
    {
        $template = 'full-membership-auto-rewelcome-corporation.php';
        
        foreach($members as $member) {
            $billing_info = billing::calculateNextBillForAccount($member['join_accounts_id']);
            $member = array_merge($member, members::get_customer_credit_card_data($member), $billing_info);

            mail::send_template(
                $from     = MEMBERSHIP_EMAIL,
                $to       = $member['members_email'],
                $subject  = 'Continuing Membership | '.SITE_NAME,
                $template = $template,
                $vars     = get_defined_vars()
            );
        }
    }

    //expiring in 1 week
    $members = $db->fetch_all('SELECT members_firstname,members_lastname,members_stripe_id,
                                 members_email,bills_datetime,members_billing_type,join_accounts_id, DATE(bills_datetime + INTERVAL 1 YEAR) as next_bills_datetime
                            FROM members
                            JOIN bills as b on b.join_members_id = members_id
                            WHERE
                                members_status != "locked" AND
                                members_status != "exempt" AND
                                bills_type = "yearly" AND
                                DATE(bills_datetime + INTERVAL 1 YEAR - INTERVAL 7 DAY) = CURDATE()');
    
    SendReminder($members);
    
    //expiring in 30 days
    $members = $db->fetch_all('SELECT members_firstname,members_lastname,members_stripe_id,
                                 members_email,bills_datetime,members_billing_type,join_accounts_id, DATE(bills_datetime + INTERVAL 1 YEAR) as next_bills_datetime
                            FROM members
                            JOIN bills as b on b.join_members_id = members_id
                            WHERE
                                members_status != "locked" AND
                                members_status != "exempt" AND
                                bills_type = "yearly" AND
                                DATE(bills_datetime + INTERVAL 1 YEAR - INTERVAL 30 DAY) = CURDATE()');
    
    SendReminder($members);
?>
