<?php

/**
 * Migrate the mebers to certificate_remind_members table if remind option is checked
 * Created by NetBeans.
 * User: Vidya
 * Date: 16/08/16
 * Time: 2:10 PM
 */
function add_to_certificate_remind_members($member_id,$certificate_id,$remind_before_expiry,$remind_after_expiry){
   $db = mysqli_db::init();
   $certificates_remind_members_table = new mysqli_db_table('certificate_remind_members');
   $remind_members = array();
       $remind_members['join_members_id'] = $member_id;
       $remind_members['join_certificates_id'] = $certificate_id;
       $remind_members['certificates_remind_before_expiry'] = $remind_before_expiry;
       $remind_members['certificates_remind_after_expiry'] = $remind_after_expiry;
       
       $certificates_remind_members_table->insert($remind_members);
   
}
$db = mysqli_db::init();
$certificates = $db->fetch_all('
        select *
        from certificates c
        left join licensed_locations ll on licensed_locations_id = join_licensed_locations_id
        where
        (certificates_remind_admin_expired =? OR certificates_remind_location_expired=?
        OR certificates_send_copy = ? OR certificates_remind_corporate_admin = ? )  ',array(1,1,1,1)
    );
foreach($certificates as $certificate){
    certificates::delete_remind_members_by_certificates_id($certificate['certificates_id']);
    if($certificate['certificates_send_copy'] || $certificate['certificates_remind_location_expired']) {
        $location_admins = $db->fetch_all('
            SELECT members_id FROM members
            INNER JOIN licensed_locations_x_members_access ON members_id = join_members_id
            INNER JOIN members_access_levels ON join_members_access_levels_id = members_access_levels_id
            WHERE members_access_levels_type = \'locationAdmin\'
            AND join_licensed_locations_id = ?', array($certificate['licensed_locations_id']));
        foreach($location_admins as $admin) {
            add_to_certificate_remind_members($admin['members_id'],$certificate['certificates_id'],$certificate['certificates_send_copy'],$certificate['certificates_remind_location_expired']);
        }
    }
    if($certificate['certificates_remind_corporate_admin'] || $certificate['certificates_remind_admin_expired']) {

        $account_admins = $db->fetch_all('
                SELECT members_id FROM members
                 WHERE members_type = \'admin\'
                   AND join_accounts_id = ?', array($certificate['join_accounts_id']));
        foreach($account_admins as $admin) {
           add_to_certificate_remind_members($admin['members_id'],$certificate['certificates_id'],$certificate['certificates_remind_corporate_admin'],$certificate['certificates_remind_admin_expired']);
        }
    }
}
