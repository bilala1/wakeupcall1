<?
//The script is ran nightly
//error_log('wakeupcall nightly ran', 1, 'test@wakeupcall.netm');

include 'vendor-certificate-request.php';
if(date('l') == 'Wednesday'){
    include 'certificate-expiration-reminder.php';
}

include 'contract-expiration-reminder.php';

//After a member has been a 'free' member for 15 days,
//they get sent an email with a "benefits of full membership"
// ALSO: sets status from 'free' to 'locked'
include 'free-membership-expired.php';

//auto billing
include('create-bills.php');
include('auto-billing.php');

//Weekly Newsletter
if(date('l') == 'Monday'){
    include 'weekly-newsletter.php';
}

//60 Days after billing expired
//include '60-days.php';    //does not work!

//2 weeks, 2 months, 4 months after full membership expired (auto bill didn't work?)
include 'full-membership-expired-reminder.php';

//2 weeks, 2 months, 4 months after free trial expired
include 'free-membership-expired-reminder.php';

//Delete members that have been 'deleted' for 60 days
include 'delete-members.php';

//have not rejoined in a while

//remind about registered webinar
include 'webinar-reminder-registered.php';

//remind about unregistered webinar
include 'webinar-reminder-unregistered.php';

//-- rss updates
include 'update-rss.php';

//-- reminder of auto-renewal
include 'membership-renewal-reminder.php';

//entity item milestone reminder
include 'entities_items_milestone_reminder.php';

//contracts milestone reminder
include 'contracts_milestone_reminder.php';

include 'process-access-logs.php';
?>
