<?php

$db = mysqli_db::init();
$dailySummarys = new mysqli_db_table('daily_summarys');
$accessLogs = new mysqli_db_table('access_logs');

$cats = array(
    '/members/index' => 'dashboard',
    '/members/my-documents/certificates' => 'certs',
    '/members/vendors' => 'certs',
    '/members/my-documents' => 'files',
    '/members/claims' => 'claims',
    '/members/contracts' => 'contracts',
    '/members/documents' => 'library',
    '/members/forums' => 'forums',
    '/members/entities' => 'businesses',
    '/members/hr' => 'hr',
    '/members/employmentlaw' => 'elaw',
    '/members/webinars' => 'webinars',
    '/members/search' => 'search',
    '/members/news' => 'news',
    '/members' => 'other'
);

$ignores = array(
    '/admin/',
    '/API/'
);

/*
Other cool queries:

select accounts_name, licensed_locations_name,
	(select count(1) from claims where join_licensed_locations_id = licensed_locations_id) as num_claims,
	(select count(1) from certificates where join_licensed_locations_id = licensed_locations_id) as num_certs,
	(select count(1) from contracts c left join contracts_x_licensed_locations on contracts_id = join_contracts_id
		where c.join_accounts_id = accounts_id and (contracts_all_locations = 1 or join_licensed_locations_id = licensed_locations_id)) as num_contracts
 from licensed_locations
 inner join accounts a on join_accounts_id = accounts_id
 inner join members on a.join_members_id = members_id
 where (members_status = 'member' or accounts_id in (50, 230, 399, 429, 460)) and accounts_id not in (450, 451)
 order by accounts_name, licensed_locations_name;

select accounts_name, m.members_firstname, m.members_lastname,
   (SELECT COALESCE(datediff(NOW(), min(members_logins_datetime)) + 1, 0) from members_logins where join_members_id = m.members_id) as total_days,
   (select count(distinct DATE(members_logins_datetime)) from members_logins where join_members_id = m.members_id) as days_logged_in
from members m
inner join accounts on accounts_id = join_accounts_id
inner join members m2 on join_members_id = m2.members_id
 where (m2.members_status = 'member' or accounts_id in (50, 230, 399, 429, 460)) and accounts_id not in (450, 451)
 order by accounts_name, members_lastname, members_firstname;



 */
function getOrCreateSummary($log, $db) {
    // This isn't re-used because it doesn't reset between inserts, causing auto-id PK issues.
    $dailySummarys = new mysqli_db_table('daily_summarys');
    $summaryQuery = "SELECT * FROM daily_summarys WHERE join_members_id = ? AND daily_summarys_date = DATE(?)";
    $summaryParams = array($log['join_members_id'], $log['access_logs_datetime']);

    $dailySummary = $db->fetch_one($summaryQuery, $summaryParams);

    if(!$dailySummary) {
        $dailySummary = array();

        $dailySummary['join_accounts_id'] = $log['join_accounts_id'];
        $dailySummary['join_members_id'] = $log['join_members_id'];
        $dailySummary['daily_summarys_date'] = $log['access_logs_datetime'];

        $dailySummarys->insert($dailySummary);
        $dailySummary = $db->fetch_one($summaryQuery, $summaryParams);
    }

    return $dailySummary;
}


function classifyLog($log, $ignores, $cats) {
    foreach($ignores as $ignore) {
        if (strpos($log['access_logs_path'], $ignore) === 0) {
            return null;
        }
    }
    foreach($cats as $prefix => $cat) {
        if (strpos($log['access_logs_path'], $prefix) === 0) {
            return $cat;
        }
    }
}

// limit processing time to 30 mins (in seconds)
set_time_limit(30*60);
do {
    $logs = $db->fetch_all("SELECT * FROM access_logs WHERE access_logs_processed = 0 AND join_members_id > 0 LIMIT 50");

    foreach ($logs as $log) {
        $dailySummary = getOrCreateSummary($log, $db);
        $category = classifyLog($log, $ignores, $cats);
        if ($category != null) {
            $propertyName = 'daily_summarys_category_' . $category;
            $previousCount = array_key_exists($propertyName, $dailySummary) ? $dailySummary[$propertyName] : 0;
            $previousTotal = array_key_exists('daily_summarys_total',$dailySummary) ? $dailySummary['daily_summarys_total'] : 0;
            $dailySummary[$propertyName] = $previousCount + 1;
            $dailySummary['daily_summarys_total'] = $previousTotal + 1;
            $dailySummarys->update($dailySummary, $dailySummary['daily_summarys_id']);
        }

        $log['access_logs_processed'] = true;
        $accessLogs->update($log, $log['access_logs_id']);
    }
} while (!empty($logs));
