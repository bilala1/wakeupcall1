<?php
//this script deletes files and db info. per member

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$delete_date = date('Y-m-d',strtotime('now - '.DELETE_ENTITY_DAYS.' days'));

// ---------------------------------------------------------------------------------------------
// - corporations to delete
//----------------------------------------------------------------------------------------------
$corp_members_to_delete = $db->fetch_all('SELECT m.members_id, c.corporations_id
                                     FROM members AS m
                                     LEFT JOIN corporations AS c ON(m.members_id = c.join_members_id)
                                     WHERE DATE(c.corporations_delete_datetime) <= ?',array($delete_date));
if($corp_members_to_delete){
    foreach($corp_members_to_delete as $corporation){
            echo '<br /><br />Deleting corporation: '.$corporation['corporations_id'];
            //remove data from all licensed_locations first
        //todo test licensed locations
            $licensed_locations_members = $db->fetch_all('SELECT join_members_id FROM licensed_locations WHERE join_corporations_id = ? ', array($corporation['corporations_id']));
            foreach($licensed_locations_members as $licensed_location_member){
                echo '<br />Deleting Corporation\'s Location [members_id]: '.$licensed_location_member['join_members_id'];
                //get licensed_location id by members id
                //todo test licensed locations
                $licensed_location_id = $db->fetch_singlet('SELECT licensed_locations_id FROM licensed_locations WHERE join_members_id = ? ', array($licensed_location_member['join_members_id']));
                echo '<br />Deleting Corporation\'s Location [licensed_locations_id]: '.$licensed_location_id;
                //-------------------------------------------------------------------------------------
                // -- delete Claim Files
                //-------------------------------------------------------------------------------------                
                    //if files are accepted into library - do not delete
                //todo test licensed locations
                    $claims_to_remove = $db->fetch_all('SELECT claims_files_file
                                                              FROM claims_files AS cf
                                                              JOIN claims AS c ON(cf.join_claims_id = c.claims_id)
                                                              WHERE c.join_licensed_locations_id = ? ', array($licensed_location_id));
                    foreach($claims_to_remove as $claim_remove){
                        //remove file from system
                        $uploaddir = SITE_PATH .'/data/claims/';
                        unlink($uploaddir.$claim_remove['claims_files_file']);
                        //remove claims rows
                        $db->query('DELETE
                                    FROM claims_files 
                                    WHERE claims_files_file = ? ', array($claim_remove['claims_files_file']));
                    }   
                //-------------------------------------------------------------------------------------
                // -- delete Claims
                //-------------------------------------------------------------------------------------
                //todo test licensed locations
                    $db->query('DELETE FROM claims 
                                       WHERE join_licensed_locations_id = ? ', array($licensed_location_id));

                //-------------------------------------------------------------------------------------
                // -- delete rest of member related data
                //-------------------------------------------------------------------------------------                   
                    remove_all_data($licensed_location_member['join_members_id']);
            } // end foreach licensed_locations

        //-------------------------------------------------------------------------------------
        // -- delete licensed_locations
        //-------------------------------------------------------------------------------------
        //todo test licensed locations
            $db->fetch_all('DELETE FROM licensed_locations WHERE join_corporations_id = ? ', array($corporation['corporations_id']));

            
        //-------------------------------------------------------------------------------------
        // -- delete corporation
        //-------------------------------------------------------------------------------------                
            $db->fetch_all('DELETE FROM corporations WHERE corporations_id = ? ', array($corporation['corporations_id']));           

                        
        //-------------------------------------------------------------------------------------
        // -- delete Corporation and its data
        //-------------------------------------------------------------------------------------     
        remove_all_data($corporation['members_id']);  
    }

}


// ---------------------------------------------------------------------------------------------
// - licensed_locations to delete
//----------------------------------------------------------------------------------------------
//todo test licensed locations
$licensed_location_members_to_delete = $db->fetch_all('SELECT m.members_id
                                     FROM members AS m
                                     LEFT JOIN licensed_locations AS ll ON(m.members_id = ll.join_members_id)
                                     WHERE DATE(ll.licensed_locations_delete_datetime) <= ?',array($delete_date));
if($licensed_location_members_to_delete){
    foreach($licensed_location_members_to_delete as $licensed_location){
        //-------------------------------------------------------------------------------------
        // -- delete Claim Files
        //-------------------------------------------------------------------------------------                
            //claim files to be deleted
            $claims_to_remove = $db->fetch_all('SELECT claims_files_file
                                                      FROM claims_files AS cf
                                                      JOIN claims AS c ON(cf.join_claims_id = c.claims_id)
                                                      WHERE c.join_licensed_locations_id = ? ', array($_SESSION['licensed_locations_id']));
            foreach($claims_to_remove as $claim_remove){
                //remove file from system
                $uploaddir = SITE_PATH .'/data/claims/';
                unlink($uploaddir.$claim_remove['claims_files_file']);
                //remove claims rows (claims_files_file should be as good as id)
                $db->query('DELETE
                            FROM claims_files 
                            WHERE claims_files_file = ? ', array($claim_remove['claims_files_file']));
            }            
        //-------------------------------------------------------------------------------------
        // -- delete rest of member related data
        //-------------------------------------------------------------------------------------                   
            remove_all_data(ActiveMemberInfo::GetMemberId());
    }
}
   
                           
    function remove_all_data($id){ //members id   
         
        $db = mysqli_db::init();
        //-----------------------------------------------------------------------------------------
        // -- delete files (and rows from tables related to files)
        //-----------------------------------------------------------------------------------------
        
            //-------------------------------------------------------------------------------------
            // -- delete Documents
            //-------------------------------------------------------------------------------------
                //if files are accepted into library - do not delete
                $documents_to_remove = $db->fetch_all('SELECT documents_id, documents_file 
                                                       FROM documents 
                                                       WHERE join_members_id = ?
                                                       AND documents_type != "library" ', array($id));
                foreach($documents_to_remove as $document_remove){
                    //remove file from system
                    
                    unlink(SITE_PATH . DOCUMENT_DIR.$document_remove['documents_file']);
                    //delete download stats
                    $db->query('DELETE FROM downloads WHERE downloads_type = "file" AND join_id = ?', array($document_remove['documents_id']));
                    //delete from 'My Files'
                    $db->query('DELETE FROM members_x_documents WHERE join_documents_id = ?', array($document_remove['documents_id']));
                }
                $db->query('DELETE FROM documents 
                                   WHERE join_members_id = ?
                                   AND documents_type != "library" ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete Certificates
            //-------------------------------------------------------------------------------------
                //if files are accepted into library - do not delete
                $certificates_to_remove = $db->fetch_all('SELECT certificates_id, certificates_file 
                                                          FROM certificates 
                                                          WHERE join_members_id = ? ', array($id));
                foreach($certificates_to_remove as $certificate_remove){
                    //remove file from system
                    $uploaddir = SITE_PATH .'/data/certificates/';
                    unlink($uploaddir.$certificate_remove['certificates_file']);
                    //delete download stats
                    $db->query('DELETE FROM downloads WHERE downloads_type = "certificate" AND join_id = ?', array($certificate_remove['certificates_id']));
                }            
                $db->query('DELETE FROM certificates 
                                   WHERE join_members_id = ? ', array($id));     
                
        //-----------------------------------------------------------------------------------------
        // -- delete rows from DB 
        //-----------------------------------------------------------------------------------------
                
            //-------------------------------------------------------------------------------------
            // -- delete Vendors
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM vendors 
                                   WHERE join_members_id = ? ', array($id));                
                
            //-------------------------------------------------------------------------------------
            // -- delete Carriers
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM carriers
                                   WHERE join_members_id = ? ', array($id));                
                                
            //-------------------------------------------------------------------------------------
            // -- delete 'My Folders'
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members_library_categories 
                                   WHERE join_members_id = ? ', array($id));    
                
            //-------------------------------------------------------------------------------------
            // -- delete from chemicals_x_members
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM chemicals_x_members 
                                   WHERE join_members_id = ? ', array($id));                    
                
            //-------------------------------------------------------------------------------------
            // -- delete from members_x_documents
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members_x_documents 
                                   WHERE join_members_id = ? ', array($id));                     
            //-------------------------------------------------------------------------------------
            // -- delete from members_x_documents
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM downloads 
                                   WHERE join_members_id = ? ', array($id));
                
            //-------------------------------------------------------------------------------------
            // -- delete actions/log
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM actions 
                                   WHERE join_members_id = ? ', array($id));                
                                
                    
            //-------------------------------------------------------------------------------------
            // -- delete notifications options
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM notifications 
                                   WHERE join_members_id = ? ', array($id));  
                
            //-------------------------------------------------------------------------------------
            // -- delete staff 
            //-------------------------------------------------------------------------------------
        //todo test licensed locations
                $staff_members = $db->fetch_all('SELECT s.join_members_id
                                                 FROM staff AS s
                                                 LEFT JOIN licensed_locations AS ll ON(s.join_licensed_locations_id = ll.licensed_locations_id)
                                                 JOIN members AS m ON(h.join_members_id = m.members_id)
                                                 WHERE m.members_id = ? 
                                                 ',array($id));
                //for each staff member who belongs to this licensed_location remove download stats
                foreach($staff_members as $staff_member){
                    echo '<br /> Staff Member [members_id]'.$staff_member['join_members_id'];
                    $db->query('DELETE FROM downloads 
                                   WHERE join_members_id = ? ', array($staff_member['join_members_id']));
                    
                    //remove information from staff table
                    $db->query('DELETE FROM staff 
                                       WHERE join_members_id = ? ', array($staff_member['join_members_id'])); 
                    //remove
                    $db->query('DELETE FROM members 
                                   WHERE members_id = ? ', array($staff_member['join_members_id'])); 
                }  
                
                

            //-------------------------------------------------------------------------------------
            // -- delete from FORUMS
            //-------------------------------------------------------------------------------------
        
            //  -- FROM TABLE                   by using this       get this            delete files too?
            // --
            // -- forums_groups_x_members   --> (join_members_id)-->join_groups_id 
            // -- forums_groups             --> (groups_id)
            // -- forums_groups_x_forums    --> (join_groups_id) -->join_forums_id
            // -- forums                    --> (forums_id)
            // -- forums_topics             --> (join_forums_id)
            // -- forums_posts              --> (join_members_id)
            // -- forums_posts_files        --> (join_members_id)                       (forums_posts_files_filename)
            // -- forums_posts_reported     --> (join_members_id)
            // -- forums_topics             --> (join_forums_id)
            // -- forums_watchdogs          --> (join_members_id)(join_topics_id)
                
                
                
            //-------------------------------------------------------------------------------------
            // -- delete Member from members table 
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members 
                                   WHERE members_id = ? ', array($id));    
                
    }// end of function remove_all_data
            
 


?>