<?php
    $db = mysqli_db::init();
    
    $exp_certs = $db->fetch_all('SELECT * FROM certificates AS c LEFT JOIN vendors AS v ON c.join_vendors_id = v.vendors_id LEFT JOIN members AS m ON c.join_members_id = m.members_id WHERE c.certificates_remind_vendor = 1 AND DATE_SUB(CURDATE(), INTERVAL 15 DAY) = c.certificates_expire');
    //echo $db->debug();
    
    foreach($exp_certs as $certificate) {
        //pre($certificate);exit;
        
         $loc_name = '';
        if($certificate['members_type'] == 'hotel') {
            //todo licensed locations
            $loc_name = $db->fetch_singlet('SELECT hotels_name FROM hotels WHERE join_members_id = ?',array($certificate['members_id']));
        }
        else {
            $loc_name = $db->fetch_singlet('SELECT corporations_name FROM corporations WHERE join_members_id = ?',array($certificate['members_id']));
        }
        
        send_exp_cert_email($certificate, $loc_name);
    }
    
    
    function send_exp_cert_email($certificate, $loc_name){
        extract($certificate);
        
        ob_start(); 
        include(VIEWS . '/emails/exp-certificate-request.php');
        $message = ob_get_contents(); 
        ob_end_clean();
        
        /*mail::send(
            $from = SALES_EMAIL,
            $to = 'test@wakeupcall.net',
            $subject =  'Certificate Expired | ' . SITE_NAME,
            $message = $message
          );
        */
        if($certificate['join_vendors_id']){
            mail::send(
                $from = AUTO_EMAIL,
                $to = $vendors_email, 
                $subject =  'Certificate Expired | ' . SITE_NAME,
                $message = $message
            );
        }
    }
?>
