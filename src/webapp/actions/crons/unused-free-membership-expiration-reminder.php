<?php

//After a member has been a 'free' member for 15 days, they get sent an email with a "benefits of full membership" 

$db = mysqli_db::init();

$members = $db->fetch_all('SELECT *
                           FROM members
                           WHERE DATE(members_datetime) = DATE_SUB(CURDATE(),INTERVAL '.FREE_MEMBER_PERIOD.' DAY)');

if($members){

    foreach($members as $member){

        //set new stataus
        $db->query('UPDATE members SET members_status = "locked" WHERE members_id = ? ', array($member['members_id']));

        //send email
        //email members
        ob_start();
        include(VIEWS . '/emails/free-membership-expiration-reminder.php');
        $message = ob_get_contents();
        ob_end_clean();

        mail::send(
            $from = SALES_EMAIL,
            $to = $member['members_email'],
            $subject =  'Free Membership Expiring | ' . SITE_NAME,
            $message = $message
        );
    }

}


?>