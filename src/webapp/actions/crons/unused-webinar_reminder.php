<?
$db = mysqli_db::init();

//get webinars 1 day away
$webinars = $db->fetch_all('
    select *
    from webinars
    where webinars_datetime between ? and ?',
    array(times::to_mysql_utc('now'), times::to_mysql_utc('now + 1 day'))
);

foreach($webinars as $webinar){
    $attendees = $db->fetch_all('select * from members join webinars_x_members on join_members_id = members_id where join_webinars_id = ?', array($webinar['webinars_id']));
    
    //send emails out (if emails aren't specific, send all in one email call using bcc)
}

?>