<?php

$db = mysqli_db::init();


$items_old = file_get_contents(SITE_PATH . '/data/rss.txt');
$items_old = unserialize($items_old);
// prepare array of current news links, so that we will ignore new articles which link to identical story
$old_links = array();
foreach($items_old as $type => $items){
    foreach($items as $item_old) {
        $old_links[] = $item_old['link'];
    }
}

$items = array();

/*$context = stream_context_create(array(
    'http' => array(
        'timeout' => 1
        )
    )
);*/

$rss_feeds = array(
    'hr' => array(
        array(
            type => 'hrdailyadvisor',
            url => 'http://hrdailyadvisor.blr.com/feed/',
            format_date => function($date) {
                return date('Y-m-d H:i:s', strtotime($date . ' - 8 HOUR'));
            }
        ),
        array(
            type => 'thinkhr',
            url => 'http://feeds.feedburner.com/thinkhrblog',
            format_date => function($date) {
                return date('Y-m-d H:i:s', strtotime($date . ' - 8 HOUR'));
            }
        )
    ),
    'hotel' => array(
        array(
            type => 'smartbrief',
            url => 'http://www2.smartbrief.com/servlet/rss?b=AHLA',
            format_date => function($date) {
                return date('Y-m-d H:i:s', strtotime($date . ' - 2 HOUR'));
            }
        ),
        array(
            type => 'hotelbusiness',
            url => 'https://www.hotelbusiness.com/feed/',
            format_date => function($date) {
                return date('Y-m-d H:i:s', strtotime($date . ' - 3 HOUR'));
            }
        ),
        array(
            type => 'hotelnewsnow',
            url => 'http://www.hotelnewsnow.com/rss?projection=164',
            format_date => function($date) {
                return date('Y-m-d H:i:s', strtotime($date));
            }
        )
    )
);

$count = 0;

function add_rss_items($feedInfo, $old_links, &$count) {

    $request = http::request($feedInfo['url'], array('timeout' => 10, 'exclude_header' => true));
    $page = $request['response'];

    if($page){
        $encoding_type = mb_detect_encoding($page);
        if ($encoding_type && $encoding_type !== 'UTF-8') {
            $page = iconv($encoding_type, 'UTF-8', $page);
            $page = str_replace("\xE2\x80\x94", '-', $page);
        }
        file_put_contents(SITE_PATH . '/data/' . $feedInfo['type'] . '.xml', $page);

        $xml = simplexml_load_string($page, 'SimpleXMLElement', LIBXML_NOCDATA);

        if($xml->channel->item){
            foreach($xml->channel->item as $item){
                if(in_array((string)$item->link, $old_links)){
                    continue;
                }
                $pubDate = $feedInfo['format_date']((string)$item->pubDate);
                $count++;
                $key = strtotime($pubDate) + $count;
                //pre($key);
                $items[$key] = array(
                    'title' => (string)$item->title,
                    'description' => (string)$item->description,
                    'link' => (string)$item->link,
                    'pubDate' => $pubDate,
                    'type' => $feedInfo['type']
                );
                //-- for debugging purposes (weird special chars)
                if(DEPLOYMENT != 'production') {
                    $filename = SITE_PATH . '/data/test.txt';
                    file_put_contents($filename, (string)$item->title . "\n\n", FILE_APPEND);
                }
            }
        }
    }

    return $items ? (is_array($items) ? $items : array($items)) : array();
}

foreach ($rss_feeds as $type => $feeds) {
    $items[$type] = array();
    foreach ($feeds as $feed_info) {
        $items[$type] += add_rss_items($feed_info, $old_links, $count);
    }
    /*
     * sort the items chronologically, desc
     */
    ksort($items[$type]);
    $items[$type] = array_reverse($items[$type]);
}
//pre($items); exit;

//-- convert simpleXMLObjects to strings so we can serialize this ish
foreach ($items as &$type) {
    foreach ($type as &$item) {
    foreach ($item as &$info) {

        $info = str_replace(
            array(
                "\xC2\x94",
                "\xC2\x96",
                "\xC2\x97",
                "\xC2\x93",
                "\xC2\x92",
                "‘",
                "’",
                "é",
                '&#039;',
                "—"
            ),
            array(
                '"',
                '-',
                "&ndash;",
                '"',
                "'",
                "'",
                "'",
                "e",
                "'",
                "&ndash;"
            ), $info);
    }
}
}


foreach($items as $t => &$l) {
    if($items_old && $items_old[$t]) {
        $l = array_merge($l, $items_old[$t]);
    }
    if(count($l)>100) {
        $l = array_slice($l, 0, 100);
    }
}

/*
 * stick em in a file!
 */
$rss_txt_file = SITE_PATH . '/data/rss.txt';
@chmod($rss_txt_file, 0755);
$items = serialize($items);
file_put_contents($rss_txt_file, $items);
?>