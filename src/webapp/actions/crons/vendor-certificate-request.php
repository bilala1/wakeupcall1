<?php
$db = mysqli_db::init();

include_once(ACTIONS . '/members/my-documents/certificates/_email_cert_request.php');

$certificates = $db->fetch_all('
    select c.*, ll.*, a.*, vendors_email, vendors_name, vendors_street
    from certificates c
    left join licensed_locations ll on licensed_locations_id = join_licensed_locations_id
    left join vendors v on vendors_id = join_vendors_id
    left join accounts a on ll.join_accounts_id = accounts_id
    where
    accounts_is_locked = 0 AND
    certificates_hidden = 0 AND
    licensed_locations_delete_datetime IS NULL AND
    (certificates_expire - INTERVAL certificates_email_days DAY) = CURDATE() and
    certificates_expire >= CURDATE() and
    certificates_remind_vendor = 1 AND
    vendors_email is not null AND
    vendors_email != ""'
);

foreach ($certificates as $certificate) {
    EmailCertRequest($certificate['certificates_id'],$certificate['certificates_email_subject']);

    $db->query(
        'UPDATE certificates
          SET certificates_remind_member_date_last = CURDATE()
          WHERE certificates_id = ?'
        , array($certificate['certificates_id']));
}


//Nag vendors about expired certificates
$certificates = $db->fetch_all('
    SELECT *, ll.*
    FROM certificates
    JOIN licensed_locations ll ON (ll.licensed_locations_id = join_licensed_locations_id)
    JOIN accounts ON ll.join_accounts_id = accounts_id
    LEFT JOIN vendors AS v ON(v.vendors_id = certificates.join_vendors_id)
    WHERE
    accounts_is_locked = 0
    AND certificates_hidden = 0
    AND licensed_locations_delete_datetime IS NULL
    AND certificates_expire <= CURDATE()
    AND certificates_remind_member_date_last <= CURDATE() - INTERVAL 1 WEEK AND
        certificates_remind_vendor_expired = 1 AND
        vendors_email is not null AND
        vendors_email != ""'
);

foreach ($certificates as $certificate) {
    EmailCertRequest($certificate['certificates_id'],$certificate['certificates_email_subject']);

    $db->query(
        'UPDATE certificates
          SET certificates_remind_member_date_last = CURDATE()
          WHERE certificates_id = ?'
        , array($certificate['certificates_id']));
}