<?php
$db = mysqli_db::init();

//get webinars 1 day away
$webinars = $db->fetch_all('
    select *
    from webinars
    where webinars_datetime between ? and ?',
    array(times::to_mysql_utc('now'), times::to_mysql_utc('now + 1 day'))
);

foreach($webinars as $webinar){
    $attendees_emails = $db->fetch_singlets('select members_email from members join webinars_x_members on join_members_id = members_id where join_webinars_id = ? and members_status != "locked"', array($webinar['webinars_id']));
    
    //send emails out (if emails aren't specific, send all in one email call using bcc)
    mail::send_template(
        $from     = AUTO_EMAIL,
        $to       = $member['members_email'],
        $subject  = 'Webinar Reminder ' . SITE_NAME,
        $template = 'webinar-reminder-registered.php',
        $vars     = get_defined_vars(),
        $cc       = null,
        $bcc      = $attendees_emails
    );
}

?>