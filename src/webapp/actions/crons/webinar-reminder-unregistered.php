<?
$db = mysqli_db::init();

//get webinars 7 days away
$webinars = $db->fetch_all('
    select *
    from webinars
    where webinars_datetime between ? and ?',
    array(times::to_mysql_utc('now + 6 days'), times::to_mysql_utc('now + 7 days'))
);

// for each webinar, email all members who:
// -- a: not attending
// -- b: B was removed, all members should receive notifications.
// -- b: have checked notifications checkbox
// -- b: notifications_webinars_update = 1 and
foreach($webinars as $webinar){
    $nonattendees_emails = $db->fetch_singlets('
        select members_email
        from members
        left join notifications as n on n.join_members_id = members_id
        left join webinars_x_members as wxm on
            wxm.join_members_id = members_id and
            join_webinars_id = ?
        where
            wxm.join_members_id is null and
            members_status != "locked"',
        array($webinar['webinars_id'])
    );

    //send emails out (if emails aren't specific, send all in one email call using bcc)
    mail::send_template(
        $from     = AUTO_EMAIL,
        $to       = SALES_EMAIL,
        $subject  = 'Webinar Reminder | ' . SITE_NAME,
        $template = 'webinar-reminder-unregistered.php',
        $vars     = get_defined_vars(),
        $cc       = null,
        $bcc      = $nonattendees_emails
    );

	$message_notify = $member['members_firstname'].' '.$member['members_lastname'] . ' has been sent a notification to reqister for this Webinar: ' . $webinar['webinars_name'];

	notify::david($subject, $message_notify);
}

?>