<?
function unsubscribe($email_addr)
{
    if($email_addr)
    {
        $db = mysqli_db::init();
        $db->query('DELETE FROM newsletter_subscribers WHERE email_address = ?', array($email_addr));
    }
}

$db = mysqli_db::init();
$forums_model = new forums;

//new articles in the past week
$recent_articles = $db->fetch_all('
    select *
    from articles
    where
        articles_newsdate > NOW() - INTERVAL 1 WEEK and
        articles_newsdate < NOW() and
        articles_active = 1'
);

//Two random FAQ's from any Industry FAQ topic. [task 714]
$recent_faqs = $db->fetch_all('
    SELECT *
    FROM faqs
    WHERE faqs_published = 1
    AND join_faqs_categories_id = 9
    ORDER BY RAND()
    LIMIT 2'
);

//topics with the most posts in the past week
$popular_forum_topics = $forums_model->get_top_wakeupcall_topics(5);

$webinars = $db->fetch_all('SELECT webinars_name,webinars_datetime,webinars_description 
                    FROM webinars
                    WHERE webinars_datetime > NOW()
                    ORDER BY webinars_datetime ASC
                    LIMIT 0,2');


$nonmembers = $db->fetch_all('select * from newsletter_subscribers');

$members = $db->fetch_all('
    select *
    from members
    left join notifications on join_members_id = members_id
    where notifications_news_update = 1
    AND members_status != "locked"'
);

$locked_members = $db->fetch_all('
    select *
    from members
    left join notifications on join_members_id = members_id
    where notifications_news_update = 1
    AND members_status = "locked"'
);

$items = file_get_contents(SITE_PATH . '/data/rss.txt');
$items = unserialize($items);

// hospitality news --------------------------------------------------------------------------------
$hospitality_news = $items['hotel'];
foreach($hospitality_news as $hn){
    if(strtotime($hn['pubDate']) < strtotime(date('Y-M-d', strtotime('now'))) && strtotime($hn['pubDate']) > strtotime('now - 1 week') ) {
        if($hn['type'] != 'smartbrief'){ //let's exclude smartbrief because it has long url and advertisements
            $last_week_hospitality_news[] = $hn;
        }
    }          
}
if($last_week_hospitality_news){
    $keys = array();
    $keys[] = rand(0,(count($last_week_hospitality_news)-1)); 
    if(count($last_week_hospitality_news)>1){
        while(count($keys)<2){ // two news stories to be displayed
            $keys[] = rand(0,(count($last_week_hospitality_news)-1)); 
            $keys = array_unique($keys);
        }
    }
    foreach($keys as $k){
        $newsletter_hospitality_news[] = $last_week_hospitality_news[$k]; // first random news
    }
    unset($keys);
}
//--------------------------------------------------------------------------------------------------

// HR news -----------------------------------------------------------------------------------------
$hr_news = $items['hr'];
foreach($hr_news as $hrn){
    if(strtotime($hrn['pubDate']) < strtotime(date('Y-M-d', strtotime('now'))) && strtotime($hrn['pubDate']) > strtotime('now - 1 week') ) {
        $last_week_hr_news[] = $hrn;
    }          
}
if($last_week_hr_news){
    $keys = array();
    $keys[] = rand(0,(count($last_week_hr_news)-1)); 
    if(count($last_week_hr_news)>1){
        while(count($keys)<2){ // two news stories to be displayed
            $keys[] = rand(0,(count($last_week_hr_news)-1)); 
            $keys = array_unique($keys);
        }
    }
    foreach($keys as $k){
        $newsletter_hr_news[] = $last_week_hr_news[$k]; // first random news
    }
}
//--------------------------------------------------------------------------------------------------
ob_start();
$notification_mgmt_url = 'http://www.wakeupcall.net/members/account/notifications.php';
include(VIEWS . '/emails/weekly-newsletter.php');
$message = ob_get_contents();
ob_end_clean();

ob_start();
$notification_mgmt_url = 'http://www.wakeupcall.net/notifications.php';
include(VIEWS . '/emails/weekly-newsletter.php');
$locked_message = ob_get_contents();
ob_end_clean();

//save newsletter into db and use it's id as Issue number
$newsletters = new mysqli_db_table('newsletters');
$newsletters->insert(array(
                        'newsletters_text' => $message,
                        'newsletters_date' => SQL('NOW()')
                    ));
$newsletters_id = $newsletters->last_id();


error_reporting(E_ALL);
ini_set('display_errors', '1');

foreach($members as $member){
    unsubscribe($member['members_email']);
    
    if (!filter_var($member['members_email'], FILTER_VALIDATE_EMAIL)) 
        continue;
    

    echo 'Newsletter sent to: '.$member['members_email'].'<br />';
    
    mail::send(
        $from = AUTO_EMAIL,
        $to = $member['members_email'],
        $subject =  'Newsletter | ' . SITE_NAME,
        $message = $message
    );

    
}

foreach($locked_members as $member){
    unsubscribe($member['members_email']);
    
    if (!filter_var($member['members_email'], FILTER_VALIDATE_EMAIL)) 
        continue;
    
    mail::send(
        $from = AUTO_EMAIL,
        $to = $member['members_email'],
        $subject =  'Newsletter | ' . SITE_NAME,
        $message = $locked_message
    );

    echo 'Newsletter sent to locked member: '.$member['members_email'].'<br />';
}

foreach($nonmembers as $member){
    
    if (!filter_var($member['email_address'], FILTER_VALIDATE_EMAIL))
        continue;
    
    mail::send(
        $from = AUTO_EMAIL,
        $to = $member['email_address'],
        $subject =  'Newsletter | ' . SITE_NAME,
        $message = $message
    );

    echo 'Newsletter sent to: '.$member['email_address'].'<br />';
}

?>
