<?php
    ob_start();
    include(SITE_PATH . '/' . VIEWS . '/emails/fax-sent.php');
    $message = ob_get_clean();
    
    //$from = 'Claim_Submission@wakeupcall.net (Claim Submission)';
    $from = AUTO_EMAIL; // noreply
    #$to = $member['members_email'];
    $to = 'test@wakeupcall.net';
    $subject = 'Your claim was successfully faxed | ' . SITE_NAME;
	if (!empty($to)) {
		mail::send($from, $to, $subject, $message);
	}
?>
