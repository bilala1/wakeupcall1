<?
require_once(LIBS . '/stripe_lib/stripe_include.php');

$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');
$notifications_table = new mysqli_db_table('notifications');
$members_settings_table = new mysqli_db_table('members_settings');
$forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');
$licensed_locations_table = new mysqli_db_table('licensed_locations');
$hotel_table = new mysqli_db_table('hotels');

// if a member is trying to renew membership after his payment was declined during automatic billing
// redirect him to member-payment-failed.php page instead.
$member_cnt = $db->fetch_singlet('SELECT COUNT(*) 
                                FROM members AS m
                                JOIN bills AS b ON (m.members_id = b.join_members_id)
                                WHERE members_status = "locked"
                                AND bills_paid = 0 
                                AND members_id = ?',array($_REQUEST['members_id']));
if($member_cnt > 0){ 
    http::redirect('member-payment-failed.php', array('members_id' => $_REQUEST['members_id']));
}
// -------------------------------------------------------------------------------------------------

// AJAX --
if ($_REQUEST['ajax'] == 'coupon-pricing') {
	$code = $db->fetch_one('
		SELECT * FROM discount_codes WHERE discount_codes_code = ? 
		AND discount_codes_start <= NOW()
		AND discount_codes_end >= NOW()', 
		array($_REQUEST['discount_code'])
	);

	if (empty($code)) {
		die('fail');
	} else {
		$result = array();
		if ($code['discount_codes_amount'] > 0) {
			$result['discount_code_discount'] = money_format('%n', $code['discount_codes_amount']);
			$result['discount_code_total'] = money_format('%n', YEARLY_COST - $code['discount_codes_amount']);
		} else {
			$result['discount_code_discount'] = $code['discount_codes_percent'] . '%';
			$result['discount_code_total'] = money_format('%n', YEARLY_COST * (1 - $code['discount_codes_percent'] / 100));
		}
        $result['discount_codes_code']=  $code['discount_codes_code'];
        $result['discount_codes_id']=  $code['discount_codes_id'];
		die(json_encode($result));
	}
} // - AJAX


// AJAX --
if ($_REQUEST['ajax'] == 'franchise-pricing') {
    $franchise = $db->fetch_one('
		SELECT * FROM franchises WHERE franchises_code = ?',
        array($_REQUEST['franchise_code'])
    );

    if (empty($franchise)) {
        die('fail');
    } else {
        $result = array();
        if ($franchise['franchises_amount_discount'] > 0) {
            $result['franchise_discount'] = money_format('%n', $franchise['franchises_amount_discount']);
            $result['franchise_total'] = money_format('%n', YEARLY_COST - $franchise['franchises_amount_discount']);
        } else {
            $result['franchise_discount'] = $franchise['franchises_pct_discount'] . '%';
            $result['franchise_total'] = money_format('%n', YEARLY_COST * (1 - $franchise['franchises_pct_discount'] / 100));
        }
        die(json_encode($result));
    }
} // - AJAX


// AJAX --
if ($_REQUEST['ajax'] == 'franchise-and-coupon-pricing') {
    $code = $db->fetch_one('
		SELECT * FROM discount_codes WHERE discount_codes_code = ?
		AND discount_codes_start <= NOW()
		AND discount_codes_end >= NOW()',
        array($_REQUEST['discount_code'])
    );
    $result = array('total' => YEARLY_COST);

    if (empty($code)) {
        $result['discount_code_discount'] = '';
        $result['discount_code_total'] = -1;
    } else {
        if ($code['discount_codes_amount'] > 0) {
            $result['discount_code_discount'] = money_format('%n', $code['discount_codes_amount']);
            $result['discount_code_total'] = money_format('%n', YEARLY_COST - $code['discount_codes_amount']);
        } else {
            $result['discount_code_discount'] = $code['discount_codes_percent'] . '%';
            $result['discount_code_total'] = money_format('%n', YEARLY_COST * (1 - $code['discount_codes_percent'] / 100));
        }
        $result['discount_codes_code']=  $code['discount_codes_code'];
        $result['discount_codes_id']=  $code['discount_codes_id'];

        if($result['discount_code_total'] < $result['total']) {
            $result['total'] = $result['discount_code_total'];
        }
    }


    $franchise = $db->fetch_one('
		SELECT * FROM franchises WHERE franchises_code = ?',
        array($_REQUEST['franchise_code'])
    );

    if (empty($franchise)) {
        $result['franchise_discount'] = '';
        $result['franchise_total'] = -1;
    } else {
        if ($franchise['franchises_amount_discount'] > 0) {
            $result['franchise_discount'] = money_format('%n', $franchise['franchises_amount_discount']);
            $result['franchise_total'] = money_format('%n', YEARLY_COST - $franchise['franchises_amount_discount']);
        } else {
            $result['franchise_discount'] = $franchise['franchises_pct_discount'] . '%';
            $result['franchise_total'] = money_format('%n', YEARLY_COST * (1 - $franchise['franchises_pct_discount'] / 100));
        }

        if($result['franchise_total'] < $result['total']) {
            $result['total'] = $result['franchise_total'];
        }
    }

    die(json_encode($result));
} // - AJAX


$card_months = array();
foreach(range(1, 12) as $month){
    $card_months[$month] = $month;
}

$card_years = array();
foreach(range(date('Y'), date('Y') + 10) as $year){
    $card_years [$year]= $year;
}

//if not an already existing member and not a member in the middle of registering
if(!$_REQUEST['members_id'] && !$_SESSION['registration']['member']){
    http::redirect(BASEURL . '/signup.php', array('error' => 'Error registering for full membership'));
}

//Why do they do this? $_SESSION['registration']['corporation']['corporations_separate_from_hotels'] = 0;
if($_REQUEST['members_id']){
// This branch is for free trial members that have expired and now need to pay
    //todo test licensed locations
    $member = $db->fetch_one('
        SELECT *
        FROM members
        inner join accounts as a on a.join_members_id = members_id
        left join corporations as c on c.join_accounts_id = accounts_id
        left join licensed_locations ll on ll.join_members_id = members_id
        WHERE members_id = ?',
        array($_REQUEST['members_id'])
    );
    $account = $member;

    if($member['corporations_id']){
        $is_corporation_member = true;

        //todo test licensed locations
        $num_locations = $db->fetch_singlet('select count(*) from licensed_locations where join_accounts_id = ?', array($member['accounts_id']));

        $locations = $db->fetch_all('
            SELECT *
            FROM licensed_locations AS ll
            LEFT JOIN franchises AS f ON ll.join_franchises_id = f.franchises_id
            LEFT JOIN corporate_locations cl ON  ll.licensed_locations_id = cl.join_licensed_locations_id
            WHERE join_accounts_id = ?',
            array($member['accounts_id'])
        );
    } else if($member['licensed_locations_id']){
        $is_hotel_member = true;
    }
} else {
//this is if the member is going through registration
    
    $member = $_SESSION['registration']['member'];
    $account = $_SESSION['registration']['account'];
    
    if($_SESSION['registration']['btype'] == 'corporation'){
        $member = array_merge($member, $_SESSION['registration']['corporation']);
        
        $is_corporation_member = true;
        
        $num_locations = ($_SESSION['registration']['other_loc'] == 'Yes') ? 1 : 0;

    }else{
        $member = array_merge($member, $_SESSION['registration']['hotel']);
        $member = array_merge($member, $_SESSION['registration']['hotel_loc']);
        $is_hotel_member = true;
    }
}

$member['address'] = $member['corporations_address'] ?: $member['licensed_locations_address'];
$member['city'] = $member['corporations_city'] ?: $member['licensed_locations_city'];
$member['state'] = $member['corporations_state'] ?: $member['licensed_locations_state'];
$member['zip'] = $member['corporations_zip'] ?: $member['licensed_locations_zip'];

$_REQUEST['members_billing_addr1'] = $member['members_billing_addr1'];
$_REQUEST['members_billing_addr2'] = $member['members_billing_addr2'];
$_REQUEST['members_billing_city'] = $member['members_billing_city'];
$_REQUEST['members_billing_state'] = $member['members_billing_state'];
$_REQUEST['members_billing_zip'] = $member['members_billing_zip'];

//Find Cost
$franchise_cost = -1;
$franchise_cost_string = '';

if($is_hotel_member){

    //$member['corporations_id'] checked to see if registration.
    //If registration, then no corporation to look up and only one hotel

    $total_cost = YEARLY_COST;
    $total_cost_string = money_format('%n', $total_cost).' USD';
}
else if($is_corporation_member){

    // This is an existing member
    if($locations && count($locations) > 0)
    {
        $num_locations = 0;
        //The max($franchise_cost, 0) is so that we get a true cost - adding from the initial -1 would be bad.
        foreach($locations as $tmp_hotel)
        {
            if($tmp_hotel['corporate_locations_id']) {
                continue;
            }
            $num_locations++;
            $total_cost += YEARLY_COST;
            if($tmp_hotel['join_franchises_id'])
            {
                if ($tmp_hotel['franchises_amount_discount'] > 0) {
                    $tmp_franchise_cost = YEARLY_COST - $tmp_hotel['franchises_amount_discount'];
                }
                else {
                    $tmp_franchise_cost = YEARLY_COST * (1 - $tmp_hotel['franchises_pct_discount'] / 100);
                }

                $franchise_cost =  max($franchise_cost, 0) + $tmp_franchise_cost;
            }
            else
            {
                $franchise_cost = max($franchise_cost, 0) +  YEARLY_COST;
            }
        }
    }
    //This is a new, registering member
    else
    {
        $num_locations = 1;
        $total_cost = YEARLY_COST;
    }

    //there must be at least 1 location:
    $num_locations = max($num_locations, 1);

    //Total cost must be for at least 1 location
    $total_cost = max(YEARLY_COST, $total_cost);

    if($franchise_cost != -1)
    {
        $franchise_cost_string = money_format('%n', $franchise_cost).' USD ('.$num_locations.' locations)';
        $franchise_discount_string =  money_format('%n', $total_cost - $franchise_cost).' USD ('.$num_locations.' locations)';
    }

    $total_cost_string = money_format('%n', $total_cost).' USD ('.$num_locations.' locations)';

}

if($_POST){

    $data = $_POST;
    $validate = new validate($data);
    $errors = array();

	if (!empty($_POST['discount_code'])) {
		$code = $db->fetch_one('
			SELECT * FROM discount_codes WHERE discount_codes_code = ? 
			AND discount_codes_start <= NOW()
			AND discount_codes_end >= NOW()', array($_POST['discount_code']));
		if (empty($code)) {
			$errors['discount_code'] = 'This discount code is invalid';
		}
        else if($code['discount_codes_max_uses'] > 0 && $code['discount_codes_uses'] >= $code['discount_codes_max_uses'])
        {
            $errors['discount_code'] = 'This discount code has been used too many times.';
        }
        else {
			if ($code['discount_codes_amount'] > 0) {
				$total_cost -= $code['discount_codes_amount'];
			} else {
				$total_cost *= 1 - ($code['discount_codes_percent'] / 100);
			}
		}
	}

    //Franchise discount
    if(!empty($_POST['franchises_code']))
    {
        $franchise = $db->fetch_one('
                    SELECT * FROM franchises WHERE franchises_code = ?',
            array($_POST['franchises_code'])
        );
        if (empty($franchise)) {
            $errors['franchises_code'] = 'This group code is invalid';
        } else {
            $franchise_code = $franchise['franchises_code'];

            if ($franchise['franchises_amount_discount'] > 0) {
                $tmp_franchise_cost = YEARLY_COST - $franchise['franchises_amount_discount'];
            } else {
                $tmp_franchise_cost = YEARLY_COST * (1 - $franchise['franchises_pct_discount'] / 100);
            }

            $franchise_cost =  $tmp_franchise_cost;
            $franchise_cost_string = money_format('%n', $franchise_cost).' USD';
            $franchise_discount_string = money_format('%n', $total_cost - $franchise_cost).' USD';
            $franchises_id = $franchise['franchises_id'];
        }
    } else {
        $franchises_id = null;
    }

    if($franchise_cost >= 0)
    {
        $total_cost = min($franchise_cost, $total_cost);
    }

    $validate->setOptional(array(
        'members_id',
        'members_billing_addr2',
        'same_address',
        'discount_code',
        'franchises_code'
    ));
    $validate->setTitles(array(
        'members_billing_addr1' => 'Address',
        'members_billing_city' => 'City',
        'members_billing_zip' => 'Zip',
        'members_card_name' => 'Name on Card',
        'members_card_num' => 'Card Number',
        'security_code' => 'Security Code'
    ))->setCreditCard('members_card_num');

    $errors = array_merge($errors, $validate->test());

    if(!$errors){
        //If the user is going through registration, then at this point we can insert their first page data into the database

        
        if(!$_REQUEST['members_id']){
            $_SESSION['registration']['member']['members_billing_type'] = "yearly";
            $members_table->insert($_SESSION['registration']['member']);
            $members_id = $members_table->last_id();
            
            $account = array_merge($_SESSION['registration']['account'], array('join_members_id' => $members_id));
            $accounts_id = accounts::create($account);
            members::join_account($members_id, $accounts_id);
            
            $notifications_table->insert(array('join_members_id' => $members_id));

            $forums_groups_x_members_table->insert(array(
                'join_groups_id' => 6,      //Message Forum
                'join_members_id' => $members_id
            ));

            // collapsable yellow information boxes
            $members_settings_table->insert(array('join_members_id' => $members_id));

            if($_SESSION['registration']['btype'] == 'corporation'){
                $crp = $_SESSION['registration']['corporation'];
                $crp['join_members_id'] = $members_id;
                $crp['join_accounts_id'] = $accounts_id;
                $corporations_id = corporations::create($crp);

                //create corp location
                $corp_loc = array();
                $corp_loc['join_members_id']           =   $members_id;
                $corp_loc['join_corporations_id']      =   $corporations_id;
                $corp_loc['licensed_locations_name']   =   $crp['corporations_name'];
                $corp_loc['licensed_locations_address']=   $crp['corporations_address'];
                $corp_loc['licensed_locations_city']   =   $crp['corporations_city'];
                $corp_loc['licensed_locations_state']  =   $crp['corporations_state'];
                $corp_loc['licensed_locations_zip']    =   $crp['corporations_zip'];
                $corp_loc['licensed_locations_active'] =   1;
                $corp_loc['join_franchises_id'] = $franchises_id;
                $corp_loc['join_accounts_id'] = $accounts_id;

                $location_id = licensed_locations::create($corp_loc);

                $corp_loc = array();
                $corp_loc['corporate_locations_active']          =   1;
                $corp_loc['join_licensed_locations_id']          =   $location_id;
                corporate_locations::create($corp_loc);


                if($_SESSION['registration']['other_loc'] == 'Yes'){
                    $_SESSION['registration']['hotel_loc']['join_members_id'] = $members_id;
                    $_SESSION['registration']['hotel_loc']['join_corporations_id'] = $corporations_id;
                    $_SESSION['registration']['hotel_loc']['join_accounts_id'] = $accounts_id;
                    $_SESSION['registration']['hotel_loc']['join_franchises_id'] = $franchises_id;
                    $location_id = licensed_locations::create($_SESSION['registration']['hotel_loc']);

                    if($_SESSION['registration']['hotel_loc']['licensed_locations_type'] == 'Hotel'){
                        $_SESSION['registration']['hotel']['join_licensed_locations_id'] = $location_id;
                        $_SESSION['registration']['hotel']['join_accounts_id'] = $accounts_id;
                        hotels::create($_SESSION['registration']['hotel']);
                    }
                }
            }
            else if($_SESSION['registration']['btype'] == 'hotel'){
                $_SESSION['registration']['hotel_loc']['join_accounts_id'] = $accounts_id;
                $_SESSION['registration']['hotel_loc']['join_members_id'] =	$members_id;
                $_SESSION['registration']['hotel_loc']['join_franchises_id'] = $franchises_id;
                $location_id = licensed_locations::create($_SESSION['registration']['hotel_loc']);

                if($_SESSION['registration']['hotel_loc']['licensed_locations_type'] == 'Hotel'){
                    $_SESSION['registration']['hotel']['join_accounts_id'] = $accounts_id;
                    $_SESSION['registration']['hotel']['join_licensed_locations_id'] = $location_id;
                    hotels::create($_SESSION['registration']['hotel']);
                }
            }
            
            $member = $db->fetch_one('
                SELECT *
                FROM members
                left join corporations as c on members_id = c.join_members_id
                left join licensed_locations as l on members_id = l.join_members_id
                left join hotels as h on l.licensed_locations_id = h.join_licensed_locations_id
                WHERE members_id = ?',
                array($members_id)
            );
        }
        
        $description = 'Initial Membership Fee';

        $approved = false;

        try
        {
            $token = Stripe_Token::create(array( "card" => array( "number" => $data['members_card_num'],
                //"cvc" => $_POST['members_card_cvc'],
                "brand" => $data['members_card_type'],
                "exp_month" => $data['members_card_expire_month'],
                "exp_year" => $data['members_card_expire_year'],
                "address_line1" =>  $data['members_billing_addr1'],
                "address_line2" =>  $data['members_billing_addr2'],
                "address_city" =>  $data['members_billing_city'],
                "address_state" =>  $data['members_billing_state'],
                "address_zip" =>  $data['members_billing_zip']
            )));

            $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                "card" => $token->id));

            $data['members_stripe_id'] = $customer->id;

            $data = arrays::filter_keys($data, array(
                'members_stripe_id',
                'discount_code'
            ));
            $members_table = new mysqli_db_table('members');
            $members_table->update($data, $member['members_id']);

            $approved = true;
        }
        catch(Exception $e)
        {
            $approved = false;
            $transaction_error = $e->getMessage();
        }

        //process payment ONLY if it has positive amount! otherwise returns error(s)
        if($approved && $total_cost > 0){
            list($approved, $transaction_error) = members::process_payment($member['members_id'], $total_cost, $description, $data['security_code']);
        }
        
        if($approved || $total_cost <= 0){

            if($total_cost <= 0) { // if cost is zero
                //record 'fake' payment (payment of zero)
                $payments_table = new mysqli_db_table('payments');
                $payments_table->insert(array(
                    'join_members_id' => $member['members_id'],
                    'payments_amount' => $total_cost,
                    'payments_datetime' => SQL('NOW()'),
                    'payments_transaction_id' => 'none'
                ));
            }

            //send email to admin if a person who have (a)expired (b)trial membership becomes a member
            if($member['members_status'] == 'locked'){
                notify::notify_admin_member_free_trial_reactivation($member['members_id']);
            }

            //now the member can be set as a full member. also set members_membership_datetime
            $members_table->update(array(
                'members_status' => 'member',
                'members_membership_datetime' => SQL('NOW()')
            ), $member['members_id']);

            //echo $db->debug();

            //record initial bill
            $bills_table = new mysqli_db_table('bills');
            $bills_table->insert(array(
                'join_members_id' => $member['members_id'],
                'bills_amount' => $total_cost,
                'bills_type' => 'yearly',
                'bills_description' => $description,
                'bills_datetime' => SQL('NOW()'),
                'bills_paid' => 1,
                'join_discount_codes_id' => isset($code['discount_codes_id']) ? $code['discount_codes_id'] : 0
            ));


            //update uses other than billing since that may be transitioned away
            if(isset($code['discount_codes_id']))
            {
                $db->query('UPDATE discount_codes SET discount_codes_uses = ? WHERE discount_codes_id = ? ', array($code['discount_codes_uses'] + 1, $code['discount_codes_id']));

                $code_log_table = new mysqli_db_table('discount_code_logs');
                $code_log_table->insert(array(
                    'discount_code_logs_code' => $code['discount_codes_code'],
                    'join_discount_codes_id' => $code['discount_codes_id'],
                    'discount_code_logs_percent' => $code['discount_codes_percent'],
                    'discount_code_logs_amount' => $code['discount_codes_amount'],
                    'discount_code_logs_dateused' => SQL('NOW()'),
                    'discount_code_logs_final_balance' => $total_cost,
                    'join_members_id' => $member['members_id'],
                    'discount_code_logs_members_firstname' => $member['members_firstname'],
                    'discount_code_logs_members_lastname' => $member['members_lastname']
                ));
            }

            //Create Corporate Forum
            forums::create_corporate_forum($member['members_id']);

            // if member JOINs without trial period
            if($_REQUEST['members_id'] || $_SESSION['registration']['type'] == 'join'){
               $template = $account['accounts_type'] == 'multi' ? 'full-membership-welcome-corporate.php':'full-membership-welcome-individual.php';
            }
            // if member registers for trial period
            else{
                $template = $_SESSION['registration']['btype'] != 'corporation' ? 'full-membership-welcome-individual-verification.php' : 'full-membership-welcome-corporate-verification.php';
            }
            mail::send_template(
                $from     = MEMBERSHIP_EMAIL,
                $to       = $member['members_email'],
                $subject  = 'Full Membership Welcome | '.SITE_NAME,
                $template,
                $vars     = get_defined_vars()
            );

            $member['last4'] = members::get_credit_last4($data['members_stripe_id']);
            $template = 'payment-receipt.php';

            $member_data = $member;
            $member_data['bills_amount'] = $total_cost;
            mail::send_template(
                $from     = MEMBERSHIP_EMAIL,
                $to       = $member['members_email'],
                $subject  = 'Payment Receipt | '.SITE_NAME,
                $template = $template,
                $vars     = get_defined_vars()
            );

            notify::notify_admin_new_full_registration($member['members_id']);

			if ($_SESSION['registration']['type'] == 'join') {
                http::redirect(BASEURL .'/wp/thank-you/', array('notice' => 'You can now login'));
			} else {
                http::redirect(BASEURL . '/wp/thank-you/');
			}
        }else{
            $errors['payment_error'] = 'There was an error processing your payment: '.$transaction_error;
        }
    }
}
?>
