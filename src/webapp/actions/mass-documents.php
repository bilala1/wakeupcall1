<?php

// This will import everything in data/documents to the site's library.  The subfolder name is used to match with the category

$path = SITE_PATH . '/data/Documents';
$dir = opendir($path);


$db = mysqli_db::init();

function add_files($path, $db) {
	$target_dir = SITE_PATH . '/data/library/';
	
	$dir = opendir($path);
	while(false !== ($file = readdir($dir))) {
		echo 'Opening ' . $file . ' - ';
		if ($file{0} == '.') {
			continue;
		}
		
		if (is_dir($path . '/' . $file)) {
			add_files($path . '/' . $file, $db);
			continue;
		}
		
		
		$new_name = file::uniqname($path, file::get_extension($file));
		rename($path . '/' . $file, $target_dir . $new_name);
		
		$cat_id = $db->fetch_singlet('SELECT library_categories_id FROM library_categories WHERE library_categories_name LIKE ?', array(array_pop(explode('/', $path))));
		if (empty($cat_id)) {
			return;
		}
		$data = array(
			'join_library_categories_id' => $cat_id,
			'join_chemicals_id' => 0,
			'join_members_id' => 0,
			'documents_title' => array_shift(explode('.', $file)),
			'documents_datetime' => SQL('NOW()'),
			'documents_file' => $new_name,
			'documents_filename' => $file,
			'documents_type' => 'library',
			'documents_status' => 'active'
		);
		$table = new mysqli_db_table('documents');
		$table->insert($data);
		echo 'Added ' . $file . '<br/>';
		
	}
}

add_files($path, $db);

?>