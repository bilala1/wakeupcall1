<?php

// This script uploads everything in /data/temp_photos to the db / banner directory

$path = SITE_PATH . '/data/temp_photos/';
$dir = opendir($path);

$upload_folder = SITE_PATH . '/data/albums/';

while(false !== ($file = readdir($dir))) {
	if ($file == '.' || $file == '..')  {
		continue;
	}

	$filename = uniqid('alb_photo_') . '.jpg';
    
	$row = new mysqli_db_table('albums_photos');
	$data = array(
		'join_albums_id' => 13,
		'join_albums_photos_id' => 0,
		'albums_photos_datestamp' => SQL('NOW()'),
		'join_global_members_id' => 0,
		'albums_photos_published' => 0
	);
    $data['albums_photos_filename'] = $filename;
    $data['albums_photos_name'] = $filename;
    
	echo $path . $file;
	
    $image = new image($path . $file);
    $image->save_original($upload_folder . $filename.'.org.jpg');
	$row->insert($data);
	echo 'Added ' . $file . '<br/>';
}


?>