<? 
// this page is for those members whose automatic bill has failed.
// on this page they will have a chance to provide valid payment information and charge their card
require_once(LIBS . '/stripe_lib/stripe_include.php');

$db = mysqli_db::init();
$members = new mysqli_db_table('members');

// check if member with this id is locked and due for payment
$member_cnt = $db->fetch_singlet('SELECT COUNT(*) 
                                FROM members AS m
                                JOIN bills AS b ON (m.members_id = b.join_members_id)
                                WHERE members_status = "locked"
                                AND bills_paid = 0
                                AND members_id = ?',array($_REQUEST['members_id']));

if($member_cnt < 1){ // if a member with this id is not found redirect to home page
    http::redirect('/');
}
else {
    
    //member info
    //todo test licensed locations
    $member = $db->fetch_one('
        SELECT members.*, corporations_name, licensed_locations_name, corporations_id, licensed_locations_id
        FROM members
        left join corporations as c on c.join_members_id = members_id
        left join licensed_locations as ll on ll.join_members_id = members_id
        WHERE members_id = ?',
        array($_REQUEST['members_id'])
    );
    
    //all outstanding bills (should be one, unless changes later)
    $bills = $db->fetch_all('SELECT * 
                             FROM bills 
                             WHERE join_members_id = ?
                             AND bills_paid = 0',array($_REQUEST['members_id']));
    //annual billing date
    $billing_date = $bills[0]['bills_datetime'];
    
    $total_cost = 0.00;
    if(is_array($bills)){
        foreach($bills as $bill){
            $total_cost += (float)$bill['bills_amount'] ;
        }
    }
    
    // if form is submitted
    if($_POST){
        // validate input
        $data = $_POST;
        $validate = new validate($data);
        $validate->setOptional(array(
            'members_billing_addr2',
            'same_address',
            'discount_code'
        ))
        ->setTitles(array(
            'members_billing_addr1' => 'Address',
            'members_billing_city' => 'City',
            'members_billing_zip' => 'Zip',
            'members_card_name' => 'Name on Card',
            'members_card_num' => 'Card Number',
            'security_code' => 'Security Code'
        ))
        ->setCreditCard('members_card_num');   
        
        $errors = $validate->test();
        
        // if no errors AND if there are bills to be paid
        if(!$errors && is_array($bills)){
        
            $description = 'Yearly subscription: Payment info updated';

            $approved = false;

            try
            {
                $token = Stripe_Token::create(array( "card" => array( "number" => $data['members_card_num'],
                    //"cvc" => $_POST['members_card_cvc'],
                    "brand" => $data['members_card_type'],
                    "exp_month" => $data['members_card_expire_month'],
                    "exp_year" => $data['members_card_expire_year'],
                    "address_line1" =>  $data['members_billing_addr1'],
                    "address_line2" =>  $data['members_billing_addr2'],
                    "address_city" =>  $data['members_billing_city'],
                    "address_state" =>  $data['members_billing_state'],
                    "address_zip" =>  $data['members_billing_zip']
                )));

                if(!$member['members_stripe_id'])
                {
                    $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                        "card" => $token->id));

                    $data['members_stripe_id'] = $customer->id;
                }
                else
                {
                    $customer = Stripe_Customer::retrieve($member['members_stripe_id']);
                    $customer->description = $member['members_email'];
                    $customer->card = $token->id;
                    $customer->save();
                }

                $approved = true;
            }
            catch(Exception $e)
            {
                $approved = false;
                $errors['error'] = $e->getMessage();
            }

            if($approved)
            {
                $approved = false;
                list($approved, $transaction_error) = members::process_payment($member['members_id'], $total_cost, $description, $data);
            }

            if($approved){
                //update member's billing information
                $data = arrays::filter_keys($data, array(
                    'members_stripe_id',
                    'discount_code'
                ));
                $data['members_status'] = 'member';
                $members->update($data, $member['members_id']);
                
                // update information in bills table
                $db->query('UPDATE bills
                            SET bills_paid = 1
                            WHERE join_members_id = ? 
                            AND bills_paid = 0', array($member['members_id']));

                //Get all locations
                if($member['corporations_id']){
                    $locations = corporations::get_licensed_locations_for_corp_id($member['corporations_id']);
                }else{
                    $locations = array(licensed_locations::get_by_id($member['licensed_locations_id']));
                }

                //Unlock location user
                foreach($locations as $location) {
                    $members->update(array('members_status' => 'member'), $location['join_members_id']);

                    //Unlock staff as well
                    $staff = $db->fetch_all('
                        select *
                        from staff
                        where join_licensed_locations_id = ?'
                        , array($location['licensed_locations_id']));

                    foreach($staff as $staff_member) {
                        $members->update(array('members_status' => 'member'), $staff_member['join_members_id']);
                    }
                }
                $db->query('UPDATE notifications set notifications_news_update = 1 WHERE join_members_id = ?', array($member['members_id']));

                //notify admin
                notify::notify_admin_member_full_account_reactivated($member['members_id']);
                
                //send email-----------------------------------------------------------------------
                //decide which file to include
                $template = 'full-membership-rewelcome-corporate.php';
                // send email
                mail::send_template(
                    $from     = MEMBERSHIP_EMAIL,
                    $to       = $member['members_email'],
                    $subject  = 'Full Membership Welcome | '.SITE_NAME,
                    $template,
                    $vars     = get_defined_vars()
                );

                $member['last4'] = members::get_credit_last4($member['members_stripe_id']);
                $template = 'payment-receipt.php';
                $member_data = $member;
                $member_data['bills_amount'] = $total_cost;

                mail::send_template(
                    $from     = MEMBERSHIP_EMAIL,
                    $to       = $member['members_email'],
                    $subject  = 'Payment Receipt | '.SITE_NAME,
                    $template = $template,
                    $vars     = get_defined_vars()
                );
                //---------------------------------------------------------------------------------
                
                //redirect to thank you page
                http::redirect(BASEURL . '/wp/thank-you-renew/',array('renewal' => 'updated-info', 'email'=>$member['members_email']));
                
            }// end - if approved
            else {
                
            }
            
        } //end - if no errors
        else {
            $errors['error'] = 'There are no outstanding bills to be paid';
        }          
        
    } // - end if form was submitted

    //credit card
    $card_months = array();
    foreach(range(1, 12) as $month){
        $card_months[$month] = $month;
    }
    $card_years = array();
    foreach(range(date('Y'), date('Y') + 10) as $year){
        $card_years [$year]= $year;
    }   

     
}


?>