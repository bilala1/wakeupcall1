<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');
$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');
$member = $members_table->get(ActiveMemberInfo::GetMemberId());

//Get franchise if available
$franchise = $db->fetch_one('
	SELECT * FROM franchises WHERE join_members_id = ?
', array(ActiveMemberInfo::GetMemberId()));

if(isset($franchise) && !empty($franchise))
{
    $album = $db->fetch_one('select * from albums where albums_name = ?', array("logos"));
    $photo = $db->fetch_one('select * from albums_photos where albums_photos_id= ?', array($franchise['join_albums_photos_id']));
}


if(ActiveMemberInfo::IsAccountMultiLocation() && ActiveMemberInfo::_IsMainAccountAdmin()) {
    $corporation = $db->fetch_one('
        select *
        from corporations
        join forums on join_forums_id = forums_id
        where join_accounts_id = ?',
        array(ActiveMemberInfo::GetAccountId())
    );
}

if($_POST){    
    
    $data = $_POST;
    $errors = array();
    
    $validate = new validate($data);
    $validate
       // ->setEmail('members_email')
        ->setOptional('members_password')
        ->setOptional('members_password2')
        ->setOptional('members_fax')
        ->setOptional('members_phone')
        ->setOptional('members_forum_signature')
        ->setMatch('members_password', 'members_password2', 'Password');
    
    if($corporation){
        $validate->setTitle('forums_name', 'Forum Name');
    }
    
    $errors = $validate->test();
    if(!$errors){
        //filter data
        $member_data = arrays::filter_keys($data, array('members_email', 'members_firstname', 'members_lastname', 'members_title', 'members_phone', 'members_fax'));

        if($data['members_password'] && ($data['members_password'] == $data['members_password2']) ){
            $member_data['members_password'] = hash('sha256', $data['members_password']);
        }
        
        $members_table->update($member_data, ActiveMemberInfo::GetMemberId());
        
        if($corporation){
            $forums_table = new mysqli_db_table('forums');
            $forums_table->update(array('forums_name' => $data['forums_name']), $corporation['forums_id']);
        }
        
        $notice = 'Account Details Updated';
        
        http::redirect(BASEURL . '/members/account/account-details.php', array('notice'  => $notice));
    }else{
        //so user input is not lost
        $member = array_merge((array)$member, $data);
        
        if($corporation){
            $corporation = array_merge((array)$corporation, $data);
        }
    }
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));
