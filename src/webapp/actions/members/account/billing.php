<?php
include(ACTIONS . '/members/protect.php');
require_once(LIBS . "/stripe_lib/stripe_include.php");
$page = str_replace(BASEURL,'',$_SERVER['REQUEST_URI']);

//PUT BACK SSL IN PRODUCTION
if(DEPLOYMENT === 'production'){
    if(!$_SERVER['HTTPS']) {
        Header( "HTTP/1.1 301 Moved Permanently" );
        Header( "Location: " . HTTPS_FULLURL . $page);
    }
}

$db = mysqli_db::init();

$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$bills = $db->fetch_all('select * from bills where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$card_months = array();
foreach(range(1, 12) as $month){
    $card_months[$month] = $month;
}

$card_years = array();
foreach(range(date('Y'), date('Y') + 10) as $year){
    $card_years [$year]= $year;
}

if($member['members_stripe_id'])
{
    $member = members::get_customer_credit_card_data($member);
}

if($_POST){
    $members = new mysqli_db_table('members');
    
    $data = $_POST;
    $validate = new validate($data);
    $validate
        ->setCreditCard('members_card_num')
        ->setOptional('members_billing_addr2');
    
    $errors = $validate->test();
    
    if(!$errors)
    {
        try
        {
            $token = Stripe_Token::create(array( "card" => array( "number" => $_POST['members_card_num'],
                //"cvc" => $_POST['members_card_cvc'],
                "brand" => $_POST['members_card_type'],
                "exp_month" => $_POST['members_card_expire_month'],
                "exp_year" => $_POST['members_card_expire_year'],
                "address_line1" =>  $_POST['members_billing_addr1'],
                "address_line2" =>  $_POST['members_billing_addr2'],
                "address_city" =>  $_POST['members_billing_city'],
                "address_state" =>  $_POST['members_billing_state'],
                "address_zip" =>  $_POST['members_billing_zip']
            )));

            if(!$member['members_stripe_id'])
            {
                    $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                                                    "card" => $token->id));

                    $data['members_stripe_id'] = $customer->id;
            }
            else
            {
                try
                {
                    $customer = Stripe_Customer::retrieve($member['members_stripe_id']);
                    $customer->description = $member['members_email'];
                    $customer->card = $token->id;
                    $customer->save();
                }
                catch(Exception $e)
                {

                    $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                        "card" => $token->id));
                    $data['members_stripe_id'] = $customer->id;
                    $members_tbl = new mysqli_db_table('members');
                    $members_tbl->update($data, $member['members_id']);
                }
            }

            $members->update($data, $member['members_id']);
            http::redirect(BASEURL .'/members/account/billing.php',array('notice'=>'Billing Information Updated'));
        }
        catch(Exception $e)
        {
            $member = $_POST;

            $errors = array('members_card_num' => $e->getMessage());
        }

    }
    else {
        $member = $_POST;
    }
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

?>
