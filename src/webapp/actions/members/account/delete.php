<?php

//TODO I can't find where this page might be used - can members delete their own accounts?

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

//delete account
if($_POST['delete']&&
    ($_SESSION['members_type']=='corporation' || (int)$_SESSION['join_corporations'] <1 )){



    if($_SESSION['members_type']=='corporation' ){
        $db->query('UPDATE corporations SET corporations_delete_datetime = NOW() WHERE corporations_id = ?',array($_SESSION['corporations_id']));
    }
    elseif((int)$_SESSION['join_corporations'] <1 ){
        $db->query('UPDATE licensed_locations SET licensed_locations_delete_datetime = NOW() WHERE licensed_locations_id = ?',array($_SESSION['licensed_locations_id']));
    }
    
    //notify admin 
    notify::notify_admin_member_account_canceled(ActiveMemberInfo::GetMemberId());
    
    members::logout();
    $notice = 'Account Deleted';
    http::redirect(BASEURL . '/index.php', array('notice'  => $notice)); 
}


?>