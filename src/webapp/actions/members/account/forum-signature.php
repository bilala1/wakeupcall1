<?php

$db = mysqli_db::init();

$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$member = $db->fetch_one('
	SELECT * FROM members WHERE members_id = ?
', array(ActiveMemberInfo::GetMemberId()));

if ($_POST) {
	$data = $_POST;

	$db->query('
		UPDATE members SET members_forum_signature = ? WHERE members_id = ?
	', array($data['members_forum_signature'], ActiveMemberInfo::GetMemberId()));

	http::redirect(FULLURL . '/members/account/forum-signature.php', array('notice' => 'Your forum signature has been saved.'));
}

?>