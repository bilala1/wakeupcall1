<?php
include(ACTIONS . '/members/protect.php');

if (!empty($_REQUEST['franchises_id'])) {

    $db = mysqli_db::init();
    //$bills_table = new mysqli_db_table('bills');
    $franchises_table = new mysqli_db_table('franchises');
    $franchise = $db->fetch_one('select * from franchises left join members on members_id = join_members_id where franchises_id = ?', array($_REQUEST['franchises_id']));


    if ($franchise['franchises_amount_discount'] > 0) {
        $franchise['code_value'] = '$'.$franchise['franchises_amount_discount'];
    } else {
        $franchise['code_value'] = $franchise['franchises_pct_discount'].'%';
    }

    $album = $db->fetch_one('select * from albums where albums_name = ?', array("logos"));
    $photo = $db->fetch_one('select * from albums_photos where albums_photos_id= ?', array($franchise['join_albums_photos_id']));

}

if($_POST){
    $data = $_POST;
    $errors = array();

    if (empty($data['code_value'])) {
        $errors['code_value'] = 'Please enter a code';
    }
    if (empty($data['code_value'])) {
        $errors['code_value'] = 'Please enter a value';
    } else {
        if (preg_match('/^\$[0-9\.]+$/', trim($data['code_value']))) {
            $franchise['franchises_amount_discount'] = str_replace('$', '', $data['code_value']);
            $franchise['franchises_pct_discount'] = '';
            if ($franchise['franchises_amount_discount'] < 0) {
                $errors['code_value'] = 'Please enter an amount greater than 0';
            }
        } else if (preg_match('/^[0-9\.]+%$/', trim($data['code_value']))) {
            $franchise['franchises_pct_discount'] = str_replace('%', '', $data['code_value']);
            $franchise['franchises_amount_discount'] = '';
            if ($franchise['franchises_pct_discount'] > 100 || $franchise['franchises_pct_discount'] < 0) {
                $errors['code_value'] = 'Please enter a percent between 0 and 100';
            }
        } else {
            $errors['code_value'] = 'Please enter in the format "$xx.xx" or "xx%"';
        }
    }
    if (count($errors) == 0) {

        $franchise['join_members_id']= $_POST['join_members_id'];
        $table = new mysqli_db_table('franchises');
        if (isset($_REQUEST['franchises_id'])) {
            $table->update($franchise, $_REQUEST['franchises_id']);
            http::redirect('list.php?notice=Franchise+Updated');
            exit;
        } else {
            $table->insert($franchise);
            http::redirect('list.php?notice=Franchise+Added');
            exit;
        }

    }



    $validate = new validate(array());
    $validate
        ->setAllOptional();
    
    $errors = $validate->test();
    
    if(empty($errors)){
        if(empty($data['franchises_id'])){
            //Create new
            $data['franchises_delete_datetime'] = SQL('NULL');
            $franchises_table->insert($data);
            $franchises_id = $franchises_table->last_id();
            
            $notice = 'Franchise added';
        }else{
            //Update
            if($data['franchises_delete']){ // if checkbox for delete is checked
                if($franchise['franchises_delete_datetime']){ // if delete date is already set, keep it
                    $data['franchises_delete_datetime'] = $franchise['franchises_delete_datetime'];
                }
                else { // if delete checkbox was chekced now, set to current date
                    $data['franchises_delete_datetime'] = SQL('NOW()');
                }
            }

            $franchises_table->update($data, $data['franchises_id']);
            $franchises_id = $data['franchises_id'];

            
            if(!$notice){
                $notice = 'Franchise updated';
            }
        }
        
        http::redirect('edit.php?franchises_id=' . $franchises_id . '&notice=' . urlencode($notice));
    }else{
        //so user input is not lost
        $franchise = array_merge($franchise, $data);
    }
}
?>