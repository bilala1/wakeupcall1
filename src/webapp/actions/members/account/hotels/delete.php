<?php
include_once ACTIONS . '/members/protect.php';

$db = mysqli_db::init();

if($_REQUEST['licensed_locations_id']){

    $user_can_delete = ActiveMemberInfo::_IsAccountAdmin() &&
        UserPermissions::UserCanAccessObject('licensed_locations', $_REQUEST['licensed_locations_id']);

    if($user_can_delete){
        $licensed_location = $db->fetch_one('SELECT * from licensed_locations WHERE licensed_locations_id = ?', array($_REQUEST['licensed_locations_id']));
                
        $db->query('UPDATE licensed_locations SET licensed_locations_delete_datetime = NOW() WHERE licensed_locations_id = ?', array($_REQUEST['licensed_locations_id']));
        $notice = 'Location is deleted. You have '.DELETE_ENTITY_DAYS.' days to renew it.';
        
        $account = ActiveMemberInfo::GetAccount();
        $current_member = ActiveMemberInfo::GetMember();
        
        mail::send_template(
                    $from     = AUTO_EMAIL,
                    $to       = ADMIN_EMAIL,
                    $subject  = 'Corporation Location Deleted ' ,
                    $template = 'admin-delete-location.php',
                    $vars     = get_defined_vars()
                );
    }
}

$query = $_GET;
unset($query['licensed_locations_id']);

http::redirect(BASEURL . '/members/account/hotels/list.php', ($query + array('notice' => $notice)));
?>