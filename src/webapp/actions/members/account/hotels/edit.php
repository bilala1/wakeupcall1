<?php

include(ACTIONS . '/members/protect.php');
include (SITE_PATH."/library/"."coupon_comparer.php");

include(MODELS . '/members/account/hotels/edit.php');

$db = mysqli_db::init();

if(!array_key_exists('licensed_locations_id', $_REQUEST) || !$_REQUEST['licensed_locations_id']) {
    // TODO This is going to have to change with shopping cart - hacking in retrieval of account billing info for now
    $account_admin = ActiveMemberInfo::GetAccountAdmin();
    $now = date('Y-m-d', strtotime('now'));
    $corp_next_bill_date = $db->fetch_one('select DATE(bills_datetime) AS bills_date, (DATE(bills_datetime) + INTERVAL 1 YEAR) AS bills_date_future from bills where join_members_id = ? and bills_type = ? order by bills_datetime desc limit 1', array($account_admin['members_id'], 'yearly'));
    $days_until_next_bill = round(times::days_between($corp_next_bill_date['bills_date_future'], $now));
    $days_in_year = round(times::days_between($corp_next_bill_date['bills_date_future'], $corp_next_bill_date['bills_date']));
}

$comparer = new CouponComparer();

// AJAX --
if ($_REQUEST['ajax'] == 'coupon-pricing') {
    $result = $comparer->GetDiscountCodePricing($db, $_REQUEST['discount_code']);

    if(empty($result)){
        die('fail');
    }
    else {
        $result['discount_code_total_as_number'] = round(($result['discount_code_total_as_number'] / $days_in_year) * $days_until_next_bill, 2);
        $result['discount_code_total'] = money_format('%n', $result['discount_code_total_as_number']).' USD';;
        die(json_encode($result));
    }
} // - AJAX


// AJAX --
if ($_REQUEST['ajax'] == 'franchise-pricing') {
    $result = $comparer->GetFranchiseCodePricing($db, $_REQUEST['franchise_code']);

    if(empty($result)){
        die('fail');
    }
    else {
        $result['franchise_code_total_as_number'] = round(($result['franchise_code_total_as_number'] / $days_in_year) * $days_until_next_bill, 2);
        $result['franchise_code_total'] = money_format('%n', $result['franchise_code_total_as_number']).' USD';;
        die(json_encode($result));
    }
} // - AJAX

// AJAX --
if ($_REQUEST['ajax'] == 'franchise-and-coupon-pricing') {
    $result1 = $comparer->GetDiscountCodePricing($db, $_REQUEST['discount_code']);
    $result2 = $comparer->GetFranchiseCodePricing($db, $_REQUEST['franchise_code']);
    $result = array_merge($result1, $result2);

    if(empty($result)){
        die('fail');
    }
    else {
        if(!empty($result1)) {
            $result['discount_code_total_as_number'] = round(($result['discount_code_total_as_number'] / $days_in_year) * $days_until_next_bill, 2);
            $result['discount_code_total'] = money_format('%n', $result['discount_code_total_as_number']).' USD';;
        }
        if(!empty($result2)) {
            $result['franchise_code_total_as_number'] = round(($result['franchise_code_total_as_number'] / $days_in_year) * $days_until_next_bill, 2);
            $result['franchise_code_total'] = money_format('%n', $result['franchise_code_total_as_number']).' USD';;
        }
        header('Content-type:application/json;charset=utf-8');
        die(json_encode($result));
    }
} // - AJAX

if(!$_REQUEST['licensed_locations_id']) {
    if ($account_admin['members_status'] == 'member') {
        $total_cost = YEARLY_COST;

        if($_REQUEST['discount_code']) {
            $discount = $comparer->GetDiscountCodePricing($db, $_REQUEST['discount_code']);
            if(array_key_exists('discount_code_total', $discount)) {
                $total_cost = min($total_cost, $discount['discount_code_total']);
                $_REQUEST['discount_codes_id'] = $discount['discount_codes_id'];
            }
        }
        if($_REQUEST['franchise_code']) {
            $discount = $comparer->GetFranchiseCodePricing($db, $_REQUEST['franchise_code']);
            if(array_key_exists('franchise_code_total', $discount)) {
                $total_cost = min($total_cost, $discount['franchise_code_total']);
                $_REQUEST['franchises_id'] = $discount['franchises_id'];
            }
        }

        //determine prorated fee amount
        $total_cost = round(($total_cost / $days_in_year) * $days_until_next_bill, 2);
    }

    // check if not exempt. exempt should not be billed
    if ($account_admin['members_status'] == 'exempt') {
        $total_cost = 0;
    }
}
$total_cost_string = money_format('%n', $total_cost).' USD';
$path_to_ajax_discount_calc = '/members/account/hotels/edit.php';

$licensed_locations_table = new mysqli_db_table('licensed_locations');
$hotels_table = new mysqli_db_table('hotels');
$corporate_locations_table = new mysqli_db_table('corporate_locations');
$bills_table = new mysqli_db_table('bills');
$notifications_table = new mysqli_db_table('notifications');

$licensed_location = licensed_locations::get_by_id_with_details($_REQUEST['licensed_locations_id']);
$location_email_permissions = emails_templates_permissions::get_for_location($_REQUEST['licensed_locations_id']);
//make sure this is this corporation's licensed_location (if trying to edit)
if($_REQUEST['licensed_locations_id'] && !in_array($_REQUEST['licensed_locations_id'], UserPermissions::AllObjectIdsForUser('licensed_locations'))){
    http::redirect(BASEURL . '/members/account/hotels/list.php', array('notice' => 'You cannot edit this location'));
}

$model = new models\members\account\hotels\edit();

$model->number_of_rooms = $licensed_location['hotels_num_rooms'];

if($_POST){
    $data = $_POST;
    $errors = array();

    $validate = new validate($data);
    $validate
        ->setOptional('franchises_id')
        ->setOptional('discount_codes_id')
        ->setOptional('discount_code')
        ->setOptional('franchise_code')
        ->setOptional('licensed_locations_franchise')
        ->setOptional('licensed_locations_fax')
        ->setOptional('hotels_num_rooms')
        ->setTitles(array(
            'licensed_locations_name' => 'Location Name',
        ))
        ->setPhoneNum('licensed_locations_phone', 'Phone')
        ->setPhoneNum('licensed_locations_fax' ,'Fax');

    // check to see if number of rooms should be included...
    $locationArray = licensed_locations::get_location_types();
    $LocationType = $locationArray[$data['licensed_locations_full_type']];
    if (!is_null($LocationType) && $LocationType == 'Hotel') {
        $validate
            ->unsetOptional('hotels_num_rooms', 'number of rooms')
            ->setInteger('hotels_num_rooms');
        //} && empty($data['hotels_num_rooms']) ) {
        //$errors['hotels_num_rooms'] = 'Please specify the number of rooms.';
    }
    //if modifying existing licensed_location
    if($_REQUEST['licensed_locations_id']){
        $validate
            ->setOptional('licensed_locations_pass')
            ->setOptional('licensed_locations_pass2')
            ->setOptional('licensed_locations_type')
            ->setOptional('licensed_locations_sub_type');

        // let these come from our earlier retrieval from the database
        $data['licensed_locations_type'] = $licensed_location['licensed_locations_type'];
        $data['licensed_locations_sub_type'] = $licensed_location['licensed_locations_sub_type'];
        $data['hotels_industry_type'] = $licensed_location['hotels_industry_type'];
        // How much scrubbing should we do?
        //unset($data['hotels_id']);
        //unset($data['corporate_locations_id']);
        //unset($data['corporations_id']);
    }
    //if creating new licensed_location
    else {
        $validate
            ->setOptional('licensed_locations_id');

        if(!in_array($data['licensed_locations_full_type'], array_keys(licensed_locations::get_location_types()))) {
            $errors['licensed_locations_full_type'] = 'Please select a location type';
        }
        $data['licensed_location_type'] = licensed_locations::get_type_from_full_type($data['licensed_locations_full_type']);
        $data['hotels_industry_type'] = $data['licensed_locations_full_type'];

        // TODO This is going to have to change with shopping cart - hacking in retrieval of account billing info for now
        // check for credit card info, if credit card info is not set, notify user
        if($total_cost > 0 && $data['licensed_location_type'] != 'Corporate Office' && strlen($account_admin['members_stripe_id']) < 1) {
            $errors[1] = 'Credit card is missing.';
        }
    }

    $errors = array_merge($errors, $validate->test());

    if(empty($errors)){

        if(empty($data['licensed_locations_id']))
        {
            // if status of this member is set to 'member' = paid member
            if(members::get_members_status(ActiveMemberInfo::GetMemberId())=='member'){
                $bill_description = 'Added location: '.$data['licensed_locations_name'].' ('.date(DATE_FORMAT).' - '.date(DATE_FORMAT, strtotime($corp_next_bill_date['bills_date_future'])).')';

                $is_discounted = false;
                //Check if discount
                if( (isset($_REQUEST['discount_codes_id']) && $_REQUEST['discount_codes_id'] > 0)
                    || (isset($_REQUEST['franchises_id']) &&  $_REQUEST['franchises_id'] > 0) )
                {
                    $is_discounted = true;
                }

                // check if not exempt. exempt should not be billed
                if($account_admin['members_status'] == 'exempt'){
                    $exempt = true;
                }
                else if ($total_cost < 1 && $is_discounted) {
                    //If discount or group code gives 100% off then allow approval
                    $approved = true;
                }
                else{
                    //attempt to charge user
                    list($approved, $transaction_error) = members::process_payment($account_admin['members_id'], $total_cost, $bill_description);
                }
                if($approved || $exempt){
                    $current_member = ActiveMemberInfo::GetMember();

                    ////if payment approved
                    //Create bill
                    $bills_table->insert(array(
                        'join_members_id' => $account_admin['members_id'],
                        'bills_amount' => $total_cost,
                        'bills_type' => 'location',
                        'bills_description' => $bill_description,
                        'bills_datetime' => SQL('NOW()'),
                        'bills_paid' => 1,
                        'join_discount_codes_id' => isset($_REQUEST['discount_codes_id']) ? $_REQUEST['discount_codes_id'] : 0
                    ));

                    //update uses other than billing since that may be transitioned away
                    if(isset($_REQUEST['discount_codes_id']) && $_REQUEST['discount_codes_id'] > 0)
                    {
                        $code = $db->fetch_one('
                            SELECT * FROM discount_codes WHERE discount_codes_id= ?
                            AND discount_codes_start <= NOW()
                            AND discount_codes_end >= NOW()',
                                                array($_REQUEST['discount_codes_id'])
                                            );

                        $db->query('UPDATE discount_codes SET discount_codes_uses = ? WHERE discount_codes_id = ? ', array($code['discount_codes_uses'] + 1, $code['discount_codes_id']));

                        $code_log_table = new mysqli_db_table('discount_code_logs');
                        $code_log_table->insert(array(
                            'discount_code_logs_code' => $code['discount_codes_code'],
                            'join_discount_codes_id' => $code['discount_codes_id'],
                            'discount_code_logs_percent' => $code['discount_codes_percent'],
                            'discount_code_logs_amount' => $code['discount_codes_amount'],
                            'discount_code_logs_dateused' => SQL('NOW()'),
                            'discount_code_logs_final_balance' => $total_cost,
                            'join_members_id' => $current_member['members_id'],
                            'discount_code_logs_members_firstname' => $current_member['members_firstname'],
                            'discount_code_logs_members_lastname' => $current_member['members_lastname']
                        ));
                    }

                }
                else { // if payment not approved
                    $errors[] = $transaction_error;
                    //so user input is not lost
                    $licensed_location = array_merge((array)$licensed_location, $data);
                }

            }
            if($approved || $exempt || $account_admin['members_status']=='free'){//if payment approved

                //Create new licensed_location
                $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();

                if(isset($_REQUEST['franchises_id']) &&  $_REQUEST['franchises_id'] > 0)
                {
                    $data['join_franchises_id'] = $_REQUEST['franchises_id'];
                }

                $licensed_locations_id = licensed_locations::create($data);

                $data['join_licensed_locations_id'] = $licensed_locations_id;
                if($data['licensed_locations_type'] == 'Corporate Office') {
                    corporate_locations::create($data);
                } else if($data['licensed_locations_type'] == 'Hotel') {
                    hotels::create($data);
                }

                $notice = 'Hotel Added';
                $account = ActiveMemberInfo::GetAccount();

                mail::send_template(
                    $from     = AUTO_EMAIL,
                    $to       = ADMIN_EMAIL,
                    $subject  = 'New Corporate Location Added ' ,
                    $template = 'admin-new-location.php',
                    $vars     = get_defined_vars()
                );


                $member = ActiveMemberInfo::GetMember();
                if (isset($member) && isset($member['members_email'])) {
                    mail::send_template(
                        $from     = AUTO_EMAIL,
                        $to       = $member['members_email'],
                        $subject  = 'New Corporate Location Added | ' . SITE_NAME,
                        $template = 'new-corporation-location-added.php',
                        $vars     = get_defined_vars()
                    );

                    $member_data = $member;
                    $member['last4'] = members::get_credit_last4($account_admin['members_stripe_id']);
                    $template = 'payment-receipt.php';
                    $member_data['bills_amount'] = $total_cost;

                    mail::send_template(
                        $from     = MEMBERSHIP_EMAIL,
                        $to       = $member['members_email'],
                        $subject  = 'Payment Receipt| '.SITE_NAME,
                        $template = $template,
                        $vars     = get_defined_vars()
                    );
                }


                if($member['members_id'] != $account_admin['members_id']) {
                    $member_data = $member = $account_admin;

                    mail::send_template(
                        $from     = MEMBERSHIP_EMAIL,
                        $to       = $account_admin['members_email'],
                        $subject  = 'Payment Receipt| '.SITE_NAME,
                        $template = $template,
                        $vars     = get_defined_vars()
                    );
                }

                unset($member);
                unset($member_data);

                http::redirect(BASEURL . '/members/account/hotels/list.php', array('notice'  => $notice));
            }else{
                $errors[] = $transaction_error;
                //so user input is not lost
                $licensed_location = array_merge((array)$licensed_location, $data);
            }
        }else{ // if editing existing licensed_location

            //Update
            $licensed_locations_table->update($data, $licensed_location['licensed_locations_id']);
            $hotels_table->update($data, $licensed_location['hotels_id']);
            $corporate_locations_table->update($data, $licensed_location['corporate_locations_id']);
            $notice = 'Location Updated';

            if(isset($_POST['edit_claims_emails_permission']))
                emails_templates_permissions::add_or_update_claims_email_for_location($licensed_location['licensed_locations_id'], true);           
            else
                emails_templates_permissions::add_or_update_claims_email_for_location($licensed_location['licensed_locations_id'], false);
            
            if(isset($_POST['edit_certs_emails_permission']))
                emails_templates_permissions::add_or_update_certs_for_location($licensed_location['licensed_locations_id'], true);   
            else
                emails_templates_permissions::add_or_update_certs_for_location($licensed_location['licensed_locations_id'], false); 
            
            
            if(ActiveMemberInfo::IsUserMultiLocation()) {
                http::redirect(BASEURL . '/members/account/hotels/list.php', array('notice'  => $notice));
            } else {
                http::redirect(BASEURL . '/members/account/hotels/edit.php', array('notice'  => $notice, 'licensed_locations_id'=> $licensed_location['licensed_locations_id']));
            }
        }
    }else{ // if errors present
        //so user input is not lost
        $licensed_location = array_merge((array)$licensed_location, $data);
    }
    $model->location_type = $data['licensed_locations_full_type'];
    $model->number_of_rooms = $data['hotels_num_rooms'];
}

?>
