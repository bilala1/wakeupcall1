<?
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$paging = new paging(25, $_REQUEST['page']);

$params = array(ActiveMemberInfo::GetAccountId());

$deletedLocationsClause = '';
if(ActiveMemberInfo::_IsMainAccountAdmin()) {
    $deletedLocationsClause =
        ' OR (ll.join_accounts_id = ? AND
         licensed_locations_delete_datetime IS NOT NULL AND
         NOW() - INTERVAL 60 day < licensed_locations_delete_datetime) ';
    $params[] = ActiveMemberInfo::GetAccountId();
}

$query = '
    SELECT SQL_CALC_FOUND_ROWS *, (
        SELECT GROUP_CONCAT(DISTINCT(members_id) SEPARATOR \',\')
        FROM members m, licensed_locations_x_members_access llxm
        WHERE (m.members_id = llxm.join_members_id AND llxm.join_licensed_locations_id = ll.licensed_locations_id)
           OR (m.join_accounts_id = ? AND m.members_type = \'admin\')
        ) as `members_ids`
    FROM licensed_locations as ll
    left join hotels as h on ll.licensed_locations_id = h.join_licensed_locations_id
    left join corporate_locations as cl on ll.licensed_locations_id = cl.join_licensed_locations_id
    WHERE '
    . UserPermissions::CreateInClause('licensed_locations')
    . $deletedLocationsClause
    . strings::orderby('licensed_locations_name', array('licensed_locations'), array('members_names'));


$licensed_locations = $db->fetch_all($query, $params);
$paging->set_total($db->found_rows());

$licensed_locations = $db->fetch_all($query, $params);

$unkeyed_members = $db->fetch_all('select members_id, members_firstname, members_lastname from members where join_accounts_id = ?',
                array(ActiveMemberInfo::GetAccountId()));
$members = array();
foreach($unkeyed_members as $member) {
    $members[$member['members_id']] = $member;
}

//Determine if the previous yearly fee would have included a temporarily deleted licensed_location
$last_bill_date = $db->fetch_singlet('select bills_datetime from bills b inner join accounts a on b.join_members_id = a.join_members_id where accounts_id = ? and bills_type = "yearly" order by bills_datetime desc limit 1', array(ActiveMemberInfo::GetAccountId()));

foreach($licensed_locations as &$licensed_location){
    if($licensed_location['licensed_locations_delete_datetime'] && strtotime($licensed_location['licensed_locations_delete_datetime']) < strtotime($last_bill_date)){
        $licensed_location['needs_charge'] = true;
    }
    licensed_locations::add_types_to_location($licensed_location);

    $licensed_location['members_ids'] = explode(',', $licensed_location['members_ids']);

    $locations_by_type[$licensed_location['licensed_locations_full_type']][] =$licensed_location;
}

unset($licensed_location);
$licensed_location_types['Corporate Office'] = 'Corporate Offices';
$licensed_location_types['Spa - Destination Spa'] = 'Destination Spas';
$licensed_location_types['Hotel - Full Service/Resort'] = 'Full Service Hotels/Resorts';
$licensed_location_types['Hotel - Limited Service'] = 'Limited Service Hotels';
$licensed_location_types['Other'] = 'Other';

// whether collapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));




?>
