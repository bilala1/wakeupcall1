<?php


include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$hotels_table = new mysqli_db_table('hotels');
$hotel = $hotels_table->get($_REQUEST['hotels_id']);

//make sure user can access this member
if(!UserPermissions::UserCanAccessObject('members', $_REQUEST['members_id'])){
    http::redirect(BASEURL . '/members/account/hotels/members/list.php', array('notice' => 'You cannot view this member'));
}

$paging = new paging(25, $_REQUEST['page']);

$member = members::get_by_id($_REQUEST['members_id']);

$activities = $db->fetch_all('
    SELECT SQL_CALC_FOUND_ROWS *
    FROM (
        SELECT actions_datetime, actions_name from actions
            WHERE join_members_id = ?
        UNION
        SELECT claims_history_datetime as `actions_datetime`, CONCAT(\'Claim Action: \', claims_history_name) as `actions_name` FROM claims_history
            WHERE join_members_id = ?
        UNION
        SELECT contracts_history_datetime as `actions_datetime`, CONCAT(\'Contract Action: \', contracts_history_description) as `actions_name` FROM contracts_history
            WHERE join_members_id = ?
        UNION
        SELECT vendors_history_datetime as `actions_datetime`, CONCAT(\'Vendors Action: \', vendors_history_description) as `actions_name` FROM vendors_history
            WHERE join_members_id = ?
        ) AS actions
    order by actions_datetime desc
    '.$paging->get_limit(),
    array($_REQUEST['members_id'], $_REQUEST['members_id'], $_REQUEST['members_id'], $_REQUEST['members_id']
));
//pre($db->debug());
$paging->set_total($db->found_rows());

?>
