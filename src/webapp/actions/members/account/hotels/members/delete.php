<?php
include_once ACTIONS . '/protect.php';
$db = mysqli_db::init();

$delete_member_id = $_REQUEST['members_id'];

$member_table = new mysqli_db_table('members');
$members_library_categories_table = new mysqli_db_table('members_library_categories');

$member_table = new mysqli_db_table('members');
$member = $db->fetch_one('
        SELECT *,
            (SELECT count(1) from members_x_documents mxd where join_members_id = members_id and documents_is_corporate_shared = 0) as `has_unshared_docs`,
            (SELECT count(1) from members_x_documents mxd where join_members_id = members_id and documents_is_corporate_shared = 1) as `has_shared_docs`
        FROM members
        WHERE members_id = ?
        ', array($delete_member_id));
$account = ActiveMemberInfo::GetAccount();

$delete_allow = false;
//How to figure out if the user can delete the member?

// If user is for a different account, block that.
if($member['join_accounts_id'] != $account['accounts_id']) {
    $delete_allow = false;
}
// Main account admin can never be deleted
else if($account['join_members_id'] == $member['members_id']) {
    $delete_allow = false;
}
// If the user is the main account admin, they can delete anyone in the account
else if(ActiveMemberInfo::_IsMainAccountAdmin()) {
    $delete_allow = true;
}
// If the user has manage admin rights, they can delete anyone in the account
else if(UserPermissions::UserCanAccessScreen('manageAdmins')) {
    $delete_allow = true;
}
// If the user is an admin, they can delete any non-admin in the account
else if(ActiveMemberInfo::_IsAccountAdmin()) {
    $delete_allow = $member['members_type'] == 'user';
}
// If the user has manageUsers roles for one or more accounts, they can delete any regular users that have associations only with their locations.
else if(UserPermissions::UserCanAccessScreen('manageUsers')) {
    $location_ids = UserPermissions::LocationIdsWithPermission('manageUsers');
    $in_clause = "(" . implode(', ', $location_ids) . ")";

    $other_locations = $db->fetch_singlet('
        SELECT COUNT(1) FROM licensed_locations_x_members_access
        WHERE join_licensed_locations NOT IN ' . $in_clause);

    $delete_allow = ($other_locations == 0);
}

if($delete_allow){

    $member['total_docs'] = $member['has_shared_docs'] + $member['has_unshared_docs'];

    if($_POST) {
        $data = $_POST;
        $errors = array();

        $validate = new validate($data);
        $validate->setAllOptional()
            ->unsetOptional('members_id');

        $errors = $validate->test();

        if($member['has_unshared_docs'] > 0 && !$data['unshared_docs']) {
            $errors['unshared_docs'] = 'Please choose what to do with the member\'s unshared documents';
        }
        if($member['has_shared_docs'] > 0 && !$data['shared_docs']) {
            $errors['shared_docs'] = 'Please choose what to do with the member\'s shared documents';
        }

        if(empty($errors)) {

            // We are transferring some documents, so create a new folder for it
            if($data['unshared_docs'] === 'transfer' || $data['shared_docs'] === 'transfer') {
                $members_library_categories_table->insert(array(
                    'members_library_categories_name' => 'From ' . $member['members_email'],
                    'join_members_id' => ActiveMemberInfo::GetMemberId()
                ));
                $members_library_categories_id = $members_library_categories_table->last_id();

                // Check if unshared docs need to be transfered to this member
                if($member['has_unshared_docs'] > 0 && $data['unshared_docs'] === 'transfer') {
                    $db->query('
                        UPDATE documents d, members_x_documents mxd SET d.join_members_id = ?
                        WHERE join_documents_id = documents_id AND d.join_members_id = ? AND documents_is_corporate_shared = 0
                        ', array(ActiveMemberInfo::GetMemberId(), $delete_member_id));
                    $db->query('
                        UPDATE members_x_documents SET join_members_id = ?, join_members_library_categories_id = ?
                        WHERE join_members_id = ? AND documents_is_corporate_shared = 0
                        ', array(ActiveMemberInfo::GetMemberId(), $members_library_categories_id, $delete_member_id));
                }

                // Check if shared docs need to be transfered to this member
                if($member['has_shared_docs'] > 0 && $data['shared_docs'] === 'transfer') {
                    $db->query('
                        UPDATE documents d, members_x_documents mxd SET d.join_members_id = ?
                        WHERE join_documents_id = documents_id AND d.join_members_id = ? AND documents_is_corporate_shared = 1
                        ', array(ActiveMemberInfo::GetMemberId(), $delete_member_id));
                    $db->query('
                        UPDATE members_x_documents SET join_members_id = ?, join_members_library_categories_id = ?
                        WHERE join_members_id = ? AND documents_is_corporate_shared = 1
                        ', array(ActiveMemberInfo::GetMemberId(), $members_library_categories_id, $delete_member_id));
                }
            }
            
            $delete_member = members::get_by_id($delete_member_id);

            members::delete($delete_member_id);

            $notice = 'The member has been removed';

            $account = ActiveMemberInfo::GetAccount();
            $current_member = ActiveMemberInfo::GetMember();

            mail::send_template(
                        $from     = AUTO_EMAIL,
                        $to       = ADMIN_EMAIL,
                        $subject  = 'Corporation User Deleted ' ,
                        $template = 'admin-delete-user.php',
                        $vars     = get_defined_vars()
                    );
            
            http::redirect(BASEURL . '/members/account/hotels/members/list.php', array('notice' => $notice));
        }
    }
} else {
    $notice = 'The member could not be deleted';
    http::redirect(BASEURL . '/members/account/hotels/members/list.php', array('notice' => $notice));
}
?>