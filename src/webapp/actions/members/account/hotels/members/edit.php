<?php
include(ACTIONS . '/members/protect.php');
$db = mysqli_db::init();


if($_REQUEST['members_id'] == ActiveMemberInfo::GetMemberId()){
    http::redirect(BASEURL . '/members/account/account-details.php');
}

$members_table = new mysqli_db_table('members');
$member = $members_table->get($_REQUEST['members_id']);

// Should people that aren't admins be able to create users that don't have at least one location permission?
// We could create a dummy "Staff" permission that grants nothing just to tie a user to a location without giving them any permissions for that location.
// Then we could require any non-admin to have at least one location sort of permissions.

//make sure this is this corporation's or licensed_location's member
if(isset($_REQUEST['members_id']) && !empty($_REQUEST['members_id']) && !UserPermissions::UserCanAccessObject('members', $_REQUEST['members_id'])){
    http::redirect(BASEURL . '/members/account/hotels/members/list.php', array('notice' => 'You cannot edit this member'));
}

$member['members_email2'] = $member['members_email'];

$location_ids = UserPermissions::LocationIdsWithPermission('manageUsers');
$in_clause = "(" . implode(', ', $location_ids) . ")";
$locations_for_access = $db->fetch_all('
        SELECT * FROM licensed_locations
        WHERE licensed_locations_id in ' . $in_clause . '
        ORDER by licensed_locations_name'
        , array($licensed_locations_id));

$access_levels = $db->fetch_all('SELECT * FROM members_access_levels');
function accessSort($a, $b) {
    $order = array(
        'locationAdmin',
        'associated',
    );
    $aVal = array_search($a['members_access_levels_type'], $order);
    $bVal = array_search($b['members_access_levels_type'], $order);
    if($aVal === FALSE) {
        $aVal = $a['members_access_levels_sensitive'] == 0 ? 500 : 1000;
    }
    if($bVal === FALSE) {
        $bVal = $b['members_access_levels_sensitive'] == 0 ? 500 : 1000;
    }
    $diff = $aVal - $bVal;
    if($diff == 0) {
        $diff = strcmp($a['members_access_levels_name'], $b['members_access_levels_name']);
    }
    return $diff;
}
usort($access_levels, accessSort);
$access_levels_by_scope = array('admin' => array(), 'account' => array(), 'location' => array());
foreach($access_levels as $access) {
    if(UserPermissions::AccountCanAccessScreen($access['members_access_levels_type'],ActiveMemberInfo::GetAccountId())){
        if($access['members_access_levels_scope'] == 'account' && $access['members_access_levels_sensitive'] == 1){
            $access['members_access_levels_scope'] = 'sensitive';
        }
        $access_levels_by_scope[$access['members_access_levels_scope']][$access['members_access_levels_type']] = $access;
    }
}

$chunked_location_access_levels = $access_levels_by_scope['location'];
unset($chunked_location_access_levels['locationAdmin']);
unset($chunked_location_access_levels['associated']);
$chunked_location_access_levels = array_chunk($chunked_location_access_levels, ceil(count($chunked_location_access_levels)/2));

$current_member_access_levels = UserPermissions::AllPermissionTypesForUser(ActiveMemberInfo::GetMemberId());
$current_member_access = UserPermissions::AllPermissionsForUser(ActiveMemberInfo::GetMemberId(), $location_ids);
$edit_member_access = UserPermissions::AllPermissionsForUser($member['members_id'], $location_ids);

if($_POST){
    $data = $_POST;
    $errors = array();

    if(!UserPermissions::UserCanAccessScreen('manageAdmins')) {
        if($member['members_type']) {
            $data['members_type'] = $member['members_type'];
        } else {
            $data['members_type'] = 'user';
        }
    }

    $validate = new validate($data);
    $validate
        ->setOptional('members_id')
        ->unsetOptional('members_type')
        ->setTitles(array(
            'members_firstname' => 'First Name',
            'members_lastname' => 'Last Name',
            'members_title' => 'Position/Title',
            'members_email2' => 'Repeat Email',
            'members_password' => 'Password',
            'members_password2' => 'Repeat Password',
            'members_type' => 'User Type'
        ))
        ->setEmail('members_email', 'Email address')
        ->setUnique('members_email', 'members', $data['members_email'], $member['members_email'])
        ->setMatch('members_password', 'members_password2', 'Password')
        ->setMatch('members_email', 'members_email2', 'Email');
    
    if($data['members_id']){
        $validate->setOptional('members_password')
                 ->setOptional('members_password2');
    }

    $errors = $validate->test();

    //Don't necessarily need a permission to be set - no permissions still gets stuff like my files, etc.

    if(empty($errors)){
        $member_data = array(
            'members_firstname' => $data['members_firstname'],
            'members_lastname' => $data['members_lastname'],
            'members_title' => $data['members_title'],
            'members_email' => $data['members_email'],
            'members_type' => $data['members_type'],
            'join_accounts_id' => ActiveMemberInfo::GetAccountId());
        
        if(empty($data['members_id'])){
            //Create new
            $member_data['members_password'] = hash('sha256', $data['members_password']);

            $member_data['members_datetime'] = SQL('NOW()');
            
            //must have same status as parent
            $member_data['members_status'] = $db->fetch_singlet('select members_status from members where members_id = ?', array(ActiveMemberInfo::GetMemberId()));
            
            $members_table->insert($member_data);
            $members_id = $members_table->last_id();
            
            $data['join_members_id'] = $members_id;
            
            //add to corporate forum //task #3469
            $forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');
            $forums_groups_id = $db->fetch_singlet("SELECT groups_id
                                                    FROM forums_groups
                                                    WHERE groups_name = CONCAT_WS( '', (
                                                        SELECT c.corporations_name
                                                        FROM corporations AS c
                                                        WHERE join_accounts_id = ? ) , ' - Location'
                                                    )", array(ActiveMemberInfo::GetAccountId()));
            $forums_groups_x_members_table->insert(array(
                'join_groups_id' => $forums_groups_id,
                'join_members_id' => $members_id
            ));

            //add to message forum group
            $forums_groups_x_members_table->insert(array(
                'join_groups_id' => 6,
                'join_members_id' => $members_id
            ));

            // Set the notification table so the new user will get the
            // newsletter.
            $notifications_table = new mysqli_db_table('notifications');
            $notification_data = array(
                'join_members_id' => $members_id,
                'notifications_news_update' => "1",
                'notifications_forum_update' => "0",
                'notifications_blog_update' => "0",
                'notifications_additional_services' => "0",
                'notifications_webinars_update' => "0",
                'notifications_training_update' => "0"
            );
            $notification_data['join_members_id'] = $members_id;
            $notifications_table->insert($notification_data);
            $account = ActiveMemberInfo::GetAccount();
                mail::send_template(
                    $from     = AUTO_EMAIL,
                    $to       = ADMIN_EMAIL,
                    $subject  = 'New user added' ,
                    $template = 'new_member_added.php',
                    $vars     = get_defined_vars()
                );
            $notice = 'New user added';
            $access_members_id = $members_id;
            
            //sending welcome mails to new user
            
            //storingto member array to reuse email template
            $member['members_firstname'] = $data['members_firstname'];
            $member['members_lastname'] = $data['members_lastname'];
            
            mail::send_template(
                $from     = MEMBERSHIP_EMAIL,
                $to       = $member_data['members_email'],
                $subject  = 'Welcome to '.SITE_NAME,
                $template = 'full-membership-welcome-corporate.php',
                $vars     = get_defined_vars()
            );
            
        }else{
            //Update
            if($data['members_password'] && ($data['members_password'] == $data['members_password2']) ){
                $member_data['members_password'] = hash('sha256', $data['members_password']);
            }
            
            $members_table->update($member_data, $data['members_id']);
            
            if($member['members_firstname'] != $data['members_firstname']
                    || $member['members_lastname'] != $data['members_lastname']
                    || $member['members_email'] != $data['members_email'] )
            {
                $account = ActiveMemberInfo::GetAccount();
                mail::send_template(
                    $from     = AUTO_EMAIL,
                    $to       = ADMIN_EMAIL,
                    $subject  = 'Member Information Changed' ,
                    $template = 'member_updated.php',
                    $vars     = get_defined_vars()
                );
            }
            $notice = 'User updated';

			$access_members_id = $data['members_id'];
        }
        
	$db = mysqli_db::init();
        $members_access_table = new mysqli_db_table('members_access');
        $loc_members_access_table = new mysqli_db_table('licensed_locations_x_members_access');

        // Can't just wipe out all permissions - if the current user is missing permissions that the edited user has, the edited user should keep them.
        foreach($current_member_access['admin'] as $access_type => $access_id) {
            $db->query("DELETE FROM members_access WHERE join_members_id = ? AND join_members_access_levels_id = ?", array($access_members_id, $access_id));
            if($data['access'][$access_type] && $data['members_type'] == 'admin') {
                $members_access_table->insert(array('join_members_id' => $access_members_id, 'join_members_access_levels_id' => $access_id));
            }
        }
        foreach($current_member_access['sensitive'] as $access_type => $access_id) {
             $db->query("DELETE FROM members_access WHERE join_members_id = ? AND join_members_access_levels_id = ?", array($access_members_id, $access_id));
            if($data['sensitive'][$access_type]){
                $members_access_table->insert(array('join_members_id' => $access_members_id, 'join_members_access_levels_id' => $access_id));
            }
        }
        foreach($current_member_access['account'] as $access_type => $access_id) {
            $db->query("DELETE FROM members_access WHERE join_members_id = ? AND join_members_access_levels_id = ?", array($access_members_id, $access_id));
            if($data['access'][$access_type] && $data['members_type'] == 'user') {
                $members_access_table->insert(array('join_members_id' => $access_members_id, 'join_members_access_levels_id' => $access_id));
            }
        }

        foreach($current_member_access['location'] as $loc_id => $loc_access) {
            if($loc_access['locationAdmin']) {
                $loc_access = $access_levels_by_scope['location'];
            }
            foreach($loc_access as $access_type => $access_id) {
                if($access_id['members_access_levels_id']) {
                    $access_id = $access_id['members_access_levels_id'];
                }
                $db->query("DELETE FROM licensed_locations_x_members_access WHERE join_members_id = ? AND join_members_access_levels_id = ? AND join_licensed_locations_id = ?",
                    array($access_members_id, $access_id, $loc_id));
                if($data['loc_access'][$loc_id][$access_type] && $data['members_type'] == 'user') {
                    $loc_members_access_table->insert(
                        array('join_members_id' => $access_members_id,
                              'join_licensed_locations_id' => $loc_id,
                              'join_members_access_levels_id' => $access_id));
                }
            }
        }
		
        http::redirect(BASEURL . '/members/account/hotels/members/list.php', array('notice'  => $notice, 'licensed_locations_id'=>$licensed_locations_id));
    }else{
        //so user input is not lost
        $staff = array_merge((array)$staff, $data);
        $member = array_merge((array)$member, $data);
    }
}
?>