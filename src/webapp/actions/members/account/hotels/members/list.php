<?
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if (UserPermissions::UserCanAccessScreen('manageUsers')) {
    $location_ids = UserPermissions::LocationIdsWithPermission('manageUsers');
    $in_clause = "(" . implode(', ', $location_ids) . ")";

    if(ActiveMemberInfo::_IsAccountAdmin()) {
        $members = $db->fetch_all('
        SELECT SQL_CALC_FOUND_ROWS *,
        (SELECT GROUP_CONCAT(DISTINCT(licensed_locations_name) SEPARATOR \', \')
            FROM licensed_locations_x_members_access llxm
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE llxm.join_members_id = members_id
            AND licensed_locations_delete_datetime IS NULL
            ) as `licensed_locations_name`
        FROM members
        WHERE members_type = \'user\' AND join_accounts_id = ?
        ' . strings::orderby('members_lastname', array('licensed_locations_x_members_access', 'members')) . '
        ', array(ActiveMemberInfo::GetAccountId()));
    } else {
        $members = $db->fetch_all('
        SELECT SQL_CALC_FOUND_ROWS *,
        (SELECT GROUP_CONCAT(DISTINCT(licensed_locations_name) SEPARATOR \', \')
            FROM licensed_locations_x_members_access llxm
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE llxm.join_members_id = members_id
            AND licensed_locations_delete_datetime IS NULL
            AND licensed_locations_id in ' . $in_clause . '
            ) as `licensed_locations_name`,
        (SELECT count(1) FROM licensed_locations_x_members_access as s
             INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
             WHERE m.members_id = s.join_members_id
               AND licensed_locations_delete_datetime IS NULL
               AND s.join_licensed_locations_id not in ' . $in_clause . '
               ) as `non_managed`
        FROM members m
        WHERE members_type = \'user\'
            AND EXISTS
            (SELECT 1 FROM licensed_locations_x_members_access as s
             INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
             WHERE m.members_id = s.join_members_id
               AND licensed_locations_delete_datetime IS NULL
               AND s.join_licensed_locations_id in ' . $in_clause . '
               )
        ' . strings::orderby('members_lastname', array('licensed_locations_x_members_access', 'members')) . '
        ', array());

    }

    if (UserPermissions::UserCanAccessScreen('manageAdmins')) {
        $admins = $db->fetch_all('
        SELECT SQL_CALC_FOUND_ROWS *,
        (SELECT 1 FROM accounts where join_members_id = members_id) as `main_account_admin`
        FROM members
         WHERE members.members_type = \'admin\'
         AND members.join_accounts_id = ?
        ' . strings::orderby('members_lastname', array('members')) . '
        ', array(ActiveMemberInfo::GetAccountId()));
    }
} else {
    $notice = 'Sorry, you don\'t have control over these members.';
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

?>
