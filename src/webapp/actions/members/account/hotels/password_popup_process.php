<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$licensed_locations_table = new mysqli_db_table('licensed_locations');
$members_table = new mysqli_db_table('members');

$licensed_location = licensed_locations::get_by_id_with_details($_REQUEST['licensed_locations_id']);

$member = $members_table->get($licensed_location['join_members_id']);

if($_POST){
    $data = $_POST;
    $errors = array();
    
    if($_REQUEST['licensed_locations_id'])
    {
        $email_taken = $db->fetch_singlet('SELECT COUNT(*)
                                   FROM members AS m
                                   JOIN licensed_locations AS ll ON (m.members_id = ll.join_members_id)
                                   WHERE ll.licensed_locations_id != ?
                                   AND members_email = ?',array($_REQUEST['licensed_locations_id'],$data['members_email']));
        if($email_taken >0)
        {
            $errors['members_email'] = 'Email address is already in use';
        }

        if($data['members_email'] !== $data['members_email2'])
        {
            $errors['members_email'] = 'Emails do not match';
            $errors['members_email2'] = 'Emails do not match';
        }

        if($data['licensed_locations_pass'] !== $data['licensed_locations_pass2'])
        {
            $errors['licensed_locations_pass'] = 'Passwords do not match';
            $errors['licensed_locations_pass2'] = 'Passwords do not match';
        }
        else
        {
            $data['members_password'] = $data['licensed_locations_pass'];
        }
    }

    if(empty($errors))
    {
        //Update
        if($data['members_password'] !== $member['members_password'])
        {
            $data['members_password'] = hash('sha256', $data['members_password']);
        }

        $member['members_password'] = $data['members_password'];
        $member['members_email'] = $data['members_email'];
        $members_table->update($member, $licensed_location['join_members_id']);
        $notice = 'Location Updated';

        http::redirect(BASEURL . '/members/account/hotels/list.php', array('notice'  => $notice));
    }
    
}
?>
    