<?php
include_once ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$bills_table = new mysqli_db_table('bills');
$params = $_GET;

if($_REQUEST['licensed_locations_id']){
    //check if licensed_location belongs to this corporation
    $licensed_location = $db->fetch_one('
        SELECT *
        FROM licensed_locations
        WHERE licensed_locations_id = ? AND join_accounts_id = ?'
        ,
        array($_REQUEST['licensed_locations_id'], ActiveMemberInfo::GetAccountId())
    );
    
    if(ActiveMemberInfo::_IsAccountAdmin() && $licensed_location && $licensed_location['licensed_locations_delete_datetime']){
        //If the previous yearly fee would have included this licensed_location, then charge for it now
        $last_bill_date = $db->fetch_singlet('select bills_datetime from bills b inner join accounts a on b.join_members_id = a.join_members_id where accounts_id = ? and bills_type = "yearly" order by bills_datetime desc limit 1', array(ActiveMemberInfo::GetAccountId()));
        if(strtotime($licensed_location['licensed_locations_delete_datetime']) < strtotime($last_bill_date)){
            $bill_description = 'Renewed location: '.$licensed_location['licensed_locations_name'].' ('.date(DATE_FORMAT, strtotime($last_bill_date)).' - '.date(DATE_FORMAT, strtotime($last_bill_date.' + 1 year')).')';
            $account_admin = ActiveMemberInfo::GetAccountAdmin();

            list($approved, $transaction_error) = members::process_payment($account_admin['members_id'], YEARLY_COST, $bill_description);
            
            if($approved){
                //Create bill
                $bills_table->insert(array(
                    'join_members_id' => $account_admin['members_id'],
                    'bills_amount' => YEARLY_COST,
                    'bills_type' => 'location',
                    'bills_description' => $bill_description,
                    'bills_datetime' => SQL('NOW()'),
                    'bills_paid' => 1
                ));
                
                $db->query('UPDATE licensed_locations SET licensed_locations_delete_datetime = NULL WHERE licensed_locations_id = ?', array($_REQUEST['licensed_locations_id']));
                $params['notice'] = 'Location was renewed.';
            }else{
                $params['error'] = 'Your credit card could not be processed';
            }
        }else{
            $db->query('UPDATE licensed_locations SET licensed_locations_delete_datetime = NULL WHERE licensed_locations_id = ?', array($_REQUEST['licensed_locations_id']));
            $params['notice'] = 'Location was renewed.';
        }
    }else{
        $params['error'] = 'You are not allowed to renew this location';
    }
    
    unset($params['licensed_locations_id']);

    http::redirect(BASEURL . '/members/account/hotels/list.php', $params);
}


?>