<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$notifications_table = new mysqli_db_table('notifications');
$notifications = $db->fetch_one('select * from notifications where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

if($_POST){
    $data = $_POST;

    if($notifications['notifications_id']) {
        unset($data['join_members_id']);    //just in case we got a hacker
        $notifications_table->update($data, $notifications['notifications_id']);
    }
    else {
        $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
        $notifications_table->insert($data);
    }

    $notice = 'Notification Settings Updated';

    http::redirect(BASEURL . '/members/account/notifications.php', array('notice' => $notice));
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_account FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

?>