<?php


if(preg_match('/^[a-z\_]{1,150}$/',$_REQUEST['type']) && ($_REQUEST['yn']=== 'true' || $_REQUEST['yn'] === 'false')){

    $db = mysqli_db::init();
    $members_settings = new mysqli_db_table('members_settings');
    $new_setting['join_members_id'] = ActiveMemberInfo::GetMemberId();

    // check if string provided in type variable exists in this table
    $field_exists = in_array($_REQUEST['type'],$db->get_fields('members_settings'));
    
    if($field_exists){
        if ($_REQUEST['yn']== 'false'){
            $return = 'off';
            
            //check if row of notifications exists for this member
            $setting_exists = $db->fetch_singlet('SELECT members_settings_id FROM members_settings WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            if($setting_exists){
                $db->query('UPDATE members_settings SET '.$_REQUEST['type'].' = 0 WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            }
            else {
                //add new row
                $new_setting[$_REQUEST['type']] = 0;
                $members_settings->insert($new_setting);
            }
        }
        else {
            $return = 'on';
            
             //check if row of notifications exists for this member
            $setting_exists = $db->fetch_singlet('SELECT members_settings_id FROM members_settings WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            if($setting_exists){
                $db->query('UPDATE members_settings SET '.$_REQUEST['type'].' = 1 WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            }
            else {
                //add new row
                $new_setting[$_REQUEST['type']] = 1;
                $members_settings->insert($new_setting);
            }
        }
        echo $return;
    }
    else {
        echo 'error: please do not modify url!';
    }

}
else {
    echo 'error';
}

?>