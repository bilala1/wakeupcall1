<?php


if(preg_match('/^[a-z\_]{1,150}$/',$_REQUEST['type']) && ($_REQUEST['yn']=== 'true' || $_REQUEST['yn'] === 'false')){

    $db = mysqli_db::init();
    $notifications_table = new mysqli_db_table('notifications');
    $new_notification['join_members_id'] = ActiveMemberInfo::GetMemberId();

    // check if string provided in type variable exists in this table
    $field_exists = in_array($_REQUEST['type'],$db->get_fields('notifications'));
    
    if($field_exists){
        if ($_REQUEST['yn']== 'false'){
            $return = 'off';
            
            //check if row of notifications exists for this member
            $setting_exists = $db->fetch_singlet('SELECT notifications_id FROM notifications WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            if($setting_exists){
                $db->query('UPDATE notifications SET '.$_REQUEST['type'].' = 0 WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            }
            else {
                //add new row
                $new_notification[$_REQUEST['type']] = 0;
                $notifications_table->insert($new_notification);
            }
        }
        else {
            $return = 'on';
            
             //check if row of notifications exists for this member
            $setting_exists = $db->fetch_singlet('SELECT notifications_id FROM notifications WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            if($setting_exists){
                $db->query('UPDATE notifications SET '.$_REQUEST['type'].' = 1 WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
            }
            else {
                //add new row
                $new_notification[$_REQUEST['type']] = 1;
                $notifications_table->insert($new_notification);
            }
        }
    }
    else {
        echo 'error: please do not modify url!';
    }

}
else {
    echo 'error';
}

?>