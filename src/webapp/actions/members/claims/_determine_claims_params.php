<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/23/15
 * Time: 11:46 AM
 */

if($_REQUEST['clear_filter'] == 'clear_filter'){
    $_REQUEST['join_licensed_locations_id']= 0;
    $_REQUEST['claims_start_time'] ='';
    $_REQUEST['claims_end_time']='';
    $_REQUEST['join_claims_status_codes_id']='';
    $startTime = '';
    $endTime = '';
    unset($_SESSION['claims_start_time']);
    unset($_SESSION['claims_end_time']);
    unset($_SESSION['join_claims_status_codes_id']);
    unset($_SESSION['claims_list_licensed_locations_id']);
}
if($_REQUEST['quick_filter'] && $_REQUEST['quick_filter']!='' ){
    $days = $_REQUEST['quick_filter'];
    $_REQUEST['claims_start_time'] = date('m/d/Y',  strtotime("-$days days"));
    $_REQUEST['claims_end_time'] = date('m/d/Y');;
}
if ( array_key_exists('join_claims_status_codes_id',$_REQUEST) ) {
    $_SESSION['join_claims_status_codes_id'] = $_REQUEST['join_claims_status_codes_id'];
}
if ( array_key_exists('join_claims_status_codes_id',$_SESSION) ) {
    $_REQUEST['join_claims_status_codes_id'] = $_SESSION['join_claims_status_codes_id'];
}
$join_claims_status_codes_id = $_REQUEST['join_claims_status_codes_id']; 
$startTime = array_key_exists('claims_start_time',$_REQUEST) && strtotime($_REQUEST['claims_start_time']) != false ? $_REQUEST['claims_start_time'] :
    ( array_key_exists('claims_start_time',$_SESSION) && strtotime($_SESSION['claims_start_time']) != false ? $_SESSION['claims_start_time'] : date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 year')));

// if there was a request start time, save it in the session for next time
if ( array_key_exists('claims_start_time',$_REQUEST) && strtotime($_REQUEST['claims_start_time']) ) {
    $_SESSION['claims_start_time'] = $_REQUEST['claims_start_time'];
}

$endTime = array_key_exists('claims_end_time',$_REQUEST) && strtotime($_REQUEST['claims_end_time']) ? $_REQUEST['claims_end_time'] :
    ( array_key_exists('claims_end_time',$_SESSION) && strtotime($_SESSION['claims_end_time']) ? $_SESSION['claims_end_time'] : date('n/j/Y', time()));

// if there was a request end time, save it in the session for next time
if ( array_key_exists('claims_end_time',$_REQUEST) ) {
    $_SESSION['claims_end_time'] = $_REQUEST['claims_end_time'];
}

$claims_show_hidden = array_key_exists('claims_show_hidden',$_REQUEST) ? $_REQUEST['claims_show_hidden'] == "1" :
    ($_SESSION['claims_show_hidden'] ? $_SESSION['claims_show_hidden'] == "1" : false );

if ( array_key_exists('claims_show_hidden',$_REQUEST) ) {
    $_SESSION['claims_show_hidden'] = $_REQUEST['claims_show_hidden'];
}

if(array_key_exists('join_licensed_locations_id',$_REQUEST))
{
    $filter_licensed_locations_id = $_REQUEST['join_licensed_locations_id'];
    $_SESSION['claims_list_licensed_locations_id'] = $_REQUEST['join_licensed_locations_id'];
    
}
else if(array_key_exists('claims_list_licensed_locations_id', $_SESSION))
{
     $filter_licensed_locations_id = $_SESSION['claims_list_licensed_locations_id'];
}

if(array_key_exists('order',$_REQUEST))
{
    $order = $_REQUEST['order'];
    $_SESSION['claims_list_order'] = $_REQUEST['order'];
    
}
else if(array_key_exists('claims_list_order', $_SESSION))
{
     $order = $_SESSION['claims_list_order'];
}

// for this display it's either show visible or show hidden but not both
$claims_show_visible = !$claims_show_hidden;