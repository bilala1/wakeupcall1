<?php
/**
 * Created by PhpStorm.
 * User: john campbell
 * Date: 6/2/15
 * Time: 6:23 AM
 * @param $carriers_id
 * @param $claims_id
 * @param bool|true $primary_carrier_submit true if this is the carrier associated with the claim...
 * @param array $submit_file_ids
 * @returns Boolean - true for success, false for failure.
 * @throws Exception
 * @throws Zend_Mail_Exception
 */
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/ClaimsEmail.php');
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/TmpEmailFileHandler.php');

function submitClaimToCarrierEmail($carriers_id, $claims_id, $primary_carrier_submit, $files, $subject, $submit_to_adjuster)
{
    if (!isset($carriers_id)) {
        throw new Exception("Carrier ID cannot be null");
    }
    if (!isset($claims_id)) {
        throw new Exception("Claim ID cannot be null");
    }

    $db = mysqli_db::init();

    $claim = $db->fetch_one("SELECT * from claims c
                            left join claim_auto as ca on ca.join_claims_id = claims_id
                            left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
                            left join claim_property as cp on cp.join_claims_id = claims_id
                            left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
                            left join claim_employment as ce on ce.join_claims_id = claims_id
                            where claims_id = ?", array($claims_id));

    $member = $db->fetch_one('SELECT members_firstname,members_lastname,members_email FROM members WHERE members_id = ?',
        array(ActiveMemberInfo::GetMemberId()));
    $carrier = $db->fetch_one('SELECT * FROM carriers WHERE carriers_id = ?', array($carriers_id));

    // get the license location name for the claim
    $claim_location = $db->fetch_singlet('SELECT ll.licensed_locations_name FROM claims c JOIN licensed_locations ll ON c.join_licensed_locations_id = ll.licensed_locations_id  WHERE claims_id = ?',
        array($claims_id));

    if (!isset($claim_location)) {
        throw new Exception("Claim must have a license location.");
    }

    $from = $member['members_email'];
    $to = $carrier['carriers_email'];
    $carrier_name = $carrier['carriers_name'];
    if($submit_to_adjuster) {
       $claim_x_carrier = $db->fetch_one('SELECT * FROM claims_x_carriers WHERE join_carriers_id = ? and join_claims_id = ?', array($carriers_id,$claims_id));
       $to = $claim_x_carrier['claims_x_carriers_adjusters_email'];
       $carrier_name = $carrier['carriers_name'] = $claim_x_carrier['claims_x_carriers_adjusters_name'];
    }
    if (!$subject) {
        $subject = 'Notice of Claim for ' . $claim_location;
    }
    $date_of_incident = date('m-d-Y', strtotime($claim['claims_datetime'])) ;
    $date_notified = date('m-d-Y', strtotime($claim['claims_datetime_notified'])) ;
    $brief_description = $claim['claims_description'];
    $claimant = '';
    if($claim['claim_employment_claimant']) {
        $claimant = $claim['claim_employment_claimant'];
    } elseif($claim['claim_workers_comp_employee_name']) {
        $claimant = $claim['claim_workers_comp_employee_name'];
    } elseif($claim['claim_general_liab_claimant']) {
        $claimant = $claim['claim_general_liab_claimant'];
    } elseif($claim['claim_auto_claimant']) {
        $claimant = $claim['claim_auto_claimant'];
    }

    $claim2carrierModel = array(
        "todays_date" => date('m-d-Y'),
        "carriers_name" => $carrier_name,
        "claim_location" => $claim_location,
        "carriers_policy_number" => $carrier['carriers_policy_number'],
        "members_firstname" => $member['members_firstname'],
        "members_lastname" => $member['members_lastname'],
        "isRecipient" => $carrier['carriers_recipient'],
        "claimant" => $claimant,
        "date_of_incident" => $date_of_incident,
        "date_notified" => $date_notified,
        "brief_description" => $brief_description
    );

    $message = null;

    $template = claims_email_templates::get_template_for_claim($claims_id);
    $default_template = claims_email_templates::getDefaultTemplate();
    if ($template) {
        $html = stripslashes($template['email_html']);
    }
    if(!$html && $default_template) {
        $html = stripslashes($default_template['email_html']);
    }
    if (!$html) {
        $html = emails_default_templates::get_claims_email_template();
    }

    if ($html != null) {
        $dynemail = new ClaimsRequestDynamicEmail();
        $message = $dynemail->generate_html($html, $claim2carrierModel);
    }

    $additional_header_data = array(
        "event_id" => 'claims-submit',
        "carrier_id" => $carriers_id,
        "carrier_name" => $claim2carrierModel['carriers_name'],
        "primary_carrier_submit" => $primary_carrier_submit
    );

    sendClaimEmail($claims_id, $to, $from, $subject, $message, $files, $additional_header_data);
    claims_history::claim_submitted_entry(ActiveMemberInfo::GetMemberId(), $claim, $primary_carrier_submit ? $carrier : null, $primary_carrier_submit ? null : $carrier, $files);
}

function sendClaimEmail($claims_id, $to, $from, $subject, $message, $files, $additional_header_data) {
    $db = mysqli_db::init();

    $member = $db->fetch_one('SELECT members_firstname,members_lastname,members_email FROM members WHERE members_id = ?',
        array(ActiveMemberInfo::GetMemberId()));

    $claim_location = $db->fetch_singlet('SELECT ll.licensed_locations_id FROM claims c JOIN licensed_locations ll ON c.join_licensed_locations_id = ll.licensed_locations_id  WHERE claims_id = ?',
        array($claims_id));

    $files_sent = '';
    $filename = array();
    foreach ((array)$files as $file) {
        if (empty($file['files_name'])) {
            continue;
        }
        $filename[] = $file['files_name'];
       $submitted_date = date('Y-m-d H:i:s');
       $claims_files_id = $file['claims_files_id'];
       $db->query("UPDATE claims_files set claims_files_submitted = '$submitted_date' where claims_files_id = ?",array($claims_files_id)); 
    }
    if(!empty($filename)){
        $files_sent = implode(", ", $filename);
    }

    $sent_emails_history_id  = emails_history::add(ActiveMemberInfo::GetAccountId(), $claim_location, $subject,
        $message, $claims_id, null, $files_sent, $to, 'email', 'pending');

    $additional_header_data = array_merge(
        $additional_header_data,
        array(
            "claim_id" => $claims_id,
            "members_firstname" => $member['members_firstname'],
            "members_email" => $member['members_email'],
            "members_id" => $member['members_id'],
            "sent_emails_history_id" => $sent_emails_history_id
        )
    );

    sendEmail($message, $subject, $to, $from, $files, $additional_header_data);
}


function sendEmail($message, $subject, $to, $from, $files, $additional_header_data){
    $paths = array(
            realpath(SITE_PATH . '/library'),
            '.'
    );
    set_include_path(implode(PATH_SEPARATOR, $paths));

    //-- Load Zend Mail
    require_once 'Zend/Mail.php';
    require_once 'Zend/Mail/Transport/Smtp.php';

    $config = array(
        'ssl' => 'tls',
        'port' => '587',
        'auth' => 'login',
        'username' => SMTP_USERNAME,
        'password' => SMTP_PASSWORD
    );
    $transport = new Zend_Mail_Transport_Smtp(SMTP_SERVER, $config);

    $mail = new Zend_Mail();
    $mail->setBodyHtml($message);
    $mail->setSubject($subject);
    $mail->addTo($to);
    $mail->setFrom($from);
    $mail->addHeader('X-Mailgun-Variables', json_encode($additional_header_data));

    foreach ((array)$files as $file) {
        if (empty($file['files_name'])) {
            continue;
        }

        if (!isset($file['files_contents'])) {
            $file['files_contents'] = \Services\Services::$fileSystem->getFileContents($file);
        }

        $attachment = $mail->createAttachment($file['files_contents']);

        $attachment->type = files::get_mime_type_by_name($file['files_name']);
        $attachment->disposition = Zend_Mime::DISPOSITION_INLINE;
        $attachment->encoding = Zend_Mime::ENCODING_BASE64;
        $attachment->filename = $file['files_name'];
    }

    if (!DISABLE_EMAIL) {
        if ($mail->send($transport)) {
            return true;
        } else {
            error_log('There was a problem emailing a copy of the claim.');
            return false;
        }
    } else {
        return true;
    }
}
