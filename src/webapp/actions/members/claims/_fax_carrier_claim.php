<?php

use Services\Services;

function sendFax($faxNumber, $contactName, $coverLetter, $files)
{
    /**************** Settings begin **************/
    $username = 'wakeupcall';  // Insert your InterFAX username here
    $password = 'wakeup01';  // Insert your InterFAX password here

    $postponetime = '2001-12-31T00:00:00-00:00';
    $retries = '1';

    $csid = 'INTERFAX';

    $pageheader = 'To: {To} From: {From} Pages: {TotalPages}';
    $subject = 'Claim Submission';
    $replyemail = FAX_EMAIL;
    $page_size = 'A4';
    $page_orientation = 'Portrait';
    $high_resolution = false;
    $fine_rendering = true;

    /**************** Settings end ****************/
    $filetypes = 'html;';
    $data = $coverLetter;
    $filesizes = strlen($coverLetter) . ';';

    foreach ($files as $file) {
        $filetype = files::get_extension($file['files_name']);
        if (!isset($file['files_contents'])) {
            $file['files_contents'] = Services::$fileSystem->getFileContents($file);
        }
        $filedata = $file['files_contents'];
        $filesize = strlen($filedata);

        $filetypes .= $filetype . ';';
        $filesizes .= $filesize . ';';
        $data .= $filedata;
    }
    $filetypes = trim($filetypes, ';');
    $filesizes = trim($filesizes, ';');

// Call WS method
    $client = new SoapClient("http://ws.interfax.net/dfs.asmx?wsdl");
// Use this version of SoapClient instantiation if tracing the SOAP call:
#$client = new SoapClient("http://ws.interfax.net/dfs.asmx?wsdl", array('trace' => 1));

    $params->Username = $username;
    $params->Password = $password;
    $params->FaxNumbers = $faxNumber;
    $params->Contacts = $contactName;
    $params->FilesData = $data;
    $params->FileTypes = $filetypes;
    $params->FileSizes = $filesizes;
    $params->Postpone = $postponetime;
    $params->RetriesToPerform = $retries;
    $params->CSID = $csid;
    $params->PageHeader = $pageheader;
    $params->JobID = '';
    $params->Subject = $subject;
    $params->ReplyAddress = $replyemail;
    $params->PageSize = $page_size;
    $params->PageOrientation = $page_orientation;
    $params->IsHighResolution = $high_resolution;
    $params->IsFineRendering = $fine_rendering;

    $result = $client->SendfaxEx_2($params);
    $fax_submission_id = $result->SendfaxEx_2Result;

//echo "REQUEST:\n" . $client->__getLastRequest() . "\n";exit;

    if ($fax_submission_id < 0) {
        $result = array(
            'notice' => 'Failed to send fax.',
            'success' => false,
            'submissionId' => $fax_submission_id
        );
    } else {
        $result = array(
            'notice' => 'Fax submission to carrier is pending. An email will be sent when the submission succeeds.',
            'success' => true,
            'submissionId' => $fax_submission_id
        );
    }
    return $result;
}

function submitClaimViaFax($claim, $recipientId, $files)
{
    $db = mysqli_db::init();
    $carrier = $db->fetch_one('SELECT * FROM carriers WHERE carriers_id = ?', array($recipientId));
    $member = ActiveMemberInfo::GetMember();

    $template = claims_email_templates::get_template_for_claim($claim['claims_id']);
    if ($template) {
        $html = stripslashes($template['email_html']);
    } else {
        $default_template = claims_email_templates::getDefaultTemplate();
        if ($default_template) {
            $html = stripslashes($default_template['email_html']);
        } else {
            $html = emails_default_templates::get_claims_email_template();
        }
    }

    // get the license location name for the claim
    $claim_location = $db->fetch_singlet('SELECT ll.licensed_locations_name FROM claims c JOIN licensed_locations ll ON c.join_licensed_locations_id = ll.licensed_locations_id  WHERE claims_id = ?',
        array($claim['claims_id']));

    if (!isset($claim_location)) {
        throw new Exception("Claim must have a license location.");
    }
    
    $date_of_incident = date(DATE_FORMAT_FULL, strtotime($claim['claims_datetime'])) ;
    $date_notified = date(DATE_FORMAT_FULL, strtotime($claim['claims_datetime_notified'])) ;
    $brief_description = $claim['claims_description'];
    $claimant = '';
    if($claim['claim_employment_claimant'])
        $claimant = $claim['claim_employment_claimant'];
    elseif($claim['claim_workers_comp_employee_name'])
        $claimant = $claim['claim_workers_comp_employee_name'];
    elseif($claim['claim_general_liab_claimant'])
        $claimant = $claim['claim_general_liab_claimant'];
    elseif($claim['claim_auto_claimant'])
        $claimant = $claim['claim_auto_claimant'];
    
    $claim2carrierModel = array(
        "todays_date" => date('m-d-Y'),
        "carriers_name" => $carrier['carriers_name'],
        "claim_location" => $claim_location,
        "carriers_policy_number" => $carrier['carriers_policy_number'],
        "members_firstname" => $member['members_firstname'],
        "members_lastname" => $member['members_lastname'],
        "isRecipient" => $carrier['carriers_recipient'],
        "claimant" => $claimant,
        "date_of_incident" => $date_of_incident,
        "date_notified" => $date_notified,
        "brief_description" => $brief_description
    ); 

    $dynemail = new ClaimsRequestDynamicEmail();
    $cover_letter = $dynemail->generate_html($html, $claim2carrierModel);

    $faxNumber = $carrier['carriers_fax'];
    $faxNumber = str_replace(' ', '',
        str_replace('-', '', str_replace('+', '', str_replace('(', '', str_replace(')', '', $faxNumber)))));

    if (!is_numeric($faxNumber)) {
        $faxResult = array(
            'success' => false,
            'notice' => 'The Carrier\'s fax is invalid.'
        );
    } else {
        if (sizeof($faxNumber == 10)) {
            $faxNumber = '+1' . $faxNumber;
        }
        $faxResult = sendFax($faxNumber, $carrier['carriers_name'], $cover_letter, $files);
    }
    
    $filename = array();
    foreach ($files as $file) {
        $filename[] = $file['files_name'];
    }
    $files_sent = '';
    if(!empty($filename)){
        $files_sent = implode(", ", $filename);
    }

    $status = $faxResult['success'] ? 'pending' : 'fail';

    emails_history::add(ActiveMemberInfo::GetAccountId(), $claim['join_licensed_locations_id'], null,
        $cover_letter, $claim['claims_id'], null,$files_sent,$faxNumber,'fax',$status,'Error code: ' . $faxResult['submissionId'], null);

    if ($faxResult['success']) {
        //record that the claim was sent (filed)
        $claims = new mysqli_db_table('claims');
        $claims_faxes = new mysqli_db_table('claims_faxes');

        $claims_update_array = array();

        $claims_faxes_array = array(
            'join_claims_id' => $claim['claims_id'],
            'claims_faxes_submission_id' => $faxResult['submissionId'],
            'claims_faxes_submitted_datetime' => date('Y-m-d H:i:s'),
            'claims_faxes_submitted_by' => ActiveMemberInfo::GetMemberId(),
            'claims_faxes_status' => -1
        );

        $claims_faxes->insert($claims_faxes_array);
        $claims_faxes_id = $claims_faxes->last_id();

        foreach ($files as $f) {
            $claims_faxes_files->insert(array(
                'join_claims_faxes_id' => $claims_faxes_id,
                'join_claims_files_id' => $f['claims_files_id']
            ));
        }

        if ($claim['join_claims_status_codes_id'] != Claims::$SUBMIT_OPEN) {
            claims::setStatusCodeAndDescription($claims_update_array, Claims::$SUBMIT_OPEN);
            claims_history::claim_status_updated_entry($member['members_id'], $claims_update_array, $claim);
        }
        if (empty($claim['claims_datetime_submitted'])) {
            $claims_update_array['claims_datetime_submitted'] = date('Y-m-d');
            claims_history::claim_submitted_date_updated_entry($member['members_id'], $claim);
        }
        $claims->update($claims_update_array, $claim['claims_id']);

        claims_history::claim_submitted_entry(ActiveMemberInfo::GetMemberId(), $claim, $carrier, null, $files);
        members::log_action(ActiveMemberInfo::GetMemberId(), "Submitted a claim");
    }
}