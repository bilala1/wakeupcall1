<?php
$carriers_show_hidden = 0;
if ($_REQUEST['clear']) {
    $_REQUEST['join_licensed_locations_id'] = array();
    $_REQUEST['carriers_start_time'] = '';
    $_REQUEST['carriers_end_time'] = '';
    $_REQUEST['carriers_show_hidden'] = '0';
    $_SESSION['carriers_end_time']='';
    $_SESSION['carriers_start_time'] ='';
    $_SESSION['carriers_filter_licensed_locations_id'] ='';
}
if(array_key_exists('carriers_show_hidden', $_REQUEST))
{
    $_SESSION['carriers_show_hidden'] = $_REQUEST['carriers_show_hidden'];
    $carriers_show_hidden = $_REQUEST['carriers_show_hidden'];
}

else if(array_key_exists('carriers_show_hidden', $_SESSION))
{
    $carriers_show_hidden = $_SESSION['carriers_show_hidden'];
}

if(array_key_exists('join_licensed_locations_id', $_REQUEST))
{
    $filter_location_id = $_REQUEST['join_licensed_locations_id'];
    if(in_array('all', $filter_location_id)) {
        $filter_licensed_locations_id = null;
    }
    $_SESSION['carriers_filter_licensed_locations_id'] = $_REQUEST['join_licensed_locations_id'];
}
else if(array_key_exists('carriers_filter_licensed_locations_id', $_SESSION))
{
    $filter_location_id = $_SESSION['carriers_filter_licensed_locations_id'];
}

$carriers_start_time = array_key_exists('carriers_start_time',$_REQUEST) && strtotime($_REQUEST['carriers_start_time']) != false ? $_REQUEST['carriers_start_time'] :
    ( array_key_exists('carriers_start_time',$_SESSION) && strtotime($_SESSION['carriers_start_time']) != false ? $_SESSION['carriers_start_time'] : date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 year')));

// if there was a request start time, save it in the session for next time
if ( array_key_exists('carriers_start_time',$_REQUEST) && strtotime($_REQUEST['carriers_start_time']) ) {
    $_SESSION['carriers_start_time'] = $_REQUEST['carriers_start_time'];
}

$carriers_end_time = array_key_exists('carriers_end_time',$_REQUEST) && strtotime($_REQUEST['carriers_end_time']) ? $_REQUEST['carriers_end_time'] :
    ( array_key_exists('carriers_end_time',$_SESSION) && strtotime($_SESSION['carriers_end_time']) ? $_SESSION['carriers_end_time'] : date('n/j/Y', time()));

// if there was a request end time, save it in the session for next time
if ( array_key_exists('carriers_end_time',$_REQUEST) ) {
    $_SESSION['carriers_end_time'] = $_REQUEST['carriers_end_time'];
}

if(array_key_exists('order',$_REQUEST))
{
    $order = $_REQUEST['order'];
    $_SESSION['carriers_list_order'] = $_REQUEST['order'];

}
else if(array_key_exists('carriers_list_order', $_SESSION))
{
    $order = $_SESSION['carriers_list_order'];
}
