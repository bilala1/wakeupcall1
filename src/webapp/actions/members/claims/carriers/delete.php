<?
include ACTIONS . '/members/protect.php';

$permitted = carriers::get_carrier($_REQUEST['carriers_id']);

if($permitted > 0 )
{
    carriers::delete($_REQUEST['carriers_id']);
    if($permitted['carriers_recipient'] == 1)
        $notice = 'The recipient has been deleted';
    else
        $notice = 'The carrier has been deleted';
}
else
{
    $notice = 'You do not have permissions to delete this carrier.';
}

http::redirect(BASEURL . '/members/claims/carriers/index.php', array('notice' => $notice));

?>