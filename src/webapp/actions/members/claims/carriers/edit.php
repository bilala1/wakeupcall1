<?php
include(ACTIONS . '/members/protect.php');

$location = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
$licensed_locations_for_member = $location;

$carrier_locations = carriers::get_carrier_locations_ids($_REQUEST['carriers_id']);


$email_recipient_locations = carriers::get_linked_email_recipient_locations_ids($_REQUEST['carriers_id']);
//$all_location_checked = count($location) == count($email_recipient_locations);

$always_email_locations = carriers::get_always_email_recipient_locations_ids($_REQUEST['carriers_id']);
$always_email_on_save_locations = carriers::get_always_email_recipient_onsave_locations_ids($_REQUEST['carriers_id']);
$always_email_onsubmit_locations = carriers::get_always_email_recipient_onsubmission_locations_ids($_REQUEST['carriers_id']);
//$all_email_locations_checked = count($location) == count($always_email_locations);
$recipient_edit_locations = carriers::get_edit_email_recipient_locations_ids($_REQUEST['carriers_id']);

$carrier_edit_locations = carriers::get_edit_carrier_locations_ids($_REQUEST['carriers_id']);
$carrier = carriers::get_carrier($_REQUEST['carriers_id']);

// did they use the all checkboxes
$carriers_all_locations_checked = $carrier['carriers_all_locations'];
$carriers_all_receive_checked = $carrier['carriers_all_receive'];
$carriers_all_receive_onsave_checked = $carrier['carriers_notify_on_save_all'];
$carriers_all_receive_onsubmission_checked = $carrier['carriers_notify_on_submission_all'];


if($carrier) {
    $perms = UserPermissions::UserObjectPermissions("carriers",$_REQUEST['carriers_id']);
}

$can_assign_locations = $can_insert_update_all = !$carrier || $perms['edit'];

$location_ids = UserPermissions::LocationIdsWithPermission('claims');
$locations = array();
foreach($location_ids as $location_id) {
    $locations[] = licensed_locations::get_by_id($location_id);
}
$isRenewal = array_key_exists('renew', $_REQUEST);
if($isRenewal) {
    $carrier['carriers_effective_start_date'] = date('n/j/Y', strtotime($carrier['carriers_effective_end_date'].' + 1 day'));
    $carrier['carriers_effective_end_date'] = '';
}

$errors = array();

if(!$_POST && $carrier && $carrier['carriers_recipient'] == 0 && !$isRenewal) {
    if(date('n/j/Y', strtotime($carrier['carriers_effective_end_date'])) == '1/1/1972')  {
        $carrier['carriers_effective_end_date'] = '';
        $errors['carriers_effective_end_date'] = 'Please enter date';
    }
    if(date('n/j/Y', strtotime($carrier['carriers_effective_start_date'])) == '1/1/1972')  {
        $carrier['carriers_effective_start_date'] = '';
        $errors['carriers_effective_start_date'] = 'Please enter date';
    }
}

if($_POST)
{
    $updateType = $_REQUEST['carriers_recipient'] == 0 ? "Carrier" : "Recipient";
    $data = $_POST;
    unset($data['carriers_id']);
    
    $validate = new validate($data);

    if($_REQUEST['carriers_no_email'] && $_REQUEST['carriers_recipient'] != 1)
    {
        $validate->setOptional('carriers_email');
    }
    else
    {
        $validate->setEmail('carriers_email');
    }
    $validate->setOptional('renew');
    $validate->setOptional('carriers_id');
    $validate->setOptional('carriers_contact');
    $validate->setOptional('carriers_coverages_other');
    $validate->setOptional('carriers_address');
    $validate->setOptional('carriers_phone');
    $validate->addUserPermissionValidation($data, 'checkbox_all_locations','carriers_all_locations', $carrier);
    $validate->addUserPermissionValidation($data, 'checkbox_receive_all','carriers_all_receive', $carrier);
    $validate->addUserPermissionValidation($data, 'checkbox_receive_all_save','carriers_notify_on_save_all', $carrier);
    $validate->addUserPermissionValidation($data, 'checkbox_receive_all_submission','carriers_notify_on_submission_all', $carrier);
    
    if( $_REQUEST['carriers_recipient'] != 1)
    {
        $validate->setDate('carriers_effective_start_date');
        $validate->setDate('carriers_effective_end_date');
        $validate->setText('coverages');
        if($data['carriers_phone'] != '')
        {
            $validate->setPhoneNum('carriers_phone','Phone Number');
        }
        $validate->setPhoneNum('carriers_fax','Fax Number');

        if(isset($data['coverages']))
        {
            $data['carriers_coverages_list'] = implode(',', $data['coverages']);
        }
    }
    else
    {
        $validate->setOptional('carriers_policy_number');
        $validate->setOptional('carriers_fax');
        $validate->setOptional('carriers_effective_start_date');
        $validate->setOptional('carriers_effective_end_date');
        $validate->setOptional('coverages');
    }
        
    
    $errors = $validate->test();

    if(!$errors)
    {
        $carriers = new mysqli_db_table('carriers');
        
        $data['join_accounts_id'] = $_SESSION['accounts_id'];
       
        if($carrier && !$isRenewal)
        {
            $old_carrier = carriers::get_carrier_all_details($carrier['carriers_id']);
            $data['carriers_all_locations'] = $data['checkbox_all_locations'] == "on";
            $data['carriers_all_receive'] = $data['checkbox_receive_all'] == "on";
            $data['carriers_notify_on_submission_all'] = $data['checkbox_receive_all_submission'] == "on";
            $data['carriers_notify_on_save_all'] = $data['checkbox_receive_all_save'] == "on";
            $carriers_id = carriers::update($carrier, $data);
            $notice = $updateType.' updated!';
            
        } else {
            $carriers_id = carriers::add_carrier(ActiveMemberInfo::GetAccountId(), $data['carriers_name'],
                    $data['carriers_contact'], $data['carriers_email'], $data['carriers_phone'], $data['carriers_fax'],
                    $data['carriers_policy_number'], $data['carriers_address'], $data['carriers_coverages_list'],
                    $data['carriers_coverages_other'], "0", $data['carriers_recipient'],
                    $data['carriers_effective_start_date'], $data['carriers_effective_end_date'],
                    $data['checkbox_all_locations'] == 'on', $data['checkbox_receive_all'] == 'on',$data['checkbox_receive_all_save'] == 'on',$data['checkbox_receive_all_submission'] == 'on');
            $notice =  $updateType.($isRenewal ? ' renewed!' : ' added!');
            carriers_history::new_carriers_entry(ActiveMemberInfo::GetMemberId(), $carriers_id,$updateType.($isRenewal ? ' renewed' : ' created'));

            if($isRenewal && $data['carriers_hide_policy'] == '1') {
                carriers::hide_carrier($carrier['carriers_id']);
                carriers_history::hidden_changed(ActiveMemberInfo::GetMemberId(), $carrier['carriers_id'], true);
            }
        }
        
        //assign carrier/recipient locations

            if($data['carriers_recipient'] == 0 )
            {
                if($_POST['carrier_allloc'])
                {
                    carriers::set_all_locations($carriers_id, true);
                }
                else 
                {
                    carriers::set_all_locations($carriers_id, false);
                }
                
                foreach($locations as $location)
                {
                    $location_can_edit_key =  'carrier_location_can_edit_' . $location['licensed_locations_id'];
                    if($data['checkbox_all_locations'] == "on") $_POST['carrier_' . $location['licensed_locations_id']] = 'on';
                    if($_POST['carrier_' . $location['licensed_locations_id']] == "on")
                    {
                        // If they're an admin, check the flag.
                        // Otherwise, if it's a new carrier, set the flag to true.
                        // If it's an existing one, don't update the flag.
                        if(ActiveMemberInfo::_IsAccountAdmin()) {
                            $can_edit = $_POST[$location_can_edit_key] == "on";
                        } else if(!$carrier) {
                            $can_edit = true;
                        } else {
                            $can_edit = carriers::get_carrier_location_can_edit($carriers_id, $location['licensed_locations_id']);
                        }

                        carriers::link_carrier($location['licensed_locations_id'], $carriers_id, $can_edit);
                    }
                    else
                    {
                        carriers::unlink_carrier($location['licensed_locations_id'], $carriers_id);
                    }
                }
            } else {
                foreach($locations as $location)
                {
                    $curr_licensed_locations_id = $location['licensed_locations_id'];
                    $location_key = 'recipient_' . $curr_licensed_locations_id;
                    $receive_all_claims_key = 'recipient_send_all_' . $curr_licensed_locations_id;
                    $receive_all_claims_onsave_key = 'recipient_send_all_on_save_' . $curr_licensed_locations_id;
                    $receive_all_claims_onsubmit_key = 'recipient_send_all_on_submission_' . $curr_licensed_locations_id;
                    $location_can_edit_key =  'recipient_location_can_edit_' . $curr_licensed_locations_id;
                    
                    if($data['checkbox_all_locations'] == "on") $_POST[$location_key] = "on";
                    if($data['checkbox_receive_all'] == "on") $_POST[$receive_all_claims_key] = "on";
                    if($data['checkbox_receive_all_submission'] == "on") $_POST[$receive_all_claims_onsubmit_key] = "on";
                    if($data['checkbox_receive_all_save'] == "on") $_POST[$receive_all_claims_onsave_key] = "on";
                    
                    if($_POST[$location_key] == "on")
                    {
                        $always_email = $_POST[$receive_all_claims_key] == "on";
                        $always_email_on_save = $_POST[$receive_all_claims_onsave_key] == "on";
                        $always_email_on_submit = $_POST[$receive_all_claims_onsubmit_key] == "on";


                        // If they're an admin, check the flag.
                        // Otherwise, if it's a new carrier, set the flag to true.
                        // If it's an existing one, don't update the flag.
                        if(ActiveMemberInfo::_IsAccountAdmin()) {
                            $can_edit = $_POST[$location_can_edit_key] == "on";
                        } else if(!$carrier) {
                            $can_edit = true;
                        } else {
                            $can_edit = carriers::get_recipient_location_can_edit($carriers_id, $location['licensed_locations_id']);
                        }

                        carriers::link_email_recipient($curr_licensed_locations_id, $carriers_id, $always_email, $can_edit,$always_email_on_save,$always_email_on_submit);
                    }
                    else
                    {
                        carriers::unlink_email_recipient($curr_licensed_locations_id, $carriers_id);
                    }
                }
            }
            if($carrier)
            {
                $new_carrier = carriers::get_carrier_all_details($carriers_id);
                carriers_history::carriers_updated_entry(ActiveMemberInfo::GetMemberId(), $new_carrier, $old_carrier,$updateType);
            }

        
        http::redirect(BASEURL .'/members/claims/carriers/index.php', array('notice' => $notice));

    }
    else
    {
        $carrier = array_merge((array) $carrier, $data);
    }
}

// coverages per carrier
if($carrier)
{
    $existing_coverages = explode(',',$carrier['carriers_coverages_list']);
}
else
{
    $existing_coverages = array();
}

//Coverage checkboxes
$coverages = arrays::sort(claims::$coverages);

?>
