<?
include ACTIONS . '/members/protect.php';

$carrier = carriers::get_carrier($_REQUEST['carriers_id']);

$type_str = $carrier['carriers_recipient'] == 0 ? 'carrier' : 'recipient';

if($carrier > 0 && $carrier['join_accounts_id'] == ActiveMemberInfo::GetAccountId() )
{
    if($_REQUEST['hide_carrier'] == 1)
    {
        carriers::hide_carrier($_REQUEST['carriers_id']);
        $notice = 'The '.$type_str.' has been hidden';
    }
    else
    {
        carriers::unhide_carrier($_REQUEST['carriers_id']);
        $notice = 'The '.$type_str.' has been un-hidden';
    }
    carriers_history::hidden_changed(ActiveMemberInfo::GetMemberId(), $_REQUEST['carriers_id'], $_REQUEST['hide_carrier'] == 1);
}
else
{
    $notice = 'You do not have permissions to hide this carrier.';
}

http::redirect(BASEURL . '/members/claims/carriers/index.php', array('notice' => $notice));

?>