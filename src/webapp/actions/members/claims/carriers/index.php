<?php
include(ACTIONS . '/members/protect.php');

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right

$paging = new paging($per_page, $current_page);

$search = $_GET['ind_search'];

$db = mysqli_db::init();

include '_determine_carriers_params.php';

$locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
$carriers = carriers::getCarriersForMember(ActiveMemberInfo::GetMemberId(), $filter_location_id, $carriers_start_time, $carriers_end_time, $order, $carriers_show_hidden);
$recipients = carriers::getRecipientsForMember(ActiveMemberInfo::GetMemberId(), $filter_location_id, $carriers_show_hidden, $order);

$model = array(
    "carriers_start_time" => $carriers_start_time,
    "carriers_end_time" => $carriers_end_time,
    "carriers_show_hidden" => $carriers_show_hidden,
    "join_licensed_locations_id" => $filter_location_id
);