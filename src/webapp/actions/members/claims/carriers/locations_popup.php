<?php include(ACTIONS . '/members/protect.php');

$location = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
$licensed_locations_for_member = $location;

$carrier_locations = carriers::get_carrier_locations_ids($_REQUEST['carriers_id']);

$carrier_all_locations_checked = count($location) == count($carrier_locations);

$email_recipient_locations = carriers::get_linked_email_recipient_locations_ids($_REQUEST['carriers_id']);
$all_location_checked = count($location) == count($email_recipient_locations);

$always_email_locations = carriers::get_always_email_recipient_locations_ids($_REQUEST['carriers_id']);
$all_email_locations_checked = count($location) == count($always_email_locations);
$recipient_edit_locations = carriers::get_edit_email_recipient_locations_ids($_REQUEST['carriers_id']);


