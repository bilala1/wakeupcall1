<?php
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$file = $db->fetch_one('SELECT * FROM claims_files INNER JOIN files on files_id = join_files_id WHERE claims_files_id = ?',
    array($_REQUEST['claims_files_id']));
$claim_id = $file['join_claims_id'];
$claim = claims::get_claim($claim_id);
$perms = UserPermissions::UserObjectPermissions('claims', $claim);

if ($claim && $perms['delete']) {

    $deleteSuccess = Services::$fileSystem->deleteFile($file['files_id']);
    if ($deleteSuccess) {
        $query = $db->query('DELETE FROM claims_files WHERE claims_files_id = ?',
            array($file['claims_files_id']));
        $deleteSuccess = is_array($query->result);
    }

    if ($deleteSuccess) {
        claims_history::file_deleted_entry(ActiveMemberInfo::GetMemberId(), $file['join_claims_id'], $file['files_name']);

        http::redirect(BASEURL . '/members/claims/edit.php?claims_id=' . $claim_id . '&notice=' . urlencode('The file was deleted successfully!'));
    } else {
        http::redirect(BASEURL . '/members/claims/edit.php?claims_id=' . $claim_id . '&notice=' . urlencode('There was a problem deleting the file.  Please try again later.'));
    }
} else {
    http::redirect(BASEURL . '/members/claims/index.php?notice=' . urlencode('You don\'t have permission to delete this file.'));
}
?>
