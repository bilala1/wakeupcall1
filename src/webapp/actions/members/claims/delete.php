<?
include ACTIONS . '/members/protect.php';
use Services\Services;

$db = mysqli_db::init();

$claim = $db->fetch_one('
    SELECT c.claims_id
    FROM claims as c
    WHERE claims_id = ?',
    array($_REQUEST['claims_id']));

if($claim){
    //claim files to be deleted
    $claims_files = $db->fetch_all('SELECT *
                                              FROM claims_files
                                              INNER JOIN files ON files_id = join_files_id
                                              WHERE join_claims_id = ? ', array($claim['claims_id']));
    foreach ($claims_files as $file) {
        Services::$fileSystem->deleteFile($file['files_id']);
        $query = $db->query('DELETE FROM claims_files WHERE claims_files_id = ?',
            array($file['claims_files_id']));
    }

    $claims_table = new mysqli_db_table('claims');
    $claim = $claims_table->get($_REQUEST['claims_id']);
    $claims_table->delete($_REQUEST['claims_id']);
    
    //remove from specific claim table
    $db->query('delete from claim_workers_comp where join_claims_id = ?', array($_REQUEST['claims_id']));
    $db->query('delete from claim_general_liab where join_claims_id = ?', array($_REQUEST['claims_id']));
    $db->query('delete from claim_auto where join_claims_id = ?', array($_REQUEST['claims_id']));
    $db->query('delete from claim_property where join_claims_id = ?', array($_REQUEST['claims_id']));
    $db->query('delete from claim_employment where join_claims_id = ?', array($_REQUEST['claims_id']));
    
    $notice = 'The claim has been deleted';
}
else {
    $notice = 'You do not have permissions to delete this claim.';
}

http::redirect(BASEURL . '/members/claims/index.php', array('notice' => $notice));

?>