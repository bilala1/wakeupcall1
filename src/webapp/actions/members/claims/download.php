<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if (isset($_REQUEST['claims_files_id'])) {
    $file = $db->fetch_one('
        SELECT *
        FROM claims_files
        INNER JOIN files ON join_files_id = files_id
        INNER JOIN claims ON claims_id = join_claims_id
        WHERE
            claims_files_id = ? AND ' . UserPermissions::CreateInClause('claims'),
        array($_REQUEST['claims_files_id']
        ));

    if ($file) {
        members::log_action(ActiveMemberInfo::GetAccountId(), 'Downloaded from Claims Files: ' . $file['files_name']);

        $downloads = new mysqli_db_table('downloads');
        //record download
        $downloads->insert(array(
            'downloads_type' => 'claim',
            'join_id' => $_REQUEST['claims_files_id'],
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'downloads_datetime' => SQL('NOW()')
        ));

        $redirectUrl = Services::$fileSystem->getFileDownloadUrl($file['files_id']);

        if(!$redirectUrl) {
            http::redirect(BASEURL . '/members/claims/index.php',
                array('notice' => 'You are unable to view this document'));
        } else {
            http::redirect($redirectUrl);
        }
    } else {
        http::redirect(BASEURL . '/members/claims/index.php', array('notice' => 'You are unable to view this document'));
    }
}
?>
