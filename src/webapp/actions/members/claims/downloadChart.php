<?
$tempDirectory = '/tmp';
$tmpFilename = $_GET['tmpFilename'];
$fullFilename = $tempDirectory.'/'.$tmpFilename;

$filename = preg_replace("/[^A-Za-z0-9]/", '', $_GET['filename']).".png";
if (!file_exists($fullFilename)) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error--file '.$fullFilename.' does not exist.', true, 500);
    return;
}
header('Content-disposition: attachment; filename='.$filename);
header('Content-type: image/png');
readfile($fullFilename);
