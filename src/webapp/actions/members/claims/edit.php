<?
use Services\Services;

include(ACTIONS . '/members/protect.php');
#require_once(LIBS . '/Zend/Service/LiveDocx.php');

include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/ClaimsEmail.php'); 

require_once('_email_carrier_claim.php');
require_once('_fax_carrier_claim.php');

// Turn off WSDL caching
ini_set ('soap.wsdl_cache_enabled', 0);
 
// Define timezone
date_default_timezone_set('America/Los_Angeles');

function set_carriers_strings($array,$index1, $index2, $index_3, $index_4, $index_5)
{
    $new_array = array();
    foreach($array as $element)
    {
        $new_array[$element[$index1]] = $element[$index2] . " Coverage: " . $element[$index_3] . " Effective: " . date('n/j/Y', strtotime($element[$index_4])) . "-" . date('n/j/Y', strtotime($element[$index_5]));
    }

    return $new_array;
}

function set_recipient_strings($array,$index1, $index2)
{
    $new_array = array();
    foreach($array as $element)
    {
        $new_array[$element[$index1]] = $element[$index2];
    }

    return $new_array;
}

function notes_sort_helper()
{
    if(!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC')
        return ' ORDER BY claims_notes_date DESC ';
    else
        return ' ORDER BY claims_notes_date ASC ';
}

$dynemail = new ClaimsRequestDynamicEmail();
$db = mysqli_db::init();

$claim = $db->fetch_one('
    SELECT c.*, ca.*, cgl.*, cp.*, cwc.*, ll.*, cf.*,ce.*
    FROM claims as c
    JOIN licensed_locations as ll on ll.licensed_locations_id = c.join_licensed_locations_id
    left join claim_auto as ca on ca.join_claims_id = claims_id
    left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
    left join claim_property as cp on cp.join_claims_id = claims_id
    left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
    left join claim_employment as ce on ce.join_claims_id = claims_id
    left join claims_faxes cf on c.claims_id = cf.join_claims_id and cf.claims_faxes_status < 0
    WHERE claims_id = ?',
    array($_REQUEST['claims_id']
));

$claims_status_values = array(
    '' => 'Please Select...',
    claims::$SUBMIT_OPEN => 'Submitted - Open',
    claims::$SUBMIT_CLOSE => 'Submitted - Closed',
    claims::$RECORD_ONLY => 'Record Only - Not submitted',
    claims::$FIRST_AID =>'First Aid Only - Not submitted');

$claim_types = $db->get_enum_values('claims', 'claims_type');
if(!UserPermissions::UserCanAccessScreen('workers_comp')){
    unset($claim_types[array_search('Workers Comp', $claim_types)]);
}
if(!UserPermissions::UserCanAccessScreen('labor_claims')){
    unset($claim_types[array_search('Labor Law', $claim_types)]);
}
if(!UserPermissions::UserCanAccessScreen('other_claims')){
    unset($claim_types[array_search('Other', $claim_types)]);
}

if($claim && $_REQUEST['claims_id']){

    if(!UserPermissions::UserCanAccessObject('claims', $claim['claims_id'])) {
        http::redirect('index.php', array('notice' => 'You don\'t have permission to edit that claim'));
    }

    $claim_files = $db->fetch_all('SELECT * FROM claims_files INNER JOIN files ON join_files_id = files_id WHERE join_claims_id = ?',
                                    array($_REQUEST['claims_id']));
    $claim['claims_amount_incurred'] = $claim['claims_amount_paid'] + $claim['claims_amount_reserved'];

    $query = '
        SELECT
        claims_notes_id, join_claims_id, claims_notes_date, claims_notes_subject,claims_notes_note 
        FROM claims_notes
        WHERE join_claims_id = ?';
    $query = $query.notes_sort_helper();
    $claim_notes = $db->fetch_all($query,array($claim['claims_id']));
    foreach($claim_notes as $claim_note){
         $existing_notes[] = $claim_note['claims_notes_id'];
    }
    if($claim['claims_filed'] == 1) {
        $claims_status_values = array(
            Claims::$SUBMIT_OPEN => 'Submitted - Open',
            Claims::$SUBMIT_CLOSE => 'Submitted - Closed');
    }
    if($claim['claims_faxes_id']) {
        $claim_original_status = $claim['join_claims_status_codes_id'];
        $claim['join_claims_status_codes_id'] = Claims::$PENDING;
        $claims_status_values[Claims::$PENDING] = "Submission Pending";
    }
    if(!$claims_status_values[$claim['join_claims_status_codes_id']]) {
        $claims_status_values[$claim['join_claims_status_codes_id']] = $claim['join_claims_status_codes_id'];
    }
    if($claim['claim_employment_id']){
        $claim_employment_involved_parties = claims::get_claim_employment_involved_parties($claim['claims_id']);
    }
    $selected_carriers = claims::get_claim_x_carriers($claim['claims_id']);
    foreach($selected_carriers as $claim_x_carrier){
        $existing_carrier[] = $claim_x_carrier['claims_x_carriers_id'];
    }
    $emails_history = emails_history::get_metadata_for_claim($_REQUEST['claims_id']);

} else {
    $claim = array();
    if($_REQUEST['action'] == 'setLocation') {
        $claim['join_licensed_locations_id'] = $_REQUEST['licensed_locations_id'];
    } else if(ActiveMemberInfo::IsUserMultiLocation()) {
        $claim['join_licensed_locations_id'] = $_REQUEST['join_licensed_locations_id'];
    } else {
        $claim['join_licensed_locations_id'] = ActiveMemberInfo::SingleLocationUserLocationId();
    }
     // Need to pick licensed location first - set that view and get out of here.
    if(!isset($claim['join_licensed_locations_id'])) {
            $licensed_locations = licensed_locations::get_licensed_locations_with_permission('claims');
            $licensed_locations_names = array();
            foreach ($licensed_locations as $licensed_location) {
                $licensed_locations_names[$licensed_location['licensed_locations_id']] = $licensed_location['licensed_locations_name'];
            }
        framework::set_view('members/claims/add.php');
        return;
    }
}
$licensed_locations_id = $claim['join_licensed_locations_id'];
$location = licensed_locations::get_by_id($licensed_locations_id);
$locationName = $location['licensed_locations_name'];

$recipients = carriers::getRecipientsForMember(ActiveMemberInfo::GetMemberId(), $licensed_locations_id);

$is_recipients_only = $_REQUEST['is_recipients_only'] == 'true';
$carriers_for_location = array();
if ( is_numeric($licensed_locations_id)) {
    $carriers = carriers::get_carriers_for_location($licensed_locations_id, $is_recipients_only);
    $carriers_for_location['']= "Please Select";
    foreach($carriers as $carrier){
        $carrierText = $carrier['carriers_name'];
        $coverages = explode(',', $carrier['carriers_coverages_list']);
        if($carrier['carriers_coverages_other']) {
            $coverages[] = $carrier['carriers_coverages_other'];
        }
        if(count($coverages) > 0) {
            $carrierText .= ' (' . implode(', ', $coverages) . ')';
        }
        if($carrier['carriers_effective_end_date']) {
            $carrierText .= ' Expires: ' . Formatting::date($carrier['carriers_effective_end_date'], 'Not Set');
        }
        $carriers_for_location[$carrier['carriers_id']] = $carrierText;
    }
}
//only used for sending email to flagged catch all recipients for an entire corporation.
if ( isset($claim['join_licensed_locations_id'])) {
    $corporate_email_recipients = carriers::get_always_email_recipients($licensed_locations_id, carriers::HIDDEN_VISIBLE, true);
}
$corproate_recipient_count = count($corporate_email_recipients);
$recipients_tobe_notified_onsave = carriers::get_recipients_tobe_notified($claim['join_licensed_locations_id'],'always_email_on_save = 1');
$recipients_tobe_notified_onsubmit = carriers::get_recipients_tobe_notified($claim['join_licensed_locations_id'],'always_email_on_submit = 1');

$recipients_strings = set_recipient_strings($recipients, 'carriers_id' , 'carriers_name');
$licensed_locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
$departments = arrays::sort(claims::$departments);
$departments = array_combine($departments, $departments) + array('other' => 'Other...');

$errors = array();

if(!$claim['claims_email_subject']) {
    $claim['claims_email_subject'] = 'Notice of Claim for '.$locationName;
}
//Get default template ID;
$default_template_id = 0;
$default_template = claims_email_templates::getDefaultTemplate();
if ($default_template) {
    $default_template_id = $default_template['claims_email_templates_id'];
}

// Get list of templates
// Note if the template is a custom one
$templates = array(0 =>  'Default ' . SITE_NAME .' template');
$locationTemplates = claims_email_templates::getTemplateForLocation($licensed_locations_id);
foreach($locationTemplates as $template) {
    if($template['claims_email_templates_id'] == $default_template_id) {
        $template['claims_email_templates_name'] = $template['claims_email_templates_name'] . ' (default)';
    }
    $templates[$template['claims_email_templates_id']] = $template['claims_email_templates_name'];
}

$customTemplate = false;
$claimsTemplate = claims_email_templates::get_template_for_claim($_REQUEST['claims_id']);
if ($claimsTemplate) {
    $templates[$claimsTemplate['claims_email_templates_id']] = "(Custom Template)";
    $customTemplate = true;
    $templateHtml = stripslashes($claimsTemplate['email_html']);
}

// Make sure to set the template ID if one isn't set
// On a post, this will get overwritten from the the $data variable
if (!isset($claim['join_claims_email_templates_id'])) {
    $claim['join_claims_email_templates_id'] = $default_template_id;
}
if($_POST){
    $data = $_POST;
//print_r($data);exit;
    $validate = new validate($data);

    //set initial validation rules
    $validate
        ->setAllOptional()
        ->unsetOptionals(array(
            'claims_datetime' => 'Date of Incident',
            'claims_type' => 'Type',
            'submit_to_carrier' => 'Submit to Carrier'
        ))
        ->setErrors(array(
            'claims_datetime' => 'Please enter the date of the incident',
            'claims_type' => 'Please select the type of claim',

            'claim_workers_comp_employee_name' => 'Please enter the employee\'s name',
            'claim_workers_comp_job_title' => 'Please enter the employee\'s job title',
            'claim_workers_comp_employee_department' => 'Please select the employee\'s department',
            'claim_workers_comp_event_department' => 'Please select the department in which the event occurred',
            'claim_workers_comp_event_type' => 'Please select the type of event',
            'claim_workers_comp_injury_type' => 'Please select the type of injury',
            'claim_workers_comp_illness_type' => 'Please select the type of illness',
            'claim_workers_comp_classification[]' => 'Please enter the event\'s classification',
            'claim_workers_comp_osha_include' => 'Please select to include in OSHA report or not',
            'claim_general_liab_loss_type' => 'Please select a type of loss',

            'claim_auto_nature' => 'Please select a nature of the claim',
        ));

    if($_POST['submit_to_carrier'] != 'Yes') {
        $validate->unsetOptionals(array(
            'join_claims_status_codes_id' => 'Status'
            ));

    }

    //set specific rules per claim type (although error strings are set above)
    if(stristr($data['claims_type'], 'Comp')){                      //Workers Comp
        $validate->unsetOptionals(array(
            'claim_workers_comp_employee_name' => '',
            'claim_workers_comp_job_title' => '',
            'claim_workers_comp_employee_department' => '',
            'claim_workers_comp_event_department' => '',
            'claim_workers_comp_event_type' => '',
            'claim_workers_comp_classification' => ''
        ));

        if($data['claim_workers_comp_event_type'] == 'Injury' ) {
            $validate->unsetOptionals(array(
                'claim_workers_comp_injury_type' => ''
            ));
            if(count($data['claim_workers_comp_body_parts']) == 0){
                $errors['claim_workers_comp_body_parts'] = 'Please select at least one body part';
            }
            $data['claim_workers_comp_illness_type'] = null;
            //make checkbox arrays into strings
            $data['claim_workers_comp_body_parts'] = implode(',', (array)$data['claim_workers_comp_body_parts']);

        } else if($data['claim_workers_comp_event_type'] == 'Illness' ) {
            $validate->unsetOptionals(array(
                'claim_workers_comp_illness_type' => ''
            ));
            $data['claim_workers_comp_injury_type'] = null;
            $data['claim_workers_comp_body_parts'] = null;
        }

        $data['claim_workers_comp_classification_G'] = 0;
        $data['claim_workers_comp_classification_H'] = 0;
        $data['claim_workers_comp_classification_I'] = 0;
        $data['claim_workers_comp_classification_J'] = 0;
        
        foreach((array)$data['claim_workers_comp_classification'] as $wc_class)
        {
            if($wc_class == "work_comp_class_G")
                 $data['claim_workers_comp_classification_G'] = 1;
            if($wc_class == "work_comp_class_H")
                 $data['claim_workers_comp_classification_H'] = 1;
            if($wc_class == "work_comp_class_I")
                 $data['claim_workers_comp_classification_I'] = 1;
            if($wc_class == "work_comp_class_J")
                 $data['claim_workers_comp_classification_J'] = 1;
        }
        
        if($data['claim_workers_comp_classification_H'] == 1) // Away from work
        {
            $validate->unsetOptionals(array(
                'claim_workers_comp_datetime_away_start' => ''
            ));
            if($data['claim_workers_comp_datetime_away_start'] != null &&
                $data['claim_workers_comp_datetime_away_end'] != null) {
                $start = strtotime($data['claim_workers_comp_datetime_away_start']);
                $end = strtotime($data['claim_workers_comp_datetime_away_end']);
                if($end - $start <= 0) {
                    $errors['claim_workers_comp_datetime_away_end'] = "End date must be after start date";
                }
            }
        } 
        else {
            $data['claim_workers_comp_datetime_away_start'] = null;
            $data['claim_workers_comp_datetime_away_end'] = null;
        }
        
        if($data['claim_workers_comp_classification_I'] == 1) // Away from work
        {
            $validate->unsetOptionals(array(
                'claim_workers_comp_datetime_restricted_start' => ''
            ));
            if($data['claim_workers_comp_datetime_restricted_start'] != null &&
                $data['claim_workers_comp_datetime_restricted_end'] != null) {
                $start = strtotime($data['claim_workers_comp_datetime_restricted_start']);
                $end = strtotime($data['claim_workers_comp_datetime_restricted_end']);
                if($end - $start <= 0) {
                    $errors['claim_workers_comp_datetime_restricted_start'] = "End date must be after start date";
                }
            }
        } 
        else {
            if($data['claim_workers_comp_classification_H'] != 1){
                $data['claim_workers_comp_datetime_restricted_start'] = null;
                $data['claim_workers_comp_datetime_restricted_end'] = null; 
            }          
        }
    }
    else if(stristr($data['claims_type'], 'Liability') || stristr($data['claims_type'], 'Other')){            //General Liability / Guest Property
        $validate->unsetOptionals(array(
            'claim_general_liab_claimant' => 'Claimant',
        ));

        if(stristr($data['claims_type'], 'Liability')) {
            $validate->unsetOptionals(array(
                'claim_general_liab_loss_type' => '',
            ));
        }
        if(count($data['claim_general_liab_loss_area']) == 0){
            $errors['claim_general_liab_loss_area'] = 'Please select at least one area of loss';
        }
        
        //make checkbox arrays into strings
        $data['claim_general_liab_loss_area'] = implode(',', $data['claim_general_liab_loss_area']);
    }
    else if(stristr($data['claims_type'], 'Auto')){                 //Auto
        $validate->unsetOptionals(array(
            'claim_auto_claimant' => 'Claimant',
            'claim_auto_nature' => ''
        ));
    }
    else if(stristr($data['claims_type'], 'Property')){             //Your Property
        
        if(count($data['claim_property_loss_types']) == 0){
            $errors['claim_property_loss_types'] = 'Please select at least one type of loss.';
        }
        $data['claim_property_loss_type'] = implode(',', (array)$data['claim_property_loss_types']);
        
    }

    $uploaded_files = files::fixUploadFileArray($_FILES['claims_file']);

    $validate->setFileType('both');
    foreach ($uploaded_files as $file) {
        if ($file['name'] == '') {
            continue;
        }

        $validate->setFile($file, 'claims_file');
    }

    if(!isset($data['claims_type'])) {
        $errors['claims_type'] = 'You must choose a type';
    }
    if($data['customize_template'] != '1'){
        $validate->setOptional('email_html');
    }

    $errors += $validate->test();

    if(!$errors){
        $licensed_locations = arrays::set_keys($licensed_locations, 'licensed_locations_id');
        
        $claims = new mysqli_db_table('claims');
        $claims_files = new mysqli_db_table('claims_files');
        $claims_faxes = new mysqli_db_table('claims_faxes');
        $claims_faxes_files = new mysqli_db_table('claims_faxes_files');
        $claims_x_carriers = new mysqli_db_table('claims_x_carriers');
        $claims_notes_table = new mysqli_db_table('claims_notes');
        $claims_notes_array = array();
        foreach($data['claims_notes_id'] as $key => $claims_notes_id){
            $claims_notes_array[$key]['claims_notes_id'] = $claims_notes_id;
        }
        foreach($data['claims_notes_subject'] as $key => $claims_notes_subject){
            $claims_notes_array[$key]['claims_notes_subject'] = $claims_notes_subject;
        }
        foreach($data['claims_notes_note'] as $key => $claims_notes_note){
            $claims_notes_array[$key]['claims_notes_note'] = $claims_notes_note;
        }
        $claims_carriers = array();
        foreach($data['join_carriers_id'] as $key => $join_carriers_id){
            $claims_carriers[$key]['join_carriers_id'] = $join_carriers_id;
        }
        foreach($data['claims_x_carriers_id'] as $key => $claims_x_carriers_id){
            $claims_carriers[$key]['claims_x_carriers_id'] = $claims_x_carriers_id;
        }
        foreach($data['claims_claim_number'] as $key => $claims_claim_number){
            $claims_carriers[$key]['claims_x_carriers_claim_number'] = $claims_claim_number;
        }
        foreach($data['claims_x_carriers_datetime_submitted'] as $key => $claims_x_carriers_datetime_submitted){
            $claims_carriers[$key]['claims_x_carriers_datetime_submitted'] = empty($claims_x_carriers_datetime_submitted) ? null : date('Y-m-d', strtotime($claims_x_carriers_datetime_submitted));
        }
        foreach($data['claims_x_carriers_datetime_closed'] as $key => $claims_datetime_closed){
            $claims_carriers[$key]['claims_x_carriers_datetime_closed'] = empty($claims_datetime_closed) ? null :  date('Y-m-d', strtotime($claims_datetime_closed));
            
        }
        foreach($data['claims_amount_paid'] as $key => $claims_amount_paid){
            $claims_carriers[$key]['claims_x_carriers_amount_paid'] = $claims_amount_paid;
        }
        foreach($data['claims_amount_reserved'] as $key => $claims_amount_reserved){
            $claims_carriers[$key]['claims_x_carriers_amount_reserved'] = $claims_amount_reserved;
        }
        foreach($data['claims_x_carriers_adjusters_name'] as $key => $adjusters_name){
            $claims_carriers[$key]['claims_x_carriers_adjusters_name'] = $adjusters_name;
        }
        foreach($data['claims_x_carriers_adjusters_email'] as $key => $adjusters_email){
            $claims_carriers[$key]['claims_x_carriers_adjusters_email'] = $adjusters_email;
        }
        foreach($data['claims_x_carriers_adjusters_phone'] as $key => $adjusters_phone){
            $claims_carriers[$key]['claims_x_carriers_adjusters_phone'] = $adjusters_phone;
        }

        foreach($data['join_status_codes_id'] as $key => $carriers_status){
            if($data['join_claims_status_codes_id']== Claims::$SUBMIT_CLOSE){
                $claims_carriers[$key]['join_claims_status_codes_id'] = Claims::$SUBMIT_CLOSE;
                $claims_carriers[$key]['claims_x_carriers_datetime_closed'] = date('Y-m-d');
            }else{
                $claims_carriers[$key]['join_claims_status_codes_id'] = $carriers_status;
                if($carriers_status == Claims::$SUBMIT_CLOSE && empty($claims_carriers[$key]['claims_x_carriers_datetime_closed'])) {
                    $claims_carriers[$key]['claims_x_carriers_datetime_closed'] = date('Y-m-d');
                }
            }
        }
        foreach($data['claim_workers_comp_medical_costs'] as $key => $workers_comp_medical_costs){
            $claims_carriers[$key]['claims_x_carriers_workers_comp_medical_costs'] = $workers_comp_medical_costs;
        }
        //todo test licensed locations
        $data['join_licensed_locations_id'] = $data['licensed_locations_id'];
        $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
        $data['claims_datetime'] = date('Y-m-d', strtotime($data['claims_datetime']));
        $data['claims_datetime_notified'] = empty($data['claims_datetime_notified']) ? null : date('Y-m-d', strtotime($data['claims_datetime_notified']));
        $data['claim_workers_comp_datetime_away_start'] = empty($data['claim_workers_comp_datetime_away_start']) ? null : date('Y-m-d', strtotime($data['claim_workers_comp_datetime_away_start']));
        $data['claim_workers_comp_datetime_away_end'] = empty($data['claim_workers_comp_datetime_away_end']) ? null : date('Y-m-d', strtotime($data['claim_workers_comp_datetime_away_end']));
        $data['claim_workers_comp_datetime_restricted_start'] = empty($data['claim_workers_comp_datetime_restricted_start']) ? null : date('Y-m-d', strtotime($data['claim_workers_comp_datetime_restricted_start']));
        $data['claim_workers_comp_datetime_restricted_end'] = empty($data['claim_workers_comp_datetime_restricted_end']) ? null : date('Y-m-d', strtotime($data['claim_workers_comp_datetime_restricted_end']));
        

        $data_files = array();

        //loop through number of uploaded files
        foreach ($uploaded_files as $index => $file) {
            if ($file['size']) {
                $file_entry = Services::$fileSystem->storeFile($file, 'claims');

                if(!$file_entry) {
                    $errors['claims_file'] = 'There was a problem uploading a file';
                    return;
                }

                $file_entry['join_files_id'] = $file_entry['files_id'];
                $data_files[] = $file_entry;
            }
        }

        $submit_file_ids = array(); // if no uploads we will use an empty
        if (isset($_POST['submit-existing-doc'])) {
            $submit_file_ids = $_POST['submit-existing-doc'];
        }

        if($claim['claims_id']){
            //Don't update status if submission is pending
            if($claim['join_claims_status_codes_id'] == Claims::$PENDING) {
                $data['join_claims_status_codes_id'] = $claim_original_status;
            }
            $old_claim = claims::get_claim($claim['claims_id']);
            //claims table
            $claims->update($data, $claim['claims_id']);
            $claims_id = $claim['claims_id'];

            //Specific claim table
            if($claim['claim_workers_comp_id']){                 //Workers Comp
                $claim_workers_comp_table = new mysqli_db_table('claim_workers_comp');
                $claim_workers_comp_table->update($data, $claim['claim_workers_comp_id']);
            }
            else if($claim['claim_general_liab_id']){            //General Liability / Guest Property
                $claim_general_liab_table = new mysqli_db_table('claim_general_liab');
                $claim_general_liab_table->update($data, $claim['claim_general_liab_id']);
            }
            else if($claim['claim_auto_id']){                    //Auto
                $claim_auto_table = new mysqli_db_table('claim_auto');
                $claim_auto_table->update($data, $claim['claim_auto_id']);
            }
            else if($claim['claim_property_id']){                //Your Property
                $claim_property_table = new mysqli_db_table('claim_property');
                $claim_property_table->update($data, $claim['claim_property_id']);
            }
            else if(stristr($data['claims_type'], 'Labor')){             //Labor
                $claim_property_table = new mysqli_db_table('claim_employment');
                $claim_employment_involved_parties = new mysqli_db_table('claim_employment_involved_parties');
                $claim_property_table->update($data, $claim['claim_employment_id']);
                $old_employment_involved_parties = claims::get_claim_employment_involved_parties($claims_id);
                $old_claim['employment_involved_parties'] = $old_employment_involved_parties;
                claims::delete_claim_employment_involved_parties_by_claims_id($claims_id);
                for($i=0;$i<count($data['claim_employment_involved_parties']);$i++){
                    if($data['claim_employment_involved_parties'][$i] !=''){
                        $claim_employment_parties['claim_employment_involved_parties_name'] = $data['claim_employment_involved_parties'][$i];
                        $claim_employment_parties['join_claims_id'] = $claims_id;
                        $claim_employment_involved_parties->insert($claim_employment_parties);
                    }
                }
                $new_employment_involved_parties = claims::get_claim_employment_involved_parties($claims_id);
                $new_claim['employment_involved_parties'] = $new_employment_involved_parties;
            }
            //claims::delete_claim_x_carriers_by_claims_id($claims_id);
            $status_closed_count = 0;
            $claims_update_array = array();
            $carrier_updated = array();
            foreach($claims_carriers as $claims_carrier){

                $claims_carrier['join_claims_id'] = $claims_id;
                
                //if the claim is submitted to carrier, change the status to submitted open
                if(isset($data['claims_submit_type_fax-' . $claims_carrier['join_carriers_id']])
                    || isset($data['claims_submit_type_email-' . $claims_carrier['join_carriers_id']])
                    || isset($data['claims_submit_type_adjuster-' . $claims_carrier['join_carriers_id']])
                ) {
                    if ($claims_carrier['join_claims_status_codes_id'] != 'SUBMIT_OPEN' && $claims_carrier['join_claims_status_codes_id'] != 'SUBMIT_CLOSE') {
                        $claims_carrier['join_claims_status_codes_id'] = 'SUBMIT_OPEN';
                    }
                    if ($claims_carrier['join_claims_status_codes_id'] == 'SUBMIT_OPEN') {
                        $claims_update_array['join_claims_status_codes_id'] = 'SUBMIT_OPEN';
                        $claims_update_array['claims_status'] = 'Submitted - Open';
                        $claims_update_array['claims_filed'] = 1;
                    }
                    if (empty($claims_carrier['claims_x_carriers_datetime_submitted'])) {
                        $claims_carrier['claims_x_carriers_datetime_submitted'] = date('Y-m-d');
                    }
                }
                if($claims_carrier['join_claims_status_codes_id'] == 'SUBMIT_CLOSE'){
                    $status_closed_count++;
                    if(empty($claims_carrier['claims_x_carriers_datetime_closed'])){
                       $claims_carrier['claims_x_carriers_datetime_closed'] = date('Y-m-d'); 
                    }
                }
                
                if(empty($claims_carrier['claims_x_carriers_id']) && !empty($claims_carrier['join_carriers_id'])){
                    $claims_carrier['claims_x_carriers_id']='';

                    $claims_x_carriers->insert($claims_carrier);
                }else if(!empty($claims_carrier['join_carriers_id'])&& !empty($claims_carrier['claims_x_carriers_id'])){
                    $carrier_updated[] = $claims_carrier['claims_x_carriers_id'];
                    $claims_x_carriers->update($claims_carrier, $claims_carrier['claims_x_carriers_id']);
                }
                
            }
            //if the status of all carriers are closed, Chnage the status of claim
            
            if(count($claims_carriers) == $status_closed_count) {
                $claims = new mysqli_db_table('claims');
                $claims_update_array['join_claims_status_codes_id'] = claims::$SUBMIT_CLOSE;
                $claims_update_array['claims_status'] = 'Submitted - Closed';
            }
            if(!empty($claims_update_array)){
                $claims->update($claims_update_array, $claims_id);
            }
            $deleted_carriers = array_diff($existing_carrier,$carrier_updated);
            if(count($deleted_carriers) > 0){
                claims::delete_claim_x_carriers_by_id($deleted_carriers);
            }
            claims_history::claim_updated_entry(ActiveMemberInfo::GetMemberId(),$new_employment_involved_parties, $old_employment_involved_parties);

            $new_claim = claims::get_claim($claim['claims_id']);
            if($new_employment_involved_parties){
                $new_claim['employment_involved_parties'] = $new_employment_involved_parties;
            }
            claims_history::claim_updated_entry(ActiveMemberInfo::GetMemberId(),$new_claim, $old_claim);
            $notes_updated = array();
            if(count($claims_notes_array) > 0){
                foreach($claims_notes_array as $claims_note){
                    if(empty($claims_note['claims_notes_id'])){
                        $claims_note['claims_notes_id'] = '';
                        $claims_note['join_claims_id'] = $claims_id;
                        if($claims_note['claims_notes_note'] && $claims_note['claims_notes_subject'])
                        {
                            claims_notes::create($claims_note);
                        }
                    }else if(!empty($claims_note['claims_notes_id'])){
                        $notes_updated[] = $claims_note['claims_notes_id'];
                        $claims_note['join_claims_id'] = $claims_id;
                        claims_notes::update($claims_note, $claims_note);
                    }
                }
            }
            $deleted_notes = array_diff($existing_notes,$notes_updated);
            if(count($deleted_notes) > 0){
                for($i = 0;$i < count($deleted_notes) ; $i++){
                    claims_notes::delete($deleted_notes[$i]);
                }
            }
            
            //notice
            $notice = 'Claim updated.';
            if($_POST['submit_to_carrier'] != 'Yes') {
               $incident_status = "updated";
               $notify_type = 'always_email = 1'; 
            }
            if($_POST['submit_to_carrier'] == 'Yes') {
                $notice = 'Claim updated and sent!';
                $incident_status = "submitted to a carrier";
                $notify_type = 'always_email = 1 OR always_email_on_submit = 1';
                
            }

        }else{
            if(isset($data['join_carriers_id']) && $data['join_carriers_id'] == "") {
                $data['join_carriers_id'] = NULL;
            }
            if(!$data['join_claims_status_codes_id']) {
                // This can only happen if they clicked submit, so set to submitted open
                $data['join_claims_status_codes_id'] = Claims::$SUBMIT_OPEN;
            }

            $claims->insert($data);
            $claims_id = $claims->last_id();
            
            $data['join_claims_id'] = $claims_id;

            foreach($claims_notes_array as $claims_note){
                $claims_note['join_claims_id'] = $claims_id;

                if($claims_note['claims_notes_note'] && $claims_note['claims_notes_subject'])
                {
                    claims_notes::create($claims_note);
                }
            }

            $status_closed_count = 0;
            $claims_update_array = array();

            foreach($claims_carriers as $claims_carrier){
                $claims_carrier['join_claims_id'] = $claims_id;
                //if the claim is submitted to carrier, change the status to submitted open
                if(isset($data['claims_submit_type_fax-'.$claims_carrier['join_carriers_id']]) || isset($data['claims_submit_type_email-'.$claims_carrier['join_carriers_id']]) 
                         || isset($data['claims_submit_type_adjuster-'.$claims_carrier['join_carriers_id']])){
                      if($claims_carrier['join_claims_status_codes_id'] !='SUBMIT_OPEN' || $claims_carrier['join_claims_status_codes_id'] !='SUBMIT_CLOSE'){
                          $claims_carrier['join_claims_status_codes_id'] = 'SUBMIT_OPEN';
                          $claims_carrier['claims_x_carriers_datetime_submitted'] = date('Y-m-d');
                      }
                      if($claims_carrier['join_claims_status_codes_id'] =='SUBMIT_OPEN'){
                          $claims_update_array['join_claims_status_codes_id'] = 'SUBMIT_OPEN';
                          $claims_update_array['claims_status'] = 'Submitted - Open';
                          $claims_update_array['claims_filed'] = 1;
                      }
                }
                if($claims_carrier['join_claims_status_codes_id'] == 'SUBMIT_CLOSE'){
                    $status_closed_count++;
                    if(empty($claims_carrier['claims_x_carriers_datetime_closed'])){
                       $claims_carrier['claims_x_carriers_datetime_closed'] = date('Y-m-d'); 
                    }
                }
                if(!empty($claims_carrier['join_carriers_id'])){
                    $claims_x_carriers->insert($claims_carrier);
                }
            }
            if(count($claims_carriers) == $status_closed_count) {
                $claims_update_array['join_claims_status_codes_id'] = claims::$SUBMIT_CLOSE;
                $claims_update_array['claims_status'] = 'Submitted - Closed';
            }
            if(!empty($claims_update_array)){
                $claims = new mysqli_db_table('claims');
                $claims->update($claims_update_array, $claims_id);
            }
            
            //Specific claim table
            if(stristr($data['claims_type'], 'Comp')){                      //Workers Comp
                $claim_workers_comp_table = new mysqli_db_table('claim_workers_comp');
                $claim_workers_comp_table->insert($data);
            }
            else if(stristr($data['claims_type'], 'Liability') || stristr($data['claims_type'], 'Other')){            //General Liability / Guest Property
                $claim_general_liab_table = new mysqli_db_table('claim_general_liab');
                $claim_general_liab_table->insert($data);
            }
            else if(stristr($data['claims_type'], 'Auto')){                 //Auto
                $claim_auto_table = new mysqli_db_table('claim_auto');
                $claim_auto_table->insert($data);
            }
            else if(stristr($data['claims_type'], 'Property')){             //Your Property
                $claim_property_table = new mysqli_db_table('claim_property');
                $claim_property_table->insert($data);
            }
            else if(stristr($data['claims_type'], 'Labor')){             //Your Property
                $claim_property_table = new mysqli_db_table('claim_employment');
                $claim_employment_involved_parties = new mysqli_db_table('claim_employment_involved_parties');
                $claim_property_table->insert($data);
                for($i=0;$i<count($data['claim_employment_involved_parties']);$i++){
                    if($data['claim_employment_involved_parties'][$i] !=''){
                        $claim_employment_parties['claim_employment_involved_parties_name'] = $data['claim_employment_involved_parties'][$i];
                        $claim_employment_parties['join_claims_id'] = $claims_id;
                        $claim_employment_involved_parties->insert($claim_employment_parties);
                    }
                }
                    
            }
            claims_history::new_claim_entry(ActiveMemberInfo::GetMemberId(), $claims_id);

            if ( $_POST['submit_to_carrier'] != 'Yes'){
                $incident_status = "created";
                $notify_type = 'always_email = 1 OR always_email_on_save = 1';
                
            }
            if ( $_POST['submit_to_carrier'] == 'Yes'){
                $incident_status = "submitted to a carrier";
                $notify_type = 'always_email = 1 OR always_email_on_submit = 1 OR always_email_on_save = 1';
                
            }

        }

        if (!empty($data_files)) {
            foreach ($data_files as $index => $file) {
                $file['join_claims_id'] = $claims_id;
                $claims_files->insert($file);
                claims_history::file_added_entry(ActiveMemberInfo::GetMemberId(), $claims_id, $file['files_name']);

                if ( $_POST['submit-new-doc'][$index] == 'on' ) {
                    array_push($submit_file_ids, $claims_files->last_id);
                }
            }
        }

        if($_POST['customize_template'] == 1 ){
            $claims_email_templates = new mysqli_db_table('claims_email_templates');
            $email_templates['join_licensed_locations_id'] = $claim['join_licensed_locations_id'];
            $email_templates['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
            $email_templates['join_claims_id'] = $claims_id;
            $template = addslashes($data['email_html']);
            $template = trim($template);
            $template = preg_replace( "/\r|\n/", "", $template );
            $email_templates['email_html'] = $template;

            $template = claims_email_templates::get_template_for_claim($claims_id);
            if(!$template['claims_email_templates_id']){
                //Insert an entry to templates table
                $claims_email_templates->insert($email_templates);
                $claims_email_templates_id = $claims_email_templates->last_id();
                $data['join_claims_email_templates_id'] = $claims_email_templates_id;
                $claims->update($data, $claims_id);
            }else{
                $claims_email_templates->update($email_templates, $template['claims_email_templates_id']);
            }
        } else {
            $template = claims_email_templates::get_template_for_claim($claims_id);
            if($template) {
                claims_email_templates::delete_by_id($template['claims_email_templates_id']);
            }
        }

        $submitFiles = array(); // if no uploads we will use an empty
        if (isset($submit_file_ids) && count($submit_file_ids) > 0) {
            $query = 'SELECT * from claims_files inner join files on join_files_id = files_id where join_claims_id = ? and claims_files_id in (' .
                str_repeat('?,', count($submit_file_ids) - 1) . '?)';
            $args = array($claims_id);
            $args = array_merge($args, $submit_file_ids);
            $submitFiles = $db->fetch_all($query, $args);
        }

        $notify_recipients_on_update = carriers::get_recipients_tobe_notified($data['join_licensed_locations_id'], $notify_type);
        foreach($notify_recipients_on_update as $recipient)
        {
            ob_start();
            include VIEWS . '/emails/notify_recipients.php';
            $message = ob_get_clean();
            $subject  = 'An incident has been ' . $incident_status . ' in ' . SITE_NAME ;
            $from     = AUTO_EMAIL;
            $to       = $recipient['carriers_email'];
            $additional_header_data = array(
                "event_id" => 'claims-notify',
                "claim_id" => $claims_id,
                "carrier_id" => $recipient['carriers_id'],
                "carrier_name" => $recipient['carriers_name']
            );
            $files_to_attach = $data['claim_send_files_to_recipients'] == 'on' ? $submitFiles : array();
            sendClaimEmail($claims_id, $to, $from, $subject, $message, $files_to_attach, $additional_header_data);
        }

        //get claim from db again
        //todo test licensed locations
        $claim = $db->fetch_one('
            SELECT c.*, ca.*, cgl.*, cp.*, cwc.*,ce.*, members.members_email
            FROM claims as c
            JOIN claims_status_codes csc on c.join_claims_status_codes_id = csc.claims_status_codes_id
            LEFT JOIN members ON members_id = c.join_members_id
            LEFT JOIN licensed_locations as ll on ll.join_members_id = members_id
            left join claim_auto as ca on ca.join_claims_id = claims_id
            left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
            left join claim_property as cp on cp.join_claims_id = claims_id
            left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
            left join claim_employment as ce on ce.join_claims_id = claims_id
            WHERE
                claims_id = ?',
            array($claims_id
        ));
        /*echo $db->debug();
        pre($claim);*/
        //send the claim (fax)

        if($_POST['submit_to_carrier'] == 'Yes'){
          foreach($data['join_carriers_id'] as $key => $join_carriers_id){
            if($claim['join_claims_status_codes_id'] != Claims::$PENDING){
                if($data['claims_submit_type_fax-'.$join_carriers_id] === 'fax'){
                    $faxResult = submitClaimViaFax($claim, $join_carriers_id, $submitFiles);
                    $notice .= '<br>' . $faxResult['notice'];
                }
                if($data['claims_submit_type_email-'.$join_carriers_id] === 'email'){
                    $email_success = submitClaimToCarrierEmail($join_carriers_id, $claims_id, true, $submitFiles, $_REQUEST['claims_email_subject'], false);
                    if($email_success) {
                        $claims_update_array = array();
                        if($claim['join_claims_status_codes_id'] != Claims::$SUBMIT_OPEN) {
                            claims::setStatusCodeAndDescription($claims_update_array, Claims::$SUBMIT_OPEN);
                            claims_history::claim_status_updated_entry(ActiveMemberInfo::GetMemberId(), $claims_update_array, $claim);
                        }
                        if(empty($claim['claims_datetime_submitted'])) {
                            $claims_update_array['claims_datetime_submitted'] = date('Y-m-d');
                            claims_history::claim_submitted_date_updated_entry(ActiveMemberInfo::GetMemberId(), $claim);
                        }
                        if(count($claims_update_array) > 0) {
                            $claims = new mysqli_db_table('claims');
                            $claims->update($claims_update_array, $claims_id);
                        }
                    }
                }
                if($data['claims_submit_type_adjuster-'.$join_carriers_id] === 'email'){
                    $email_success = submitClaimToCarrierEmail($join_carriers_id, $claims_id, true, $submitFiles, $_REQUEST['claims_email_subject'], true);
                }
            }
          }  
        }
        
        // send a copy to all carriers/recipient that were selected from the drop down
        if($_POST['send_copy_carriers_ids'] )
        {
            foreach(array_unique((array)$_POST['send_copy_carriers_ids']) as $email_carriers_id)
            {
                if (!empty($email_carriers_id) ) {
                    submitClaimToCarrierEmail($email_carriers_id, $claims_id, false, $submitFiles, $_REQUEST['claims_email_subject'], false);
                }
            }
        }
        

        http::redirect(BASEURL .'/members/claims/index.php', array('notice' => $notice));
    }else{
        $claim = array_merge((array) $claim, $data);
    }
}
