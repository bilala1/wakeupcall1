<?php
include(ACTIONS . '/members/protect.php');
$claims_email_templates_id = $_REQUEST['claims_email_templates_id'];
$template = claims_email_templates::get_by_id($claims_email_templates_id);
$claims_id = $template['join_claims_id'];
$licenced_location_id = $template['join_licensed_locations_id']; 

if($licenced_location_id == 0){ //all location
    if (ActiveMemberInfo::_IsAccountAdmin()){
        claims_email_templates::delete_by_id($claims_email_templates_id);
        if($claims_id){
            claims::remove_claims_email_templates($claims_id);
            $notice = 'Template Deleted';
        }
    }
}else{
    $permissions = emails_templates_permissions::get_for_location($licenced_location_id);
    if (ActiveMemberInfo::_IsAccountAdmin() || $permissions['location_can_override_claims_email'] == 1){
        claims_email_templates::delete_by_id($claims_email_templates_id);
        if($claims_id){
            $claims_id::remove_claims_email_templates($claims_id);
             $notice = 'Template Deleted';
        }
    }
}

http::redirect(BASEURL .'/members/claims/email_templates/index.php', array('notice' => $notice));
?>