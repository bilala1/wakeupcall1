<?
include(ACTIONS . '/members/protect.php');
include(SITE_PATH . '/' . MODELS . '/DynamicEmail/ClaimsEmail.php'); 
include(SITE_PATH . '/' . MODELS . '/DynamicEmail/TmpEmailFileHandler.php'); 

$dynemail = new ClaimsRequestDynamicEmail();

//add, edit, on_the_fly
$email_edit_type = $_REQUEST['email_edit_type'] ? $_REQUEST['email_edit_type'] : 'add';
$licensed_locations_id = $_REQUEST['licensed_locations_id'] ? $_REQUEST['licensed_locations_id'] : 0;
$claims_email_templates_id = $_REQUEST['claims_email_templates_id'] ? $_REQUEST['claims_email_templates_id'] : 0;

if($claims_email_templates_id){
    $template = claims_email_templates::get_by_id($claims_email_templates_id);
}
$template_html = $_REQUEST['email_html'] ? $_REQUEST['email_html'] : 
                 ($template ? $template['email_html'] : emails_default_templates::get_claims_email_template());

if($email_edit_type == 'add')
{
    $template_html = emails_default_templates::get_claims_email_template();
    $licensed_locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
}

if($email_edit_type == 'edit' || $email_edit_type == 'on_the_fly')
{
     $licensed_locations = null;
     $edit_location = licensed_locations::get_by_id($licensed_locations_id);
     $claims_email_templates_name = claims_email_templates::get_email_template_name($claims_email_templates_id);
}


$corporate_email = claims_email_templates::get_for_account(ActiveMemberInfo::GetAccountId());

if($_POST)
{
    $template_html =  $_POST['email_html'];
    $claims_email_templates_name = $_POST['claims_email_templates_name'];
    $validate = new validate($_POST);
    $validate
        ->setOptional('licensed_locations_id');
    $errors = array();
    if($_POST['claims_email_templates_name'] == ''){
        $validate->setTitle('claims_email_templates_name',"template name");
    }
    $email_template = claims_email_templates::check_email_templates_exists(ActiveMemberInfo::GetAccountId(),$claims_email_templates_name,$claims_email_templates_id);
    if(!empty($email_template)){
       $errors['claims_email_templates_name'] = "This name exists for another template";
    }
    $errors = array_merge($errors, $validate->test());
    $template_html =  $_POST['email_html'];
    $licensed_locations_id = $_POST['licensed_locations_id'] ? $_POST['licensed_locations_id'] : 0;

    if(empty($errors)){
        claims_email_templates::add_or_update_for_location(ActiveMemberInfo::GetAccountId(), $licensed_locations_id, $template_html,$claims_email_templates_name,$claims_email_templates_id);

        http::redirect(BASEURL .'/members/claims/email_templates/index.php', array('notice' => $notice));
    }
}
?>

