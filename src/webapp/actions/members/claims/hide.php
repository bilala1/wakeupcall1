<?
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$query = 'SELECT * FROM claims WHERE claims_id=?';
$claims = $db->fetch_all($query, array($_REQUEST['claims_id']));

if($claims > 0 )
{
    $claim = $claims[0];

    if($_REQUEST['hide_claim'] == 1)
    {
        $claim['claims_hidden'] = 1;
        $notice = 'The claim has been hidden';
    }
    else
    {
        $claim['claims_hidden'] = 0;
        $notice = 'The claim has been un-hidden';
    }

    $claims_table = new mysqli_db_table('claims');
    $claims_table->update($claim, $claim['claims_id']);
    $claims_id = $claim['claims_id'];
}
else
{
    $notice = 'You do not have permissions to hide this claim.';
}

http::redirect(BASEURL . '/members/claims/index.php', array('notice' => $notice));

?>