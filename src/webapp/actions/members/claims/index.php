<?php
include(ACTIONS . '/members/protect.php');

// show all
$paging = new paging(PHP_INT_MAX, 1);

// determine the date range. The logic goes--take the param in the request if exists. If not
// take the saved param in the session. If not use the default date.

include '_determine_claims_params.php';

$locations = licensed_locations::get_licensed_locations_with_permission(members_access_levels::claims);

$claims = claims::get_paged_claims_list($paging, $startTime, $endTime,
                                $order, $claims_show_hidden, $claims_show_visible, ActiveMemberInfo::GetMemberId(),
    $filter_licensed_locations_id,$join_claims_status_codes_id);

foreach($claims as &$claim) {
    //get files for this claim
    $claim['files'] = $db->fetch_all('SELECT claims_files_id, files_name FROM claims_files INNER JOIN files ON join_files_id = files_id WHERE join_claims_id = ? ', array($claim['claims_id']));
    $claim['has_emails'] = $db->fetch_singlet('SELECT count(1) FROM sent_emails_history where join_claims_id = ?', array($claim['claims_id']));
    unset($claim);
}
$claims_status_values = array(
    '' => 'Any status',
    claims::$SUBMIT_OPEN => 'Submitted - Open',
    claims::$SUBMIT_CLOSE => 'Submitted - Closed',
    claims::$RECORD_ONLY => 'Record Only - Not submitted',
    claims::$FIRST_AID =>'First Aid Only - Not submitted',
    Claims::$PENDING => "Submission Pending");
foreach($claims as $claim){
    if($claim['claims_faxes_id']) {
        $claim['join_claims_status_codes_id'] = Claims::$PENDING;
        $claim['claims_status_code_description'] = "Submission Pending";
    }
    if($claim['claim_workers_comp_id']){                 //Workers Comp
        $workcomp_claims[] = $claim;
    }
    else if($claim['claim_auto_id']){                    //Auto
        $auto_claims[] = $claim;
    }
    else if($claim['claim_property_id']){                //Your Property
        $property_claims[] = $claim;
    }
    else if('Other' == $claim['claims_type']){                //Your Property
        $other_claims[] = $claim;
    }
     else if($claim['claim_general_liab_id']){            //General Liability / Guest Property
        $liability_claims[] = $claim;
    }
    else if($claim['claim_employment_id']){            //General Liability / Guest Property
        $labor_claims[] = $claim;
    }
}

// whether colapse or expand information box at the top of the page
$infobox = MembersSettings::showInfoBoxFor("claims");
$model = array(
    "claims_start_time" => $startTime,
    "claims_end_time" => $endTime,
    "claims_show_hidden" => $claims_show_hidden,
    "join_licensed_locations_id"=>$filter_licensed_locations_id
);

?>
