<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$note = $db->fetch_one('SELECT * FROM claims_notes WHERE claims_notes_id = ?',array($_REQUEST['claims_notes_id']));
$claims_id = $_REQUEST['claims_id'];
if(!UserPermissions::UserCanAccessObject('claims', $note['join_claims_id'])) {
    http::redirect('../index.php', array('notice' => 'You don\'t have permission to edit that claim'));
}

$query = $db->query('DELETE FROM claims_notes WHERE claims_notes_id = ?',array($_REQUEST['claims_notes_id']));
//pre($_REQUEST);
//pre($query);
if(is_array($query->result)) {
    $notice = 'The note was deleted successfully!';
}
else {
    $notice = 'There was a problem deleting the note.  Please try again later.';
}
if($_REQUEST['redirect'] == 'popup') {
    http::redirect('notes_popup.php'  , array('claims_id' => $claims_id, 'notice' => $notice));
} 
if('claims' == $nav_id)
    http::redirect(BASEURL .'/members/claims/index.php', array('notice' => $notice));
else if($nav_id)
    http::redirect(BASEURL .'/members/claims/edit.php', array('claims_id' => $nav_id, 'notice' => $notice));
else if($claims_id)
    http::redirect(BASEURL .'/members/claims/edit.php', array('claims_id' => $claims_id, 'notice' => $notice));
else
    http::redirect(BASEURL .'/members/claims/index.php', array('notice' => $notice));