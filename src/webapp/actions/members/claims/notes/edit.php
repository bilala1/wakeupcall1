<?
include(ACTIONS . '/members/protect.php');

$claims_note = claims_notes::get_claims_note_with_data($_REQUEST['claims_notes_id']);
$claims_id = $_REQUEST['claims_id'];
$nav_id = $_REQUEST['nav_id'];
$errors = array();

if($_POST)
{
    $data = $_POST;
    unset($data['nav_id']);

    if(!$claims_note['join_claims_id']) {
        $data['join_claims_id'] = $claims_id;
    }

    if($claims_note)
    {
        $claims_notes_id = claims_notes::update($claims_note, $data);
        $notice = 'Note updated!';
    }else
    {
        $claims_notes_id = claims_notes::create($data);
        $notice = 'Note added!';
    }
    http::redirect('notes_popup.php?claims_id='.$claims_id, array('notice' => $notice));
}

?>
