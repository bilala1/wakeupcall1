<?php
include(ACTIONS . '/members/protect.php');

function sort_order()
{
    if(!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC')
        return ' ORDER BY claims_notes_date DESC ';
    else
        return ' ORDER BY claims_notes_date ASC ';
}

$db = mysqli_db::init();

$paging = new paging(25, $_REQUEST['page']);

if($_REQUEST['claims_id'])
{
    $wheres[] = 'join_claims_id = ?';
    $params[] = $_REQUEST['claims_id'];
}

$claim_notes = $db->fetch_all('
    SELECT
    claims_notes_id, join_claims_id, claims_notes_date, claims_notes_subject
    FROM claims_notes
    '.strings::where($wheres).
    sort_order().
    $paging->get_limit()
    , $params
);

$paging->set_total($db->found_rows());
?>
