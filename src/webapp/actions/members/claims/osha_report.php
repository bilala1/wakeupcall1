<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);
include(ACTIONS . '/members/protect.php');
include(ACTIONS . '/members/claims/osha_summary.php');

function my_date_string($month, $year, $end = false)
{
    $day = 1;
    if($end)
    {
        $a_date = date('m/d/Y', strtotime($month . "/1/". $year ));
        $day = date("t", strtotime($a_date));
    }
    return date('m/d/Y', strtotime($month . "/". $day . "/". $year ));
}
function date_string($date)
{
    return $date ? date('m/d/Y', strtotime($date)) : '';
}
function get_days_away($start, $end, $max) {
    if(!$start) {
        return 0;
    }
    if(!$end) {
        // If using today would be over the max, just use today. Otherwise it's tbd;
        $daysSoFar = get_days_away($start, date('m/d/Y'), $max);
        return $daysSoFar == $max ? $max : 'TBD';
    }

    $daysAway = strtotime($end) - strtotime($start);
    $daysAway = ceil($daysAway  /(60*60*24)); //seconds * minutes * hours = days

    return min($max, $daysAway);
}

$start_time = my_date_string($_REQUEST['from_month'], $_REQUEST['from_year'], false);
$end_time = my_date_string($_REQUEST['to_month'], $_REQUEST['to_year'], true);

//get claims
$paging = new paging(PHP_INT_MAX, 1);

$claims = array();

$zipTmpFolder = sys_get_temp_dir() . '/' . uniqid();
mkdir($zipTmpFolder);

$locationIds = $_REQUEST['join_licensed_locations_id'];

if(!ActiveMemberInfo::IsUserMultiLocation()) {
    $locationIds = array(ActiveMemberInfo::SingleLocationUserLocationId());
}

// moving all queries outside the loop
$member = $db->fetch_one('SELECT * from members where members_id = ?', 
                array(ActiveMemberInfo::GetMemberId()));
if($locationIds[0] == 'all'){
    unset($locationIds[0]);
}

$claims = claims::get_paged_claims_list($paging, $start_time, $end_time, null, false, true,
                                     ActiveMemberInfo::GetMemberId(),  $locationIds);
$total_claims = count($claims);

$locations_details = $db->fetch_all('select * from licensed_locations where licensed_locations_id in (' .implode(",", $locationIds) .')');
foreach($locations_details as $locations){
    $location[$locations['licensed_locations_id']] = $locations;
    $summary = new osha_summary($locations, $_REQUEST['to_year'],$total_claims);

    //Write Claims
    foreach ($claims as $claim)
    { 
        if($claim['join_licensed_locations_id'] != $locations['licensed_locations_id']) continue;

    if ($claim['claim_workers_comp_id'] && $claim['claim_workers_comp_osha_include'] == true)
        {
            if(!$claim['claim_workers_comp_employee_name'] || $claim['claim_workers_comp_osha_privacy'] == true)
            {
                $claim['claim_workers_comp_employee_name'] = "";
            }

            $skin = ($claim['claim_workers_comp_illness_type'] == 'Skin Disorder');
            $respiratory = ($claim['claim_workers_comp_illness_type'] == 'Respiratory Condition');
            $poison = ($claim['claim_workers_comp_illness_type'] == 'Poisoning');
            $hearingLoss = ($claim['claim_workers_comp_illness_type'] == 'Hearing Loss');
            $other = !($skin | $respiratory | $poison | $hearingLoss);

            unset($daysAway);
            unset($restrictedDays);
            if($claim['claim_workers_comp_classification_H'])
            {
                $daysAway = get_days_away($claim['claim_workers_comp_datetime_away_start'], $claim['claim_workers_comp_datetime_away_end'], 180);
                $daysLeft = is_numeric($daysAway) ? 180 - $daysAway : 180;
                $restrictedDays = get_days_away($claim['claim_workers_comp_datetime_restricted_start'], $claim['claim_workers_comp_datetime_restricted_end'], $daysLeft);
            }
            elseif($claim['claim_workers_comp_classification_I'])
            {
                $restrictedDays = get_days_away($claim['claim_workers_comp_datetime_restricted_start'], $claim['claim_workers_comp_datetime_restricted_end'], 180);
            }

            $case_number = $claim['claim_workers_comp_case_number']?$claim['claim_workers_comp_case_number']:'';
            if("Injury" == $claim['claim_workers_comp_event_type'])
            {
                $other = false;
                $injury = true;
                
                $summary->add_incident($case_number, $claim['claim_workers_comp_employee_name'],
                    $claim['claim_workers_comp_job_title'], date_string($claim['claims_datetime']),
                    $claim['claim_workers_comp_event_department'], $claim['claims_description'],
                    $daysAway, $restrictedDays, $injury, $skin, $respiratory,$poison, $hearingLoss, $other,
                    $claim['claim_workers_comp_classification_G'],$claim['claim_workers_comp_classification_H'],
                    $claim['claim_workers_comp_classification_I'],$claim['claim_workers_comp_classification_J']);
            }
            else if("Illness" == $claim['claim_workers_comp_event_type'])
            {
                $injury = false;
                $summary->add_incident($case_number, $claim['claim_workers_comp_employee_name'],
                    $claim['claim_workers_comp_job_title'], date_string($claim['claims_datetime']),
                    $claim['claim_workers_comp_event_department'], $claim['claims_description'],
                    $daysAway, $restrictedDays, $injury, $skin, $respiratory,$poison, $hearingLoss, $other,
                    $claim['claim_workers_comp_classification_G'],$claim['claim_workers_comp_classification_H'],
                    $claim['claim_workers_comp_classification_I'],$claim['claim_workers_comp_classification_J']);

            }
        }
    }
        if(count ($locationIds) >1)
        {
            $summary->generate_workbook_to_dir($zipTmpFolder, $locations['licensed_locations_name'], $_REQUEST['to_year']);
            $summary->clean_up();
        }
        else
        {
            $summary->download_workbook($locations['licensed_locations_name'], $_REQUEST['to_year']);
            $summary->clean_up();
        }
    }

if(count ($locationIds) > 1)
    osha_summary::download_zip_archive($zipTmpFolder);
?>