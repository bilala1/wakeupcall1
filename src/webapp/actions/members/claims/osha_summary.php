<?php
include(ACTIONS . '/members/protect.php');
include(LIBS . '/PHPExcel/PHPExcel.php');

abstract class RestrictionType
{
    const Death = 'Death';
    const DaysAway = 'DaysAway';
    const RestrictedCases = 'RestrictedCases';
    const OtherCases = 'OtherCases';
}

final class osha_summary
{
    const SUMMARY_START_ROW = 25;
    const SUMMARY_END_ROW = 37;

    private $mWrittenMembers = 0;
    private $mWorksheetNum = 1;
    private $mEntryNum = 0;
    private $mWorkBook = NULL;
    private $mTemplateWorksheet = NULL;
    private $mOverallTemplateWorksheet = NULL;

    private $mPageTotalDeaths = 0;
    private $mPageTotalDaysAway = 0;
    private $mPageTotalRestrictions = 0;
    private $mPageTotalOtherCases = 0;

    private $mPageTotalInjury = 0;
    private $mPageTotalSkinDisorder = 0;
    private $mPageTotalRespiratoryCondition = 0;
    private $mPageTotalPoisoning = 0;
    private $mPageTotalHearingLoss = 0;
    private $mPageTotalOther = 0;

    private $mOverallTotalDeaths = 0;
    private $mOverallTotalDaysAway = 0;
    private $mOverallTotalRestrictions = 0;
    private $mOverallTotalOtherCases = 0;

    private $mOverallTotalInjury = 0;
    private $mOverallTotalSkinDisorder = 0;
    private $mOverallTotalRespiratoryCondition = 0;
    private $mOverallTotalPoisoning = 0;
    private $mOverallTotalHearingLoss = 0;
    private $mOverallTotalOther = 0;
    private $mLocation = null;
    private $mYear = 2014;
    private $mTotalPages = 1;
    
    function osha_summary($location, $year,$total_claims)
    {
        $this->mLocation = $location;
        $this->mWrittenMembers = 0;
        $this->mYear = $year;
        $this->mTotalPages = ceil($total_claims/15);
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 
            'memoryCacheSize' => '64MB'
        );
        
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        $this->mWorkBook = PHPExcel_IOFactory::load(ACTIONS . '/members/claims/osha_summary.xls');
        
        $this->mTemplateWorksheet = $this->mWorkBook->getSheet(0);
        $this->mOverallTemplateWorksheet = $this->mWorkBook->getSheet(1);

        $this->add_worksheet('OSHA 300 - Page ' . ($this->mWorksheetNum));
    }

    public function add_incident($caseNo, $EmployeeName, $JobTitle, $injuryDate, $injuryLocation,
                                 $injuryDescription, $awayFromWorkDays, $restrictedDays, $injury,
                                 $skinDisorder, $respritoryDisorder, $poisoning, $hearingLoss, $otherIllness,
                                 $classification_g, $classification_h, $classification_i, $classification_j)
    {
        $row = $this->mEntryNum + osha_summary::SUMMARY_START_ROW;

        if($row > osha_summary::SUMMARY_END_ROW)
        {
            $this->add_worksheet('OSHA 300 - Page ' . ($this->mWorksheetNum));
            $row = $this->mEntryNum + osha_summary::SUMMARY_START_ROW;
        }

        $worksheet = $this->mWorkBook->getSheet($this->mWorksheetNum);

        $worksheet->setCellValueByColumnAndRow(0, $row, $caseNo);
        $worksheet->setCellValueByColumnAndRow(1, $row, $EmployeeName);
        $worksheet->setCellValueByColumnAndRow(2, $row, $JobTitle);
        $worksheet->setCellValueByColumnAndRow(3, $row, $injuryDate);
        $worksheet->setCellValueByColumnAndRow(4, $row, $injuryLocation);
        $worksheet->setCellValueByColumnAndRow(5, $row, $injuryDescription);

        if($classification_g)
        {
            $this->mPageTotalDeaths++;
            $this->mOverallTotalDeaths++;
            $worksheet->setCellValueByColumnAndRow(6, $row, 'X');
        }
        if($classification_h)
        {
            $this->mPageTotalDaysAway++;
            $this->mOverallTotalDaysAway++;
            $worksheet->setCellValueByColumnAndRow(7, $row, 'X');
        }
        if($classification_i && ! $classification_h)
        {
            $this->mPageTotalRestrictions++;
            $this->mOverallTotalRestrictions++;
            $worksheet->setCellValueByColumnAndRow(8, $row, 'X');
        }
        if($classification_j)
        {
            $this->mPageTotalOtherCases++;
            $this->mOverallTotalOtherCases++;
            $worksheet->setCellValueByColumnAndRow(9, $row, 'X');
        }

        $worksheet->setCellValueByColumnAndRow(10, $row, $awayFromWorkDays);
        $worksheet->setCellValueByColumnAndRow(11, $row, $restrictedDays);

        if($injury)
        {
            $this->mPageTotalInjury++;
            $this->mOverallTotalInjury++;
            $worksheet->setCellValueByColumnAndRow(12, $row, 'X');
        }
        if($skinDisorder)
        {
            $this->mPageTotalSkinDisorder++;
            $this->mOverallTotalSkinDisorder++;
            $worksheet->setCellValueByColumnAndRow(13, $row, 'X');
        }
        if($respritoryDisorder)
        {
            $this->mPageTotalRespiratoryCondition++;
            $this->mOverallTotalRespiratoryCondition++;
            $worksheet->setCellValueByColumnAndRow(14, $row, 'X');
        }
        if($poisoning)
        {
            $this->mPageTotalPoisoning++;
            $this->mOverallTotalPoisoning++;
            $worksheet->setCellValueByColumnAndRow(15, $row, 'X');
        }
        if($hearingLoss)
        {
            $this->mPageTotalHearingLoss++;
            $this->mOverallTotalHearingLoss++;
            $worksheet->setCellValueByColumnAndRow(16, $row, 'X');
        }
        if($otherIllness)
        {
            $this->mPageTotalOther++;
            $this->mOverallTotalOther++;
            $worksheet->setCellValueByColumnAndRow(17, $row, 'X');
        }

        $this->mEntryNum++;
    }

    public function download_workbook($locationName, $toyear)
    {
        $this->print_totals();
        $this->print_final_summary();
        $this->mWorkBook->removeSheetByIndex(0);
        $this->mWorkBook->removeSheetByIndex(0);

        $outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'oshatmp') .".xlsx";
        $outputWriter = PHPExcel_IOFactory::createWriter($this->mWorkBook, "Excel2007");
        $outputWriter->setPreCalculateFormulas(false);
        $outputWriter->save($outputFileName);

        header('Content-Type: application/xls');
        $locationName = str_replace(" ", "_", $locationName);
        $locationName = str_replace("'", "", $locationName);
        $locationName = str_replace(",", "", $locationName);
        $locationName = str_replace(";", "", $locationName);
        $fileName = "OSHA_300_Log_{$toyear}_{$locationName}.xlsx";
        header("Content-Disposition: attachment; filename={$fileName};");
        header('Content-Length: ' . filesize($outputFileName));

        readfile($outputFileName);
    }

    public function generate_workbook_to_dir($zipTmpFolder, $locationName, $toyear)
    {
        $this->print_totals();
        $this->print_final_summary();
        $this->mWorkBook->removeSheetByIndex(0);
        $this->mWorkBook->removeSheetByIndex(0);
        
        $locationName = str_replace(" ", "_", $locationName);
        $outputFileName = $zipTmpFolder. '/'. $toyear . '_'.$locationName. '.xlsx';
        $outputWriter = PHPExcel_IOFactory::createWriter($this->mWorkBook, "Excel2007");
        $outputWriter->setPreCalculateFormulas(false);
        $outputWriter->save($outputFileName);
        
    }
    
    public static function download_zip_archive($zipTmpFolder)
    {
        $outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'oshaziptmp') .".xlsx";
        $z = new ZipArchive();
        $z->open($outputFileName, ZIPARCHIVE::CREATE);
        $z->addEmptyDir("osha summaries");
        osha_summary::folderToZip($zipTmpFolder, $z, "osha summaries");
        $z->close();
        
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=' . "OSHA_300_Reports.zip" . ';');
        header('Content-Length: ' . filesize($outputFileName));

        readfile($outputFileName);
    }
    
    public static function folderToZip($folder, &$zipFile, $subfolder = null) 
    {
        if ($zipFile == null) {
            // no resource given, exit
            return false;
        }
        // we check if $folder has a slash at its end, if not, we append one
        $folder .= end(str_split($folder)) == "/" ? "" : "/";
        $subfolder .= end(str_split($subfolder)) == "/" ? "" : "/";
        // we start by going through all files in $folder
        $handle = opendir($folder);
        while ($f = readdir($handle)) {
            if ($f != "." && $f != "..") {
                if (is_file($folder . $f)) {
                    // if we find a file, store it
                    // if we have a subfolder, store it there
                    if ($subfolder != null)
                        $zipFile->addFile($folder . $f, $subfolder . $f);
                    else
                        $zipFile->addFile($folder . $f);
                } elseif (is_dir($folder . $f)) {
                    // if we find a folder, create a folder in the zip 
                    $zipFile->addEmptyDir($f);
                    // and call the function again
                    folderToZip($folder . $f, $zipFile, $f);
                }
            }
        }
    }
    
    private function add_worksheet($worksheet_name)
    {
        //reset counters
        if($this->mEntryNum != 0) $this->print_totals();

        $this->mEntryNum = 0;
        $this->mPageTotalDeaths = 0;
        $this->mPageTotalDaysAway = 0;
        $this->mPageTotalRestrictions = 0;
        $this->mPageTotalOtherCases = 0;

        $this->mPageTotalInjury = 0;
        $this->mPageTotalSkinDisorder = 0;
        $this->mPageTotalRespiratoryCondition = 0;
        $this->mPageTotalPoisoning = 0;
        $this->mPageTotalHearingLoss = 0;
        $this->mPageTotalOther = 0;

        //create new sheet
        $newSheet = $this->mTemplateWorksheet->copy();
        $newSheet->setTitle($worksheet_name);
        $generated =  "Generated on ".Date('m/d/Y'). " for the period of ".$_REQUEST['from_month'].'/'.$_REQUEST['from_year'].' through '.$_REQUEST['to_month'].'/'.$_REQUEST['to_year'];
        $newSheet->getCell('A1')->setValue($generated);
        $newSheet->setCellValueByColumnAndRow(13, 3, $this->mYear);
        $newSheet->setCellValueByColumnAndRow(10, 11, $this->mLocation['licensed_locations_name']);
        $newSheet->setCellValueByColumnAndRow(9, 12, $this->mLocation['licensed_locations_city']);
        $newSheet->setCellValueByColumnAndRow(13, 12, $this->mLocation['licensed_locations_state']);
        
        $this->mWorkBook->addSheet($newSheet);
        $this->mWorksheetNum++;
    }

    private function print_final_summary()
    {
        $worksheet = $this->mOverallTemplateWorksheet->copy();
        $worksheet->setTitle('OSHA 300 Summary');
        $this->mWorkBook->addSheet($worksheet);

        $worksheet->setCellValueByColumnAndRow(0, 25, $this->mOverallTotalDeaths);
        $worksheet->setCellValueByColumnAndRow(2, 25, $this->mOverallTotalDaysAway);
        $worksheet->setCellValueByColumnAndRow(4, 25, $this->mOverallTotalRestrictions);
        $worksheet->setCellValueByColumnAndRow(6, 25, $this->mOverallTotalOtherCases);

        $lastPage = $this->mWorksheetNum - 1;
        $worksheet->setCellValueByColumnAndRow(0, 34,
            "=IF({$lastPage}=COUNT('OSHA 300 - Page 1:OSHA 300 - Page {$lastPage}'!K38),SUM('OSHA 300 - Page 1:OSHA 300 - Page {$lastPage}'!K38),\"TBD\")");
        $worksheet->setCellValueByColumnAndRow(4, 34,
            "=IF({$lastPage}=COUNT('OSHA 300 - Page 1:OSHA 300 - Page {$lastPage}'!L38),SUM('OSHA 300 - Page 1:OSHA 300 - Page {$lastPage}'!L38),\"TBD\")");

        $worksheet->setCellValueByColumnAndRow(2, 42, $this->mOverallTotalInjury);
        $worksheet->setCellValueByColumnAndRow(2, 43, $this->mOverallTotalSkinDisorder);
        $worksheet->setCellValueByColumnAndRow(2, 44, $this->mOverallTotalRespiratoryCondition);
        $worksheet->setCellValueByColumnAndRow(6, 42, $this->mOverallTotalPoisoning);
        $worksheet->setCellValueByColumnAndRow(6, 43, $this->mOverallTotalHearingLoss);
        $worksheet->setCellValueByColumnAndRow(6, 44, $this->mOverallTotalOther);
        
        $worksheet->setCellValueByColumnAndRow(35, 2, $this->mYear);
        $worksheet->setCellValueByColumnAndRow(22, 15, $this->mLocation['licensed_locations_name']);
        $worksheet->setCellValueByColumnAndRow(17, 17, $this->mLocation['licensed_locations_address']);
        $worksheet->setCellValueByColumnAndRow(17, 19, $this->mLocation['licensed_locations_city']);
        $worksheet->setCellValueByColumnAndRow(29, 19, $this->mLocation['licensed_locations_state']);
        $worksheet->setCellValueByColumnAndRow(34, 19, $this->mLocation['licensed_locations_zip']);
    }

    private function print_totals()
    {
        $worksheet = $this->mWorkBook->getSheet($this->mWorksheetNum);
        $row = 38;

        $worksheet->setCellValueByColumnAndRow(6, $row, $this->mPageTotalDeaths);
        $worksheet->setCellValueByColumnAndRow(7, $row, $this->mPageTotalDaysAway);
        $worksheet->setCellValueByColumnAndRow(8, $row, $this->mPageTotalRestrictions);
        $worksheet->setCellValueByColumnAndRow(9, $row, $this->mPageTotalOtherCases);
        $worksheet->setCellValueByColumnAndRow(12, $row, $this->mPageTotalInjury);
        $worksheet->setCellValueByColumnAndRow(13, $row, $this->mPageTotalSkinDisorder);
        $worksheet->setCellValueByColumnAndRow(14, $row, $this->mPageTotalRespiratoryCondition);
        $worksheet->setCellValueByColumnAndRow(15, $row, $this->mPageTotalPoisoning);
        $worksheet->setCellValueByColumnAndRow(16, $row, $this->mPageTotalHearingLoss);
        $worksheet->setCellValueByColumnAndRow(17, $row, $this->mPageTotalOther);
        //page no
        $worksheet->setCellValueByColumnAndRow(10, 46, ($this->mWorksheetNum - 1).' of '.$this->mTotalPages);
    }
    
    public function clean_up()
    {
        //$this->mWorkBook->disconnectWorksheets();
    }
}
?>