<?php
include(ACTIONS . '/members/protect.php');
include(LIBS . '/PHPExcel/PHPExcel.php');

function date_string($date)
{
    return $date ? date('m/d/Y', strtotime($date)) : '';
}

function setup_worksheet($worksheetObj)
{
    $worksheetObj->getColumnDimension('A', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('B', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('C', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('D', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('E', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('F', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('G', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('H', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('I', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('J', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('K', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('L', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('M', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('N', true)->setAutoSize(true);
}

function merge_item_columns($worksheetObj, $col_start, $col_end, $row_start, $row_end)
{
    for($i = $col_start; $i <= $col_end; $i++)
    {
        $worksheetObj->mergeCellsByColumnAndRow($i, $row_start, $i, $row_end);
    }
}

function apply_border($worksheetObj, $base_row, $merge_end, $end_col)
{
    for($col = 0; $col <= $end_col; $col++)
    {
        if($col <= $end_col - 2)
        {
            $worksheetObj->getStyleByColumnAndRow($col, $base_row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $worksheetObj->getStyleByColumnAndRow($col, $merge_end)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            
            for($row = $base_row; $row <= $merge_end; $row++)
            {
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getLeft() ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            }
        }
        else 
        {
            for($row = $base_row; $row <= $merge_end; $row++)
            {
                if($row == $base_row)
                {
                    $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                }
                if($row == $merge_end)
                {
                    $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                }
                
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getLeft() ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            }
        }
    }
}

function write_workers_comp_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Claimant");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Injury Type");

    $worksheetObj->setCellValueByColumnAndRow(4, 1, "Body Part");

    $worksheetObj->setCellValueByColumnAndRow(5, 1, "Dept.");

    $worksheetObj->setCellValueByColumnAndRow(6, 1, "Status");

    $worksheetObj->setCellValueByColumnAndRow(7 ,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow(8 ,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow(9, 1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow(10, 1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow(11, 1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow(12, 1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow(13, 1, "History");

    $worksheetObj->getStyle("A1:N1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:N1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:N1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_workers_comp_item($worksheetObj, $rowIndex, $merge_end, $claim)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $claim['licensed_locations_name']);
   
    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, date_string($claim['claims_datetime']));

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $claim['claim_workers_comp_employee_name']);
    
    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $claim['claim_workers_comp_injury_type']);
    
    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, $claim['claim_workers_comp_body_parts']);
    
    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, $claim['claim_workers_comp_department']);
    
    $worksheetObj->setCellValueByColumnAndRow(6, $rowIndex, $claim['claims_status_code_description']);
    
    $worksheetObj->setCellValueByColumnAndRow(7, $rowIndex, date_string($claim['claims_datetime_submitted']));
    
    $worksheetObj->setCellValueByColumnAndRow(8, $rowIndex, date_string($claim['claims_datetime_closed']));
    
    $worksheetObj->setCellValueByColumnAndRow(9, $rowIndex, $claim['claims_amount_paid']);
    
    $worksheetObj->setCellValueByColumnAndRow(10, $rowIndex, $claim['claims_amount_reserved']);
    
    $worksheetObj->setCellValueByColumnAndRow(11, $rowIndex, $claim['claims_amount_incurred']);

    merge_item_columns($worksheetObj, 0, 11, $rowIndex, $merge_end);
}

function write_general_liab_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Claimant");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Area of Loss");

    $worksheetObj->setCellValueByColumnAndRow(4, 1, "Type of Loss");

    $worksheetObj->setCellValueByColumnAndRow(5, 1, "Status");

    $worksheetObj->setCellValueByColumnAndRow(6 ,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow(7 ,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow(8, 1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow(9, 1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow(10, 1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow(11, 1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow(12, 1, "History");

    $worksheetObj->getStyle("A1:M1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:M1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:M1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_general_liab_item($worksheetObj, $rowIndex, $merge_end, $claim)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $claim['licensed_locations_name']);

    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, date_string($claim['claims_datetime']));

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $claim['claim_general_liab_claimant']);

    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $claim['claim_general_liab_loss_area']);

    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, $claim['claim_general_liab_loss_type']);

    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, $claim['claims_status_code_description']);

    $worksheetObj->setCellValueByColumnAndRow(6, $rowIndex, date_string($claim['claims_datetime_submitted']));

    $worksheetObj->setCellValueByColumnAndRow(7, $rowIndex, date_string($claim['claims_datetime_closed']));

    $worksheetObj->setCellValueByColumnAndRow(8, $rowIndex, $claim['claims_amount_paid']);

    $worksheetObj->setCellValueByColumnAndRow(9, $rowIndex, $claim['claims_amount_reserved']);

    $worksheetObj->setCellValueByColumnAndRow(10, $rowIndex, $claim['claims_amount_incurred']);
    
    merge_item_columns($worksheetObj, 0, 10, $rowIndex, $merge_end);
}

function write_auto_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Claimant");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Nature of Claim");

    $worksheetObj->setCellValueByColumnAndRow(4, 1, "Status");

    $worksheetObj->setCellValueByColumnAndRow(5 ,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow(6 ,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow(7, 1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow(8, 1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow(9, 1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow(10, 1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow(11, 1, "History");

    $worksheetObj->getStyle("A1:L1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:L1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_auto_item($worksheetObj, $rowIndex,$merge_end, $claim)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $claim['licensed_locations_name']);

    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, date_string($claim['claims_datetime']));

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $claim['claim_auto_claimant']);

    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $claim['claim_auto_nature']);

    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, $claim['claims_status_code_description']);

    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, date_string($claim['claims_datetime_submitted']));

    $worksheetObj->setCellValueByColumnAndRow(6, $rowIndex, date_string($claim['claims_datetime_closed']));

    $worksheetObj->setCellValueByColumnAndRow(7, $rowIndex, $claim['claims_amount_paid']);

    $worksheetObj->setCellValueByColumnAndRow(8, $rowIndex, $claim['claims_amount_reserved']);

    $worksheetObj->setCellValueByColumnAndRow(9, $rowIndex, $claim['claims_amount_incurred']);
    
    merge_item_columns($worksheetObj, 0, 9, $rowIndex, $merge_end);
}

function write_property_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Type of Loss");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Status");

    $worksheetObj->setCellValueByColumnAndRow(4 ,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow(5 ,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow(6, 1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow(7, 1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow(8, 1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow(9, 1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow(10, 1, "History");

    $worksheetObj->getStyle("A1:K1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:K1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:K1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_property_item($worksheetObj, $rowIndex, $merge_end, $claim)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $claim['licensed_locations_name']);

    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, date_string($claim['claims_datetime']));

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $claim['claim_property_loss_type']);

    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $claim['claims_status_code_description']);

    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, date_string($claim['claims_datetime_submitted']));

    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, date_string($claim['claims_datetime_closed']));

    $worksheetObj->setCellValueByColumnAndRow(6, $rowIndex, $claim['claims_amount_paid']);

    $worksheetObj->setCellValueByColumnAndRow(7, $rowIndex, $claim['claims_amount_reserved']);

    $worksheetObj->setCellValueByColumnAndRow(8, $rowIndex, $claim['claims_amount_incurred']);
    
    merge_item_columns($worksheetObj, 0, 8, $rowIndex, $merge_end);
}
function write_labor_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Claimant");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Incident Location");

    $worksheetObj->setCellValueByColumnAndRow(4, 1, "Status");

    $worksheetObj->setCellValueByColumnAndRow(5 ,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow(6 ,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow(7, 1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow(8, 1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow(9, 1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow(10, 1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow(11, 1, "History");

    $worksheetObj->getStyle("A1:L1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:L1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}
function write_labor_item($worksheetObj, $rowIndex,$merge_end, $claim)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $claim['licensed_locations_name']);

    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, date_string($claim['claims_datetime']));

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $claim['claim_employment_claimant']);

    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $claim['claim_employment_incident_location']);

    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, $claim['claims_status_code_description']);

    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, date_string($claim['claims_datetime_submitted']));

    $worksheetObj->setCellValueByColumnAndRow(6, $rowIndex, date_string($claim['claims_datetime_closed']));

    $worksheetObj->setCellValueByColumnAndRow(7, $rowIndex, $claim['claims_amount_paid']);

    $worksheetObj->setCellValueByColumnAndRow(8, $rowIndex, $claim['claims_amount_reserved']);

    $worksheetObj->setCellValueByColumnAndRow(9, $rowIndex, $claim['claims_amount_incurred']);
    
    merge_item_columns($worksheetObj, 0, 9, $rowIndex, $merge_end);
}
function write_notes($worksheetObj, $col_index, $rowIndex, $claim)
{
    $claim_notes = claims_notes::get_claims_notes_with_data($claim['claims_id']);

    $merge_end = $rowIndex;
    foreach($claim_notes as $note)
    {
        $claim_note = new PHPExcel_RichText();
        $objDate = $claim_note->createTextRun(date_string($note['claims_notes_date']). ' '. $note['claims_notes_subject'] . ': ');
        $objDate->getFont()->setBold(true);
        
        $objText = $claim_note->createTextRun($note['claims_notes_note']);
        $objText->getFont()->setBold(false);

        $claim_note_text = $objDate->getText() . $objText->getText();
        $worksheetObj->setCellValueByColumnAndRow($col_index, $merge_end, strip_tags($claim_note_text));
        $merge_end++;
    }
    if($merge_end == $rowIndex) $merge_end++;
    return $merge_end;
}

function write_histories($worksheetObj, $col_index, $rowIndex, $claim)
{
    $claim_histories = claims_history::get_claims_history_with_data(ActiveMemberInfo::GetMemberId(), $claim['claims_id']);
    $claim_history = new PHPExcel_RichText();

    $merge_end = $rowIndex;
    foreach($claim_histories as $history)
    {
        $objHistDate = $claim_history->createTextRun(date_string($history['claims_history_datetime']) . ": ");
        $objHistDate->getFont()->setBold(true);

        $objHistText = $claim_history->createTextRun($history['claims_history_name']);
        $objHistText->getFont()->setBold(false);
        
        $claim_history_text = $objHistDate->getText() . $objHistText->getText();
        
        $worksheetObj->setCellValueByColumnAndRow($col_index, $merge_end, strip_tags($claim_history_text));
        $merge_end++;
    }
    if($merge_end == $rowIndex) $merge_end++;
    return $merge_end;
}

function write_notes_and_history($worksheetObj, $notes_col_index, $history_col_index, $rowIndex, $claim)
{
    $notes_merge_end = write_notes($worksheetObj, $notes_col_index, $rowIndex, $claim);
    $history_merge_end = write_histories($worksheetObj, $history_col_index, $rowIndex, $claim);
    
    if($notes_merge_end > $history_merge_end)
    {
        $merge_start = $history_merge_end - 1;
        $worksheetObj->mergeCellsByColumnAndRow($history_col_index, $merge_start, $history_col_index, $notes_merge_end);
        return $notes_merge_end;
    }
    else if($notes_merge_end < $history_merge_end)
    {  
        $merge_start = $notes_merge_end - 1;
        $worksheetObj->mergeCellsByColumnAndRow($notes_col_index, $merge_start, $notes_col_index, $history_merge_end);
        return $history_merge_end;
    }
    
    return $notes_merge_end;
}

$paging = new paging(35, $_REQUEST['page']);

$objPHPExcel = new PHPExcel();

$worksheetObj = $objPHPExcel->getSheet(0);

$worksheetObj->setShowGridlines(false);
include '_determine_claims_params.php';

//get claims
$claims = claims::get_paged_claims_list($paging, $startTime, $endTime,
                    $_REQUEST['order'], $claims_show_hidden, $claims_show_visible, 
                    ActiveMemberInfo::GetMemberId(), $filter_licensed_locations_id,$join_claims_status_codes_id);

//row starts at 2 for the header in 1
$rowIndex = 2;

if ($_REQUEST['report_type'] == 'Workers Comp')
{ //Workers Comp
    write_workers_comp_header($worksheetObj, $rowIndex, $claim);
}
else if ($_REQUEST['report_type'] == 'General Liability / Guest Property' || $_REQUEST['report_type'] == 'Other')
{ //General Liability / Guest Property
    write_general_liab_header($worksheetObj, $rowIndex, $claim);
}
else if ($_REQUEST['report_type'] == 'Auto')
{ //Auto
    write_auto_header($worksheetObj, $rowIndex, $claim);
}
else if ($_REQUEST['report_type'] == 'Your Property')
{ //Your Property
    write_property_header($worksheetObj, $rowIndex, $claim);
}
else if ($_REQUEST['report_type'] == 'Labor')
{ //labor claims
    write_labor_header($worksheetObj, $rowIndex, $claim);
}

setup_worksheet($worksheetObj);
$lfcr = "\n";

//Write Claims
foreach ($claims as $claim)
{
    $claim['claims_filed'] = $claim['claims_filed'] == 1 ? 'Yes' : 'No';
    $claim['claims_amount_incurred'] = $claim['claims_amount_paid'] + $claim['claims_amount_reserved'];
    
    if ($claim['claim_workers_comp_id'] && stripslashes($_REQUEST['report_type']) == 'Workers Comp')
    { //Workers Comp
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        $merge_end = write_notes_and_history($worksheetObj, 12, 13, $rowIndex, $claim);
        write_workers_comp_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 13);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
    else if ($claim['claim_general_liab_id']  && $claim['claims_type'] != "Other" && $_REQUEST['report_type'] == 'General Liability / Guest Property')
    { //General Liability / Guest Property
        $merge_end = write_notes_and_history($worksheetObj, 11, 12, $rowIndex, $claim);
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        write_general_liab_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 12);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
    else if ($claim['claim_auto_id'] && $_REQUEST['report_type'] == 'Auto')
    { //Auto
        $merge_end = write_notes_and_history($worksheetObj, 10, 11, $rowIndex, $claim);
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        write_auto_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 11);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
    else if ($claim['claim_property_id']&& $_REQUEST['report_type'] == 'Your Property')
    { //Your Property
        $merge_end = write_notes_and_history($worksheetObj, 9, 10, $rowIndex, $claim);
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        write_property_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 10);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
    else if ($claim['claims_type'] == "Other" && $_REQUEST['report_type'] == 'Other')
    { //General Liability / Guest Property
        $merge_end = write_notes_and_history($worksheetObj, 11, 12, $rowIndex, $claim);
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        write_general_liab_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 12);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
    else if ($claim['claims_type'] == "Labor Law" && $_REQUEST['report_type'] == 'Labor')
    { //Labor
        $merge_end = write_notes_and_history($worksheetObj, 11, 12, $rowIndex, $claim);
        $claims_date = claims::get_claims_x_carriers_dates($claim['claims_id']);
        $claim['claims_datetime_submitted'] = $claims_date[0];
        $claim['claims_datetime_closed'] = $claims_date[1];
        write_labor_item($worksheetObj, $rowIndex, $merge_end, $claim);
        apply_border($worksheetObj, $rowIndex, $merge_end, 12);
        $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
        $rowIndex = $merge_end + 1;
    }
}
$cellRange = 'A0' . ':O' . $rowIndex;
$worksheetObj->getStyle($cellRange)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'phpxltmp') .".xlsx";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$objWriter->save($outputFileName);

header('Content-Type: application/xls');
header('Content-Disposition: attachment; filename=' . "report.xlsx" . ';');
header('Content-Length: ' . filesize($outputFileName));

readfile($outputFileName);
?>