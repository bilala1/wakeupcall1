<?
$includeHidden = !is_null($_GET['includeHidden']); // includeHidden will only be set if checked
if($_GET['join_claims_status_codes_id'] == 'all'){
    $_GET['join_claims_status_codes_id']='';
}
$areasOfLoss = claims::get_gen_claim_by_area_of_loss($_GET['licensed_locations_id'], $_GET['claim_type'], $_GET['claims_start_time_dialog'], $_GET['claims_end_time_dialog'], $includeHidden,$_GET['join_claims_status_codes_id']);

$response = Array();
$response["title"] = reportHelpers::getTitle(urldecode($_GET['title']),
    $_GET['claims_start_time_dialog'],
    $_GET['claims_end_time_dialog'],
    $_GET['licensed_locations_id'],$includeHidden);

$data = Array();
$data["cols"] = Array(
    Array("label" => "Area of Loss", "type" => "string"),
    Array("label" => "Claim Count", "type" => "number"),
);
$rows = Array();
$maxH = 0;
$areas = [];
foreach($areasOfLoss as $claimAreaOfLoss) {
    foreach (explode(',',$claimAreaOfLoss) as $areaOfLoss) {
        if (!array_key_exists($areaOfLoss, $areas)) {
            $areas[$areaOfLoss] = 0;
        }
        $areas[$areaOfLoss]++;
    }
}
foreach($areas as $area => $count){
    $rows[] = Array("c" => Array(Array("v" => $area),
        Array("v" => $count)));
    $maxH = max($maxH, $ount);
}
$data["rows"] = $rows;
$response["data"] = $data;
$response["maxH"] = $maxH;
echo json_encode($response);