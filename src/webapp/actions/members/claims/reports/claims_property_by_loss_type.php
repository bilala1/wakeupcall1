<?
$includeHidden = !is_null($_GET['includeHidden']); // includeHidden will only be set if checked
if($_GET['join_claims_status_codes_id'] == 'all'){
    $_GET['join_claims_status_codes_id']='';
}
$claims = claims::get_property_by_loss($_GET['licensed_locations_id'], $_GET['claims_start_time_dialog'], $_GET['claims_end_time_dialog'], $includeHidden,$_GET['join_claims_status_codes_id']);

$response = Array();
$response["title"] = reportHelpers::getTitle(urldecode($_GET['title']),
    $_GET['claims_start_time_dialog'],
    $_GET['claims_end_time_dialog'],
    $_GET['licensed_locations_id'],$includeHidden);

$data = Array();
$data["cols"] = Array(
    Array("label" => "Loss Type", "type" => "string"),
    Array("label" => "Claim Count", "type" => "number"),
);
$rows = Array();
$maxH = 0;
foreach($claims as $claim) {
    $rows[] = Array("c" => Array(Array("v" => $claim["claim_property_loss_type"]),
        Array("v" => $claim["claimCount"])));
    $maxH = max($maxH, $claim["claimCount"]);
}
$data["rows"] = $rows;
$response["data"] = $data;
$response["maxH"] = $maxH;
echo json_encode($response);