<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/8/15
 * Time: 11:49 AM
 *
 * this will return the reports for the given claim type.
 *
 * Input: $_GET['claimType'] the type of the claim, e.g. Worker's Comp, General, etc.
 * Will return list list of reports for that claim type
 *
 */
include MODELS. '/members/claims/claims_reports.php';

$multiLocation = ActiveMemberInfo::IsUserMultiLocation();

if ( !array_key_exists('claimType',$_GET) ) {
    throw new Exception("claimType not set");
}
$claimType = ClaimReportTypes::getClaimType($_GET['claimType']);
if ( $claimType < 0 ) {
    throw new Exception($_GET['claimType']." claimType did not resolve");
}

$reportList = Array();
foreach(ClaimReports::$list as $key => $report) {
    if ( $multiLocation == false && $report['corpOnly'] == true) {
        continue;
    }
    if ($report['reportType'] == $claimType) {
        $report['reportId'] = $key;
        $reportList[] = $report;
    }
}

echo json_encode($reportList);