<?
$tempDirectory = '/tmp';
if (!file_exists($tempDirectory)) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error--directory '.$tempDirectory.' does not exist.', true, 500);
    return;
}
$filename = tempnam($tempDirectory,'chart-').'.png';
$data = explode(",",$_POST["image"]);
$encodedData = str_replace(' ','+',$data[1]);
file_put_contents($filename,base64_decode($encodedData));
echo basename($filename);