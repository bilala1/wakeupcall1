<?php

include(ACTIONS . '/members/protect.php');

if($_POST)
{
    $data = $_POST;

    $validate = new validate($data);
    $errors = $validate
        ->setOptional('message')
        ->test();

    if(!$errors){
        $from     = AUTO_EMAIL;
        $to       = SALES_EMAIL;
        $subject  = "New Contact Inquiry - " . SITE_NAME;
        $message  = "

        <b>First Name:</b> {$data['firstname']} <br />
        <b>Last Name:</b> {$data['lastname']} <br />
        <b>Email:</b> {$data['email']} <br />
        <b>Message:</b> {$data['message']}<br />
        ";

        mail::send($from, $to, $subject, $message);

		notify::david($subject, $message);

        http::redirect(BASEURL . '/members/contact-thankyou.php');
    }
}

?>
