<?php

if (array_key_exists('order', $_REQUEST)) {
    $order = $_REQUEST['order'];
    $_SESSION['contracts_list_order'] = $_REQUEST['order'];
} else if (array_key_exists('contracts_list_order', $_SESSION)) {
    $order = $_SESSION['contracts_list_order'];
}

if ($_REQUEST['clear']) {
    $_REQUEST['join_licensed_locations_id'] = array();
    $_REQUEST['contracts_start_time'] = '';
    $_REQUEST['contracts_end_time'] = '';
    $_REQUEST['quick_filter'] = '';
    $_REQUEST['contracts_show_hidden'] = '0';
}

if (array_key_exists('join_licensed_locations_id', $_REQUEST) || $_REQUEST['filter']) {
    $filter_licensed_locations_id = $_REQUEST['join_licensed_locations_id'];
    if(in_array('all', $filter_licensed_locations_id)) {
        $filter_licensed_locations_id = null;
    }
    $_SESSION['contracts_list_licensed_locations_id'] = $filter_licensed_locations_id;
} else if (array_key_exists('contracts_list_licensed_locations_id', $_SESSION)) {
    $filter_licensed_locations_id = $_SESSION['contracts_list_licensed_locations_id'];
}

if ($_REQUEST['quick_filter'] && $_REQUEST['quick_filter'] != '') {
    $_REQUEST['contracts_start_time'] = date('m/d/Y');
    $days = $_REQUEST['quick_filter'];
    $_REQUEST['contracts_end_time'] = date('m/d/Y', strtotime("+$days days"));;
}

// If the start_time param was passed, we need to use it (even if empty)
$contracts_start_time = '';
if (array_key_exists('contracts_start_time', $_REQUEST)) {
    if (strtotime($_REQUEST['contracts_start_time'])) {
        $contracts_start_time = $_REQUEST['contracts_start_time'];
    } else {
        $contracts_start_time = '';
    }
    $_SESSION['contracts_start_time'] = $contracts_start_time;
} else if (array_key_exists('contracts_start_time', $_SESSION)) {
    $contracts_start_time = $_SESSION['contracts_start_time'];
}

// If the end_time param was passed, we need to use it (even if empty)
$contracts_end_time = '';
if (array_key_exists('contracts_end_time', $_REQUEST)) {
    if (strtotime($_REQUEST['contracts_end_time'])) {
        $contracts_end_time = $_REQUEST['contracts_end_time'];
    } else {
        $contracts_end_time = '';
    }
    $_SESSION['contracts_end_time'] = $contracts_end_time;
} else if (array_key_exists('contracts_end_time', $_SESSION)) {
    $contracts_end_time = $_SESSION['contracts_end_time'];
}

if (array_key_exists('contracts_show_hidden', $_REQUEST)) {
    $_SESSION['contracts_show_hidden'] = $_REQUEST['contracts_show_hidden'];
    $contracts_show_hidden = $_REQUEST['contracts_show_hidden'];
} else if (array_key_exists('contracts_show_hidden', $_SESSION)) {
    $contracts_show_hidden = $_SESSION['contracts_show_hidden'];
} else {
    $contracts_show_hidden = $_SESSION['contracts_show_hidden'] = 0;
}
