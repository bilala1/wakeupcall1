<?php
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$file = $db->fetch_one('SELECT * FROM contracts_files INNER JOIN files on files_id = join_files_id WHERE contracts_files_id = ?',
    array($_REQUEST['contracts_files_id']));
$contract_id = $file['join_contracts_id'];
$contract = contracts::get_contract($contract_id);
$perms = UserPermissions::UserObjectPermissions('contracts', $contract);

if ($contract && $perms['delete']) {

    $deleteSuccess = Services::$fileSystem->deleteFile($file['files_id']);
    if ($deleteSuccess) {
        $query = $db->query('DELETE FROM contracts_files WHERE contracts_files_id = ?',
            array($file['contracts_files_id']));
        $deleteSuccess = is_array($query->result);
    }

    if ($deleteSuccess) {
        contracts_history::file_deleted_entry(ActiveMemberInfo::GetMemberId(), $file['join_contracts_id'], $file['files_name']);

        http::redirect(BASEURL . '/members/contracts/edit.php?contracts_id=' . $contract_id . '&notice=' . urlencode('The file was deleted successfully!'));
    } else {
        http::redirect(BASEURL . '/members/contracts/edit.php?contracts_id=' . $contract_id . '&notice=' . urlencode('There was a problem deleting the file.  Please try again later.'));
    }
} else {
    http::redirect(BASEURL . '/members/contracts/index.php?notice=' . urlencode('You don\'t have permission to delete this file.'));
}
?>
