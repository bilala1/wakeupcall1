<?
use Services\Services;

include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();

$contract_id = $_REQUEST['contracts_id'];
$contract = contracts::get_contract($contract_id);
$perms = UserPermissions::UserObjectPermissions('contracts', $contract);

if ($contract && $perms['delete']) {

    $contracts_table = new mysqli_db_table('contracts');
    $contracts_table->delete($contract['contracts_id']);

    $db->query('DELETE
                    FROM contracts_x_licensed_locations
                    WHERE join_contracts_id = ? ', array($contract['contracts_id']));

    $db->query('DELETE
                    FROM contracts_history
                    WHERE join_contracts_id = ? ', array($contract['contracts_id']));

    //contract files to be deleted
    $contracts_files = $db->fetch_all('SELECT *
                                              FROM contracts_files
                                              INNER JOIN files ON files_id = join_files_id
                                              WHERE join_contracts_id = ? ', array($contract['contracts_id']));
    foreach ($contracts_files as $file) {
        Services::$fileSystem->deleteFile($file['files_id']);
        $query = $db->query('DELETE FROM contracts_files WHERE contracts_files_id = ?',
            array($file['contracts_files_id']));
    }

    $notice = 'The contract has been deleted';
} else {
    $notice = 'You do not have permissions to delete this contract.';
}

http::redirect(BASEURL . '/members/contracts/index.php', array('notice' => $notice));

?>