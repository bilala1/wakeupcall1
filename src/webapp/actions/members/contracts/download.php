<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if (isset($_REQUEST['contracts_files_id'])) {
    $file = $db->fetch_one('
        SELECT *
        FROM contracts_files
        INNER JOIN files ON join_files_id = files_id
        INNER JOIN contracts ON contracts_id = join_contracts_id
        WHERE
            contracts_files_id = ? AND ' . UserPermissions::CreateInClause('contracts', array('checkView' => true)),
        array($_REQUEST['contracts_files_id']
        ));
    //pre($db->debug()); exit;
    if ($file) {
        members::log_action(ActiveMemberInfo::GetAccountId(), 'Downloaded from Contracts Files: ' . $file['files_name']);

        $downloads = new mysqli_db_table('downloads');
        //record download
        $downloads->insert(array(
            'downloads_type' => 'contract',
            'join_id' => $_REQUEST['contracts_files_id'],
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'downloads_datetime' => SQL('NOW()')
        ));

        $redirectUrl = Services::$fileSystem->getFileDownloadUrl($file['files_id']);

        if(!$redirectUrl) {
            http::redirect(BASEURL . '/members/contracts/index.php',
                array('notice' => 'You are unable to view this document'));
        } else {
            http::redirect($redirectUrl);
        }
    } else {
        http::redirect(BASEURL . '/members/contracts/index.php', array('notice' => 'You are unable to view this document'));
    }
}
?>
