<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

function notes_sort_helper()
{
    if (!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC')
        return ' ORDER BY contracts_notes_date DESC ';
    else
        return ' ORDER BY contracts_notes_date ASC ';
}

$db = mysqli_db::init();

$contract = contracts::get_contract($_REQUEST['contracts_id']);
if($contract) {
    $permissions = UserPermissions::UserObjectPermissions('contracts', $contract);
    $contractFiles = contracts::get_files($contract['contracts_id']);
}
$licensed_locations = arrays::sort(licensed_locations::get_licensed_locations_with_permission('contracts'));
foreach($licensed_locations as $licensed_locations){
    $licensed_locations_array[$licensed_locations['licensed_locations_id']] = $licensed_locations['licensed_locations_name'];
}
if($_REQUEST['contracts_id'] && (!$contract || !$permissions['edit'])) {
    http::redirect(BASEURL .'/members/contracts/index.php', array('notice' => 'You don\'t have permission to edit that contract'));
}
$remind_expired_members = contracts::get_expired_remind_members($_REQUEST['contracts_id']);
$contracts_milestones_existing = contracts::get_contracts_milestones($_REQUEST['contracts_id']);
$contracts_milestones_remind_members = array();
$existing_milestone_ids = array();
foreach($contracts_milestones_existing as $contracts_milestone){
    $existing_milestone_ids[] = $contracts_milestone['contracts_milestones_id'];
    $contracts_milestones['contracts_milestones_remind_days'] = contracts::get_contracts_milestones_remind_days($contracts_milestone['contracts_milestones_id']);
    $contracts_milestones['contracts_milestones_remind_members'] = contracts::get_contracts_milestones_remind_members($contracts_milestone['contracts_milestones_id']);
    $contracts_milestones_remind_members[$contracts_milestone['contracts_milestones_id']] = $contracts_milestones['contracts_milestones_remind_members'];
    $contracts_milestones['contracts_milestones_additional_recipients'] = contracts::get_contracts_milestones_additional_recipients($contracts_milestone['contracts_milestones_id']);
}
$existing_milestone_count = count($contracts_milestones_existing);
$contracts_status_values = array(
    '' => 'Please Select...',
    'Active' => 'Active',
    'Month to Month' => 'Month to Month',
    'Autorenew-Annual'=>'Auto Renew - Annual',
    'Expired' => 'Expired',
    'Inactive' => 'Inactive');

$contracts_transition_status_values = array(
    '' => 'Please Select...',
    'Month to Month' => 'Month to Month',
    'Autorenew-Annual'=>'Auto Renew - Annual',
    'Expired' => 'Expired',
    'Inactive' => 'Inactive');

$contracts_reminder_times = array(
    '' => 'Please Select...',
    '30' => '30 days',
    '60' => '60 days',
    '90' => '90 days',
    '120' => '120 days',
    '180' => '180 days');

if ($contract && $_REQUEST['contracts_id']) {
    $contract_files = $db->fetch_all('SELECT * FROM contracts_files INNER JOIN files on join_files_id = files_id WHERE join_contracts_id = ?',
        array($_REQUEST['contracts_id']));
    //pre($contract_files);

    if ($contract['contracts_all_locations']) {
        $contract['join_licensed_locations'] = array('all');
    }

    $additional_recipients = $db->fetch_all('SELECT * FROM contracts_additional_recipients WHERE join_contracts_id = ?',
        array($_REQUEST['contracts_id']));
}

$errors = array();

if ($_POST) {
    $data = $_POST;
//print_r($data);exit;
    $validate = new validate($data);

    //set initial validation rules
    $validate
        ->setAllOptional()
        ->unsetOptionals(array(
            'contracts_effective_date' => 'Effective Date',
            'contracts_status' => 'Status',
            'contracts_contracted_party_name' => 'Contracted Party'
        ))
        ->setErrors(array(
            'contracts_effective_date' => 'Please enter the effective date of the contract',
            'join_licensed_locations' => 'Please select at least one location'
        ))
        ->addMultiLocationSelectionValidation($data, 'contract', $contract);

    $uploaded_files = files::fixUploadFileArray($_FILES['contracts_file']);

    $validate->setFileType('both');
    foreach ($uploaded_files as $file) {
        if ($file['name'] == '') {
            continue;
        }

        $validate->setFile($file, 'contracts_file');
    }

    $errors += $validate->test();
    if (!$errors) {
        $contracts = new mysqli_db_table('contracts');
        $contracts_files = new mysqli_db_table('contracts_files');
        $contract_remind_members = new mysqli_db_table('contract_remind_members');
        $contracts_x_licensed_locations = new mysqli_db_table('contracts_x_licensed_locations');
        $contracts_additional_recipients = new mysqli_db_table('contracts_additional_recipients');

        $data['contracts_effective_date'] = date('Y-m-d', strtotime($data['contracts_effective_date']));
        $data['contracts_expiration_date'] = empty($data['contracts_expiration_date']) ? null : date('Y-m-d', strtotime($data['contracts_expiration_date']));
        $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();

        $data_files = array();

        //loop through number of uploaded files
        foreach ($uploaded_files as $index => $file) {
            if ($file['size']) {
                $file_entry = Services::$fileSystem->storeFile($file, 'contracts');

                if(!$file_entry) {
                    $errors['contracts_file'] = 'There was a problem uploading a file';
                    return;
                }

                $file_entry['join_files_id'] = $file_entry['files_id'];
                $data_files[] = $file_entry;
            }
        }
        $remind_members = array();
        for($i=0;$i<count($data['contracts_expired']) ; $i++){
            $remind_members[$i]['join_members_id'] = $data['contracts_expired'][$i];
            $remind_members[$i]['contracts_remind_expiry'] = 1;
        }
        if ($contract) {
            $old_contract = contracts::get_contract($contract['contracts_id']);
            //contracts table
            $contracts->update($data, $contract['contracts_id']);
            $contracts_id = $contract['contracts_id'];
            if($data['contracts_expired_notification'] == 1){
                contracts::delete_remind_members_by_contracts_id($contracts_id);
                if(!empty($remind_members)){
                    foreach($remind_members as $remind_member){
                        $remind_member['join_contracts_id']= $contracts_id;
                        $contract_remind_members->insert($remind_member);
                    }
                }
            }

            //clean up old things
            // If this was a single location user, there is nothing to do here - they can't add or remove any other locations
            if(ActiveMemberInfo::IsUserMultiLocation()) {
                $in_clause = implode(', ', UserPermissions::LocationIdsWithPermission('contracts'));
                $db->query('DELETE FROM contracts_x_licensed_locations
                              WHERE join_contracts_id = ?
                                AND join_licensed_locations_id in ('.$in_clause.')',
                    array($contracts_id));

                foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                    $contract_x_licensed_location['join_contracts_id'] = $contracts_id;
                    $contract_x_licensed_location['join_licensed_locations_id'] = $licensed_locations_id;
                    $contracts_x_licensed_locations->insert($contract_x_licensed_location);
                }
            }


            $db->query('DELETE FROM contracts_additional_recipients WHERE join_contracts_id = ?', array($contracts_id));
            foreach ($data['contracts_additional_recipients_email'] as $contracts_additional_recipient_email) {
                if(!$contracts_additional_recipient_email) {
                    continue;
                }
                $additional_recipient['join_contracts_id'] = $contracts_id;
                $additional_recipient['contracts_additional_recipients_email'] = $contracts_additional_recipient_email;
                $contracts_additional_recipients->insert($additional_recipient);
            }
            
            $new_contract = contracts::get_contract($contract['contracts_id']);

            contracts_history::contract_updated_entry(ActiveMemberInfo::GetMemberId(), $new_contract, $old_contract);

            //notice
            $notice = 'Contract updated';
        } else {
            $contracts->insert($data);
            $contracts_id = $contracts->last_id();
            if(!empty($remind_members) && $data['contracts_expired_notification'] == 1){
                foreach($remind_members as $remind_member){
                    $remind_member['join_contracts_id']= $contracts_id;
                    $contract_remind_members->insert($remind_member);
                }
            }

            foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                $contract_x_licensed_location['join_contracts_id'] = $contracts_id;
                $contract_x_licensed_location['join_licensed_locations_id'] = $licensed_locations_id;
                $contracts_x_licensed_locations->insert($contract_x_licensed_location);
            }

            $data['join_contracts_id'] = $contracts_id;

            contracts_history::new_contract_entry(ActiveMemberInfo::GetMemberId(), $contracts_id);

            $notice = 'Contract added';
        }
        
        //add remove entity item milestone
        contracts_milestones::addUpdateMilestone($contracts_id,$existing_milestone_ids,$data);

        //If this is the first file, make it the active one.
        if(!$contractFiles && count($data_files) == 1) {
            $data_files[0]['contracts_files_active'] = 1;
        }

        if (!empty($data_files)) {
            foreach ($data_files as $file) {
                $file['join_contracts_id'] = $contracts_id;
                $contracts_files->insert($file);
                contracts_history::file_added_entry(ActiveMemberInfo::GetMemberId(), $contract_id, $file['files_name']);
            }
        }

        $contract = contracts::get_contract($contracts_id);

        http::redirect(BASEURL . '/members/contracts/index.php', array('notice' => $notice));
    } else {
        $contract = array_merge((array)$contract, $data);
    }
}
?>
