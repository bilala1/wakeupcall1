<?
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$contract = contracts::get_contract($_REQUEST['contracts_id']);
if($contract) {
    $permissions = UserPermissions::UserObjectPermissions('contracts', $contract);
}

if($contract && $permissions['edit']) {

    if ($_REQUEST['hide_contract'] == 1) {
        $contract['contracts_hidden'] = 1;
        $notice = 'The contract has been hidden';
    } else {
        $contract['contracts_hidden'] = 0;
        $notice = 'The contract has been un-hidden';
    }

    $contracts_table = new mysqli_db_table('contracts');
    $contracts_table->update($contract, $contract['contracts_id']);
    $contracts_id = $contract['contracts_id'];

    contracts_history::hidden_changed(ActiveMemberInfo::GetMemberId(), $_REQUEST['contracts_id'], $_REQUEST['hide_contract'] == 1);
} else {
    $notice = 'You do not have permissions to hide this contract.';
}

http::redirect(BASEURL . '/members/contracts/index.php', array('notice' => $notice));

?>