<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(35, $_REQUEST['page']);

include '_determine_contracts_params.php';
$contracts = contracts::get_paged_contracts_list($paging, $contracts_start_time, $contracts_end_time, $order,
                                                 $contracts_show_hidden, $filter_licensed_locations_id);

$contract_types = array();
$contract_types['Active'] = array();
$contract_types['Month to Month'] = array();
$contract_types['Autorenew-Annual'] = array();
$contract_types['Expired'] = array();
$contract_types['Inactive'] = array();

foreach ($contracts as $contract) {
    $contract_types[$contract['contracts_status']][] = $contract;
}

$userPrefs = UserPreferences::getForMember(ActiveMemberInfo::GetMemberId() );
// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_contracts FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$model = array(
    "contracts_start_time" => $contracts_start_time,
    "contracts_end_time" => $contracts_end_time,
    "contracts_show_hidden" => $contracts_show_hidden,
    "join_licensed_locations_id" => $filter_licensed_locations_id
);

$contracts_status_values = array(
    'Active' => 'Active',
    'Month to Month' => 'Month to Month',
    'Autorenew-Annual'=>'Auto Renew - Annual',
    'Expired' => 'Expired',
    'Inactive' => 'Inactive');

?>
