<?php
include(ACTIONS . '/members/protect.php');
include(LIBS . '/PHPExcel/PHPExcel.php');

function date_string($date)
{
    return $date ? date('m/d/Y', strtotime($date)) : '';
}

function setup_worksheet($worksheetObj)
{
    $worksheetObj->getColumnDimension('A', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('B', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('C', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('D', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('E', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('F', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('G', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('H', true)->setAutoSize(true);
}

function apply_border($worksheetObj, $base_row, $merge_end, $end_col, $offset)
{
    $offset_end_col = $end_col + $offset;
    
    for($col = 0; $col <= $offset_end_col; $col++)
    {
        if($col <= $offset_end_col - 1)
        {
            $worksheetObj->getStyleByColumnAndRow($col, $base_row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $worksheetObj->getStyleByColumnAndRow($col, $merge_end)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
            
            for($row = $base_row; $row <= $merge_end; $row++)
            {
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getLeft() ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            }
        }
        else 
        {
            for($row = $base_row; $row <= $merge_end; $row++)
            {
                if($row == $base_row)
                {
                    $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                }
                if($row == $merge_end)
                {
                    $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                }
                
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $worksheetObj->getStyleByColumnAndRow($col, $row)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            }
        }
    }
}

function merge_item_columns($worksheetObj, $col_start, $col_end, $row_start, $row_end)
{
    for($i = $col_start; $i <= $col_end; $i++)
    {
        $worksheetObj->mergeCellsByColumnAndRow($i, $row_start, $i, $row_end);
    }
}

function write_headers($worksheetObj)
{
    $offset = 0;
    $headerRange = "A1:G1";

    if(ActiveMemberInfo::IsUserMultiLocation()) {
        $offset = 1;
        $worksheetObj->setCellValueByColumnAndRow(0, 1, "Location");
        $headerRange = "A1:H1";
    }

    $worksheetObj->setCellValueByColumnAndRow(0 + $offset, 1, "Contracted Party");

    $worksheetObj->setCellValueByColumnAndRow(1 + $offset, 1, "Contact Name");

    $worksheetObj->setCellValueByColumnAndRow(2 + $offset, 1, "Effective Date");

    $worksheetObj->setCellValueByColumnAndRow(3 + $offset, 1, "Expiration Date");

    $worksheetObj->setCellValueByColumnAndRow(4 + $offset, 1, "Description");

    $worksheetObj->setCellValueByColumnAndRow(5 + $offset, 1, "Contract Type");

    $worksheetObj->setCellValueByColumnAndRow(6 + $offset, 1, "History");

    $worksheetObj->getStyle($headerRange)->getFont()->setBold(true);
    $worksheetObj->getStyle($headerRange)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle($headerRange)->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_item($worksheetObj, $rowIndex, $merge_end, $contract, $offset)
{
    if(ActiveMemberInfo::IsUserMultiLocation())
    {
        $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $contract['licensed_locations_name']);
    }

    $worksheetObj->setCellValueByColumnAndRow(0 + $offset, $rowIndex, $contract['contracts_contracted_party_name']);

    $worksheetObj->setCellValueByColumnAndRow(1 + $offset, $rowIndex, $contract['contracts_contact_name']);

    $worksheetObj->setCellValueByColumnAndRow(2 + $offset, $rowIndex, date_string($contract['contracts_effective_date']));

    $worksheetObj->setCellValueByColumnAndRow(3 + $offset, $rowIndex, date_string($contract['contracts_expiration_date']));

    $worksheetObj->setCellValueByColumnAndRow(4 + $offset, $rowIndex, $contract['contracts_description']);

    $worksheetObj->setCellValueByColumnAndRow(5 + $offset, $rowIndex, $contract['contracts_status']);

    if($merge_end > $rowIndex) {
        merge_item_columns($worksheetObj, 0, 5 + $offset, $rowIndex, $merge_end);
    }
}

function write_histories($worksheetObj, $col_index, $rowIndex, $contract, $offset)
{   
    $contract_histories = contracts_history::get_contracts_history_with_data(ActiveMemberInfo::GetMemberId(), $contract['contracts_id']);
    $contract_history = new PHPExcel_RichText();

    $merge_end = $rowIndex;

    foreach ($contract_histories as $history) 
    {
        $objHistDate = $contract_history->createTextRun(date_string($history['contracts_history_datetime']) . ": ");
        $objHistDate->getFont()->setBold(true);

        $contract_history_text = $objHistDate->getText() . strip_tags($history['contracts_history_description']);
        
        $worksheetObj->setCellValueByColumnAndRow($col_index + $offset, $merge_end, $contract_history_text);
        $merge_end++;
    }
    $merge_end--;
    if($merge_end < $rowIndex) {
        $merge_end = $rowIndex;
    }
    return $merge_end;
}


$offset = 0;
if(ActiveMemberInfo::IsUserMultiLocation()) {
    $offset = 1;
}
    
$paging = new paging(35, $_REQUEST['page']);
include '_determine_contracts_params.php';

$objPHPExcel = new PHPExcel();

$worksheetObj = $objPHPExcel->getSheet(0);
$worksheetObj->setShowGridlines(false);

//get contracts
$contracts = contracts::get_paged_contracts_list($paging, $contracts_start_time, $contracts_end_time,
    $order, $contracts_show_hidden, $filter_licensed_locations_id);

//row starts at 2 for the header in 1
$rowIndex = 2;

write_headers($worksheetObj, $rowIndex, $contract);

setup_worksheet($worksheetObj);
$lfcr = "\n";

//Write contracts
foreach ($contracts as &$contract) {
    $contract['contracts_history'] = $contract_history;

    $merge_end = write_histories($worksheetObj, 6, $rowIndex, $contract, $offset);
    write_item($worksheetObj, $rowIndex, $merge_end, $contract, $offset, $offset);
    apply_border($worksheetObj, $rowIndex, $merge_end, 6, $offset);
    $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
    $rowIndex = $merge_end + 1;

}
$cellRange = 'A0' . ':O' . $rowIndex;

$worksheetObj->getStyle($cellRange)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'phpxltmp') . ".xlsx";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$objWriter->save($outputFileName);


header('Content-Type: application/xls');
header('Content-Disposition: attachment; filename=' . "report.xlsx" . ';');
header('Content-Length: ' . filesize($outputFileName));

readfile($outputFileName);
?>