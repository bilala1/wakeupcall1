<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';

if($_REQUEST['documents_id']){
    $members_x_documents_table = new mysqli_db_table('members_x_documents');
    $members_x_documents_x_location_table = new mysqli_db_table('members_x_documents_x_licensed_locations');
    
    $db = mysqli_db::init();
    
    $exists = $db->fetch_singlet('SELECT COUNT(join_members_id) 
                                  FROM members_x_documents
                                  WHERE join_members_id = ?
                                  AND join_documents_id = ? ', array(ActiveMemberInfo::GetMemberId(),$_REQUEST['documents_id']));
    if($exists < 1){ // if a person does not yet have a file in his documents, add it
        $insert = array(
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'join_documents_id' => $_REQUEST['documents_id']
        );
        // when adding documents from document library
        if(http::is_ajax() && $_REQUEST['join_library_categories_id']){
            $insert['join_members_library_categories_id'] = $_REQUEST['join_library_categories_id'];
        }

        if($_REQUEST['join_members_library_categories_id']){
            $insert['join_members_library_categories_id'] = $_REQUEST['join_members_library_categories_id'];
        }

        if($_REQUEST['documents_is_corporate_shared']){
            $insert['documents_is_corporate_shared'] = $_REQUEST['documents_is_corporate_shared'];
        }

        if($_REQUEST['members_x_documents_all_locations']){
            $insert['members_x_documents_all_locations'] = $_REQUEST['members_x_documents_all_locations'];
        }
        
        $members_x_documents_table->insert($insert);
        $members_x_documents_id = $members_x_documents_table->last_id();
        if(!empty($_REQUEST['join_licensed_locations'])){
            foreach ($_REQUEST['join_licensed_locations'] as $licensed_locations_id) {
                $members_x_documents_x_location['join_members_x_documents_id'] = $members_x_documents_id;
                $members_x_documents_x_location['join_licensed_locations_id'] = $licensed_locations_id;
                $members_x_documents_x_location_table->insert($members_x_documents_x_location);
            }
        }
        members::log_action(ActiveMemberInfo::GetMemberId(), "Added an existing document to library");

        $notice = 'The document has been added to your documents';
    }
    else {
        $notice = 'This document is in your documents already.';
    }
    
    if(http::is_ajax()) {
        echo $notice;
        exit;
    }
    
    if($_REQUEST['join_library_categories_id']){
        http::redirect(HTTP_FULLURL . '/members/documents/index.php?join_library_categories_id='.$_REQUEST['join_library_categories_id'].'&notice=' . urlencode($notice));
    }
    elseif($_REQUEST['redirect']){ // if comming from search page, return there
         http::redirect(HTTP_FULLURL .urldecode($_REQUEST['redirect']).'&notice=' . urlencode($notice));
    }
    else {
        http::redirect(HTTP_FULLURL . '/members/documents/index.php?notice=' . urlencode($notice));
    }
}


?>
