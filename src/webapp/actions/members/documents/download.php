<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$downloads = new mysqli_db_table('downloads');

$document = $db->fetch_one('select * from documents inner join files on join_files_id = files_id where documents_id = ?', array($_REQUEST['documents_id']));

if($document){
    if((members::get_members_status(ActiveMemberInfo::GetMemberId())== 'free') && ((int)$document['documents_freetrial']==0)){
        http::redirect(BASEURL . '/members/documents/index.php', array('notice' => 'Restricted to full members.'));
    }

    //record download
    $downloads->insert(array(
        'downloads_type' => 'file',
        'join_id' => $_REQUEST['documents_id'],
        'join_members_id' => ActiveMemberInfo::GetMemberId(),
        'downloads_datetime' => SQL('NOW()')
    ));
    
    if($document['documents_type'] == 'library'){
        members::log_action(ActiveMemberInfo::GetMemberId(), 'Downloaded from Doc Library: '.$document['documents_filename']);
    }else{
        members::log_action(ActiveMemberInfo::GetMemberId(), 'Downloaded from My Files: '.$document['documents_filename']);
    }

    $redirectUrl = \Services\Services::$fileSystem->getFileDownloadUrl($document);

    http::redirect($redirectUrl);
}else{
    http::redirect(BASEURL . '/members/my-documents/documents/index.php', array('notice' => 'You are unable to view this document'));
}

?>