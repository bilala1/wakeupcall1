<?php

$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
$wheres = array();
$params = array();
$fields = documents::describe();

foreach($fields as $field){
    $field_name = $field['Field'];

    $field_value = $_REQUEST[$field_name];
    if(isset($field_value) && $field_value != ''){
        if(stristr($field_name, 'join') || $field['Type'] == 'tinyint(1)'){
			if ($field_name !== 'join_library_categories_id') {
				$wheres[] = $field_name.' = ?';
				$params[] = $field_value;
			} else {
				//-- include all the subcategories too...
				$library_categories[] = $field_value;
				$library_categories += $db->fetch_singlets('select library_categories_id FROM library_categories WHERE join_library_categories_id = ?', array($field_value));

				$wheres[] = 'join_library_categories_id IN ('.implode(',', $library_categories).')';
			}
        }else{
            $wheres[] = $field_name.' like ?';
            $params[] = '%'.$field_value.'%';
        }
    }
}

if($_REQUEST['keyword']){
    $wheres[] = 'FIND_IN_SET(?,documents_keywords)';
    $params[] = $_REQUEST['keyword'];
}

$wheres[] = 'documents_type = "library"';
$wheres[] = 'documents_status = "active"';


//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
//default order
if(!isset($_REQUEST['order'])){
    $_REQUEST['order'] = 'documents_title';
    $_REQUEST['by'] = 'ASC';
}

$by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
$orderby = ' ORDER BY ' . $db->escape_column($_REQUEST['order']) . ' ' . $by;

//------------------------------------------------------------------------------
//-- Get List
//------------------------------------------------------------------------------
$paging = new paging(25, $_REQUEST['page']);

$documents = $db->fetch_all('
    SELECT SQL_CALC_FOUND_ROWS d.*, f.*, mxd.members_x_documents_id 	AS in_library
    FROM documents AS d
    LEFT JOIN files f ON files_id = join_files_id
    LEFT JOIN members_x_documents AS mxd ON mxd.join_documents_id = d.documents_id AND mxd.join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()).'
    '.strings::where($wheres).'
    '.$orderby.'
    '.$paging->get_limit(),
    $params
);

$total = $db->found_rows();

$paging->set_total($total);

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_doc_library FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$parent_id = 0;
if($_REQUEST['join_library_categories_id']) {
    $parent_id = $db->fetch_singlet('SELECT join_library_categories_id 
                                    FROM library_categories
                                    WHERE library_categories_id = ?',
                                    array($_REQUEST['join_library_categories_id']));
}
?>
