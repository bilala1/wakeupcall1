<?php
use Services\Services;

$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

if($_REQUEST['documents_id']){
    $members_x_documents_table = new mysqli_db_table('members_x_documents');
    $documents_table = new mysqli_db_table('documents');
    
    $members_x_documents_table
        ->where('join_documents_id = '.$db->filter($_REQUEST['documents_id']))
        ->where('join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()))
        ->delete();
    
    $document = $documents_table->get($_REQUEST['documents_id']);
    
    if(in_array($document['documents_type'], array('personal', 'submitted'))){

        $documents_table->delete($_REQUEST['documents_id']);
        if($document['documents_format'] == 'file') {
            //remove file from system
            Services::$fileSystem->deleteFile($document['join_files_id']);
        }

        //delete download stats
        $db->query('DELETE FROM downloads WHERE downloads_type = "file" AND join_id = ?', array($document['documents_id']));
        $notice = 'The document has been deleted';
    } else {
        $notice = 'The document has been removed from your documents';
    }

    if($_REQUEST['redirect']){ // if comming from search page, return there
        http::redirect(HTTP_FULLURL .$_REQUEST['redirect'].'&notice=' . urlencode($notice));
    }
    else {
        http::redirect(HTTP_FULLURL . '/members/my-documents/index.php?notice=' . urlencode($notice));
    }
}


?>
