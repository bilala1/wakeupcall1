<?php
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

$member = members::get_contact_info(ActiveMemberInfo::GetMemberId());

/*
$member = $db->fetch_one("
	SELECT *, CONCAT(m.members_firstname, ' ', m.members_lastname) as members_fullname
	FROM members as m
	LEFT JOIN hotels as h
	ON m.members_id = h.join_members_id
	WHERE m.members_id = ?
", array(ActiveMemberInfo::GetMemberId()));
*/
$elaw_faqs = $db->fetch_all("
	SELECT * FROM faqs
	WHERE join_faqs_categories_id = ?
", array(8));

if ($_POST) {

	$data = $_POST;

	if(empty($data['question_content'])) {
		$errors['question_content'] = 'You must enter a question to send an inquiry.';
	}
    if(empty($data['parties_involved'])) {
		$errors['parties_involved'] = 'Please enter names of parties involved.';
	}


	if (empty($errors)) {
		$to = ELAW_EMAIL;
		$from = $data['members_email'];
		$subject = 'Employment Law Question | '.SITE_NAME;

		ob_start();
		include VIEWS . '/emails/elaw_contact.php';
		$message = ob_get_clean();

		mail::send($from, $to, $subject, $message);

        //send thank you email
        /*** Uncomment to activate
        $to = $data['members_email'];
        #$to = 'john@comentum.com';
        $from = CONCIERGE_EMAIL;
        $subject = 'Thank you for your Request | '.SITE_NAME;
        
        ob_start();
        include VIEWS . '/emails/thank-you4request.php';
        $message = ob_get_clean();
        
        mail::send($from,$to,$subject,$message);
        ***/
		$notice = "Your inquiry has been sent successfully.";
	}
    else {
        $member = array_merge($member, $data);
    }
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_empl_law FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>
