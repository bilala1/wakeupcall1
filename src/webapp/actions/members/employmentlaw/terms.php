<?php
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

$infobox = $db->fetch_singlet('SELECT members_settings_infobox_empl_law FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

if ($_POST) {
	$data = $_POST;

	if (!empty($data['members_elaw_agreement_yn'])) {
		$data['members_elaw_agreement_datetime'] = date('Y-m-d H:i:s');

		$members_table = new mysqli_db_table('members');
		$members_table->update($data, ActiveMemberInfo::GetMemberId());
	}
}

$member = $db->fetch_one("
	SELECT * FROM members
	WHERE members_id = ?
", array(ActiveMemberInfo::GetMemberId()));

$agreed_to_terms = $member['members_elaw_agreement_yn'];
$agreed_date = $member['members_elaw_agreement_datetime'];

if ($agreed_to_terms and !empty($agreed_date)) {
	http::redirect(FULLURL . '/members/employmentlaw/index.php');
}
?>
