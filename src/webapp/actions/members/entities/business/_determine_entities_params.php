<?php

if (array_key_exists('order', $_REQUEST)) {
    $order = $_REQUEST['order'];
    $_SESSION['businesses_list_order'] = $_REQUEST['order'];
} else if (array_key_exists('businesses_list_order', $_SESSION)) {
    $order = $_SESSION['businesses_list_order'];
}

if ($_REQUEST['clear']) {
    $_REQUEST['join_licensed_locations_id'] = array();
    $_REQUEST['businesses_show_hidden'] = '0';
}

if (array_key_exists('join_licensed_locations_id', $_REQUEST) || $_REQUEST['filter']) {
    $filter_licensed_locations_id = $_REQUEST['join_licensed_locations_id'];
    if($filter_licensed_locations_id && in_array('all', $filter_licensed_locations_id)) {
        $filter_licensed_locations_id = null;
    }
    $_SESSION['businesses_list_licensed_locations_id'] = $filter_licensed_locations_id;
} else if (array_key_exists('businesses_list_licensed_locations_id', $_SESSION)) {
    $filter_licensed_locations_id = $_SESSION['businesses_list_licensed_locations_id'];
}

if (array_key_exists('businesses_show_hidden', $_REQUEST)) {
    $_SESSION['businesses_show_hidden'] = $_REQUEST['businesses_show_hidden'];
    $businesses_show_hidden = $_REQUEST['businesses_show_hidden'];
} else if (array_key_exists('businesses_show_hidden', $_SESSION)) {
    $businesses_show_hidden = $_SESSION['businesses_show_hidden'];
} else {
    $businesses_show_hidden = $_SESSION['businesses_show_hidden'] = 0;
}