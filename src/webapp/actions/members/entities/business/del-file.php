<?php
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$file = $db->fetch_one('SELECT * FROM entities_files INNER JOIN files on files_id = join_files_id WHERE entities_files_id = ?',
    array($_REQUEST['entities_files_id']));
$entityId = $file['join_entities_id'];
$entity = entities::get_business_entity($entityId);
$perms = UserPermissions::UserObjectPermissions('entities', $entityId);

if ($entity && $perms['delete']) {
    $deleteSuccess = Services::$fileSystem->deleteFile($file['files_id']);
    if ($deleteSuccess) {
        $query = $db->query('DELETE FROM entities_files WHERE entities_files_id = ?',
            array($file['businesses_files_id']));
        $deleteSuccess = is_array($query->result);
    }

    if ($deleteSuccess) {
        entities_history::file_deleted_entry(ActiveMemberInfo::GetMemberId(), $file['join_entities_id'], $file['files_name']);

        http::redirect('edit.php?businesses_id=' . $business_id . '&notice=' . urlencode('The file was deleted successfully!'));
    } else {
        http::redirect('edit.php?businesses_id=' . $business_id . '&notice=' . urlencode('There was a problem deleting the file.  Please try again later.'));
    }
} else {
    http::redirect('index.php?notice=' . urlencode('You don\'t have permission to delete this file.'));
}
?>
