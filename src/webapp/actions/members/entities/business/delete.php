<?
use Services\Services;

include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();

$business_id = $_REQUEST['business_entities_id'];
$business = entities::get_business_entity($business_id);
$perms = UserPermissions::UserObjectPermissions('business_entities', $business);

if ($business && $perms['delete']) {

    $entities_table = new mysqli_db_table('entities');
    $business['entities_delete_datetime'] = sql('NOW()');
    $entities_table->update($business, $business['join_entities_id']);

    $notice = 'The business has been deleted';
} else {
    $notice = 'You do not have permissions to delete this business.';
}

http::redirect(BASEURL . '/members/entities/business/index.php', array('notice' => $notice));

?>