<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if (isset($_REQUEST['entities_files_id'])) {
    $file = $db->fetch_one('
        SELECT *
        FROM entities_files ef
        INNER JOIN files ON join_files_id = files_id
        INNER JOIN entities ON entities_id = ef.join_entities_id
        INNER JOIN business_entities be ON entities_id = be.join_entities_id
        WHERE
            entities_files_id = ? AND ' . UserPermissions::CreateInClause('business_entities'),
        array($_REQUEST['entities_files_id']
        ));
    //pre($db->debug()); exit;
    if ($file) {
        members::log_action(ActiveMemberInfo::GetAccountId(), 'Downloaded from Business Tracker Files: ' . $file['files_name']);

        $downloads = new mysqli_db_table('downloads');
        //record download
        $downloads->insert(array(
            'downloads_type' => 'business',
            'join_id' => $_REQUEST['entities_files_id'],
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'downloads_datetime' => SQL('NOW()')
        ));

        $redirectUrl = Services::$fileSystem->getFileDownloadUrl($file);

        if(!$redirectUrl) {
            http::redirect(BASEURL . '/members/entities/business/index.php',
                array('notice' => 'You are unable to view this document'));
        } else {
            http::redirect($redirectUrl);
        }
    } else {
        http::redirect(BASEURL . '/members/entities/business/index.php', array('notice' => 'You are unable to view this document'));
    }
}
?>
