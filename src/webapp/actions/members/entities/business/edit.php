<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

function notes_sort_helper()
{
    if (!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC') {
        return ' ORDER BY entities_notes_datetime DESC ';
    } else {
        return ' ORDER BY entities_notes_datetime ASC ';
    }
}

$db = mysqli_db::init();

if(isset($_REQUEST['business_entities_id']) && !empty($_REQUEST['business_entities_id'] )) {
    $business = entities::get_business_entity($_REQUEST['business_entities_id']);
} elseif(isset($_REQUEST['entities_id']) && !empty($_REQUEST['entities_id'] )) {
    $business = entities::get_business_entity_by_entity_id($_REQUEST['entities_id']);
}

if ($business) {
    $permissions = UserPermissions::UserObjectPermissions('business_entities', $business);
}

$entity_item_data = entities_items::get_entities_items_by_entity_id($business['entities_id']);
if ($_REQUEST['business_entities_id'] && (!$business || !$permissions['edit'])) {
    http::redirect(BASEURL . '/members/entities/business/index.php',
        array('notice' => 'You don\'t have permission to edit that business'));
}
$licensed_locations = licensed_locations::get_licensed_locations_with_permission('business_entities');

if ($business) {
    $entity_files = entities::get_business_files($_REQUEST['business_entities_id']);
    $licensed_location_name = $business['licensed_locations_name'];
    $query = '
        SELECT
        entities_notes_id, join_entities_id, entities_notes_datetime, entities_notes_subject, entities_notes_note
        FROM entities_notes
        WHERE join_entities_id = ?';
    $query = $query.notes_sort_helper();
    $entity_notes = $db->fetch_all($query,array($business['entities_id']));
    foreach ($entity_notes as $entities_note){
        $existing_notes_ids[] = $entities_note['entities_notes_id'];
    }
}
if(count($licensed_locations) == 1) {
    $licensed_location_name = $licensed_locations[0]['licensed_locations_name'];
}



$errors = array();
if ($_POST) {
    $data = $_POST;  
    $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
    if(!$business) {
        if(count($licensed_locations) == 1) {
            $data['join_licensed_locations'] = $licensed_locations[0]['licensed_locations_id'];
        }
    }

    $validate = new validate($data);

    //set initial validation rules
    $validate
        ->setAllOptional()
        ->unsetOptionals(array())
        ->setErrors(array())
        ->addMultiLocationSelectionValidation($data, 'business_entitie', $business);

    $uploaded_files = files::fixUploadFileArray($_FILES['entities_files']);

    $validate->setFileType('both');
    foreach ($uploaded_files as $file) {
        if ($file['name'] == '') {
            continue;
        }

        $validate->setFile($file, 'entities_files');
    }

    $errors += $validate->test();

    if (!$errors) {
        $businesses = new mysqli_db_table('business_entities');
        $entities = new mysqli_db_table('entities');
        $entities_x_licensed_locations = new mysqli_db_table('business_entities_x_licensed_locations');

        if ($business) {
            $entities_id = $business['join_entities_id'];
            
            $business_entities_id = $business['business_entities_id']; 
            if(!$data['business_entities_id']) $data['business_entities_id'] = $business_entities_id;
            if(!$data['entities_id']) $data['entities_id'] = $entities_id;
            $old_business = entities::get_business_entity($business['entities_id']);
            //businesses table
            $businesses->update($data, $business['business_entities_id']);
            $entities->update($data, $business['join_entities_id']);
            $business_entities_id = $business['business_entities_id'];

            $new_business = entities::get_business_entity($business['business_entities_id']);

            entities_history::entity_updated_entry(ActiveMemberInfo::GetMemberId(), $new_business, $old_business);

            //notice
            $notice = 'Business updated';
        } else {
            $entities->insert($data);
            $entities_id = $entities->last_id();
            $data['join_entities_id'] = $entities_id;

            $businesses->insert($data);
            $business_entities_id = $businesses->last_id();
            $data['join_business_entities_id'] = $business_entities_id;

            entities_history::new_entity_entry(ActiveMemberInfo::GetMemberId(), $business_entities_id);

            $notice = 'Business added';
        }
         //clean up old things
        // If this was a single location user, there is nothing to do here - they can't add or remove any other locations
        $new_location_ids = array();
        if(ActiveMemberInfo::IsUserMultiLocation()) {
            $in_clause = implode(', ', UserPermissions::LocationIdsWithPermission('business_entities'));
                $db->query('DELETE FROM business_entities_x_licensed_locations
                          WHERE join_business_entities_id = ?
                                AND join_licensed_locations_id in ('.$in_clause.')',
                    array($business_entities_id));
            foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                $entities_x_licensed_location['join_business_entities_id'] = $business_entities_id;
                $entities_x_licensed_location['join_licensed_locations_id'] = $licensed_locations_id;
                $new_location_ids[] = $licensed_locations_id;
                $entities_x_licensed_locations->insert($entities_x_licensed_location);
            }
        }
        //for single location user, add it only once
        if(!ActiveMemberInfo::IsUserMultiLocation() && (!$_REQUEST['business_entities_id'] && !$_REQUEST['entities_id'] )) {
            foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                $entities_x_licensed_location['join_business_entities_id'] = $business_entities_id;
                $entities_x_licensed_location['join_licensed_locations_id'] = $licensed_locations_id;               
                $entities_x_licensed_locations->insert($entities_x_licensed_location);
            }
        }
        
        //if any location is removed and a remind member is seleted for that location, moe them to additional users
        if(count($entity_item_data) > 0){
            if($business['business_entities_all_locations'] == 1){
                    $new_location_ids = UserPermissions::LocationIdsWithPermission('business_entities'); 
            }
            foreach($new_location_ids as $location_id){
                $users_foreach_location[] = UserPermissions::UsersWithLocationPermission('business_entities',$location_id);
            }
            foreach($users_foreach_location as $users_for_locations){
                foreach($users_for_locations as $users_for_location){
                    $users[$users_for_location['members_id']] = $users_for_location['members_id'];
                }
            }
            foreach($entity_item_data as $entity_item){
                $entity_item_milestones[] = entities_items::get_milestone_members_for_business_entity($entity_item['entities_items_id']);
            }
            $milestone_members = array();
            $entity_item_milestone_members_array = array();
            foreach($entity_item_milestones as $entity_item_milestone){
                foreach($entity_item_milestone as $entity_item_milestone_members){
                    $milestone_members[] = $entity_item_milestone_members['join_members_id'];
                    $entity_item_milestone_members_array[$entity_item_milestone_members['entities_items_milestones_remind_members_id']]['entities_items_id'] = $entity_item_milestone_members['join_entities_items_id'] ;
                    $entity_item_milestone_members_array[$entity_item_milestone_members['entities_items_milestones_remind_members_id']]['entities_items_milestones_id'] = $entity_item_milestone_members['join_entities_items_milestones_id'] ;
                    $entity_item_milestone_members_array[$entity_item_milestone_members['entities_items_milestones_remind_members_id']]['members_id'] = $entity_item_milestone_members['join_members_id'] ;
                }
            }
            $removed_location_users = array_diff($milestone_members,$users);
            $additional_recipients = array();
            foreach($removed_location_users as $location_users){
                foreach($entity_item_milestone_members_array as $remind_members){
                    if($location_users == $remind_members['members_id']){
                        $additional_recipients[$remind_members['entities_items_milestones_id']]['join_entities_items_id'] = $remind_members['entities_items_id'];
                        $additional_recipients[$remind_members['entities_items_milestones_id']]['join_entities_items_milestones_id'] = $remind_members['entities_items_milestones_id'];
                        $additional_recipients[$remind_members['entities_items_milestones_id']]['entities_items_milestones_additional_recipients_email'] = members::get_members_email($remind_members['members_id']);
                        $additional_recipients[$remind_members['entities_items_milestones_id']]['entities_items_milestones_remind_members_id'] = $remind_members['members_id'];
                    }
                }

            }
            $entities_items_milestones_additional_recipients_table = new mysqli_db_table('entities_items_milestones_additional_recipients');

            foreach($additional_recipients as $additional_recipient){
                $check_existing = entities_items::checkadditional_recipient($additional_recipient['join_entities_items_milestones_id'],$additional_recipient['entities_items_milestones_additional_recipients_email']);
                if(count($check_existing) == 0){
                    $entities_items_milestones_additional_recipients_table->insert($additional_recipient);
                }
                entities_items::remove_remind_members($additional_recipient['join_entities_items_milestones_id'],$additional_recipient['entities_items_milestones_remind_members_id']);
            }
        }
        // If it's a save of an existing entity, send them back to index. If it's new, keep them there so they can add tracked items.
        if($_REQUEST['business_entities_id']) {
            http::redirect(BASEURL . '/members/entities/business/index.php', array('notice' => $notice));
        } else {
            http::redirect(BASEURL . '/members/entities/business/edit.php', array('business_entities_id' => $business_entities_id, 'notice' => $notice));
        }
    } else {
        $business = array_merge((array)$business, $data);
    }
}
?>
