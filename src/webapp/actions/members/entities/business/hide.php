<?
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$business = entities::get_business_entity($_REQUEST['business_entities_id']);
if($business) {
    $permissions = UserPermissions::UserObjectPermissions('business_entities', $business);
}

if($business && $permissions['edit']) {
    if ($_REQUEST['hide_business'] == 1) {
        $business['entities_hidden'] = 1;
        $notice = 'The business has been hidden';
    } else {
        $business['entities_hidden'] = 0;
        $notice = 'The business has been un-hidden';
    }

    $entities_table = new mysqli_db_table('entities');
    $entities_table->update($business, $business['entities_id']);
    $business_entities_id = $business['business_entities_id'];

    entities_history::hidden_changed(ActiveMemberInfo::GetMemberId(), $business['entities_id'], $_REQUEST['hide_business'] == 1);
} else {
    $notice = 'You do not have permissions to hide this business.';
}

http::redirect(BASEURL . '/members/entities/business/index.php', array('notice' => $notice));

?>