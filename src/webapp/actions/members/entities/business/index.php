<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(35, $_REQUEST['page']);

include '_determine_entities_params.php';
$businesses = entities::get_paged_businesses_list($paging, $order, $businesses_show_hidden, $filter_licensed_locations_id);

$model = array(
    "businesses_show_hidden" => $businesses_show_hidden,
    "join_licensed_locations_id" => $filter_licensed_locations_id
);

?>
