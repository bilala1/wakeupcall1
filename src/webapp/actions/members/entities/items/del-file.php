<?php
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$file = $db->fetch_one('SELECT * FROM entities_items_files INNER JOIN files on files_id = join_files_id WHERE entities_items_files_id = ?',
    array($_REQUEST['entities_items_files_id']));
$entityItemId = $file['join_entities_items_id'];

$entity_item = entities_items::get_entities_item($entityItemId);
$perms = UserPermissions::UserObjectPermissions('entities', $entity_item);

if ($entity_item && $perms['delete']) {
    $deleteSuccess = Services::$fileSystem->deleteFile($file['files_id']);
    if ($deleteSuccess) {
        $query = $db->query('DELETE FROM entities_items_files WHERE entities_items_files_id = ?',
            array($file['entities_items_files_id']));
        $deleteSuccess = is_array($query->result);
    }

    if ($deleteSuccess) {
        entities_items_history::file_deleted_entry(ActiveMemberInfo::GetMemberId(), $file['join_entities_items_id'], $file['files_name']);

        http::redirect('edit.php?entities_id='.$entity_item['entities_id'].'&entities_items_id='.$entityItemId.'&notice=' . urlencode('The file was deleted successfully!'));
    } else {
        http::redirect('edit.php?entities_id='.$entity_item['entities_id'].'&entities_items_id='.$entityItemId. '&notice=' . urlencode('There was a problem deleting the file.  Please try again later.'));
    }
} else {
    http::redirect('index.php?notice=' . urlencode('You don\'t have permission to delete this file.'));
}
?>
