<?
use Services\Services;

include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$entityId = $_REQUEST['entities_id'];
$entity = entities::get_entity($entityId);
$entityType = entities::get_entity_type($entity);

$entities_items_id = $_REQUEST['entities_items_id'];
$entity_item = entities_items::get_entities_item($entities_items_id);
$perms = UserPermissions::UserObjectPermissions('entities_items', $entity_item);

if ($entities_items_id && $perms['delete']) {
    if(entities_items::delete_entities_items($entities_items_id)) {
        $notice = 'The entity item is deleted successfully!';
    }
}else {
    $notice = 'You do not have permissions to delete this tracked item.';
}

http::redirect(BASEURL . '/members/entities/'.$entityType.'/edit.php?entities_id='.$entityId.'&notice='.$notice);

?>