<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if (isset($_REQUEST['entities_items_files_id'])) {
    $file = $db->fetch_one('
        SELECT *
        FROM entities_items_files ef
        INNER JOIN files ON join_files_id = files_id
        INNER JOIN entities_items ON entities_items_id = ef.join_entities_items_id
        INNER JOIN entities ON entities_id = join_entities_id
        WHERE
            entities_items_files_id = ?',
        array($_REQUEST['entities_items_files_id']
        ));

   // pre($db->debug()); exit;
    if ($file) {
        members::log_action(ActiveMemberInfo::GetAccountId(), 'Downloaded from Entity items Files: ' . $file['files_name']);

        $downloads = new mysqli_db_table('downloads');
        //record download
        $downloads->insert(array(
            'downloads_type' => 'entities items',
            'join_id' => $_REQUEST['entities_items_files_id'],
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'downloads_datetime' => SQL('NOW()')
        ));

        $redirectUrl = Services::$fileSystem->getFileDownloadUrl($file);

        if(!$redirectUrl) {
            http::redirect(BASEURL . '/members/entities/items/edit.php?entities_id='.$file['entities_id'].'&entities_items_id='.$file['entities_items_id'],
                array('notice' => 'You are unable to view this document'));
        } else {
            http::redirect($redirectUrl);
        }
    } else {
        http::redirect(BASEURL . '/members/entities/items/edit.php?entities_id='.$file['entities_id'].'&entities_items_id='.$file['entities_items_id'], array('notice' => 'You are unable to view this document'));
    }
}
?>
