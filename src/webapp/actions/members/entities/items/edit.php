<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

function notes_sort_helper()
{
    if (!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC') {
        return ' ORDER BY entities_items_notes_datetime DESC ';
    } else {
        return ' ORDER BY entities_items_notes_datetime ASC ';
    }
}

$db = mysqli_db::init();
$entityId = $_REQUEST['entities_id'];
$entity = entities::get_entity($_REQUEST['entities_id']);


$business_entities_id = $entity['business_entities_id'];
if($entity['business_entities_all_locations'] == 1){
   $location_ids = UserPermissions::LocationIdsWithPermission('business_entities'); 
}else{
    $location_ids = entities::fetch_location_for_business_entity($business_entities_id);
}
$users = array();
$users_foreach_location = array();
$user_ids = array();
foreach($location_ids as $location_id){
    $users_foreach_location[] = UserPermissions::UsersWithLocationPermission('business_entities',$location_id);
}
foreach($users_foreach_location as $users_for_locations){
    foreach($users_for_locations as $users_for_location){
        $user_ids[] = $users_for_location['members_id'];
        $users[$users_for_location['members_id']] = $users_for_location;
    }
}
if ($entity) {
    $permissions = UserPermissions::UserObjectPermissions('entities', $entity);
}
$entityType = entities::get_entity_type($entity);
if (!$entity || !$permissions['edit']) {
    http::redirect(BASEURL . '/members/entities/'.$entityType.'/index.php',
        array('notice' => 'You don\'t have permission to edit that ' . $entityType));
}

$entities_item = entities_items::get_entities_item($_REQUEST['entities_items_id']);
if ($entity) {
    $permissions = UserPermissions::UserObjectPermissions('entities', $entity);
}
if ($_REQUEST['entities_items_id'] && (!$entities_item || !$permissions['edit'])) {
    http::redirect(BASEURL . '/members/entities/'.$entityType.'/index.php',
        array('notice' => 'You don\'t have permission to edit that entity item'));
}

if ($entities_item) {
    $entity_items_files = entities_items::get_entities_items_files($_REQUEST['entities_items_id']);
        $query = 'SELECT *         
        FROM entities_items_notes
        WHERE join_entities_items_id = ?';
    $query = $query.notes_sort_helper();
    $entities_items_notes = $db->fetch_all($query,array($_REQUEST['entities_items_id']));
    $entities_items_milestones_existing = entities_items::get_entities_items_milestones($_REQUEST['entities_items_id']);
    $entities_items_milestone_ids = array();
    $entities_items_milestones_remind_members = array();
    $existing_entities_items_milestones_remind_days = array();
    $existing_entities_items_milestones_additional_recipients = array();
    foreach($entities_items_milestones_existing as $entities_items_milestones){
        $existing_milestone_ids[]= $entities_items_milestones['entities_items_milestones_id'];
        $existing_entities_items_milestones_remind_days[$entities_items_milestones['entities_items_milestones_id']] = entities_items::get_entities_items_milestones_remind_days($entities_items_milestones['entities_items_milestones_id']);
        $entities_items_milestones['entities_items_milestones_remind_members'] = entities_items::get_entities_items_milestones_remind_members($entities_items_milestones['entities_items_milestones_id']);
        $entities_items_milestones_remind_members[$entities_items_milestones['entities_items_milestones_id']] = $entities_items_milestones['entities_items_milestones_remind_members'];        
        $existing_entities_items_milestones_additional_recipients[$entities_items_milestones['entities_items_milestones_id']] = entities_items::get_entities_items_milestones_additional_recipients($entities_items_milestones['entities_items_milestones_id']);
    }
    
}

$errors = array();

if ($_POST) {
    $data = $_POST;
    $data['join_entities_id'] = $_REQUEST['entities_id'];

    $validate = new validate($data);

    //set initial validation rules
    $validate
        ->setAllOptional()
        ->unsetOptionals(array())
        ->setErrors(array());

    $uploaded_files = files::fixUploadFileArray($_FILES['entities_items_files']);

    $validate->setFileType('both');
    foreach ($uploaded_files as $file) {
        if ($file['name'] == '') {
            continue;
        }

        $validate->setFile($file, 'entities_items_files');
    }

    $errors += $validate->test();

    if (!$errors) {
        $entity_items = new mysqli_db_table('entities_items');
        $entities_items_files = new mysqli_db_table('entities_items_files');

        $data_files = array();

        //loop through number of uploaded files
        foreach ($uploaded_files as $index => $file) {
            if ($file['size']) {
                $file_entry = Services::$fileSystem->storeFile($file, 'entities');

                if (!$file_entry) {
                    $errors['entities_items_files'] = 'There was a problem uploading a file';
                    return;
                }

                $file_entry['join_files_id'] = $file_entry['files_id'];
                $data_files[] = $file_entry;
            }
        }

        $data['entities_items_effective_date'] = empty($data['entities_items_effective_date']) ? null : date('Y-m-d', strtotime($data['entities_items_effective_date']));
        
        if ($entities_item) {
            $entities_id = $entities_item['join_entities_id'];
            $old_entity_items = entities_items::get_entities_item($entities_item['entities_items_id']);
            //entity items table           
            $entity_items->update($data, $entities_item['entities_items_id']);

            $entities_items_id = $entities_item['entities_items_id'];

            $new_entity_item = entities_items::get_entities_item($entities_items_id);

            entities_items_history::entity_updated_entry(ActiveMemberInfo::GetMemberId(), $new_entity_item, $entities_item);

            //notice
            $notice = 'Entity item updated';
        } else {
            $entity_items->insert($data);
            $entities_items_id = $entity_items->last_id();
            $data['join_entities_items_id'] = $entities_items_id;
            entities_items_history::new_entity_entry(ActiveMemberInfo::GetMemberId(), $entities_items_id);
            entities_history::item_added_entry(ActiveMemberInfo::GetMemberId(), $entity_id, $data['entities_items_name']);
            $notice = 'Entity item added';
        }
        //add remove entity item milestone
        entities_items_milestones_and_notes::addUpdateMilestone($entities_items_id,$existing_milestone_ids,$data);
        //add remove entity item notes
        entities_items_milestones_and_notes::addUpdateNotes($entities_items_id, $entities_items_notes, $data);

        if (!empty($data_files)) {
            foreach ($data_files as $file) {
                $file['join_entities_items_id'] = $entities_items_id;
                $entities_items_files->insert($file);
                entities_items_history::file_added_entry(ActiveMemberInfo::GetMemberId(), $entities_items_id,$file['files_name']);
            }
        }

        http::redirect(BASEURL . '/members/entities/'.$entityType.'/edit.php?entities_id='.$entityId.'&notice='.$notice);
    } else {
        $entities_item = array_merge((array)$entities_item, $data);
    }
}
?>
