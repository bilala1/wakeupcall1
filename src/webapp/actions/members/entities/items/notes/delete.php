<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$note = $db->fetch_one('SELECT * FROM entities_items_notes WHERE entities_items_notes_id = ?',array($_REQUEST['entities_items_notes_id']));

$entities_items_id = $note['join_entities_items_id'];
$entities_item = entities_items::get_entities_item($entities_items_id);
$entityId = $entities_item['join_entities_id'];
$entity = entities::get_entity($entityId);
$entityType = entities::get_entity_type($entity);

if(!UserPermissions::UserCanAccessObject($entityType.'_entities', $entity)) {
    http::redirect('../../'.$entityType.'/index.php', array('notice' => 'You don\'t have permission to edit that item'));
}

if(entities_items::delete_entities_items_notes($_REQUEST['entities_items_notes_id'])) {
    $notice = 'The note was deleted successfully!';
}
else {
    $notice = 'There was a problem deleting the note.  Please try again later.';
}

if($_REQUEST['redirect'] == 'popup') {
    http::redirect('notes_popup.php?entities_items_id=' . $entities_items_id . '&notice=' . $notice);
} else {
    http::redirect(BASEURL . '/members/entities/items/edit.php?entities_id=' . $entityId . '&entities_items_id=' . $entities_items_id . '&notice=' . $notice);
}