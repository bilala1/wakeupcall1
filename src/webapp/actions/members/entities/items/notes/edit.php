<?
include(ACTIONS . '/members/protect.php');

$entities_items_note = entities_items::get_entities_items_note_with_data($_REQUEST['entities_items_notes_id']);
$entities_items_id = $_REQUEST['entities_items_id'];

$errors = array();

if($_POST)
{
    $data = $_POST;

    $data['join_entities_items_id'] = $entities_items_id;
    $data['entities_items_notes_datetime'] = SQL("NOW()");;
    $entities_items_notes_table = new mysqli_db_table('entities_items_notes');
    if($entities_items_note)
    {
        $entities_items_notes_table->update($data, $entities_items_note['entities_items_notes_id']);
        $entities_items_note_new = entities_items::get_entities_items_note_with_data($_REQUEST['entities_items_notes_id']);
        entities_items_history::note_updated_entry(ActiveMemberInfo::GetMemberId(), $entities_items_id, $entities_items_note,$entities_items_note_new);
        $notice = 'Note updated!';
    }else{
        $entities_items_notes_table->insert($data);
        $entities_items_notes_id = $entities_items_notes_table->last_id();
        entities_items_history::note_added_entry(ActiveMemberInfo::GetMemberId(), $entities_items_id, $data);
        $notice = 'Note added!';
    }

    http::redirect('notes_popup.php?entities_items_id='.$entities_items_id.'&notice='.$notice);

}

?>
