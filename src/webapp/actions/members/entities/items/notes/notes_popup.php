<?php
include(ACTIONS . '/members/protect.php');

function sort_helper()
{
    if(!$_REQUEST['note_sort_order'] || !$_REQUEST['note_sort_order'] == 'DESC')
        return ' ORDER BY entities_items_notes_datetime DESC ';
    else
        return ' ORDER BY entities_items_notes_datetime ASC ';
}

$db = mysqli_db::init();

$paging = new paging(25, $_REQUEST['page']);

if($_REQUEST['entities_items_id'])
{
    $wheres[] = 'join_entities_items_id = ?';
    $params[] = $_REQUEST['entities_items_id'];
}

$entity_notes = $db->fetch_all('
    SELECT
    entities_items_notes_id, join_entities_items_id, entities_items_notes_datetime, entities_items_notes_subject,entities_items_notes_note
    FROM entities_items_notes
    '.strings::where($wheres).
    sort_helper().
    $paging->get_limit()
    , $params
);

$paging->set_total($db->found_rows());
?>
