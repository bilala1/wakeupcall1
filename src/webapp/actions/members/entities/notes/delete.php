<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$entities_id = $_REQUEST['entities_id'];
$nav_id = $_REQUEST['nav_id'];
$type = $_REQUEST['type'] == 'personnel' ? 'personnel' : 'business';

$note = $db->fetch_one('SELECT * FROM entities_notes WHERE entities_notes_id = ?',array($_REQUEST['entities_notes_id']));

if(!UserPermissions::UserCanAccessObject($type.'_entities', $note['join_entities_id'])) {
    http::redirect('../'.$type.'/index.php', array('notice' => 'You don\'t have permission to edit that entity'));
}

if(entities_notes::delete($_REQUEST['entities_notes_id'])) {
    $notice = 'The note was deleted successfully!';
}
else {
    $notice = 'There was a problem deleting the note.  Please try again later.';
}

if('entities' == $nav_id)
    http::redirect(BASEURL .'/members/entities/' . $type . '/index.php', array('notice' => $notice));
else if($nav_id)
    http::redirect(BASEURL .'/members/entities/' . $type . '/edit.php', array($type.'_entities_id' => $nav_id, 'notice' => $notice));
else if($entities_id)
    http::redirect(BASEURL .'/members/entities/' . $type . '/edit.php', array($type.'_entities_id' => $entities_id, 'notice' => $notice));
else
    http::redirect(BASEURL .'/members/entities/' . $type . '/index.php', array('notice' => $notice, 'notice' => $notice));
