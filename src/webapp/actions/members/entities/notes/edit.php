<?
include(ACTIONS . '/members/protect.php');

$entities_note = entities_notes::get_entities_note_with_data($_REQUEST['entities_notes_id']);
$entities_id = $_REQUEST['entities_id'];
$nav_id = $_REQUEST['nav_id'];
$type = $_REQUEST['type'] == 'personnel' ? 'personnel' : 'business';
$errors = array();

if($_POST)
{
    $data = $_POST;
    unset($data['nav_id']);

    if(!$entities_note['join_entities_id']) {
        $data['join_entities_id'] = $entities_id;
    }

    if($entities_note)
    {
        $entities_notes_id = entities_notes::update($entities_note, $data);
        $notice = 'Note updated!';
    }else
    {
        $entities_notes_id = entities_notes::create($data);
        $notice = 'Note added!';
    }

//    if('entities' == $nav_id)
//        http::redirect(BASEURL .'/members/entities/' . $type . '/index.php', array('notice' => $notice));
//    else if($nav_id)
//        http::redirect(BASEURL .'/members/entities/' . $type . '/edit.php', array($type.'_entities_id' => $nav_id, 'notice' => $notice));
//    else if($entities_id)
//        http::redirect(BASEURL .'/members/entities/' . $type . '/edit.php', array($type.'_entities_id' => $entities_id, 'notice' => $notice));
//    else
//        http::redirect(BASEURL .'/members/entities/' . $type . '/index.php', array('notice' => $notice, 'notice' => $notice));
}

?>
