<?php
if(ActiveMemberInfo::GetMemberId())
{
    http::redirect('members/index.php');
}

$db = mysqli_db::init();

$errors = array();

if(!empty($_POST)) 
{
	$member = $db->fetch_one('SELECT * FROM members
	    WHERE members_email = ? 
	    AND members_status != "locked"', array($_REQUEST['members_email']));

    if($member) 
    {
		// create new password, like dgr458
		$newpass .= strings::getrandchar('lc', 3);
		$newpass .= strings::getrandchar('num', 3);
		$member['members_password'] = hash('sha256', $newpass);

         // save new password
         $table = new mysqli_db_table('members');
         $table->update($member, $member['members_id']);
         
         // Email New Password
		ob_start(); 
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forgot-password.php');
		$message = ob_get_contents(); 
		ob_end_clean();
		
		mail::send(
			$from = ADMIN_EMAIL, 
			$to = $member['members_email'], 
			$subject ='Password Reset | ' . SITE_NAME,
			$message = $message
		);

        http::redirect(BASEURL . '/members/forgot-password.php', array('notice' => 'Your new password has been sent to ' . $member['members_email']));
    } else {
        $errors['members_email'] = 'Your email is either not active or not in the system.';
       
	}
}

if($_REQUEST['notice'])
{
	$notice = strip_tags(urldecode($_REQUEST['notice']));

}

?>
