<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

//------------------------------------------------------------------------------
//-- Forum Permissions
//------------------------------------------------------------------------------
/*$forum_permissions = groups::get_user_permissions(forums::get_forum_id_by_post($_REQUEST['posts_id']));
if(!$forum_permissions['reply']){
    http::redirect(HTTP_BASEURL . '/members/forums/view-topic.php?topics_id='.$_REQUEST['topics_id'].'&notice='.urlencode('You are not allowed to create a reply on this forum'));
}*/

$db = mysqli_db::init();

// Add (or remove) this multiquote
$topics_id = $_REQUEST['topics_id'];
$posts_id  = $_REQUEST['posts_id'];

if($topics_id and $posts_id){
    if(!$_SESSION['multiquote'][$topics_id][$posts_id]){
        $action = 'added';
        $_SESSION['multiquote'][$topics_id][$posts_id] = $posts_id;
    }
    else {
        // Remove it...
        $action = 'removed';
        unset($_SESSION['multiquote'][$topics_id][$posts_id]);
    }
}

echo json_encode(array('action' => $action));

?>
