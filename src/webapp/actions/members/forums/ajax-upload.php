<?php
include(ACTIONS . '/members/protect.php');

//No permission check!!

$db = mysqli_db::init();

$file = $_FILES['filename'];
$old_name = $file['name'];
$extension = file::get_extension($file['name']);

$filename = uniqid(md5(microtime())). '.' . $extension;
$upload = file::upload($file, $filename, FORUM_UPLOAD_DIR, unserialize(FORUM_MIMETYPES), unserialize(FORUM_EXTENSIONS), FORUM_MAX_FILE_SIZE);

if($upload === true){
    $uploaded = true;
}else{
    $uploaded = false;
    $error = $upload;
}

echo json_encode(array(
    'filename' => $filename,
    'error' => $error,
    'uploaded' => $uploaded,
    'real_filename' => $old_name,
    'debug' => $debug
));
?>
