<?php
include(ACTIONS . '/members/protect.php');

include_once 'header.php';

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 0;
$per_page = (isset($_REQUEST['perpg'])) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Get Category
//------------------------------------------------------------------------------
$category = $db->fetch_one('SELECT * FROM forum_categories WHERE forum_category_id = ?', array($_REQUEST['forum_category_id']));
$last_post_date = 'IFNULL(forum_posts.post_date, forum_topics.post_date)';
//------------------------------------------------------------------------------
//-- Get Topics
//------------------------------------------------------------------------------
$topics = $db->fetch_all('SELECT forum_topics.*, members.members_username AS author,
        COUNT(DISTINCT forum_posts.forum_post_id) AS posts,
        DATE_FORMAT(' . $last_post_date . ', "' . MYSQL_DATE . '") AS post_date
    FROM forum_posts AS forum_topics
    LEFT JOIN members ON forum_topics.join_members_id = members.members_id
    LEFT JOIN forum_posts ON forum_posts.parent_id = forum_topics.forum_post_id
    WHERE forum_topics.join_forum_category_id = ? AND forum_topics.parent_id = ?
    GROUP BY forum_topics.forum_post_id
    ORDER BY ' . $last_post_date . ' DESC
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    array($_REQUEST['forum_category_id'], 0));
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM forum_posts
    WHERE join_forum_category_id = ? AND parent_id = ?',
    array($_REQUEST['forum_category_id'], 0));
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/members/forums/category.php');

foreach($topics as &$topic)
{
    $last_post = $db->fetch_one('SELECT members.members_username AS author, forum_posts.join_members_id,
        DATE_FORMAT(forum_posts.post_date, "' . MYSQL_DATE . '") AS post_date
        FROM forum_posts
        LEFT JOIN members ON forum_posts.join_members_id = members.members_id
        WHERE forum_posts.parent_id = ?
        ORDER BY forum_posts.post_date
        DESC LIMIT 1', array($topic['forum_post_id']));
    if($last_post)
    {
        $topic['last_post'] = $last_post;
    }
    else
    {
        $topic['last_post'] = $topic;
    }
    $topic['teaser'] = substr(html::filter(strip_tags(html::bbcode_format($topic['body']))), 0, 160);
    unset($topic);
}
?>
