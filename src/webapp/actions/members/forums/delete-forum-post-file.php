<?php
$db = mysqli_db::init();

$forums_posts_files_id = $_POST['forum_posts_files_id'];

$file = $db->fetch_one('
	SELECT * FROM forums_posts_files WHERE forums_posts_files_id = ?
', array($forums_posts_files_id));

$file_path = SITE_PATH . '/data/' . $file['forums_posts_files_filename'];

@unlink($file_path);

$db->query('DELETE FROM forums_posts_files WHERE forums_posts_files_id = ?', array($forums_posts_files_id));

?>