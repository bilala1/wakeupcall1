<?
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$downloads = new mysqli_db_table('downloads');

$forums_posts_file = $db->fetch_one('select * from forums_posts_files where forums_posts_files_id = ?', array($_REQUEST['forums_posts_files_id']));

if($forums_posts_file){
    //record download
    $downloads->insert(array(
        'downloads_type' => 'forum file',
        'join_id' => $_REQUEST['forums_posts_files_id'],
        'join_members_id' => ActiveMemberInfo::GetMemberId(),
        'downloads_datetime' => SQL('NOW()')
    ));
    
    members::log_action(ActiveMemberInfo::GetMemberId(), 'Downloaded from Forum: '.$forums_posts_file['forums_posts_files_original_filename']);
    
    $filename = files::unique_sym_name($forums_posts_file['forums_posts_files_original_filename']);
    
    // Create symlink
    exec('ln -s '.SITE_PATH.'/data/'.$forums_posts_file['forums_posts_files_filename'].' "'.SITE_PATH.'/data/syms/'.$filename.'"');
    http::redirect(BASEURL .'/data/syms/'.$filename);
}else{
    echo 'You cannot download this file';
}

?>