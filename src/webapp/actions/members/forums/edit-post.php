<?php
$db = mysqli_db::init();

$forums = new forums;
$breadcrumb = $forums->build_trail($forums->get_topic_breadcrumb($_REQUEST['topics_id']));

if ($_REQUEST['posts_id']) {
	$post = $db->fetch_one('
		SELECT
		p.*,
		m.members_id,
		CONCAT(m2.members_firstname, " ", m2.members_lastname) as editor
		FROM forums_posts AS p
		LEFT JOIN members AS m ON m.members_id = p.join_members_id
		LEFT JOIN members AS m2 ON m2.members_id = p.posts_modified_join_members_id
		WHERE p.posts_id = ?
	', array($_REQUEST['posts_id']));
	$post['posts_enabled'] = unserialize($post['posts_enabled']);

	$post['posts_body'] = strip_tags($post['posts_body']);

	if ($post['join_members_id'] != ActiveMemberInfo::GetMemberId()) {
		if (!empty($_REQUEST['topics_id'])) {
			http::redirect(FULLURL . '/members/forums/view-topic.php?topics_id=' . $_REQUEST['topics_id']);
		} else {
			http::redirect(FULLURL . '/members/forums/view-forum.php?forums_id=1');
		}
	}
} else {
	if (!empty($_REQUEST['topics_id'])) {
		http::redirect(FULLURL . '/members/forums/new-reply.php?topics_id=' . $_REQUEST['topics_id']);
	} else {
		http::redirect(FULLURL . '/members/forums/view-forum.php?forums_id=1');
	}
}

if ($_POST) {
	$data = $_REQUEST;

	$data['posts_body'] = nl2br($data['posts_body']);

	$data['posts_enabled'] = serialize(array(
										   'bbcode' => $data['enable_bbcode'],
										   'signature' => $data['enable_signature']
									   ));
	unset($data['enable_bbcode'], $data['enable_signature']);

	//-- stupid.... but whatever. -_-
	$data['join_topics_id'] = $data['topics_id'];
	unset($data['topics_id']);

	$data['join_members_id'] = $data['posts_modified_join_members_id'] = ActiveMemberInfo::GetMemberId();

	$data['posts_modified_date'] = date('Y-m-d H:i:s');

	$posts_table = new mysqli_db_table('forums_posts');
	$posts_table->update($data, $post['posts_id']);

	$all_topic_posts = $db->fetch_singlets('
		SELECT posts_id FROM forums_posts
		WHERE join_topics_id = ?
	', array($_REQUEST['topics_id']));
    $page = ceil(count($all_topic_posts) / 5) - 1; // 5 posts per page
    $page = ($page >= 0) ? $page : 0;

	http::redirect(FULLURL . '/members/forums/view-topic.php?p='.$page.'&topics_id='.$_REQUEST['topics_id'], array('notice' => 'Your post has been updated'));
}

?>