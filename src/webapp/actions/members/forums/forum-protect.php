<?php
include(ACTIONS . '/members/protect.php');

$forums_id = isset($_REQUEST['forums_id']) ? $_REQUEST['forums_id'] : forums::get_forum_id_by_topic($_REQUEST['topics_id']);

$forum_permissions = groups::get_user_permissions($forums_id);

if(!$forum_permissions['view']){
    //strings::pre_dump($forum_permissions);
    //http::redirect(HTTP_BASEURL . '/members/forums/index.php?notice='.urlencode('You are not allowed to view this forum'));
    http::redirect(HTTP_BASEURL . '/members/index.php', array('notice' => 'You are not allowed to view this forum'));
}

unset($forums_id);
?>