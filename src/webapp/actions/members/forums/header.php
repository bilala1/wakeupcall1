<?php
include_once ACTIONS . '/members/protect.php';

$db = mysqli_db::init();

$users = new members();
$forum_user = $users->get(ActiveMemberInfo::GetMemberId());
$forum_user['clusters'] = $db->fetch_all('SELECT * FROM members_x_clusters WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$forum_categories = array();

foreach($forum_user['clusters'] as $cluster)
{
    $forum_categories = array_merge($forum_categories, 
        $db->fetch_all('SELECT * FROM forum_categories 
            WHERE FIND_IN_SET(?, clusters) 
            ORDER BY zorder, name', 
            array($cluster['join_clusters_id'])
        )
   );
}
$forum_categories = array_unique($forum_categories);
?>
