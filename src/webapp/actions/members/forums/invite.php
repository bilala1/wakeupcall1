<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if(!ActiveMemberInfo::GetMemberId()){
    framework::set_view('no-access.php');
    return;
}
$forums = new forums;
$recent_posts = $forums->get_recent_posts();

$tags      = tags::get_tags('forums');
$tag_count = tags::get_count('forums');

if($_POST){
    foreach($_POST['emails'] as $email){
        if(strings::is_email($email)){
            // Send the email...
            
            ob_start();
            include(VIEWS .'/emails/forums-invite.php');
            $message = ob_get_contents();
            ob_end_clean();
            
            mail::send(
                $from = AUTO_EMAIL,
                $to = $email,
                'You\'ve been invited! | ' . SITE_NAME,
                $message = $message
            );
        }
    }
    
    $notice = 'Your invitations have been sent.';
}

?>
