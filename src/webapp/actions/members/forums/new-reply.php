<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

//------------------------------------------------------------------------------
//-- Forum Permissions
//------------------------------------------------------------------------------
$forum_permissions = groups::get_user_permissions(forums::get_forum_id_by_topic($_REQUEST['topics_id']));
if(!$forum_permissions['reply']){
    http::redirect(HTTP_BASEURL . '/members/forums/view-topic.php?topics_id='.$_REQUEST['topics_id'].'&notice='.urlencode('You are not allowed to create a reply on this forum'));
}

$db = mysqli_db::init();

$topic = $db->fetch_one('SELECT * FROM forums_topics WHERE topics_id = ?', array($_REQUEST['topics_id']));
if($topic){ // Good There's a topic...
    if($topic['topics_status'] == 'locked'){
        framework::set_view('no-access.php');
        return;
    }
}
else {
    framework::set_view('no-access.php');
    return;
}

$forums = new forums;
$breadcrumb = $forums->build_trail($forums->get_topic_breadcrumb($_REQUEST['topics_id']));

// Find all of the quoted posts...
$quoted = '';
foreach((array) $_SESSION['multiquote'][$_REQUEST['topics_id']] as $posts_id){
    $post = $db->fetch_one('SELECT p.*, CONCAT(members_firstname, " ", members_lastname) as members_username FROM forums_posts AS p
        LEFT JOIN members AS m ON m.members_id = p.join_members_id
        WHERE p.posts_id = ?
        GROUP BY p.posts_id', array($posts_id));

    $quoted .= '[quote][b]'.$post['members_username'].'[/b] said...'."\n";
    $quoted .= $post['posts_body'];
    $quoted .= '[/quote]'."\n";
} unset($post);


if($_POST){
    $topics = new mysqli_db_table('forums_topics');
    $posts  = new mysqli_db_table('forums_posts');
    $data   = $_POST;

    if(empty($_POST['posts_body']) || $_POST['posts_body'] == '&nbsp;'){
        $errors[] = 'You must enter a post body';
    }

    if(!sizeof($errors)){
        unset($_SESSION['multiquote'][$_REQUEST['topics_id']]);

        // Topics id (need to update it a sec...)
        $topics_id = $_REQUEST['topics_id'];

		$data['enable_bbcode'] = 1;
        $enabled = serialize(array(
         	'bbcode' => $data['enable_bbcode'],
            'signature' => $data['enable_signature']
        ));

		//-- filter out the bad words!
		$bad_words = $db->fetch_singlets('
			SELECT bad_words_word
			FROM bad_words
		');
		foreach ($bad_words as $bad_word) {
			$bad_word = addcslashes($bad_word, '*$()\?-+!%&^');
			$bad_word = trim($bad_word);
			$data['posts_body'] = preg_replace('/\\b'.$bad_word.'\\b/i', ' [censored]', $data['posts_body']);

		}
        $post_body = str_replace('&nbsp;', ' ', wordwrap($data['posts_body'], 50, " ", false));
        $post_body = str_replace('[ /url]', '[/url]',$post_body);
        $posts->insert(array(
            'join_topics_id' => $topics_id,
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'posts_postdate' => SQL('UTC_TIMESTAMP()'),
            //'posts_tags' => implode(',', (array) tags::add_tags($data['posts_tags'])),
            'posts_body' => $post_body, // break LONG string into smaller of 60 chars
            'posts_enabled' => $enabled,
            'posts_approved' => forums::post_needs_approval(forums::get_forum_id_by_topic($_REQUEST['topics_id'])) ? 0 : 1
        ));
        $posts_id = $posts->last_id();
        members::log_action(ActiveMemberInfo::GetMemberId(), "Posted on the forum");

        //add files to post
        if($data['files']){
            $posts_files  = new mysqli_db_table('forums_posts_files');
            foreach($data['files'] as $filename){
                $filename = explode('~~',$filename);
                $posts_files->insert(array(
                    'join_posts_id' => $posts_id,
                    'join_members_id' => ActiveMemberInfo::GetMemberId(),
                    'forums_posts_files_datetime' => SQL('UTC_TIMESTAMP'),
                    'forums_posts_files_original_filename' => $filename[0],
                    'forums_posts_files_filename' => $filename[1],
                    'forums_posts_files_size' => filesize(FORUM_UPLOAD_DIR . $filename[1]),
                    'forums_posts_files_ext' => file::get_extension($filename[1])
                ));
            }
        }

        tags::recalculate_forums(); // Recalculate usage

        $topics->update(array(
            'topics_last_posts_id' => $posts_id,
            'topics_last_posts_date' => SQL('UTC_TIMESTAMP()'),
        ), $topics_id);

        $forums = new forums;
        $total_posts = $forums->get_posts_total($topics_id);
        $page = ceil($total_posts / 5) - 1; // 5 posts per page
        $page = ($page >= 0) ? $page : 0;

        if($data['add_watchdog']){
            $watchdogs = new mysqli_db_table('forums_watchdogs');
            $watchdogs->where('join_topics_id = '.$db->filter($topics_id))
                ->where('join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()))
                ->delete();

            $watchdogs->insert(array(
                'join_members_id' => ActiveMemberInfo::GetMemberId(),
                'join_topics_id' => $topics_id,
                'watchdogs_datestamp' => SQL('UTC_TIMESTAMP()'),
            ));
        }

        // Go through each watchdog other than the current member and send them an update... (dear god)
        $dogs = $db->fetch_all('SELECT members_email, members_firstname FROM forums_watchdogs AS w
            LEFT JOIN members AS m ON m.members_id = w.join_members_id
            WHERE m.members_id != ?
            AND m.members_status != "locked"
            AND w.join_topics_id = ?
            GROUP BY m.members_id', array(ActiveMemberInfo::GetMemberId(), $topics_id));

        $dogs[] = array('members_email' => MONITOR_EMAIL, 'members_firstname' => 'Monitor');
        $tos = array();
        foreach ($dogs as $member) {
            $tos[$member['members_email']] = $member;
        }
        ob_start();
        include(VIEWS .'/emails/forum-new-reply.php');
        $message = ob_get_contents();
        ob_clean();

        mail::batchSend(AUTO_EMAIL, $tos, 'New Reply To A Watched Topic | ' . SITE_NAME, $message);

        http::redirect(BASEURL .'/members/forums/view-topic.php?topics_id='.$topics_id.'&p='.$page);
    }
}

?>
