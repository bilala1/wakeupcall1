<?php
include(ACTIONS . '/members/protect.php');

$forums = new forums;
#pre($_SESSION);
//------------------------------------------------------------------------------
//-- Forum Permissions
//------------------------------------------------------------------------------
if($_REQUEST['topics_id']){
    $topic = $forums->get_topic($_REQUEST['topics_id']);
    $_REQUEST['forums_id'] = $topic['join_forums_id'];

    $files = $forums->get_files($topic['posts_id']);
}


$forum_permissions = groups::get_user_permissions($_REQUEST['forums_id']);
if(!$forum_permissions['new_topic']){
    http::redirect(HTTP_BASEURL . '/members/forums/view-forum.php?forums_id='.$_REQUEST['forums_id'].'&notice='.urlencode('You are not allowed to add/edit a new post to this forum'));
}

$db = mysqli_db::init();

$breadcrumb = $forums->build_trail($forums->get_forum_breadcrumb($_REQUEST['forums_id']));

if($_POST){
    $topics = new mysqli_db_table('forums_topics');
    $posts  = new mysqli_db_table('forums_posts');
    $data   = $_POST;

    //if(empty($data['posts_body'])){
      //  $errors[] = 'You must enter a topic body';
    //}

    if(empty($data['topics_title'])){
        $errors[] = 'You must enter a topic title';
    }

    if(!sizeof($errors)){
        if($data['topics_id']){
            $topics->update(array(
                'topics_title' => $data['topics_title'],
                'topics_stickied' => $data['sticky_topic'] && ActiveMemberInfo::_IsAccountAdmin() ? 1 : 0
            ), $data['topics_id']);

            $topics_id = $data['topics_id'];
            $notice = 'The topic has been updated';
        }else{
            $topics->insert(array(
                'join_forums_id' => $data['forums_id'],
                'join_members_id' => ActiveMemberInfo::GetMemberId(),
                'topics_title' => html::filter($data['topics_title']),
                'topics_createdate' => SQL('UTC_TIMESTAMP()'),
                'topics_status' => 'active',
                'topics_approved' => forums::topic_needs_approval($data['forums_id']) ? 0 : 1,
                'topics_stickied' => $data['sticky_topic'] && ActiveMemberInfo::_IsAccountAdmin() ? 1 : 0
            ));

            // Topics id (need to update it a sec...)
            $topics_id = $topics->last_id();
            $notice = 'The topic has been created';
            members::log_action(ActiveMemberInfo::GetMemberId(), "Created a new topic on the forum: " . html::filter($data['topics_title']));
        }

        $enabled = serialize(array(
            'bbcode' => 1,
            'signature' => $data['enable_signature']
        ));

        if($data['topics_id']){
            $posts->update(array(
                'posts_body' => htmlentities(wordwrap($data['posts_body'], 50, " ", false)),
                'posts_tags' => implode(',', tags::add_tags($data['topics_tags'])),
                'posts_enabled' => $enabled
            ), $topic['posts_id']);

            $posts_id = $topic['posts_id'];
        }else{
            $post_body = htmlentities(wordwrap($data['posts_body'], 50, " ", false));
            $post_body = str_replace('[ /url]', '[/url]',$post_body);
            $posts->insert(array(
                'join_topics_id' => $topics_id,
                'join_members_id' => ActiveMemberInfo::GetMemberId(),
                'posts_postdate' => SQL('UTC_TIMESTAMP()'),
                'posts_tags' => implode(',', tags::add_tags($data['topics_tags'])),
                'posts_body' => $post_body,
                'posts_enabled' => $enabled,
                'posts_approved' => forums::post_needs_approval($data['forums_id']) ? 0 : 1
            ));

            $posts_id = $posts->last_id();
        }

        //add files to post
        if($data['files']){
            $posts_files  = new mysqli_db_table('forums_posts_files');
            foreach($data['files'] as $filename){
                $filename = explode('~~',$filename);
                $posts_files->insert(array(
                    'join_posts_id' => $posts_id,
                    'join_members_id' => ActiveMemberInfo::GetMemberId(),
                    'forums_posts_files_datetime' => SQL('UTC_TIMESTAMP'),
                    'forums_posts_files_original_filename' => $filename[0],
                    'forums_posts_files_filename' => $filename[1],
                    'forums_posts_files_size' => filesize(FORUM_UPLOAD_DIR . $filename[1]),
                    'forums_posts_files_ext' => file::get_extension($filename[1])
                ));
            }
        }

        $topics->update(array(
            'topics_last_posts_id' => $posts_id,
            'topics_last_posts_date' => SQL('UTC_TIMESTAMP()'),
        ), $topics_id);

        /*points::addpoints(10, ActiveMemberInfo::GetMemberId(), 'forums', array(
            'topics_id' => $topics_id
        ));*/

        if($data['add_watchdog']){
            $watchdogs = new mysqli_db_table('forums_watchdogs');
            $watchdogs->where('join_topics_id = '.$db->filter($topics_id))
                ->where('join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()))
                ->delete();

            $watchdogs->insert(array(
                'join_members_id' => ActiveMemberInfo::GetMemberId(),
                'join_topics_id' => $topics_id,
                'watchdogs_datestamp' => SQL('UTC_TIMESTAMP()'),
            ));
        }

        //If new topic, and in corporate forum, then email hotels in corporation
        if(!$data['topics_id'] && $data['forums_id'] != 0 && !forums::is_in_message_forum($data['forums_id'])){
            $members = $db->fetch_all('
                select members_email, members_firstname, c.corporations_name
                from members m
                inner join accounts a on m.join_accounts_id = a.accounts_id
                inner join corporations c on c.join_accounts_id = a.accounts_id
                where join_forums_id = ?
                and members_id != ?', array($data['forums_id'], ActiveMemberInfo::GetMemberId()));

            if(count($members) > 0) {
                $data['corporations_name'] = $members[0]['corporations_name'];
                $tos = array();
                foreach ($members as $member) {
                    $tos[$member['members_email']] = $member;
                }
                ob_start();
                include VIEWS . '/emails/forums-new-topic-corporate-forum.php';
                $message = ob_get_contents();
                ob_clean();

                mail::batchSend(AUTO_EMAIL, $tos, 'New Topic in ' . $member['corporations_name'] . ' Forum | ' . SITE_NAME, $message);
            }
        }
        
        //If new topic, and in message forum, then email members
        if(!$data['topics_id'] && ($data['forums_id'] == 0 || forums::is_in_message_forum($data['forums_id']))){
            //TODO: check this file for member_status != "locked" ?
            $members = $db->fetch_all('
                select members_email, members_firstname
                from members
                join notifications on join_members_id = members_id
                where notifications_forum_update = 1
                  and members_status != "locked"'
            );

            $members[] = array('members_email' => MONITOR_EMAIL, 'members_firstname' => 'Monitor');
            $tos = array();
            foreach ($members as $member) {
                $tos[$member['members_email']] = $member;
            }
            ob_start();
            include VIEWS . '/emails/forums-new-topic-message-forum.php';
            $message = ob_get_contents();
            ob_clean();

            mail::batchSend(AUTO_EMAIL, $tos, 'New Topic in Message Forum | ' . SITE_NAME, $message);
        }

        tags::recalculate_forums();

        http::redirect(FULLURL .'/members/forums/view-topic.php?topics_id='.$topics_id.'&notice='.urlencode($notice));
    }
}

?>
