<?php
include(ACTIONS . '/members/protect.php');

include_once 'header.php';

$category = $db->fetch_one('SELECT * FROM forum_categories WHERE forum_category_id = ?', array($_REQUEST['forum_category_id']));

$errors = array();

if($_REQUEST['forum_post_id'])
{
    $topic = $db->fetch_one('SELECT forum_posts.*, members.members_username AS author,
                members.members_avatar, members.members_forum_signature
            FROM forum_posts
            LEFT JOIN members ON forum_posts.join_members_id = members.members_id
            WHERE forum_posts.forum_post_id = ?', array($_REQUEST['forum_post_id']));
    $posts = $db->fetch_all('
    (
        SELECT forum_posts.*, members.members_username AS author,
                members.members_avatar, members.members_forum_signature
            FROM forum_posts
            LEFT JOIN members ON forum_posts.join_members_id = members.members_id
            WHERE forum_posts.forum_post_id = ?
    ) UNION (
        SELECT forum_posts.*, members.members_username AS author,
                members.members_avatar, members.members_forum_signature
            FROM forum_posts
            LEFT JOIN members ON forum_posts.join_members_id = members.members_id
            WHERE forum_posts.parent_id = ?
    )', array($_REQUEST['forum_post_id'], $_REQUEST['forum_post_id']));
    foreach ($posts as &$post)
    {
        $post['body'] = html::bbcode_format($post['body']);
        unset($post);
    }
}

if(!empty($_POST))
{
    $data = $_POST;
    $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
    $data['post_date']       = SQL('NOW()');
    $data['body']            = strip_tags($data['body']);


    $db->fetch_singlet('
        SELECT DISTINCT forum_category_id FROM forum_categories
        LEFT JOIN members_x_clusters ON (FIND_IN_SET(join_clusters_id, forum_categories.clusters) AND join_members_id = ?)
        WHERE forum_category_id=?
        ', array(ActiveMemberInfo::GetMemberId(), $_REQUEST['forum_post_id'])
    );


    if (count(array_intersect($in_clusters, $allowed_groups)) > 0) {
        $errors['title'] = 'You are not allowed to post a topic in this category';
    }

    if(empty($data['title']))
    {
        $errors['title'] = 'Please fill in a topic title';
    }
    if(empty($data['body']))
    {
        $errors['body'] = 'Please fill in a topic body';
    }

    if(empty($errors)) {
        $posts = new forum_posts();
        $posts->insert($data);
        $post_id = $posts->last_id();

        $topic_id = $data['parent_id'] ? $data['parent_id'] : $post_id;

        http::redirect(BASEURL . '/members/forums/post.php?forum_post_id=' . $topic_id . '&notice=' . urlencode('Your ' . ($data['parent_id'] ? 'Reply' : 'Topic') . ' has been posted'));
    }
}
else
{
    $data = array(
        'title' => '',
        'body' => ''
    );
}

?>
