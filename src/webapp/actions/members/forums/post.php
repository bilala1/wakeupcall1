<?php
include(ACTIONS . '/members/protect.php');

include_once 'header.php';

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = isset($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = isset($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5; // only should 5 pages left or right
//------------------------------------------------------------------------------
//-- Get Posts
//------------------------------------------------------------------------------

$topic = $db->fetch_one('SELECT forum_posts.*, members.members_username AS author,
            members.members_avatar, members.members_forum_signature
        FROM forum_posts
        LEFT JOIN members ON forum_posts.join_members_id = members.members_id
        WHERE forum_posts.forum_post_id = ?', array($_REQUEST['forum_post_id']));
$category = $db->fetch_one('SELECT * FROM forum_categories WHERE forum_category_id = ?', array($topic['join_forum_category_id']));
$posts = $db->fetch_all('
    (
        SELECT forum_posts.*, members.members_username AS author,
                members.members_avatar, members.members_forum_signature,
                members.members_id,
                DATE_FORMAT(forum_posts.post_date, "' . MYSQL_DATE . '") AS post_date
            FROM forum_posts
            LEFT JOIN members ON forum_posts.join_members_id = members.members_id
            WHERE forum_posts.forum_post_id = ?
    ) UNION (
        SELECT forum_posts.*, members.members_username AS author,
                members.members_avatar, members.members_forum_signature,
                members.members_id,
                DATE_FORMAT(forum_posts.post_date, "' . MYSQL_DATE . '") AS post_date
            FROM forum_posts
            LEFT JOIN members ON forum_posts.join_members_id = members.members_id
            WHERE forum_posts.parent_id = ?
    )
    LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
    array($_REQUEST['forum_post_id'], $_REQUEST['forum_post_id']));
//------------------------------------------------------------------------------
//-- Get Total Posts
//------------------------------------------------------------------------------
$total = $db->fetch_singlet('SELECT COUNT(*) FROM forum_posts WHERE parent_id = ?', array($topic['forum_post_id'])) + 1; // Add one becuase of topic is + 1 + replies
$pages = ceil($total / $per_page);
//------------------------------------------------------------------------------
//-- Get Pagination
//------------------------------------------------------------------------------
$pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/members/forums/post.php');

$topic['body'] = html::bbcode_format($topic['body']);
$topic['body'] = nl2br($topic['body']);

foreach($posts as &$post)
{
    $post['body'] = html::bbcode_format($post['body']);
    $post['body'] = nl2br($post['body']);

    unset($post);
}


if(isset($_REQUEST['notice']))
{
    $notice = strip_tags(urldecode($_REQUEST['notice']));
}
?>
