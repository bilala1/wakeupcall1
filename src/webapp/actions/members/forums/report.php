<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

//------------------------------------------------------------------------------
//-- Forum Permissions
//------------------------------------------------------------------------------
/*$forum_permissions = groups::get_user_permissions(forums::get_forum_id_by_post($_REQUEST['posts_id']));
if(!$forum_permissions['view']){
    http::redirect(HTTP_BASEURL . '/members/forums/view-topic.php?topics_id='.$_REQUEST['topics_id'].'&notice='.urlencode('You are not allowed to create a reply on this forum'));
}*/

$db = mysqli_db::init();
$member = $db->fetch_one('SELECT members_firstname,members_lastname
                            FROM members
                            WHERE members_id = ?',
                            array(ActiveMemberInfo::GetMemberId()));

// Lets see if this is a legitimate post...
$post = $db->fetch_one('SELECT * 
    FROM forums_posts AS fp
    LEFT JOIN forums_topics AS ft ON (ft.topics_id = fp.join_topics_id)
    WHERE posts_id = ?', array($_REQUEST['posts_id']));
if(!$post){
    http::redirect(BASEURL .'/members/forums/index.php');
}
$reasons = $db->fetch_all('SELECT * FROM report_reasons');

if($_POST){
    $data = $_POST;
    
    if(empty($data['reasons_id'])){
        $errors[] = 'Please select a reason to report this post.';
    }

    if(empty($data['report_desc'])){
        $errors[] = 'Please include a description of this report.';
    }

    if(!sizeof($errors)){
        $fpr = new mysqli_db_table('forums_posts_reported');

        $fpr->insert(array(
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'join_posts_id' => $data['posts_id'],
            'join_report_reasons_id' => $data['reasons_id'],
            'posts_reported_datetime' => SQL('UTC_TIMESTAMP()'),
            'posts_reported_desc' => $data['report_desc']
        ));
        
        
        //send an alert email to monitor@wakeupcall.net
        $subject = 'Abuse Reported in Message Forum';
        $from = AUTO_EMAIL;
        $to = MONITOR_EMAIL;
        
        // Email Monitor/Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forum-report-abuse.php');
		$message = ob_get_contents();
		ob_end_clean();        
        /*
        $message = '<p>Dear Admin/Monitor,</p>
        <p>A member has reported abuse in the forums.  You can take action by clicking
        <a href="' . FULLURL . '/admin/forums/view-post.php?posts_id=' . $data['posts_id'] . '">here</a>.</p>
        WAKEUP CALL';
        */
        #echo $message;exit;
        mail::send($from,$to,$subject,$message);
        
        http::redirect(BASEURL .'/members/forums/report-thankyou.php?topics_id='.$post['join_topics_id']);
    }else{
        http::redirect(BASEURL .'/members/forums/report.php?posts_id='.$data['posts_id'].'&notice='.urlencode(implode('<br />',$errors)));
    }
}

?>
