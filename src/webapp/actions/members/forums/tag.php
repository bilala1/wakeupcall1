<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$forums = new forums;

$tag = $db->fetch_one('SELECT * FROM global_tags WHERE global_tags_id = ?', array($_REQUEST['global_tags_id']));

$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5;

$total = $db->fetch_singlet('
    SELECT COUNT(*)
    FROM forums_posts
    LEFT JOIN forums_topics on topics_id = join_topics_id
    WHERE
        FIND_IN_SET(?, posts_tags) and
        join_forums_id = ?', array($_REQUEST['global_tags_id'], $_REQUEST['join_forums_id']));

$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages);

// Let's find all posts where the tag is used...
$posts = $forums->get_posts_by_forum_tag($_REQUEST['join_forums_id'], $_REQUEST['global_tags_id'], ($current_page * $per_page), $per_page);

?>
