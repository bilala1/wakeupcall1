<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/forums/forum-protect.php');
$db = mysqli_db::init();
//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 10;
$max_pages = 5;

//$db = mysqli_db::init();

$forums = new forums;

$topics     = $forums->get_topics($_REQUEST['forums_id'], ($current_page * $per_page), $per_page);
$breadcrumb = $forums->build_trail($forums->get_forum_breadcrumb($_REQUEST['forums_id']));
$forum      = $forums->get_forum($_REQUEST['forums_id']);

$total = $forums->get_topics_total($_REQUEST['forums_id']);
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages);

$tags      = tags::get_tags('forums');
$tag_count = tags::get_count('forums');

// whether collapse or expand information box at the top of the page
$infobox_corporate_forum = $db->fetch_singlet('SELECT members_settings_infobox_corporate_forum FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));
$infobox_message_forum = $db->fetch_singlet('SELECT members_settings_infobox_message_forum FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


//notification checkbox
$notifications = $db->fetch_one('
    SELECT notifications_forum_update
    FROM notifications 
    WHERE join_members_id = ?',
    array(ActiveMemberInfo::GetMemberId())
);

?>
