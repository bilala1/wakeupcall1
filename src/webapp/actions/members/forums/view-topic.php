<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/forums/forum-protect.php');

$db = mysqli_db::init();

//------------------------------------------------------------------------------
//-- Track Forum Topic Visits
//------------------------------------------------------------------------------
    //get row if exists
    $exists = $db->fetch_singlet('SELECT COUNT(*) 
                                  FROM forums_topics_visits 
                                  WHERE join_forums_topics_id = ?
                                  AND join_members_id = ?', array($_REQUEST['topics_id'],ActiveMemberInfo::GetMemberId()));
    if($exists){ // update
        $db->query('UPDATE forums_topics_visits
                    SET forums_topics_visits_datetime = NOW()
                    WHERE join_forums_topics_id = ?
                    AND join_members_id = ?', array($_REQUEST['topics_id'],ActiveMemberInfo::GetMemberId()));
    }
    else { // insert
        $db->query('INSERT INTO forums_topics_visits
                    VALUES(NULL,?,?,NOW())',array($_REQUEST['topics_id'],ActiveMemberInfo::GetMemberId()));
    }

//------------------------------------------------------------------------------
//-- Pagination Settings
//------------------------------------------------------------------------------
$current_page = ($_REQUEST['p']) ? intval($_REQUEST['p']) : 0;
$per_page = ($_REQUEST['perpg']) ? intval($_REQUEST['perpg']) : 5;
$max_pages = 5;

$forums = new forums;
$breadcrumb = $forums->build_trail($forums->get_topic_breadcrumb($_REQUEST['topics_id']));

$forums_id = $db->fetch_singlet('SELECT join_forums_id FROM forums_topics WHERE topics_id = ?',
                            array($_REQUEST['topics_id']));
/*
$forums_id = array();
$referer = parse_url($_SERVER['HTTP_REFERER']);
$query = explode('&',$referer['query']);
foreach($query as $q) {
    if(stristr($q,'forums_id')) {
        $forums_id = explode('=',$q);
        $forums_id = $forums_id[1];
    }
}
*/

$total = $forums->get_posts_total($_REQUEST['topics_id']);
$pages = ceil($total / $per_page);
$pages = html::paginate($_GET, $current_page, $max_pages, $pages);

// Update the views on the topic...
$db->query('UPDATE forums_topics SET topics_views = (topics_views + 1) WHERE topics_id = ?', array($_REQUEST['topics_id']));

$topic = $forums->get_topic($_REQUEST['topics_id']);
$posts = $forums->get_posts($_REQUEST['topics_id'], ($current_page * $per_page), $per_page);
if($current_page == 0){
    $topic_post = $posts[0];
}
// Is this member watching this post?
$watching = $db->fetch_singlet('SELECT COUNT(*) FROM forums_watchdogs
    WHERE join_members_id = ?
    AND join_topics_id = ?', array(ActiveMemberInfo::GetMemberId(), $_REQUEST['topics_id']));

$tags      = tags::get_tags('forums');
$tag_count = tags::get_count('forums');

include(ACTIONS . '/members/forums/new-reply.php');

?>
