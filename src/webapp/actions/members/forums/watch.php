<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$topics_id = (int) $_REQUEST['topics_id'];
$db = mysqli_db::init();

if(ActiveMemberInfo::GetMemberId()){
    $watchdogs = new mysqli_db_table('forums_watchdogs');
    $watchdogs->where('join_topics_id = '.$db->filter($topics_id))
        ->where('join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()))
        ->delete();

    $watchdogs->insert(array(
        'join_members_id' => ActiveMemberInfo::GetMemberId(),
        'join_topics_id' => $topics_id,
        'watchdogs_datestamp' => SQL('UTC_TIMESTAMP()'),
    ));
}

http::redirect(BASEURL .'/members/forums/view-topic.php?topics_id='.$topics_id);


?>
