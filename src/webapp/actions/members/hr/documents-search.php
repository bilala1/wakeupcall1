<?php

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$paging = new paging(10, $_REQUEST['page']);

$wheres = array();
$params = array();

if($_REQUEST['documents_title']){
    $wheres[] = 'documents_title LIKE ?';
    $params[] = '%'.$_REQUEST['documents_title'].'%';
}
// ------------------------------------
//- only display docs in HR category (8)
// ------------------------------------
    $wheres[] = 'join_library_categories_id = ? ';
    $params[] = 8;
    
    
$documents = $db->fetch_all('
    SELECT SQL_CALC_FOUND_ROWS *
    FROM documents '.
    strings::where($wheres).
    $db->orderby('documents_title').
    $paging->get_limit()
    , $params
);
//pre($db->debug());

$paging->set_total($db->found_rows());

?>