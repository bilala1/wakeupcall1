<?php

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$forwardUrl=$_REQUEST['url'];

$agreement_yn = $db->fetch_singlet('SELECT members_hr_agreement_yn FROM members WHERE members_id = ?', array(ActiveMemberInfo::GetMemberId()));

if($agreement_yn != '1') {
    $redirectUrl = 'index.php';
    if($forwardUrl) {
        $redirectUrl .= '?url='.urlencode($forwardUrl);
    }
} else {
    $redirectUrl = 'http://www.hr360.com/autologin.aspx?GUID=3e64529e-8af7-498e-a737-8413631ade91';
    if($forwardUrl) {
        $redirectUrl .= '&ReturnURL='.urlencode($forwardUrl);
    }
}

http::redirect($redirectUrl);