<?php

//$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$member = members::get_by_id(ActiveMemberInfo::GetMemberId());

$forwardUrl = $_REQUEST['url'];

if($_POST['submit']){
    if($_POST['agree_with_agreement']){
        if($_POST['agree_with_agreement']==1){
                $db->query('UPDATE members
                            SET members_hr_agreement_yn = 1, members_hr_agreement_datetime = ? 
                            WHERE members_id = ? ', array(SQL('NOW()'),ActiveMemberInfo::GetMemberId()));
        }
    }
    else {
            $errors['agree_with_agreement'] = 'You have to agree to use this service.';
        }
}

$agreement_yn = $db->fetch_singlet('SELECT members_hr_agreement_yn FROM members WHERE members_id = ?', array(ActiveMemberInfo::GetMemberId()));

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_hr FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>
