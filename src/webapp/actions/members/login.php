<?php
$db = mysqli_db::init();

if ($_POST) {

    $member = $db->fetch_one('select * from members where members_email = ? and members_password = ?', array($_POST['login_email'], hash('sha256', $_POST['login_password'])));


    if ($member && //member record exists
        // if member is paid member OR during 15 days of free trial
        (members::get_members_status($member['members_id']) == 'member' || (members::get_members_status($member['members_id']) == 'free' && strtotime($member['members_datetime'] . ' + ' . FREE_MEMBER_PERIOD . ' days') >= strtotime('now'))) &&
        // if member's coprporation or hotel has not been removed
        !members::get_entity_deleted($member['members_id']) &&
        !accounts::isLocked($member['join_accounts_id'])
    ) {
        //Do this first since it resets the session
        members::log_member_in($member);

        //Custom logo
        if ($member) {
            //Check if admin of a franchise
            $franchise = $db->fetch_one('
            SELECT *
            FROM franchises
            WHERE join_members_id = ?', array($member['members_id']));

            //TODO ACCOUNTS member could belong to multiple locations with different franchises
            //If not admin, see if related to location for franchise hotel
            if (!$franchise) {
                $franchise = $db->fetch_one('
                    SELECT f.*
                    FROM franchises f
                    INNER JOIN licensed_locations ll on f.franchises_id = ll.join_franchises_id
                    INNER JOIN licensed_locations_x_members_access llm on ll.licensed_locations_id = llm.join_licensed_locations_id
                    WHERE llm.join_members_id = ?', array($member['members_id']));
            }

            //If not associated with franchise hotel, see if account admin is owner of a franchise
            if (!$franchise) {
                $franchise = $db->fetch_one('
                    SELECT f.*
                    FROM franchises f
                    INNER JOIN accounts a on a.join_members_id = f.join_members_id
                    WHERE a.accounts_id = ?', array($member['join_accounts_id']));
            }

            if ($franchise) {
                $photo = $db->fetch_one('
                    SELECT albums_photos_id, albums_photos_filename, join_albums_id
                    FROM albums_photos
                    WHERE albums_photos_id = ?', array($franchise['join_albums_photos_id']));

                $album = $db->fetch_one('
                    SELECT albums_id, albums_root_folder
                    FROM albums
                    WHERE albums_id = ?', array($photo['join_albums_id']));

                $_SESSION['custom_logo_directory'] = $album['albums_root_folder'];
                $_SESSION['custom_logo_file_name'] = $photo['albums_photos_filename'];
            } else {
                if (isset($_SESSION['custom_logo_directory'])) {
                    unset($_SESSION['custom_logo_directory']);
                }

                if (isset($_SESSION['custom_logo_file_name'])) {
                    unset($_SESSION['custom_logo_file_name']);
                }
            }
        }

        $redirect = FULLURL . '/members/index.php';
        if ($_COOKIE['wakeup_redirect']) {
            $redirect = $_COOKIE['wakeup_redirect'];
        // remove welcome video until non-flash solution
        //} elseif ($member['members_show_startup_video'] != 0) {
        //    $redirect = FULLURL . '/members/startup/video.php';
        } elseif (!UserPermissions::UserCanAccessScreen('dashboard')) {
            $redirect = FULLURL . '/members/news/rss-news.php';
        }

        setcookie('wakeup_redirect', '', time()-86400, '/');

        if (http::is_ajax()) {
            //----------------------------------------------------------------------------------
            //-- cookie to remember email - if opted for
            //----------------------------------------------------------------------------------
            if ((int)$_POST['remember_me'] == 1 && $member) {
                setcookie('wakeupcall', $member['members_email'], time() + 2592000, '/'); //create cookie - for a month
            } else {
                setcookie('wakeupcall', $member['members_email'], time() - 3, '/'); //delete cookie
            }
            // ---------------------------------------------------------------------------------
            echo json_encode(array(
                'logged' => 1,
                'show_video' => 0,//$member['members_show_startup_video'],
                'redirect' => $redirect
            ));
        } else {
            http::redirect($redirect);
        }
    } else {
        $renewable = false;
        if (members::get_members_status($member['members_id']) == 'locked'
            || accounts::isLocked($member['join_accounts_id'])) {
            $_SESSION['registration_members_id'] = $member['members_id'];
            $hasPaidBill = accounts::accountHasPaidBill($member['join_accounts_id']);
            $message = $hasPaidBill ? 'Your membership has expired' : 'Your free trial has expired';
            if (members::get_renewable_tf($member['members_id'])) { // if this member is permitted to renew own account
                $_SESSION['registration']['member'] = $member;
                $error = $message . '.';
                $renewable = true;
            } else {
                $error = $message . ': please contact your account manager';
            }
        } // if status is free, but trial time has expired (this should not happen, since expired members assigned 'locked' status
        elseif ((members::get_members_status($member['members_id']) == 'free' && strtotime($member['members_datetime'] . ' + ' . FREE_MEMBER_PERIOD . ' days') < strtotime('now'))) {
            $_SESSION['registration_members_id'] = $member['members_id'];
            if (members::get_renewable_tf($member['members_id'])) { // if this member is permitted to renew own account
                $error = 'Your free trial has expired.';
                $renewable = true;
            } else {
                $error = 'Your free trial has expired: please contact your account manager';
            }
        } // if entitiy was marked deleted, and in 60 days period
        elseif (members::get_entity_deleted($member['members_id'])) {
            $error = members::get_entity_deleted($member['members_id']);
        } else {
            $error = 'Incorrect Login';
        }

        if (http::is_ajax()) {
            echo json_encode(array(
                'error' => $error,
                'renewable' => $renewable,
                'members_id' => $member['members_id']
            ) + $_POST);
        } else {
            http::redirect(FULLURL . '/login.php', array('notice' => $error));
        }
    }


}
?>
