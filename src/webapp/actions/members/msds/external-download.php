<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$id_array = preg_split('/msds_id=/',$_GET['url']);

members::log_msds_download(ActiveMemberInfo::GetMemberId(), 'SDS Download: '.$_GET['msds_name'],$id_array[1]);

http::redirect($_GET['url']);

?>