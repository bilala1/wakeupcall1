<?php
$hidden = null;

// If the start_time param was passed, we need to use it (even if empty)
if($_REQUEST['clear_filter'] == 'clear_filter'){
    $_REQUEST['join_licensed_locations_id']= 0;
    $_REQUEST['certificates_start_time'] = '';
    $_REQUEST['certificates_end_time']= '';
}
if($_REQUEST['quick_filter'] && $_REQUEST['quick_filter']!='' ){
    $_REQUEST['certificates_start_time'] = date('m/d/Y');
    $days = $_REQUEST['quick_filter'];
    $_REQUEST['certificates_end_time'] = date('m/d/Y', strtotime("+$days days"));;
}
if ($_REQUEST['pref']) {
    UserPreferences::setForMember(ActiveMemberInfo::GetMemberId(), 'user_prefs_contract_expire_lead_days', $_REQUEST['pref']);
}
$certificates_start_time = '';
if (array_key_exists('certificates_start_time', $_REQUEST)) {
    if (strtotime($_REQUEST['certificates_start_time'])) {
        $certificates_start_time = $_REQUEST['certificates_start_time'];
    } else {
        $certificates_start_time = '';
    }
    $_SESSION['certificates_start_time'] = $certificates_start_time;
} else if (array_key_exists('certificates_start_time', $_SESSION)) {
    $certificates_start_time = $_SESSION['certificates_start_time'];
}

// If the end_time param was passed, we need to use it (even if empty)
$certificates_end_time = '';
if (array_key_exists('certificates_end_time', $_REQUEST)) {
    if (strtotime($_REQUEST['certificates_end_time'])) {
        $certificates_end_time = $_REQUEST['certificates_end_time'];
    } else {
        $certificates_end_time = '';
    }
    $_SESSION['certificates_end_time'] = $certificates_end_time;
} else if (array_key_exists('certificates_end_time', $_SESSION)) {
    $certificates_end_time = $_SESSION['certificates_end_time'];
}

$certificates_show_hidden = 0;

if (array_key_exists('certificates_show_hidden', $_REQUEST)) {
    $_SESSION['certificates_show_hidden'] = $_REQUEST['certificates_show_hidden'];
    $certificates_show_hidden = $_REQUEST['certificates_show_hidden'];
} else if (array_key_exists('certificates_show_hidden', $_SESSION)) {
    $certificates_show_hidden = $_SESSION['certificates_show_hidden'];
}

if (array_key_exists('join_licensed_locations_id', $_REQUEST)) {
    $filter_location_id = $_REQUEST['join_licensed_locations_id'];
    $_SESSION['certificates_filter_licensed_locations_id'] = $_REQUEST['join_licensed_locations_id'];
} else if (array_key_exists('certificates_filter_licensed_locations_id', $_SESSION)) {
    $filter_location_id = $_SESSION['certificates_filter_licensed_locations_id'];
}

$order = $_REQUEST['order'];