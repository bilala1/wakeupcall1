<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 8/29/15
 * Time: 4:23 AM
 * @param $certificates_id
 * @throws Zend_Mail_Exception
 */
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/CertRequestEmail.php');
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/TmpEmailFileHandler.php');

function EmailCertRequest($certificates_id, $subject = '')
{
    $member = ActiveMemberInfo::GetMember();
    $db = mysqli_db::init();
    $cert = $db->fetch_one('SELECT *
                            FROM certificates AS c
                            INNER JOIN accounts ON join_accounts_id = accounts_id
                            LEFT JOIN vendors AS v ON c.join_vendors_id = v.vendors_id
                            left join licensed_locations ll on licensed_locations_id = join_licensed_locations_id
                            WHERE certificates_id = ?', array($certificates_id));

    if (!$member) {
        // No active member, this is an automated email. Try to grab an admin from the location
        $member = $db->fetch_one(
            'SELECT m.* from members m
              LEFT JOIN licensed_locations_x_members_access lm on lm.join_members_id = members_id
              LEFT JOIN members_access_levels mal ON lm.join_members_access_levels_id = mal.members_access_levels_id
              WHERE lm.join_licensed_locations_id = ?
              AND mal.members_access_levels_type = \'locationAdmin\''
            , array($cert['join_licensed_locations_id']));
    }
    if (!$member) {
        // No location admin, grab main admin
        $member = accounts::getMainAdmin($cert['join_accounts_id']);
    }
    if ($cert['certificates_include_additional_insured_request'] == 1) {
        $certificates_id = $cert['certificates_id'];
        $certificate_legal_names = legal_entities::get_legal_entity_names_for_certificate($certificates_id);
        foreach ($certificate_legal_names as $certificate_legal_name) {
            $additional_insured[] = $certificate_legal_name['legal_entity_names_name'];
        }
    }
    $certificates_coverages = certificates::get_coverags_by_certificates_id($certificates_id);
    foreach ($certificates_coverages as $certificates_coverage) {
        if ($certificates_coverage['certificate_coverages_name'] == "Worker's Compensation (W/C)") {
            $amount = array(
                'amount1' => $certificates_coverage['certificate_coverages_amount_1'],
                'amount2' => $certificates_coverage['certificate_coverages_amount_2'],
                'amount3' => $certificates_coverage['certificate_coverages_amount_3'],
            );
        } else {
            $amount = $certificates_coverage['certificate_coverages_amount_1'];
        }
        $coverages[$certificates_coverage['certificate_coverages_name']] = $amount;
    }

    $certRequestModel = array(
        'vendors_name' => $cert['vendors_name'],
        'vendors_street' => $cert['vendors_street'],
        'loc_name' => $cert['licensed_locations_name'],
        'certificates_coverages' => $coverages,
        'certificates_coverage_other' => '',
        'members_fax' => $cert['licensed_locations_fax'],
        'members_billing_addr1' => $cert['licensed_locations_address'],
        'members_billing_city' => $cert['licensed_locations_city'],
        'members_billing_state' => $cert['licensed_locations_state'],
        'members_billing_zip' => $cert['licensed_locations_zip'],
        'certificates_request_cert_to_email' => $cert['certificates_request_cert_to_email'],
        'certificates_request_cert_to_fax' => $cert['certificates_request_cert_to_fax'],
        'certificates_request_cert_to_address' => $cert['certificates_request_cert_to_address'],
        'certificates_include_additional_insured_request' => $cert['certificates_include_additional_insured_request'],
        'certificates_default_location_name' => $cert['certificates_default_location_name'],
        'certificate_additional_insured' => $additional_insured
    );

    $message = null;
    $template = certificates_email_templates::get_template_for_certificate($certificates_id);
    $default_template = certificates_email_templates::getDefaultTemplate();
    if ($template) {
        $html = stripslashes($template['email_html']);
    } elseif ($default_template) {
        $html = stripslashes($default_template['email_html']);
    } else {
        $html = emails_default_templates::get_certificate_request_template();
    }

    $dynemail = new CertRequestDynamicEmail();
    $message = $dynemail->generate_html($html, $certRequestModel);

    $status = 'pending';
    $status_reason = null;
    if ($cert['accounts_is_locked'] == 1) {
        $status = 'fail';
        $status_reason = 'Account is locked.';
    }

    if (!$subject) {
        $subject = 'Certificate Request';
    }

    $email_sent_to = $cert['vendors_email'];

    $sent_emails_history_id = emails_history::add(
        $cert['join_accounts_id'],
        $cert['join_licensed_locations_id'],
        $subject,
        $message,
        null,
        $certificates_id,
        null,
        $email_sent_to,
        'email',
        $status,
        $status_reason);

    if ($cert['accounts_is_locked'] == 0) {
        $additional_header_data = json_encode(array(
            "event_id" => 'cert-request',
            "vendors_id" => $cert['vendors_id'],
            "vendors_name" => $cert['vendors_name'],
            "members_firstname" => $member['members_firstname'],
            "members_email" => $member['members_email'],
            "certificates_id" => $certificates_id,
            "sent_emails_history_id" => $sent_emails_history_id
        ));


        if (!mail::send(
            $member['members_email'],
            $cert['vendors_email'],
            $subject,
            $message,
            null,
            null,
            'X-Mailgun-Variables:' . $additional_header_data
        )
        ) {
            error_log('There was a problem emailing a vendor cert request.');
        }
    }
}
