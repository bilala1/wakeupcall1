<?
include ACTIONS . '/members/protect.php';

$file = certificates::get_file($_REQUEST['certificates_files_id']);

$status = certificates::deleteFile($_REQUEST['certificates_files_id']);
$notice = $status['notice'];
http::redirect(BASEURL . '/members/my-documents/certificates/edit.php', array('notice' => $notice, 'certificates_id' => $file['join_certificates_id'],'action'=>$_REQUEST['action']));

?>