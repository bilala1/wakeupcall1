<?
use Services\Services;

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$downloads = new mysqli_db_table('downloads');

//this performs an access check
$certFile = certificates::get_file($_REQUEST['certificates_files_id']);

if($certFile){
    //record download
    $downloads->insert(array(
        'downloads_type' => 'certificate',
        'join_id' => $certFile['join_certificates_id'],
        'join_members_id' => ActiveMemberInfo::GetMemberId(),
        'downloads_datetime' => SQL('NOW()')
    ));
    
    members::log_action(ActiveMemberInfo::GetMemberId(), 'Downloaded from Certificates: '.$certFile['files_name'], $certFile);

    $redirectUrl = Services::$fileSystem->getFileDownloadUrl($certFile['files_id']);

    if(!$redirectUrl) {
        http::redirect(BASEURL . '/members/my-documents/certificates/index.php', array('notice' => 'You are unable to view this document'));
    } else {
        http::redirect($redirectUrl);
    }
}else{
    http::redirect(BASEURL . '/members/my-documents/certificates/index.php', array('notice' => 'You are unable to view this document'));
}

?>