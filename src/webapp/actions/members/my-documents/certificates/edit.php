<?php
use Services\Services;

include(ACTIONS . '/members/protect.php');

include_once('_email_cert_request.php');
//include ACTIONS . '/members/my-documents/certificates/email_templates/edit.php'; 
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/TmpEmailFileHandler.php'); 
$dynemail = new CertRequestDynamicEmail();

$db = mysqli_db::init();

$access = UserPermissions::UserCanAccessScreen('certificates');
$certificate = certificates::get_certificate($_REQUEST['certificates_id']);
if($certificate) {
    $permissions = UserPermissions::UserObjectPermissions('certificates', $certificate);
    $certFiles = certificates::get_files($certificate['certificates_id']);
    $emails_history = emails_history::get_metadata_for_certificate($_REQUEST['certificates_id']);
}
if(!$access || ($certificate && !$permissions['edit'])) {
    http::redirect(BASEURL .'/members/my-documents/certificates/index.php', array('notice' => 'You don\'t have permission to edit that certificate'));
}

if($certificate) {
    $certificate['certificates_remind_other_chk'] = $certificate['certificates_remind_other'] ? '1' : '0';
    $certificate['certificates_remind_other_expired_chk'] = $certificate['certificates_remind_other_expired'] ? '1' : '0';
    $certificate['certificates_request_cert_to_email_chk'] = $certificate['certificates_request_cert_to_email'] ? '1' : '0';
} else {
    $certificate = array();

    if($_REQUEST['action'] == 'setLocation') {
        $certificate['join_licensed_locations_id'] = $_REQUEST['licensed_locations_id'];
    } else if(ActiveMemberInfo::IsUserMultiLocation()) {
        $certificate['join_licensed_locations_id'] = $_REQUEST['join_licensed_locations_id'];
    } else {
        $certificate['join_licensed_locations_id'] = ActiveMemberInfo::SingleLocationUserLocationId();
    }

    // Need to pick licensed location first - set that view and get out of here.
    if(!isset($certificate['join_licensed_locations_id'])) {

            $licensed_locations = licensed_locations::get_licensed_locations_with_permission('certificates');
            if(!empty($_REQUEST['vendors_id'])){
               // show only the locations of vendor
               $vendor_locations = vendors::get_vendor_locations_by_vendorid($_REQUEST['vendors_id']);
               $vendors_locations = array();
               $i = 0;
               foreach ($licensed_locations as $licensed_location) {
                   if(in_array($licensed_location['licensed_locations_id'], $vendor_locations)){
                        $vendors_locations[$i]['licensed_locations_id'] = $licensed_location['licensed_locations_id'];
                        $vendors_locations[$i]['licensed_locations_name'] = $licensed_location['licensed_locations_name'];
                        $i++;
                   }
               }
               if(!empty($vendor_locations)){
                $licensed_locations = $vendors_locations;
               }
            }
            $licensed_locations_names = array();
            foreach ($licensed_locations as $licensed_location) {
                $licensed_locations_names[$licensed_location['licensed_locations_id']] = $licensed_location['licensed_locations_name'];
                $vendors_by_location[$licensed_location['licensed_locations_id']] = array();
                if($vendors_for_location){
                    foreach($vendors_for_location as $vendor) {
                        $vendors_by_location[$licensed_location['licensed_locations_id']][] = array_intersect_key($vendor, array('vendors_id' => '', 'vendors_name' => '', 'vendors_email' => ''));
                    }
                }
                usort($vendors_by_location[$licensed_location['licensed_locations_id']], 'sortByVendorName');
            }

        framework::set_view('members/my-documents/certificates/add.php');
        return;
    }

    $certificate['certificates_request_cert_to_email_chk'] = '1';
    $currentMember = members::get_contact_info(ActiveMemberInfo::GetMemberId());
    $certificate['certificates_request_cert_to_email'] = $currentMember['members_email'];
    $certificate['certificates_email_days'] = '30';
}

$licensed_locations_id = $certificate['join_licensed_locations_id'];
$location = licensed_locations::get_by_id($licensed_locations_id);

$locationName = $location['licensed_locations_name'];
$certificate['certificates_default_location_name'] = !isset( $certificate['certificates_default_location_name'])?$locationName:$certificate['certificates_default_location_name'] ;

//Get default template ID;
$default_template_id = 0;
$default_template = certificates_email_templates::getDefaultTemplate();
if ($default_template) {
    $default_template_id = $default_template['certificates_email_templates_id'];
}

// Get list of templates
// Note if the template is a custom one
$templates = array(0 =>  'Default ' . SITE_NAME .' template');
$locationTemplates = certificates_email_templates::getTemplateForLocation($licensed_locations_id);
foreach($locationTemplates as $template) {
    if($template['certificates_email_templates_id'] == $default_template_id) {
        $template['certificates_email_templates_name'] = $template['certificates_email_templates_name'] . ' (default)';
    }
    $templates[$template['certificates_email_templates_id']] = $template['certificates_email_templates_name'];
}

$customTemplate = false;
$certTemplate = certificates_email_templates::get_template_for_certificate($_REQUEST['certificates_id']);
if ($certTemplate) {
    $templates[$certTemplate['certificates_email_templates_id']] = "(Custom Template)";
    $customTemplate = true;
    $templateHtml = stripslashes($certTemplate['email_html']);
}

// Make sure to set the template ID if one isn't set
// On a post, this will get overwritten from the the $data variable
if (!isset($certificate['join_certificates_email_templates_id'])) {
    $certificate['join_certificates_email_templates_id'] = $default_template_id;
}

//$allCoverages = explode(',', $certificate['certificates_coverages']);
//print_r($allCoverages);
//
//$coverages = array();
//foreach($allCoverages as $coverage){
//    $coverages[$coverage] = $coverage;
//}

function sortByVendorName($a, $b) {
    return strcasecmp($a['vendors_name'], $b['vendors_name']);
}

$allVendors = licensed_locations::getObjectsAssignedToLocation('vendors', $certificate['join_licensed_locations_id']);
$vendors = array();
foreach($allVendors as $vendor){
    if($vendor['vendors_hidden'] == '1') {
        continue;
    }
    $vendors[$vendor['vendors_id']] = $vendor['vendors_name'];
}
// Make sure currently selected vendor is in the list - could have been hidden or something
$thisVendor = vendors::get_vendor($certificate['join_vendors_id']);
$vendors[$thisVendor['vendors_id']] = $thisVendor['vendors_name'];
natcasesort($vendors);

$remind_expiring_members = certificates::get_expiring_remind_members($_REQUEST['certificates_id']);
$remind_expired_members = certificates::get_expired_remind_members($_REQUEST['certificates_id']);
if($certificate['certificates_id']){
   $all_legal_entity_names = certificates::get_certificates_x_legal_entity_names($certificate['certificates_id']);
   $allCoverages = certificates::get_coverags_by_certificates_id($certificate['certificates_id']);
   $coverages = array();
    foreach($allCoverages as $coverage){
        $coverages[$coverage['certificate_coverages_name']]['certificate_coverages_name'] = $coverage['certificate_coverages_name'];
        $coverages[$coverage['certificate_coverages_name']]['certificate_coverages_amount_1'] = $coverage['certificate_coverages_amount_1'];
        $coverages[$coverage['certificate_coverages_name']]['certificate_coverages_amount_2'] = $coverage['certificate_coverages_amount_2'];
        $coverages[$coverage['certificate_coverages_name']]['certificate_coverages_amount_3'] = $coverage['certificate_coverages_amount_3'];
    }
}

if(!$certificate['certificates_email_subject']) {
    $certificate['certificates_email_subject'] = 'Certificate Request';
}

$entity_name_by_location[] = "Please Select";
$entity_name_for_locations = legal_entities:: getAssignedToLocation($licensed_locations_id);
if($entity_name_for_locations){ //fixed the warning in case empty array
    foreach ($entity_name_for_locations as $entity_name_for_location){
        if($entity_name_for_location['legal_entity_names_hidden'] != 1 || in_array($entity_name_for_location['legal_entity_names_id'], $all_legal_entity_names) ){
            $entity_name_by_location[$entity_name_for_location['legal_entity_names_id']] = $entity_name_for_location['legal_entity_names_name'];
        }
    }
}

$entity_name_by_location['add'] = 'New...';

$add_leagal_name = false;
if(count($entity_name_by_location) == 2){
    $all_legal_entity_names[] = 'add';
} else if(!count($all_legal_entity_names)){
    $all_legal_entity_names[] = '';
}

$errors = array();

if($_POST){
    $data = $_POST;

    //If validation fails, this makes sure the coverage checkboxes stay the way they were submitted.
    $coverages = $data['coverages'];
    //$coverages = arrays::values_as_keys($coverages);
    $coverages_array = array();
    for($i=0;$i<count($coverages);$i++){
        $certificate_coverarge_name = $coverages[$i];
        preg_match('#\((.*?)\)#', $certificate_coverarge_name, $match);
        $short_name = $match[1];
        $coverages_array[$i]['certificate_coverages_name'] = $certificate_coverarge_name;
        $coverages_array[$i]['certificate_coverages_shortname'] = $short_name;
        if($short_name == 'W/C'){ 
          $coverages_array[$i]['certificate_coverages_amount_1']  = $data['coverages_amount'][$certificate_coverarge_name]['amount1'];
          $coverages_array[$i]['certificate_coverages_amount_2']  = $data['coverages_amount'][$certificate_coverarge_name]['amount2'];
          $coverages_array[$i]['certificate_coverages_amount_3']  = $data['coverages_amount'][$certificate_coverarge_name]['amount3'];
        }else{
            $coverages_array[$i]['certificate_coverages_amount_1'] = $data['coverages_amount'][$coverages_array[$i]['certificate_coverages_name']];
            $coverages_array[$i]['certificate_coverages_amount_2']  = '';
            $coverages_array[$i]['certificate_coverages_amount_3']  = '';
        }
    }
    if($data['certificates_coverage_other']!='')
    {
      $coverages_array[count($coverages)]['certificate_coverages_name'] = $data['certificates_coverage_other'];
      $coverages_array[count($coverages)]['certificate_coverages_shortname'] = '';
      $coverages_array[count($coverages)]['certificate_coverages_amount_1'] = $data['certificates_coverages_other_amount'];
    }else if(!isset($data['certificates_coverage_other'])){  
        $data['certificates_coverage_other'] ='';
    }
    $all_legal_entity_names = $data['join_legal_entity_names_id'];
    
    if ($_REQUEST['action'] != 'update') {
        if (!ActiveMemberInfo::IsUserMultiLocation()) {
            $data['join_licensed_locations_id'] = ActiveMemberInfo::SingleLocationUserLocationId();
        } else {
            $data['join_licensed_locations_id'] = $data['licensed_locations_id'];
        }
    }

    $validate = new validate($data);
    
    $validate
        ->setOptional('certificates_id')
        ->setOptional('certificates_file')
        ->setOptional('certificates_coverage_other')
        ->setOptional('certificates_coverages_other_amount')
        ->setOptional('certificates_expire')
        ->setOptional('action')
        ->setOptional('join_licensed_locations_id');

    $data['join_licensed_locations_id'] = $certificate['join_licensed_locations_id'];

    if($data['certificates_request_cert_to_email_chk'] != '1') {
        $validate->setOptional('certificates_request_cert_to_email');
        $data['certificates_request_cert_to_email'] = '';
    } else {
        $validate->setEmail('certificates_request_cert_to_email', 'Please enter a valid email');
    }

    if ($_REQUEST['action'] != 'update') {
        if($data['certificates_remind_other_chk'] != '1') {
            $validate->setOptional('certificates_remind_other');
            $data['certificates_remind_other'] = '';
        } else {
            $validate->setEmail('certificates_remind_other', 'Please enter a valid email');
        }
        if($data['certificates_remind_member'] == '1') {
            if($data['certificates_remind_other_expired_chk'] != '1') {
                $validate->setOptional('certificates_remind_other_expired');
                $data['certificates_remind_other_expired'] = '';
            } else {
                $validate->setEmail('certificates_remind_other_expired', 'Please enter a valid email');
            }
            if($data['certificates_remind_vendor_expired'] != '1' &&
                //$data['certificates_remind_location_expired'] != '1' &&
                //$data['certificates_remind_admin_expired'] != '1' &&
                !isset($data['certificate_remind_members_after_expire'])&&
                $data['certificates_remind_other_expired'] != '1')
            {
                $errors['certificates_reminder_expired_email'] = 'Please select someone to get an expiration reminder';
            }
        } else {
            $validate->setOptional(array(
                'certificates_remind_vendor_expired',
                //'certificates_remind_location_expired',
                //'certificates_remind_admin_expired',
                'certificates_remind_other_expired'
            ));
            $data['certificates_remind_other_expired'] = '';
        }
    } else {
        $validate->setOptional('join_vendors_id');
        $validate->setOptional('certificates_email_subject');

        $data['join_vendors_id'] = $certificate['join_vendors_id'];
    }

    if($data['certificates_type'] == 'regular'){
        $validate->setOptional('certificates_project_name');
    }
    if($data['customize_template'] != '1'){
        $validate->setOptional('email_html');
    }

    $validate->setTitles(array(
        'certificates_expire' => 'expiration date',
        'join_licensed_locations_id' => 'location',
        'join_vendors_id' => 'vendor',
        'certificates_email_subject' => 'email subject line'
    ));

    $uploaded_files = files::fixUploadFileArray($_FILES['certificates_file']);

    $validate->setFileType('both');
    foreach ($uploaded_files as $file) {
        if ($file['name'] == '') {
            continue;
        }

        $validate->setFile($file, 'certificates_file');
    }

    $errors = $validate->test();

    if($errors['join_licensed_locations_id']){
        $errors['licensed_locations_id'] = $errors['join_licensed_locations_id'];
    }

    if ($_REQUEST['action'] != 'update') {
        if (!$data['coverages'] && !$data['certificates_coverage_other']) {
            $errors['coverages'] = 'Specify at least 1 coverage type';
        }
        if ($data['join_legal_entity_names_id'] == 0 && $data['certificates_include_additional_insured_request']) {
            $errors['join_legal_entity_names_id'] = 'Specify at least 1 legal entity name';
        }
        if($data['certificates_remind_vendor'] != '1' &&
            $data['certificates_remind_other_chk'] != '1' &&
            !isset($data['certificate_remind_members_before_expire'])
            // && $data['certificates_send_copy'] != '1' &&
            //$data['certificates_remind_corporate_admin'] != 1
            )
        {
            $errors['certificates_reminder_email'] = 'Please select someone to get an expiration reminder';
        }
        if($data['certificates_include_additional_insured_request'] == 1){
            $legal_names = $data['join_legal_entity_names_id'];
            for($i =0; $i< count($legal_names); $i++){
                if($legal_names[$i] == '0'){
                  $errors["join_legal_entity_names_id"] = 'Please select a legal name';  
                }
                if($legal_names[$i] == 'add' && $data['legal_entity_names_name'][$i] == ''){ //if new entity name is not entered
                  $errors["join_legal_entity_names_id"] = 'Please select a legal name';
                }
            }
            
        }
    }

    if(!$errors){
        $certificates = new mysqli_db_table('certificates');
        $certificates_files = new mysqli_db_table('certificates_files');
        $certificates_remind_members = new mysqli_db_table('certificate_remind_members');
        $certificates_email_templates = new mysqli_db_table('certificates_email_templates');
        $legal_entity_names = new mysqli_db_table('legal_entity_names');
        $certificates_x_legal_entity_names = new mysqli_db_table('certificates_x_legal_entity_names');
        $certificate_coverages = new mysqli_db_table('certificate_coverages');
        $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
        if($data['certificates_expire'] != ''){
            $data['certificates_expire'] = date('Y-m-d', strtotime($data['certificates_expire']));
        }

        $data_files = array();

        //loop through number of uploaded files
        foreach ($uploaded_files as $index => $file) {
            if ($file['size']) {
                $file_entry = Services::$fileSystem->storeFile($file, 'certificates');

                if(!$file_entry) {
                    $errors['certificates_file'] = 'There was a problem uploading a file';
                    return;
                }

                $file_entry['join_files_id'] = $file_entry['files_id'];
                $data_files[] = $file_entry;
            }
        }
       
        //Combining  notify beore expire and notify after expire members into one array
        $remind_members = array();
        for($i=0;$i<count($data['certificate_remind_members_before_expire']) ; $i++){
            $remind_members[$i]['join_members_id'] = $data['certificate_remind_members_before_expire'][$i];
            $remind_members[$i]['certificates_remind_before_expiry'] = 1;
            $remind_members[$i]['certificates_remind_after_expiry'] = 0;
        }
        for($i=0;$i<count($data['certificate_remind_members_after_expire']) ; $i++){
            $fount_flag = 0;
            for($j=0;$j<count($remind_members);$j++){
                if($remind_members[$j]['join_members_id'] == $data['certificate_remind_members_after_expire'][$i]){
                    $remind_members[$j]['certificates_remind_after_expiry'] = 1;
                    $fount_flag = 1;
                }
            }
            
            if($fount_flag == 0){
                $count = count($remind_members)+1;
                $remind_members[$count] ['join_members_id']= $data['certificate_remind_members_after_expire'][$i];
                $remind_members[$count] ['certificates_remind_after_expiry'] = 1;
                $remind_members[$count] ['certificates_remind_before_expiry'] = 0;
            }
        }

        $certificate_legal_entity = array();
        $legal_entity_names_details = array();
        $j = 0; $k=0;
        for($i=0;$i< count($data['join_legal_entity_names_id']);$i++){
            if($data['join_legal_entity_names_id'][$i]== 'add'){
                $locationId = (ActiveMemberInfo::_IsAccountAdmin() && $data['associate_with'][$i] == 'Account') ? '0' : $licensed_locations_id;
                $legal_entity_names_details[$j]['legal_entity_names_name'] = $data['legal_entity_names_name'][$i];
                $legal_entity_names_details[$j]['join_locations_id'] = $locationId;
                $legal_entity_names_details[$j]['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
                $j++;
            }else{
                $certificate_legal_entity[$k]=$data['join_legal_entity_names_id'][$i];
                $k++;
            }

        }
        
        for($i=0; $i<count($legal_entity_names_details); $i++){
            $legal_entity_names->insert($legal_entity_names_details[$i]);
            $certificate_legal_entity[] = $legal_entity_names->last_id();
        }
        if($certificate['certificates_id']){
            $certificates_id = $certificate['certificates_id'];

            //Activity Log stuff
            $changes = array();
            foreach(array_diff($data, $certificate) as $field => $change){
                switch($field){
                    case 'certificates_name':
                        $changes[] = 'Name to '.$change;
                        break;

                    case 'certificates_project_name':
                        $changes[] = 'Project Name to '.$change;
                        break;

                    case 'certificates_file':
                        $changes[] = 'File to '.$data['certificates_filename'];
                        break;

                    case 'certificates_expire':
                        $changes[] = 'Expiration Date to '.date(DATE_FORMAT, strtotime($change));
                        break;

                    case 'certificates_email_days':
                        $changes[] = 'Reminder Days to '.$change;
                        break;

                    case 'certificates_remind_vendor':
                        $changes[] = 'Remind Vendor to '.($change ? 'Yes' : 'No');
                        break;

                    case 'certificates_remind_member':
                        $changes[] = 'Remind Me Until Updated to '.($change ? 'Yes' : 'No');
                        break;

                    case 'certificates_send_copy':
                        $changes[] = 'Send Me A Copy Of Vendor Notification to '.($change ? 'Yes' : 'No');
                        break;

                    case 'join_vendors_id':
                        $changes[] = 'Vendor to '.$vendors[$change];
                        break;

                    case 'certificates_coverages':
                        $changes[] = 'Coverages to '.$change;
                        break;

                    case 'certificates_coverage_other':
                        $changes[] = 'Other coverage to '.$change;
                        break;

                    case 'certificates_type':
                        $changes[] = 'Type to '. ($change == 'special' ? 'Special Projects' : 'Regular Operations');
                        break;

                    case 'licensed_locations':
                        $changes[] = 'Locations to '. $licensed_locations_names[$change];
                        break;
                }
            }
            
            if($changes){
                members::log_action(ActiveMemberInfo::GetMemberId(), 'Edited Certificate: '.implode(', ', $changes), $certificate['certificates_id']);
            }
            
            $certificates->update($data, $certificate['certificates_id']);

            if ($_REQUEST['action'] != 'update') {
                certificates::delete_certificates_x_legal_entity_by_certificates_id($certificates_id);
                for ($i = 0; $i < count($certificate_legal_entity); $i++) {
                    $certificates_x_legal_entity['join_legal_entity_names_id'] = $certificate_legal_entity[$i];
                    $certificates_x_legal_entity['join_certificates_id'] = $certificates_id;
                    $certificates_x_legal_entity_names->insert($certificates_x_legal_entity);
                }

                certificates::delete_remind_members_by_certificates_id($certificates_id);
                if(!empty($remind_members)){
                    foreach($remind_members as $remind_member){
                        $remind_member['join_certificates_id']= $certificates_id;
                        $certificates_remind_members->insert($remind_member);
                    }
                }
                if(!empty($coverages_array)){
                    certificates::delete_coverags_by_certificates_id($certificates_id);
                    foreach($coverages_array as $coverage){
                        $coverage['join_certificates_id']= $certificates_id;
                        $certificate_coverages->insert($coverage);
                      // print_r($coverage);
                    }
                }
            }

            $notice = 'Certificate updated!';
        } else {
            $certificates->insert($data);
            $certificates_id = $certificates->last_id();
            
            for($i=0; $i < count($certificate_legal_entity);$i++){
                $certificates_x_legal_entity['join_legal_entity_names_id'] = $certificate_legal_entity[$i];
                $certificates_x_legal_entity['join_certificates_id'] = $certificates_id;
                $certificates_x_legal_entity_names->insert($certificates_x_legal_entity);
            }
            $notice = 'Certificate added!';
            members::log_action(ActiveMemberInfo::GetMemberId(), 'New certificate added', $certificates_id);

            if(!empty($remind_members)){
                foreach($remind_members as $remind_member){
                    $remind_member['join_certificates_id']= $certificates_id;
                    $certificates_remind_members->insert($remind_member);
                }
            }
            if(!empty($coverages_array)){
                foreach($coverages_array as $coverage){
                    $coverage['join_certificates_id']= $certificates_id;
                    $certificate_coverages->insert($coverage);
                }
            }
        }

        //If this is the first file, make it the active one.
        if(!$certFiles && count($data_files) == 1) {
            $data_files[0]['certificates_files_active'] = 1;
        }

        if (!empty($data_files)) {
            foreach ($data_files as $file) {
                $file['join_certificates_id'] = $certificates_id;
                $certificates_files->insert($file);
                $certificates_files_id = $certificates_files->last_id();
                members::log_action(ActiveMemberInfo::GetMemberId(), 'New certificate file added: '.$file['files_name'], $certificates_id);
            }
        }
        if($_REQUEST['action'] == 'update') {
            if(count($data_files) == 1 && !empty($certificates_files_id)){
                certificates::setActiveFile($certificates_files_id);
            }
        }

        if($_REQUEST['action'] != 'update') {
            if($data['customize_template'] == 1){
                $email_templates['join_licensed_locations_id'] = $certificate['join_licensed_locations_id'];
                $email_templates['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
                $email_templates['join_certificates_id'] = $certificates_id;
                $template = addslashes($data['email_html']);
                $template = trim($template);
                $template = preg_replace( "/\r|\n/", "", $template );
                $email_templates['email_html'] = $template;

                $template = certificates_email_templates::get_template_for_certificate($certificates_id);
                if(!$template['certificates_email_templates_id']){
                    //Insert an entry to templates table
                    $certificates_email_templates->insert($email_templates);
                    $certificates_email_templates_id = $certificates_email_templates->last_id();
                    $data['join_certificates_email_templates_id'] = $certificates_email_templates_id;
                    $certificates->update($data, $certificates_id);
                }else{
                    $certificates_email_templates->update($email_templates, $template['certificates_email_templates_id']);
                }
            } else {
                $template = certificates_email_templates::get_template_for_certificate($certificates_id);
                if($template) {
                    certificates_email_templates::delete_by_id($template['certificates_email_templates_id']);
                }
            }
        }

        if(isset($data['request_cert']) && $data['join_vendors_id']) {
            EmailCertRequest($certificates_id, $data['certificates_email_subject']);
        }

        http::redirect(BASEURL .'/members/my-documents/certificates/index.php', array('notice' => $notice));
    } else {
        $certificate = array_merge((array) $certificate, $data);
    }
}

//Defaults
if(!$certificate){
    $certificate['certificates_remind_vendor'] = 1;
    $certificate['certificates_remind_text'] = 'A certificate on WAKEUP CALL is expiring.';
}
