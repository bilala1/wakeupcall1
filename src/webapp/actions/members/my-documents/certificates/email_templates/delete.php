<?php
include(ACTIONS . '/members/protect.php');

if($_REQUEST['certificates_email_templates_id'] !== null)
{
    $certificates_email_templates_id = $_REQUEST['certificates_email_templates_id'];
    $template = certificates_email_templates::get_by_id($certificates_email_templates_id);
    $certificate_id = $template['join_certificates_id'];
    $licenced_location_id = $template['join_licenced_locations_id']; 
    if($licenced_location_id == 0){ //all location
        if (ActiveMemberInfo::_IsAccountAdmin()){
            certificates_email_templates::delete_by_id($certificates_email_templates_id);
            if($certificate_id){
                certificates::remove_certificates_email_templates($certificate_id);
            }
        }
    }else{
        $permissions = emails_templates_permissions::get_for_location($location['licensed_locations_id']);
        if (ActiveMemberInfo::_IsAccountAdmin() || $permissions['location_can_override_certificates_email'] == 1){
            certificates_email_templates::delete_by_id($certificates_email_templates_id);
            if($certificate_id){
                certificates::remove_certificates_email_templates($certificate_id);
            }
        }
    }
    
   //certificates_email_templates::delete_for_location(ActiveMemberInfo::GetAccountId(), $licensed_locations_id);
}

 http::redirect(BASEURL .'/members/my-documents/certificates/email_templates/index.php', array('notice' => 'Template deleted'));
?>

