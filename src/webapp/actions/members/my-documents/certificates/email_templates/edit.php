<?
include(ACTIONS . '/members/protect.php');
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/CertRequestEmail.php'); 
include_once(SITE_PATH . '/' . MODELS . '/DynamicEmail/TmpEmailFileHandler.php'); 
$dynemail = new CertRequestDynamicEmail();

//add, edit, on_the_fly
$email_edit_type = $_REQUEST['email_edit_type'] ? $_REQUEST['email_edit_type'] : 'add';
$licensed_locations_id = $_REQUEST['licensed_locations_id'] ? $_REQUEST['licensed_locations_id'] : 0;
$certificates_email_templates_id = $_REQUEST['certificates_email_templates_id']?$_REQUEST['certificates_email_templates_id']:0;
$certificates_id = $_REQUEST['certificates_id']?$_REQUEST['certificates_id']:0;
//$template = certificates_email_templates::get_best_fallback_for_location(ActiveMemberInfo::GetAccountId(), $licensed_locations_id);
//$template = certificates_email_templates::getLocationsForAccount();
if($certificates_email_templates_id){
    $template = certificates_email_templates::get_template($certificates_email_templates_id);
}
$corporate_template = certificates_email_templates::get_for_account(ActiveMemberInfo::GetAccountId());

$template_html = $_REQUEST['email_html'] ? $_REQUEST['email_html'] : 
                 ($template ? stripslashes($template['email_html']) : emails_default_templates::get_certificate_request_template());

if($email_edit_type == 'add')
{
    $template_html = emails_default_templates::get_certificate_request_template();
    $licensed_locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
   /* $tmp_locations = Array();
    
    if(ActiveMemberInfo::_IsAccountAdmin() && !$corporate_template)
    {
        $tmp_locations[] = array('licensed_locations_id'=> 0,
                                 'licensed_locations_name' => 'Corporate Master Template');
    }
    
    foreach($licensed_locations as $location)
    {
        $tmp_templates = certificates_email_templates::get_for_location(ActiveMemberInfo::GetAccountId(), $location['licensed_locations_id']);
        $permissions = emails_templates_permissions::get_for_location($location['licensed_locations_id']);
        $permitted = ActiveMemberInfo::_IsAccountAdmin() || $permissions['location_can_override_certificates_email'] == 1;
        
        if($permitted && ($tmp_templates == null || count($tmp_templates) == 0))
        {
            $tmp_locations[] = ($location);
        }
    }

    $licensed_locations = $tmp_locations;*/
}
if($email_edit_type == 'edit' || $email_edit_type == 'on_the_fly')
{
     $licensed_locations = null;
     $edit_location = licensed_locations::get_by_id($licensed_locations_id);
     $certificates_email_templates_name = certificates_email_templates::get_email_template_name($certificates_email_templates_id);
}

if($email_edit_type == 'on_the_fly')
{
//    $tmp = TmpEmailFileHandler::GetTempEmailFile($_REQUEST['tmpfile_id']);
//
//    $template_html = $tmp != null ? $tmp : $template_html;
    if($certificates_id){
        
        $template = certificates_email_templates::get_template_for_certificate($certificates_id);
        if($template){
            $certificates_email_templates_id = $template['certificates_email_templates_id'];
        }
    }
    $template_html = $_REQUEST['email_html'] ? $_REQUEST['email_html'] : 
                     ($template ? $template['email_html'] :  emails_default_templates::get_certificate_request_template());
}

if($_POST)
{
    $template_html =  $_POST['email_html'];
    $certificates_email_templates_name = $_POST['certificates_email_templates_name'];
    
    if($email_edit_type == 'on_the_fly')
    {
        //certificates_email_templates::add_or_update_email_template($licensed_locations_id,$template_html,$certificates_id,$certificates_email_templates_id);
        
//        TmpEmailFileHandler::WriteTempEmailFile($_REQUEST['tmpfile_id'], $template_html);
//        
//        return;
    }
    $validate = new validate($_POST);
    $validate
        ->setOptional('licensed_locations_id');
    $errors = array();
    if($_POST['certificates_email_templates_name'] == ''){
        $validate->setTitle('certificates_email_templates_name',"template name");
        //$errors['certificates_email_templates_name'] = "Please enter the template name";
    }
    $email_template = certificates_email_templates::check_email_templates_exists(ActiveMemberInfo::GetAccountId(),$certificates_email_templates_name,$certificates_email_templates_id);
    if(!empty($email_template)){
       $errors['certificates_email_templates_name'] = "This name exists for another template";
    }
    $errors = array_merge($errors, $validate->test());
    if(empty($errors)){
        $licensed_locations_id = $_POST['licensed_locations_id'] ? $_POST['licensed_locations_id'] : 0;
        certificates_email_templates::add_or_update_for_location(ActiveMemberInfo::GetAccountId(), $licensed_locations_id, $template_html,$certificates_email_templates_name,$certificates_email_templates_id);
        http::redirect(BASEURL .'/members/my-documents/certificates/email_templates/index.php', array('notice' => $notice));
    }
}
?>

