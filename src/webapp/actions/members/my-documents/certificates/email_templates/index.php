<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(35, $_REQUEST['page']);

$licensed_locations = licensed_locations::get_licensed_locations_with_permission('certificates');

$filter_location_id = array_key_exists('licensed_locations_id', $_REQUEST) ? $_REQUEST['licensed_locations_id'] :
    (array_key_exists('certs_email_licensed_locations_id',
        $_SESSION) ? $_SESSION['certs_email_licensed_locations_id'] : 0);

$_SESSION['certs_email_licensed_locations_id'] = $filter_location_id;

$order = $_REQUEST['order'];
$email_templates = certificates_email_templates::get_paged_certificates_templates_list($paging,$order, $filter_location_id);

$i=0;
$tmp_templates = array();
foreach ($email_templates as $email_template) {
    if ($filter_location_id !=0 && ($email_template['join_licensed_locations_id'] == 0  || $filter_location_id == $email_template['join_licensed_locations_id'])) {
       $tmp_templates[$i] = $email_template;
       $i++;
    }
    
}
if(!empty($tmp_templates)){
   $email_templates = $tmp_templates;
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_certificates FROM members_settings WHERE join_members_id = ?',
    array(ActiveMemberInfo::GetMemberId()));


?>
