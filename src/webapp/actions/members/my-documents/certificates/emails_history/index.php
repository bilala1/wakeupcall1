<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(35, $_REQUEST['page']);

$licensed_locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());
if($_REQUEST['clear_filter'] == 'clear_filter'){
    $_REQUEST['email_history_start_time']= '';
    $_REQUEST['email_history_end_time'] ='';
    $_REQUEST['join_licensed_locations_id']='';
    $startTime = '';
    $endTime = '';
    $filter_location_id = '';
    unset($_SESSION['email_history_start_time']);
    unset($_SESSION['email_history_end_time']);
    unset($_SESSION['email_history_licensed_locations_id']);
}
$filter_location_id = array_key_exists('join_licensed_locations_id', $_REQUEST) ? $_REQUEST['join_licensed_locations_id'] :
                      (array_key_exists('email_history_licensed_locations_id', $_SESSION) ? $_SESSION['email_history_licensed_locations_id'] : 0);

$_SESSION['email_history_licensed_locations_id'] = $filter_location_id;

if(array_key_exists('order',$_REQUEST))
{
    $order = $_REQUEST['order'];
    $_SESSION['email_history_order'] = $_REQUEST['order'];
    
}
else if(array_key_exists('email_history_order', $_SESSION))
{
     $order = $_SESSION['email_history_order'];
}

$startTime = array_key_exists('email_history_start_time',$_REQUEST) && strtotime($_REQUEST['email_history_start_time']) != false ? $_REQUEST['email_history_start_time'] :
    ( array_key_exists('email_history_start_time',$_SESSION) && strtotime($_SESSION['email_history_start_time']) != false ? $_SESSION['email_history_start_time'] : date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 year')));

// if there was a request start time, save it in the session for next time
if ( array_key_exists('email_history_start_time',$_REQUEST) && strtotime($_REQUEST['email_history_start_time']) ) {
    $_SESSION['email_history_start_time'] = $_REQUEST['email_history_start_time'];
}

$endTime = array_key_exists('email_history_end_time',$_REQUEST) && strtotime($_REQUEST['email_history_end_time']) ? $_REQUEST['email_history_end_time'] :
    ( array_key_exists('email_history_end_time',$_SESSION) && strtotime($_SESSION['email_history_end_time']) ? $_SESSION['email_history_end_time'] : date('n/j/Y', time()));

// if there was a request end time, save it in the session for next time
if ( array_key_exists('email_history_end_time',$_REQUEST) ) {
    $_SESSION['email_history_end_time'] = $_REQUEST['email_history_end_time'];
}

$emails = emails_history::get_all_metadatas(ActiveMemberInfo::GetAccountId(), $filter_location_id, null, $order, $startTime, $endTime, $paging, true,'certificates');

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_certificates FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));
?>
