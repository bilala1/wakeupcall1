<?
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();

//this performs an access check
$certificate = certificates::get_certificate($_REQUEST['certificates_id']);
$perms = UserPermissions::UserObjectPermissions('certificates', $cert_id);

if ($certificate && $perms['edit']) {

    if ($_REQUEST['hide_certificate'] == 1) {
        $certificate['certificates_hidden'] = 1;
        $notice = 'The certificate has been hidden';
    } else {
        $certificate['certificates_hidden'] = 0;
        $notice = 'The certificate has been un-hidden';
    }

    $certificates_table = new mysqli_db_table('certificates');
    $certificates_table->update($certificate, $certificate['certificates_id']);
    $certificates_id = $certificate['certificates_id'];

    members::log_action(ActiveMemberInfo::GetMemberId(), 'Certificate '.($_REQUEST['hide_certificate'] == 1 ? 'hidden' : 'unhidden'), $certificate['certificates_id']);
} else {
    $notice = 'You do not have permissions to edit this certificate.';
}

http::redirect(BASEURL . '/members/my-documents/certificates/index.php', array('notice' => $notice));

?>