<?
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

//this performs an access check
$cert = certificates::get_certificate($_REQUEST['certificates_id']);

$actions = $db->fetch_all('
    select *
    from actions
    join certificates as c on certificates_id = join_certificates_id
    where
        join_certificates_id = ?
    order by actions_datetime',
    array($cert['certificates_id'])
);

?>