<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(250, $_REQUEST['page']);

include '_determine_certs_params.php';

$locations = licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId());

$certificates = certificates::get_paged_certificates_list($paging, $certificates_start_time, $certificates_end_time,
    $order, $certificates_show_hidden, $filter_location_id);

$userPrefs = UserPreferences::getForMember(ActiveMemberInfo::GetMemberId() );
foreach($certificates as &$cert){
    $cert['certificates_coverages'] = implode(', ',
        $db->fetch_singlets(
            'SELECT COALESCE(NULLIF(certificate_coverages_shortname, \'\'), certificate_coverages_name)
              from certificate_coverages where join_certificates_id = ?',
            array($cert['certificates_id'])));
    $cert['has_emails'] = $db->fetch_singlet('SELECT count(1) FROM sent_emails_history where join_certificates_id = ?', array($cert['certificates_id']));
}

$regular_certs = array();
$special_certs = array();

foreach($certificates as $certificate){
    if($certificate['certificates_type'] == 'regular'){
        $regular_certs[] = $certificate;
    }else{
        $special_certs[] = $certificate;
    }
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_certificates FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));
$model = array(
    "certificates_start_time" => $certificates_start_time,
    "certificates_end_time" => $certificates_end_time,
    "certificates_show_hidden" => $certificates_show_hidden,
    "join_licensed_locations_id" => $filter_location_id
);
?>