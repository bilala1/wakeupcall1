<?php
include(ACTIONS . '/members/protect.php');
$legal_entity_names_id = $_REQUEST['legal_entity_names_id'];
$legal_entities = legal_entities::getById($legal_entity_names_id);
$perms = UserPermissions::UserObjectPermissions('certificates', $legal_entities);
if ($legal_entities && $perms['delete']) {
    $licenced_location_id = $legal_entities['join_locations_id']; 
    if($licenced_location_id == 0){ //all location
        if (ActiveMemberInfo::_IsAccountAdmin()){
            legal_entities::delete_by_id($legal_entity_names_id);
        }
    }else{
        legal_entities::delete_by_id($legal_entity_names_id);
    }
}
http::redirect(BASEURL .'/members/my-documents/certificates/legal_names/index.php', array('notice' => 'Legal Entity name deleted'));
?>
