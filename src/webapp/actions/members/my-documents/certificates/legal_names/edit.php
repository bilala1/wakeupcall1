<?
include(ACTIONS . '/members/protect.php');
$legal_entity_names_id = $_REQUEST['legal_entity_names_id']?$_REQUEST['legal_entity_names_id']:'';
if($legal_entity_names_id){
    $legal_entities = legal_entities::getById($legal_entity_names_id);
}

$licensed_locations = licensed_locations::get_licensed_locations_with_permission('certificates');
if(ActiveMemberInfo::_IsAccountAdmin()) {
    array_unshift($licensed_locations, array('licensed_locations_id' => '0', 'licensed_locations_name' => 'All Locations'));
}
if($_POST)
{
    $validate = new validate($_POST);
    $validate
        ->setOptional('join_licensed_locations_id')
        ->setOptional('legal_entity_names_id');
    $errors = array();
    if($_POST['legal_entity_names_name'] == ''){
        $validate->setTitle('legal_entity_names_name',"Legal Entity Name");
    }
    $data['legal_entity_names_name'] = $_POST['legal_entity_names_name'];
    $data['join_locations_id'] = $_POST['join_locations_id'];
    $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
    $legal_entity_names = new mysqli_db_table('legal_entity_names');
    $errors = array_merge($errors, $validate->test());
    if(empty($errors)){
        if(!$legal_entity_names_id){
            $legal_entity_names->insert($data);
            $notice = 'Added';
        }else{
           $legal_entity_names->update($data,$legal_entity_names_id);
           $notice = 'Updated';
        }
        http::redirect(BASEURL .'/members/my-documents/certificates/legal_names/index.php', array('notice' => $notice));
    } else {
        $legal_entities = array_merge((array)$legal_entities, $data);
    }
}
?>