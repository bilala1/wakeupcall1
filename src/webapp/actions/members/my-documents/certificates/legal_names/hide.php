<?
include ACTIONS . '/members/protect.php';
$db = mysqli_db::init();
$legal_entity_names_id = $_REQUEST['legal_entity_names_id'];
$legal_entities = legal_entities::getById($legal_entity_names_id);
$perms = UserPermissions::UserObjectPermissions('certificates', $legal_entities);

if($legal_entities && $perms['edit'])
{
    if($_REQUEST['hide_entity_name'] == 1)
    {
        $legal_entities['legal_entity_names_hidden'] = 1;
        $notice = 'Legal entity name has been hidden';
    }
    else
    {
        $legal_entities['legal_entity_names_hidden'] = 0;
        $notice = 'Legal entity name has been un-hidden';
    }

    $legal_entity_names = new mysqli_db_table('legal_entity_names');
    $legal_entity_names->update($legal_entities, $legal_entity_names_id);
  
}
else
{
    $notice = 'You do not have permissions to hide this legal entity name.';
}

http::redirect('index.php', array('notice' => $notice));

?>