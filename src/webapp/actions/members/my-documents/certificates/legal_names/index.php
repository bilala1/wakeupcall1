<?php
include(ACTIONS . '/members/protect.php');
$paging = new paging(10, $_REQUEST['page']);

if(!isset($_SESSION['legal_entity_show_hidden'])) {
    $_SESSION['legal_entity_show_hidden'] = 0;
}
if(isset($_REQUEST['legal_entity_show_hidden'])) {
    $_SESSION['legal_entity_show_hidden'] = $_REQUEST['legal_entity_show_hidden'];
}
$legal_entities = legal_entities::getAllLegalEntities($_SESSION['legal_entity_show_hidden']);
// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_certificates FROM members_settings WHERE join_members_id = ?',
    array(ActiveMemberInfo::GetMemberId()));
?>