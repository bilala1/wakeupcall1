<?php
use Services\Services;

$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';
include(models . '/my_documents.php');



$db = mysqli_db::init();
$documents_table = new mysqli_db_table('documents');
$members_x_documents_table = new mysqli_db_table('members_x_documents');
$members_x_documents_x_location_table = new mysqli_db_table('members_x_documents_x_licensed_locations');
$document = $db->fetch_one('
    SELECT *
    FROM documents
    INNER JOIN files ON files_id = join_files_id
    join members_x_documents on join_documents_id = documents_id
    WHERE
        documents_id = ? and
        documents.join_members_id = ?',
    array($_REQUEST['documents_id'], ActiveMemberInfo::GetMemberId())
);
$members_x_documents_id = $document['members_x_documents_id'];
$members_x_documents_locations = myDocuments::get_locations($members_x_documents_id);
$document['join_licensed_locations'] = $members_x_documents_locations;

//Security
if($_REQUEST['documents_id'] && !$document){
    http::redirect(BASEURL . '/members/my-documents/index.php', array('notice' => 'You cannot edit this document'));
}

$_categories = library_categories::get_categories_tree(0,0,-1,false);
$categories = array('' => '-select-');
foreach($_categories as $category){
    $categories[$category['library_categories_id']] = str_repeat('-', $category['sub_level']) . $category['library_categories_name'];
}

$member_categories = $db->fetch_all('select * from members_library_categories where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$errors = array();

if($_POST){
    $data = $_POST;

    $validate = new validate($data);
    $validate->setOptional('documents_id');
    $validate->setOptional('redirect');
    if($data['documents_is_corporate_shared'] == 1){
        $validate ->addMultiLocationSelectionValidation($data, 'members_x_document', $data);
    }
    $validate
        ->setTitle('documents_title', 'Title')
        ->setExtensions('xls','xlt','xlsx','xlb','xlam','xlsb','xlsm','xltm','xlsx',
            'doc','ppt','pdf','docx', 'pptx',
            'jpg','png', 'gif','bmp','txt', 'rtf')
        ->setMimeTypes(
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'application/msword',
            'application/zip',
            'application/vnd.ms-office',
            'text/plain',
            'application/pdf',
            'image/jpeg',
            'image/png',
            'image/bmp',
            'image/gif',
            'application/mspowerpoint',
            'application/vnd.ms-powerpoint',
            'application/x-mspowerpoint',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'application/vnd.ms-powerpoint.template.macroEnabled.12 potm',
            'application/vnd.openxmlformats-officedocument.presentationml.template',
            'application/vnd.ms-powerpoint.addin.macroEnabled.12',
            'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.ms-excel.addin.macroEnabled.12',
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'application/vnd.ms-excel.sheet.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel.template.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'application/vnd.ms-excel',
            'application/vnd.ms-excel.addin.macroEnabled.12',
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'application/vnd.ms-excel.sheet.macroEnabled.12',
            'application/vnd.ms-excel.template.macroEnabled.12',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/x-excel',
            'application/x-msexcel'
        );

    $file = $_FILES['documents_file'];

    if(!$_REQUEST['documents_id'] or $file['size']){ // Only validate files when we're not updating...
        $validate->setFile($file, 'documents_file');
    }
    
    $errors = $validate->test();
    
    if($errors['documents_file'] == ' is not a valid file'){
        $errors['documents_file'] = 'Please upload a file';
    }
    
    if(!$errors){
        $new_document = array();

        if(!$_REQUEST['documents_id'] or $file['size']){
            $file_entry = Services::$fileSystem->storeFile($file, 'documents');

            if(!$file_entry) {
                $errors['documents_file'] = 'There was a problem uploading a file';
                return;
            }

            $new_document['join_files_id'] = $file_entry['files_id'];

            if(isset($document['files_id'])) {
                Services::$fileSystem->deleteFile($document);
            }
        }

        $new_document += array(
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'documents_title' => $data['documents_title'],
            'documents_type' => 'personal',
            'documents_status' => 'active',
            'documents_freetrial' => 1
        );
        $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
        if($_REQUEST['documents_id']){
            $documents_table->update($new_document, $_REQUEST['documents_id']);
            $members_x_documents_table->update($data, $document['members_x_documents_id']);
            $documents_id = $_REQUEST['documents_id'];
            $notice = 'Document updated';
            
            myDocuments::delete_locations_by_members_x_documents_id($members_x_documents_id);
            if($data['documents_is_corporate_shared'] == 1){
                foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                    $members_x_documents_x_location['join_members_x_documents_id'] = $document['members_x_documents_id'];
                    $members_x_documents_x_location['join_licensed_locations_id'] = $licensed_locations_id;
                    $members_x_documents_x_location_table->insert($members_x_documents_x_location);
                }
            }

            members::log_action(ActiveMemberInfo::GetMemberId(), "Updated a document in library");
        }else{
            $new_document['documents_datetime'] = SQL('NOW()');
            
            $documents_table->insert($new_document);
            $documents_id = $documents_table->last_id();
            $notice = 'Document added';

            //Add to members list of documents
            framework::call_action('/members/documents/add-document',
                array('documents_id' => $documents_id,
                    'join_members_library_categories_id' => $data['join_members_library_categories_id'],
                    'documents_is_corporate_shared' => $data['documents_is_corporate_shared'],
                    'join_licensed_locations' => $data['join_licensed_locations'],
                    'members_x_documents_all_locations' => $data['members_x_documents_all_locations']));

            members::log_action(ActiveMemberInfo::GetMemberId(), "Uploaded a document to library");
        }
        
        
        if($_REQUEST['redirect']){ // if comming from search page, return there
            http::redirect(HTTP_FULLURL .urldecode($_REQUEST['redirect']).'&notice=' . urlencode($notice));
        }
        else {
            http::redirect(BASEURL .'/members/my-documents/index.php', array('notice' => $notice));
        }
    }
    else {
        $document = array_merge((array) $document, $data);
    }
}

?>
