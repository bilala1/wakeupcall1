<?  //edit2.php, can only change folder
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';
include(models . '/my_documents.php');

$db = mysqli_db::init();
$documents_table = new mysqli_db_table('documents');
$members_x_documents_table = new mysqli_db_table('members_x_documents');
$members_x_documents_x_location_table = new mysqli_db_table('members_x_documents_x_licensed_locations');
$document = $db->fetch_one('
    SELECT *
    FROM documents
    join members_x_documents on join_documents_id = documents_id
    WHERE
        documents_id = ? and
        members_x_documents.join_members_id = ?',
    array($_REQUEST['documents_id'], ActiveMemberInfo::GetMemberId())
);

//Security
$members_x_documents_id = $document['members_x_documents_id'];
$members_x_documents_locations = myDocuments::get_locations($members_x_documents_id);
$document['join_licensed_locations'] = $members_x_documents_locations;

if($_REQUEST['documents_id'] && !$document){
    http::redirect(BASEURL . '/members/my-documents/index.php', array('notice' => 'You cannot edit this document'));
}

$member_categories = $db->fetch_all('select * from members_library_categories where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$errors = array();

if($_POST){
    $data = $_POST;

     $validate = new validate($data);
    //set initial validation rules
    if($data['documents_is_corporate_shared'] == 1){
        $validate ->addMultiLocationSelectionValidation($data, 'members_x_document', $data);
    }
    $validate->setOptional('documents_id');
    $validate->setOptional('redirect');
    $errors = $validate->test();
    if(!$errors){
        $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
        if($_REQUEST['documents_id']){
            $members_x_documents_table->update($data, $document['members_x_documents_id']);
            $documents_id = $_REQUEST['documents_id'];
            $notice = 'Document updated';

            $documents_table->update($document, $document['documents_id']);
            myDocuments::delete_locations_by_members_x_documents_id($members_x_documents_id);
            if($data['documents_is_corporate_shared'] == 1){
                foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
                    $members_x_documents_x_location['join_members_x_documents_id'] = $document['members_x_documents_id'];
                    $members_x_documents_x_location['join_licensed_locations_id'] = $licensed_locations_id;
                    $members_x_documents_x_location_table->insert($members_x_documents_x_location);
                }
            }
        }
        
        if($_REQUEST['redirect']){ // if comming from search page, return there
            http::redirect(HTTP_FULLURL .urldecode($_REQUEST['redirect']).'&notice=' . urlencode($notice));
        }
        else {
            http::redirect(BASEURL .'/members/my-documents/index.php', array('notice' => $notice));
        }
    }
    else {
        $document = array_merge((array) $document, $data);
    }
}

?>
