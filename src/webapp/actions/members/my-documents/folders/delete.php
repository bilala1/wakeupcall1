<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include_once ACTIONS . '/members/protect.php';

$folders_table = new mysqli_db_table('members_library_categories');
$folders_table->delete($_REQUEST['members_library_categories_id']);
$notice = 'The item has been removed';

$db = mysqli_db::init();
$db->query('
	UPDATE members_x_documents
	SET join_members_library_categories_id = 0
	WHERE join_members_library_categories_id = ?',
	array($_REQUEST['members_library_categories_id'])
);

$query = $_GET;
unset($query['members_library_categories_id']);

http::redirect(FULLURL . '/members/my-documents/index.php', ($query + array('notice' => $notice)));
?>