<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';

$folders_table = new mysqli_db_table('members_library_categories');
$folder = $folders_table->get($_REQUEST['members_library_categories_id']);

if($_POST){
    $data = $_POST;
    $errors = array();
    
    $validate = new validate($data);
    $validate
        ->setAllOptional()
        ->unsetOptional('members_library_categories_name')
        ->setTitles(array(
            'members_library_categories_name' => 'Folder Name'));
    
    $errors = $validate->test();
    
    if(!$errors){
        $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
        
        if(!$data['members_library_categories_id']){
            //Create new
            $folders_table->insert($data);
            $folders_id = $folders_table->last_id();
            
            $notice = 'Folder added';
        }else{
            //Update
            $folders_table->update($data, $data['members_library_categories_id']);
            $folders_id = $data['members_library_categories_id'];
            
            $notice = 'Folder updated';
        }
        
        http::redirect(BASEURL . '/members/my-documents/index.php?', array(
            'notice'  => $notice
        ));
    }else{
        //so user input is not lost
        $folder = array_merge((array)$folder, $data);
    }
}
?>