<?php
include_once ACTIONS . '/members/protect.php';
$db = mysqli_db::init();

//----------------------------------------------------------------------
//-- Pagination Settings
//----------------------------------------------------------------------
$paging = new paging(100);

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
list($wheres, $params) = $db->build_wheres_params(array('members_library_categories'), $_REQUEST);

$wheres[] = 'join_members_id = ?';
$params[] = ActiveMemberInfo::GetMemberId();

//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
$orderby = $db->orderby('members_library_categories_name');

//----------------------------------------------------------------------
//-- List
//----------------------------------------------------------------------
$folders = $db->fetch_all('
    SELECT SQL_CALC_FOUND_ROWS *
    FROM members_library_categories' .
    strings::where($wheres) .
    $orderby .
    $paging->get_limit(),
    $params
);

$paging->set_total($db->found_rows());

?>
