<?php
$allow_staff = true; //whether a staff/member of a hotel has access to this page
include ACTIONS . '/members/protect.php';
include(models . '/my_documents.php');

$db = mysqli_db::init();

if($_POST && UserPermissions::UserCanAccessScreen('shareDocuments')){
    $data = $_POST;

    $corp_docs = $data['edit_documents_is_corporate_shared'];
    $members_x_documents_table = new mysqli_db_table('members_x_documents');

    foreach($corp_docs as $doc_id => $is_corporate) {
        $document = $db->fetch_one('
            SELECT members_x_documents_id FROM members_x_documents
            WHERE
                join_documents_id = ? and
                join_members_id = ?',
            array($doc_id, ActiveMemberInfo::GetMemberId())
        );

        if($document){
            $members_x_documents_table->update(array('documents_is_corporate_shared' => $is_corporate), $document['members_x_documents_id']);
        }
    }

    if($_POST['ajax']) {
        die(json_encode(array("result" => "ok")));
    }
}


//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
if(!isset($_REQUEST['order'])){
    //default order
    $_REQUEST['order'] = 'members_library_categories_name';
    $_REQUEST['by'] = 'ASC';
}

$orderby = ' ORDER BY ' . $db->escape_column($_REQUEST['order']) . ' ' . ($_REQUEST['by'] == 'ASC' ? 'ASC' : 'DESC');

if($_REQUEST['order'] == 'members_library_categories_name'){
    $orderby = ' order by IF(ISNULL(members_library_categories_name),1,0), members_library_categories_name asc, documents_title';
}

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
$wheres = array();
$params = array();
$fields = documents::describe();
foreach($fields as $field){
    $field_name = $field['Field'];
    $field_value = $_REQUEST[$field_name];
    if(isset($field_value) && $field_value != ''){
        if(stristr($field_name, 'join') || $field['Type'] == 'tinyint(1)'){
            $wheres[] = $field_name.' = ?';
            $params[] = $field_value;
        }else{
            $wheres[] = $field_name.' like ?';
            $params[] = '%'.$field_value.'%';
        }
    }
}

if($_REQUEST['keyword']){
    $wheres[] = 'FIND_IN_SET(?,documents_keywords)';
    $params[] = $_REQUEST['keyword'];
}

//------------------------------------------------------------------------------
//-- Get List
//------------------------------------------------------------------------------
$documents = $db->fetch_all('
    SELECT * 
    FROM documents AS d
    LEFT JOIN files ON join_files_id = files_id
    JOIN members_x_documents as mxd on
        mxd.join_documents_id = documents_id and
        mxd.join_members_id = '.$db->filter(ActiveMemberInfo::GetMemberId()).'
    '.strings::where($wheres).'
    GROUP BY documents_id
    ORDER BY d.documents_filename ASC
    ',//.$orderby,
    $params
);

$categories = $db->fetch_all('
	SELECT *
	FROM members_library_categories
	WHERE join_members_id = ' . $db->filter(ActiveMemberInfo::GetMemberId())
    . ' ORDER BY members_library_categories_name ASC'
);




//separate into folders
$folders = array();

foreach($categories as $cat) {
	$cat['files'] = array();
	$folders[$cat['members_library_categories_id']] = $cat;
}
foreach($documents as $document){
    $folders[$document['join_members_library_categories_id']]['files'][] = $document;
}

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_files FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>
