<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/20/15
 * Time: 1:53 PM
 */
use Services\Services;

$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$documents_table = new mysqli_db_table('documents');

$new_document = array();

$data = explode(",",$_POST["image"]);
$encodedData = str_replace(' ','+',$data[1]);
$contents= base64_decode($encodedData);

$file_entry = Services::$fileSystem->storeFileContents("chart.png", $contents, 'documents');

if(!$file_entry) {
    $errors['documents_file'] = 'There was a problem uploading a file';
    return;
}

$new_document['join_files_id'] = $file_entry['files_id'];

$new_document += array(
    'join_members_id' => ActiveMemberInfo::GetMemberId(),
    'join_library_categories_id' => $_POST['members_library_categories_id'],
    'documents_title' => stripslashes(urldecode($_POST['title'])),
    'documents_type' => 'personal',
    'documents_status' => 'active',
    'documents_freetrial' => 1
);
$new_document['documents_datetime'] = SQL('NOW()');

$documents_table->insert($new_document);
$documents_id = $documents_table->last_id();

framework::call_action('/members/documents/add-document',
    array('documents_id' => $documents_id,
        'join_members_library_categories_id' => $_POST['members_library_categories_id']));

