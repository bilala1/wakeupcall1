<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');

//------------------------------------------------------------------------------
//-- build order
//------------------------------------------------------------------------------
if(!isset($_REQUEST['order'])){
    //default order
    $_REQUEST['order'] = 'members_library_categories_name';
    $_REQUEST['by'] = 'ASC';
}

$orderby = ' ORDER BY ' . $db->escape_column($_REQUEST['order']) . ' ' . ($_REQUEST['by'] == 'ASC' ? 'ASC' : 'DESC');

if($_REQUEST['order'] == 'members_library_categories_name'){
    $orderby = ' order by IF(ISNULL(members_library_categories_name),1,0), members_library_categories_name asc, documents_title';
}

//------------------------------------------------------------------------------
//-- build where
//------------------------------------------------------------------------------
$wheres = array();
$params = array();
$fields = documents::describe();
foreach($fields as $field){
    $field_name = $field['Field'];
    $field_value = $_REQUEST[$field_name];
    if(isset($field_value) && $field_value != ''){
        if(stristr($field_name, 'join') || $field['Type'] == 'tinyint(1)'){
            $wheres[] = $field_name.' = ?';
            $params[] = $field_value;
        }else{
            $wheres[] = $field_name.' like ?';
            $params[] = '%'.$field_value.'%';
        }
    }
}

if($_REQUEST['keyword']){
    $wheres[] = 'FIND_IN_SET(?,documents_keywords)';
    $params[] = $_REQUEST['keyword'];
}


//------------------------------------------------------------------------------
//-- Get List
//------------------------------------------------------------------------------

$sharedDocInClause = UserPermissions::CreateInClause('members_x_documents');
$wheres[] = $sharedDocInClause;

$wheres[] = 'documents_is_corporate_shared = 1';

$documents = $db->fetch_all('
    SELECT * 
    FROM documents AS d
    LEFT JOIN files ON files_id = join_files_id
    INNER JOIN members_x_documents as mxd ON mxd.join_documents_id = documents_id
    INNER JOIN members m ON mxd.join_members_id = m.members_id
    LEFT JOIN members_library_categories cat on mxd.join_members_library_categories_id = cat.members_library_categories_id
    '.strings::where($wheres).'
    GROUP BY documents_id
    ORDER BY files_name ASC
    ',//.$orderby, 
    $params
);

//separate into folders
$folders = array();
foreach($documents as $document){
    $cat_name = $document['members_library_categories_name'];
    if(!$cat_name) {
        $cat_name = 'General';
    }
    if(!$folders[$cat_name]) {
        $folders[$cat_name] = array();
    }
    $folders[$cat_name][] = $document;
}


//uksort($folders, "natcasesort");
array_multisort(array_keys($folders), SORT_NATURAL | SORT_FLAG_CASE , $folders);

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_my_files FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

?>
