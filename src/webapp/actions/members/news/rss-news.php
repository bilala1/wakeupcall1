<?php
$allow_staff = true; //whether a staff/member of a hotel has access to this page
include ACTIONS . '/members/protect.php';

$news_items = file_get_contents(SITE_PATH . '/data/rss.txt');
$news_items = unserialize($news_items);
$news_items = $news_items['hotel'];
//pre($items);
$db = mysqli_db::init();
// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_news FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

$today = date('Y-m-d');
$show_date = $_REQUEST['date'] ?: $today;

$items = array();
$date_list = array();


foreach($news_items as $item) {
    if($item !=null){
        $pubDate =date('Y-m-d', strtotime($item['pubDate']));
        $date_list[$pubDate] = !empty($date_list[$pubDate]) ? $date_list[$pubDate] + 1 : 1;
        if ($pubDate == $show_date) {
            $items[] = $item;
        }
    }
}
krsort($date_list);

$show_date = $_REQUEST['date'] ?: (!empty($date_list) ? array_shift(array_keys($date_list)) : '');

foreach($news_items as $item) { //They're probably going to be sorted in order to start with, but w/e
    if ($item !=null && date('Y-m-d', strtotime($item['pubDate'])) == $show_date) {
        $items[] = $item;
    }
}


?>
