<?php

/*$req_ssl = array('/full-membership.php','/members/account/billing.php');
    
$page = str_replace(BASEURL,'',$_SERVER['REQUEST_URI']);
$page = parse_url($page);
$page = $page['path'];

if(in_array($page,$req_ssl) && !$_SERVER['HTTPS']) {
    Header( "HTTP/1.1 301 Moved Permanently" );
    Header( "Location: " . HTTPS_FULLURL . $page);
    }
elseif(!in_array($page,$req_ssl) && $_SERVER['HTTPS']) {
    Header( "HTTP/1.1 301 Moved Permanently" );
    Header( "Location: " . HTTP_FULLURL . $page);
}*/

if($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    return;
}
if(!ActiveMemberInfo::GetMemberId()){
    $requrl = str_replace(BASEURL, '', $_SERVER['REQUEST_URI']);
    setcookie('wakeup_redirect', HTTP_FULLURL . $requrl, time()+3600, '/'); // cookie stays valid for an hour
    http::redirect(HTTP_FULLURL . '/login.php', array('notice' => 'You must log in.'));
}

//of another person logs in with same username and password, log the first person out
$db = mysqli_db::init();
$member = $db->fetch_one('SELECT members_lastsession FROM members WHERE members_id = ?',array(ActiveMemberInfo::GetMemberId()));
if(session_id() != $member['members_lastsession']){
    unset($_SESSION);
    http::redirect(HTTP_FULLURL .'/login.php?notice=You+have+been+logged+out+because+someone+else+with+this+username+and+password+has+logged+in.');
}

//check if entity to which this member belongs was deleted
// checks every hour

if(strtotime($_SESSION['deleted_check'].' + 1 hour') < strtotime(' now ')){
    
    if(members::get_entity_deleted(ActiveMemberInfo::GetMemberId())){
        members::logout();
    }
    else {
        $_SESSION['deleted_check'] = date(DATE_FORMAT_FULL, strtotime('now'));
    }
}
