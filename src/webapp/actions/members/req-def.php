<?php

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$term = $_REQUEST['term'];

$def_id = $db->fetch_singlet('SELECT definitions_id FROM definitions
                                WHERE definitions_word = ? AND
                                definitions_pending = 1',
                                array($_REQUEST['term']));
if(!$def_id) {                                
    //insert term into definitions table as pending
    $definitions = new mysqli_db_table('definitions');
    $data['definitions_word'] = $term;
    $data['definitions_pending'] = 1;
    $data['join_members_id'] = ActiveMemberInfo::GetMemberId();
    $data['definitions_requested_datetime'] = SQL('NOW()');
    $definitions->insert($data);
    $def_id = $definitions->last_id();
}
                            
$member = $db->fetch_one("SELECT CONCAT_WS(' ',members_firstname,members_lastname) AS members_fullname,
                                members_email FROM members
                                WHERE members_id = ?",
                                array(ActiveMemberInfo::GetMemberId()));
                                
//email concierge of new request
ob_start();
include VIEWS . '/emails/definition-request.php';
$message = ob_get_contents();
ob_clean();

mail::send(AUTO_EMAIL, CONCIERGE_EMAIL, 'New Definition Request | ' . SITE_NAME, $message);

//------------------------------------------------------------------------------------------------
// add admin notification
$member = members::get_contact_info(ActiveMemberInfo::GetMemberId());
$subject = 'New Definition Request | ' . SITE_NAME;
$message_notify = '<p>
                    A new Definition Request was submitted:<br />
                    <blockquote>
                        Email: <strong>' . $member['members_email'] . '</strong><br />
                        Name: <strong>' . $member['members_fullname'] . '</strong><br />
                        Organization: <strong>' . $member['entity_name'] . '</strong><br />
                        Definition: <strong><a href="'.FULLURL.'/admin/faqs/terms/edit.php?definitions_id='.$def_id.'">' . $term . '</a></strong><br />
                    </blockquote><br />';
notify::david($subject, $message_notify);
//------------------------------------------------------------------------------------------------


//email member thank you for request
ob_start();
include VIEWS . '/emails/thank-you4request.php';
$message = ob_get_contents();
ob_clean();

mail::send(AUTO_EMAIL, $member['members_email'], 'Thank You for Your Request | ' . SITE_NAME, $message);

echo json_encode('Thank you!');
?>
