<?php

include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$additional_services_notification = $db->fetch_singlet('SELECT notifications_additional_services 
                                                        FROM notifications 
                                                        WHERE join_members_id = ?',array(ActiveMemberInfo::GetMemberId()));

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_addit_services FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>