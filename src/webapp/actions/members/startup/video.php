<?php
include(ACTIONS . '/members/protect.php');

$redirect = $_COOKIE['wakeup_redirect'];

if (empty($redirect)) {
	$redirect = FULLURL . '/members/index.php';
}

if ($_POST) {

	$show_video = $_POST['members_show_startup_video'];

	$members_table = new mysqli_db_table('members');
	$members_table->update(array(
								'members_show_startup_video' => $show_video
						   ), ActiveMemberInfo::GetMemberId());

	http::redirect($redirect);
}
?>