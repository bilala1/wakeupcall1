<?

include(ACTIONS . '/members/protect.php');

if($_POST){
    $referer = $_SERVER['HTTP_REFERER'];
    //echo "Referer: " . $referer;exit;

    $data = $_POST;
    if(isset($data['referer'])) {
        $referer = $data['referer'];
    }

    if(!$data['suggestion']){
        $errors[] = 'Please type a suggestion';
    }

    if(!$errors){
        $members_table = new mysqli_db_table('members');
        $member = $members_table->get(ActiveMemberInfo::GetMemberId());
        $member['members_fullname'] = $member['members_firstname'].' '.$member['members_lastname'];

        ob_start();
        include(VIEWS . '/emails/admin-suggestion.php');
        $message = ob_get_contents();
        ob_end_clean();

        mail::send(
            $from = AUTO_EMAIL,
            //$to = SALES_EMAIL,
            $to = CONCIERGE_EMAIL,
            $subject =  'Suggestion | ' . SITE_NAME,
            $message = $message
        );

		$message_notify = 'A member has left a suggestion:<br />
			<br />
			<strong>Member:</strong> ' . $member['members_firstname'].' '.$member['members_lastname'] . '<br />
			<strong>Type:</strong> ' . $data['suggestion_type'] . '<br />
			<strong>Suggestion:</strong> ' . $data['suggestion'] . '<br />
			<br />';

		notify::david($subject, $message_notify);

        //send thank you email
        $to = $member['members_email'];
        #$to = 'test@wakeupcall.net';
        $from = CONCIERGE_EMAIL;
        $subject = 'Thank you for your Request | '.SITE_NAME;

        ob_start();
        include VIEWS . '/emails/thank-you4request.php';
        $message = ob_get_clean();

        mail::send($from,$to,$subject,$message);

        http::redirect($referer, array('notice' => 'Thank you for your suggestion'));
    }
    else {
        http::redirect($referer, array('notice' => 'Please type something in the suggestion box.'));
    }
}

?>
