<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$surveys_table = new mysqli_db_table('surveys');

/*$survey = $db->fetch_one('SELECT s.* FROM surveys AS s
    LEFT JOIN survey_responses As sr ON sr.join_surveys_id = s.surveys_id AND sr.join_members_id = ?
    WHERE sr.survey_responses_id IS NULL
    GROUP BY s.surveys_id
    ORDER BY s.surveys_datetime DESC
    LIMIT 1', array(ActiveMemberInfo::GetMemberId())
);*/

$survey = $surveys_table->get($_REQUEST['surveys_id']);

if(!$survey || $survey['surveys_status'] == 'inactive'){
    http::redirect(FULLURL . '/members/news/index.php');
}

$survey['options'] = $db->fetch_all('SELECT * FROM survey_options WHERE join_surveys_id = ?', array($survey['surveys_id']));

?>
