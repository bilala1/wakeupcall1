<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$surveys = surveys::get_unanswered(ActiveMemberInfo::GetMemberId());

if(count($surveys) == 1){
    http::redirect(FULLURL . '/members/surveys/index.php?surveys_id='.$surveys[0]['surveys_id']);
}

?>