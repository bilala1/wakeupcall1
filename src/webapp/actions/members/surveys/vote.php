<?php
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

if($_POST){
    $data = $_POST;
    $survey_responses = new mysqli_db_table('survey_responses');
    
    if(!$data['survey_options_id']){
        $message = 'You must make a vote!';
    }
    else if(!$db->fetch_singlet('SELECT COUNT(*) FROM survey_responses WHERE join_members_id = ? AND join_surveys_id = ?', array(ActiveMemberInfo::GetMemberId(), $data['surveys_id']))){
        // Actually tally the vote
        
        $survey_responses->insert(array(
            'join_members_id' => ActiveMemberInfo::GetMemberId(),
            'join_surveys_id' => $data['surveys_id'],
            'join_survey_options_id' => $data['survey_options_id'],
            'survey_responses_datetime' => SQL('NOW()')
        ));
        
        surveys::create_chart($data['surveys_id']);
        
        $message = 'Thank you for your vote!';
    }
    else {
        $message = 'You have already voted in this survey.';
    }
}
else {
    $message = 'There was a post error.';
}

?>
