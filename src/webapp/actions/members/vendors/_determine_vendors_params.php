<?php


if ($_REQUEST['clear']) {
    $_REQUEST['join_licensed_locations_id'] = array();
    $_REQUEST['vendors_show_hidden'] = '0';
}

if (array_key_exists('join_licensed_locations_id', $_REQUEST) || $_REQUEST['filter']) {
    $filter_licensed_locations_id = $_REQUEST['join_licensed_locations_id'];
    if($filter_licensed_locations_id && in_array('all', $filter_licensed_locations_id)) {
        $filter_licensed_locations_id = null;
    }
    $_SESSION['vendors_licensed_locations_id'] = $filter_licensed_locations_id;
} else if (array_key_exists('vendors_licensed_locations_id', $_SESSION)) {
    $filter_licensed_locations_id = $_SESSION['vendors_licensed_locations_id'];
}

if (array_key_exists('vendors_show_hidden', $_REQUEST)) {
    $_SESSION['vendors_show_hidden'] = $_REQUEST['vendors_show_hidden'];
    $vendors_show_hidden = $_REQUEST['vendors_show_hidden'];
} else if (array_key_exists('vendors_show_hidden', $_SESSION)) {
    $vendors_show_hidden = $_SESSION['vendors_show_hidden'];
} else {
    $vendors_show_hidden = $_SESSION['vendors_show_hidden'] = 0;
}