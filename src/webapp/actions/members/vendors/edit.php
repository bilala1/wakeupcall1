<?php
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$vendor = vendors::get_vendor($_REQUEST['vendors_id']);
if ($_REQUEST['vendors_id']) {
    $perms = UserPermissions::UserObjectPermissions('vendors', $vendor);
    if (!$perms['edit']) {
        http::redirect(BASEURL . '/members/vendors/index.php', array('notice' => 'You don\'t have permission to edit that vendor'));
    }
}

if ($vendor && $_REQUEST['vendors_id']) {
    if ($vendor['vendors_all_locations']) {
        $vendor['join_licensed_locations'] = array('all');
    }
    $bypass_location = 0;
    if(count($vendor['join_licensed_locations']) == 1 && $vendor['join_licensed_locations'][0] != 'all'){
        $vendor_location = $vendor['join_licensed_locations'][0];
        $bypass_location = 1;
    }
    $certificates = vendors::get_vendor_certificates($_REQUEST['vendors_id']);
    foreach($certificates as &$cert){
    $cert['certificates_coverages'] = implode(', ',
        $db->fetch_singlets(
            'SELECT COALESCE(NULLIF(certificate_coverages_shortname, \'\'), certificate_coverages_name)
              from certificate_coverages where join_certificates_id = ?',
            array($cert['certificates_id'])));
    $cert['has_emails'] = $db->fetch_singlet('SELECT count(1) FROM sent_emails_history where join_certificates_id = ?', array($cert['certificates_id']));
    }
}

$errors = array();

if ($_POST) {
    $data = $_POST;
    $validate = new validate($data);

    $validate->setEmail('vendors_email');
    $validate->setOptional('vendors_id')
        ->setOptional('return_to_cert')
        ->setOptional('vendors_phone')
        ->setOptional('vendors_street')
        ->setOptional('vendors_services')
        ->setErrors(array(
            'join_licensed_locations' => 'Please select at least one location'
        ))
        ->addMultiLocationSelectionValidation($data, 'vendor', $vendor);

    if ($data['vendors_phone'] != '') {
        $validate->setPhoneNum('vendors_phone', 'Phone Number');
    }

    $errors = $validate->test();

    if (!$errors) {
        $vendors = new mysqli_db_table('vendors');
        $_POST['vendors_name'] = stripslashes($_POST['vendors_name']);
        $_POST['vendors_contact_name'] = stripslashes($_POST['vendors_contact_name']);

        $vendors_x_licensed_locations = new mysqli_db_table('vendors_x_licensed_locations');
        
        if ($vendor) {
            $old_vendor = vendors::get_vendor($vendor['vendors_id']);
            $vendors->update($data, $vendor['vendors_id']);
            $vendors_id = $vendor['vendors_id'];
            $query = 'DELETE FROM vendors_x_licensed_locations WHERE join_vendors_id = ?';
            $wheres = array($vendors_id);
            if (!ActiveMemberInfo::_IsAccountAdmin() && $vendor['vendors_all_locations'] == 0) {
                $location_ids = UserPermissions::LocationIdsWithPermission('certificates');
                $in_clause = implode($location_ids, ',');
                $query .= ' AND join_licensed_locations_id IN (' . $in_clause . ')';
            }
            $db->query($query, $wheres);
            $notice = 'Vendor updated!';
        } else {
            $data['join_accounts_id'] = ActiveMemberInfo::GetAccountId();
            $vendors->insert($data);
            $vendors_id = $vendors->last_id();
            vendors_history::new_vendor_entry(ActiveMemberInfo::GetMemberId(), $vendors_id);
            $notice = 'Vendor added!';
        }
        
        foreach ($data['join_licensed_locations'] as $licensed_locations_id) {
            $vendor_x_licensed_location['join_vendors_id'] = $vendors_id;
            $vendor_x_licensed_location['join_licensed_locations_id'] = $licensed_locations_id;
            $vendors_x_licensed_locations->insert($vendor_x_licensed_location);
        }
        if ($vendor) {
            $new_vendor = vendors::get_vendor($vendors_id);
            vendors_history::vendors_updated_entry(ActiveMemberInfo::GetMemberId(), $new_vendor, $old_vendor);
        }
        if ($data['return_to_cert']) {
            $params = array('notice' => $notice);

            if (is_numeric($data['return_to_cert'])) {
                $params['certificates_id'] = $data['return_to_cert'];
            }

            http::redirect(BASEURL . '/members/my-documents/certificates/edit.php', $params);
        } else {
            http::redirect(BASEURL . '/members/vendors/index.php', array('notice' => $notice));
        }
    } else {
        $vendor = array_merge((array)$vendor, $data);
    }
}

?>
