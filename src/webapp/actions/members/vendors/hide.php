<?
include ACTIONS . '/members/protect.php';

$db = mysqli_db::init();
$vendor = vendors::get_vendor($_REQUEST['vendors_id']);
$perms = UserPermissions::UserObjectPermissions('vendors', $vendor, array('multiLocation' => true));

if($vendor && $perms['edit'])
{
    if($_REQUEST['hide_vendor'] == 1)
    {
        $vendor['vendors_hidden'] = 1;
        $notice = 'The vendor has been hidden';
    }
    else
    {
        $vendor['vendors_hidden'] = 0;
        $notice = 'The vendor has been un-hidden';
    }

    $vendors_table = new mysqli_db_table('vendors');
    $vendors_table->update($vendor, $vendor['vendors_id']);
    $vendors_id = $vendor['vendors_id'];
    
    vendors_history::hidden_changed(ActiveMemberInfo::GetMemberId(), $_REQUEST['vendors_id'], $_REQUEST['hide_vendor'] == 1);

    if($_REQUEST['hide_certs'] == 1) {
        $db->query('UPDATE certificates SET certificates_hidden = 1 WHERE join_vendors_id = ?', array($vendor['vendors_id']));
    }
}
else
{
    $notice = 'You do not have permissions to hide this vendor.';
}

http::redirect(BASEURL . '/members/vendors/index.php', array('notice' => $notice));

?>