<?php
include(ACTIONS . '/members/protect.php');

$paging = new paging(10, $_REQUEST['page']);

include '_determine_vendors_params.php';
$vendors = vendors::get_paged_vendors_list($paging, $_REQUEST['order'], $_GET['ind_search'],
    $vendors_show_hidden, $filter_licensed_locations_id);
$model = array(
    "vendors_show_hidden" => $vendors_show_hidden,
    "join_licensed_locations_id" => $filter_licensed_locations_id
);

?>
