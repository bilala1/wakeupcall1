<?php
include(ACTIONS . '/members/protect.php');
include(LIBS . '/PHPExcel/PHPExcel.php');

function date_string($date)
{
    return $date ? date('m/d/Y', strtotime($date)) : '';
}

function setup_worksheet($worksheetObj)
{
    $worksheetObj->getColumnDimension('A', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('B', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('C', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('D', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('E', true)->setAutoSize(true);
    $worksheetObj->getColumnDimension('F', true)->setAutoSize(true);
}

function write_header($worksheetObj)
{
    $worksheetObj->setCellValueByColumnAndRow(0, 1 , "Vendor's Name");

    $worksheetObj->setCellValueByColumnAndRow(1, 1, "Contact Name");

    $worksheetObj->setCellValueByColumnAndRow(2 ,1, "Email");

    $worksheetObj->setCellValueByColumnAndRow(3, 1, "Phone Number");

    $worksheetObj->setCellValueByColumnAndRow(4, 1, "Address");

    $worksheetObj->setCellValueByColumnAndRow(5, 1, "Services");

    $worksheetObj->getStyle("A1:F1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:F1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

function write_item($worksheetObj, $rowIndex, $vendor)
{
    $worksheetObj->setCellValueByColumnAndRow(0, $rowIndex, $vendor['vendors_name']);

    $worksheetObj->setCellValueByColumnAndRow(1, $rowIndex, $vendor['vendors_contact_name']);

    $worksheetObj->setCellValueByColumnAndRow(2, $rowIndex, $vendor['vendors_email']);

    $worksheetObj->setCellValueByColumnAndRow(3, $rowIndex, $vendor['vendors_phone']);

    $worksheetObj->setCellValueByColumnAndRow(4, $rowIndex, $vendor['vendors_street']);

    $worksheetObj->setCellValueByColumnAndRow(5, $rowIndex, $vendor['vendors_services']);

//    $worksheetObj->getCellByColumnAndRow(12, $rowIndex)->setValue($vendor['vendors_notes']);
//    $worksheetObj->getCellByColumnAndRow(12, $rowIndex)->getStyle()->getAlignment()->setWrapText(true);
//
//    $worksheetObj->setCellValueByColumnAndRow(13, $rowIndex, $vendor['vendors_history']);
//    $worksheetObj->getCellByColumnAndRow(13, $rowIndex)->getStyle()->getAlignment()->setWrapText(true);
}

$paging = new paging(35, $_REQUEST['page']);

$objPHPExcel = new PHPExcel();

$worksheetObj = $objPHPExcel->getSheet(0);

//get vendors
$show_hidden = isset($_SESSION['vendors_show_hidden']) ? $_SESSION['vendors_show_hidden'] : 0;

$vendors = vendors::get_all(null, $show_hidden);

//row starts at 2 for the header in 1
$rowIndex = 2;

write_header($worksheetObj, $rowIndex, $vendor);

setup_worksheet($worksheetObj);
$lfcr = "\n";

//Write vendors
foreach ($vendors as &$vendor)
{
    write_item($worksheetObj, $rowIndex, $vendor);
    $worksheetObj->getRowDimension($rowIndex, true)->setzeroHeight(true);
    $rowIndex++;
}

$cellRange = 'A0' . ':O' . $rowIndex;

$worksheetObj->getStyle($cellRange)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'phpxltmp') .".xlsx";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$objWriter->save($outputFileName);

header('Content-Type: application/xls');
header('Content-Disposition: attachment; filename=vendors.xlsx;');
header('Content-Length: ' . filesize($outputFileName));

readfile($outputFileName);
?>