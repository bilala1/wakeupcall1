<?
$allow_staff = true; //whether a staff/member of a hotel has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$webinars_table = new mysqli_db_table('webinars');
    $webinars_x_members_table = new mysqli_db_table('webinars_x_members');

$webinar = $webinars_table->get($_REQUEST['webinars_id']);

$webinar_passed = strtotime($webinar['webinars_datetime']) < strtotime(times::to_mysql_utc('now - 30 min')); 
$webinar_upcoming = (strtotime($webinar['webinars_datetime']) < strtotime(times::to_mysql_utc('now + 30 min'))) && (strtotime($webinar['webinars_datetime']) > strtotime(times::to_mysql_utc('now - 1 hour')));       //upcoming or during

$webinars_attending = $db->fetch_singlets('select join_webinars_id from webinars_x_members where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

//-- to fill webinar check box.
$notifications = $db->fetch_one('select * from notifications where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_webinars FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

if($webinar_passed && $webinar['webinars_file_recording']){
    $dimensions = flvdim(SITE_PATH . WEBINAR_DIR . $webinar['webinars_file_recording']);
}


function flvdim($name) {
    $file = @fopen($name, 'rb');
    if($file === false)
        return false;

    $header = fread($file, 2048);
    fclose($file);
    if($header === false)
        return false;

    return array(
        'width' => flvdim_get($header, 'width'),
        'height' => flvdim_get($header, 'height')
    );
}

function flvdim_get($header, $field) {
    $pos = strpos($header, $field);
    if($pos === false)
        return false;

    $pos += strlen($field) + 2;
    return flvdim_decode(ord($header[$pos]), ord($header[$pos + 1]));
}

function flvdim_decode($byte1, $byte2) {
    $high1 = $byte1 >> 4;
    $high2 = $byte2 >> 4;
    $low1 = $byte1 & 0x0f;

    $mantissa = ($low1 << 4) | $high2;

    // (1 + m�2^(-8))�2^(h1 + 1) = (2^8 + m)�2^(h1 - 7)
    return ((256 + $mantissa) << $high1) >> 7;
}

?>