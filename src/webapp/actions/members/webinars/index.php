<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

//notification checkboxes
$notifications = $db->fetch_one('
    SELECT notifications_webinars_update, notifications_training_update 
    FROM notifications 
    WHERE join_members_id = ?',
    array(ActiveMemberInfo::GetMemberId())
);

$webinars_attending = $db->fetch_singlets('select join_webinars_id from webinars_x_members where join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));

//----------------------------------------------------------------------------------------
// -- Upcoming webinars
//----------------------------------------------------------------------------------------
$paging_upcoming = new paging(20, $_GET['page_upcoming'], null, 'page_upcoming');

//$orderby_upcoming = $db->orderby('webinars_datetime_upcoming');
switch($_REQUEST['order']){
    case 'webinars_datetime_upcoming':
        $orderby_upcoming = ' ORDER BY webinars_datetime_upcoming '.$_REQUEST['by'].' ';
        break;
    case 'webinars_name_upcoming':
        $orderby_upcoming = ' ORDER BY webinars_name '.$_REQUEST['by'].' ';
        break;
    default:
        $orderby_upcoming = ' ORDER BY webinars_datetime_upcoming ASC ';
        
}
$wheres_upcoming[] = 'webinars_datetime > ?';
$params_upcoming[] = times::to_mysql_utc(date(DATE_FORMAT_FULL, strtotime('now - 30 min ')));

$upcoming = $db->fetch_all('
    select SQL_CALC_FOUND_ROWS *, webinars_name AS webinars_name_upcoming, webinars_datetime as webinars_datetime_upcoming, COUNT(join_members_id) as num_attendees
    from webinars
    left join webinars_x_members on join_webinars_id = webinars_id' .
    strings::where($wheres_upcoming) . '
    group by webinars_id ' .
    $orderby_upcoming .
    $paging_upcoming->get_limit(),
    $params_upcoming
);
//pre($db->debug());

$paging_upcoming->set_total($db->found_rows());

//----------------------------------------------------------------------------------------
// -- Past webinars
//----------------------------------------------------------------------------------------
$paging_past = new paging(20, $_GET['page_past'], null, 'page_past');

//unset($_REQUEST['order'], $_REQUEST['by']);
//$orderby_past = $db->orderby('webinars_datetime_past', 'DESC');
switch($_REQUEST['order']){
    case 'webinars_datetime_past':
        $orderby_past = ' ORDER BY webinars_datetime_past '.$_REQUEST['by'].' ';
        break;
    case 'webinars_name_past':
        $orderby_past = ' ORDER BY webinars_name '.$_REQUEST['by'].' ';
        break;
    default:
        $orderby_past = ' ORDER BY webinars_datetime_past DESC ';
        
}
$wheres_past[] = 'webinars_datetime < ?';
$params_past[] = times::to_mysql_utc(date(DATE_FORMAT_FULL, strtotime('now - 30 min ')));

$past = $db->fetch_all('
    select SQL_CALC_FOUND_ROWS *, webinars_name AS webinars_name_past, webinars_datetime as webinars_datetime_past, COUNT(join_members_id) as num_attendees
    from webinars
    left join webinars_x_members on join_webinars_id = webinars_id' .
    strings::where($wheres_past) . '
    group by webinars_id' .
    $orderby_past .
    $paging_past->get_limit(),
    $params_past
);
//pre($db->debug());
$paging_past->set_total($db->found_rows());

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_webinars FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>