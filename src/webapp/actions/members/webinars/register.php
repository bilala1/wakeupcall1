<?
$allow_staff = true; //whether a staff/member of a hotel has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$webinars_x_members_table = new mysqli_db_table('webinars_x_members');
$members_table = new mysqli_db_table('members');
$webinars_table = new mysqli_db_table('webinars');

$webinar = $webinars_table->get($_REQUEST['webinars_id']);

$webinar['webinars_datetime'] = times::from_mysql_utc($webinar['webinars_datetime']);

$params = array(
    'join_webinars_id' => $_REQUEST['webinars_id'],
    'join_members_id' => ActiveMemberInfo::GetMemberId()
);

//make sure there's no duplicates
$db->query('delete from webinars_x_members where join_webinars_id = ? and join_members_id = ?', $params);

$params['webinars_x_members_datetime'] = SQL('NOW()');
$webinars_x_members_table->insert($params);

//Send Email
$member = $members_table->get(ActiveMemberInfo::GetMemberId());
mail::send_template(
    $from     = AUTO_EMAIL,
    $to       = $member['members_email'],
    $subject  = 'Webinar Registration | ' . SITE_NAME,
    $template = 'webinar-registration.php',
    $vars     = get_defined_vars()
);

http::redirect(FULLURL . '/members/webinars/index.php', array('notice' => 'You have been registered for the webinar'));

?>