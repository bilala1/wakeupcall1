<?
$allow_staff = true; //whether a staff/member of a hotell has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

//notification checkboxes
$notifications = $db->fetch_one('
    SELECT notifications_webinars_update, notifications_training_update 
    FROM notifications 
    WHERE join_members_id = ?',
    array(ActiveMemberInfo::GetMemberId())
);

// whether colapse or expand information box at the top of the page
$infobox = $db->fetch_singlet('SELECT members_settings_infobox_training FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));


?>