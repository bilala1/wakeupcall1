<?
$allow_staff = true; //whether a staff/member of a hotel has access to this page
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();
$webinars_x_members_table = new mysqli_db_table('webinars_x_members');

$params = array(
    'join_webinars_id' => $_REQUEST['webinars_id'],
    'join_members_id' => ActiveMemberInfo::GetMemberId()
);

$db->query('delete from webinars_x_members where join_webinars_id = ? and join_members_id = ?', $params);

http::redirect(FULLURL . '/members/webinars/index.php', array('notice' => 'You have been unregistered for the webinar'));

?>