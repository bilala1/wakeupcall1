<?php

$db = mysqli_db::init();

if (!empty($_REQUEST['franchise_code'])) {
    $db = mysqli_db::init();
    $franchise = $db->fetch_one('SELECT * FROM franchises WHERE franchises_code = ?', array($_REQUEST['franchise_code']));

    if(empty($franchise))
    {
        $franchise_code_error = 'The group code is not valid.';
    }
    else
    {
        if($franchise)
        {
            $photo = $db->fetch_one('
                SELECT albums_photos_id, albums_photos_filename, join_albums_id
                FROM albums_photos
                WHERE albums_photos_id = ?', array($franchise['join_albums_photos_id']));

            $album = $db->fetch_one('
                SELECT albums_id, albums_root_folder
                FROM albums
                WHERE albums_id = ?', array($photo['join_albums_id']));

            $_SESSION['custom_logo_directory'] = $album['albums_root_folder'];
            $_SESSION['custom_logo_file_name'] = $photo['albums_photos_filename'];
        }
    }
}
else
{
    $franchise_code_error = 'The group code is required for a group discount.';
}

if(!empty($franchise_code_error))
{
    http::redirect(BASEURL .'/index.php?' . http_build_query(array(
        'code_error' => $franchise_code_error)));
}


include('register.php');

?>