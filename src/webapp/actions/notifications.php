<?php

$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');
$notifications_table = new mysqli_db_table('notifications');

if ($_POST) {
    $data = $_POST;

    $validate = new validate($data);

    $validate
        ->setErrors(array(
            'members_email_address' => 'Please enter the email address to unsubscribe.'
        ));

    $errors = $validate->test();

    $member = $db->fetch_one('SELECT * FROM members where members_email = ?', array($data['members_email_address']));

    if (!$errors) {
        if (!$member['members_id']) {
            $errors['members_email_address'] = 'That email address could not be found';
        } else if ($member['members_status'] != 'locked') {
            $errors['members_email_address'] = 'Please log in to manage your notifications';
        }
    }

    if (!$errors) {
        $db->query('UPDATE notifications set notifications_news_update = 0 WHERE join_members_id = ?', array($member['members_id']));
        $notice = 'Email address ' . $data['members_email_address'] . ' unsubscribed';
    }

}
?>