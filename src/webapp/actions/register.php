<?
$db = mysqli_db::init();
$members_table = new mysqli_db_table('members');
$notifications_table = new mysqli_db_table('notifications');
$members_settings_table = new mysqli_db_table('members_settings');
$forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');
$hotel_table = new mysqli_db_table('hotels');
$corp_locations_table = new mysqli_db_table('corporate_locations');


if($_POST){
    $data = $_POST;
    $errors = array();

    $_SESSION['registration']['type'] = $data['type'];                // 'join' or 'register'
    $_SESSION['registration']['btype'] = $data['btype'];              // 'hotel' or 'corporation'
    $_SESSION['registration']['other_loc'] = $data['other_loc'];       // 'Yes' or 'No'

    $free_trial = ($data['type'] != 'join');

    if(!isset($data['btype'])) {
        $errors['btype'] = 'Please choose a business type.';
    }
    else if($data['btype'] == 'hotel') {
        $data['hotels_name'] = $data['hotels_name_autocomplete'];
        $_REQUEST['hotels_name'] = $data['hotels_name'];

        //$hotel_exists = $hotel_table->fetch_one('SELECT COUNT(*) FROM hotels WHERE hotels_name = ?',trim($_REQUEST['hotels_name']));

        //if($hotel_exists) {
        //    $errors['hotels_name'] = 'Hotel already exists';
        //}

        $validate = new validate($data);
        $validate
            ->setAllOptional()
            ->unsetOptionals(array(
                'hotels_name' => 'Location Name',
                'hotels_address' => 'Address',
                'hotels_city' => 'City',
                'hotels_state' => 'State',
                'hotels_zip' => 'Zip',
                'hotels_members_firstname' => 'Firstname',
                'hotels_members_lastname' => 'Lastname',
                'hotels_members_title' => 'Title',
                'hotels_members_phone' => 'Phone',
                'hotels_members_email' => 'E-Mail',
                'hotels_pass' => 'Password'
            ))
            ->setMinLength('hotels_pass', 6)
            ->setMatch('hotels_pass', 'hotels_pass2')
            ->setMatch('hotels_members_email', 'hotels_members_email2')
            //->setUnique('hotels_name', 'hotels')
            ->setEmail('hotels_members_email')
            ->setPhoneNum('hotels_members_phone','Phone')
            ->setPhoneNum('hotels_members_fax','Fax');

        if(!validate::is_unique('members_email', 'members', $data['hotels_members_email'])){
            $errors['hotels_members_email'] = 'Email is already in use';
        }

        if($data['hotels_name'] && !validate::is_unique('licensed_locations_name', 'licensed_locations', $data['hotels_name'])){
            $errors['hotels_name'] = 'This location is already using the WAKEUP CALL service. Please change your location or, if you are the owner of this location, contact us.';
        }

        if(!in_array($data['ind_type'], array_keys(licensed_locations::get_location_types()))) {
            $errors['ind_type'] = 'Please select a location type';
        }
        $data['licensed_location_type'] = licensed_locations::get_type_from_full_type($data['ind_type']);

        // check to see if number of rooms should be included...
        if ($data['licensed_location_type'] == 'Hotel') {
            $validate->unsetOptional('hotels_num_rooms', 'number of rooms')
                ->setInteger('hotels_num_rooms');
        }

        $errors += $validate->test();
    }
    else if($data['btype'] == 'corporation') {
        $data['corporations_name'] = $data['corporations_name_autocomplete'];
        $_REQUEST['corporations_name'] = $data['corporations_name'];

        $data['first_hotels_name'] = $data['first_hotels_name_autocomplete'];
        $_REQUEST['first_hotels_name'] = $data['first_hotels_name'];

        $validate = new validate($data);
        $validate
            ->setAllOptional()
            ->unsetOptionals(array(
                'corporations_name' => 'Company Name',
                'corporations_address' => 'Address',
                'corporations_city' => 'City',
                'corporations_state' => 'State',
                'corporations_zip' => 'Zip',
                'members_firstname' => 'First Name',
                'members_lastname' => 'Last Name',
                'members_title' => 'Title',
                'members_phone' => 'Phone',
                'members_email' => 'Email',
                'members_password' => 'Password'
            ))
            ->setMinLength('members_password', 6)
            ->setMatch('members_password', 'members_password2')
            ->setUnique('members_email', 'members')
            ->setEmail('members_email')
            ->setMatch('members_email', 'members_email2')
            ->setPhoneNum('members_phone','Phone');

        if($data['other_loc'] == 'Yes'){
            $validate
                ->unsetOptionals(array(
                    'first_ind_type' => 'Industry Type',
                    'first_hotels_name' => 'Location Name',
                    'first_hotels_address' => 'Address',
                    'first_hotels_city' => 'City',
                    'first_hotels_state' => 'State',
                    'first_hotels_zip' => 'Zip',
                ));

            if($data['first_hotels_name'] && !validate::is_unique('hotels_name', 'hotels', $data['first_hotels_name'])){
                $errors['first_hotels_name'] = 'This location is already using the WAKEUP CALL service. Please change your location or, if you are the owner of this location, contact us.';
            }

            if(!in_array($data['first_ind_type'], array_keys(licensed_locations::get_location_types()))) {
                $errors['first_ind_type'] = 'Please select a location type';
            }
            $data['licensed_location_type'] = licensed_locations::get_type_from_full_type($data['first_ind_type']);
            // check to see if number of rooms should be included...
            if ($data['licensed_location_type'] == 'Hotel') {
                $validate->unsetOptional('first_hotels_num_rooms', 'number of rooms')
                    ->setInteger('first_hotels_num_rooms');
            }
        }

        if(!validate::is_unique('corporations_name', 'corporations', $data['corporations_name'])){
            $errors['corporations_name'] = 'This location is already using the WAKEUP CALL service. Please change your location or, if you are the owner of this location, contact us.';
        }

        if(!isset($data['separate'])) {
            $errors['separate'] = 'Please select yes or no.';
        }

        $errors += $validate->test();
    }

    if ($free_trial) {
        if(!empty($data['trial_code'])) {
            $code = $db->fetch_one('
                SELECT * FROM discount_codes WHERE discount_codes_code = ?
                AND discount_codes_start <= NOW()
                AND discount_codes_end >= NOW()
                AND discount_codes_type = \'trial\'', array($data['trial_code']));
            if (empty($code)) {
                $errors['trial_code'] = 'This trial code is invalid';
            }
            else if($code['discount_codes_max_uses'] > 0 && $code['discount_codes_uses'] >= $code['discount_codes_max_uses'])
            {
                $errors['trial_code'] = 'This trial code has been used too many times.';
            }
        } else {
            $errors['trial_code'] = "Trial code is required for a free trial.";
        }
    }

    if($data['music'] != 314){
        $errors[] = 'You must have javascript enabled';
    }

    //Save session variables
    //$_SESSION['register_form'] = $data;

    //pre($errors);
    if(empty($errors)){

        if($data['btype'] == 'corporation') {

            $member = arrays::filter_keys($data, array(
                'members_firstname',
                'members_lastname',
                'members_title',
                'members_phone',
                'members_fax',
                'members_email'
            ));

            $member = array_merge($member, array(
                'members_status' => 'free',
                'members_datetime' => times::to_mysql('now'),    //SQL('NOW()'),    It's put in session, can can't use SQL
                'members_type' => 'admin',
                'members_verified_code' => $data['type'] == 'join' ? '' : md5(uniqid(rand())),
                'members_password' => hash('sha256', $data['members_password'])
            ));


                
            $account = array(
                'accounts_type' => 'multi',
                'accounts_name' => $data['corporations_name'],
                'accounts_address' => $data['corporations_address'],
                'accounts_city' => $data['corporations_city'],
                'accounts_state' => $data['corporations_state'],
                'accounts_zip' => $data['corporations_zip'],
            );
            
            if($free_trial) 
            {
                $members_id = members::create($member);
                $account = array_merge($account, array('join_members_id' => $members_id));
                $accounts_id = accounts::create($account);
                members::join_account($members_id, $accounts_id);
            } else {
                $_SESSION['registration']['member'] = $member;
                $_SESSION['registration']['account'] = $account;
            }

            $crp = array();
            $crp['join_members_id']         = 	$members_id;
            $crp['join_forums_id']          = 	0; // will be updated during verification
            $crp['corporations_name']       = 	$data['corporations_name'];
            $crp['corporations_address']    =   $data['corporations_address'];
            $crp['corporations_city']       =   $data['corporations_city'];
            $crp['corporations_state']      =   $data['corporations_state'];
            $crp['corporations_zip']        =   $data['corporations_zip'];
            $crp['corporations_num_locations'] = $data['corporate_locations'];
            $crp['corporations_separate_from_hotels'] = $data['separate'] == 'Yes' ? 1 : 0;
            $crp['corporations_active']     = 	1;
            $crp['join_accounts_id'] = $accounts_id;

            if($free_trial) {
                $corporations_id = corporations::create($crp);

                //create corp location
                $corp_loc = array();
                $corp_loc['join_members_id']           =   $members_id;
                $corp_loc['join_corporations_id']      =   $corporations_id;
                $corp_loc['licensed_locations_name']   =   $crp['corporations_name'];
                $corp_loc['licensed_locations_address']=   $crp['corporations_address'];
                $corp_loc['licensed_locations_city']   =   $crp['corporations_city'];
                $corp_loc['licensed_locations_state']  =   $crp['corporations_state'];
                $corp_loc['licensed_locations_zip']    =   $crp['corporations_zip'];
                $corp_loc['licensed_locations_active'] =   1;
                $corp_loc['join_accounts_id'] = $accounts_id;
                $location_id = licensed_locations::create($corp_loc);

                $corp_loc = array();
                $corp_loc['corporate_locations_active']          =   1;
                $corp_loc['join_licensed_locations_id']          =   $location_id;
                corporate_locations::create($corp_loc);
            } else {
                $_SESSION['registration']['corporation'] = $crp;
            }

            if($data['other_loc'] == 'Yes'){
                //create hotel
                $hotel = array();
                $hotel_loc = array();
                $hotel_loc['join_members_id']           =   $members_id;
                $hotel_loc['join_corporations_id']      =   $corporations_id;
                $hotel_loc['licensed_locations_name']   =   $data['first_hotels_name'];
                $hotel_loc['licensed_locations_address']=   $data['first_hotels_address'];
                $hotel_loc['licensed_locations_city']   =   $data['first_hotels_city'];
                $hotel_loc['licensed_locations_state']  =   $data['first_hotels_state'];
                $hotel_loc['licensed_locations_zip']    =   $data['first_hotels_zip'];
                $hotel_loc['licensed_location_type']    = $data['licensed_location_type'];
                $hotel_loc['licensed_locations_active'] =   1;
                $hotel['hotels_industry_type']          =   $data['first_ind_type'];
                $hotel['hotels_num_rooms']              =   $data['first_hotels_num_rooms'];
                $hotel['hotels_active']                 =   1;
                $hotel['join_accounts_id'] = $accounts_id;

                if($free_trial) {
                    $location_id = licensed_locations::create($hotel_loc);

                    if($data['licensed_locations_type'] == 'Hotel'){
                        $hotel['join_licensed_locations_id'] = $location_id;
                        hotels::create($hotel);
                    }
                } else {
                    $_SESSION['registration']['hotel'] = $hotel;
                    $_SESSION['registration']['hotel_loc'] = $hotel_loc;
                }
            } // end - if adding another location



        }else{
            $member = array();
            $member['members_firstname'] = $data['hotels_members_firstname'];
            $member['members_lastname'] = $data['hotels_members_lastname'];
            $member['members_title'] = $data['hotels_members_title'];
            $member['members_phone'] = $data['hotels_members_phone'];
            $member['members_fax'] = $data['hotels_members_fax'];
            $member['members_email'] = $data['hotels_members_email'];

            $member = array_merge($member, array(
                'members_status' => 'free',
                'members_datetime' => times::to_mysql('now'),    //SQL('NOW()'),
                'members_type' => 'admin',
                'members_verified_code' => $data['type'] == 'join' ? '' : md5(uniqid(rand())),
                'members_password' => hash('sha256', $data['hotels_pass'])
            ));

            $account = array(
                'accounts_type' => 'single',
                'accounts_name' => $data['hotels_name'],
                'accounts_address' => $data['hotels_address'],
                'accounts_city' => $data['hotels_city'],
                'accounts_state' => $data['hotels_state'],
                'accounts_zip' => $data['hotels_zip'],
            );
            
            if($free_trial) 
            {
                $members_id = members::create($member);
                $account = array_merge($account, array('join_members_id' => $members_id));
                $accounts_id = accounts::create($account);
                members::join_account($members_id, $accounts_id);
            } else {
                $_SESSION['registration']['member'] = $member;
                $_SESSION['registration']['account'] = $account;
            }

            //Hotel is not defined yet here.
            $hotel = array();
            $hotel_loc = array();
            $hotel_loc['join_members_id']           =   $members_id;
            $hotel_loc['licensed_locations_name']   =   $data['hotels_name'];
            $hotel_loc['licensed_locations_address']=   $data['hotels_address'];
            $hotel_loc['licensed_locations_city']   =   $data['hotels_city'];
            $hotel_loc['licensed_locations_state']  =   $data['hotels_state'];
            $hotel_loc['licensed_locations_zip']    =   $data['hotels_zip'];
            $hotel_loc['licensed_location_type']    = $data['licensed_location_type'];
            $hotel_loc['licensed_locations_active'] =   1;
            $hotel_loc['join_accounts_id'] = $accounts_id;
            $hotel['hotels_num_rooms']              =   $data['hotels_num_rooms'];
            $hotel['hotels_active']                 =   1;
            $hotel['hotels_industry_type']            =   $data['ind_type'];
            $hotel['join_accounts_id'] = $accounts_id;

            if($free_trial) {
                $location_id = licensed_locations::create($hotel_loc);

                if($data['licensed_locations_type'] == 'Hotel'){
                    $hotel['join_licensed_locations_id'] = $location_id;
                    hotels::create($hotel);
                }
            } else {
                $_SESSION['registration']['hotel'] = $hotel;
                $_SESSION['registration']['hotel_loc'] = $hotel_loc;
            }

        }
        if($free_trial){
            //Update discount code uses
            $db->query('UPDATE discount_codes SET discount_codes_uses = ? WHERE discount_codes_id = ? ', array($code['discount_codes_uses'] + 1, $code['discount_codes_id']));

            //send email to David only for those who registered for free trial
            ob_start();
            include VIEWS . '/emails/register-verify.php';
            $message = ob_get_contents();
            ob_clean();
            mail::send(MEMBERSHIP_EMAIL, $member['members_email'], '15 Day Free Trial Welcome | '.SITE_NAME, $message);

			$message_notify = '<p>'. $member['members_firstname'] .' has signed up for a 15-day trial.</p>
				<p>Their 15 Day Free Trial will end on '. date(DATE_FORMAT, strtotime(' + '.FREE_MEMBER_PERIOD.' day')) .'.</p>
				<p>The user name on record is: '. $member['members_email'] .'</p>';
			notify::david('New 15 Day Free Trial', $message_notify);
            notify::notify_admin_new_trial_registration($members_id);
        }
        
        if($free_trial){
            if($data['btype'] == 'corporation') {
                $emails = array();
                $emails[] = $data['members_email'];
                if($data['other_loc'] == 'Yes'){
                    $emails[] = $data['first_hotels_members_email'];
                }
                $emails = implode(', ',$emails);
            }
            else {
                $emails = $member['members_email'];
            }
            
            http::redirect(BASEURL . '/wp/thank-you/?email=' . urlencode($emails));
        } else {
            //die('done 1st page of join');
            http::redirect(BASEURL . '/full-membership.php');
        }
    }
}

/*$_REQUEST = array_merge($_REQUEST, $_SESSION['register_form']);

if(http::is_ajax()){
    exit;
}*/

?>
