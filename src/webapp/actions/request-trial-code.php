<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 2/20/15
 * Time: 6:48 PM
 */

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_REQUEST['trial_code_firstname']) ||
        !isset($_REQUEST['trial_code_lastname']) ||
        !isset($_REQUEST['trial_code_email']) ||
        !isset($_REQUEST['trial_code_phone'])
    ) {
        echo("must included all required information");
        http_response_code(400);
    }
    $to = 'membership@wakeupcall.net';
    $msg = "Request for Free Trial.\r\n"
        . "First Name: " . $_REQUEST['trial_code_firstname'] . "\r\n"
        . "Last Name: " . $_REQUEST['trial_code_lastname'] . "\r\n"
        . "Email: " . $_REQUEST['trial_code_email'] . "\r\n"
        . "Phone: " . $_REQUEST['trial_code_phone'] . "\r\n";

    mail::send(
        $from = AUTO_EMAIL, // noreply@wakeupcall.net
        $to = $to,
        $subject = 'Free Trial Code Requested',
        $message = $msg
    );
    file_put_contents(SITE_PATH . "/data/trialRequest.txt",$msg,FILE_APPEND);
} else {
    framework::set_view("404.php");
}

