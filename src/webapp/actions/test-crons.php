<?php

if (DEPLOYMENT != 'production') {
    $script = $_REQUEST['script'];
    $notice = array();

    if($_POST['set_testing_email_address_filter']) {
        $_SESSION['testing_email_address_filter']  = $_POST['testing_email_address_filter'];
        $notice[] = 'Set email filter to ' . $_POST['testing_email_address_filter'];
    }

    if($_POST['clear_testing_email_address_filter']) {
        unset($_SESSION['testing_email_address_filter']);
        $notice[] =  'Cleared email filter';
    }

    //Whitelist to prevent arbitrary script execution
    switch ($script) {
        case 'certificate-expiration-reminder':
        case 'contract-expiration-reminder':
        case 'free-membership-expired':
        case 'create-bills':
        case 'auto-billing':
        case 'weekly-newsletter':
        case 'full-membership-expired-reminder':
        case 'free-membership-expired-reminder':
        case 'delete-members':
        case 'webinar-reminder-registered':
        case 'webinar-reminder-unregistered':
        case 'membership-renewal-reminder':
        case 'noonly':
        case 'update-rss':
        case 'check-faxes':
        case 'vendor-certificate-request':
        case 'entities_items_milestone_reminder':
        case 'contracts_milestone_reminder':
        case 'process-access-logs':

            if(!isset($_SESSION['testing_email_address_filter'])) {
                $notice[] = 'No script run - set an email address filter first!';
                break;
            }
            
            if(FALSE == include 'crons/' . $script . '.php') {
                $err = error_get_last();
                $notice[] = 'Warning occurred including ' . $script .': ' . $err;
            } else {
                $notice[] = 'Ran script ' . $script;
            }
            break;

        default:
            $notice[] = 'No script to run';
    }
}
?>