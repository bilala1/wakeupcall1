<?

include(ACTIONS . DIRECTORY_SEPARATOR .'admin/protect.php');

define("CORP_MEMBER_ID",166); // David DeMoss corporation member id
define("CORPORATION_ID",35); // David DeMoss corporation id
define("HOTEL_MEMBER_ID",194); // David DeMoss hotel member id. belongs to corp. above
define("IND_HOTEL_MEMBERS_ID",126); // Rick. independent hotel
define("WEBINAR_ID",20); //some sample existing webinar

$db = mysqli_db::init();
//member
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$member['members_verified_code'] = 'abcdef1234567890abcdef'; // -- register-verify.php
//post
$post = $db->fetch_one('SELECT * 
    FROM forums_posts AS fp
    LEFT JOIN forums_topics AS ft ON (ft.topics_id = fp.join_topics_id)
    WHERE posts_id = ?', array(70));
//data -- post from form..
$data = array();

$count_emails = 0; // total count of emails

// --------------------------------------------------------------------------------------------------------------
// -- Registration Related Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center">Registration Related Emails</h1>';

// Trial membership registration email -------------------------------------------------------------
$subject = 'New member validated email';
$location = members::get_contact_info($member['members_id']);
$member_id = $member['members_id'];
$to = MEMBERSHIP_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.'  Sent to: '.$to.' (email to admin)<br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-validated-email.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------


// Trial membership registration email -------------------------------------------------------------
$subject = '15 Day Free Trial Welcome ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.'  Sent to: '.$to.'<br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/register-verify.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Trial membership registration email -------------------------------------------------------------
$member_data = $db->fetch_one('SELECT * FROM members WHERE members_id = '.HOTEL_MEMBER_ID); // David DeMoss - hotel
$corporation = $db->fetch_one('SELECT * FROM corporations WHERE corporations_id = '.CORPORATION_ID); // David DeMoss corporation
$data['members_password'] = 'cba321';
$subject = 'New Corporate Location ';
$to = $member_data['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.'  Sent to: '.$to.' (free trial) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/new-corporation-location.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

//when registering right away
// Full membership HOTEL registration email --------------------------------------------------------
//todo test licensed locations
$licensed_locations = $db->fetch_one('SELECT * FROM members AS m JOIN licensed_locations AS ll ON (ll.join_members_id = m.members_id) WHERE members_id = '.HOTEL_MEMBER_ID);
$subject = 'Full Membership Welcome ';
$to = $licensed_locations['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (hotels: when joins right away) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-welcome-individual.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership Corporation registration email --------------------------------------------------
$subject = 'Full Membership Welcome ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (corporate: when joins right away) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-welcome-corporate.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify Admin Corporate account canceled ---------------------------------------------------------
$subject = ' Member has canceled an account';
$to = MEMBERSHIPS_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-canceled-account.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------



echo '<h3 style="text-align:center">Registration Expiration Related Emails</h3>';

// Free membership expiration email ----------------------------------------------------------------
$subject = 'End of 15 Day Free Trial ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/free-membership-expired.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// email to Admin Free membership expiration email -------------------------------------------------
$subject = 'Member\'s trial account expired ';
$to = MEMBERSHIP_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-trial-expired.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Free membership expiration Remainder email : sent after 2 weeks ---------------------------------
$subject = 'Free Membership Expired  ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (after 2 weeks) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/free-membership-expired-reminder.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Free membership expiration Remainder email : sent after 2 and 4 months --------------------------
$subject = 'Free Membership Expired  ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (after 2 and 4 months) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/free-membership-expired-reminder-months.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership expiration Remainder email : sent after 1 month ---------------------------------
$subject = 'Full Membership Expired  ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (after 1 month) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-expired-reminder.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership expiration Remainder email : sent after 3 months --------------------------------
$subject = 'Full Membership Expired  ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (after 3 months) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-expired-reminder-months.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership renewal credit card failed ------------------------------------------------------
$subject = 'Credit Card Did Not Go Through for Renewal ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/payment-failed.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify Admin: Full membership renewal credit card failed  ---------------------------------------
$subject = 'Member\'s account expired: payment failed  ';
$to = MEMBERSHIP_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-membership-expired-payment-failed.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// after free trial expires and member becomes full member

// Full membership Corporation registration email --------------------------------------------------
$subject = 'Full Membership Welcome ';
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss - hotel
$to = $member['members_email'];
$member['members_verified_code'] = 'abcdef9876543210abcdef';
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (corporate: when joins after being free member) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-welcome-corporate-verification.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership Hotel registration email --------------------------------------------------------
$subject = 'Full Membership Welcome ';
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.HOTEL_MEMBER_ID); // David DeMoss - hotel
$member['members_verified_code'] = 'abcdef9876543210abcdef';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (hotels: when joins after being free member) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-welcome-individual-verification.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------



// renewal after expiration

// Full membership Hotel renewal after expiration email --------------------------------------------
// member-payment-failed.php
$subject = 'Full Membership Welcome ';
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.HOTEL_MEMBER_ID); // David DeMoss
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (hotels: when renews after expiration) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-rewelcome-individual.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership Corporation renewal after expiration email --------------------------------------
// member-payment-failed.php
$subject = 'Full Membership Welcome ';
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.MEMBERSHIP_EMAIL.' Sent to: '.$to.' (corporation: when renews after expiration) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-rewelcome-corporate.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify admin about Full membership Corporation renewal after expiration email ------------------
// actions/member-payment-failed.php ---> notify::notify_admin_member_full_account_reactivated()
$subject = 'Member has reactivated an account ';
$location = members::get_contact_info($member['members_id']);
$to = MEMBERSHIPS_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-full-reactivated-account.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify Admin: Full membership renewal credit card failed  ---------------------------------------
// actions/full-membership.php ---> notify::notify_admin_member_free_trial_reactivation()
$subject = 'Member reactivated expired account (trial member became full member)  ';
$to = MEMBERSHIP_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-trial-expired-reactivated.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------



// Full membership renewal: hotel continuing -------------------------------------------------------
$subject = ' Continuing Membership ';
$member = $db->fetch_one('SELECT members_firstname,members_lastname,
                                     members_email
                                  FROM members WHERE members_id = '.IND_HOTEL_MEMBERS_ID); // David DeMoss
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (hotel) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-auto-rewelcome-individual.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Full membership renewal: corporation continuing -------------------------------------------------
$subject = ' Continuing Membership ';
$member = $db->fetch_one('SELECT members_firstname,members_lastname,
                                     members_email
                                  FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
try
{
    $data_customer = Stripe_Customer::retrieve($member['members_stripe_id']);
    $member = members::get_customer_credit_card_data($member);

}
catch(Exception $e)
{

}
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. MEMBERSHIP_EMAIL.' Sent to: '.$to.' (corporation) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/full-membership-auto-rewelcome-corporation.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------



echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Certificates Related Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Certificates Related Emails</h1>';

// Certificate Expiration Reminder  -----------------------------------------------------------------
// sent 45 or 30 or 15 or 0 days before expiration date
$subject = 'Certificate Expiring ';
$certificate  = $db->fetch_one('select *
                                        from certificates
                                        left join members on members_id = join_members_id
                                        left join vendors on vendors_id = join_vendors_id
                                        WHERE certificates.join_members_id = ?',array(CORP_MEMBER_ID));
//extract($certificate);
if($members_type == 'corporation') {
    $loc_name = $db->fetch_singlet('SELECT corporations_name FROM corporations WHERE join_members_id = ?',array($members_id));
}
else {
    //todo test licensed locations
    $loc_name = $db->fetch_singlet('SELECT licensed_locations_name FROM licensed_locations WHERE join_members_id = ?',array($members_id));
}
$from = $certificate['members_email'];
$to = $certificate['vendors_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. $from.' Sent to: '.$to.' (sent 45 or 30 or 15 or 0 days before expiration date) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/certificate-expiration-reminder.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Certificate Expired   ---------------------------------------------------------------------------
// sent with 45,30,15,0 days before expiration date
$subject = 'Certificate Expired ';
$from = AUTO_EMAIL;
$to = $certificate['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. $from.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/certificate-expired.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Certificate Request   ---------------------------------------------------------------------------
//
$subject = 'Certificate Request ';
$from = $certificate['members_email'];
$to = $certificate['vendors_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. $from.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/cert-request.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Claims Related Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Claims Related Emails</h1>';

// Certificate Request   ---------------------------------------------------------------------------
//
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
//todo test licensed locations
$hotelname = $db->fetch_singlet('SELECT licensed_locations_name FROM licensed_locations WHERE join_members_id = ?', array($member['members_id']));
if($hotelname == '') {
    $hotelname = $db->fetch_singlet('SELECT corporations_name FROM corporations WHERE join_members_id = ?', array($member['members_id']));
}
$carrier = $db->fetch_one('SELECT * FROM carriers WHERE join_members_id = ?', array($member['members_id']));
$subject = 'Notice of Claim for ' . $hotelname;
$from = $certificate['members_email'];
$to = $certificate['vendors_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. $from.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/claim2carrier.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Miscellaneous Users Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Miscellaneous Users Emails</h1>';

// Thank you for your request email ----------------------------------------------------------------
$subject = 'Thank You for Your Request ';
$member = $db->fetch_one('SELECT *, CONCAT_WS(" ", members_firstname,members_lastname) AS members_fullname FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (sent after suggestion, asking faq, requesting defintion) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/thank-you4request.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Employment Law Question email -------------------------------------------------------------------
$subject = 'Employment Law Question ';
$data['members_fullname']   = $member['members_fullname'];
$data['members_title']      = $member['members_title'];
$data['entity_name']        = $location['entity_name'];
$data['address']            = $location['address'];
$data['city']               = $location['city'];
$data['state']              = $location['state'];
$data['zip']                = $location['zip'];
$data['members_email']      = $member['members_email'];
$data['parties_involved']   = 'John Doe';
$data['question_content']   = ' How can our company follow the risk management process specification which is concerned with contemporary privacy laws?';
$to = 'wakeupcall@wilsonelser.com';
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.$member['members_email'].' Sent to: '.$to.' <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/elaw_contact.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//---------------------------------------------------------------------------------------------------

// Forgot Password / Password Reset email ----------------------------------------------------------
$subject = 'Password Reset ';
$member = $db->fetch_one('SELECT *, CONCAT_WS(" ", members_firstname,members_lastname) AS members_fullname FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$newpass .= strings::getrandchar('lc', 3);
$newpass .= strings::getrandchar('num', 3);
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forgot-password.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Newsletter email --------------------------------------------------------------------------------
$subject = 'Newsletter ';
$member = $db->fetch_one('SELECT *, CONCAT_WS(" ", members_firstname,members_lastname) AS members_fullname FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.'  <br />';
$forums_model = new forums;
//new articles in the past week
$recent_articles = $db->fetch_all('
            select *
            from articles
            where
                articles_newsdate > NOW() - INTERVAL 1 WEEK and
                articles_newsdate < NOW() and
                articles_active = 1'
);

//faqs answered in the past week
$recent_faqs = $db->fetch_all('
            select *
            from faqs
            where faqs_published = 1
            AND join_faqs_categories_id = 9
            ORDER BY RAND()
            LIMIT 2'
);

// hospitality news
$hospitality_news = file_get_contents(SITE_PATH . '/data/news-rss.txt');
$hospitality_news = unserialize($hospitality_news);
foreach($hospitality_news as $hn){
    if(strtotime($hn['pubDate']) < strtotime(date('Y-M-d', strtotime('now'))) && strtotime($hn['pubDate']) >= strtotime('now - 1 week') ) {
        if($hn['type'] != 'smartbrief'){ //let's exclude smartbrief because it has long url and advertisements
            $last_week_hospitality_news[] = $hn;
        }
    }
}
if($last_week_hospitality_news){
    $keys = array();
    $keys[] = rand(0,(count($last_week_hospitality_news)-1));
    if(count($last_week_hospitality_news)>1){
        while(count($keys)<2){ // two news stories to be displayed
            $keys[] = rand(0,(count($last_week_hospitality_news)-1));
            $keys = array_unique($keys);
        }
    }
    foreach($keys as $k){
        $newsletter_hospitality_news[] = $last_week_hospitality_news[$k]; // first random news
    }
    unset($keys);
}

// HR news
$hr_news = file_get_contents(SITE_PATH . '/data/hr-rss.txt');
$hr_news = unserialize($hr_news);

foreach($hr_news as $hrn){
    if(strtotime($hrn['pubDate']) < strtotime(date('Y-M-d', strtotime('now'))) && strtotime($hrn['pubDate']) > strtotime('now - 1 week') ) {
        $last_week_hr_news[] = $hrn;
    }
}
if($last_week_hr_news){
    $keys = array();
    $keys[] = rand(0,(count($last_week_hr_news)-1));
    if(count($last_week_hr_news)>1){
        while(count($keys)<2){ // two news stories to be displayed
            $keys[] = rand(0,(count($last_week_hr_news)-1));
            $keys = array_unique($keys);
        }
    }
    foreach($keys as $k){
        $newsletter_hr_news[] = $last_week_hr_news[$k]; // first random news
    }
}

//topics with the most posts in the past week (from model forums->get_top_wakeupcall_topics() )
// with the exception of having 10 weeks instead of 1.. to make sure there are some forum posts..
$popular_forum_topics =  $db->fetch_all('
            select forums_topics.*, CONCAT_WS(" ", members_firstname, members_lastname) AS members_name
            from forums_topics
            join forums on forums_id = join_forums_id
            join forums_posts on join_topics_id = topics_id
            LEFT JOIN members ON (forums_topics.join_members_id = members.members_id)
            where
                join_forums_id = 1 and
                posts_postdate > NOW() - INTERVAL 10 WEEK
            group by join_topics_id
            order by count(join_topics_id) desc, topics_last_posts_date
            limit 5');

$webinars = $db->fetch_all('SELECT webinars_name,webinars_datetime,webinars_description
                            FROM webinars
                            WHERE webinars_datetime > NOW()
                            ORDER BY webinars_datetime ASC
                            LIMIT 0,2');
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/weekly-newsletter.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------


echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Miscellaneous Admin Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Miscellaneous Admin Emails</h1>';

// Notify Admin: Suggestion  -----------------------------------------------------------------------
// actions/members/suggestion-box.php
$subject = 'Suggestion ';
$data['suggestion_type'] = 'General Question/Request';
$data['suggestion'] = 'Thank you very much for providing this service. It helped us with risk management';
$to = CONCIERGE_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-suggestion.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify Admin: New Definition Request  -----------------------------------------------------------
// actions/members/suggestion-box.php
$subject = 'New Definition Request';
$to = CONCIERGE_EMAIL;
$term = 'Risk Management Process Area';
$def_id = 1001;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '. AUTO_EMAIL.' Sent to: '.$to.' (email to admin) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/definition-request.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Notify Admin: FAQ Question from a Member email --------------------------------------------------
$subject = 'Question from a Member ';
$data['faqs_question'] = 'Why are there so many questions in the FAQ?';
$member = $db->fetch_one('SELECT *, CONCAT_WS(" ", members_firstname,members_lastname) AS members_fullname FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$member['entity_name'] = $location['entity_name'];
$to = CONCIERGE_EMAIL;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.$member['members_email'].' Sent to: '.$to.' <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/faq-question.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------


echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Forum Related Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Forum Related Emails</h1>';

// forum report abuse email ------------------------------------------------------------------------
$subject = 'Abuse Reported in Message Forum';
$to = 'monitor@wakeupcall.net';
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.'<br />';
$data['posts_id'] = 25;
$data['report_desc'] = 'This post violates ethical standard of electronic communications.';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forum-report-abuse.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// New reply to topic email ------------------------------------------------------------------------
$subject = 'New Reply To A Topic';
$to = 'monitor@wakeupcall.net';
$members_firstname = 'Monitor';
$topics_id = 1001;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (sent to monitor on new replies)  <br /> ';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forum-new-reply.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// New reply to WATCHED topic email ----------------------------------------------------------------
$subject = 'New Reply To A Watched Topic';
$to = $member['members_email'];
$members_firstname = $member['members_firstname'];
$topics_id = 1001;
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (sent to users who follow/watch forum topic) <br />  ';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forum-new-reply.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// New topic in corporate forum  -------------------------------------------------------------------
$member['corporations_name'] = $location['entity_name'];
$subject = 'New Topic in '.$member['corporations_name'].' Forum';
$to = $member['members_email'];
$members_firstname = $member['members_firstname'];
$data['topics_title']  = 'New Security Measures';
$data['topics_body']   = 'Starting next month, our organization will start new security measures. For more information contact Jane Doe.';
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (sent to members when new topic is posted in corporate forum) <br />  ';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forums-new-topic-corporate-forum.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// New topic in Message forum  ---------------------------------------------------------------------
$subject = 'New Topic in Message Forum';
$to = $member['members_email'];
$members_firstname = $member['members_firstname'];
$data['topics_title']  = 'New Security Measures';
$data['topics_body']   = 'Starting next month, our organization will start new security measures. For more information contact Jane Doe.';
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (sent to members when new topic is posted in message forum) <br />  ';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/forums-new-topic-message-forum.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------


echo '<hr />';
// --------------------------------------------------------------------------------------------------------------
// -- Webinars Related Emails
// --------------------------------------------------------------------------------------------------------------
echo '<h1 style="text-align:center"> Webinars Related Emails</h1>';

// Webinar Registration email ----------------------------------------------------------------------
$subject = 'Webinar Registration ';
$member = $db->fetch_one('SELECT * FROM members WHERE members_id = '.CORP_MEMBER_ID); // David DeMoss
$to = $member['members_email'];
$webinars_table = new mysqli_db_table('webinars');
$webinar = $webinars_table->get(WEBINAR_ID);
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.'  <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/webinar-registration.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Webinar Reminder for registered email -----------------------------------------------------------
$subject = 'Webinar Reminder ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (reminder for registered members) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/webinar-reminder-registered.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------

// Webinar Reminder for unregistered email ---------------------------------------------------------
$subject = 'Webinar Reminder ';
$to = $member['members_email'];
echo '<span style="font-size: 140%">'.$subject.'</span>   | From: '.AUTO_EMAIL.' Sent to: '.$to.' (reminder for unregistered members) <br />';
include(VIEWS . DIRECTORY_SEPARATOR . 'emails/webinar-reminder-unregistered.php');
echo '<hr style="border: dotted thin #ccc" />';
$count_emails ++;
//--------------------------------------------------------------------------------------------------


echo '<h1>Total Emails: '.$count_emails.'</h1>';


?>