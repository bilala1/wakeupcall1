<?php
$db = mysqli_db::init();

if($_REQUEST['code'] == ''){
    echo 'Code is invalid';
    exit;
}
$members = new mysqli_db_table('members');
$members->members_verified_code = $_REQUEST['code'];
$member = $members->fetch_one();

if($member){
   $members->update(array(
        'members_verified_code' => ''
    ), $member['members_id']);
   
   // notify admin that member has verified their email
   notify::notify_admin_member_validated_email($member['members_id']);


    //if corporation, create forum if one does not exist (if verification resent)
    $corporation = $db->fetch_one('SELECT * FROM corporations WHERE join_accounts_id = ? ', array($member['join_accounts_id']));

    //If forum not created, create
    if($corporation && $corporation['join_forums_id'] == 0){
        //create corporate forum
        forums::create_corporate_forum($member['members_id']);
    }
    
    http::redirect(BASEURL . '/login.php', array('notice' => 'You have verified your account. Please log in.'));
}else{
    http::redirect(BASEURL . '/login.php', array('notice' => 'This account has already been verified.'));
}
?>
