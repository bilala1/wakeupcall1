<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/23/15
 * Time: 7:25 AM
 * Called when we get a bounce from a claims submit email.
 */

$failed_email = $_POST['recipient']; // the email address that failed
$failed_name = $_POST['vendors_name'];
$vendors_id = $_POST['vendors_id'];
$certificates_id = $_POST['certificates_id'];
$members_email = $_POST['members_email'];
$members_firstname = $_POST['members_firstname'];
$template = 'cert-request-email-failed.php';
mail::send_template(
    $from     = AUTO_EMAIL,
    $to       = $members_email,
    $subject  = 'WAKEUP CAll: Certification Request Email Problem for '.$failed_name,
    $template = $template,
    $vars     = get_defined_vars(),
    $cc       = null,
    $bcc      = SUPPORT_EMAIL
);
