<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/23/15
 * Time: 7:25 AM
 * Called when we get a bounce from a claims submit email.
 */

$failed_email = $_POST['recipient']; // the email address that failed
$failed_name = $_POST['carrier_name'];
$members_email = $_POST['members_email'];
$members_firstname = $_POST['members_firstname'];
$claim_id = $_POST['claim_id'];
$carrier_id = $_POST['carrier_id'];
$sent_emails_history_id = $_POST['sent_emails_history_id'];
$template = 'claims-carrier-email-failed.php';
mail::send_template(
    $from     = AUTO_EMAIL,
    $to       = $members_email,
    $subject  = 'WAKEUP CAll: Claims Email Problem for '.$failed_name,
    $template = $template,
    $vars     = get_defined_vars(),
    $cc       = null,
    $bcc      = SUPPORT_EMAIL
);

claims_history::create_claim_entry($_POST['members_id'], $claim_id, 'Claim email submission <b>failed</b> to send to '.$failed_name);
