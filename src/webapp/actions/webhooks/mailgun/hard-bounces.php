<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/23/15
 * Time: 7:24 AM
 */
include LIBS . '/http_response_code.php';
include 'mailgun-helpers.php';

if (!verifyWebhookSignature()) {
    // can use the http_response_code 
    //http_response_code(401);
    header("HTTP/1.1 401 Unauthorized");
    return;
}

$event_id = $_POST['event_id'];

$sent_emails_history_id = $_POST['sent_emails_history_id'];
$sent_emails_history_reason = $_POST['error'];
$db = mysqli_db::init();
$db->query("UPDATE sent_emails_history set sent_emails_history_status = ?, sent_emails_history_reason = ? where sent_emails_history_id =  ?",
    array('fail', $sent_emails_history_reason, $sent_emails_history_id));

switch($event_id) {
    case 'claims-submit' :
        include 'events/claims-carrier-email-failed.php';
        break;
    case 'cert-request' :
        include 'events/cert-request-email-failed.php';
        break;
}