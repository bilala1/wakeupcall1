<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/23/15
 * Time: 11:46 AM
 */

function verifyWebhookSignature($postData = NULL) {
    if(is_null($postData)) {
        $postData = $_POST;
    }
    $hmac = hash_hmac('sha256', "{$postData["timestamp"]}{$postData["token"]}", MAILGUNAPIKEY);
    $sig = $postData['signature'];
    if(function_exists('hash_equals')) {
        // hash_equals is constant time, but will not be introduced until PHP 5.6
        return hash_equals($hmac, $sig);
    }
    else {
        return ($hmac == $sig);
    }
}