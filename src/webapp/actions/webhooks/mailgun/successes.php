<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 5/23/15
 * Time: 7:24 AM
 */
include LIBS . '/http_response_code.php';
include 'mailgun-helpers.php';

if (!verifyWebhookSignature()) {
    // can use the http_response_code 
    //http_response_code(401);
    header("HTTP/1.1 401 Unauthorized");
    return;
}

$event_id = $_POST['event_id'];

include 'events/email-success.php';

//switch($event_id) {
//    case 'claims-submit' :
//        include 'events/email-success.php';
//        break;
//    case 'cert-request' :
//        include 'events/email-success.php';
//        break;
//}