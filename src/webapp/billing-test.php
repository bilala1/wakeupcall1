<?php
include './script-test-include.php';


function AddReportCell($string)
{
    return '<td style="border-style: solid;border-width: 1px;">' . $string . '</td>';
}

$db = mysqli_db::init();

if($_POST)
{

    echo '<table style="border-style: solid;border-width: 3px;"><tr>'
        .AddReportCell('<b>Error>status</b>'). AddReportCell( '<b>Message</b>').'</tr>';


    $billing_member = $db->fetch_one('
    select *
    from members
    left join accounts as a on a.join_members_id = members_id
    where members_email = "'. $_POST['user_email'] . '"');

    if($billing_member)
    {
        echo '<tr>'.AddReportCell('Selected Member:') .AddReportCell($_POST['user_email']) . '</tr>';
        $count_bills = $db->fetch_all('select * from bills where join_members_id = ' . $billing_member['members_id']);
    }
    else
        echo '<tr>'.AddReportCell('Member Not Found:') .AddReportCell($_POST['user_email']). '</tr>';

    if($billing_member && $_POST['billing_yearly'] && $_POST['billing_yearly']=="on")
    {
        $db->fetch_one('update members set members_billing_type = "yearly" where members_id =' .$billing_member['members_id']);
        echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Members billing set to yearly') . '</tr>';
    }

    if($billing_member && $_POST['billing_monthly'] && $_POST['billing_monthly']=="on")
    {
        $db->fetch_one('update members set members_billing_type = "monthly" where members_id =' .$billing_member['members_id']);
        echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Members billing set to monthly') . '</tr>';
    }

    if($billing_member && $_POST['user_exempt'] && $_POST['user_exempt']=="on")
    {
        $db->fetch_one('update members set members_status = "exempt" where members_id =' .$billing_member['members_id']);
        echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Members status set to exempt') . '</tr>';
    }

    if($billing_member && $_POST['user_member'] && $_POST['user_member']=="on")
    {
        $db->fetch_one('update members set members_status = "member" where members_id =' .$billing_member['members_id']);
        echo '<tr>'.AddReportCell('STATUS') . AddReportCell('Members status set to member') . '</tr>';
    }

    if($billing_member && $_POST['reset_bills'] && $_POST['reset_bills']=="on")
    {
        $db->fetch_one('delete from bills where join_members_id = ' .$billing_member['members_id']);

        echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Members Bills reset') . '</tr>';
    }

    if($billing_member && $_POST['bills_new_date'] && preg_match('/\d{4}-\d{2}-\d{2}/', $_POST['bills_new_date']))
    {
        if(count($count_bills) == 0)
        {
            echo '<tr>'.AddReportCell('ERROR') .AddReportCell('Bill day subtract failed, no bills, run create-bills.php.') . '</tr>';
        }

        $db->query(
            'update bills set bills_datetime = ?
             WHERE join_members_id = ? AND bills_type != \'location\' ORDER BY bills_datetime DESC LIMIT 1',
            array($_POST['bills_new_date'], $billing_member['members_id']));

        echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Set recent bill date to '.$_POST['bills_new_date']) . '</tr>';
    }

    if($billing_member && $_POST['run_createbill'] && $_POST['run_createbill']=="on")
    {
        if($billing_member['member_stats'] == 'exempt')
        {
            echo '<tr>'.AddReportCell('ERROR') .AddReportCell('Create bills script has failed member is exempt.') . '</tr>';
        }
        else
        {
            require_once('./actions/crons/create-bills.php');
            echo '<tr>'.AddReportCell('STATUS') .AddReportCell('Create bills script has been run') . '</tr>';
        }
    }

    if($billing_member && $_POST['run_autobill'] && $_POST['run_autobill']=="on")
    {

        if($billing_member['member_stats'] == 'exempt')
        {
            echo '<tr>'.AddReportCell('ERROR') .AddReportCell('auto billing script failed member is exempt.') . '</tr>';
        }
        else if(count($count_bills) == 0)
        {
            echo '<tr>'.AddReportCell('ERROR') .AddReportCell('auto billing script failed member has no bills, run create-bills.php.') . '</tr>';
        }
        else
        {
            require_once('./actions/crons/auto-billing.php');
            echo '<tr>'.AddReportCell('STATUS') .AddReportCell('auto billing script has been run') . '</tr>';
        }
    }
    echo '</table>';
}


    echo '<form name="BillingEMailInitForm" method="post">';

    echo '<table style="border-style: solid;border-width: 3px;"><tr>'
        .AddReportCell('<b>User Management</b>'). '</tr>';

    echo '<tr>';
    echo AddReportCell('Billing Test E-Mail: <input name="user_email" type="text" value="'.$_POST['user_email'].'"> *required for all');
    echo '</tr>';

    echo '<tr>';
    echo AddReportCell('Set User Billing To Monthly: <input name="billing_monthly" type="checkbox">');
    echo '</tr>';

    echo '<tr>';
    echo AddReportCell('Set User Billing To Yearly: <input name="billing_yearly" type="checkbox">');
    echo '</tr>';

    echo '<tr>';
    echo AddReportCell('Set User Exempt: <input name="user_exempt" type="checkbox">');
    echo '</tr>';

    echo '<tr>';
    echo AddReportCell('Set User Member: <input name="user_member" type="checkbox">');
    echo '</tr>';
    echo '</table><br><br>';


    echo '<table style="border-style: solid;border-width: 3px;"><tr>'
        .AddReportCell('<b>Bills Management</b>'). '</tr>';

    echo '<tr>';
    echo AddReportCell('Clear Existing Bills: ' . '<input name="reset_bills" type="checkbox">');
    echo '</tr>';

    echo '<tr>';
    echo AddReportCell('Set most recent bill date (yyyy-mm-dd): ' . '<input name="bills_new_date" type="text">');
    echo '</tr>';
    echo '</table><br><br>';


    echo '<table style="border-style: solid;border-width: 3px;"><tr>'
        .AddReportCell('<b>Run Scripts</b>') . '</tr>';

    echo '<tr>';
    echo AddReportCell('Run create-bills.php: ' . '<input name="run_createbill" type="checkbox">');
    echo '</tr><tr>';
    echo AddReportCell('Run auto-billing.php: ' . '<input name="run_autobill" type="checkbox">');
    echo '</tr></table>';

    echo '<input type="submit">';
    echo '</form>';
?>