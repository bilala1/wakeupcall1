<?php
ob_start("ob_gzhandler");
ini_set('session.gc-maxlifetime', 60 * 60 * 24 * 8); // session expires in 8 days
if(isset($_GET['PHPSESSID']) && $_GET['PHPSESSID']) {
    session_id($_GET['PHPSESSID']);
}
session_start();
//error_reporting(0);
error_reporting(E_ALL ^ E_NOTICE);

$loader = require_once 'vendor/autoload.php';
$loader->addPsr4(null, __DIR__);

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

define('DEPLOYMENT', getenv('WUC_DEPLOYMENT'));

//------------------------------------------------------------------------------
//-- Application Settings
//------------------------------------------------------------------------------
define('ACTIONS', 'actions');
define('VIEWS', 'views');
define('LIBS', 'library');
define('MODELS', 'models');
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_MONETARY, 'en_US.UTF-8');

//------------------------------------------------------------------------------
//-- Site Settings
//------------------------------------------------------------------------------

define('SITE_NAME', 'WAKEUP CALL');
define('SITE_HEAD', '');
define('MAINTENANCE_MODE', 'OFF');
//define('MAINTENANCE_MODE', 'ON');

define('HTTP_BASEURL', getenv('WUC_HTTP_BASEURL'));
define('HTTP_FULLURL', getenv('WUC_HTTP_FULLURL'));
define('HTTPS_BASEURL', getenv('WUC_HTTPS_BASEURL'));
define('HTTPS_FULLURL', getenv('WUC_HTTPS_FULLURL'));

define('SITE_PATH', getenv('WUC_SITE_PATH'));
define('SALES_EMAIL', getenv('WUC_SALES_EMAIL'));
define('SITE_EMAIL', getenv('WUC_SITE_EMAIL'));
define('AUTO_EMAIL', getenv('WUC_AUTO_EMAIL'));
define('CONCIERGE_EMAIL', getenv('WUC_CONCIERGE_EMAIL'));
define('MEMBERSHIP_EMAIL',getenv('WUC_MEMBERSHIP_EMAIL'));
define('MEMBERSHIPS_EMAIL',getenv('WUC_MEMBERSHIPS_EMAIL'));
define('CONTACT_EMAIL',getenv('WUC_CONTACT_EMAIL'));
define('ADMIN_EMAIL', getenv('WUC_ADMIN_EMAIL'));
define('MONITOR_EMAIL', getenv('WUC_MONITOR_EMAIL'));
define('FAX_EMAIL', getenv('WUC_FAX_EMAIL'));
define('ELAW_EMAIL', getenv('WUC_ELAW_EMAIL'));
define('SMTP_USERNAME', getenv('WUC_SMTP_USERNAME'));
define('SMTP_PASSWORD', getenv('WUC_SMTP_PASSWORD'));
define('SMTP_SERVER', getenv('WUC_SMTP_SERVER'));
define('MAILGUNAPIKEY', getenv('WUC_MAILGUNAPIKEY'));
define('MAILGUN_DOMAIN', getenv('WUC_MAILGUN_DOMAIN'));
define('DISABLE_EMAIL', getenv('WUC_DISABLE_EMAIL') == "true");

define('FILE_SYSTEM_PROVIDER', getenv('WUC_FILE_SYSTEM_PROVIDER'));
define('AWS_ACCESS_KEY_ID', getenv('WUC_AWS_ACCESS_KEY_ID'));
define('AWS_SECRET_ACCESS_KEY', getenv('WUC_AWS_SECRET_ACCESS_KEY'));
define('AWS_REGION', getenv('WUC_AWS_REGION'));
define('AWS_BUCKET', getenv('WUC_AWS_BUCKET'));

define('PER_PAGE', 20);  // Number of items to show per page
define('SITE_PHONE', '');
define('CHEMICAL_DIR', '/data/library/chemicals');
define('DOCUMENT_DIR', '/data/library/');
define('WEBINAR_DIR', '/data/webinars/');
define('FREE_MEMBER_PERIOD', 15); // period for free members is 15 days
define('BILLING_MEMBER_PERIOD', 1); // billing period in months
define('YEARLY_COST', 3000);
define('MONTHLY_COST', 200);
define('DELETE_ENTITY_DAYS',60); // duration in days after which deleted entity will be REALLY deleted
define('FORUM_FORMER_MEMBER',58);// an ID of a db record of a Former Member.

define('FORUM_MIMETYPES', serialize(array(
    'image/bmp',
    'image/jpeg',
    'image/pjpeg',
    'image/gif',
    'image/png',
    'image/x-png',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //-- .docx
    'text/plain',
    'application/rtf',
    'application/pdf',
    'application/x-pdf',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', //-- .xlsx
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation', //-- .pptx
    'application/x-stuffit',
    'application/x-tar',
    'application/zip',
    'audio/mpeg',
    'audio/x-wav')));
define('FORUM_EXTENSIONS', serialize(array(
    'bmp',
    'jpg',
    'jpeg',
    'gif',
    'png',
    'doc',
    'docx',
    'txt',
    'rtf',
    'pdf',
    'xls',
    'xlsx',
    'sit',
    'tar',
    'zip',
    'mp3',
    'wav')));
define('FORUM_UPLOAD_DIR', SITE_PATH . '/data/');
define('FORUM_MAX_FILE_SIZE', 10 * 1024 * 1024);

//------------------------------------------------------------------------------
//-- Database Settings
//------------------------------------------------------------------------------
define('DB_SERVER', getenv('WUC_DB_SERVER'));
define('DB_USER', getenv('WUC_DB_USER'));
define('DB_PASSWD', getenv('WUC_DB_PASSWD'));
define('DB_NAME', getenv('WUC_DB_NAME'));

define('DB_DEBUG', true);
define('MYSQL_DATE', '%M %d, %Y at %h:%i %p');
define('AES_KEY', 'hoodlewoodle6');

//------------------------------------------------------------------------------
//-- Ecommerce
//------------------------------------------------------------------------------
define('PAYMENT_ID', getenv('WUC_PAYMENT_ID'));
define('PAYMENT_KEY', getenv('WUC_PAYMENT_KEY'));

//------------------------------------------------------------------------------
//-- Date Formats
//------------------------------------------------------------------------------
define('DATE_FORMAT', 'm/d/Y');
define('DATE_FORMAT_SQL', '%m/%d/%Y');
define('DATE_FORMAT_FULL', 'm/d/Y h:ia');
define('DATE_FORMAT_TIME', 'g:i A');
define('DATE_FORMAT_FULL_SQL', '%m/%d/%Y %l:%i%p');

define('SENTRY_DSN', getenv('WUC_SENTRY_DSN'));
define('ENABLE_SENTRY', getenv('WUC_ENABLE_SENTRY' == 'true'));

if(ENABLE_SENTRY) {
    try {
        $ravenClient = new Raven_Client(SENTRY_DSN);
        $ravenClient->install();
    } catch (Exception $e) {
        //ignore
    }
}

//------------------------------------------------------------------------------
//-- Autoload
//------------------------------------------------------------------------------
include_once LIBS . '/framework.php';
require('./constants.php');

framework::startup();

//------------------------------------------------------------------------------
//-- Routes
//------------------------------------------------------------------------------
$routes = array();
$routes['admin/'] = array(
    'action' => 'admin/index.php'
);
$routes['admin'] = array(
    'action' => 'admin/index.php'
);
framework::routes($routes);

//------------------------------------------------------------------------------
//-- Run
//------------------------------------------------------------------------------
framework::shutdown();
?>
