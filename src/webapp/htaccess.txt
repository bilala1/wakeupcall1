<FilesMatch "^\.">
    Order allow,deny
   Deny from all
</FilesMatch>

<IfModule mod_deflate.c>
<FilesMatch "\.(js|css)$">
SetOutputFilter DEFLATE
</FilesMatch>
</IfModule>

RewriteEngine on
RewriteBase /

# RewriteRule ^(.*) http://208.66.56.61$1
# ProxyPassMatch ^(.*) http://208.66.56.61$1

#RewriteCond %{HTTP_HOST} ^wakeupcall.net$ [NC]
#RewriteRule ^(.*)$ http://www.wakeupcall.net/$1 [R=301,L]

# secures only full-membership.php
#RewriteCond %{https} off 
#RewriteRule ^full-membership.php https://www.wakeupcall.net/full-membership.php [R=301,QSA,L]
#RewriteCond %{https} on
#RewriteRule ^((?![full\-membership]).*).php$ http://www.wakeupcall.net/$1.php [R=301,QSA,L]

# secure every page
RewriteCond %{SERVER_PORT} 80 
RewriteRule ^(.*)$ https://www.wakeupcall.net/$1 [R,L]

# Post Tags
RewriteRule ^members/forums/tags/?$ members/forums/tags.php [QSA,E=FRAMEWORK:1,L]
RewriteRule ^members/forums/tag[s]?/(.*)/([0-9]+)/([0-9]+)?$ members/forums/tag.php?global_tags_id=$2&join_forums_id=$3 [QSA,E=FRAMEWORK:1,L]

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule $ bootstrap.php

RewriteRule ^$ index.php [R=301]
