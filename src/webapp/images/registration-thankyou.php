<? ini_set('display_errors','1');

include VIEWS . '/header.php'?>
<div id="content">
    <div id="leftcol" class="w230 left">
    	<img src="/images/contact.jpg" />
    </div>

    <!-- middlecol -->
    <div  class="w470 left" style="margin-left: 30px">
        <h1 class="headline">Thank you!</h1>
        <? if ($_REQUEST['renewal'] === 'updated-info'): //if person comes here from member-payment-failed.php page ?>
            <p>Thank you for renewal with WAKEUP CALL! An email has been sent to your email address <?= urldecode($_REQUEST['email']) ?>.  
            </p>        
        <? else: ?>
            <p>Thank you for registering with WAKEUP CALL! An email has been sent to your email address <?= urldecode($_REQUEST['email']) ?>.  <br>
                We hope that you enjoy your Free Trial.  Please let us know if you have any questions at <a href="mailto:concierge@wakeupcall.net">concierge@wakeupcall.net</a>.
            </p>
        <? endif; ?>
	</div>
    <br clear="all" />  
 </div>   

<? include VIEWS . '/footer.php' ?>
