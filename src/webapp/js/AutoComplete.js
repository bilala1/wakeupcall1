/*  Modified to be used with wakeupcall */

var AutoComplete = new Class({
    Implements: Options,
    options: {
        not_found: false,
        item_clicked: function(){}
    },
    initialize: function(elem, url, onSubmit){
        this.onSubmit = function () {}; // do nothing

        this.elem = $(elem);
        this.url = url;
        if(typeof onSubmit !== 'undefined') {
            this.onSubmit = onSubmit;
        }
        this.req = false;

        this.plugin = this.elem.clone();
        this.plugin.set('name', this.plugin.get('name') + '_autocomplete');
        this.plugin.set('autocomplete', 'off');
        this.elem.setStyle('display', 'none');
        this.plugin.inject(this.elem, 'after');

        this.plugin.store('autocomplete', this);
        this.plugin.addEvent('keyup', this.request.bind(this));
        this.plugin.addEvent('keydown', this.ignoreSubmit.bind(this));
        this.plugin.addEvent('blur', (function (evt) {
            (function () {
                this.kill();
            }).bind(this).delay(500);
        }).bind(this));
        this.plugin.form.autocomplete = 'off';
    },
    ignoreSubmit: function(evt) {
        if(evt.key == 'enter') {
            evt.stop();
            this.submit();
        }
    },
    submit: function() {
        var row = this.select();
        if (typeof row !== 'undefined') {
            var decode = new Element('div');
            var title = decode.set('html', row.title).get('text');
            var id = decode.set('html', row.id).get('text');

            this.plugin.set('value', title);
            this.elem.set('value', id);

            // Fix for value not being set .. wtf!?
            this.elem.setAttribute('value', id);
            this.plugin.setAttribute('value', title);

            this.elem.value = id;
            this.plugin.value = title;
            decode.destroy();
            this.onSubmit.call(this);
            this.elem.fireEvent('change', this.elem);
        }
        this.kill();
    },
    request: function(evt) {
        if(evt.key == 'esc' || this.plugin.value.length < 3) {
            this.kill();
            return;
        } else if (evt.key == 'down') {
            this.selectPrevious();
        }  else if (evt.key == 'up') {
            this.selectNext();
        } else if(evt.key == 'enter') {
            this.select();
            evt.stop();
        } else {
            if (this.req !== false) {
                this.req.cancel();
            }
            this.req = new Request.JSON({
                url: this.url,
                method:'post',
                onComplete: this.load
            });
            this.req.autocomplete = this;
            this.req.send('action=autocomplete&value=' + this.plugin.value);
        }
    },
    getSelection: function() {
        if(!this.container) {
            return;
        }
        var selection = this.container.getElements('.autocompletee');
        if(!selection.length) {
            return false;
        }
        selection = selection[0];
        return selection;
    },
    select: function(evt) {
        if (this.container !== false) {
            return this.getSelection().data;
        } else if(this.container === false) {
            //console.log(this);
        }
    },
    selectFirst: function() {
        if(this.container !== null) {
            this.container.getFirst().addClass('autocompletee');
        }
    },
    selectLast: function() {
        this.container.getLast().addClass('autocompletee');
    },
    selectPrevious: function() {
        if(!this.container) {
            return
        }
        var curr = this.container.getElements('.autocompletee');
        if(curr.length == 0) {
            this.selectFirst();
        } else {
            curr = curr[0];
            curr.removeClass('autocompletee');
            var next = curr.getNext();
            if(next) {
                next.addClass('autocompletee');
            } else {
                this.selectFirst();
            }
        }
    },
    selectNext: function() {
        if(!this.container) {
            return
        }
        var curr = this.container.getElements('.autocompletee');
        if(curr.length == 0) {
            this.selectFirst();
        } else {
            curr = curr[0];
            curr.removeClass('autocompletee');
            var previous = curr.getPrevious();
            if(previous) {
                previous.addClass('autocompletee');
            } else {
                this.selectLast();
            }
        }
    },
    load: function(sels) {
        var autocomplete = this.autocomplete;
        autocomplete.req = false;
        autocomplete.create();

        if(typeof(autocomplete.container) !== 'undefined') {
            autocomplete.container.empty();
        }

        if(sels && sels.length > 0) {

            for(var i=0; i<sels.length; i++) {
                var sel = sels[i];
                var markup = new RegExp(autocomplete.plugin.value, "gi");
                if(sel.title === null) {
                    continue;
                }
                markup = sel.title.replace(markup, '<strong>'+autocomplete.plugin.value+'</strong>');
                var div = new Element('div', {
                    'html': markup,
                    'events': {
                        'click': function(evt) {
                            autocomplete.submit.bind(autocomplete)();
                            $('locErr').show();
                        },
                        'mouseover': function(evt) {
                            this.getParent().getElements('div').each(function(item, index) {
                                item.removeClass('autocompletee');
                            });
                            this.addClass('autocompletee');
                        }
                    }
                });
                div.data = sel;
                div.inject(autocomplete.container);
            }
        }
        else {
            if(autocomplete.options.not_found !== false) {
                var div = new Element('div', {'html': autocomplete.options.not_found});
                div.inject(autocomplete.container);
            }
        }

    },
    create: function() {
        if(this.container) {
            return;
        }
        var pos = this.plugin.getPosition();
        var siz = this.plugin.getSize();
        var div = new Element('div', {
            'class': 'autocompleter'
        });
        div.setStyles({
            'top': pos.y+siz.y,
            'left': pos.x,
            'width': siz.x
        });
        div.inject(document.body);
        this.container = div;
    },
    kill: function() {
        if(!this.container) {
            return;
        }
        this.container.destroy();
        this.container = false;
    }
});
