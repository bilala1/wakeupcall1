// MooCal.js v1.3
// Author Robert Ross <robert@thirdoctave.com>

if(!Selectors.Pseudo.selected){
    Selectors.Pseudo.selected = function(){
	    return (this.selected && 'option' == this.get('tag'));
    };
}

var MooCal = new Class({
    Implements: [Options, Events],
    
    options: {
        format: 'mm/dd/yyyy',
        yearrange: [new Date().getFullYear() - 10, new Date().getFullYear() + 5],
        width: null,
        readonly: true,
        leaveempty: true,
        trigger: false,
        clickout: true
    },
    
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    curdate: {},
    elem: {},
    container: {},
    tbody: {},
    visible: false,
    shim: {},
    yearrange: {
        min: null,
        max: null
    },
    yearselect: {},
    
    initialize: function(elem, options){
        this.setOptions(options);
        
        this.yearrange.min = this.options.yearrange[0];
        this.yearrange.max = this.options.yearrange[1];
        
        if(Browser.Engine.name == 'trident') {
            this.container = new Element('div', {
                'class': 'cal-container',
                'styles': {
                    'display': 'none'
                }
            }).inject($(document.body));
        }
        else {
        this.container = new Element('div', {
            'class': 'cal-container',
            'styles': {
                'visibility': 'hidden',
                'display': 'none'
            }
        }).inject($(document.body));
        }
        
        this.shim = new IframeShim(this.container, {
            display: true
        });
        
        if(typeof elem == 'string'){
            elem = $(elem);
        }
        
        this.elem = elem;
        this.elem.set('autocomplete', false);
        this.elem.set('readonly', this.options.readonly);
        this.elem.store('MooCal', this);
        
        var datestring = elem.get('value');
        
        var date = new Date();
        
        if(datestring) {
            var parsestring = this.options.format;
            parsestring = parsestring.replace('mm', '%m');
            parsestring = parsestring.replace('dd', '%d');
            parsestring = parsestring.replace('yyyy', '%Y');
            
            Date.defineParser(parsestring);
            date.parse(datestring);
        }
        
        this.curdate = {
            'day': date.getDate(),
            'month': date.getMonth(),
            'year': date.getFullYear(),
            'dateobj': date
        };
        
        this.showDate = {
            'month': date.getMonth(),
            'year': date.getFullYear(),
            'dateobj': date
        };
        
        if(!this.options.leaveempty){
            this.formatDate();
        }
        
        this.container.setStyles({
            'position': 'absolute',
            'z-index': '105'
        });
        
        elem.addEvents({
            'keyup': (function (evt) {
                if(evt.key === 'esc') {
                    this.hideCal();
                }
            }).bind(this)
        });
        
        var trigger = (this.options.trigger === false) ? elem : this.options.trigger;
        trigger = $(trigger);
        trigger.addEvents({
            'click': (function(evt){
                this.visible = true;
                this.showCal();
                
                this.fireEvent('show', [this.curdate.day, this.curdate.month + 1, this.curdate.year]);
                if(evt) {
                    evt.stopPropagation();
                }
            }).bind(this)
        });
        
        if(this.options.clickout) {
            this.container.addEvent('clickout', (function() {
                this.hideCal();
            }).bind(this));
        }
    },

    positionContainer: function() {
        // Find the relative position if there is one.
        var parent = this.elem.getParent();
        var relative = $(document.body);
        /* this wasn't working...
         while(parent) {
         if(parent.getStyle('position') === 'relative') {
         relative = parent;
         break;
         }
         parent = parent.getParent();
         }*/

        var elempos = this.elem.getCoordinates(relative);

        this.container.setStyles({
            'left': Math.abs(elempos.left),
            'top': Math.abs(elempos.bottom)
            /*'width': (this.options.width) ? this.options.width : elempos.width,*/
        });
    },
    
    bodyClose: function (evt) {
        var elem = false;
        if(typeof evt.target != 'undefined') {
            elem = $(evt.target);
        }
        
        if(elem !== false && evt.target.hasChild(this.elem)) {
            this.hideCal();
        } else {
            return true;
        }
    },
    
    showCal: function(){
        // Build the calendar...
        var container = this.container;
        this.positionContainer();

        if(Browser.Engine.name == 'trident') {
            container.setStyle('display','block');
        }
        else {
            container.setStyles({
                display: 'block',
                visibility: 'visible'
            });
        }
        
        this.shim.show();
        this.container.empty();
        this.visible = true;
        
        var controls = new Element('div', {
            'class': 'cal-controls'
        }).inject(container);
        
        var monthselect = new Element('select', {'class': 'cal-select'}).inject(controls);
        this.months.each((function(month, idx){
            var option = new Element('option', {
                'html': month.substr(0, 3),
                'value': idx
            }).inject(monthselect);
            
            if(this.curdate.month == idx){
                option.set('selected', true);
            }
        }).bind(this));
        
        // Month select change event.
        monthselect.addEvent('change', (function(evt) {
            var eleme = evt.target;
            
            this.changeShow(eleme.get('value').toInt(), this.showDate.year);
        }).bind(this));
        
        var yearselect = new Element('select', {'class': 'cal-select'}).inject(controls);
        this.populateYears(yearselect);
        this.yearselect = yearselect;
        // Year select change event.
        yearselect.addEvent('change', (function(evt){
            var eleme = evt.target;
            
            this.changeShow(this.showDate.month, eleme.get('value').toInt());
        }).bind(this));
        
        // Create a back and next buttons for months...
        var prevnext = new Element('span', {
            'class': 'cal-prevnext'
        }).inject(controls);
        
        var prev = new Element('a', {
            'html': '&laquo;',
            'href': '#',
            'class': 'cal-prevnext-prev'
        }).inject(prevnext);
        
        var next = new Element('a', {
            'html': '&raquo;',
            'href': '#',
            'class': 'cal-prevnext-next'
        }).inject(prevnext);
        
        // Add Events to them:
        prev.addEvent('click', function(evt){
            var month, year;
            
            if(this.showDate.month == 0){
                month = 11;
                year = this.showDate.year - 1;
            }
            else {
                month = this.showDate.month - 1;
                year = this.showDate.year;
            }
            
            if(year < this.yearrange.min){
                this.yearrange.min = year;
            }
            
            monthselect.getElement('option:selected').set('selected', false);
            monthselect.getElements('option').each(function(opt, idx){
                if(idx == month){
                    opt.set('selected', 'selected');
                }
            });
            
            yearselect.getElement('option:selected').set('selected', false);
            this.populateYears(this.yearselect);
            yearselect.getElements('option').each(function(opt, idx){
                if(opt.get('value') == year){
                    opt.set('selected', 'selected');
                }
            });
            
            this.changeShow(month, year);
            
            evt.stopPropagation();
            return false;
        }.bind(this));
        
        next.addEvent('click', function(evt){
            var month, year;
            
            if(this.showDate.month == 11){
                month = 0;
                year = this.showDate.year + 1;
            }
            else {
                month = this.showDate.month + 1;
                year = this.showDate.year;
            }
            
            if(year > this.yearrange.max){
                this.yearrange.max = year;
            }
            
            monthselect.getElement('option:selected').set('selected', false);
            monthselect.getElements('option').each(function(opt, idx){
                if(idx == month){
                    opt.set('selected', 'selected');
                }
            });
            
            yearselect.getElement('option:selected').set('selected', false);
            this.populateYears(this.yearselect);
            yearselect.getElements('option').each(function(opt, idx){
                if(opt.get('value') == year){
                    opt.set('selected', 'selected');
                }
            });
            
            this.changeShow(month, year);
            
            evt.stopPropagation();
            return false;
        }.bind(this));
        
        // Close the window...
        var close = new Element('a', {
            'href': '',
            'html': '&times;',
            'styles': {
                'float': 'right'
            },
            'class': 'cal-closelink'
        }).inject(controls);
        close.addEvent('click', (function(e){
            e.stop();
            this.hideCal();
        }).bind(this));
        
        new Element('br', {
            'clear': 'right'
        }).inject(controls);
        
        var table   = new Element('table', {
            'width': '100%'
        }).inject(container);
        
        var tbody   = new Element('tbody').inject(table);
        this.tbody = tbody;
        
        var daysRow = new Element('tr').inject(tbody);
        
        this.days.each(function(day, idx) {
            var td = new Element('td', {
                'class': 'cal-dayrow',
                'html': day.substring(0, 1),
                'width': '14%'
            });
            td.inject(daysRow);
        });
        
        this.buildCal(tbody);
        
        container.inject($(document.body));
        
        this.shim.position();
            
        
        return true;     
    },
    
    hideCal: function() {
        if(this.onClose() === false) {
            return;
        }
    
        this.container.empty();
        this.shim.dispose();
        
        if(Browser.Engine.name == 'trident') {
            this.container.setStyles({
                'display': 'none'
            });
        }
        else {    
            this.container.setStyles({
                'display': 'none',
                'visibility': 'none'
            });
        }
    },
    
    buildCal: function(tbody){
        tbody.getElements('.cal-weekrow').each(function(elem, idx){
            elem.destroy();
        });
        
        var lastday = this.showDate.dateobj.get('lastdayofmonth');
        var firstday = this.showDate.dateobj.setDate(1);
        firstday = new Date(firstday).getDay();
        
        var totaldays = lastday + firstday;
        var weeks = Math.ceil(totaldays / 7);
        var active = false;
        var realday = 1;
        
        for(var i = 0; i < weeks; i++){
            var weekrow = new Element('tr', {
                'class': 'cal-weekrow'
            }).inject(tbody);            
            
            for(var d = 0; d < 7; d++){
                if(i === 0 && d == firstday){
                    active = true;
                }
                
                var td = new Element('td', {
                    'class': (active) ? 'cal-active' : 'cal-inactive',
                    'align': 'center',
                    'html': (active) ? realday : ''
                }).inject(weekrow);
                
                if(active){
                    if(realday == this.curdate.day && this.showDate.month == this.curdate.month && this.showDate.year == this.curdate.year){
                        td.addClass('cal-selected');
                    }
                    
                    realday = realday + 1;
                    td.addEvents({
                        'click': (function(evt){
                            var eleme = evt.target;
                            
                            this.changeDate(eleme.get('text').toInt(), this.showDate.month, this.showDate.year);
                            
                            this.fireEvent('change', [this.curdate.day, this.curdate.month + 1, this.curdate.year, this]);
                            this.formatDate();
                        }).bind(this),
                        
                        'mouseenter': function(){
                            this.addClass('cal-activehover');
                        },
                        
                        'mouseleave': function(){
                            this.removeClass('cal-activehover');
                        }
                    });
                }
                if(realday > lastday){
                    active = false;
                }
            }
        }
    },
    
    changeDate: function(day, month, year){
        var date = this.curdate.dateobj;
        this.curdate.day   = day.toInt();
        this.curdate.month = month.toInt();
        this.curdate.year  = year.toInt();
        
        date.setMonth(month.toInt());
        date.setFullYear(year.toInt());
        date.setDate(day.toInt());
        
        this.curdate.dateobj = date;
        
        this.buildCal(this.tbody);
    },
    
    changeShow: function(month, year){
        var date = this.showDate.dateobj;
        this.showDate.month = month;
        this.showDate.year  = year;
        
        date.setMonth(month.toInt());
        date.setFullYear(year.toInt());
        
        this.showDate.dateobj = date;
        
        this.buildCal(this.tbody);
    },
    
    formatDate: function(){
        var day = this.curdate.day;
        var month = this.curdate.month;
        var year = this.curdate.year;
        
        var format = this.options.format;
        
        format = format.replace('dd', day);
        format = format.replace('mm', month + 1);
        format = format.replace('yyyy', year);
        
        this.elem.set('value', format);
    },
    
    setCalDate: function(day, month, year){
        this.changeDate(day, monoth, year);
        this.formatDate();
    },
    
    onChange: function(day, month, year, calobj){
        // Change event
    },
    
    onShow: function(day, month, year){
        // Show event
    },
    
    onClose: function () {
        return true;
    },
    
    populateYears: function(yearselect){
        yearselect.empty();
        for(var y = this.yearrange.min; y <= this.yearrange.max; y++){
            var option = new Element('option', {
                'html': y,
                'value': y
            }).inject(yearselect);
            
            if(this.curdate.year == y){
                option.set('selected', true);
            }
        }
    }
        
});
