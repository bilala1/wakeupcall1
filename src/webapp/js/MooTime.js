// MooTime v0.1
// Author Robert Ross <robert@thirdoctave.com>

var MooTime = new Class({
    Implements: [Options, Events],
    options: {
        format: 'h:m a',
        multiplier: 5
    },
    
    elem: {},
    container: {},
    curtime: {
        hour: 0,
        minute: 0,
        ampm: false
    },
    
    selected: {
        hour: false,
        minute: false,
        ampm: false
    },
    
    shim: {},
    
    initialize: function(elem, options){
        this.setOptions(options);
        
        if($type(elem) == 'string'){
            this.elem = $(elem);
        }
        else {
            this.elem = elem;
        }
        
        // Change some attributes...
        this.elem.set('readonly', true);
        this.elem.set('autocomplete', false);
        
        this.elem.addEvent('click', function(evt){
            this.showTime();
            evt.stopPropagation();
        }.bind(this));
        
        this.container = new Element('div', {
            'class': 'time-container',
            'styles': {
                'z-index': 105,
                'display': 'none'
            }
        }).inject($(document.body));
        this.buildPicker();
        
        this.container.addEvent('clickout', function(){
            this.hideTime();
        }.bind(this));
        
        
        this.shim = new IframeShim(this.container, {
            display: false
        });
    },
    
    showTime: function(){
        var coords = this.elem.getCoordinates();
        
        this.container.setStyles({
            position: 'absolute',
            left: coords.left,
            top: coords.bottom,
            'z-index': 100,
            'display': 'block'
        });
        
        this.shim.position();
        this.shim.show();
    },
    
    buildPicker: function(){
        var table = new Element('table');
        var tbody = new Element('tbody');
        
        table.inject(this.container);
        tbody.inject(table);
        
        var hoursrow = new Element('tr');
        var minutesrow = new Element('tr');
        var ampmrow = new Element('tr');
        
        hoursrow.inject(tbody);
        var hourscol = new Element('td', {
            'class': 'time-hourscol',
            'html': 'Hour'
        }).inject(hoursrow);
        var hourpick = new Element('td').inject(hoursrow);
        
        minutesrow.inject(tbody);
        var minutescol = new Element('td', {
            'class': 'time-minutescol',
            'html': 'Minute'
        }).inject(minutesrow);
        var minutepick = new Element('td').inject(minutesrow);
        
        ampmrow.inject(tbody);
        var ampmcol = new Element('td', {
            'class': 'time-ampmcol',
            'html': 'AM / PM'
        }).inject(ampmrow);
        var ampmpick = new Element('td').inject(ampmrow);
        
        for(var h = 0; h < 12; h++){
            new Element('div', {
                'html': (h + 1),
                'class': 'time-hour'
            }).inject(hourpick);
        }
        
        for(var m = 0; m < 60; m++){
            var realmin = m * this.options.multiplier;
            if(realmin >= 60){
                break;
            }
            
            new Element('div', {
                'html': (realmin < 10) ? '0' + realmin : realmin,
                'class': 'time-minute'
            }).inject(minutepick);
        }
        
        // AM / PM inject
        var am = new Element('div', {
            html: 'AM',
            'class': 'time-ampm'
        }).inject(ampmpick);
        
        var pm = new Element('div', {
            html: 'PM',
            'class': 'time-ampm'
        }).inject(ampmpick);
        
        $$('.time-hour', '.time-minute', '.time-ampm').addEvents({
            'mouseenter': function(){
                this.addClass('time-select-hover');
            },
            'mouseleave': function(){
                this.removeClass('time-select-hover');
            }
        });
        
        this.container.getElements('.time-ampm').addEvent('click', function(evt){
            var target = evt.target;
            target.getParent('td').getElements('div.time-select-active').each(function(elem, idx){
                elem.removeClass('time-select-active');
            });
            
            this.selected.ampm = target.get('text');
            target.addClass('time-select-active');
            
            this.checkSelected();
        }.bind(this));
        
        this.container.getElements('.time-hour').addEvent('click', function(evt){
            var target = evt.target;
            target.getParent('td').getElements('div.time-select-active').each(function(elem, idx){
                elem.removeClass('time-select-active');
            });
            this.selected.hour = target.get('text');
            target.addClass('time-select-active');
            
            this.checkSelected();
        }.bind(this));
        
        this.container.getElements('.time-minute').addEvent('click', function(evt){
            var target = evt.target;
            target.getParent('td').getElements('div.time-select-active').each(function(elem, idx){
                elem.removeClass('time-select-active');
            });
            
            target.addClass('time-select-active');
            var minute = target.get('text');
            this.selected.minute = minute;
            
            this.checkSelected();
        }.bind(this));
    },
        
    checkSelected: function(){
        if(this.selected.minute && this.selected.hour && this.selected.ampm){
            var string = this.options.format;
            string = string.replace('m', this.selected.minute);
            string = string.replace('h', this.selected.hour);
            string = string.replace('a', this.selected.ampm);
            
            this.elem.set('value', string);
            
            this.selected = {
                hour: false,
                minute: false,
                ampm: false
            };
            this.container.getElements('div.time-select-active').each(function(elem, idx){
                elem.removeClass('time-select-active');
            });
            this.hideTime();
        }
    },
    
    hideTime: function(){
        this.container.setStyle('display', 'none');
        this.shim.hide();
    }
});
