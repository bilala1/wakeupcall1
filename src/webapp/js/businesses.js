jQuery(function() {
    var $ = jQuery;
    $('body').on('click', '.notes_popup', function(e) {
        e.preventDefault();
        var parts = e.currentTarget.id.split('_');

        var entities_id = parts[1];
        var entities_items_id = parts[2];

        var iframeUrl = BASEURL + '/members/entities/items/notes/notes_popup.php?entities_id='+entities_id+'&entities_items_id=' + entities_items_id;

        openIframeDialog(iframeUrl, 'Notes');
    });
});

window.addEvent('domready', function () {
    $('save').addEvent('click', function (e) {
        var form = $('editbusiness');
        if (chkBusinessForm(form)) {
            form.submit();
        } else {
            e.preventDefault();
        }
    });

    $$('.history').addEvent('click', function () {
            var parts = this.get('id').split('_');

            var id = parts[1];
            openDialog(BASEURL + '/members/entities/items/history.php?entities_items_id=' + id,450,300);
            return false;
            
        });
    
    $$('.files').addEvent('click', function () {
        var parts = this.get('id').split('_');

        var entities_id = parts[1];
        var entities_items_id = parts[2];
        openDialog(BASEURL + '/members/entities/items/files.php?entities_id='+entities_id+'&entities_items_id=' + entities_items_id,450,300);
        return false;
    });
    //check_location_change();
     jQuery('#licensed_locations input:checkbox').bind('change', check_location_change);
});

function check_location_change(e){
   if(this.checked==false && form_mode == 'edit'){
       jQuery('#location_change_message').show();
   }
}
function show_note() {
    var notebox = document.getElementById("notebox");
    notebox.show();
}

function createErrorMessage(input, message) {
    var el = new Element('p');

    input = (typeof(input) === 'string') ? $(input) : input;

    el.set('class', 'form-error');
    el.set('html', message);
    el.inject(input, 'after');
}

function validateRequired(input, message) {
    var value;

    input = (typeof(input) === 'string') ? $(input) : input;
    value = !!input.get('value');

    if (!value) {
        createErrorMessage(input, message);
    }
    return value;
}

function anySelected(inputName) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    return value;
}

function chkBusinessForm(form) {
    var formIsValid = true;

    $$('p.form-error').destroy();

    formIsValid &= validateRequired('entities_name', 'Please enter a name');

    if (!formIsValid) {
        scrollToFirstError()
    }

    return formIsValid;
}

window.addEvent('load', function () {
    $$('.datepicker').each(function (item) {
        new MooCal($(item), {
            clickout: true,
            yearrange: [new Date().getFullYear() - 3, new Date().getFullYear() + 5],
            readonly: false
        });
    });

    var fileCount = 0;

    $$('.deleteRow').addEvent('click', deleteRow);
});

function deleteRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function delete_notes(e) {
    if (confirm("Are you sure? You want to delete this?")) {
        jQuery(e).parents('tr').remove();
    }
}