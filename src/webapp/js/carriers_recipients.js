window.addEvent('domready', function(){
    $('coverage_other').addEvent('click', function(){
        if(this.checked == true){
            $('carriers_coverages_other').set('disabled', '').focus();
        }
        else {
            $('carriers_coverages_other').set('disabled', 'disabled');
        }
    })
});

window.addEvent('load', function(){
    new MooCal($('carriers_effective_start_date'), {
        leaveempty: true,
        clickout: true
    }).addEvent('change', showCallout);

    new MooCal($('carriers_effective_end_date'), {
        leaveempty: true,
        clickout: true
    }).addEvent('change', showCallout);

    RecipientHideHandler();
});

$('carriers_phone').addEvent('keyup',function(event){
    if(event.event.keyCode != '8' && event.keyCode != '46') {
        formatPhone(this);
    }
});
$('carriers_fax').addEvent('keyup',function(event){
    if(event.event.keyCode != '8' && event.keyCode != '46') {
        formatPhone(this);
    }
});

function formatPhone(input) {
    if(input.value.length == 1 && input.value != '(') {
        input.value = '(' + input.value;
    }
    if(input.value.length == 4) {
        input.value = input.value + ') ';
    }
    if(input.value.length == 5) {
        var spare = input.value.substr(input.value.length - 1);
        if(spare != ')') {
            input.value = input.value.substr(0,input.value.length - 1) + ') ' + spare;
        }
        else {
            input.value = input.value + ' ';
        }
    }
    if(input.value.length == 6) {
        var spare = input.value.substr(input.value.length - 1);
        if(spare != ' ') {
            input.value = input.value.substr(0,input.value.length -1) + ' ' + spare;
        }
    }
    if(input.value.length == 10) {
        var spare = input.value.substr(input.value.length-1);
        if(spare != '-') {
            input.value = input.value.substr(0,input.value.length - 1) + '-' + spare;
        }
    }
    if(input.value.length > 14) {
        input.value = input.value.substr(0,input.value.length - 1);
    }
}

    function RecipientHideHandler()
    {
        var carrierTable = document.getElementById("carrier_location_table");
        var recipientTable = document.getElementById("recipient_location_table");
        var recipientElements = document.getElementById("Recipient_Elements");
        var recipientOnlyElements = document.getElementById("Recipient_Only_Elements");
        var carrierElements = document.getElementById("Carrier_Elements");
        var saveBtn = document.getElementById("save_button");
        var saveBtnAdd = document.getElementById("save_button_add");
        var selectBox = document.getElementById("carriers_recipient");
        var value = selectBox.get('value');

        if(value == '1')
        {
            saveBtn && saveBtn.show();
            saveBtnAdd && saveBtnAdd.show();
            recipientElements.show();
            recipientOnlyElements.show();
            carrierElements.hide();
            
            if(carrierTable != null)
                carrierTable.hide();
            
            if(recipientTable != null)
                recipientTable.show();
        }
        else if(value == '0')
        {
            saveBtn && saveBtn.show();
            saveBtnAdd && saveBtnAdd.show();
            recipientElements.show();
            recipientOnlyElements.hide();
            carrierElements.show();
            
            if(carrierTable != null)
                carrierTable.show();
            
            if(recipientTable != null)
                recipientTable.hide();
        }
        else
        {
            saveBtn && saveBtn.hide();
            saveBtnAdd && saveBtnAdd.hide();
            recipientElements.hide();
            recipientOnlyElements.hide();
            carrierElements.hide();
            
            if(carrierTable != null)
                carrierTable.hide();
            
            if(recipientTable != null)
                recipientTable.hide();
        }
    }

    function init_all_checkbox(all_check_id, child_check_id)
    { 
        checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');
        
        for(var i=0, n=checkboxes.length; i < n; i++)
        {
            if(document.getElementById(all_check_id) && document.getElementById(all_check_id).checked)
            {
                checkboxes[i].checked = true;
            }

            checkboxes[i].addEvent('click', function(){
                if(document.getElementById(all_check_id) && document.getElementById(all_check_id).checked)
                {
                    document.getElementById(all_check_id).checked = false;
                    if(all_check_id == 'checkbox_all_locations'){
                        document.querySelector('input[id^=checkbox_receive_all_save]').checked = false;
                        document.querySelector('input[id^=checkbox_receive_all_save]').style.visibility = "hidden";
                        document.querySelector('input[id^=checkbox_receive_all_submission]').checked = false;
                        document.querySelector('input[id^=checkbox_receive_all_submission]').style.visibility = "hidden";
                        document.querySelector('input[id^=checkbox_receive_all').checked = false;
                        document.querySelector('input[id^=checkbox_receive_all').style.visibility = "hidden";
                    }
                    
                }

                return true;
            });
        } 
        if(document.getElementById(all_check_id) ){
            document.getElementById(all_check_id).addEvent('click', function(){
                checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');

                for(var i=0, n=checkboxes.length; i < n; i++)
                {
                    checkboxes[i].checked = document.getElementById(all_check_id).checked;
                }
                if(all_check_id == 'checkbox_receive_all_submission' || all_check_id == 'checkbox_receive_all_save' ){
                    document.querySelector('input[id^=checkbox_receive_all').checked = false;
                    checkboxes = document.querySelectorAll('input[id^=recipient_send_all_check3]');

                    for(var i=0, n=checkboxes.length; i < n; i++)
                    {
                        checkboxes[i].checked = false;
                    }
                }
                if(all_check_id == 'checkbox_receive_all'){
                    document.querySelector('input[id^=checkbox_receive_all_save').checked = false;
                    document.querySelector('input[id^=checkbox_receive_all_submission').checked = false;
                    checkboxes = document.querySelectorAll('input[id^=recipient_send_all_check1]');

                    for(var i=0, n=checkboxes.length; i < n; i++)
                    {
                        checkboxes[i].checked = false;
                    }
                    checkboxes = document.querySelectorAll('input[id^=recipient_send_all_check2]');

                    for(var i=0, n=checkboxes.length; i < n; i++)
                    {
                        checkboxes[i].checked = false;
                    }
                }

                return true;
            });
        }
    }
    function check_location_on_selection(recieve_all_checkboxes,replace_name){
        for(var i=0, n=recieve_all_checkboxes.length; i < n; i++)
        {
            recieve_all_checkboxes[i].addEvent('click', function(e){
                e=e||window.event;   

                var name = e.target.name.replace(replace_name, 'recipient_');

               if(e.target.checked == true) 
                    document.querySelector('input[name^="'+ name +'"]').checked = true;
            });
        }
    }
    function uncheck_options_on_deselection(location_checkboxes,replace_name){
        for(var i=0, n=location_checkboxes.length; i < n; i++)
        {
            location_checkboxes[i].addEvent('click', function(e){
                e=e||window.event;   

                var name = e.target.name.replace('recipient_', replace_name);

               if(e.target.checked == false)
                    document.querySelector('input[name^="'+ name +'"]').checked = false;
            });
        }
    }

    function init_recipient_checkbox(all_check_id, child_check_id, location_all_id, location_child_id,checkbox_receive_all_submission,checkbox_receive_all)
    { 
        if( document.getElementById(location_all_id) && !document.querySelector('input[id^="'+ location_all_id +'"]').checked)
        {
            document.querySelector('input[id^="'+ all_check_id +'"]').style.visibility = "hidden";
            document.querySelector('input[id^="'+ checkbox_receive_all_submission +'"]').style.visibility = "hidden";
            document.querySelector('input[id^="'+ checkbox_receive_all +'"]').style.visibility = "hidden";
        }

        receive_all_onupdate_checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');
        check_location_on_selection(receive_all_onupdate_checkboxes,'recipient_send_all_on_save_');
         
        receive_all_onsubmission_checkboxes = document.querySelectorAll('input[id^=recipient_send_all_check2]');
        check_location_on_selection(receive_all_onsubmission_checkboxes,'recipient_send_all_on_submission_');
        
        recieve_all_checkboxes= document.querySelectorAll('input[id^=recipient_send_all_check3]');
        check_location_on_selection(recieve_all_checkboxes,'recipient_send_all_');
        
        location_checkboxes =  document.querySelectorAll('input[id^="'+ location_child_id +'"]');
        
        uncheck_options_on_deselection(location_checkboxes,'recipient_send_all_on_save_');
        uncheck_options_on_deselection(location_checkboxes,'recipient_send_all_on_submission_');
        uncheck_options_on_deselection(location_checkboxes,'recipient_send_all_');
        
        if(document.getElementById(location_all_id)){
            document.querySelector('input[id^="'+ location_all_id +'"]').addEvent('click', function(e)
            {
                if(document.querySelector('input[id^="'+ location_all_id +'"]').checked)
                {
                    document.querySelector('input[id^="'+ all_check_id +'"]').style.visibility = "visible";
                    document.querySelector('input[id^="'+ checkbox_receive_all_submission +'"]').style.visibility = "visible";

                    document.querySelector('input[id^="'+ checkbox_receive_all +'"]').style.visibility = "visible";
                }
                else
                {
                    document.querySelector('input[id^="'+ all_check_id +'"]').checked = false;
                    document.querySelector('input[id^="'+ all_check_id +'"]').style.visibility = "hidden";
                    
                    document.querySelector('input[id^="'+ checkbox_receive_all_submission +'"]').checked = false;
                    document.querySelector('input[id^="'+ checkbox_receive_all_submission +'"]').style.visibility = "hidden";
                    
                    document.querySelector('input[id^="'+ checkbox_receive_all +'"]').checked = false;
                    document.querySelector('input[id^="'+ checkbox_receive_all +'"]').style.visibility = "hidden";

                    checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');

                    for(var i=0, n=checkboxes.length; i < n; i++)
                    {
                        checkboxes[i].checked = false;
                    }
                }
            }); 
        }

    }

    function init_can_edit_controls(all_check_id, child_check_id, can_edit_id, location_name_prefix, edit_name_prefix)
    {   
        if(document.getElementById(all_check_id) ){
            document.getElementById(all_check_id).addEvent('click', function(){
                checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');

                if(!document.getElementById(all_check_id).checked)
                {
                    checkboxes = document.querySelectorAll('input[id^="'+ can_edit_id +'"]');

                    for(var i=0, n=checkboxes.length; i < n; i++)
                    {
                        checkboxes[i].checked = false;
                    }
                }
                return true;
            });  
        }
        can_edit_checkboxes =  document.querySelectorAll('input[id^="'+ can_edit_id +'"]');
        location_checkboxes =  document.querySelectorAll('input[id^="'+ child_check_id +'"]');

        for(var i=0, n=can_edit_checkboxes.length; i < n; i++)
        {
            
                can_edit_checkboxes[i].addEvent('click', function(e){
                e=e||window.event;   

                var name = e.target.name.replace(edit_name_prefix, location_name_prefix);
             
                if(e.target.checked == true) 
                    document.querySelector('input[name^="'+ name +'"]').checked = true;
            });
        }

        for(var i=0, n=location_checkboxes.length; i < n; i++)
        {
            location_checkboxes[i].addEvent('click', function(e){
                e=e||window.event;   

                var name = e.target.name.replace(location_name_prefix, edit_name_prefix);

               if(e.target.checked == false)
                    document.querySelector('input[name^="'+ name +'"]').checked = false;
            });
        }
    }
    function toggleCheckboxSelection(e){
        var name = jQuery(e).attr('name');
        var matches = name.split('_');
        if(matches.length == 6){
            var ids = matches[5];
        }else{
            var ids = matches[3];
        }
        if((name == 'recipient_send_all_on_save_'+ ids && document.querySelector('input[name^="recipient_send_all_on_save_'+ ids +'"]').checked == true) || 
           (name == 'recipient_send_all_on_submission_'+ ids && document.querySelector('input[name^="recipient_send_all_on_submission_'+ ids +'"]').checked == true )){
       
            document.querySelector('input[name^="recipient_send_all_'+ ids +'"]').checked = false;
        }
        if(name == 'recipient_send_all_'+ ids && document.querySelector('input[name^="recipient_send_all_'+ ids +'"]').checked == true){
            document.querySelector('input[name^="recipient_send_all_on_save_'+ ids +'"]').checked = false;
            document.querySelector('input[name^="recipient_send_all_on_submission_'+ ids +'"]').checked = false;
        }
        

    }

    window.onload = function(){
        init_all_checkbox('checkbox_all_locations', 'recipient_location_check');
        init_all_checkbox('checkbox_receive_all_save', 'recipient_send_all_check1');
        init_all_checkbox('checkbox_receive_all_submission', 'recipient_send_all_check2');
        init_all_checkbox('checkbox_receive_all', 'recipient_send_all_check3');
        init_can_edit_controls('checkbox_all_locations', 'recipient_location_check', 'recipient_location_can_edit_', 'recipient_', 'recipient_location_can_edit_')
        init_recipient_checkbox('checkbox_receive_all_save', 'recipient_send_all_check1','checkbox_all_locations', 'recipient_location_check','checkbox_receive_all_submission','checkbox_receive_all');
        init_all_checkbox('check_all_box_carrier', 'carrier_location_check');
        init_can_edit_controls('check_all_box_carrier', 'carrier_location_check', 'carrier_location_can_edit_', 'carrier_', 'carrier_location_can_edit_')
};
function chkCarrierForm() {
    var isValid = true;
    if (document.getElementById('carriers_recipient').value === '0') {
        isValid &= validatePolicy();
    }
    return isValid;
}

function validatePolicy() {
    var $ = jQuery;
    var isValid = true;

    isValid &= window.WUC.validateRequired('carriers_effective_start_date', "Start date is required");
    isValid &= window.WUC.validateRequired('carriers_effective_end_date', "End date is required");

    if(isValid) {
        var startDate = Date.parse($('#carriers_effective_start_date').val()),
            endDate = Date.parse($('#carriers_effective_end_date').val());

        if(isNaN(startDate)) {
            window.WUC.createErrorMessage('carriers_effective_start_date', "Start date is not a valid date");
            isValid = false;
        } else if(isNaN(endDate)) {
            window.WUC.createErrorMessage('carriers_effective_end_date', "End date is not a valid date");
            isValid = false;
        } else if(startDate >= endDate) {
            window.WUC.createErrorMessage('carriers_effective_end_date', "End date must be after start date");
            isValid = false;
        }
    }
    if($(".carrier_location").length>0){
        if($(".carrier_location:visible:checked").length<1){
            window.WUC.createErrorMessage('carrier_location_table', "Please select at least one location");
            isValid = false;
        }
    }

    return isValid;
}

var $saveButton = $('save_button');
if($saveButton) {
    $saveButton.addEvent('click', function (e) {
        this.disabled = true;
        $$('p.form-error').destroy();
        var form = $('editcarrier');
        if (!chkCarrierForm(form)) {
            this.disabled = false;
            e.preventDefault();
        } else {
            form.submit();
        }
    });
}

function showCallout() {
    jQuery('#renewWarning').slideDown();
}
