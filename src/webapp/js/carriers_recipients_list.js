window.addEvent('domready', function () {
   $$('.history').addEvent('click', function () {
       var parts = this.get('id').split('_');

       var id = parts[1];
       openDialog(BASEURL + '/members/claims/carriers/history.php?carriers_id=' + id,450,300);
       return false;
   });

    window.addEvent('load', function()
    {
        //calendar pickers
        new MooCal($('carriers_start_time'), {
            leaveempty: false,
            clickout: true
        });

        new MooCal($('carriers_end_time'), {
            leaveempty: false,
            clickout: true
        });
    });
    
    var cookieName = 'carrierstab';
    var tabCookie = Cookie.read(cookieName);
    if (!tabCookie ) { tabCookie = 'Carriers'};
    var tab = $$('#' + tabCookie);
    tab.show();
    $$('.tab-title[data-report-type=' + tabCookie + ']').addClass('activetab');

    $$('.tab-title').addEvent('click',function() {
        $$('.tab-title').removeClass('activetab');
        $$('.tab-content').hide();
        var tab = this.get('data-report-type');
        Cookie.write(cookieName, tab);
        this.addClass('activetab');
        $$('#' + tab).show();
    });

});
