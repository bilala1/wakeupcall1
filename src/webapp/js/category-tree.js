var CatTree = new Class({
    input: {},
    
    initialize: function(input, categories_id, changeFnc){
        var next = input.getNext('select.cattree');
        if(next){
            next.fireEvent('die');
        }
        
        this.input = input;
        
        var select = new Element('select', {
            'class': 'cattree'
        });
        
        new Element('option', {
            'value': '',
            'text': 'None'
        }).inject(select);
        
        var parent = this;
        
        new Request.JSON({
            url: BASEURL + '/ajax-library-categories.php',
            method: 'get',
            onSuccess: function(json){
                json.each(function(option, idx){
                    var option = new Element('option', {
                        'value': option.library_categories_id,
                        'text': option.library_categories_name
                    }).inject(select);
                    
                }.bind(this));
                
                parent.addChangeEvents(select, changeFnc);
                
            }.bind(this)
        }).get({
            library_categories_id: categories_id
        });
    },
    
    addChangeEvents: function(select, changeFnc){
        // Don't inject it if there's no sub categories
        if(select.getChildren().length > 1){ 
            select.inject(this.input, 'after');
            
            select.addEvent('change', function(){
                if(this.get('value') == ''){
                    var next = this.getNext('select.cattree');
                    if(next){
                        next.fireEvent('die');
                    }
                    return false;
                }
                changeFnc(this.get('value'));
                
                new CatTree(this, this.get('value'), changeFnc); // Recursive
            });
            
            select.addEvent('die', function(){
                var next = this.getNext('select.cattree');
                if(next){
                    next.fireEvent('die');
                }
                
                this.dispose();
            });
        }
        else {
            select.dispose();
        } 
    }
});
