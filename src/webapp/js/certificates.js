
function setReqCertChkBox(){
    var box = $('request_cert'),
        file = $('certificates_file'),
        fileSelected = file.get('value');

    if(fileSelected) {
        box.set('checked', '');
    }
}

window.addEvent('domready', function () {
    $('save').addEvent('click', function (e) {
        this.disabled = true;
        var form = $('editCertificate');
        if (chkCertificateForm(form)) {
            form.submit();
        } else {
            this.disabled = false;
            e.preventDefault();
        }
    });

    updateLocationAdminEmail();

    jQuery('#previewEmail').bind('click', previewEmail);
    jQuery('#customizeEmail').bind('click', customizeEmail);
    jQuery('#deleteCustomEmail').bind('click', deleteCustomEmail);
    jQuery('#collapseCustomEmail').bind('click', collapseCustomEmail);
    jQuery('#join_vendors_id').bind('change',UpdateVendorInSummary );
    jQuery('#certificates_email_days').bind('change', function (e) {
       jQuery('#summary_expiration_days').html(jQuery('#certificates_email_days').val());
    });

    var $addlEntityTemplate = $('addlInsuredTemplate');
    $addlEntityTemplate.set('id', '').remove();
    $('AddLegalEntityName').addEvent('click', function (e) {
        e.stop();
        var $clone = $addlEntityTemplate.clone(),
            $fieldset = $($$('#legal_entity fieldset')[0]),
            childCount = $fieldset.children.length;
        $fieldset.adopt($clone);
        $clone.getElements('input[type=radio]').set('name', 'associate_with[' + childCount + ']');

        new Fx.Reveal($clone).reveal();
    });

    $(document.body).addEvent('change:relay(select.j-entitySelect)', onEntityNameSelectChange);

    $(document.body).addEvent('click:relay(a.j-removeAddlEntity)', onRemoveEntityName);
    $$('.coverage_checkbox').addEvent('change', function(e){ 
        var checked_id = this.id;
        var amt_id = checked_id + '_amount';
        var $amt_textbox = $(amt_id)
        var $amt_textbox2 = $('coverages_amount2');
        var $amt_textbox3 =  $('coverages_amount3');
        var disabled = $(checked_id).get('checked') ? '' : 'disabled';
        $amt_textbox.set('disabled', disabled);
        if(checked_id == "coverage_comp"){
            $amt_textbox2.set('disabled', disabled);
            $amt_textbox3.set('disabled', disabled);
        }
    });
});

function onEntityNameSelectChange() {
    var $select = $(this),
        $add = $select.getSiblings('.j-newAddlEntity');

    if($select.get('value') == 'add') {
        new Fx.Reveal($select, {mode: 'horizontal', display: 'inline-block'}).dissolve();
        $add.each(function ($el) {
            new Fx.Reveal($el, {mode: 'horizontal', display: 'inline-block'}).reveal();
        });
    }
}

function onRemoveEntityName(){
    var $link = $(this),
        $container = $link.getParent('.j-addlInsuredLine');

    new Fx.Reveal($container).addEvent('complete', function() {
        $container.remove();
    }).dissolve();
}

function updateLocationAdminEmail(){
    var location_id = jQuery('#join_licensed_locations_id').val();

    var url = '/API/users_with_location_permission.php?licensed_locations_id=' + location_id+'&permission=certificates';
    jQuery.ajax({url: url, success: function(result){
       if(result) {
            var users1 = '';
            var users2 = '';
            var col = 1;
            jQuery.each(result,function(key,value){
                var check_expiring = "";
                var check_expired = "";
                if(jQuery.inArray(parseInt(value.id), remind_expiring_members) != -1) {
                   check_expiring = "checked=checked";
                }
                if(jQuery.inArray(parseInt(value.id), remind_expired_members) != -1) {
                   check_expired = "checked=checked";
                }
                if(col==1){
                    users1 += '<div>';
                }
                users1 +='<label class="Dynlabel"><span><input id="certificate_remind_members_before_expire_'+value.id+'" class="input-checkbox" type="checkbox" name="certificate_remind_members_before_expire[]" value="'+value.id+'" '+check_expiring+'></span>&nbsp;'+value.lastName +', '+value.firstName +' ('+value.permission+')' +"</label>";
                users2 +='<label class="Dynlabel"><span><input id="certificate_remind_members_after_expire_'+value.id+'" class="input-checkbox" type="checkbox" name="certificate_remind_members_after_expire[]" value="'+value.id+'" '+check_expired+'></span>&nbsp;'+value.lastName +', '+value.firstName +' ('+value.permission+')'  +"</label>";
                col = col+1;
                if(col == length){
                   users1 += '<label class="clearfix"></label></div>';

                }
                
            });
            
            jQuery('#remind_before_expiry').html(users1);
            jQuery('#remind_after_expiry').html(users2);
            
        }
    }
    });
}
function UpdateVendorInSummary(){
 var vendor = jQuery('option:selected', jQuery(this)).text(); 
 jQuery('#summary_vendor').html(vendor);
}
function anySelected(inputName) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    return value;
}
validateRequired = window.WUC.validateRequired ;
createErrorMessage = window.WUC.createErrorMessage;

function chkCertificateForm(form) {
    var certificateIsValid = true;
    var isUpdate = !$('coverage_other');

    $$('p.form-error').destroy();

    var $vendor = $('join_vendors_id');
    if($vendor) {
        certificateIsValid &= validateRequired($vendor, 'Please select a vendor');
    }

    if(!isUpdate) {
        var amt = $$('.coverage_amt').get('value');
        var notvalid = false;
        amt.forEach(function(item) {
           if (item !='' && isNaN(parseFloat(item))) {
            certificateIsValid = false;
            notvalid = true;
           }
        })
        if(notvalid){
            createErrorMessage($('coverage_other_amount').getParent('.form-field'), 'Please enter numaric values');
        }
        
        if ($('certificates_include_additional_insured_request').checked) {
            $$('.j-addlInsuredLine').forEach(function(item) {
                if(item.id == 'addlInsuredTemplate') {
                    return;
                }
                var $line = $(item),
                    $select = $($line.getElements('select')[0]),
                    selectedValue = $select.get('value');
                if(!selectedValue || selectedValue == "0") {
                    certificateIsValid = false;
                    createErrorMessage($line, 'Please select an additionally insured legal entity');
                } else if(selectedValue == 'add') {
                    var $text = $($line.getElements('.j-entityName')[0]),
                        $radios = $line.getElements('input[type=radio]'),
                        $checkedRadios = $line.getElements('input[type=radio]:checked');
                    if(!$text.get('value')) {
                        certificateIsValid = false;
                        createErrorMessage($line, 'Please enter the new legal entity name');
                    }
                    if($radios.length && !$checkedRadios.length) {
                        certificateIsValid = false;
                        createErrorMessage($line, 'Please select if this legal entity name is for the account or the location');
                    }
                }
            });
        }
        if (!anySelected('coverages[]') && !$('certificates_coverage_other').get('value')) {
            certificateIsValid = false;
            createErrorMessage($('coverage_other').getParent(), 'Please select at least one coverage type');
        }
        if (!anySelected('certificates_remind_vendor')
            && !anySelected("certificate_remind_members_before_expire[]")
            && !anySelected('certificates_remind_other_chk')) {
            certificateIsValid = false;
            createErrorMessage($('certificates_remind_other_chk').getParent('.form-field'), 'Please select someone to receive the email');
        }

        if (anySelected('certificates_remind_other_chk')) {
            certificateIsValid &= validateRequired('certificates_remind_other', 'Please enter an email address');
        }

        certificateIsValid &= validateRequired('certificates_email_subject', 'Please enter a subject line');

        if (anySelected('certificates_request_cert_to_email_chk')) {
            certificateIsValid &= validateRequired('certificates_request_cert_to_email', 'Please enter an email address');
        }

        if (anySelected('certificates_remind_member')) {
            if (!anySelected('certificates_remind_vendor_expired')
                && !anySelected('certificates_remind_other_expired_chk')
                && !anySelected("certificate_remind_members_after_expire[]")) {
                certificateIsValid = false;
                createErrorMessage($('certificates_remind_other_expired_chk').getParent('.form-field'), 'Please select someone to receive the email');
            }

            if (anySelected('certificates_remind_other_expired_chk')) {
                certificateIsValid &= validateRequired('certificates_remind_other_expired', 'Please enter an email address');
            }
        }
        $$('.j-file').forEach(function(item) {
            certificateIsValid &= validate_filetype($(item));              
        });
    }

    if(!certificateIsValid){
        scrollToFirstError()
    }
    return certificateIsValid;
}

function customizeEmail() {
    var join_certificates_email_templates_id = jQuery('#join_certificates_email_templates_id').val();
    if(join_certificates_email_templates_id == '(Custom Template)'){
       if(tinyMCE.activeEditor) {
            tinyMCE.activeEditor.setContent(jQuery('#email_html').val());
       };
       new Fx.Reveal($('email_template')).reveal();
       $('customizeEmail').hide();
    }else{
        var url = '/API/certificates/customize_email_template.php?join_certificates_email_templates_id=' + join_certificates_email_templates_id;
        jQuery.ajax({
            url: url,
            success: function (result) {
                jQuery('#customize_template').val(1);
                jQuery('#email_html').val(result);

                if(tinyMCE.activeEditor) {
                    tinyMCE.activeEditor.setContent(result);
                }

                new Fx.Reveal($('email_template')).reveal();
                $('customizeEmail').hide();
                jQuery('#join_certificates_email_templates_id')
                    .attr('disabled', 'disabled')
                    .append('<option selected>(Custom Template)</option>');
            }
        });
    }
}

function previewEmail(e) {
    var form = jQuery("#editCertificate");
    jQuery('#email_html').val(tinyMCE.get('email_html').getContent()); //workarround to get tinymce content in ajaxpost
    jQuery.ajax('/members/my-documents/certificates/email_templates/preview_certificate_email_popup.php',
        {
            type: "POST",
            data: form.serializeArray()
        })
        .done(function (respose) {
            new Element('div', {html: respose, 'class': 'MooDialog myDialog'}).MooDialog(
                {
                    size: {
                        width: 800,
                        height: 500
                    }
                });

            return false;
        });
}

function deleteCustomEmail() {
    var certificatesId = jQuery('#certificates_id').val();
    new Fx.Reveal($('email_template')).dissolve();
    jQuery('#customize_template').val('0');
    jQuery('#customizeEmail').show();
    jQuery('#join_certificates_email_templates_id').removeAttr('disabled').val(jQuery('#default_template_id').val());
    jQuery('option:contains("(Custom Template)")').remove();
}

function collapseCustomEmail() {
    //var certificatesId = jQuery('#certificates_id').val();
    jQuery('#email_html').val(tinyMCE.get('email_html').getContent());
    new Fx.Reveal($('email_template')).dissolve();
    jQuery('#customizeEmail').show();
}

window.addEvent('load', function () {
    updateLocationAdminEmail();
    var isUpdate = !$('coverage_other');

    $$('.datepicker').each(function (item) {
        new MooCal($(item), {
            clickout: true,
            yearrange: [new Date().getFullYear() - 3, new Date().getFullYear() + 5],
            readonly:false
        }).addEvent('change', function () {
            if(item.id == 'certificates_expire'){
                setTimeout(function(){ 
                    jQuery('#summary_expiry').html(jQuery('#certificates_expire').val()); 
                }, 1000);
            }
        });
    });

    var fileCount = 0;
    //Files
    $('add_file').addEvent('click', function (e) {
        e.stop();
        var newRow = '<td></td>' +
            '<td><input type="file" name="certificates_file[]" class="j-file"></td>' +
            '<td></td>' +
            '<td><a href="#">Delete</a></td>';
        newRow = new Element('tr', {html: newRow});
        newRow.inject($('uploaded-files'));
        newRow.getElement('a').addEvent('click', deleteRow);
        ++fileCount;
    });

    $('certificates_type').addEvent('change', function(){
        $('special_project_name').setStyle('display', ($('certificates_type').get('value') == 'special' ? 'block' : 'none'));
    }).fireEvent('change');

    $('certificates_request_cert_to_email_chk').addEvent('change', function(){
        $('certificates_request_cert_to_email').set('disabled', $('certificates_request_cert_to_email_chk').get('checked') ? '' : 'disabled');
    }).fireEvent('change');

    if(!isUpdate) {

        $('certificates_include_additional_insured_request').addEvent('change', function () {
            var disabled = $('certificates_include_additional_insured_request').get('checked') ? '' : 'disabled';

            $$(document.getElementsByName('join_legal_entity_names_id[]')).set('disabled', disabled);
            $$(document.getElementsByName('legal_entity_names_name[]')).set('disabled', disabled);
            $$(document.getElementsByName('associate_with[]')).set('disabled', disabled);
            $$('j-removeAddlEntity').set('disabled', disabled);
            $('AddLegalEntityName').set('disabled', disabled);
        }).fireEvent('change');
        
        $('coverage_other').addEvent('change', function () {
            $('certificates_coverage_other').set('disabled', $('coverage_other').get('checked') ? '' : 'disabled');
        }).fireEvent('change');
        
        $('certificates_remind_other_chk').addEvent('change', function () {
            $('certificates_remind_other').set('disabled', $('certificates_remind_other_chk').get('checked') ? '' : 'disabled');
        }).fireEvent('change');

        $('certificates_remind_other_expired_chk').addEvent('change', function () {
            $('certificates_remind_other_expired').set('disabled', $('certificates_remind_other_expired_chk').get('checked') ? '' : 'disabled');
        }).fireEvent('change');

        $('certificates_remind_member').addEvent('change', function () {
            var disabled = (0 == $('certificates_remind_member').get('value'));
            [
                'certificates_remind_other_expired_chk',
                'certificates_remind_vendor_expired'
            ].forEach(function (id) {
                if($(id))
                    $(id).set('disabled', disabled ? 'disabled' : '');
            });

            disabled = disabled || !$('certificates_remind_other_expired_chk').get('checked');
            $('certificates_remind_other_expired').set('disabled', disabled ? 'disabled' : '');
        }).fireEvent('change');
    }
});

function deleteRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function PreviewTemplate($email_id)
{
    openDialog('emails_history/preview_popup.php?id=' + $email_id, 800, 500);
    return false;
}