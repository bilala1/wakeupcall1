window.addEvent('domready', function(){
    $$('.history').addEvent('click', function(){
        var parts = this.get('id').split('_');
        
        var id = parts[1];
        openDialog(BASEURL + '/members/my-documents/certificates/history.php?certificates_id='+id,450,300);
        
        return false;
    });

    $$('.emailHistory').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];
        
        openDialog('emails_history/certificate_history.php?certificates_id='+id,800,400);
  
        return false;
    });

    $$('.files').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog('/members/my-documents/certificates/files.php?certificates_id='+id,450,300);

        return false;
    });
    
     jQuery('#clear_button').bind('click', clear_filter);

});
function clear_filter(){ 
        jQuery('#clear_filter').val("clear_filter");
    }
function PreviewTemplate($email_id)
{
    openDialog('emails_history/preview_popup.php?id=' + $email_id,800,500);
    return false;
}

window.addEvent('load', function(){
        new MooCal($('certificates_start_time'), {
            leaveempty: true,
            readonly:false,
            clickout: true
        });

        new MooCal($('certificates_end_time'), {
            leaveempty: true,
            readonly:false,
            clickout: true
        });
    //infobox settings
        var webinarsRequest = new Request({
        url: BASEURL +'/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_certificates');
        });

    });