
function updateCarrierSelection(event, selectedValue) {
    var llId = $('licensed_locations_id').get('value'),
        carrierSelect = document.getElementById("join_carriers_id");

    //Can't change licensed location now, so we don't need to populate this dynamically. Consolidated code to load carriers into the action
    // if(!carrierSelect) {
    //     return;
    // }
    // removeOptions(carrierSelect);
    // var url = '/API/carriers_for_location.php?is_recipients_only=false&licensed_locations_id=' + llId;
    // new Request.JSON( {
    //     url: url,
    //     onSuccess: function(carriers) {
    //         if (carriers.length == 0) {
    //             new Element('option',{'html':'No carriers for this location', value: ""}).inject($('join_carriers_id'));
    //         } else {
    //             new Element('option',{'html':'Please Select', 'value':''}).inject($('join_carriers_id'));
    //         }
    //         Object.each(carriers,function(value){
    //             var coverages = [];
    //             if(value.carriers_coverages_list) {
    //                 coverages = coverages.concat(value.carriers_coverages_list.split(','));
    //             }
    //             if(value.carriers_coverages_other) {
    //                 coverages.push(value.carriers_coverages_other);
    //             }
    //             var text = value.carriers_name + ' (' + coverages.join(', ') + ')';
    //             if(value.carriers_effective_end_date) {
    //                 text += ' Expires: ' + value.carriers_effective_end_date.split(' ')[0];
    //             }
    //             new Element('option',{'value':value['carriers_id'],'html':text}).inject($('join_carriers_id'));
    //            // new Element('option',{'value':value['carriers_id'],'html':text}).inject($('join_carriers_id1'));
    //         });
    //         if (selectedValue) {
    //             jQuery('#join_carriers_id').val(selectedValue);
    //         }
    //         jQuery("#join_carriers_id").trigger("change");
    //     }
    // }).get();

    removeOptions(document.getElementById("send_copy_carriers_ids"));
    var url = '/API/carriers_for_location.php?is_recipients_only=true&licensed_locations_id=' + llId;
    new Request.JSON( {
        url: url,
        onSuccess: function(carriers) {
            if (carriers.length == 0) {
                new Element('option',{'html':'No recipients for this location', 'value':''}).inject($('send_copy_carriers_ids'));
            } else {
                new Element('option',{'html':'-none-', 'value':''}).inject($('send_copy_carriers_ids'));
            }
            Object.each(carriers,function(value){
                new Element('option',{'value':value['carriers_id'],'html':value['carriers_name']}).inject($('send_copy_carriers_ids'));
            });
            jQuery("#send_copy_carriers_ids").trigger("change");
        }
    }).get();

}

function verifyDelete(event) {
    var target = jQuery(event.target),
        jcar = target.closest('.j-carrier'),
        fieldid = jcar.attr('id');
    event.preventDefault();
    var $el = $(event.target);
    var submit_type = jQuery(this).data('submittype');
    var carrier_name = jQuery(this).data('carrier_name');

    if (submit_type != 'SUBMIT_OPEN' && submit_type != 'SUBMIT_CLOSE') {
        var message = 'Are you sure you want to delete this?';
        new MooDialog.Confirm(message, function () {
                if (fieldid != 'carrierDetails') {
                    $(fieldid).remove();
                    //remove the submission notice info when we remove the carrier
                    removeUnusedCarrierSubmissions();
                }
            },
            function () {
            },
            {scroll: false}
        );
    } else {
        verifyDeleteCarriers(submit_type, carrier_name, fieldid);
    }
}

function verifyDeleteCarriers(submit_type, carrier_name, fieldid) {
    var dialog = new MooDialog({
            closeButton: false,
            size: {
                width: 550,
                height: 200
            }
        }),
        $template = jQuery('#delete-with-certs').children().clone();

    $template.find('.j-cancel').click(function () {
        dialog.close();
    });

    $template.find('.j-delete').click(function () {
        if (fieldid != 'carrierDetails') $(fieldid).remove();
        //remove the submission notice info when we remove the carrier
        removeUnusedCarrierSubmissions();
        jQuery('#submit_message ').append(jQuery('<div > Carrier <b>' + carrier_name + '</b> will be removed from this claim.</div>'));
        dialog.close();
    });

    dialog.toElement().addClass('deleteWithCerts');
    dialog.setContent($template[0]);
    dialog.open();
}
jQuery(function() {
    var $ = jQuery;
    $('body').on('click', '.notes', function(e) {
        e.preventDefault();
        var parts = e.currentTarget.id.split('_');
        var id = parts[1];
        var iframeUrl = BASEURL + '/members/claims/notes/view.php?claims_notes_id=' + id+'&back=no';
        openIframeDialog(iframeUrl, 'Notes');
    });
});

window.addEvent('domready', function () {
    toggleSubmissionEmail();
    disableClosedDate();
    if(jQuery('#claim_workers_comp_employee_name')){
        $('claim_workers_comp_employee_name').addEvent('change',function(){
            jQuery('#summary_claims_employee').html(jQuery('#claim_workers_comp_employee_name').val());
        })
    }
    if(jQuery('#claim_general_liab_claimant')){
        $('claim_general_liab_claimant').addEvent('change',function(){
            jQuery('#summary_claims_employee').html(jQuery('#claim_general_liab_claimant').val());
        })
    }
    if(jQuery('#claim_auto_claimant')){
        $('claim_auto_claimant').addEvent('change',function(){
            jQuery('#summary_claims_employee').html(jQuery('#claim_auto_claimant').val());
        })
    }
    if(jQuery('#claim_employment_claimant')){
        $('claim_employment_claimant').addEvent('change',function(){
            jQuery('#summary_claims_employee').html(jQuery('#claim_employment_claimant').val());
        })
    }
    
    jQuery(function () {
        $$('body').addEvent('click:relay(".j-confirmDelete")',  verifyDelete);
    });
    
    $('licensed_locations_id').addEvent('change', updateCarrierSelection);
    // when we return from a post error we might have already selected the carrier
    // pass the value of this carrier to the updateCarrierSelection so after it
    // gets the list of carriers for the location the selection will remain
    updateCarrierSelection(null, jQuery('#selected_carrier_id').attr("value"));


document.getElementById('osha_include_help').addEvent('click', function () {
        openDialog('/members/claims/osha_include_help.php', 900, 850);
        return false;
    });
    
document.getElementById('osha_privacy_help').addEvent('click', function () {
    
    openDialog('/members/claims/osha_privacy_help.php',450,300);
        return false;
    });

    document.getElementById('work_comp_class_G').addEvent('change', function () {
            classificationChanged('work_comp_class_G');
        });

    document.getElementById('work_comp_class_H').addEvent('change', function () {
            classificationChanged('work_comp_class_H');
        });

    document.getElementById('work_comp_class_I').addEvent('change', function () {
            classificationChanged('work_comp_class_I');
        });

    document.getElementById('work_comp_class_J').addEvent('change', function () {
            classificationChanged('work_comp_class_J');
        });

    var checkedItem = $$('input[name=claim_workers_comp_classification[]]:checked')[0];
    if (checkedItem) {
        classificationChanged(checkedItem.get('id'));
    } else {
        classificationChanged();
    }
        
    var saveOnly = $('saveOnly'),
        form = $('editClaim');
    if(saveOnly){

        saveOnly.addEvent('click', function () {
           this.disabled = true; 
            if (chkClaimForm(form, false)) {
               if( validateCarrier(form,false)){
           var checkedParts = $$('.showOnSelect:checked');
                    if(checkedParts.length > 0){
                       anyChecked = 1; 
                       $('submit_to_carrier').set('value', 'Yes');
                    }else{
                       anyChecked = 0;
                       $('submit_to_carrier').set('value', 'No');
                    }
                    form.submit();
               }else{
                    this.disabled = false; 
                }
            }else{
                this.disabled = false; 
            }
        });
    
    }
   
    jQuery('#previewEmail').bind('click', previewEmail);
    jQuery('#customizeEmail').bind('click', customizeEmail);
    jQuery('#deleteCustomEmail').bind('click', deleteCustomEmail);
    jQuery('#collapseCustomEmail').bind('click', collapseCustomEmail);
    
     var noteCount =jQuery('.notebox').length
    //Notes
    $('another_note').addEvent('click', function (e) {
        e.stop();
        var newRow = '<td>'+
            '<input id="claims_notes_subject_'+noteCount+'" class="input-text show_on_edit" name="claims_notes_subject['+noteCount+']">'+
            '<input type="hidden" id="claims_hidden_subject_'+noteCount+'">'+
            '<span id="claims_notes_subject_text_'+noteCount+'" class="hide_on_edit"></span></td>' +
            '<td><span id="claims_notes_note_text_'+noteCount+'" class="hide_on_edit"></span>'+
            '<input type="hidden" id="claims_hidden_notes_'+noteCount+'">'+
            '<textarea id="claims_notes_note_'+noteCount+'" class="show_on_edit" cols="50" rows="5" name="claims_notes_note['+noteCount+']"></textarea></td>' +
            '<td>&nbsp;</td>' +
            '<td><div class="actionMenu"><ul>'+
            '<li><a href="#"  id="donenotes_'+noteCount+'_new" class="show_on_edit donenotes">Done</a></li>'+
            '<li><a href="#"  id="cancelnotes_'+noteCount+'_new" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>'+
            '<li><a href="#"  id="editnotes_'+noteCount+'" class="hide_on_edit edit_notes" style="display:none;">Edit</a></li>'+
            '<li><a href="#"  id="deletenotes_'+noteCount+'" class="delete show_on_edit">Cancel</a></li>'+
            '</ul></div></td>';
        newRow = new Element('<tr>', {html: newRow});
        newRow.setAttribute('class','notebox')
        newRow.inject($('notebox_container'));
        newRow.getElement('a.delete').addEvent('click', deleteRow);
        ++noteCount;
    });
    
    
    
   $('osha_popup').addEvent('click', function (e) {
       openDialog('/members/claims/osha_flowchart.php', 900, 850);
       return false;
    });
    
    $$('body').addEvent('click:relay(.cancelnotes)', function (e) {
        e.stop();
        var parts = this.get('id').split('_');
        var id = parts[1];
        jQuery('#claims_notes_subject_text_'+id).html(jQuery('#claims_hidden_subject_'+id).val())
        jQuery('#claims_notes_subject_'+id).val(jQuery('#claims_hidden_subject_'+id).val())
        jQuery('#claims_notes_subject_text_'+id).show();
        jQuery('#claims_notes_subject_'+id).hide();
        jQuery('#claims_notes_note_text_'+id).html(jQuery('#claims_hidden_note_'+id).val())
        jQuery('#claims_notes_note_'+id).val(jQuery('#claims_hidden_notes_'+id).val())
        jQuery('#claims_notes_note_text_'+id).show();
        jQuery('#claims_notes_note_'+id).hide();
        jQuery(this).parents('ul').find('.hide_on_edit').show();
        if(jQuery('#donenotes_'+id)) jQuery('#donenotes_'+id).hide();
        if(jQuery('#donenotes_'+id+'_new')) jQuery('#donenotes_'+id+'_new').hide();
        if('#cancelnotes_'+id) jQuery('#cancelnotes_'+id).hide();
        if(jQuery('#cancelnotes_'+id+'_new'))jQuery('#cancelnotes_'+id+'_new').hide();
        return ;
    });
    $$('body').addEvent('click:relay(.donenotes)', function (e) {
        e.stop();
        var parts = this.get('id').split('_');
        var id = parts[1];
        jQuery('#claims_notes_subject_text_'+id).html($('claims_notes_subject_'+id).get('value'))
        jQuery('#claims_hidden_subject_'+id).val($('claims_notes_subject_'+id).get('value'));
        jQuery('#claims_notes_subject_text_'+id).show();
        jQuery('#claims_notes_subject_'+id).hide();
        jQuery('#claims_notes_note_text_'+id).html($('claims_notes_note_'+id).get('value'))
        jQuery('#claims_hidden_notes_'+id).val($('claims_notes_note_'+id).get('value')) 
        jQuery('#claims_notes_note_text_'+id).show();
        jQuery('#claims_notes_note_'+id).hide();
        jQuery(this).parents('ul').find('.hide_on_edit').show();
        if('#donenotes_'+id) jQuery('#donenotes_'+id).hide();
        if('#donenotes_'+id+'_new') jQuery('#donenotes_'+id+'_new').hide();
        if('#cancelnotes_'+id) jQuery('#cancelnotes_'+id).hide();
        if('#cancelnotes_'+id+'_new') jQuery('#cancelnotes_'+id+'_new').hide();
        jQuery('#deletenotes_'+id).hide();
        if(parts[2] && parts[2] == 'new'){
            //changing label from cancel to delete
            jQuery('#deletenotes_'+id).replaceWith( '<a href="#"  id="deletenotes_'+id+'" class="delete show_on_edit">Delete</a>');
        }
        jQuery('#deletenotes_'+id).show();
        return ;
    });
    
    $$('body').addEvent('click:relay(.edit_notes)', function (e) {
        e.stop();
        jQuery(this).parents('tr').find('.show_on_edit').show();
        jQuery(this).parents('tr').find('.hide_on_edit').hide();
    });
    $$('body').addEvent('click:relay(.delete)', deleteRow);
       window.WUC.ensureUncollapsed(jQuery('#carrierDetails1 .j-toggle-content'));
});

$('add_carrier').addEvent('click', function (e) {
    e.stop();
    var $carrierInfoTemplate = $('carrier_info_template'),
        $clone = $carrierInfoTemplate.getElement('.j-carrier').clone();
    var carrierCount = $$('.j-carrier').length;
    carrierCount = carrierCount + 1;

    $clone.set('id', 'carrierDetails' + carrierCount);
    //clear the values while cloning
    $clone.getElements('.input-text').set('value', '');
    $clone.getElements('select').set('value', '');

    $clone.inject($carrierInfoTemplate, 'before');
    new Fx.Reveal($clone).reveal();

    jQuery($clone).find('.j-toggle-collapse-initial .j-toggle-trigger').click();
});

function customizeEmail() {
    var join_claims_email_templates_id = jQuery('#join_claims_email_templates_id').val();
    if(join_claims_email_templates_id == '(Custom Template)'){
       if(tinyMCE.activeEditor) {
            tinyMCE.activeEditor.setContent(jQuery('#email_html').val());
       };
       new Fx.Reveal($('email_template')).reveal();
       $('customizeEmail').hide();
    }else{
        var url = '/API/claims/customize_email_template.php?join_claims_email_templates_id=' + join_claims_email_templates_id;
        jQuery.ajax({
            url: url,
            success: function (result) {
                jQuery('#customize_template').val(1);
                jQuery('#email_html').val(result);

                if(tinyMCE.activeEditor) {
                    tinyMCE.activeEditor.setContent(result);
                }

                new Fx.Reveal($('email_template')).reveal();
                $('customizeEmail').hide();
                jQuery('#join_claims_email_templates_id')
                    .attr('disabled', 'disabled')
                    .append('<option selected>(Custom Template)</option>');
                }
        });
    }
}

function previewEmail(e) {
    var form = jQuery("#editClaim");
    jQuery('#email_html').val(tinyMCE.get('email_html').getContent()); //workarround to get tinymce content in ajaxpost
    jQuery.ajax('/members/claims/email_templates/preview_claims_email_popup.php',
        {
            type: "POST",
            data: form.serializeArray()
        })
        .done(function (respose) {
            new Element('div', {html: respose, 'class': 'MooDialog myDialog'}).MooDialog(
                {
                    size: {
                        width: 800,
                        height: 500
                    }
                });

            return false;
        });
}

function deleteCustomEmail() {
    var certificatesId = jQuery('#claims_id').val();
    new Fx.Reveal($('email_template')).dissolve();
    jQuery('#customize_template').val('0');
    jQuery('#customizeEmail').show();
    jQuery('#join_claims_email_templates_id').removeAttr('disabled').val(jQuery('#default_template_id').val());
    jQuery('option:contains("(Custom Template)")').remove();
}

function collapseCustomEmail() {
    //var certificatesId = jQuery('#claims_id').val();
    jQuery('#email_html').val(tinyMCE.get('email_html').getContent());
    new Fx.Reveal($('email_template')).dissolve();
    jQuery('#customizeEmail').show();
}

function validateRequiredMulti(inputName, errInput, message) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    if (!value) {
        window.WUC.createErrorMessage(errInput, message);
    }
    return value;
}

function validateWorkersCompClassification()
{
    var valid = validateRequiredMulti('claim_workers_comp_classification[]', 'claim_workers_comp_classification', 'Please select the case classification');

    if(!valid)
    {
        return false;
    }
    
    return true;
}

function validateOptionalEndAfterStart(startField, endField) {
    startField = (typeof(startField) === 'string') ? $(startField) : startField;
    endField = (typeof(endField) === 'string') ? $(endField) : endField;

    var start = moment(startField.get('value')),
        end = moment(endField.get('value')),
        fieldsValid = true;

    if (startField.get('value') && !start.isValid()) {
        window.WUC.createErrorMessage(startField, 'Start date is not a valid format');
        fieldsValid = false;
    }

    if (endField.get('value') && !end.isValid()) {
        window.WUC.createErrorMessage(endField, 'End date is not a valid format');
        fieldsValid = false;
    }

    if (start.isValid && end.isValid()) {
        if (end.diff(start, 'days') <= 0) {
            window.WUC.createErrorMessage(endField, 'End date must be after start date');
            fieldsValid = false;
        }
    }
    return fieldsValid;
}

function chkClaimForm(form, submittingToCarrier) {
    var claimIsValid = true,
        $typeError = $('type_error');

    $$('p.form-error').destroy();
    $typeError.setStyle('display', 'none');

    claimIsValid &= window.WUC.validateRequired('claims_datetime', 'Please enter the date of the incident');

    var checkedParts = $$('.showOnSelect:checked');
    if(checkedParts.length <= 0){
        claimIsValid &= window.WUC.validateRequired('join_claims_status_codes_id', 'Please select a status for the claim');
    }

    var checked_type = $$('input[name=claims_type]:checked'),
        type_checked = checked_type.length > 0;

    if (!type_checked) {
        $typeError.setStyle('display', 'block');
        claimIsValid = false;
    } else {
        switch (checked_type[0].get('value')) {
            case 'Workers Comp':
                claimIsValid &= checkWorkersCompForm();
                break;
            case 'Other':
                claimIsValid &= checkOtherClaim();
                break;
            case 'General Liability / Guest Property':
                claimIsValid &= checkGeneralLiabilityClaim();
                break;
            case 'Auto':
                claimIsValid &= checkAutoClaim();
                break;
            case 'Your Property':
                claimIsValid &= checkPropertyClaim();
                break;
            case 'Labor Law':
                claimIsValid &= checkLaborClaim();
                break;
        }
    }

    var locationInput = $('licensed_locations_id');
    if(locationInput) {
        claimIsValid &= window.WUC.validateRequired(locationInput, 'Please select a location');
    }

    updateAmountFields();

    claimIsValid &= window.WUC.validateRequired('claims_email_subject', 'Please enter a subject line');
    claimIsValid &=validateCarrier(form);
    $$('.j-file').forEach(function(item) {
        claimIsValid &= validate_filetype($(item));
    });
    if(!claimIsValid){
        scrollToFirstError()
    }
    return claimIsValid;
}
function checkLaborClaim(){
    var claimIsValid = true;
    claimIsValid &= window.WUC.validateRequired('claim_employment_claimant', 'Please enter the employee\'s name');
    claimIsValid &= window.WUC.validateRequired('claim_employment_job_title', 'Please enter the employee\'s job title');
    claimIsValid &= window.WUC.validateRequired('claim_employment_department', 'Please select the employee\'s department');
    claimIsValid &= window.WUC.validateRequired('claim_employment_incident_location', 'Please enter the incident location');
    return claimIsValid;
}
function checkWorkersCompForm() {
    var claimIsValid = true;

    
  
    claimIsValid &= window.WUC.validateRequired('claim_workers_comp_employee_name', 'Please enter the employee\'s name');

    claimIsValid &= window.WUC.validateRequired('claim_workers_comp_job_title', 'Please enter the employee\'s job title');

    claimIsValid &= window.WUC.validateRequired('claim_workers_comp_employee_department', 'Please select the employee\'s department');

    claimIsValid &= window.WUC.validateRequired('claim_workers_comp_event_department', 'Please select the department where the event occurred');

    claimIsValid &= window.WUC.validateRequired('claim_workers_comp_event_type', 'Please select the event type');

    var eventType = $('claim_workers_comp_event_type').get('value');

    if (eventType === 'Injury') {
        claimIsValid &= validateRequiredMulti('claim_workers_comp_body_parts[]', 'body_part', 'Please select at least one body part');
    } else if (eventType === 'Illness') {
        claimIsValid &= validateRequiredMulti('claim_workers_comp_illness_type', 'ill_type', 'Please select the type of illness');
    }

    claimIsValid &= validateWorkersCompClassification();

    return claimIsValid;
}

function checkOtherClaim() {
    var claimIsValid = true;

    claimIsValid &= window.WUC.validateRequired('claim_general_liab_claimant', 'Please enter the name of the claimant');

    claimIsValid &= validateRequiredMulti('claim_general_liab_loss_area[]', 'area_loss', 'Please select at least one area of loss');

    return claimIsValid;
}

function checkGeneralLiabilityClaim() {
    var claimIsValid = true;

    claimIsValid &= window.WUC.validateRequired('claim_general_liab_claimant', 'Please enter the name of the claimant');

    claimIsValid &= validateRequiredMulti('claim_general_liab_loss_area[]', 'area_loss', 'Please select at least one area of loss');

    claimIsValid &= validateRequiredMulti('claim_general_liab_loss_type', 'claim_general_liab_loss_type_other', 'Please select the type of loss');

    return claimIsValid;
}

function checkAutoClaim() {
    var claimIsValid = true;

    claimIsValid &= window.WUC.validateRequired('claim_auto_claimant', 'Please enter the name of the claimant');

    claimIsValid &= validateRequiredMulti('claim_auto_nature', 'claim_auto', 'Please select the nature of the claim');

    return claimIsValid;
}

function checkPropertyClaim() {
    var claimIsValid = true;
   
    claimIsValid &= validateRequiredMulti('claim_property_loss_types[]', 'claim_property', 'Please select at least one type of loss.');

    return claimIsValid;
}

//Claim Type
$$('input[name=claims_type]').addEvent('click', function () {
    var value = $$('input[name=claims_type]:checked')[0].get('value');
    $$('.claim_type').setStyle('display', 'none');

    if (value.indexOf('Comp') != -1) {
        $$('.claim_workers_comp').setStyle('display', 'block');
        $$('.claims_recipient_fax').setStyle('display', 'block');
    }
    else if (value.indexOf('Liability') != -1) {
        $$('.claim_general_liab').setStyle('display', 'block');
        $$('.gen-liab-only').setStyle('display', 'block');
        $$('.claims_recipient_fax').setStyle('display', 'block');
    }
    else if (value.indexOf('Auto') != -1) {
        $$('.claim_auto').setStyle('display', 'block');
        $$('.claims_recipient_fax').setStyle('display', 'block');
    }
    else if (value.indexOf('Property') != -1) {
        $$('.claim_property').setStyle('display', 'block');
        $$('.claims_recipient_fax').setStyle('display', 'block');
    }
    else if (value.indexOf('Other') != -1) {
        $$('.claim_general_liab').setStyle('display', 'block');
        $$('.gen-liab-only').setStyle('display', 'none');
        $$('.claims_recipient_fax').setStyle('display', 'none');
    }
    else if (value.indexOf('Labor') != -1) {
        $$('.claim_labor_law').setStyle('display', 'block');
        $$('.claims_recipient_fax').setStyle('display', 'none');
    }
});
$$('input[name=claims_type]:checked').fireEvent('click');

function eventTypeChanged() {
    var type = $('claim_workers_comp_event_type').get('value');
    $$('.inj_ill').hide();
    $$('.inj_ill_' + type).show();
}
$('claim_workers_comp_event_type').addEvent('change', eventTypeChanged);
eventTypeChanged();

function classificationChanged(selectedItem) {
    // implementation of check boxes to act like radio buttons
    // deal with it, I guess...

    document.getElementById('work_comp_class_G').checked = false;
    document.getElementById('work_comp_class_H').checked = false;
    document.getElementById('work_comp_class_I').checked = false;
    document.getElementById('work_comp_class_J').checked = false;

    var detailsH = $$('.wc_classification_details_H');
    detailsH.hide();

    var detailsI = $$('.wc_classification_details_I');
    detailsI.hide();


    if (selectedItem === 'work_comp_class_G') {
        document.getElementById('work_comp_class_G').checked = true;
    }

    if (selectedItem === 'work_comp_class_H') {
        document.getElementById('work_comp_class_H').checked = true;
        var classificationH = $$('input[id=work_comp_class_H]:checked')[0];
        detailsH.show();
        $$(classificationH).getParent().grab($$('.wc_classification_details_container_H')[0], 'after');
        detailsI.show();
        classificationI = $$('input[id=work_comp_class_I]')[0];
        $(classificationI).getParent().grab($$('.wc_classification_details_container_I')[0], 'after');
    }

    if (selectedItem === 'work_comp_class_I') {
        document.getElementById('work_comp_class_I').checked = true;
        classificationI = $$('input[id=work_comp_class_I]:checked')[0];
        detailsI.show();
        $$(classificationI).getParent().grab($$('.wc_classification_details_container_I')[0], 'after');
    }

    if (selectedItem === 'work_comp_class_J') {
        document.getElementById('work_comp_class_J').checked = true;
    }
    return;

        var classificationG = $$('input[id=work_comp_class_G]:checked');

    // if G is selected uncheck the others and hide their additional form
    // data
    if (classificationG.length > 0) {
    }

    if (classificationH.length > 0) {
        classificationH = classificationH[0];
        $$('.wc_classification_details_H').show();
        $$(classificationH).getParent().grab($$('.wc_classification_details_container_H')[0], 'after');
    }

    var classificationI = $$('input[id=work_comp_class_I]:checked'),
        detailsI = $$('.wc_classification_details_I');


    detailsH.hide();
    detailsI.hide();
    
    if (classificationH.length > 0) {
        classificationH = classificationH[0];
        $$('.wc_classification_details_H').show();
        $$(classificationH).getParent().grab($$('.wc_classification_details_container_H')[0], 'after');
    }
    
    if (classificationI.length > 0) {
        classificationI = classificationI[0];
        $$('.wc_classification_details_I').show();
        $$(classificationI).getParent().grab($$('.wc_classification_details_container_I')[0], 'after');
    }
}

function updateBodyPartToggle() {
    var checkedParts = $$('.body_part:checked'),
        anyChecked = checkedParts.length > 0,
        link = $('toggle_body_parts'),
        bodyParts = $('body_part'),
        selectedParts = $('selected_body_parts');

    var parts = [];
    for (var i = 0; i < checkedParts.length; ++i) {
        var part = checkedParts[i].get('value');
        if(part == 'Other') {
            var otherValue = $('claim_workers_comp_body_parts_other').get('value');
            if(otherValue) {
                part = part + ' (' + otherValue + ')';
            }
        }
        parts.push(part);
    }
    selectedParts.set('text', parts.join(', '));
    selectedParts.show();

    if (!anyChecked) {
        link.hide();
        bodyParts.show('inline-block');
        selectedParts.set('text', 'Select one or more body parts');
    } else {
        link.show();

        var bodyPartsVisible = bodyParts.getStyle('display') !== 'none';
        if (bodyPartsVisible) {
            link.set('text', 'Collapse...')
        } else {
            link.set('text', 'Change...')
        }
    }
}
function toggleBodyPartDisplay() {
    $('body_part').toggle('inline-block');
    updateBodyPartToggle();
}
$$('.body_part').addEvent('change', updateBodyPartToggle);
$('claim_workers_comp_body_parts_other').addEvent('change', updateBodyPartToggle);
$('toggle_body_parts').addEvent('click', toggleBodyPartDisplay);
updateBodyPartToggle();

function setOtherVisibility(triggerElement, otherElement) {
    var setVis = function() {
        var radioVal = triggerElement.get('checked'),
            text = otherElement;

        if (radioVal) {
            text.show('inline');
        } else {
            text.hide();
        }
    };
    var listenEl = triggerElement;
    if(listenEl.type == 'radio') {
        listenEl = $$('input[name="' + listenEl.name + '"]');
    }
    listenEl.addEvent('change', setVis);
    setVis();
}
setOtherVisibility($('claim_workers_comp_injury_type-other'), $('claim_workers_comp_injury_type_other'));
setOtherVisibility($('claim_workers_comp_illness_type-other'),$('claim_workers_comp_illness_type_other'));
setOtherVisibility($('claim_workers_comp_body_parts-other'),$('claim_workers_comp_body_parts_other'));
setOtherVisibility($('claim_general_liab_loss_area-other'),$('claim_general_liab_loss_area_other'));
setOtherVisibility($('claim_general_liab_loss_type-other'),$('claim_general_liab_loss_type_other'));
setOtherVisibility($('claim_auto_nature-other'),$('claim_auto_nature_other'));
setOtherVisibility($('claim_property_loss_type-other'),$('claim_property_loss_type_other'));

window.addEvent('domready', function () {
    new MooCal($('claims_datetime'), {
        leaveempty: true,
        clickout: true
    }).addEvent('change', function () {
        setTimeout(function(){ 
            jQuery('#summary_claims_date').html(jQuery('#claims_datetime').val()); 
        }, 1000);
    });

    $$('body').addEvent('click:relay(input[name="claims_x_carriers_datetime_submitted[]"])', function (event) {
        var input = $$('input[name="claims_x_carriers_datetime_submitted[]"]');
        input.each(function(input) {
            new MooCal(input, {
                leaveempty: true,
                clickout: true,
                readonly: false
            });
        });
    });
    new MooCal($('claims_datetime_notified'), {
        leaveempty: true,
        clickout: true,
        readonly: false
    });

    $$('body').addEvent('click:relay(input[name="claims_datetime_closed[]"])', function (event) {
        var claims_datetime_closed = $$('input[name="claims_datetime_closed[]"]');
        claims_datetime_closed.each(function(claims_datetime_closed) {
            new MooCal(claims_datetime_closed, {
                leaveempty: true,
                clickout: true,
                readonly: false
            });
         });
    });

    $$('.datepicker').each(function (item) {
        new MooCal($(item), {
            leaveempty: true,
            clickout: true,
            readonly: false
    });
    });

    var delayUpdateDurations = onEventLoop(updateDurationFields);

    $('claim_workers_comp_datetime_away_start').addEvent('blur', delayUpdateDurations)
        .retrieve('MooCal').addEvent('change', delayUpdateDurations);
    $('claim_workers_comp_datetime_away_end').addEvent('blur', delayUpdateDurations)
        .retrieve('MooCal').addEvent('change', delayUpdateDurations);

    $('claim_workers_comp_datetime_restricted_start').addEvent('blur', delayUpdateDurations)
        .retrieve('MooCal').addEvent('change', delayUpdateDurations);
    $('claim_workers_comp_datetime_restricted_end').addEvent('blur', delayUpdateDurations)
        .retrieve('MooCal').addEvent('change', delayUpdateDurations);

    var fileCount = 1;
    //Files
    $('add_file').addEvent('click', function (e) {
        e.stop();
        var newRow = '<td><input type="checkbox" name="submit-new-doc[' + fileCount + ']"/></td>' +
            '<td><input type="file" name="claims_file[' + fileCount + ']" class="j-file"></td>' +
            '<td></td>' +
            '<td><a href="#">Delete</a></td>';
        newRow = new Element('tr', {html: newRow});
        newRow.inject($('uploaded-files'));
        newRow.getElement('a').addEvent('click', deleteRow);
        ++fileCount;
    });
    
    var count = 1;
    $('add_copy_carrier').addEvent('click', function (e) {
        e.stop();
        var input = $('send_copy_carriers_ids').clone();
        input.set("value","");
        input.set('id','send_copy_carriers_ids'+count);
        input.set('class','automatic');
        input.inject(this.getParent(), 'before');
        new Element('br').inject(input, 'before');
        ++count;
    });
    

 $$('body').addEvent('blur:relay(input[name="claims_amount_reserved[]"])',  updateAmountFields);
 $$('body').addEvent('blur:relay(input[name="claims_amount_paid[]"])',  updateAmountFields);
 $$('body').addEvent('change:relay(".showOnSelect")',  toggleSubmissionEmail);
 $$('body').addEvent('blur:relay(input[name="claims_x_carriers_adjusters_email[]"])', updateSumbissionMethod  );
 $$('body').addEvent('blur:relay(input[name="claims_x_carriers_adjusters_name[]"])', updateSumbissionMethodAdjusterName  );
 
$$('body').addEvent('change:relay(select[name="join_status_codes_id[]"])', function (event) {
    var fieldid = jQuery(event.target).closest('fieldset');
    if(jQuery(this).val() == 'SUBMIT_CLOSE'){
        fieldid.find('input[name="claims_datetime_closed[]"]').prop('disabled',false);
    }else{
        fieldid.find('input[name="claims_datetime_closed[]"]').prop('disabled',true);
    }
});
    
     $('add-another').addEvent('click', function (e) {
        e.stop();
        var input = $('claim_employment_involved_parties').clone();
        input.set("value","");
        input.inject(this, 'before');
        new Element('br').inject(input, 'before');
    });


    var $submitAll = $('submitAll');
    if ($submitAll) {
        $submitAll.addEvent('change', function () {
            var submits = $submitAll.getParent('table').getElements('input[type=checkbox');
            for (var i = 0; i < submits.length; ++i) {
                submits[i].checked = $submitAll.checked;
            }
        });
    }
});
function disableClosedDate(){
    jQuery('.j-carrier:visible').each(function() {
        var $carrier = jQuery(this);
        status = $carrier.find('select[name="join_status_codes_id[]"]').val();
        //close_date = $carrier.find('input[name="claims_datetime_closed[]"]')
        if(status == 'SUBMIT_CLOSE' ){
            $carrier.find('input[name="claims_datetime_closed[]"]').prop('disabled',false);
        }else{
            $carrier.find('input[name="claims_datetime_closed[]"]').prop('disabled',true);
        }
       
    });
}
function deleteRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function validateCarrier(form){
     var claimIsValid = true;
    jQuery('.j-carrier').each(function() {
        var $carrier = jQuery(this);
        var fieldsetid = $carrier.closest('fieldset').attr('id');
        var carriers_id = $carrier.find('[name="join_carriers_id[]"]').val();
        if(carriers_id){
            var status = $carrier.find('select[name="join_status_codes_id[]"]');
            if(status.length >0){
                
                var submit_type =  $carrier.find('select[name="join_status_codes_id[]"]').val();

                if( (jQuery('#carriers_fax_input-'+carriers_id+':checkbox:checked').length <= 0 && 
                     jQuery('#carriers_email_input-'+carriers_id+':checkbox:checked').length <= 0 &&
                     jQuery('#carriers_adjuster_input-'+carriers_id+':checkbox:checked').length <= 0 )
                     && !submit_type){
                   claimIsValid = false;
                   window.WUC.createjQueryErrorMessage(status,'Please select status')
                }
            } 
            if( $carrier.find('input[name="claims_amount_paid[]"]').length >0){
                var paid =  $carrier.find('input[name="claims_amount_paid[]"]').val();
               
                if (isNaN(parseFloat(paid))) {
                    claimIsValid = false;
                    window.WUC.createjQueryErrorMessage( $carrier.find('input[name="claims_amount_paid[]"]'),'Please enter a number for amount paid')
                }
            } 
            if( $carrier.find('input[name="claims_amount_reserved[]"]').length >0){
                var reserved =  $carrier.find('input[name="claims_amount_reserved[]"]').val();
                if (isNaN(parseFloat(reserved))) {
                    claimIsValid = false;
                    window.WUC.createjQueryErrorMessage( $carrier.find('input[name="claims_amount_reserved[]"]'),'Please enter a number for reserved paid')
                }
            } 
        }
    });
    return claimIsValid;
    
}
function updateSumbissionMethodAdjusterName(){

    var fieldset_id = jQuery(this).parents(".j-carrier").attr('id');
    var carriers_id =  jQuery('#'+fieldset_id).find('select[name="join_carriers_id[]"]').val();
    if(jQuery.type(carriers_id) === "undefined"){
        carriers_id = jQuery('#'+fieldset_id).find('input:hidden[name="join_carriers_id[]"]').val();
    }
    var adjusters_name = jQuery('#'+fieldset_id).find('input[name="claims_x_carriers_adjusters_name[]"]').val();
    if( jQuery('#carriers_adjuster_name-'+carriers_id).length > 0 ){
        jQuery('#carriers_adjuster_name-'+carriers_id).html(adjusters_name)
    }
    if($('carriers_adjuster_input-'+carriers_id) && $('carriers_adjuster_input-'+carriers_id).checked == false){
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
         jQuery('#carrier_sent-'+carriers_id).remove();
        }
    }else{
        var msg = '<div id="carrier_sent-'+carriers_id+'"> The claim will be submitted to <b>'+ adjusters_name+'</b> via email</div>';
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
        }
        jQuery('#not_sent').remove();
        jQuery('#submit_message').append(jQuery(msg));
    }
}
function updateSumbissionMethod(){
    var fieldset_id = jQuery(this).parents(".j-carrier").attr('id');
    var carriers_id =  jQuery('#'+fieldset_id).find('select[name="join_carriers_id[]"]').val();
    if(jQuery.type(carriers_id) === "undefined"){
        carriers_id = jQuery('#'+fieldset_id).find('input:hidden[name="join_carriers_id[]"]').val();
    }
    var adjusters_email = jQuery('#'+fieldset_id).find('input[name="claims_x_carriers_adjusters_email[]"]').val();
    var adjusters_name = jQuery('#'+fieldset_id).find('input[name="claims_x_carriers_adjusters_name[]"]').val();
    if(adjusters_email.length>0){
        var submission_method ='<label><input class = "showOnSelect" id="carriers_adjuster_input-'+carriers_id+'" type="checkbox" name="claims_submit_type_adjuster-'+carriers_id+'" value="adjuster"> ';
        submission_method += '<span id="carriers_adjuster_name-'+carriers_id+'">'+adjusters_name+'</span> (<span id="carriers_adjuster_display'+carriers_id+'">'+adjusters_email+'</span>)</label>';
        jQuery('#adjuster_info-'+carriers_id).html(submission_method);
        if($('carriers_adjuster_input-'+carriers_id) && $('carriers_adjuster_input-'+carriers_id).checked == false){
            if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
            }
        }
        toggleSubmissionEmail();
        jQuery('#carriers_adjuster_input-'+carriers_id).on('change',function(){
                    
            unSelectOther(this,adjusters_name,"email");
        });
    }
}
function updateAmountFields() {
 jQuery('.j-carrier').each(function() {
     
   var $carrier = jQuery(this);
   var paid_text =  $carrier.find('input[name="claims_amount_paid[]"]');
   var paid = paid_text[0].get('value').replace(/[$,]/g, '') || '0';
   
   var reserved_text =$carrier.find('input[name="claims_amount_reserved[]"]');
   var reserved = reserved_text[0].get('value').replace(/[$,]/g, '') || '0';
   
   var medicalCosts_text = $carrier.find('input[name="claim_workers_comp_medical_costs[]"]');
   if(medicalCosts_text.length > 0 ) var medicalCosts = medicalCosts_text[0].get('value').replace(/[$,]/g, '') || '0';

    paid = parseFloat(paid);
    reserved = parseFloat(reserved);
    medicalCosts = parseFloat(medicalCosts);

    if (!isNaN(paid)) {
        setMoneyValue(paid, $carrier.find('input[name="claims_amount_paid[]"]'));
    }

    if (!isNaN(reserved)) {
        setMoneyValue(reserved, $carrier.find('input[name="claims_amount_reserved[]"]'));
    }

    if (!isNaN(medicalCosts)) {
       setMoneyValue(medicalCosts,  $carrier.find('input[name="claim_workers_comp_medical_costs[]"]'));
    }

    if (!isNaN(paid) && !isNaN(reserved)) {
        setMoneyValue(paid + reserved, $carrier.find('input[name="claims_amount_incurred[]"]'));
    } else {
        $carrier.find('input[name="claims_amount_incurred[]"]').val('');
    }

 });
}

function setMoneyValue(value, element) {
    value = '' + Math.floor(value * 100);
    while (value.length < 3) {
        value = '0' + value;
    }
    element.val(value.substring(0, value.length - 2) + '.' + value.substring(value.length - 2));
    //element.set('value', value.substring(0, value.length - 2) + '.' + value.substring(value.length - 2));
}

function updateDurationFields() {
    var wcAwayStartField = $('claim_workers_comp_datetime_away_start'),
        wcAwayEndField = $('claim_workers_comp_datetime_away_end'),
        wcAwayStart = moment(wcAwayStartField.get('value')),
        wcAwayEnd = moment(wcAwayEndField.get('value')),
        diff;

    if (wcAwayStart.isValid()) {
        if (!wcAwayEnd.isValid()) {
            wcAwayEnd = moment();
        }
        diff = wcAwayEnd.diff(wcAwayStart, 'days');
        if (diff < 0) {
            diff = 'End date is before start date';
        }
    } else {
        diff = '';
    }
    $('claim_workers_comp_total_away').set('value', diff);
    
    var wcRestrictedStartField = $('claim_workers_comp_datetime_restricted_start'),
        wcRestrictedEndField = $('claim_workers_comp_datetime_restricted_end'),
        wcRestrictedStart = moment(wcRestrictedStartField.get('value')),
        wcRestrictedEnd = moment(wcRestrictedEndField.get('value'));

    if (wcRestrictedStart.isValid()) {
        if (!wcRestrictedEnd.isValid()) {
            wcRestrictedEnd = moment();
        }
        diff = wcRestrictedEnd.diff(wcRestrictedStart, 'days');
        if (diff < 0) {
            diff = 'End date is before start date';
        }
    } else {
        diff = '';
    }
    $('claim_workers_comp_total_restricted').set('value', diff);
}
updateDurationFields();

function show_note() {
    var rightColumn = document.getElementById("notebox");
    rightColumn.show();
}

function onEventLoop(fn, scope) {
    scope = scope || window;
    return function () {
        var args = arguments;
        setTimeout(function () {
            fn.apply(scope, args);
        }, 0);
    };
}
function updateEmailNotice(){
    var arr = [];
    jQuery('.automatic:visible').each(function(k,v){
        if(jQuery(v).is('input')){
            arr.push(jQuery(v).parent().text().trim());
        }
        else{
            if(jQuery(v).find('option:selected').text().trim()!='-none-'){
                arr.push(jQuery(v).find('option:selected').text().trim());
            }
        }
    });
    jQuery('#additional_email').html(arr.join(', '))
}
function toggleSubmissionEmail(){
     var checkedParts = $$('.showOnSelect:checked');
     if(checkedParts.length > 0){
        anyChecked = 1;      
     }else{
        anyChecked = 0;
     }
     if(anyChecked==1){ 
         if($('notify_on_submission')) {
             $('notify_on_submission').show();
         }
         jQuery('#not_sent').remove();
     }else{ 
         if($('notify_on_submission')) {
            $('notify_on_submission').hide();
         }
         var notify_message = "This claim will not be sent to your carrier. To send the claim to your carrier, select an email or fax option above.";
         jQuery('#submit_message ').append(jQuery('<div id="not_sent">'+notify_message+'</div>'));
     }
     updateEmailNotice();
     manage_button_title();
     
}
function manage_button_title(){
    //manage button title
     var btn_title = 'Save';
     var notify = jQuery('#notify').val();
     var checkedParts = $$('.showOnSelect:checked');
     if(checkedParts.length > 0){
         btn_title += ', Submit';
     }
     if(notify > 0){
         btn_title += ' And Notify';
     }
     jQuery('#saveOnly').val(btn_title);
}
//////////////////// JQUERY
function unSelectOther(elem,carrier_name,method){
    var element_id = elem.id;
    var element = element_id.split('-');
    var carriers_id = element[1];
    if (element_id == 'carriers_fax_input-'+carriers_id &&  $('carriers_fax_input-'+carriers_id).checked == true)
    {
        //if fax is selected uncheck email option
        if($('carriers_email_input-'+carriers_id)) $('carriers_email_input-'+carriers_id).checked = false;
        if($('carriers_adjuster_input-'+carriers_id)) $('carriers_adjuster_input-'+carriers_id).checked = false;
        
        //add a div with message near save option
        var msg = '<div id="carrier_sent-'+carriers_id+'"> The claim will be submitted to <b>'+ carrier_name+'</b> via '+method +'.</div>';
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
        }
        jQuery('#not_sent').remove(); //remove not sent message
        jQuery('#submit_message').append(jQuery(msg));
    }
    if (element_id == 'carriers_adjuster_input-'+carriers_id && $('carriers_adjuster_input-'+carriers_id).checked == true)
    {
        if($('carriers_fax_input-'+carriers_id)) $('carriers_fax_input-'+carriers_id).checked = false;
        if($('carriers_email_input-'+carriers_id)) $('carriers_email_input-'+carriers_id).checked = false;
        var msg = '<div id="carrier_sent-'+carriers_id+'"> The claim will be submitted to <b>'+ carrier_name+'</b> via '+method +'.</div>';
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
        }
        jQuery('#not_sent').remove();
        jQuery('#submit_message').append(jQuery(msg));
        
    }
    if (element_id == 'carriers_email_input-'+carriers_id && $('carriers_email_input-'+carriers_id).checked == true)
    {
        if($('carriers_fax_input-'+carriers_id)) $('carriers_fax_input-'+carriers_id).checked = false;
        if($('carriers_adjuster_input-'+carriers_id)) $('carriers_adjuster_input-'+carriers_id).checked = false;
        var msg = '<div id="carrier_sent-'+carriers_id+'"> The claim will be submitted to <b>'+ carrier_name+'</b> via '+method +'.</div>';
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
        }
        jQuery('#not_sent').remove();
        jQuery('#submit_message').append(jQuery(msg));
        
    }
    //if both are unchecked, remove the message
    var fax_checked = false;
    if($('carriers_fax_input-'+carriers_id) && $('carriers_fax_input-'+carriers_id).checked == true ) fax_checked = true ;
    var email_checked = false;
    if($('carriers_email_input-'+carriers_id) && $('carriers_email_input-'+carriers_id).checked == true ) email_checked = true ;
    var adjuster_checked = false;
    if($('carriers_adjuster_input-'+carriers_id) && $('carriers_adjuster_input-'+carriers_id).checked == true ) adjuster_checked = true ;
    if(!fax_checked && !email_checked && !adjuster_checked){
        if( jQuery('#carrier_sent-'+carriers_id).length > 0 ){
             jQuery('#carrier_sent-'+carriers_id).remove();
        }
    }
   
    manage_button_title();
    
}
jQuery(document).ready(function($){
    $$('body').addEvent('change:relay(select[name="send_copy_carriers_ids[]"])', function (e) {
        updateEmailNotice();
    });
    $$('body').addEvent('change:relay(select[name="join_carriers_id[]"])', function (event) {
        removeUnusedCarrierSubmissions();
        if ( $(this).val() ) {
            $.getJSON("/API/carriers.php?carriers_id=" + $(this).val(), function (data) {
                if (!data) return;
                var carriers_id = data.carriers_id;
                var carriers_fax_display = data.carriers_fax;
                var carriers_email_display = data.carriers_email;
                
                var carrier_name = data.carriers_name;
                var submission_method = '<label>Submit to carrier <i>'+carrier_name+'</i></label>';
                submission_method += ' <fieldset>';
                if(carriers_fax_display){
                    submission_method +='<label><input class = "showOnSelect" id="carriers_fax_input-'+carriers_id+'" type="checkbox" name="claims_submit_type_fax-'+carriers_id+'" value="fax" />';
                    submission_method +='Fax (<span id="carriers_fax_display">'+carriers_fax_display+'</span>)</label><br>';
                }
                if(carriers_email_display){
                    submission_method +='<label><input class = "showOnSelect" id="carriers_email_input-'+carriers_id+'" type="checkbox" name="claims_submit_type_email-'+carriers_id+'" value="email">';
                    submission_method +='Email (<span id="carriers_email_display'+carriers_id+'">'+carriers_email_display+'</span>)</label>';
                }
                submission_method +='<div id="adjuster_info-'+carriers_id+'"></div>';
                submission_method +='</fieldset>';
                if(jQuery('#submission_info').find('#submission_info_'+carriers_id).length) {
                    jQuery('#submission_info_'+carriers_id).html(submission_method);
                }else{
                    jQuery('#submission_info ').append('<div class="form-field j-carrierSubmissionInfo" id="submission_info_'+carriers_id+'">'+submission_method+' </div>');
                }
                jQuery('#carriers_fax_input-'+carriers_id).on('change',function(){
                    unSelectOther(this,carrier_name,"fax");
                });
                jQuery('#carriers_email_input-'+carriers_id).on('change',function(){
                    unSelectOther(this,carrier_name,"email");
                });
            });
        } 
    });
});

function removeUnusedCarrierSubmissions() {
    var carrier_ids = jQuery.map(jQuery('[name=join_carriers_id\\[\\]]'),function(i) {
        return "submission_info_"+i.value;
    });
    jQuery('.j-carrierSubmissionInfo').each(function(i, el) {
        var id = el.id;
        if(carrier_ids.indexOf(id) <0) {
            $(el).remove();
        }
    })
}
function PreviewTemplate($email_id)
{
    openDialog('emails_history/preview_popup.php?id=' + $email_id, 800, 500);
    return false;
}

function delete_notes(e) {
    if (confirm("Are you sure? You want to delete this?")) {
        jQuery(e).parents('tr').remove();
    }
}
