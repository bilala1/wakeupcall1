jQuery(function() {
    var $ = jQuery;
    $('body').on('click', '.notes', function(e) {
        e.preventDefault();
        var parts = e.currentTarget.id.split('_');
        var id = parts[1];
        var iframeUrl = BASEURL + '/members/claims/notes/notes_popup.php?claims_id='+id;
        openIframeDialog(iframeUrl, 'Notes');
    });
});
window.addEvent('domready', function(){

    $$('.history').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];
        openDialog(BASEURL + '/members/claims/history.php?claims_id='+id,450,300);

        return false;
    });

    $$('.emailHistory').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];
        
        openDialog('emails_history/claim_history.php?claims_id='+id,800,400);

        return false;
    });
    
    $$('.files').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];
        openDialog(BASEURL + '/members/claims/files.php?claims_id='+id,450,300);

        return false;
    });
    
     // show the last tab that was selected. This is to give the user a good ux
    // when using the "return to claims list" from the edit page.
    var cookieName = 'claimstab';
    var tabCookie = Cookie.read(cookieName);
    if (!tabCookie ) { tabCookie = 'WorkersComp'};
    var tab = $$('#' + tabCookie);
    tab.show();
    $$('.tab-title[data-report-type=' + tabCookie + ']').addClass('activetab');

    $$('.tab-title').addEvent('click',function() {
        $$('.tab-title').removeClass('activetab');
        $$('.tab-content').hide();
        var tab = this.get('data-report-type');
        Cookie.write(cookieName, tab);
        this.addClass('activetab');
        $$('#' + tab).show();
        updateReportDropDown($$('.activetab').get('data-report-type'));
    });
    // add the reports to the reporting dropdown
    updateReportDropDown($$('.activetab').get('data-report-type'));
});

function openReport(data){
    queryString = "&reportid=" + data.id;
    // set the select dropdown back to show report
    this.selectedIndex = 0;

    var reqDialog = new MooDialog.Request(BASEURL + '/members/claims/claims_reports.php?' + queryString, null, {
        'class': 'MooDialog myDialog',
        autoOpen: false,
        size: {
            width: 800,
            height: 800
        },
        onShow: function() {

            // There are times when the dialog will call the onShow method but the contents of the
            // dialog haven't been added to the dom. Delay the initalization of the chart
            // a bit if we can't immediately find the dialog elements in the dom
            if (!$('reportRefresh')) {
                setTimeout(initChartDialog, 3000);
            } else {
                initChartDialog();
            }
        }
    });
    reqDialog.setRequestOptions({
        evalResponse: true,
        evalScript: true,
        onRequest: function(){
            reqDialog.setContent('loading...');
        },
        onComplete: function() {
        }
    }).open();

    return false;
}
function updateReportDropDown(report_type) {
    new Request.JSON({
            url: BASEURL + 'reports/reports_for_claim_type.php?claimType=' + report_type,
            onSuccess: function (reportList) {
                var reportDropDown = $('claim-reports');
                var report_list ='';
                var arrayLength = reportList.length;
                if (arrayLength > 0 ) {
                    for (var i = 0; i < arrayLength; i++) {
                        report_list += '<li><a href ="#" onclick="openReport(this)" id="'+reportList[i].reportId+'">'+reportList[i].title+'</a></li>';
                    }
                } else {
                    jQuery('#claim-reports').html('No Reports...');
                }
                 jQuery('#claim-reports').html(report_list);
            }
        }
    ).get();
}



function initChartDialog()
{
    $('reportRefresh').addEvent('click',function(event) {
        event.stop();
        chartRenderer.renderChart();
    });

    $('reportPrint').addEvent('click',function(event) {
        event.stop();
        var reportForm = $('report-form');
        var queryString = reportForm.toQueryString();
        var win = window.open('/members/claims/single_page_claims_reports.php?' + queryString);
        win.focus();
    });

    $('saveToFile').addEvent('click',function(event) {
        event.stop();
        new MooDialog.Request(BASEURL + '/members/claims/reportSaveDialog.php', null,
            {
                useScrollBar: false,
                onShow: function() {
                    var dialog = this;
                    var title = $('reportTitleString').getProperty('value').replace(/\+/g, ' ').trim();
                    $('reportName').setProperty('value',decodeURIComponent(title));
                    $('cancelReportButton').addEvent('click',function(event){
                        event.stop();
                        dialog.close();
                    });
                    $('saveReportButton').addEvent('click',function(event){
                        event.stop();
                        $('reportSaveError').set('html',"&nbsp;")
                        $('reportSaveErrorFolder').set('html',"&nbsp;")
                        var reportName = $('reportName').getProperty('value').trim();
                        if ( reportName.length === 0 ) {
                            $('reportSaveError').set('html',"You must provide a Report Name.");
                            return;
                        }

                        var categoryIdValue = $('members_library_categories_id').getSelected().get("value")[0];
                        if (!categoryIdValue) {
                            $('reportSaveErrorFolder').set('html',"You must select a folder.");
                            return;
                        }
                        var req = new Request({
                            method: 'post',
                            url: '/members/my-documents/saveChartToFile.php',
                            data: 'title=' + encodeURIComponent(reportName) + '&members_library_categories_id=' + categoryIdValue + '&image='+ $('chartDownload').get('data-chart'),
                            onFailure: function(xhr) {
                                alert('Failure has occurred. Info=' + xhr.statusText);
                            },
                            onSuccess: function(response) {
                                alert(response);
                            }
                        });
                        req.send();
                        dialog.close();
                    });
                },
                size: {
                    width: 400,
                    height: 140
                }
            }
        ).open();
    });

    $('chartDownload').addEvent('click',function(event) {
        event.stop();
        var data = this.getProperty('data-chart');
        var req = new Request({
            method: 'post',
            url: '/members/claims/uploadChartToTemp.php',
            data: 'image='+ $('chartDownload').get('data-chart'),
            onFailure: function(xhr) {
                alert('Failure has occurred. Info=' + xhr.statusText);
            },
            onSuccess: function(response) {
                window.location.href = '/members/claims/downloadChart.php?tmpFilename='+ response + '&filename=' + $('reportTitleString').getProperty('value');
            }
        });
        req.send();
    });

    new MooCal($('claims_start_time_dialog'), {
        leaveempty: false,
        clickout: true
    });

    new MooCal($('claims_end_time_dialog'), {
        leaveempty: false,
        clickout: true
    });

    // since the date picker is in a dialog, we need to set the date picker's
    // z-index higher than the dialog--the dialog is 6000.
    $$('.cal-container').setStyle('z-index','6001');

    // grab the data
    chartRenderer.renderChart();
}

    window.addEvent('load', function(){
        //calendar pickers
        new MooCal($('claims_start_time'), {
            leaveempty: true,
            clickout: true
        });

        new MooCal($('claims_end_time'), {
            leaveempty: true,
            clickout: true
        });

        //infobox settings
        var webinarsRequest = new Request({
            url: '/members/ajax-change-infobox-setting.php',
            method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_claims');
        });
         jQuery('#clear_button').bind('click', clear_filter);
    })
    
    function clear_filter(){ 
        jQuery('#clear_filter').val("clear_filter");
    }
    function PreviewTemplate($email_id)
    {
        openDialog('emails_history/preview_popup.php?id=' + $email_id,800,500);

        return false;
    }
