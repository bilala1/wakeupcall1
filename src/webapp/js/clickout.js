Element.Events.clickout = new Hash({
    base: 'click',
    condition: (function(){ return false; }),
    onAdd: function(fn){
        $(document.body).addEvent('click', function(event){
            var elem = false;
            if(typeof event.target != 'undefined') {
                elem = $(event.target);
            }
            
            if(elem !== false && !this.hasChild(elem)){
                fn(event);
            }
            else {
                return false;
            }
        }.bind(this));
    }
});
