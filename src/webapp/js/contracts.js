var recipients = null;

window.addEvent('domready', function () {

    $('save').addEvent('click', function () {
        this.disabled = true;
        var form = $('editContract');
        if (chkContractForm(form)) {
            form.submit();
        }else{
            this.disabled = false;
        }
    });
    jQuery('#licensed_locations input:checkbox').bind('change', debounce(onLicensedLocationChange, 50));
    onLicensedLocationChange();

    $$('body').addEvent('click:relay(".add-milestone-recipient")', function (e) {
        e.stop();
        var rowid = this.id.split('_');;
        var milestoneCount = rowid[1];
        var newRow = '<label><span>Email Address:</span></label> ' +
            '<input name="contracts_milestones_additional_recipients_email_'+milestoneCount+'[]" maxlength="200" class="input-text"> '+
            '<a href="#" class="delete-recipient">Delete</a>';
        newRow = new Element('div', {'class': 'form-field', html: newRow});

        newRow.inject($('additional-milestone-recipients_'+milestoneCount), 'bottom');
        newRow.getElement('a').addEvent('click', deleteRecipient);
    });
    
    $$('body').addEvent('click:relay(".j-confirmDelete")',  verifyDelete);//
    
    $$('body').addEvent('click:relay(.milestone_date)', getCal );
    
    $('contracts_contracted_party_name').addEvent('change',function(){
        jQuery('#summary_contracts_contracted_party_name').html(jQuery('#contracts_contracted_party_name').val());
    })
    $('contracts_description').addEvent('change',function(){
        jQuery('#summary_contracts_description').html(jQuery('#contracts_description').val());
    })
    $('contracts_status').addEvent('change',function(){
        var contracts_status = jQuery('option:selected', jQuery(this)).text(); 
        jQuery('#summary_contracts_status').html(contracts_status);
    })
});

function verifyDelete(event) {
        var fieldid = jQuery(event.target).closest('fieldset').attr('id');
        event.preventDefault();
        var $el = $(event.target);
        var message =  'Are you sure you want to delete this milestone?'
            new MooDialog.Confirm(message, function () {
                $(fieldid).remove();
            },
            function () {
            },
            {scroll: false}
        );
}

function onLicensedLocationChange() {
    var users = {};
    var locations = jQuery('#licensed_locations input:checkbox:checked');
    if (locations.length == 0) {
        locations = jQuery('#single_location_id')
    }
    var asyncRequests = [];
    var summary_location = [];
    for (var i = 0; i < locations.length; i++) {
        var location_id = jQuery(locations[i]).attr('value');
        
        if (location_id != 'all') {
            //code to disply location name in summary
            summary_location.push(licensed_locations_array[location_id]);
            //end
            var url = '/API/users_with_location_permission.php?licensed_locations_id=' + location_id + '&permission=contracts';
            asyncRequests.push(jQuery.ajax({
                url: url,
                success: function (result) {
                    if (result) {
                        result.forEach(function(user) {
                            users[user.id] = user;
                        });
                    }
                }
            }));
        }
    }
    
    //code to disply location name in summary
    if(summary_location){
        var summary_locations = summary_location.join(", ");
        jQuery(summary_locations_name).html(summary_locations);
    }
    //end
    
    jQuery.when.apply(null, asyncRequests).done(function () {
        recipients = [];
        for(var key in users) {
            if(users.hasOwnProperty(key)) {
                recipients.push(users[key]);
            }
        }
        recipients.sort(function(a, b) {
            var val = a.lastName.toLowerCase().localeCompare(b.lastName.toLowerCase());
            if(val == 0) {
                val = a.firstName.toLowerCase().localeCompare(b.firstName.toLowerCase());
            }
            return val;
        });
        updateRecipients();
    });
}

function checkboxForRecipient(recipient, checked, fieldName) {
    checked = checked ? 'checked="checked"' : '';
    return '<label class="Dynlabel">' +
        '<span><input class="input-checkbox" type="checkbox" name="' + fieldName + '[]" value="' + recipient.id + '" ' + checked + '>' +
        '</span>&nbsp;'
        + recipient.lastName + ', ' + recipient.firstName + ' (' + recipient.permission +
        ")</label>";
}

function updateRecipients() {
    updateContractRecipients();
    updateAllMilestonesRecipients();
}

function updateContractRecipients() {
    var template = '';
    recipients.forEach(function (recipient) {
        var checked = jQuery.inArray(recipient.id, remindExpiredMembers) != -1;
        template += checkboxForRecipient(recipient, checked, 'contracts_expired');
    });
    jQuery('#remind_expired').html(template);
}

function updateAllMilestonesRecipients() {
    var milestoneCount = jQuery('.j-additional-milestone').length;
    while (milestoneCount) {
        --milestoneCount;
        updateMilestoneRecipients(milestoneCount);
    }
}

function updateMilestoneRecipients(index) {
    var $milestone = jQuery('#milestoneDetails_' + index),
        milestoneId = $milestone.find('input[name^=contracts_milestones_id]').val(),
        initialCheckedMembers = (milestoneId && remindMilestoneMembers[milestoneId]) || [];

    //todo - add members that are currently checked to the initialCheckedMembers variable.
    var template = '';
    recipients.forEach(function (recipient) {
        var checked = jQuery.inArray(recipient.id, initialCheckedMembers) != -1;
        template += checkboxForRecipient(recipient, checked, 'contracts_milestones_remind_members_' + index);
    });
    $milestone.find('.remindMembersContainer').html(template);
}

function AddNewMilestone(){
    var milestoneCount = $$('.j-additional-milestone').length;
    var milestone_template =  '<legend>New Milestone <a class="j-toggle-trigger"></a> '+
                              '<a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">'+
                              '<img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" /> </a>'+
                              '</legend>'+
                              '<div class="j-toggle-content">'+
                              '<div class="form-field">'+
                              '<label><span>Milestone Description:</span></label>'+
                              '<input type="text" name="contracts_milestones_description['+milestoneCount+']">'+
                              '</div>'+
                              '<div class="form-field">'+
                              '<label><span>Milestone Date:</span></label>'+
                              '<input type="text" name="contracts_milestones_date['+milestoneCount+']" class="milestone_date">'+
                              '</div>'+
                              '<div class="form-field" id="remind_days_'+milestoneCount+'">'+
                              '<label><span>Remind before:</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="contracts_milestones_remind_days_'+milestoneCount+'[]" value= 0 ><label class="milestone_check"><span>0 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="contracts_milestones_remind_days_'+milestoneCount+'[]" value= 30 ><label class="milestone_check"><span>30 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="contracts_milestones_remind_days_'+milestoneCount+'[]" value= 60 ><label class="milestone_check"><span>60 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="contracts_milestones_remind_days_'+milestoneCount+'[]" value= 90 ><label class="milestone_check"><span>90 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="contracts_milestones_remind_days_'+milestoneCount+'[]" value= 120 ><label class="milestone_check"><span>120 Days</span></label>'+
                              '</div>'+
                              '<div class="form-field">'+
                              '<label>Send Emails To:</label><fieldset class="send-rem-check">'+
                              '<div class="remindMembersContainer" id="remind_milestone_'+milestoneCount+'"></div>'+
                              '</div>'+
                              '<label>Additional Email Recipients:</label>'+
                              '<a href="#" class="add-item add-milestone-recipient" id ="add-milestone-recipient_'+milestoneCount+'">Add a recipient</a><br/>'+
                              '<div id="additional-milestone-recipients_'+milestoneCount+'"></div>'+
                              '</div>';
    milestone_template = new Element('fieldset', {'id':'milestoneDetails_'+milestoneCount,'class': 'j-toggle-parent j-additional-milestone', html: milestone_template,style:"display:none;"});
    milestone_template.getElement('.milestone_date').addEvent('click', getCal);

    milestone_template.inject($('j-additional-milestone'), 'bottom');
    new Fx.Reveal(milestone_template).reveal();

    updateMilestoneRecipients(milestoneCount);
    return false;
}

createErrorMessage = window.WUC.createErrorMessage;
validateRequired = window.WUC.validateRequired ;

function validateRequiredMulti(inputName, errInput, message) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    if (!value) {
        createErrorMessage(errInput, message);
    }
    return value;
}

function validateOptionalEndAfterStart(startField, endField) {
    startField = (typeof(startField) === 'string') ? $(startField) : startField;
    endField = (typeof(endField) === 'string') ? $(endField) : endField;

    var start = moment(startField.get('value')),
        end = moment(endField.get('value')),
        fieldsValid = true;

    if (startField.get('value') && !start.isValid()) {
        createErrorMessage(startField, 'Effective date is not a valid format');
        fieldsValid = false;
    }

    if (endField.get('value') && !end.isValid()) {
        createErrorMessage(endField, 'Expiration date is not a valid format');
        fieldsValid = false;
    }

    if (start.isValid && end.isValid()) {
        if (end.diff(start, 'days') <= 0) {
            createErrorMessage(endField, 'Expiration date must be after effective date');
            fieldsValid = false;
        }
    }
    if(!fieldsValid){
        scrollToFirstError()
    }
    return fieldsValid;
}
function anySelected(inputName) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    return value;
}
function chkContractForm(form) {
    var contractIsValid = true;

    $$('p.form-error').destroy();

    contractIsValid &= validateRequired('contracts_effective_date', 'Please enter the effective date of the contract');
    contractIsValid &= validateOptionalEndAfterStart('contracts_effective_date', 'contracts_expiration_date');
    contractIsValid &= validateRequired('contracts_contracted_party_name', 'Please enter the name of the contracted party');
    contractIsValid &= validateRequired('contracts_status', 'Please select the status of the contract');

    var status = $('contracts_status').get('value'),
        active = 'Active' === status;

    if (active) {
        contractIsValid &= validateRequired('contracts_transition_status_to', 'Please select the status to transition to at the end of the contract');
        contractIsValid &= validateRequired('contracts_expiration_notify_lead_days', 'Please select the number of days in advance of the contract\'s expiration that you\'d like to be notified');
    }
    if (!anySelected('contracts_expired[]') && $('contracts_expired_notification').checked) {
        contractIsValid &= false;
        createErrorMessage($('remind_expired').getParent('.form-field'), 'Please select someone to receive the email');
    }
    contractIsValid &= validateMilestone(form);
    $$('.j-file').forEach(function(item) {
        contractIsValid &= validate_filetype($(item));
    });
    if(!contractIsValid){
        scrollToFirstError()
    }
    return contractIsValid;
}

function validateMilestone(form){
     var contractIsValid = true;
     var i = 0;
    jQuery('.j-additional-milestone').each(function() {
        var $milestone = jQuery(this);
        var milestone_date = $milestone.find('input[name="contracts_milestones_date['+i+']"]');
        var milestone_date_val = milestone_date.val();
        if(milestone_date_val && !moment(milestone_date_val).isValid()){
            contractIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_date,'Please enter valid date')
        }
        var milestone_description = $milestone.find('input[name="contracts_milestones_description['+i+']"]');
        var milestone_description_val = milestone_description.val();
        if(milestone_date_val == '' && milestone_description_val == ''){
            //donothing
        }else if(milestone_date_val == '' && milestone_description_val != ''){
            contractIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_date,'Please enter date');
        }else if(milestone_description_val == '' && milestone_date_val ){
            contractIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_description,'Please enter description');
        }
        var milestone_remind_days = $milestone.find('input[name="contracts_milestones_remind_days_'+i+'[]"]');
        if(!window.WUC.anySelected('contracts_milestones_remind_days_'+i+'[]') && !window.WUC.anySelected('contracts_milestones_remind_members_'+i+'[]') ){
             //donothing
        }
        if(window.WUC.anySelected('contracts_milestones_remind_days_'+i+'[]') && !window.WUC.anySelected('contracts_milestones_remind_members_'+i+'[]') ){
            contractIsValid = false;
            window.WUC.createjQueryErrorMessage(jQuery('#remind_milestone_'+i),'Please select someone to receive the email');
        }
        if(!window.WUC.anySelected('contracts_milestones_remind_days_'+i+'[]') && window.WUC.anySelected('contracts_milestones_remind_members_'+i+'[]') ){
            contractIsValid = false;
            window.WUC.createjQueryErrorMessage(jQuery('#remind_days_'+i),'Please select the remind before days ');
        }
        i++;
    });
    return contractIsValid;
}

window.addEvent('load', function () {
    $$('.datepicker').each(function (item) {
        new MooCal($(item), {
            leaveempty: true,
            clickout: true,
            readonly: false
        }).addEvent('change', function () {
            if(item.id == 'contracts_expiration_date'){
                setTimeout(function(){ 
                    jQuery('#summary_expiry').html(jQuery('#contracts_expiration_date').val()); 
                }, 1000);
            }
        });
    });

    var fileCount = 0;
    //Files
    $('add_file').addEvent('click', function (e) {
        e.stop();
        var newRow = '<td>&nbsp;</td><td><input type="file" name="contracts_file[' + fileCount + ']" class="j-file"></td>' +
            '<td>&nbsp;</td><td><a href="#">Delete</a></td>';
        newRow = new Element('tr', {html: newRow});
        newRow.inject($('uploaded-files'));
        newRow.getElement('a').addEvent('click', deleteRow);
        ++fileCount;
    });

    $$('.delete-recipient').addEvent('click', deleteRecipient);

    $('add-recipient').addEvent('click', function (e) {
        e.stop();
        var newRow = '<label><span>Email Address:</span></label> ' +
            '<input name="contracts_additional_recipients_email[]" maxlength="200" class="input-text"> '+
            '<a href="#" class="delete-recipient">Delete</a>';
        newRow = new Element('div', {'class': 'form-field', html: newRow});
        newRow.inject($('additional-recipients'), 'bottom');
        newRow.getElement('a').addEvent('click', deleteRecipient);
    });
    
    var milestoneCount = $$('.milestone_date').length;
    $('add_milestone').addEvent('click', function (e) {
        e.stop;
        AddNewMilestone();
    });
    
    
    });  
function getCal(){
    var input = $$('input[name="'+this.name+'"');
        input.each(function(input) {
            new MooCal(input, {
                leaveempty: true,
                clickout: true,
                readonly: false
            });
        });
}

function deleteMilestoneRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function deleteRecipient(e) {
    e.stop();
    $(this).getParent('div').destroy();
}

function deleteRow(e) {
    e.stop();
    $(this).getParent('tr').destroy();
}

function updateBasedOnStatus() {
    var status = $('contracts_status').get('value'),
        active = 'Active' === status;

    $('contracts_transition_status_to').set('disabled', active ? '' : 'disabled');
    $('contracts_expiration_notify_lead_days').set('disabled', active ? '' : 'disabled');
}
updateBasedOnStatus();
$('contracts_status').addEvent('change', updateBasedOnStatus);
