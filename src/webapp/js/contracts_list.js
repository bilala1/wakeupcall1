window.addEvent('domready', function () {
    $$('.history').addEvent('click', function () {
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog(BASEURL + '/members/contracts/history.php?contracts_id=' + id,450,300);

        return false;
    });

    $$('.files').addEvent('click', function () {
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog(BASEURL + '/members/contracts/files.php?contracts_id=' + id,450,300);

        return false;
    });

    $$('.notes').addEvent('click', function () {
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog(BASEURL + '/members/contracts/notes/notes_popup.php?contracts_id=' + id,450,300);

        return false;
    });

    $$('.details').addEvent('click', function () {
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog(BASEURL + '/members/contracts/details.php?contracts_id=' + id,450,300);

        return false;
    });
});

    window.addEvent('load', function () {
        
        new MooCal($('contracts_start_date'), {
            leaveempty: true,
            readonly:false,
            clickout: true
        });

        new MooCal($('contracts_expiration_date'), {
            leaveempty: true,
            readonly:false,
            clickout: true
        });

        //infobox settings
        var webinarsRequest = new Request({
            url: BASEURL + '/members/ajax-change-infobox-setting.php',
            method: 'get',
            onSuccess: function (responseText) {
                if (responseText == 'on') {
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html', 'Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html', 'Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function (event) {
            if ($$('.page-desc')[0].get('style').match(/display: none/gi)) {
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show + '&type=members_settings_infobox_contracts');
        });
    })