jQuery(function(){
    var $ = jQuery;
    var $file = $('#documents_file');
    $file.on('change', function(){
        var fi = $file.files;
        for(var i=0;i<fi.length;i++) {
            var file = fi[i];
            var n = file.name;
            n = n.charAt(0).toUpperCase() + n.substr(1,n.lastIndexOf(".") - 1);

            if(i == 0) {
                $('documents_title').set('value',n);
            }
            else {
                $('documents_title').set('value',$('documents_title').get('value') + '|' + n);
            }
        }

    });

    var $format = $('#documents_format');
    function formatChanged() {
        var format = $format.val();
        $('.j-documentFormatDetails').hide();
        $('.j-documentFormatDetails-'+format).show();
    }
    $format.on('change', formatChanged);
    formatChanged();
});