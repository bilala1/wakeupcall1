window.addEvent('domready', function () {
    $('save').addEvent('click', function (e) {
        var form = $('edititems');
        if (chkEntityItemsForm(form)) {
            form.submit();
        } else {
            e.preventDefault();
        }
    });

//    $$('.notes').addEvent('click', function () {
//        var parts = this.get('id').split('_');
//        var id = parts[1];
//        openDialog(BASEURL + '/members/entities/items/notes/view_popup.php?entities_items_notes_id=' + id,450,300);
//        return false;
//    });

     var noteCount =jQuery('.notebox').length
    //Notes
    $('another_note').addEvent('click', function (e) {
        e.stop();
        var newRow = '<td>'+
            '<input id="entities_items_notes_subject_'+noteCount+'" class="input-text show_on_edit" name="entities_items_notes_subject['+noteCount+']">'+
            '<input type="hidden" id="entities_items_hidden_subject_'+noteCount+'">'+
            '<span id="entities_items_notes_subject_text_'+noteCount+'" class="hide_on_edit"></span></td>' +
            '<td><span id="entities_items_notes_note_text_'+noteCount+'" class="hide_on_edit"></span>'+
            '<input type="hidden" id="entities_items_hidden_note_'+noteCount+'">'+
            '<textarea id="entities_items_notes_note_'+noteCount+'" name="entities_items_notes_note['+noteCount+']" class="show_on_edit"></textarea></td>' +
            '<td>&nbsp;</td>' +
            '<td><div class="actionMenu"><ul>'+
            '<li><a href="#"  id="donenotes_'+noteCount+'_new" class="show_on_edit donenotes">Done</a></li>'+
            '<li><a href="#"  id="cancelnotes_'+noteCount+'_new" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>'+
            '<li><a href="#"  id="editnotes_'+noteCount+'" class="hide_on_edit edit_notes" style="display:none;">Edit</a></li>'+
            '<li><a href="#"  id="deletenotes_'+noteCount+'" class="delete show_on_edit">Cancel</a></li>'+
            '</ul></div></td>';
        newRow = new Element('<tr>', {html: newRow});
        newRow.setAttribute('class','notebox')
        newRow.inject($('notebox_container'));
        newRow.getElement('a.delete').addEvent('click', deleteRow);
        ++noteCount;
    });
    
    $$('body').addEvent('click:relay(.cancelnotes)', function (e) {
        e.stop();
        var parts = this.get('id').split('_');
        var id = parts[1];
        
        jQuery('#entities_items_notes_subject_'+id).val(jQuery('#entities_items_hidden_subject_'+id).val())
        jQuery('#entities_items_notes_subject_text'+id).html(jQuery('#entities_items_hidden_subject_'+id).val())
        
        jQuery('#entities_items_notes_subject_text_'+id).show();
        jQuery('#entities_items_notes_subject_'+id).hide();

        jQuery('#entities_items_notes_note_'+id).val(jQuery('#entities_items_hidden_note_'+id).val())
        jQuery('#entities_items_notes_note_text_'+id).html(jQuery('#entities_items_hidden_note_'+id).val())
        jQuery('#entities_items_notes_note_text_'+id).show();
        jQuery('#entities_items_notes_note_'+id).hide();
        jQuery(this).parents('ul').find('.hide_on_edit').show();
        if(jQuery('#donenotes_'+id)) jQuery('#donenotes_'+id).hide();
        if(jQuery('#donenotes_'+id+'_new')) jQuery('#donenotes_'+id+'_new').hide();
        if('#cancelnotes_'+id) jQuery('#cancelnotes_'+id).hide();
        if(jQuery('#cancelnotes_'+id+'_new'))jQuery('#cancelnotes_'+id+'_new').hide();
        return ;
    });
    $$('body').addEvent('click:relay(.donenotes)', function (e) {
        e.stop();
        var parts = this.get('id').split('_');
        var id = parts[1];

        jQuery('#entities_items_notes_subject_text_'+id).html($('entities_items_notes_subject_'+id).get('value'))
        jQuery('#entities_items_hidden_subject_'+id).val($('entities_items_notes_subject_'+id).get('value'))
        jQuery('#entities_items_notes_subject_text_'+id).show();
        jQuery('#entities_items_notes_subject_'+id).hide();
        jQuery('#entities_items_notes_note_text_'+id).html($('entities_items_notes_note_'+id).get('value'))
        jQuery('#entities_items_hidden_note_'+id).val($('entities_items_notes_note_'+id).get('value'))

        jQuery('#entities_items_notes_note_text_'+id).show();
        jQuery('#entities_items_notes_note_'+id).hide();
        jQuery(this).parents('ul').find('.hide_on_edit').show();
        if('#donenotes_'+id) jQuery('#donenotes_'+id).hide();
        if('#donenotes_'+id+'_new') jQuery('#donenotes_'+id+'_new').hide();
        if('#cancelnotes_'+id) jQuery('#cancelnotes_'+id).hide();
        if('#cancelnotes_'+id+'_new') jQuery('#cancelnotes_'+id+'_new').hide();
        jQuery('#deletenotes_'+id).hide();
        if(parts[2] && parts[2] == 'new'){
            //changing label from cancel to delete
            jQuery('#deletenotes_'+id).replaceWith( '<a href="#"  id="deletenotes_'+id+'" class="delete show_on_edit">Delete</a>');
        }
        jQuery('#deletenotes_'+id).show();
        return ;
    });
    
    $$('body').addEvent('click:relay(.edit_notes)', function (e) {
        e.stop();
        jQuery(this).parents('tr').find('.show_on_edit').show();
        jQuery(this).parents('tr').find('.hide_on_edit').hide();
    });
    
    $('add_milestone').addEvent('click', function (e) {
        e.stop;
        AddNewMilestone();
    });
    
    $$('body').addEvent('click:relay(.milestone_date)', getCal );
    
    $$('.delete-recipient').addEvent('click', deleteRecipient);
    $$('body').addEvent('click:relay(".j-confirmDelete")',  verifyDelete);

    $$('body').addEvent('click:relay(".add-milestone-recipient")', function (e) {
        e.stop();
        var rowid = this.id.split('_');;
        var milestoneCount = rowid[1];
        var newRow = '<label><span>Email Address:</span></label> ' +
            '<input name="entities_items_milestones_additional_recipients_email_'+milestoneCount+'[]" maxlength="200" class="input-text"> '+
            '<a href="#" class="delete-recipient">Delete</a>';
        newRow = new Element('div', {'class': 'form-field', html: newRow});

        newRow.inject($('additional-milestone-recipients_'+milestoneCount), 'bottom');
        newRow.getElement('a').addEvent('click', deleteRecipient);
    });
    
    $$('body').addEvent('click:relay(.delete)', deleteRow);
    
});
function verifyDelete(event) {
        var fieldid = jQuery(event.target).closest('fieldset').attr('id');
        event.preventDefault();
        var $el = $(event.target);
        var message =  'Are you sure you want to delete this milestone?';
            new MooDialog.Confirm(message, function () {
                $(fieldid).remove();
            },
            function () {
            },
            {scroll: false}
        );
}
function AddNewMilestone(){
    var milestoneCount = $$('.j-additional-milestone').length;
    var milestone_template =  '<legend>New Milestone <a class="j-toggle-trigger"></a> '+
                              '<a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">'+
                              '<img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" /> </a>'+
                              '</legend>'+
                              '<div class="j-toggle-content">'+
                              '<div class="form-field">'+
                              '<label><span>Milestone Description:</span></label>'+
                              '<input type="text" name="entities_items_milestones_description['+milestoneCount+']">'+
                              '</div>'+
                              '<div class="form-field">'+
                              '<label><span>Milestone Date:</span></label>'+
                              '<input type="text" name="entities_items_milestones_date['+milestoneCount+']" class="milestone_date">'+
                              '</div>'+
                              '<div class="form-field" id="remind_days_'+milestoneCount+'">'+
                              '<label><span>Remind before:</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="entities_items_milestones_remind_days_'+milestoneCount+'[]" value= 0 ><label class="milestone_check"><span>0 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="entities_items_milestones_remind_days_'+milestoneCount+'[]" value= 30 ><label class="milestone_check"><span>30 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="entities_items_milestones_remind_days_'+milestoneCount+'[]" value= 60 ><label class="milestone_check"><span>60 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="entities_items_milestones_remind_days_'+milestoneCount+'[]" value= 90 ><label class="milestone_check"><span>90 Days</span></label>'+
                              '<input type="checkbox" class="input-checkbox"  name="entities_items_milestones_remind_days_'+milestoneCount+'[]" value= 120 ><label class="milestone_check"><span>120 Days</span></label>'+
                              '</div>'+
                              '<div class="form-field">'+
                              '<label>Send Emails To:</label><fieldset class="send-rem-check">'+
                              '<div class="remindMembersContainer" id="remind_milestone_'+milestoneCount+'"></div>'+
                              '</div>'+
                              '<label>Additional Email Recipients:</label>'+
                              '<a href="#" class="add-item add-milestone-recipient" id ="add-milestone-recipient_'+milestoneCount+'">Add a recipient</a><br/>'+
                              '<div id="additional-milestone-recipients_'+milestoneCount+'"></div>'+
                              '</div>';
    milestone_template = new Element('fieldset', {'id':'milestoneDetails_'+milestoneCount,'class': 'j-toggle-parent j-additional-milestone', html: milestone_template,style:"display:none;"});
    milestone_template.getElement('.milestone_date').addEvent('click', getCal);

    milestone_template.inject($('j-additional-milestone'), 'bottom');
    new Fx.Reveal(milestone_template).reveal();

    updateMilestoneRecipients(milestoneCount);
    return false;
}
function updateMilestoneRecipients(index) {
    var $milestone = jQuery('#milestoneDetails_' + index);
    var template = '';   
    for(var key in remindMilestoneMembers) {
        template += '<label class="Dynlabel">' +
        '<span><input class="input-checkbox" type="checkbox" name="entities_items_milestones_remind_members_'+ index + '[]" value="' + remindMilestoneMembers[key]. members_id + '" >' +
        '</span>&nbsp;'
        +remindMilestoneMembers[key].members_lastname+ ', '+remindMilestoneMembers[key].members_firstname +' ('+remindMilestoneMembers[key].permission+')</label>';
    };
   
    $milestone.find('.remindMembersContainer').html(template);
}

function deleteRecipient(e) {
    e.stop();
    $(this).getParent('div').destroy();
}
function getCal(){
    var input = $$('input[name="'+this.name+'"');
        input.each(function(input) {
            new MooCal(input, {
                leaveempty: true,
                clickout: true,
                readonly: false
            });
        });
}

function deleteMilestoneRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function validateItemNotes(){
     var i = 0;
     var formIsValid = true;
    jQuery('#notebox_container').each(function() {
        var $item_notes = jQuery(this);
        var notes_subject = $item_notes.find('input[name="entities_items_notes_subject['+i+']"]');
        var notes_subject_val = notes_subject.val();
        var notes_note = $item_notes.find('textarea[name="entities_items_notes_note['+i+']"]');
        var notes_note_val = notes_note.val();
        if(notes_subject_val == '' && notes_note_val == ''){
            //donothing
        }else if(notes_note_val == '' && notes_subject_val != ''){
            formIsValid &= false;
            window.WUC.createjQueryErrorMessage(notes_subject,'Please enter subject');
        }else if(notes_subject_val == '' && notes_note_val!='' ){
            formIsValid &= false;
            window.WUC.createjQueryErrorMessage(notes_subject,'Please enter notes');
        }
        i++;
    });
    return formIsValid;
}
function validateMilestone(form){
     var formIsValid = true;
     var i = 0;
    jQuery('.j-additional-milestone').each(function() {
        var $milestone = jQuery(this);
        var milestone_date = $milestone.find('input[name="entities_items_milestones_date['+i+']"]');
        var milestone_date_val = milestone_date.val();
        if(milestone_date_val && !moment(milestone_date_val).isValid()){
            formIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_date,'Please enter valid date')
        }
        var milestone_description = $milestone.find('input[name="entities_items_milestones_description['+i+']"]');
        var milestone_description_val = milestone_description.val();
        if(milestone_date_val == '' && milestone_description_val == ''){
            //donothing
        }else if(milestone_date_val == '' && milestone_description_val != ''){
            formIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_date,'Please enter date');
        }else if(milestone_description_val == '' && milestone_date_val ){
            formIsValid = false;
            window.WUC.createjQueryErrorMessage(milestone_description,'Please enter description');
        }
        var milestone_remind_days = $milestone.find('input[name="entities_items_milestones_remind_days_'+i+'[]"]');
        if(!window.WUC.anySelected('entities_items_milestones_remind_days_'+i+'[]') && !window.WUC.anySelected('entities_items_milestones_remind_members_'+i+'[]') ){
             //donothing
        }
        if(window.WUC.anySelected('entities_items_milestones_remind_days_'+i+'[]') && !window.WUC.anySelected('entities_items_milestones_remind_members_'+i+'[]') ){
            formIsValid = false;
            window.WUC.createjQueryErrorMessage(jQuery('#remind_milestone_'+i),'Please select someone to receive the email');
        }
        if(!window.WUC.anySelected('entities_items_milestones_remind_days_'+i+'[]') && window.WUC.anySelected('entities_items_milestones_remind_members_'+i+'[]') ){
            formIsValid = false;
            window.WUC.createjQueryErrorMessage(jQuery('#remind_days_'+i),'Please select the remind before days ');
        }
        
        i++;
    });
    return formIsValid;
}
function show_note() {
    var notebox = document.getElementById("notebox");
    notebox.show();
}

createErrorMessage = window.WUC.createErrorMessage;
validateRequired = window.WUC.validateRequired;

function anySelected(inputName) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    return value;
}

function chkEntityItemsForm(form) {
    var formIsValid = true;

    $$('p.form-error').destroy();

    formIsValid &= validateRequired('entities_items_name', 'Please enter a name');
    formIsValid &= validateItemNotes(form);
    formIsValid &= validateMilestone(form)
    $$('.j-file').forEach(function(item) {
        formIsValid &= validate_filetype($(item));

    });
    if (!formIsValid) {
        scrollToFirstError()
    }

    return formIsValid;
}

window.addEvent('load', function () {
    $$('.datepicker').each(function (item) {
        new MooCal($(item), {
            clickout: true,
            yearrange: [new Date().getFullYear() - 3, new Date().getFullYear() + 5],
            readonly: false
        });
    });

    var fileCount = 0;
    //Files
    $('add_file').addEvent('click', function (e) {
        e.stop();
        var newRow =
            '<td><input type="file" name="entities_items_files[]" class="j-file"></td>' +
            '<td></td>' +
            '<td><a href="#">Delete</a></td>';
        newRow = new Element('tr', {html: newRow});
        newRow.inject($('uploaded-files'));
        newRow.getElement('a').addEvent('click', deleteRow);
        ++fileCount;
    });

    $$('.deleteRow').addEvent('click', deleteRow);
});

function deleteRow(e) {
    e.preventDefault();
    $(e.target).getParent('tr').destroy();
}
function delete_notes(e) {
    if (confirm("Are you sure? You want to delete this?")) {
        jQuery(e).parents('tr').remove();
    }
}
jQuery(function() {
    var $ = jQuery;
    $('body').on('click', '.notes', function(e) {
        e.preventDefault();
        var parts = e.currentTarget.id.split('_');
        var id = parts[1];
        var iframeUrl = BASEURL + '/members/entities/items/notes/view.php?entities_items_notes_id=' + id+'&back=no';
        openIframeDialog(iframeUrl, 'Notes');
    });
});