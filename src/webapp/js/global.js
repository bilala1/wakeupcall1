// From lodash
function debounce(func, wait, options) {
    var lastArgs,
        lastThis,
        maxWait,
        result,
        timerId,
        lastCallTime,
        lastInvokeTime = 0,
        leading = false,
        maxing = false,
        trailing = true;

    wait = wait || 0;
    if (options) {
        leading = !!options.leading;
        maxing = 'maxWait' in options;
        maxWait = maxing ? nativeMax(options.maxWait || 0, wait) : maxWait;
        trailing = 'trailing' in options ? !!options.trailing : trailing;
    }

    function invokeFunc(time) {
        var args = lastArgs,
            thisArg = lastThis;

        lastArgs = lastThis = undefined;
        lastInvokeTime = time;
        result = func.apply(thisArg, args);
        return result;
    }

    function leadingEdge(time) {
        // Reset any `maxWait` timer.
        lastInvokeTime = time;
        // Start the timer for the trailing edge.
        timerId = setTimeout(timerExpired, wait);
        // Invoke the leading edge.
        return leading ? invokeFunc(time) : result;
    }

    function remainingWait(time) {
        var timeSinceLastCall = time - lastCallTime,
            timeSinceLastInvoke = time - lastInvokeTime,
            result = wait - timeSinceLastCall;

        return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
    }

    function shouldInvoke(time) {
        var timeSinceLastCall = time - lastCallTime,
            timeSinceLastInvoke = time - lastInvokeTime;

        // Either this is the first call, activity has stopped and we're at the
        // trailing edge, the system time has gone backwards and we're treating
        // it as the trailing edge, or we've hit the `maxWait` limit.
        return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
        (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
    }

    function timerExpired() {
        var time = new Date().valueOf();
        if (shouldInvoke(time)) {
            return trailingEdge(time);
        }
        // Restart the timer.
        timerId = setTimeout(timerExpired, remainingWait(time));
    }

    function trailingEdge(time) {
        timerId = undefined;

        // Only invoke if we have `lastArgs` which means `func` has been
        // debounced at least once.
        if (trailing && lastArgs) {
            return invokeFunc(time);
        }
        lastArgs = lastThis = undefined;
        return result;
    }

    function cancel() {
        if (timerId !== undefined) {
            clearTimeout(timerId);
        }
        lastInvokeTime = 0;
        lastArgs = lastCallTime = lastThis = timerId = undefined;
    }

    function flush() {
        return timerId === undefined ? result : trailingEdge(new Date().valueOf());
    }

    function debounced() {
        var time = new Date().valueOf(),
            isInvoking = shouldInvoke(time);

        lastArgs = arguments;
        lastThis = this;
        lastCallTime = time;

        if (isInvoking) {
            if (timerId === undefined) {
                return leadingEdge(lastCallTime);
            }
            if (maxing) {
                // Handle invocations in a tight loop.
                timerId = setTimeout(timerExpired, wait);
                return invokeFunc(lastCallTime);
            }
        }
        if (timerId === undefined) {
            timerId = setTimeout(timerExpired, wait);
        }
        return result;
    }
    debounced.cancel = cancel;
    debounced.flush = flush;
    return debounced;
}

window.WUC = window.WUC || {};

window.WUC.allChanged = function() {
    var inputs = $$('#licensed_locations input');

    // set the checked state of all of the boxes to the checked state of all
    inputs.set('checked', $('licensed_locations_all').get('checked') ? 'checked' : '');
};

window.WUC.oneChanged = function() {
    var inputs = $$('#licensed_locations input'),
        checked = $$('#licensed_locations input:checked');
    // Set all checked state if all other inputs are checked.
    // This kind of magically doesn't re-check all when all locations are selected individually,
    // which is good - we want them to be able to select all current locations with it applying
    // to future locations.
    $('licensed_locations_all').set('checked', inputs.length === checked.length ? 'checked' : '');
};

window.addEvent('domready', function () {
    $$(document.body).addEvent('click:relay(a.deletecheck)', function (evt) {
        evt.stop();
        var url = this.get('href');

        var message = this.get('title') != null ? this.get('title') : 'Are you sure you want to delete this?';

        new MooDialog.Confirm(message, function () {
                window.location.href = url;
            },
            function () {
                return false;
            }, {scroll: false}
        );

    });

    $$('input.phone').addEvent('keyup', function (event) {
        if (event.event.keyCode != '8' && event.keyCode != '46') {
            formatPhone(this);
        }
    });

    function formatPhone(input) {
        if (input.value.length == 1 && input.value != '(') {
            input.value = '(' + input.value;
        }
        if (input.value.length == 4) {
            input.value = input.value + ') ';
        }
        if (input.value.length == 5) {
            var spare = input.value.substr(input.value.length - 1);
            if (spare != ')') {
                input.value = input.value.substr(0, input.value.length - 1) + ') ' + spare;
            }
            else {
                input.value = input.value + ' ';
            }
        }
        if (input.value.length == 6) {
            var spare = input.value.substr(input.value.length - 1);
            if (spare != ' ') {
                input.value = input.value.substr(0, input.value.length - 1) + ' ' + spare;
            }
        }
        if (input.value.length == 10) {
            var spare = input.value.substr(input.value.length - 1);
            if (spare != '-') {
                input.value = input.value.substr(0, input.value.length - 1) + '-' + spare;
            }
        }
        if (input.value.length > 14) {
            input.value = input.value.substr(0, input.value.length - 1);
        }
    }

    if($('licensed_locations_all')) {
        if($('licensed_locations_all').get('checked')) {
            window.WUC.allChanged();
        }
        $('licensed_locations_all').addEvent('change', window.WUC.allChanged);
        $$('#licensed_locations input').addEvent('change', window.WUC.oneChanged);
    }


    $$('.datepicker').each(function(input) {
        new MooCal($(input), {
            leaveempty: true,
            readonly: false,
            clickout: true
        });
    });
});

jQuery(function () {
    var $ = jQuery;

    $('body').on('click', '.help-icon', function (e) {
        var content = $(e.currentTarget).data('content');
        if (content) {
            e.preventDefault();
            var dialog = new MooDialog();
            dialog.setContent(content);
            dialog.open();
        }
    });

    $('body').on('click', 'a.page-dialog-trigger', function(e) {
        e.preventDefault();
        $(e.currentTarget).toggleClass('expanded');
        var dialog = $(e.currentTarget).data('target');
        $(dialog).slideToggle();
        $('.page-dialog-background').fadeToggle();
    });

    $('body').on('click', '.page-dialog-background', function(e) {
        e.preventDefault();
        $('a.page-dialog-trigger.expanded').removeClass('expanded');
        $('.page-dialog:visible').slideUp();
        $('.page-dialog-background').fadeOut();
    });

    $('body').on('change', '.page-dialog input[name="join_licensed_locations_id[]"][value="all"]', function(e) {
        var checked = e.target.checked;
        $(e.currentTarget).parent().parent().find('input[name="join_licensed_locations_id[]"]').prop('checked', checked);
    });

    $('body').on('change', '.page-dialog input[name="join_licensed_locations_id[]"]', function(e) {
        var $group = $(e.currentTarget).parent().parent(),
            unchecked = $group.find('input[name="join_licensed_locations_id[]"]:not(:checked)'),
            all = $group.find('input[name="join_licensed_locations_id[]"]'),
            wasChecked = e.target.checked;

        if(wasChecked && unchecked.length == 1) {
            $group.find('input[value="all"]').prop('checked', true);
        }

        if(!wasChecked) {
            $group.find('input[value="all"]').prop('checked', false);
        }
    });

    $('body').on('click', 'a.j-toggle-trigger', function(e) {
        e.preventDefault();
        var parent = $(e.currentTarget).closest('.j-toggle-parent');
        parent.toggleClass('j-toggle-collapsed');
        parent.children('.j-toggle-content').slideToggle();
        parent.children('.j-toggle-summary').slideToggle();
    });
});

function scrollToFirstError(){
    var scroll_position = jQuery(window).scrollTop();
    // The siblings are checked because some errors are displaying outside the div
    var first_error = jQuery('p.form-error:first').siblings("div").position();
    if(!first_error){
        first_error = jQuery('p.form-error:first').closest("div").position();
    }
    var top_position = first_error.top;
    if(scroll_position > (top_position-20) ){
        scroll_position = top_position-20;
    }
    jQuery("html, body").animate({ scrollTop: scroll_position }, "slow");
}

window.WUC.ensureUncollapsed = function(el) {
    var parent = jQuery(el);
    var safety = 10;
    do {
        parent = parent.parent().closest('.j-toggle-parent');
        if (parent.hasClass('j-toggle-collapsed')) {
            parent.removeClass('j-toggle-collapsed');
            parent.children('.j-toggle-content').slideDown();
            parent.children('.j-toggle-summary').slideUp();
        }
    } while(parent.length && safety--)
};

window.WUC.scrollToFirstError = scrollToFirstError;

window.WUC.createErrorMessage = function(input, message) {
    var el = new Element('p');

    input = (typeof(input) === 'string') ? $(input) : input;

    el.set('class', 'form-error');
    el.set('html', message);
    el.inject(input, 'after');

    window.WUC.ensureUncollapsed(el);
    return el;
};

window.WUC.validateRequired = function(input, message) {
    var value;

    input = (typeof(input) === 'string') ? $(input) : input;
    value = !!input.get('value');

    if (!value) {
        window.WUC.createErrorMessage(input, message);
    }
    return value;
};

window.WUC.createjQueryErrorMessage = function (input, message) {
    var el = jQuery("<p class = 'form-error'>"+message+"</p>").insertAfter(input);
    window.WUC.ensureUncollapsed(el);
    return el;
};

window.WUC.anySelected = function(inputName) {
    var inputs = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length > 0;

    return value;
};

window.WUC.anySelectedOrNoInput = function(inputName) {
    var inputs = $$('input[name="' + inputName + '"]'),
        checked = $$('input[name="' + inputName + '"]:checked'),
        value = inputs.length === 0 || checked.length > 0;

    return value;
}

function openDialog(url,width,height){
   var reqDialog = new MooDialog.Request(url, null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: width,
                height: height
            }
        });

        reqDialog.setRequestOptions({
            onRequest: function(){
                reqDialog.setContent('loading...');
            }
        }).open();
}

function openIframeDialog(url, title) {
    var $ = jQuery;

    var minWidth = 300,
        minHeight = 500,
        width = window.innerWidth * .5,
        height = window.innerHeight * .5;

    var template = '<div class="dialog"><iframe style="width:100%; height: 100%" src="'+url+'"/></div>';
    $(template).dialog({
        modal: true,
        draggable: false,
        height: height,
        width: width,
        minHeight: minHeight,
        minWidth: minWidth,
        title: title
    });
}

function validate_filetype(file) {
    var _validFileExtensions = ['doc', 'docx', 'pdf','jpg','jpeg','png','gif','xls','xlsx']; 
    var fileName = file.get('value');
    var file_type = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (file_type.length > 0) {
        var blnValid = false;
        for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (file_type.toLowerCase() == sCurExtension.toLowerCase()) {
                blnValid = true;
                break;
            }
        }

        if (!blnValid) {
            file.getParent('td').addClass('error');
            window.WUC.createErrorMessage($('uploaded-files'), "Please upload files having extensions: <b>'doc', 'docx', 'pdf','jpg','jpeg','png','gif','xls','xlsx'");
            return false;
        }
    }

  
    return true;
}