(function ($) {

    'use strict';

    function verifyDelete(event) {
        event.preventDefault();

        var $el = $(event.target),
            numCerts = $el.data('numcerts'),
            legal_entity_names_id = $el.data('legal_entity_names_id');
        if (numCerts == 0) {
            verifyDeleteNoCerts($el, legal_entity_names_id);
        } else {
            verifyDeleteCerts($el, numCerts, legal_entity_names_id);
        }
    }

    function verifyDeleteNoCerts($el, legal_entity_names_id) {
        var message = $el.attr('title') || 'Are you sure you want to delete this?';

        new MooDialog.Confirm(message, function () {
                window.location.href = 'delete.php?legal_entity_names_id=' + legal_entity_names_id;
            },
            function () {
            },
            {scroll: false}
        );
    }

    function verifyDeleteCerts($el, numCerts, legal_entity_names_id) {
        var dialog = new MooDialog({
                closeButton: false,
                size: {
                    width: 550,
                    height: 200
                }
            }),
            $template = $('#delete-with-certs').children().clone();

        $template.find('.j-cancel').click(function () {
            dialog.close();
        });

        $template.find('.j-hide').click(function () {
            window.location.href = 'hide.php?hide_entity_name=1&legal_entity_names_id=' + legal_entity_names_id;
        });

        $template.find('.j-delete').click(function () {
            window.location.href = 'delete.php?legal_entity_names_id=' + legal_entity_names_id;
        });

        dialog.toElement().addClass('deleteWithCerts');
        dialog.setContent($template[0]);
        dialog.open();
    }

    $(function () {
        $('.j-delete').click(verifyDelete)
    });
})(jQuery);