function chkLocationForm(form) {
    var locationIsValid = true;
    $$('p.form-error').destroy();
    var location = $('licensed_locations_full_type').get('value');
    if(location == 'Please Select') {
        locationIsValid = false;
        window.WUC.createErrorMessage('licensed_locations_full_type', 'Please select a location type');
    }
    if(location != 'Please Select' && location != 'Other' && location != 'Corporate Office') {
        if(!$('hotels_num_rooms').get('value')){
            locationIsValid = false;
            window.WUC.createErrorMessage('hotels_num_rooms', 'Please enter the number of rooms');
        }
    }
    locationIsValid &= window.WUC.validateRequired('licensed_locations_name', 'Please enter Location Name');
    locationIsValid &= window.WUC.validateRequired('licensed_locations_address', 'Please enter Address');
    locationIsValid &= window.WUC.validateRequired('licensed_locations_city', 'Please enter City');
    locationIsValid &= window.WUC.validateRequired('licensed_locations_state', 'Please select State');
    locationIsValid &= window.WUC.validateRequired('licensed_locations_zip', 'Please enter Zip code');
    locationIsValid &= window.WUC.validateRequired('licensed_locations_phone', 'Please enter Phone');
    if(!locationIsValid){
        scrollToFirstError()
    }
    return locationIsValid;
}
window.addEvent('domready', function(){
    
    $('save').addEvent('click', function (e) {
        this.disabled = true;
        var form = $('add-hotel-form');
        if (chkLocationForm(form)) {
            form.submit();
        } else {
            this.disabled = false;
            e.preventDefault();
        }
    });
    var locationTypes = {
        'Hotel - Full Service/Resort': 'Hotel',
        'Hotel - Limited Service': 'Hotel',
        'Spa - Destination Spa': 'Hotel',
        'Other': 'Other',
        'Corporate Office': 'Corporate Office'
    }

    function updateFormBasedOnType() {
        var value = $$('#licensed_locations_full_type').get('value'),
            locationType = locationTypes[value];

        $$('#licensed_locations_type').set('value', locationType || '');

        if(locationTypes[value] === 'Hotel'){
            $$('#hotels_num_rooms').setStyle('display', 'inline');
            var $errorTag = $$('#hotels_num_rooms').getNext();
            if ($errorTag.hasClass('error')) {
                $errorTag.setStyle('display', 'block');
            }
            $$('.rooms_msg').setStyle('display', 'none');
        } else {
            $$('#hotels_num_rooms').set('value','').setStyle('display', 'none');
            var $errorTag = $$('#hotels_num_rooms').getNext();
            if ($errorTag.hasClass('error')) {
                $errorTag.setStyle('display', 'none');
            }
            $$('.rooms_msg').setStyle('display', 'inline');
        }
    }

    $$('#hotels_num_rooms').getParent().grab(new Element('span', {text:'Does Not Apply'}).addClass('rooms_msg'));
    updateFormBasedOnType();

    $$('#licensed_locations_full_type').addEvent('change', updateFormBasedOnType);
    

    //$('discount_code_btn').addEvent('click', discount).fireEvent('click');
    if($('discount_code_btn') != null)
        $('discount_code_btn').addEvent('click', discount).fireEvent('click');
});



function discount(e) {
    if (typeof e != 'undefined') {
        e.stop();
    }

    if($('discount_code').get('value') == '' && $('franchise_code').get('value') == '')
    {
        $('discount-wrapper').setStyle('display', 'none');
        $('franchise-discount-wrapper').setStyle('display', 'none');
        $('total-cost').set('html', totalCostString);
        return;
    }
    else if ($('discount_code').get('value') == '') {
        $('discount-wrapper').setStyle('display', 'none');
        $('franchise-discount-wrapper').setStyle('display', 'block');
    }
    else if ($('franchise_code').get('value') == '') {
        $('discount-wrapper').setStyle('display', 'block');
        $('franchise-discount-wrapper').setStyle('display', 'none');
    }

    var request = new Request.JSON({
        url:'/members/account/hotels/edit.php',
        method:'get',
        onSuccess:function (json, text) {

            var error_container = $('franchise_code').getSiblings('.form-error');
            error_container.hide();
            error_container = $('discount_code').getSiblings('.form-error');
            error_container.hide();

            if($('discount_code').get('value') != '' && json.discount_code_discount == undefined
                    && $('franchise_code').get('value') != '' && json.franchise_code_discount == undefined)
            {
                $('total-cost').set('html', totalCostString);
                window.alert('The discount code nor the group code entered is valid.  Please check the values and try again.');
                return;
            }
            else if($('franchise_code').get('value') != '' && json.franchise_code_discount == undefined)
            {
                $('total-cost').set('html', totalCostString);
                window.alert('The group code entered is not valid.  Please check the group code, if you want to use it, and then try again .');
            }
            else if($('discount_code').get('value') != '' && json.discount_code_discount == undefined)
            {
                $('total-cost').set('html', totalCostString);
                window.alert('The discount code entered is not valid.  Please check the discount code, if you want to use it, and then try again .');
            }


            var coupon_discount = json.discount_code_discount;
            var coupon_total = json.discount_code_total;
            var coupon_total_as_number = json.discount_code_total_as_number ==  undefined ? -1 : json.discount_code_total_as_number;
            var coupon_code_id =  json.discount_codes_id ==  undefined ? -1 : json.discount_codes_id;

            var group_discount = json.franchise_code_discount;
            var group_total = json.franchise_code_total;
            var group_total_as_number = json.franchise_code_total_as_number ==  undefined ? -1 : json.franchise_code_total_as_number;
            var group_code_id = json.franchises_id == undefined ? -1 : json.franchises_id;

            if( group_total_as_number >= 0 && coupon_total_as_number >= 0)
            {
                if(coupon_total_as_number < group_total_as_number)
                {
                    $('discount-wrapper').setStyle('display', 'block');
                    $('discount-field').set('html', coupon_discount);
                    $('discount_codes_id').set('value', coupon_code_id);
                    $('franchise_code').set('value', '');
                    $('franchise-discount-wrapper').setStyle('display', 'none');
                    $('franchise-discount-field').set('html', '');
                    $('franchises_id').set('value', -1);
                    $('total-cost').set('value', coupon_total);
                    window.alert('The discount code offers greater savings than the group code.  That discount will be applied instead of the group code discount.');
                }
                else
                {
                    $('discount-wrapper').setStyle('display', 'none');
                    $('discount-field').set('html', '');
                    $('discount_codes_id').set('value', -1);
                    $('discount_code').set('value', '');
                    $('franchise-discount-wrapper').setStyle('display', 'block');
                    $('franchise-discount-field').set('html', group_discount);
                    $('franchises_id').set('value', group_code_id);
                    $('total-cost').set('value', group_total);
                    window.alert('The group code offers greater savings than the discount code.  The group discount will be applied instead of the discount code.');
                }
            }
            else if( group_total_as_number < 0 && coupon_total_as_number < 0)
            {
                $('discount-wrapper').setStyle('display', 'none');
                $('discount-field').set('html', '');
                $('discount_codes_id').set('value', -1);
                $('discount_code').set('value', '');
                $('franchise-discount-wrapper').setStyle('display', 'none');
                $('franchise-discount-field').set('html', '');
                $('franchise_code').set('value', '');
                $('franchises_id').set('value', -1);
            }
            else if( group_total_as_number < 0 )
            {
                $('discount-wrapper').setStyle('display', 'block');
                $('discount-field').set('html', coupon_discount);
                $('discount_codes_id').set('value', coupon_code_id);
                $('franchise_code').set('value', '');
                $('franchise-discount-wrapper').setStyle('display', 'none');
                $('franchise-discount-field').set('html', '');
                $('franchises_id').set('value', -1);
                $('total-cost').set('value', coupon_total);
//                var error_container = $('discount_code').getSiblings('.form-error');
//                if (error_container.length != 0) {
//                    error_container.setStyle('display', 'none');
//                }
            }
            else  if( coupon_total_as_number < 0 )
            {
                $('discount_code').set('value', '');
                $('discount-wrapper').setStyle('display', 'none');
                $('discount-field').set('html', '');
                $('discount_codes_id').set('value', -1);
                $('franchise-discount-wrapper').setStyle('display', 'block');
                $('franchise-discount-field').set('html', group_discount);
                $('franchises_id').set('value', group_code_id);
                $('total-cost').set('value', group_total);

                //new MooDialog.Con('The group code offers greater savings than the discount code.  The group discount will be applied instead of the discount code.');

            }
        },
        onError:function (text) {
            var error_container;

            error_container = $('discount_code').getSiblings('.form-error');
            if($('discount_code').get('value') != '') {
                if (error_container.length == 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('discount_code').getParent());

                }
                error_container.set('html', 'This discount code is invalid');
                error_container.setStyle('display', 'blocklid code');
            } else {
                error_container.hide();
            }

            error_container = $('franchise_code').getSiblings('.form-error');
            if($('franchise_code').get('value') != '') {
                if (error_container.length == 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('franchise_code').getParent());

                }
                error_container.set('html', 'This franchise code is invalid');
                error_container.setStyle('display', 'blocklid code');
            } else {
                error_container.hide();
            }

            $('discount-wrapper').setStyle('display', 'none');
            $('discount-field').set('html', '');
            $('discount_codes_id').set('value', -1);
            $('franchise-discount-wrapper').setStyle('display', 'none');
            $('franchise-discount-field').set('html', '');
            $('franchises_id').set('value', -1);
            $('total-cost').set('value', totalCostString);
        }
    }).get({ajax:'franchise-and-coupon-pricing', discount_code:$('discount_code').get('value'), franchise_code:$('franchise_code').get('value'), total:totalCost});
}