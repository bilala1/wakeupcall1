(function () {
    var $ = jQuery;

    function centerUnder(element, target) {
        var targetLocation = target.offset();

        targetLocation.left += target.width() / 2;
        targetLocation.top += target.height() + 10;

        targetLocation.left -= element.width() / 2;
        element.offset(targetLocation);
    }

    function submitLogin(e) {
        e.preventDefault();

        $('.login-error').text('');

        var login_email = $(e.target.username).val();
        var login_password = $(e.target.password).val();

        $.ajax({
            type: 'POST',
            url: '/members/login.php',
            data: {
                login_email: login_email,
                login_password: login_password,
                remember_me: 0
            },
            dataType: 'json'
        }).then(function (json) {
            if (json.maintenanceMode) {
                $('.login-error').text(json.maintenanceMode);
                return;
            }

            if (json.error) {
                if (json.error == 'Your free trial has expired.') {
                    var members_id = json.members_id;
                    location.href = 'full-membership.php?notice=Your+free+trial+has+expired&members_id=' + members_id;
                } else {
                    $('.login-error').text(json.error);
                }
                return;
            }

            if (typeof json.logged != 'undefined') {
                var redirect = Cookies.get('wakeup_redirect');
                if (redirect) {
                    Cookies.remove('wakeup_redirect');
                }
                window.location.href = json.redirect;
            }
        });
    }

    $('.wuc-login').click(function (e) {
        e.preventDefault();
        var loginForm =
            '<div class="login-form">' +
            '<div class="login-modal">' +
            '<span class="login-error"></span>' +
            '<form>' +
            '<label for="username">Email:</label>' +
            '<input type="text" name="username">' +
            '<label for="password">Password:</label>' +
            '<input type="password" name="password">' +
            '<a href="/forgot-password.php">Forgot Password</a>' +
            '<input type="submit" value="Login">' +
            '<input type="reset" value="Cancel">' +
            '</form>' +
            '</div>' +
            '</div>';
        $('body').append(loginForm);
        centerUnder($('.login-modal'), $('.wuc-login'))
        $('.login-form').submit(submitLogin);
        $('.login-form input[type=reset]').click(function (e) {
            e.preventDefault();
            $('.login-form').remove();
        });
        $('.login-form input[name=username]').focus();
    });
    $('head').append($('<script src="/js/js.cookie.js"></script>'));
})();