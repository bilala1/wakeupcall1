var flash;
var login_email;

window.addEvent('load', function(){
    if(Browser.ie && Browser.version < 7){
        $('memberlogin').getParent().set('html', 'Please upgrade your browser to use this site.');
        return;
    }

    init_flash();

    if($('memberlogin')){
        $('memberlogin').addEvent('submit', do_login);
        /*$$('#login_email, #login_password').addEvent('keyup', function(event){
            if(event.key == 'enter'){
                event.stop();
                do_login(event);
            }
        });*/
    }

    $('login_keycard').addEvent('click', function(evt){
        do_login(evt);
    });
});

function do_login(evt){
    evt.stop();

    login_email = $('login_email').get('value');
    var login_password = $('login_password').get('value');
    var remember_me = 0;
    if($('remember_me').checked){
         remember_me = 1;
    }
    else {
         remember_me = 0;
    }
   // var remember_me = $('remember_me').get('value');

    var req = new Request.JSON({
        method: 'post',
        url: BASEURL + '/members/login.php',
        onSuccess: function(json){
            if(typeof json.error != 'undefined'){
                $('login_error').set('text', json.error);
                flash = new Swiff('incorrect-login.swf', {
                    width:  140,
                    height: 80,
                    container: $('keyCard'),
                    params: {
                        loop: 'false'
                    }
                });
                setTimeout(init_flash, 3200);
                if(json.renewable) {
                    var members_id = json.members_id;
                    setTimeout(function(){
                        location.href = 'full-membership.php?notice=' + encodeURIComponent(json.error) + '&members_id=' + members_id;
                    }, 3600);
                }
            }else{
                //$('login_error').set('html', '&nbsp;');
                if(typeof json.logged != 'undefined'){
                    var pause = 0;
                    if(Browser.Plugins.Flash && (Browser.Plugins.Flash.version != 0)){
                        try{
                            flash.object.Play();
                            pause = 1800;
                        }catch(e){}
                    }

					if (json.show_video == 0) {
						setTimeout(function(){
							var redirect = Cookie.read('wakeup_redirect');
							if(redirect){
								Cookie.dispose('wakeup_redirect');
								window.location.href = redirect;
							}else{
								window.location.href = BASEURL + '/members/index.php';
							}
						}, pause);
					} else {
						setTimeout(function() {
							window.location.href = BASEURL + '/members/startup/video.php';
						}, pause);
					}
                }
            }
        }
    });

    req.post({login_email: login_email, login_password: login_password, remember_me: remember_me});
}

function init_flash(){
    flash = new Swiff('login.swf', {
        width:  140,
        height: 80,
        container: $('keyCard'),
        params: {
            wMode: 'transparent',
            play: 'false',
            loop: 'false'
        }
    });
}
