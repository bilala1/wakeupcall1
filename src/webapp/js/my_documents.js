window.addEvent('domready', function () {
    if($('documents_is_corporate_shared').checked){
             $('show_location').show();
    }
    document.getElementById('documents_is_corporate_shared').addEvent('change', function () {
         if($('documents_is_corporate_shared').checked){
             $('show_location').show();
         }else{
             $('show_location').hide();
         }
    }); 
    $('save').addEvent('click', function () {
     this.disabled = true;
     var form = $('form');
     if (chkDocumentForm(form)) {
         form.submit();
     }else{
         this.disabled = false;
         return false;
     }
    });
});
function chkDocumentForm(form) {
    var documentIsValid = true;
    $$('p.form-error').destroy();
    if (!window.WUC.anySelected('join_licensed_locations[]') && $('documents_is_corporate_shared').checked) {
        documentIsValid = false;
        window.WUC.createErrorMessage($('licensed_locations').getParent('.form-field'), 'Please select at least one location');
    }
    if(!documentIsValid){
        scrollToFirstError()
    }
    return documentIsValid;
}