jQuery(function () {
    var $ = jQuery;
    var categories = [
        {id: 'dashboard', label: 'Dashboard', type: 'number'},
        {id: 'certs', label: 'Certificates', type: 'number'},
        {id: 'claims', label: 'Claims', type: 'number'},
        {id: 'contracts', label: 'Contracts', type: 'number'},
        {id: 'businesses', label: 'Businesses', type: 'number'},
        {id: 'files', label: 'Files', type: 'number'},
        {id: 'library', label: 'Document Library', type: 'number'},
        {id: 'forums', label: 'Forums', type: 'number'},
        {id: 'hr', label: 'HR', type: 'number'},
        {id: 'elaw', label: 'E-Law', type: 'number'},
        {id: 'webinars', label: 'Webinars', type: 'number'},
        {id: 'search', label: 'Search', type: 'number'},
        {id: 'news', label: 'News', type: 'number'},
        {id: 'other', label: 'Other', type: 'number'}
    ];

    $('.j-report-form').on('submit', generateReport);
    $('.datepicker').each(function (index, input) {
        new MooCal(new Element(input), {
            leaveempty: true,
            readonly: false,
            clickout: true
        });
    });

    $('input[name=reportType]').on('change', function () {
        var reportType = $('input[name=reportType]:checked').val();
        $('.j-chartOptions').hide();
        $('.j-chartOptions-' + reportType).show();
    });

    function generateReport(e) {
        e.preventDefault();
        console.log($(".j-report-form").serialize());
        updateCategoryEnablement();
        $.ajax({
            type: "POST",
            url: '/API/admin/daily_summaries.php',
            data: $(".j-report-form").serialize(), // serializes the form's elements.
            success: function (data) {
                data = JSON.parse(data);
                data = data.map(function (summary) {
                    var clean = {
                        moment: moment(summary.daily_summarys_date),
                        date:summary.daily_summarys_date
                    };
                    return categories.reduce(function(clean, category) {
                        clean[category.id] = summary['daily_summarys_category_'+category.id];
                        return clean;
                    }, clean);
                });
                updateReport(data);
            }
        });
    }

    function updateCategoryEnablement() {
        categories.forEach(function(category) {
            category.enabled = !!$('.j-byTime-includeArea[value='+category.id+']:checked').length;
        });
    }

    function updateReport(data) {
        if(!data.length) {
            $('#j-usage-chart').text('No data available - please adjust start/end dates, or select a different account.')
            return;
        }
        switch ($('input[name=reportType]:checked').val()) {
            case 'byTime':
                generateTimeChart(data);
                break;
            case 'byArea':
            default:
                generateAreaChart(data);
                break;
        }
    }

    function getEmptyData() {
        return categories.reduce(function(data, category){
            if(category.enabled) {
                data[category.id] = 0
            }
            return data;
        }, {});
    }

    function addToTotal(total, counts) {
        return categories.reduce(function(total, category){
            if(category.enabled) {
                total[category.id] += counts[category.id] || 0;
            }
            return total;
        }, total || getEmptyData());
    }

    function generateTimeChartRow(dailySummary) {
        var c = [{v: dailySummary.moment.toDate()}];

        categories.reduce(function(c, category){
            if(category.enabled) {
                c.push({v: dailySummary[category.id]});
            }
            return c;
        }, c);

        return {c: c};
    }

    function getColsForEnabledCategories() {
        var cols=[{id: 'label', label: 'Date', type: 'date'}];

        cols = categories.reduce(function(cols, category){
            if(category.enabled) {
                cols.push(category);
            }
            return cols;
        }, cols);

        return cols;
    }

    function generateTimeChart(data) {
        var tempMap = {}
        var groupBy = $('input[name=byTimeGroupType]:checked').val();

        // need an object w/keys as date, and values as getEmptyData() with sums
        var totals = data.reduce(function (totals, summary) {
            //set moment to correct group
            summary.moment = summary.moment.startOf(groupBy);
            var dayTotals = tempMap[summary.moment];
            if (!dayTotals) {
                dayTotals = getEmptyData();
                totals.push(dayTotals);
                dayTotals.moment = summary.moment;
                tempMap[summary.moment] = dayTotals;
            }
            dayTotals = addToTotal(dayTotals, summary);
            return totals;
        }, []);

        totals.sort(function (a, b) {
            return a.moment - b.moment;
        })

        var chartData = {
            cols: getColsForEnabledCategories(),
            rows: totals.map(generateTimeChartRow)
        };
        var data = new google.visualization.DataTable(chartData);

        var format;
        switch(groupBy) {
            case 'month':
                format= 'MMM yyyy';
                break;
            case 'week':
            case 'day':
                default:
                format = 'M/d/yyyy';
                break;
        }

        // https://developers.google.com/chart/interactive/docs/gallery/areachart#stacking-areas
        var options = {
            title: "Site Usage over Time",
            isStacked: $('input[name=byTimeStackType]:checked').val(),
            hAxis: {
                format: format
            }
        };
        var chart = new google.visualization.AreaChart(document.getElementById('j-usage-chart'));
        google.visualization.events.addListener(chart, 'error', onError);

        chart.draw(data, options);
    }

    function onError(id, message) {
        console.error(id + ": " + message);
    }

    function generateAreaChartRowsForEnabledCategories(totals) {
        return categories.reduce(function(rows, category) {
            if(category.enabled) {
                rows.push({c: [{v: category.label}, {v: totals[category.id]}]});
            }
            return rows;
        }, []);
    }

    function generateAreaChart(data) {
        var totals = data.reduce(addToTotal);
        var chartData = {
            cols: [
                {id: 'label', label: 'Area of Site', type: 'string'},
                {id: 'total', label: 'Total Hits', type: 'number'}
            ],
            rows: generateAreaChartRowsForEnabledCategories(totals)
        };
        var data = new google.visualization.DataTable(chartData);

        var options = {
            title: "Site Usage by Area"
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('j-usage-chart'));
        google.visualization.events.addListener(chart, 'error', onError);

        chart.draw(data, options);
    }
});
