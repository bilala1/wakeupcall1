jQuery(function () {
    var $ = jQuery;

    document.getElementById('account_admin_help').addEvent('click', function () {
        var reqDialog = new MooDialog.Request('/members/account/hotels/members/account_admin_help.php', null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: 450,
                height: 300
            }
        });

        reqDialog.setRequestOptions({
            onRequest: function () {
                reqDialog.setContent('loading...');
            }
        }).open();

        return false;
    });
    
    document.getElementById('regular_user_help').addEvent('click', function () {
        var reqDialog = new MooDialog.Request('/members/account/hotels/members/regular_user_help.php', null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: 450,
                height: 300
            }
        });

        reqDialog.setRequestOptions({
            onRequest: function () {
                reqDialog.setContent('loading...');
            }
        }).open();

        return false;
    });
    
    function onMembersTypeChange() {
        var isAdmin = 'admin' === $('input[name="members_type"]:checked').val();
        $('#adminPerms').toggle(isAdmin);
        $('#accountPerms').toggle(!isAdmin);
        $('#locationPerms').toggle(!isAdmin);
    }

    $('input[name="members_type"]').change(onMembersTypeChange);
    onMembersTypeChange();

    function onLocationAdminChange(e) {
        var $box = $(this),
            isAdmin = !!$box.prop('checked'),
            locId = $box.attr('data-location'),
            $perms = $('.locationPerm[data-location="' + locId + '"][data-sensitive="false"]');

        $perms.prop('disabled', isAdmin);
        $perms.prop('checked', isAdmin);
    }
    $('input.locationAdmin').change(onLocationAdminChange);
});