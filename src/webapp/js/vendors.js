window.addEvent('domready', function () {
    $('save').addEvent('click', function (e) {
        this.disabled = true;
        var form = $('editvendor');
        if (chkVendorForm(form)) {
            form.submit();
        } else {
            this.disabled = false;
            e.preventDefault();
        }
    });

    $$('.emailHistory').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog('/members/my-documents/certificates/emails_history/certificate_history.php?certificates_id='+id,450,300);

        return false;
    });

    $$('.history').addEvent('click', function(){
        var parts = this.get('id').split('_');

        var id = parts[1];

        openDialog('/members/my-documents/certificates/history.php?certificates_id='+id,450,300);

        return false;
    });
});

function PreviewTemplate($email_id)
{
    openDialog('/members/my-documents/certificates/emails_history/preview_popup.php?id=' + $email_id, 800, 500);

    return false;
}

function chkVendorForm(form) {
    var vendorIsValid = true;
    $$('p.form-error').destroy();
    if (!window.WUC.anySelectedOrNoInput('join_licensed_locations[]')) {
            vendorIsValid = false;
            window.WUC.createErrorMessage($('licensed_locations').getParent(), 'Please select at least one location');
        }
    vendorIsValid &= window.WUC.validateRequired('vendors_name', 'Please enter Vendors Name');
    vendorIsValid &= window.WUC.validateRequired('vendors_contact_name', 'Please enter Contact Name');
    vendorIsValid &= window.WUC.validateRequired('vendors_email', 'Please enter Email');
    if(!vendorIsValid){
        scrollToFirstError()
    }
    return vendorIsValid;
}