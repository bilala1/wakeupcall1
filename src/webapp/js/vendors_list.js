(function ($) {

    'use strict';

    function verifyDelete(event) {
        event.preventDefault();

        var $el = $(event.target),
            numCerts = $el.data('numcerts'),
            vendorId = $el.data('vendorid');
        if (numCerts == 0) {
            verifyDeleteNoCerts($el, vendorId);
        } else {
            verifyDeleteCerts($el, numCerts, vendorId);
        }
    }

    function verifyDeleteNoCerts($el, vendorId) {
        var message = $el.attr('title') || 'Are you sure you want to delete this?';

        new MooDialog.Confirm(message, function () {
                window.location.href = 'delete.php?vendors_id=' + vendorId;
            },
            function () {
            },
            {scroll: false}
        );
    }

    function verifyDeleteCerts($el, numCerts, vendorId) {
        var dialog = new MooDialog({
                closeButton: false,
                size: {
                    width: 550,
                    height: 200
                }
            }),
            $template = $('#delete-with-certs').children().clone();

        $template.find('.j-cancel').click(function () {
            dialog.close();
        });

        $template.find('.j-hide').click(function () {
            window.location.href = 'hide.php?hide_vendor=1&hide_certs=1&vendors_id=' + vendorId;
        });

        $template.find('.j-delete').click(function () {
            window.location.href = 'delete.php?vendors_id=' + vendorId;
        });

        dialog.toElement().addClass('deleteWithCerts');
        dialog.setContent($template[0]);
        dialog.open();
    }

    $(function () {
        $('.j-delete').click(verifyDelete)
    });
    window.addEvent('domready', function () {
        $$('.history').addEvent('click', function () {
            var parts = this.get('id').split('_');

            var id = parts[1];
            
            openDialog(BASEURL + '/members/vendors/history.php?vendors_id=' + id,450,300);

            return false;
        });
    });
})(jQuery);