<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/22/15
 * Time: 10:35 AM
 */


class ActiveMemberInfo
{

    /** Marked special with underscore because most times you probably want to check if this is a multi-location
     * user, not if they're an admin.  Also look into UserPermissions for object access.  The main use of this
     * function is to see if users can check an "All locations" box, or to check that locations can view/edit.
     */
    static function _IsAccountAdmin()
    {
        return $_SESSION['_is_account_admin'];
    }

    /** Marked special with underscore because most times you probably want to check if this is a multi-location
     * user or an account admin, not if they're the main admin.  Also look into UserPermissions for object access.  The main use of this
     * function is to see if users can check an "All locations" box, or to check that locations can view/edit.
     */
    static function _IsMainAccountAdmin()
    {
        return $_SESSION['_is_main_account_admin'];
    }

    static function IsChildAccount()
    {
        // true for accounts with a parent, false for others.
        return !!$_SESSION['parent_account_id'];
    }

    static function IsAccountMultiLocation()
    {
        // true for corporations/parents, false for singles.
        return $_SESSION['_accounts_type'] != 'single';
    }

    static function IsParentAccount()
    {
        // true for parents, false for others.
        return $_SESSION['_accounts_type'] == 'parent';
    }

    static function IsUserMultiLocation()
    {
        // determine if the user will be able to view multiple locations or just one.
        // True if account is multi-location and user is admin or has access to multiple locations
        // Could consider caching this at some point, at least per request
        return ActiveMemberInfo::IsAccountMultiLocation() &&
        (ActiveMemberInfo::_IsAccountAdmin() || 1 < count(UserPermissions::AllObjectIdsForUser('licensed_locations')));
    }

    static function SingleLocationUserLocationId()
    {
        if (ActiveMemberInfo::IsUserMultiLocation()) {
            throw new Exception('Tried to get single location id for multi-location user');
        }
        if (!$_SESSION['licensed_locations_id']) {
            $locations = UserPermissions::AllObjectIdsForUser('licensed_locations');
            $_SESSION['licensed_locations_id'] = $locations[0];
        }
        return $_SESSION['licensed_locations_id'];
    }

    static function GetMemberId()
    {
        return isset($_SESSION['members_id']) ? $_SESSION['members_id'] : null;
    }

    static function GetMember()
    {
        return members::get_by_id(ActiveMemberInfo::GetMemberId());
    }

    static function GetAccountId()
    {
        return isset($_SESSION['accounts_id']) ? $_SESSION['accounts_id'] : null;
    }

    static function GetParentAccountId()
    {
        return $_SESSION['parent_account_id'];
    }

    static function GetAccount()
    {
        $db = mysqli_db::init();
        return $db->fetch_one(
            'SELECT * from accounts a
             WHERE a.accounts_id = ?'
            , array(ActiveMemberInfo::GetAccountId()));
    }

    /**
     * Gets the main admin of the account of the current user.  Used for billing, etc.
     */
    static function GetAccountAdmin()
    {
        return accounts::getMainAdmin(ActiveMemberInfo::GetAccountId());
    }
}