<?php

final class Formatting
{
    final public static function emailStatus($sentEmail) {
        switch($sentEmail['sent_emails_history_status']) {
            case 'success':
                return 'Success';
            case 'pending':
                return 'Pending';
            case 'fail':
                return 'Delivery Failed: ' . $sentEmail['sent_emails_history_status_reason'];
            default:
                return 'Unknown';
        }
    }

    final public static function dbKeysToJavascript($array)
    {
        $output = array();
        foreach ($array as $key => $value) {
            if (is_string($key)) {
                $key = preg_replace_callback('/_([a-z])/',
                    function ($matches) {
                        return strtoupper($matches[1]);
                    },
                    $key);
            }
            if (is_array($value)) {
                $value = self::dbKeysToJavascript($value);
            }
            $output[$key] = $value;
        }
    }

    final public static function date($stringDate, $emptyText = '') {
        if($stringDate == null || $stringDate == '') {
            return $emptyText;
        }
        return date('n/j/Y', strtotime($stringDate));
        // check for '1/1/1972'?
    }
}