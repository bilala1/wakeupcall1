<?php

class MooDialogClass
{
    ///Opening tags for a fixed size moo dialog
    public static function StartFixedMooDialog($dialogTitle)
    {
        return '<span id="MooDialogFixed"><div class="fixedDialogDiv"><div id="content" style="height: 100%; width: 100%">'.
               '<table align="center" class="dialogContentTable">'.      
                ($dialogTitle ? '<tr><td class="dialogTitleCell"><h3 class="clear">'.$dialogTitle.'</h3></td>' : '').
               '</tr><tr><td valign="Top" class="dialogContentCell">';     
    }
    
    ///Opening tags for a fixed size moo dialog
    public static function EndFixedMooDialog()
    {
        return ' </td></tr></table></div></div></span>';
    }
    
}