<?php
include('PHPExcel.php');
include('../http.php');

class columns
{
    public $COL_MIN = 0;
    public $COL_LOCATION = 0;
    public $COL_DOI = 1;
    public $COL_CLAIMANT = 2;
    public $COL_INJURY_TYPE = 3;
    public $COL_BODY_PART = 4;
    public $COL_DEPT = 5;
    public $COL_STATUS = 6;
    public $COL_DATE_SUBMITTED = 7;
    public $COL_DATE_CLOSED = 8;
    public $COL_AMOUNT_PAID = 9;
    public $COL_AMOUNT_RESERVED = 10;
    public $COL_TOTAL_INCURRED = 11;
    public $COL_NOTES = 12;
    public $COL_HISTORY = 13;
    public $COL_MAX = 13;

    public function num_to_letter($numeric)
    {

    }
}
function write_header($worksheetObj)
{
    $cols = new columns();

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_LOCATION, 1 , "Location");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_DOI,1, "DOI");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_CLAIMANT,1, "Claimant");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_INJURY_TYPE,1, "Injury Type");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_BODY_PART,1, "Body Part");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_DEPT,1, "Dept.");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_STATUS,1, "Status");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_DATE_SUBMITTED,1, "Date Submitted");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_DATE_CLOSED,1, "Date Closed");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_AMOUNT_PAID,1, "Amount Paid");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_AMOUNT_RESERVED,1, "Amount Reserved");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_TOTAL_INCURRED,1, "Total Incurred");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_NOTES,1, "Notes");

    $worksheetObj->setCellValueByColumnAndRow($cols->COL_HISTORY,1, "History");

    $worksheetObj->getStyle("A1:N1")->getFont()->setBold(true);
    $worksheetObj->getStyle("A1:N1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $worksheetObj->getStyle("A1:N1")->getFill()->getStartColor()->setARGB('FFFFFF00');
}

$objPHPExcel = new PHPExcel();

$worksheetObj = $objPHPExcel->getSheet(0);


write_header($worksheetObj);


$outputFileName = @tempnam(PHPExcel_Shared_File::sys_get_temp_dir(), 'phpxltmp') .".xls";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$objWriter->save($outputFileName);


header('Content-Type: application/xls');
header('Content-Disposition: attachment; filename=' . "report.xls" . ';');
header('Content-Length: ' . filesize($outputFileName));

readfile($outputFileName);
?>