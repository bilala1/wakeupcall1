<?php

/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/22/15
 * Time: 10:35 AM
 */
class UserPermissions
{

    private static function update_config($config, $object_type)
    {
        if($object_type === 'vendors') {
            $config['permission'] = 'certificates';
        }

        if($object_type === 'vendors' ||
            $object_type === 'contracts' ||
            $object_type === 'carriers' ||
            $object_type === 'members_x_documents' ||
            $object_type === 'business_entities') {
            $config['multiLocation'] = true;
        }
        return $config;
    }

    private static function IsMainAccountAdmin()
    {
        return $_SESSION['_is_main_account_admin'];
    }

    private static function IsAccountAdmin()
    {
        return $_SESSION['_is_account_admin'];
    }

    public static function AccountCanAccessScreen($screenType, $accountId) {
        $db = mysqli_db::init();

        if(!UserPermissions::IsSiteFeatureEnabled($screenType)) {
            return false;
        }

        //If the screen type can be turned on/off for the account, check that first
        $accountFeature = $db-> fetch_one(
            'SELECT af.*, COALESCE(acc.accounts_x_account_features_enabled, 1) AS accounts_x_account_features_enabled FROM account_features af
              LEFT JOIN accounts_x_account_features acc ON account_features_id = join_account_features_id AND acc.join_accounts_id = ?
              WHERE account_features_type = ?;',
            array($accountId, $screenType));
        if($accountFeature && array_key_exists('account_features_type', $accountFeature)) {
            //The screen can be toggled per account - is it disabled?
            if(0 == $accountFeature['accounts_x_account_features_enabled']) {
                return false;
            }
        }
        return true;
    }

    public static function IsSiteFeatureEnabled($siteFeatureCode) {
        $db = mysqli_db::init();

        $siteFeatureActive = $db->fetch_singlet(
            'SELECT site_features_active from site_features where site_features_code = ?;',
            array($siteFeatureCode));

        return $siteFeatureActive !== 0;
    }

    public static function UserCanAccessScreen($screen_type)
    {
        if ($screen_type == 'dashboard') {
            return UserPermissions::UserCanAccessScreen('claims') ||
                UserPermissions::UserCanAccessScreen('certificates') ||
                UserPermissions::UserCanAccessScreen('contracts') ||
                UserPermissions::UserCanAccessScreen('business_entities');
        }

        $db = mysqli_db::init();

        //If the screen type can be turned on/off for the account, check that first
        if(!UserPermissions::AccountCanAccessScreen($screen_type, ActiveMemberInfo::GetAccountId())) {
            return false;
        }
        //If child account also check parent permissions
        if(ActiveMemberInfo::IsChildAccount()) {
            if(!UserPermissions::AccountCanAccessScreen($screen_type, ActiveMemberInfo::GetParentAccountId())) {
                return false;
            }
        }

        //Main account admins have all permissions
        if (UserPermissions::IsMainAccountAdmin()) {
            return true;
        }

        //Is it an admin level permission?
        $member_access_levels = $db->fetch_one(
            "SELECT mal.members_access_levels_id, ma.join_members_id
             FROM members_access_levels mal
                LEFT JOIN members_access ma ON join_members_access_levels_id = members_access_levels_id
                AND ma.join_members_id = ?
             WHERE
                mal.members_access_levels_type = ?
                AND mal.members_access_levels_scope = 'admin'"
            , array(ActiveMemberInfo::GetMemberId(), $screen_type)
        );
        // We'll get a record here if it is an admin level screen, regardless of permissions
        if ($member_access_levels) {
            // It is account level, coerce presence of members id to boolean
            return !!$member_access_levels['join_members_id'];
        }
        
        //Is it an account level screen?
        $member_access_levels = $db->fetch_one(
            "SELECT mal.members_access_levels_id, ma.join_members_id, mal.members_access_levels_sensitive
             FROM members_access_levels mal
                LEFT JOIN members_access ma ON join_members_access_levels_id = members_access_levels_id
                AND ma.join_members_id = ?
             WHERE
                mal.members_access_levels_type = ?
                AND mal.members_access_levels_scope = 'account'"
            , array(ActiveMemberInfo::GetMemberId(), $screen_type)
        );

        // We'll get a record here if it is an account level screen, regardless of permissions
        if ($member_access_levels) {

            //If admin, allow them in if it's sensitive and they have it, or if it's not sensitive
            if (UserPermissions::IsAccountAdmin()) {
                if ($member_access_levels['members_access_levels_sensitive'] == 1) {
                    return !!$member_access_levels['join_members_id'];
                }
                return true;
            }

            // It is account level, coerce presence of members id to boolean
            return !!$member_access_levels['join_members_id'];
        }

        // At this point admins get all permissions
        if (UserPermissions::IsAccountAdmin()) {
            return true;
        }
        
        // Check for locationAdmin (if not sensitive) or the specific permission
        $licensed_locations_x_member_access = $db->fetch_one(
            "SELECT 1
             FROM members_access_levels mal
                INNER JOIN members_access_levels mal2 on mal2.members_access_levels_type = ?
                INNER JOIN licensed_locations_x_members_access ma ON mal.members_access_levels_id = join_members_access_levels_id
                INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
             WHERE
                ma.join_members_id = ?
                AND licensed_locations_delete_datetime IS NULL
                AND (mal.members_access_levels_type = ? or (mal.members_access_levels_type = 'locationAdmin' and mal2.members_access_levels_sensitive = 0))
                AND mal2.members_access_levels_scope = 'location'"
            , array($screen_type, ActiveMemberInfo::GetMemberId(), $screen_type)
        );
        if ($licensed_locations_x_member_access) {
            return true;
        }

        return false;
    }

    static function LocationIdsWithPermission($permission, $config = array())
    {
        $config = UserPermissions::update_config($config, $permission);
        if (UserPermissions::IsAccountAdmin()) {
            return UserPermissions::AllLicensedLocationsIdsForUser($config);
        } else {
            if($config['permission']) {
                $permission = $config['permission'];
            }
            $db = mysqli_db::init();
            $ids = $db->fetch_singlets(
                "SELECT DISTINCT llxm.join_licensed_locations_id
                 FROM licensed_locations_x_members_access llxm
                 INNER JOIN members_access_levels mal ON mal.members_access_levels_id = join_members_access_levels_id
                 INNER JOIN members_access_levels mal2 on mal2.members_access_levels_type = ?
                 INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
                 WHERE llxm.join_members_id = ?
                    AND licensed_locations_delete_datetime IS NULL
                    AND (mal.members_access_levels_type = ? or (mal.members_access_levels_type = 'locationAdmin' and mal2.members_access_levels_sensitive = 0))"
                , array($permission, ActiveMemberInfo::GetMemberId(), $permission)
            );
            return $ids;
        }
    }

    static function UserCanAccessObject($object_type, $object_or_id, $config = array())
    {
        $config = UserPermissions::update_config($config, $object_type);
        //If user is account admin, just check the account id


        //If user is location admin, check location id


        //If user is not location admin, check permissions and location id

        //Quick hack that is not as performant - get all the IDs and verify it's in the array
        if (is_array($object_or_id)) {
            $object_or_id = $object_or_id[$object_type . '_id'];
        }
        return in_array($object_or_id, UserPermissions::AllObjectIdsForUser($object_type, $config));
    }

    // For the time being, this function assumes that general access has already been checked via AllObjectIdsForUser or a similar method.
    static function UserObjectPermissions($object_type, $object_or_id, $config = array())
    {
        $config = UserPermissions::update_config($config, $object_type);
        //If user is account admin, they're good to go.
        if (UserPermissions::IsAccountAdmin()) {
            return array('view' => true, 'edit' => true, 'delete' => true);
        }

        if($object_type == 'carriers') {
            //carriers are special, they have permissions on a location by location basis.
            return UserPermissions::UserCarrierPermissions($object_or_id, $config);
        }

        //If user is not account admin, need to verify location_can_view and location_can_edit, if they exist.
        //For delete, if it is a multiLocation object, need to verify that it only belongs to one location.  In the future,
        //when everyone can be working as all of their locations, will need to update that so they can delete if they have
        //access to every location that the object is assigned to.

        $db = mysqli_db::init();

        //If they didn't pass in the object, grab it.
        if (!is_array($object_or_id)) {
            $query = "SELECT * FROM " . $object_type . " WHERE " . $object_type . "_id = ?";
            $object_or_id = $db->fetch_one($query, array($object_or_id));
        }

        $perms = array();
        $key = $object_type . '_location_can_view';
        if (array_key_exists($key, $object_or_id)) {
            $perms['view'] = $object_or_id[$key] == 1;
        } else {
            $perms['view'] = true;
        }
        $key = $object_type . '_location_can_edit';
        if (array_key_exists($key, $object_or_id)) {
            $perms['edit'] = $object_or_id[$key] == 1;
        } else {
            $perms['edit'] = true;
        }
        if ($config['multiLocation']) {
            //TODO ACCOUNTS Update this to check if user has permission to all locations for this object
            if ($perms['edit']) {
                $location_count = $db->fetch_one(
                    "SELECT COUNT(1) as licensed_locations_count
                        FROM " . $object_type . "_x_licensed_locations xll
                        WHERE join_" . $object_type . "_id = ?", array($object_or_id[$object_type . '_id']));
                $perms['delete'] = $location_count['licensed_locations_count'] == 1;
            } else {
                $perms['delete'] = false;
            }
        } else {
            $perms['delete'] = $perms['edit'];
        }

        return $perms;
    }

    static function UserCarrierPermissions($object_or_id, $config) {
        $db = mysqli_db::init();

        if (is_array($object_or_id)) {
            $object_or_id = $object_or_id['carriers_id'];
        }
        $in_clause = strings::CreateInClauseForIds(UserPermissions::LocationIdsWithPermission('claims'), 'join_licensed_locations');

        $query = "SELECT *,
         (SELECT COUNT(1) from licensed_locations_x_carriers
          WHERE join_carriers_id = carriers_id
            AND ". $in_clause .") as carriers_location_can_view,
         (SELECT COUNT(1) from licensed_locations_x_email_recipients
          WHERE join_carriers_id = carriers_id
            AND ". $in_clause .") as recipients_location_can_view,
         (SELECT COUNT(1) from licensed_locations_x_carriers
          WHERE location_can_edit = 1
            AND join_carriers_id = carriers_id
            AND ". $in_clause .") as carriers_location_can_edit,
         (SELECT COUNT(1) from licensed_locations_x_email_recipients
          WHERE location_can_edit = 1
            AND join_carriers_id = carriers_id
            AND ". $in_clause .") as recipients_location_can_edit
         FROM carriers WHERE carriers_id = ?";
        $object_or_id = $db->fetch_one($query, array($object_or_id));

        $perms = array();
        if ($object_or_id['carriers_location_can_view'] > 0 || $object_or_id['recipients_location_can_view'] > 0 ) {
            $perms['view'] = true;
        } else {
            $perms['view'] = false;
        }
        
        if ($object_or_id['carriers_location_can_edit'] > 0 || $object_or_id['recipients_location_can_edit'] > 0 ) {
            $perms['edit'] = true;
        } else {
            $perms['edit'] = false;
        }
            //TODO ACCOUNTS Update this to check if user has permission to all locations for this object
            if ($perms['edit'] && $object_or_id['carriers_all_locations'] != 1) {
                $carrier_location_count = $db->fetch_singlet(
                    "SELECT COUNT(1)
                        FROM licensed_locations_x_carriers xll
                        WHERE join_carriers_id = ?", array($object_or_id['carriers_id']));
                $recipient_location_count = $db->fetch_singlet(
                    "SELECT COUNT(1)
                        FROM licensed_locations_x_email_recipients xll
                        WHERE join_carriers_id = ?", array($object_or_id['carriers_id']));
                $perms['delete'] = ($carrier_location_count + $recipient_location_count) == 1;
            } else {
                $perms['delete'] = false;
            }

        return $perms;
    }

    static function AllObjectIdsForUser($object_type, $config = array())
    {
        $config = UserPermissions::update_config($config, $object_type);

        if ($object_type == 'licensed_locations') {
            return UserPermissions::AllLicensedLocationsIdsForUser($config);
        }

        if ($object_type == 'members') {
            return UserPermissions::AllManageableMembersIdsForUser($config);
        }

        $obj_id = $object_type . "_id";
        $query = "SELECT " . $obj_id . " FROM " . $object_type;

        if ($config['noLocation']) {
            $wheres = array("join_accounts_id = ?");
            $params = array($_SESSION['accounts_id']);
        } else {
            // sad hack... for shared docs we just want all the locations they are associated with, because
            // it's a view thing not actually the ability to share documents.
            if($object_type == 'members_x_documents') {
                $location_ids = self::AllLicensedLocationsIdsForUser($config);
            } else {
                $location_ids = self::LocationIdsWithPermission($object_type, $config);
            }
            $location_clause = strings::CreateInClauseForIds($location_ids, 'join_licensed_locations');
            if ($config['multiLocation']) {
                $wheres[] =
                    "(
                        EXISTS(SELECT 1 FROM " . $object_type . "_x_licensed_locations
                        WHERE join_" . $object_type . "_id = " . $object_type . "_id
                        AND " . $location_clause . ")
                     OR
                       (join_accounts_id = ? AND " . $object_type . "_all_locations = 1)
                     )";
                $params = array(ActiveMemberInfo::GetAccountId());
            } else {
                $wheres[] = $location_clause;
            }
        }

        if (!UserPermissions::IsAccountAdmin()) {
            if ($config['checkView']) {
                $wheres[] = $object_type . "_location_can_view = 1";
            }
            if ($config['checkEdit']) {
                $wheres[] = $object_type . "_location_can_edit = 1";
            }
        }

        $query .= strings::where($wheres);
        $db = mysqli_db::init();
        $objects = $db->fetch_all($query, $params);
        $ids = array();
        foreach ($objects as $object) {
            $ids[] = $object[$obj_id];
        }

        return $ids;
    }

    private static function AllLicensedLocationsIdsForUser($config = array())
    {

        $query = "SELECT DISTINCT licensed_locations_id FROM licensed_locations";

        if (UserPermissions::IsAccountAdmin()) {
            $wheres = array("join_accounts_id = ?");
            $params = array($_SESSION['accounts_id']);
        } else {
            $query .= ' INNER JOIN licensed_locations_x_members_access ll on join_licensed_locations_id = licensed_locations_id';
            $wheres = array("ll.join_members_id = ?");
            $params = array(ActiveMemberInfo::GetMemberId());
        }

        $wheres[] = 'licensed_locations_delete_datetime IS NULL';

        $query .= strings::where($wheres);
        $db = mysqli_db::init();
        $ids = $db->fetch_singlets($query, $params);
        return $ids;
    }

    private static function AllManageableMembersIdsForUser($config = array())
    {
        $member_ids = array();
        if (UserPermissions::UserCanAccessScreen('manageUsers')) {
            $db = mysqli_db::init();
            $location_ids = UserPermissions::LocationIdsWithPermission('manageUsers');
            $in_clause = "(" . implode(', ', $location_ids) . ")";
            if(ActiveMemberInfo::_IsAccountAdmin()) {
                $member_ids = $db->fetch_singlets('
                    SELECT members_id
                    FROM members
                    WHERE members_type = \'user\' AND join_accounts_id = ?
                    ', array(ActiveMemberInfo::GetAccountId()));
            } else {
                $member_ids = $db->fetch_singlets('
                    SELECT members_id
                    FROM members m
                    WHERE members_type = \'user\'
                        AND EXISTS
                        (SELECT 1 FROM licensed_locations_x_members_access as s
                         INNER JOIN licensed_locations on join_licensed_locations_id = licensed_locations_id
                         WHERE m.members_id = s.join_members_id
                           AND licensed_locations_delete_datetime IS NULL
                           AND s.join_licensed_locations_id in ' . $in_clause . '
                           )
                    ', array());
            }

            if (UserPermissions::UserCanAccessScreen('manageAdmins')) {
                $member_ids = array_merge($member_ids,
                    $db->fetch_singlets('
                        SELECT members_id
                        FROM members
                         WHERE members.members_type = \'admin\'
                         AND members.join_accounts_id = ?
                        ', array(ActiveMemberInfo::GetAccountId()))
                );
            }
        }
        return $member_ids;
    }

    static function CreateInClause($object_type, $config = array())
    {
        $config = UserPermissions::update_config($config, $object_type);

        //TODO ACCOUNTS these should always be ints, so we don't need params right?
        $ids = UserPermissions::AllObjectIdsForUser($object_type, $config);
        return strings::CreateInClauseForIds($ids, $object_type);
    }

    static function AllPermissionTypesForUser($members_id)
    {
        if (!$members_id) {
            $members_id = ActiveMemberInfo::GetMemberId();
        }

        $db = mysqli_db::init();

        $accountId = $db->fetch_singlet('select join_accounts_id from members where members_id = ?',
            array($members_id));

        //get user's main admin/admin status
        $is_main_admin = false;
        $is_admin = !!$db->fetch_one("select 1 from members where members_type = 'admin' and members_id = ?",
            array($members_id));
        if ($is_admin) {
            $is_main_admin = !!$db->fetch_one("select 1 from accounts where join_members_id = ?", array($members_id));
        }

        $user_access = $db->fetch_singlets('
            SELECT distinct members_access_levels_type FROM members_access
            INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
            WHERE join_members_id = ?
            ', array($members_id));

        $user_access = array_merge($user_access, $db->fetch_singlets('
            SELECT distinct members_access_levels_type FROM licensed_locations_x_members_access
            INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
            WHERE join_members_id = ?
            ', array($members_id)));

        if (array_search('locationAdmin', $user_access) !== false) {
            $user_access = array_merge($user_access,
                $db->fetch_singlets("SELECT distinct members_access_levels_type FROM members_access_levels WHERE members_access_levels_scope = 'location' AND members_access_levels_sensitive = 0",
                    array()));
        }
        if ($is_admin) {
            $user_access = array_merge($user_access,
                $db->fetch_singlets("SELECT distinct members_access_levels_type FROM members_access_levels WHERE members_access_levels_scope in('account', 'location') and members_access_levels_sensitive = 0",
                    array()));
        }
        if ($is_main_admin) {
            $user_access = array_merge($user_access,
                $db->fetch_singlets("SELECT distinct members_access_levels_type FROM members_access_levels",
                    array()));
        }

        $finalPermissions = array();

        foreach ($user_access as $access) {
            if (false === array_search($access, $finalPermissions)
                && UserPermissions::AccountCanAccessScreen($access, $accountId)
            ) {
                $finalPermissions[] = $access;
            }
        }

        return $finalPermissions;
    }

    static function AllPermissionsForUser($members_id, $location_ids)
    {
        $user_access_by_type = array('admin' => array(), 'account' => array(), 'location' => array());
        if(!$members_id) {
            return $user_access_by_type;
        }

        $db = mysqli_db::init();
        //get user's main admin/admin status
        $is_main_admin = false;
        $is_admin = !!$db->fetch_one("select 1 from members where members_type = 'admin' and members_id = ?", array($members_id));
        if ($is_admin) {
            $is_main_admin = !!$db->fetch_one("select 1 from accounts where join_members_id = ?", array($members_id));
        }
        $location_in_clause = "(" . implode(', ', $location_ids) . ")";

        $user_access = $db->fetch_all('
            SELECT * FROM members_access
            INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
            WHERE join_members_id = ?
            ', array($members_id));

        if ($is_admin) {
            $user_access = array_merge($user_access, $db->fetch_all("SELECT * FROM members_access_levels WHERE members_access_levels_scope = 'account' and members_access_levels_sensitive = 0", array()));
        }
        if ($is_main_admin) {
            $user_access = array_merge($user_access, $db->fetch_all("SELECT * FROM members_access_levels WHERE members_access_levels_scope = 'admin'", array()));
        }

        foreach ($user_access as $access) {
            if($access['members_access_levels_scope'] == 'account' && $access['members_access_levels_sensitive'] == 1){
                $access['members_access_levels_scope'] = 'sensitive';
            }
            $user_access_by_type[$access['members_access_levels_scope']][$access['members_access_levels_type']] = $access['members_access_levels_id'];
        }

        if ($is_admin) {
            $user_location_access = $db->fetch_all('
                SELECT members_access_levels_type, members_access_levels_id, licensed_locations_id as join_licensed_locations_id FROM members_access_levels
                INNER JOIN licensed_locations ON licensed_locations_id IN ' . $location_in_clause . '
                WHERE members_access_levels_type = \'locationAdmin\' OR
                 (members_access_levels_sensitive = 1 AND members_access_levels_scope = \'location\')
                ', array());
        } else {
            $user_location_access = $db->fetch_all('
                SELECT * FROM licensed_locations_x_members_access
                INNER JOIN members_access_levels ON members_access_levels_id = join_members_access_levels_id
                WHERE join_members_id = ?
                AND join_licensed_locations_id IN ' . $location_in_clause . '
                ', array($members_id));
        }
        $all_location_perms = $db->fetch_all("SELECT * FROM members_access_levels WHERE members_access_levels_scope = 'location'", array());

        foreach ($user_location_access as $access) {
            if (!$user_access_by_type['location'][$access['join_licensed_locations_id']]) {
                $user_access_by_type['location'][$access['join_licensed_locations_id']] = array();
            }
            if($access['members_access_levels_type'] === 'locationAdmin') {
                foreach($all_location_perms as $loc_perm) {
                    if($loc_perm['members_access_levels_sensitive'] == 0 || $is_main_admin) {
                        $user_access_by_type['location'][$access['join_licensed_locations_id']][$loc_perm['members_access_levels_type']] = $loc_perm['members_access_levels_id'];
                    }
                }
            } else {
                $user_access_by_type['location'][$access['join_licensed_locations_id']][$access['members_access_levels_type']] = $access['members_access_levels_id'];
            }
        }

        return $user_access_by_type;
    }

    static function UsersWithLocationPermission($permission, $locationId, $config = array())
    {
        $db = mysqli_db::init();
        $config = UserPermissions::update_config($config, $permission);
        if($config['permission']) {
            $permission = $config['permission'];
        }
        $adminMembers = $db->fetch_all('select members_id, members_firstname, members_lastname, members_email, \'Account Admin\' as permission from licensed_locations ll
            join accounts a on (a.accounts_id = ll.join_accounts_id )
            join members m on (m.join_accounts_id= a.accounts_id) 
            where members_type = "admin" and licensed_locations_id = ? order by members_lastname', array($locationId) );
        
        $userMembers = $db->fetch_all(
                "SELECT DISTINCT members_id, m.members_firstname, m.members_lastname, members_email, mal.members_access_levels_name as permission
                 FROM licensed_locations_x_members_access llxm
                 INNER JOIN members_access_levels mal ON mal.members_access_levels_id = join_members_access_levels_id
                 INNER JOIN members_access_levels mal2 on mal2.members_access_levels_type = ?
                 INNER JOIN members m ON (m.members_id = llxm.join_members_id)
                 WHERE llxm.join_licensed_locations_id = ?
                    AND (mal.members_access_levels_type = ? or (mal.members_access_levels_type = 'locationAdmin' and mal2.members_access_levels_sensitive = 0))
                 order by m.members_lastname"
                , array($permission, $locationId, $permission)
            );

        return array_merge($adminMembers, $userMembers);
    }

}