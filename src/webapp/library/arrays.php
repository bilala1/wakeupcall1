<?php
#REVISION: 10
#REVISION: 8

final class arrays
{
    public static function cleankeys($input, $keys)
    {
        foreach($keys as $key)
        {
            unset($input[$key]);
        }   
        
        return $input;
    }
    
    //keeps only the passed in keys
    public static function filter_keys($array, $good_keys){
        /*$good_array = array();
        foreach($good_keys as $good_key){
            $good_array[$good_key] = $array[$good_key];
        }
        
        return $good_array;*/
        
        return array_intersect_key($array, array_flip($good_keys));
    }
    
    /**
     *
     * @param array $array
     * @param string $normalize
     * @return array
     */
    public static function denormalization(&$array, $normalize)
    {
        $obj = array();
        $keys = array_keys($array);
        foreach($keys as $key)
        {
            $normalized = $normalize . '_';
            $normalized_length = strlen($normalized);
            $pretension = substr($key, 0, $normalized_length);
            if($pretension == $normalized)
            {
                $key_length = strlen($key);
                $obj_key = substr($key, $normalized_length, $key_length);
                $obj[$obj_key] = &$array[$key];
            }
        }
        return $obj;
    }

    /**
     * Strips out key fragments in an array
     *
     * @param array $ref_array
     * @param array $chk_array
     * @return <type>
     */
    public static function strip($ref_array, $chk_array)
    {
        if(!is_array($chk_array))
        {
            $chk_array = func_get_args();
            array_shift($chk_array);
        }
        $data = array_diff_key($ref_array, array_flip($chk_array));
        return $data;
    }

    /**
     * Extracts array values into a global namespace
     * @param array $array
     * @param string | bool $filter
     */
    public static function extract(&$array, $filter = false)
    {
        //-- Cant walk array because its out of scope to have globals effect 
        //-- this function
        //-- //array_walk($array, create_function('&$item1, $key', 'global $$key;'));
        foreach($array as $key => $value) 
        {
            global $$key;
        }
        extract($array, EXTR_REFS);
        if($filter) 
        {
            $extract_keys    = array_keys(get_defined_vars());
            $filtered        = $filter . '_';
            $filtered_length = strlen($filter);
            
            foreach($extract_keys as $key) 
            {
                $pretension = substr($key, 0, $filtered_length);
                if($pretension == $filtered)
                {
                    $obj[substr($key, 1, strlen($key))] = $$key;
                }
                else
                {
                    unset($$key);
                }
            }
        }
    }
    
    /**
     * Sets the value at $index as the key
     * Will overwrite values if the key already exists
     * @param array $array
     * @param string $index
     * @return array
     */
    public static function set_keys($array, $index)
    {
        $new_array = array();
        foreach($array as $element){
            $new_array[$element[$index]] = $element;
        }
        
        return $new_array;
    }
    
    /**
     * Sets the value at $index1 as the key, and the value at $index2 as the value.
     * Will overwrite values if the key already exists.
     * @param array $array
     * @param string $index1
     * @param string $index2
     * @return array
     */
    public static function set_keys_values($array,$index1,$index2){
        $new_array = array();
        foreach($array as $element){
            $new_array[$element[$index1]] = $element[$index2];
        }
        
        return $new_array;
    }
    
    public static function values_as_keys($array){
        $new_array = array();
        foreach($array as $element){
            $new_array[$element] = $element;
        }
        
        return $new_array;
    }
    
    // Only validates empty or completely associative arrays
    public static function is_assoc($arr){
        return (is_array($arr) && count(array_filter(array_keys($arr),'is_string')) == count($arr));
    }
    
    //sorts the array, RETURNING the sorted array
    public static function sort($array){
        sort($array);
        return $array;
    }
}
?>
