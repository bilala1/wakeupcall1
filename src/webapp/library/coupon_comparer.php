<?php



class CouponComparer{

    public function GetDiscountCodePricing($db, $discount_code){
        $code = $db->fetch_one('
		SELECT * FROM discount_codes WHERE discount_codes_code = ?
		AND discount_codes_start <= NOW()
		AND discount_codes_end >= NOW()',
            array($discount_code)
        );

        $result = array();

        if (empty($code)) {
            //die('fail');
        } else {
            if ($code['discount_codes_amount'] > 0) {
                $discount_code_total_as_number = YEARLY_COST - $code['discount_codes_amount'];
                $result['discount_code_discount'] = money_format('%n', $code['discount_codes_amount']);
                $result['discount_code_total'] = money_format('%n', $discount_code_total_as_number);
                $result['discount_code_total_as_number'] = $discount_code_total_as_number;
            } else {
                $discount_code_total_as_number = YEARLY_COST * (1 - $code['discount_codes_percent'] / 100);
                $result['discount_code_discount'] = $code['discount_codes_percent'] . '%';
                $result['discount_code_total'] = money_format('%n', $discount_code_total_as_number);
                $result['discount_code_total_as_number'] = $discount_code_total_as_number;
            }
            $result['discount_codes_code']=  $code['discount_codes_code'];
            $result['discount_codes_id']=  $code['discount_codes_id'];
            //die(json_encode($result));
        }

        return $result;
    }

    public function GetFranchiseCodePricing($db, $franchise_code){
        $franchise = $db->fetch_one('
		SELECT * FROM franchises WHERE franchises_code = ?',
            array($franchise_code)
        );

        $result = array();

        if (empty($franchise)) {
            //die('fail');
        } else {
            //TODO, change franchise discount and franchise_total to franchise_code_discount
            //TODO, and franchise_code_total on full membership if use this call
            if ($franchise['franchises_amount_discount'] > 0) {
                $franchise_code_total_as_number  =  YEARLY_COST - $franchise['franchises_amount_discount'];
                $result['franchise_code_discount'] = money_format('%n', $franchise['franchises_amount_discount']);
                $result['franchise_code_total'] = money_format('%n', $franchise_code_total_as_number);
                $result['franchise_code_total_as_number']    = $franchise_code_total_as_number;
            } else {
                $franchise_code_total_as_number  =  YEARLY_COST * (1 - $franchise['franchises_pct_discount'] / 100);
                $result['franchise_code_discount'] = $franchise['franchises_pct_discount'] . '%';
                $result['franchise_code_total'] = money_format('%n', $franchise_code_total_as_number);
                $result['franchise_code_total_as_number']    = $franchise_code_total_as_number;
            }
            $result['franchises_code']=  $franchise['franchises_code'];
            $result['franchises_id']=  $franchise['franchises_id'];
            //die(json_encode($result));
        }

        return $result;
    }

}



//// AJAX --
//if ($_REQUEST['ajax'] == 'coupon-pricing') {
//    //if array empty:  //die('fail');
//    //else      //die(json_encode($result));
//} // - AJAX
//
//
//// AJAX --
//if ($_REQUEST['ajax'] == 'franchise-pricing') {
//
//} // - AJAX
//


?>