<?php

final class files
{
    //function to upload a file.
    //defaults: MIMETYPES, EXTENSIONS, UPLOAD_DIR, MAX_FILE_SIZE
    //$allowed_mimetypes = array('image/jpeg','image/pjpeg','image/gif','image/png','image/x-png');
    //$allowed_extensions = array('jpg','jpeg','gif','png');
    //returns true on success or string (error message) on failure
    public static function upload($file, $filename = null, $upload_dir = UPLOAD_DIR, $allowed_mimetypes = null, $allowed_extensions = null, $max_file_size = MAX_FILE_SIZE, $allow_overwrite = true)
    {
        //defaults
        $filename = $filename ? $filename : $file['name'];
        $allowed_mimetypes = $allowed_mimetypes ? $allowed_mimetypes : unserialize(MIMETYPES);
        $allowed_extensions = $allowed_extensions ? $allowed_extensions : unserialize(EXTENSIONS);
        
        //there was an error with the upload
        if($file['error'] != 'UPLOAD_ERR_OK'){
            return 'The file was not uploaded successfully. Please try again.';
        }
        
        //the passed in file is not an 'uploaded' file
        if(!is_uploaded_file($file['tmp_name'])){
            return 'The file was not uploaded. Please try again.';
        }
        
        //not allowed mimetype
        if(!in_array($mime_type = file::get_mime_type($file['tmp_name']), $allowed_mimetypes)){
            return 'You cannot upload this type of file: '.$mime_type;
        }
        
        //not allowed extension
        if(!in_array($filetype = file::get_extension($filename), $allowed_extensions)){
            return 'You cannot upload a file with this extension: '.$filetype;
        }
        
        //there is already a file with the same name
        if(!$allow_overwrite && is_file($upload_dir.$filename)){
            return 'There is already a file with this name.';
        }
        
        //the file is too big
        if($file['size'] > $max_file_size){
            return 'The file is larger than '.$max_file_size.' bytes.';
        }
        
        //the file was able to be moved
        if(!move_uploaded_file($file['tmp_name'],$upload_dir.$filename)){
            return 'The file was not uploaded successfully. Please try again. Could not move to '.$upload_dir.$filename;
        }
        
        return true;
    }
    
    public static function get_mime_type($filename){
        $file_info = exec('file -i ' . escapeshellarg($filename));
        $mime_type = explode(':', $file_info);
        $mime_type = explode(';', $mime_type[1]);
        return trim($mime_type[0]);
    }

    public static function get_mime_type_by_name($filename){
        //return Psr7\mimetype_from_filename($filename) ?: 'application/octet-stream';
        return 'application/octet-stream';
    }
    
    public static function get_extension($filename){
        //Combining these results in pass by reference error on strict.
        $tmpArray = explode('.',$filename);
        return array_pop($tmpArray);
    }
    
    public static function uniqname($dir, $ext){
        $filename = mt_rand() . '.' . $ext;
        while(file_exists($dir . $filename)){
            $filename = mt_rand() . '.' . $ext;
        }
        
        return $filename;
    }
    
    public static function unique_sym_name($filename){
        $file_parts = explode('.', $filename);
        $extension = array_pop($file_parts);
        $symname = files::encode_file_name(implode('.', $file_parts)) . '_' . rand(10000, 99999) . '.' . $extension;
        return $symname;
    }
    
    public static function encode_file_name($phrase){
        return preg_replace('/[^a-z0-9\-]/', '', strtolower(str_replace(' ', '-', $phrase)));
    }

    public static function fixUploadFileArray($uploadFile) {
        $result = array();
        if(is_array($uploadFile)) {
            foreach ($uploadFile as $key1 => $value1) {
                foreach ($value1 as $key2 => $value2) {
                    $result[$key2][$key1] = $value2;
                }
            }
        }
        return $result;
    }
}

?>
