<?php
#REVISION: 1
#REVISION: 1
/*
 * TODO: custom 404 view
 */
final class framework
{
    static function disable_view()
    {
        if(!defined('FRAMEWORK_NOVIEW'))
        {
            define('FRAMEWORK_NOVIEW', true);
        }
    }
    
    static function set_view($view)
    {
        if(!defined('FRAMEWORK_VIEW'))
        {
            define('FRAMEWORK_VIEW', $view);
        }
    }
    
    static function shutdown()
    {
        global $_BODY;
        //-- Setup Constants
        define('BASEURL', (isset($_SERVER['HTTPS'])) ? HTTPS_BASEURL : HTTP_BASEURL);
        define('FULLURL', (isset($_SERVER['HTTPS'])) ? HTTPS_FULLURL : HTTP_FULLURL);

        // if maintenance mode, bail
        if (MAINTENANCE_MODE == 'ON' && strpos($_SERVER['REQUEST_URI'], 'under_maintenance') === false) {
            if (strpos($_SERVER['REQUEST_URI'], '/api/') !== false ||
                (http::is_ajax() && strpos($_SERVER['REQUEST_URI'], '/members/login.php') !== false)
            ) {
                $_SERVER['REDIRECT_REDIRECT_FRAMEWORK'] = true;
                $_SERVER['REDIRECT_URL'] = '/API/maintenance.php';
            } else {
                header('Location: ' . HTTPS_FULLURL . '/under_maintenance.php', true, 302);
                exit;
            }
        }

        //-- Clean Router
        $uri = $_SERVER['REQUEST_URI'];
        if(array_key_exists('REDIRECT_REDIRECT_FRAMEWORK', $_SERVER) && $_SERVER['REDIRECT_REDIRECT_FRAMEWORK'])
        {
            $uri = $_SERVER['REDIRECT_URL'];
        }
        $uri_base = BASEURL . '/';
        if($uri_base === '/' and $uri{0} === '/') 
        {
            $uri = substr($uri, 1, strlen($uri));
        }
        else
        {
            $uri = str_replace($uri_base, '', $uri);
        }
        $uri = explode('?', $uri);
        $uri = array_shift($uri);
        //-- action
        $routes = (defined('ROUTES')) ? unserialize(ROUTES) : false;
        $action = null;
        if(is_array($routes) and isset($routes[$uri]))
        {
            $route = $routes[$uri];
            $_GET = array_merge($route, $_GET);
            $_REQUEST = array_merge($route, $_REQUEST);
            $action = $route['action'];
        }
        else
        {
            $action = $uri;
        }
        
        if($action === 'debug-framework')
        {
            return self::debug_framework();
        }
        
        //-- View
        $view = $action;

        $action_path = ACTIONS . DIRECTORY_SEPARATOR . $action;
        $view_path = VIEWS . DIRECTORY_SEPARATOR . $view;
        //-- Check for existance
        if(!is_file($action_path) and !is_file($view_path) and !defined('FRAMEWORK'))
        {
            header("HTTP/1.1 404 Not Found");
            header("Status: 404 Not Found");
            
            if(VIEWS . DIRECTORY_SEPARATOR . '404.php')
            {
                ob_start();
                include(VIEWS . DIRECTORY_SEPARATOR . '404.php');
                self::output(ob_get_clean());
            }
            else 
            {
                echo '<html>
                    <head>
                    <title>Status: 404 Not Found</title>

                    </head>
                    <body>

                    <h2>Status: 404 Not Found</h2>
                    <h4>The Page does not exist.</h4>
                    </body>
                    </html>';
            }
            trigger_error("Page Cannot Be Found: " . $_SERVER['REQUEST_URI'], E_USER_ERROR);
        }

        //-- Load action
        if(is_file($action_path))
        {
            $access_logs_id = AccessLog::RecordCurrentAccess();
            include($action_path);
        }

        //-- Load View
        if(!defined('FRAMEWORK_NOVIEW'))
        {
            //-- Load View
            ob_implicit_flush(false);
            ob_start(array('framework','output'));
            if(stristr($view, '.css'))
            {
                header('Content-type: text/css');
                $expires = 60*60*24*14;
                header("Pragma: public");
                header("Cache-Control: maxage=".$expires);
                header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');
                if(is_file($view_path))
                {
                    header('Etag: ' . md5_file($view_path) . '');
                }
            }
            // Override View
            if(defined('FRAMEWORK_VIEW'))
            {
                $view_path = SITE_PATH . DIRECTORY_SEPARATOR  . VIEWS . DIRECTORY_SEPARATOR . FRAMEWORK_VIEW;
            }
            // Buffer View
            if(is_file($view_path))
            {
                include($view_path);
            }
            ob_flush();
            ob_end_flush();
            ob_implicit_flush(true);
        }
        restore_error_handler();
    }

    static function startup()
    {
        global $_tmp_session;
        
        $_tmp_session = $_SESSION;
        $_SESSION = $_tmp_session[md5(SITE_PATH)];
        
        spl_autoload_register(array('framework', 'autoload'));
        //set_error_handler(array('framework', 'error_handler'));
        register_shutdown_function(function () {
            global $_tmp_session;
            $lastErr = error_get_last();
            if($lastErr && DEPLOYMENT == 'development') {
                $isJson = false;
                foreach(getallheaders() as $key=>$header) {
                    if(stripos($key, 'Accept') !== false && stripos($header, 'application/json') !== false) {
                        $isJson = true;
                        break;
                    }
                }
                if(!$isJson) {
                    pre($lastErr);
                }
            }
            $_tmp_session[md5(SITE_PATH)] = $_SESSION;
            $_SESSION = $_tmp_session;
        });
        Services\Services::configure();
    }
    
    static function call_action($action, array $params)
    {
        $action_path = SITE_PATH . '/actions/' . $action . '.php';
        $__get = $_GET;
        $__post = $_POST;
        $__request = $_REQUEST;
        $_GET = $_POST = $_REQUEST = $params;
        $output = false;
        if(file_exists($action_path))
        {
            $output = (include $action_path);
        }
        $_GET = $__get;
        $_POST = $__post;
        $_REQUEST = $__request;
        return $output;
    }

    static function output($buffer, $mode='print')
    {
        // Fits absolute urls
        $url = FULLURL . '/';
        
        //----------------------------------------------------------------------
        //-- Replace html attributes
        //----------------------------------------------------------------------
        foreach(array('href', 'src', 'action', 'background') as $attr)
        {
            // Replace what we want to avoid
            $buffer = str_replace($attr . '="//', $attr . '="-/', $buffer);
            $buffer = str_replace($attr . '=\'//', $attr . '=\'-/', $buffer);

            // Replace what we want
            $buffer = str_replace($attr . '="/', $attr . '="' . $url, $buffer);
            $buffer = str_replace($attr . '=\'/', $attr . '=\'' . $url, $buffer);

            // Put back what we didn't want.
            $buffer = str_replace($attr . '="-/', $attr . '="//', $buffer);
            $buffer = str_replace($attr . '=\'-/', $attr . '=\'//', $buffer);
        }
        //----------------------------------------------------------------------
        //-- Replace css attributes
        //----------------------------------------------------------------------
        foreach(array('url') as $attr)
        {
            // Replace what we want to avoid
            $buffer = str_replace($attr . '(//', $attr . '(-/', $buffer);

            // Replace what we want
            $buffer = str_replace($attr . '(/', $attr . '(' . $url, $buffer);

            // Put back what we didn't want.
            $buffer = str_replace($attr . '(-/', $attr . '(//', $buffer);
        }
        
        if($mode == 'print')
        {
            echo $buffer;
        } else {
            return $buffer;
        }
    }

    static function autoload($class) 
    {
        if (class_exists($class, false) or interface_exists($class, false)) 
        {
            return;
        }
        $file_lib = LIBS . DIRECTORY_SEPARATOR . $class . '.php';
        $file_model = MODELS . DIRECTORY_SEPARATOR . $class . '.php';
        if(is_file($class . '.php'))
        {
            include_once $class . '.php';
        }
        else if(is_file($file_lib))
        {
            include_once $file_lib;
        }
        else if(is_file($file_model))
        {
            include_once $file_model;
        }
    }
    
    static function error_handler($errno, $errstr, $errfile, $errline)
    {
        $site_url = $_SERVER['REQUEST_URI'];
        $site_url = str_replace(BASEURL, '', $site_url);
        $site_url = FULLURL . $site_url;
        if($errno === 8)
        {
            return;
        }
        
        //-- Get Backtrace
        ob_start();
        debug_print_backtrace();
        $backtrace = ob_get_contents();    
        ob_end_clean();
        
        $file = SITE_PATH  . '/data/error_handle.php';
        
        $index = 0;
        if(is_file($file))
        {
            $index = (string) filesize($file);
            chmod($file, 0777);
        }
        
        $log = '$log []= array(
            \'uri\'       => \'' . $site_url . '\',
            \'post\'      => \'' . base64_encode(serialize($_POST)) . '\',
            \'get\'       => \'' . base64_encode(serialize($_GET)) . '\',
            \'session\'   => \'' . base64_encode(serialize($_SESSION)) . '\',
            \'backtrace\' => \'' . base64_encode(serialize($backtrace)) . '\',
            \'ts\'        => \'' . strtotime('now') . '\',
            \'no\'        => \'' . $errno . '\',
            \'str\'       => \'' . addslashes($errstr) . '\',
            \'file\'      => \'' . $errfile . '\',
            \'line\'      => \'' . $errline . '\',
            \'log_byte\'  => \'' . $index . '\'
        );';
        $log = str_replace(array("\n", "\r"), "", $log) . "\n";
        file_put_contents($file, $log, FILE_APPEND);
        switch ($errno)
        {
            case E_USER_ERROR:
                exit(1);
                break;
            default:
                break;
        }
        return true;
    }

    static function debug_framework()
    {
        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Comentum Framework</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <style type="text/css" media="screen">
            html { min-width: 600px; }

            body, div, td, th, h2, h3, h4 { /* redundant rules for bad browsers  */
                                            font-family: verdana,sans-serif;
                                            font-size:    x-small;
                                            voice-family: "\"}\"";
                                            voice-family: inherit;
                                            font-size: small;
                                            color: #333;
            }
            .clearhack { display: inline; } /*Clears Box Model Hack in IE5*/

            body {
                background: #EDEBE3;
                color: #333;
                padding: 1em 20px 3em 20px;
                margin: 0;
            }

            a { color: #06C; }
            a:hover { color: #333; }
            a:active { color: #000; }

            p { line-height: 140%; }

            h1,h2 {
                font-family: trebuchet ms;
                font-weight: bold;
                color: #333;
            }

            h1 {
                font-size: 180%;
                margin: 0;
            }

            h1 a { text-decoration: none; color: #333; }
            h1 a:hover { border-bottom: 1px dotted #666; color: #000; }

            h2 {
                font-size: 140%;
                padding-bottom: 2px;
                border-bottom: 1px solid #CCC;
                margin: 0;
            }

            p.note {
                background: #EEE;
                padding: 4px;
                font-family: tahoma;
                font-size: 85%;
                line-height: 130%;
                margin-top: 0;
            }
        </style>
        <style type="text/css" media="screen">
            #main {
                border: 1px solid #666;
                clear: both;
                background: #FFF3B3;
                padding-top: 2em;
            }

            #contents {
                padding: 1.5em;
                background: #FFFDF3;
                min-height: 300px;
            }

            #header {
                position: relative;
                width: 100%;
                height: 3em;
                width: 45em; /* a width is required for Opera, older Mozilla browsers, and Konqueror browsers */
            }

            #header ul#primary {
                margin: 0;
                padding: 0;
                position: absolute;
                bottom: -1px;
                width: 45em; /* a width is required for Opera, older Mozilla browsers, and Konqueror browsers */
            }

            #header ul#primary li  {
                display: inline;
                list-style: none;
            }

            #header ul#primary a,#header ul#primary span,#header ul#primary a.current {
                width: 8em;
                display: block;
                float: left;
                padding: 4px 0;
                margin: 1px 2px 0 0;
                text-align: center;
                font-family: tahoma, verdana, sans-serif;
                font-size: 85%;
                text-decoration: none;
                color: #333;
            }

            #header ul#primary span,#header ul#primary a.current,#header ul#primary a.current:hover {
                border: 1px solid #666;
                border-bottom: none;
                background: #FFF3B3;
                padding-bottom: 6px;
                margin-top: 0;
            }

            #header ul#primary a {
                background: #FFFAE1;
                border: 1px solid #AAA;
                border-bottom: none;
            }

            #header ul#primary a:hover {
                margin-top: 0;
                border-color: #666;
                background: #FFF7CD;
                padding-bottom: 5px;
            }

            #header ul#secondary {
                position: absolute;
                margin: 0;
                padding: 0;
                bottom: -1.4em;
                left: 1px;
                width: 50em; /* a width is required for Opera, older Mozilla browsers, and Konqueror browsers */
            }

            #header ul#secondary li a,#header ul#secondary li span {
                width: auto;
                display: block;
                float: left;
                padding: 0 10px;
                margin: 0;
                text-align: auto;
                border: none;
                border-right: 1px dotted #AAA;
                background: none;

            }

            #header ul#secondary li a {
                color: #06C;
                text-decoration: underline;
            }

            #header ul#secondary li a:hover {
                color: #333;
                background: transparent;
                padding: 0 10px;
                border: none;
                border-right: 1px dotted #AAA;
            }

            #header ul#secondary li a:active {
                color: #000;
                background: transparent;
            }

            #header ul#secondary li:last-child a { border: none; }
        </style>
    </head>
    <body>

        <h1><a href="index.html">Comentum Framework</a></h1>
        <div id="header">
            <ul id="primary">';
        if($_GET['tab'] === 'clear-log' or !isset($_GET['tab']))
        {
            $html .= '<li><span>Clear Log</span></li>';
        }
        else
        {
            $html .= '<li><a href="?tab=clear-log">Clear Log</a></li>';
        }

        if($_GET['tab'] === 'dashboard' or !isset($_GET['tab']))
        {
            $html .= '<li><span>Dashboard</span></li>';
        }
        else
        {
            $html .= '<li><a href="?tab=dashboard">Dashboard</a></li>';
        }

        if($_GET['tab'] === 'logs')
        {
            $html .= '
                <li><span>Logs</span>
                    <ul id="secondary">
                        <li><a href="?tab=logs&sub_tab=fatal">Fatal</a></li>
                        <li><a href="?tab=logs&sub_tab=warning">Warnings</a></li>
                        <li><a href="?tab=logs&sub_tab=other">Others</a></li>
                    </ul>
                </li>
                ';
        }
        else
        {
            $html .= '<li><a href="?tab=logs">Logs</a></li>';
        }
        $html .= '
            </ul>
        </div>
        <div id="main">
            <div id="contents">';
            if($_GET['tab'] === 'logs')
            {
                $html .= '<h2>Logs</h2>';
                $html .= '<p class="note">' . ucwords(isset($_GET['sub_tab']) ? $_GET['sub_tab'] : '' ) . '</p>';

                $html .= '<table cellspacing="0" cellpadding="5" border="1">';
                $html .= '  <tr>';
                $html .= '      <th>Time</th>';
                $html .= '      <th>Type</th>';
                $html .= '      <th>Message</th>';
                $html .= '      <th>Line</th>';
                $html .= '      <th>File</th>';
                $html .= '  </tr>';
                $error_type = 0;
                
                
                $error_reports = array(
                    2 => 'Warning',
                    8 => 'Notice'
                );
                
                    
                if(stristr($_GET['sub_tab'], 'show-event'))
                {
                    $fp = fopen(SITE_PATH  . '/data/error_handle.php', 'r');
                    //fseek($fp, 0);
                    fseek($fp, (int) str_replace('show-event', '', $_GET['sub_tab']));
                    $line = fgets($fp);
                    eval($line);
                    $log_id = 0;
                }
                else 
                {
                    $log = self::get_log();
                }
                
                if(isset($_GET['sub_tab']) and $_GET['sub_tab'] === 'warning')
                {
                    $error_type = 2;
                }
                else if(isset($_GET['sub_tab']) and $_GET['sub_tab'] === 'notice')
                {
                    $error_type = 8;
                }
                
                if(isset($log_id))
                {
                    self::draw_row($html, $log[$log_id], $error_type, 'full');
                }
                else
                {
                    foreach ($log as $entry)
                    {
                        self::draw_row($html, $entry, $error_type, 'partial');
                    }
                }
                $html .= '</table>';
            }
            else if($_GET['tab'] === 'clear-log')
            {
                if(is_file(SITE_PATH . '/data/error_handle.php'))
                {
                    unlink(SITE_PATH . '/data/error_handle.php');
                }
            }
            else if($_GET['tab'] === 'dashboard')
            {
                $log = self::get_log();
                
                $html .= '
                <h2>Dashboard</h2>
                <p class="note">
                    Recent Events
                </p>';
            
                $error_reports = array(
                    2 => 'Warning',
                    8 => 'Notice'
                );
                    
                $html .= '<table cellspacing="5">';
                $html .= '  <tr style="backgroud-color: silver">';
                $html .= '      <th>Id</th>';
                $html .= '      <th>Rule Name</th>';
                $html .= '      <th>Count</th>';
                $html .= '      <th>Last Occ.</th>';
                $html .= '      <th>Occurred at...</th>';
                $html .= '  </tr>';

                foreach ($log as $i => $entry)
                {
                    $style = '';
                    
                    if($entry['no'] == 2)
                    {
                        $style .= 'background: orange';
                    }

                    $html .= '  <tr style="' .  $style . '">';
                    $html .= '      <td><a href="?tab=logs&sub_tab=show-event' . $entry['log_byte'] . '">' . $i . '</a></td>';
                    $html .= '      <td>' . date('M j, Y \a\t h:i:s a', $entry['ts']) . '</td>';
                    $html .= '      <td>' . $error_reports[$entry['no']] . '</td>';
                    $html .= '      <td>' . $entry['str'] . '</td>';
                    $html .= '      <td>' . $entry['line'] . '</td>';
                    $html .= '      <td>' . $entry['file'] . '</td>';
                    $html .= '  </tr>';
                }
                $html .= '</table>';
            }
        $html .= '
            </div>
        </div>
    </body>
</html>';
        echo $html;
    }
    
    static function draw_row(&$html, $entry, $error_type, $display)
    {
        $style = '';

        if($entry['no'] == 2)
        {
            $style .= 'background: orange';
        }

        if ((int) $entry['no'] === $error_type)
        {
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td colspan="5"><strong>Webpage: ' . $entry['uri'] . '</strong></td>';
            $html .= '  </tr>';
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td>' . date('M j, Y \a\t h:i:s a', $entry['ts']) . '</td>';
            $html .= '      <td>' . $error_reports[$entry['no']] . '</td>';
            $html .= '      <td>' . $entry['str'] . '</td>';
            $html .= '      <td>Line: ' . $entry['line'] . '</td>';
            $html .= '      <td>' . $entry['file'] . '</td>';
            $html .= '  </tr>';
        }    
        
        if($display == 'full')
        {
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td colspan="5"><pre><strong>GET: ' . var_export(unserialize(base64_decode($entry['get'])), true) . '</strong></pre></td>';
            $html .= '  </tr>';
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td colspan="5"><pre><strong>POST: ' . var_export(unserialize(base64_decode($entry['post'])), true) . '</strong></pre></td>';
            $html .= '  </tr>';
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td colspan="5"><pre><strong>SESSION: ' . var_export(unserialize(base64_decode($entry['session'])), true) . '</strong></pre></td>';
            $html .= '  </tr>';
            $html .= '  <tr style="' .  $style . '">';
            $html .= '      <td colspan="5"><pre><strong>BACKTRACE: ' . var_export(unserialize(base64_decode($entry['backtrace'])), true) . '</strong></pre></td>';
            $html .= '  </tr>';
        }
    }
    
    static function get_log()
    {
        $log = array();
        if(is_file(SITE_PATH  . '/data/error_handle.php'))
        {
            $file = SITE_PATH  . '/data/error_handle.php';
            $lines = `tail -n 100 $file`;
            eval($lines);
        }    
        
        return $log;
    }


    static function routes(array $routes)
    {
        define('ROUTES', serialize($routes));
    }
    
    static function parse_css($css) {
        $selectors = array();
        $buffer = $css;
        while(preg_match('/(?ims)([a-z0-9\s\.\:#_\-@]+)\{([^\}]*)\}/', $buffer, $arr))
        {
            $selectors[trim($arr[1])] = trim($arr[2]);
            $buffer = str_replace($arr[0], '', $buffer);
        }
        if(sizeof($selectors)){
            foreach($selectors as $selector=>$declaration){
                $css_selector = str_replace('.', '\.', $selector);
                $css_selector = str_replace(' ', '\s', $selector);
                $css = preg_replace('/include\-selector\:?\s' . $css_selector . ';/i', $declaration, $css);
                unset($selectors[$selector]);
                while(preg_match('/(?ims)([a-z0-9\s\.\:#_\-@]+)\:([^;]*)\;{0,1}/', $declaration, $arr) ){
                    $arrSelectors[$selector][trim($arr[1])] = trim( $arr[2] );
                    $declaration = str_replace($arr[0], '', $declaration );
                }
            }
        }
        return $css;
    }
    
    //returns true if this code is eventually from a function: call_action  (TODO: make sure from framework class too)
    static function is_from_call_action(){
        foreach(debug_backtrace() as $trace){
            if($trace['function'] == 'call_action'){
                return true;
            }
        }
        
        return false;
    }
}

function pre_dump($variables){
    echo '<pre style="font-size:10pt; background-color:white; text-align:left; padding:10px; font-weight:bold; color:black">';
    foreach($variables as $index => $variable){
        var_dump($variable);
        //var_export($variable);
        
        if($index < count($variables)-1){
            echo '<br /><hr /><br />';
        }
    }
    echo '</pre><hr /><hr />';
}

function pre(){
    pre_dump(func_get_args());
}
?>
