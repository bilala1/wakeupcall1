<?php
#REVISION: 25
#REVISION: 19
final class html
{
    const selected = 'selected="selected"';
    const checked = 'checked="checked"';
    //-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //-- IF YOU GUYS FORMAT THE COUNTRIES I WILL KILL YOU. DON'T PUT THEM ON  !!
    //-- ONE LINE.                                                            !!
    //-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public static $us_states = array('AL' => 'Alabama', 'AK' => 'Alaska',
	    'AS' => 'American Samoa', 'AZ' => 'Arizona', 'AR' => 'Arkansas',
	    'AA' => 'Armed Forces Americas', 'AE' => 'Armed Forces Europe',
	    'AP' => 'Armed Forces Pacific', 'CA' => 'California',
	    'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware',
	    'DC' => 'District of Columbia', 'FM' => 'F.S. Micronesia',
	    'FL' => 'Florida', 'GA' => 'Georgia', 'GU' => 'Guam ', 'HI' => 'Hawaii',
	    'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa',
	    'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana',
	    'ME' => 'Maine', 'MH' => 'Marshall Islands', 'MD' => 'Maryland',
	    'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota',
	    'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana',
	    'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire',
	    'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York',
	    'NC' => 'North Carolina', 'ND' => 'North Dakota',
	    'MP' => 'Northern Mariana Islands', 'OH' => 'Ohio', 'OK' => 'Oklahoma',
	    'OR' => 'Oregon', 'PW' => 'Palau', 'PA' => 'Pennsylvania',
	    'PR' => 'Puerto Rico', 'RI' => 'Rhode Island', 'SC' => 'South Carolina',
	    'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas',
	    'UT' => 'Utah', 'VT' => 'Vermont', 'VI' => 'Virgin Islands',
	    'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia',
	    'WI' => 'Wisconsin', 'WY' => 'Wyoming'
    );
	public static $countries = array('US' => 'United States',
	    'UM' => 'United States Minor Outlying Islands', 'AF' => 'Afghanistan',
	    'AX' => 'Aland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria',
	    'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola',
	    'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua and Barbuda',
	    'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba',
	    'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan',
	    'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh',
	    'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium',
	    'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan',
	    'BO' => 'Bolivia', 'BA' => 'Bosnia and Herzegovina', 'BW' => 'Botswana',
	    'BV' => 'Bouvet Island', 'BR' => 'Brazil',
	    'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam',
	    'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi',
	    'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada',
	    'CV' => 'Cape Verde', 'KY' => 'Cayman Islands',
	    'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile',
	    'CN' => 'China', 'CX' => 'Christmas Island',
	    'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia',
	    'KM' => 'Comoros', 'CG' => 'Congo',
	    'CD' => 'Congo, The Democratic Republic of the', 'CK' => 'Cook Islands',
	    'CR' => 'Costa Rica', 'CI' => 'Cote d\'Ivoire', 'HR' => 'Croatia',
	    'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic',
	    'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica',
	    'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt',
	    'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea',
	    'EE' => 'Estonia', 'ET' => 'Ethiopia',
	    'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands',
	    'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France',
	    'GF' => 'French Guiana', 'PF' => 'French Polynesia',
	    'TF' => 'French Southern Territories', 'GA' => 'Gabon',
	    'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana',
	    'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland',
	    'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam',
	    'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea',
	    'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti',
	    'HM' => 'Heard Island and McDonald Islands',
	    'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras',
	    'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland',
	    'IN' => 'India', 'ID' => 'Indonesia',
	    'IR' => 'Iran, Islamic Republic of', 'IQ' => 'Iraq', 'IE' => 'Ireland',
	    'IM' => 'Isle of Man', 'IL' => 'Israel', 'IT' => 'Italy',
	    'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan',
	    'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati',
	    'KP' => 'Korea, Democratic People\'s Republic of',
	    'KR' => 'Korea, Republic of', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan',
	    'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia',
	    'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia',
	    'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein',
	    'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao',
	    'MK' => 'Macedonia, The Former Yugoslav Republic of',
	    'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia',
	    'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta',
	    'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania',
	    'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico',
	    'FM' => 'Micronesia, Federated States of',
	    'MD' => 'Moldova, Republic of', 'MC' => 'Monaco', 'MN' => 'Mongolia',
	    'MS' => 'Montserrat', 'ME' => 'Montenegro', 'MA' => 'Morocco',
	    'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia',
	    'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands',
	    'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia',
	    'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger',
	    'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island',
	    'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman',
	    'PK' => 'Pakistan', 'PW' => 'Palau',
	    'PS' => 'Palestinian Territories, Occupied', 'PA' => 'Panama',
	    'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru',
	    'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland',
	    'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar',
	    'RE' => 'Reunion', 'RO' => 'Romania', 'RU' => 'Russian Federation',
	    'RW' => 'Rwanda', 'BL' => 'Saint Barthelemy', 'SH' => 'Saint Helena',
	    'KN' => 'Saint Kitts and Nevis', 'LC' => 'Saint Lucia',
	    'MF' => 'Saint Martin', 'PM' => 'Saint Pierre and Miquelon',
	    'VC' => 'Saint Vincent and the Grenadines', 'WS' => 'Samoa',
	    'SM' => 'San Marino', 'ST' => 'Sao Tome and Principe',
	    'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia',
	    'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore',
	    'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands',
	    'SO' => 'Somalia', 'ZA' => 'South Africa',
	    'GS' => 'South Georgia and the South Sandwich Islands', 'ES' => 'Spain',
	    'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname',
	    'SJ' => 'Svalbard and Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden',
	    'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic',
	    'TW' => 'Taiwan, Province of China', 'TJ' => 'Tajikistan',
	    'TZ' => 'Tanzania, United Republic of', 'TH' => 'Thailand',
	    'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau',
	    'TO' => 'Tonga', 'TT' => 'Trinidad and Tobago', 'TN' => 'Tunisia',
	    'TR' => 'Turkey', 'TM' => 'Turkmenistan',
	    'TC' => 'Turks and Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda',
	    'UA' => 'Ukraine', 'AE' => 'United Arab Emirates',
	    'GB' => 'United Kingdom', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan',
	    'VU' => 'Vanuatu', 'VE' => 'Venezuela', 'VN' => 'Viet Nam',
	    'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.',
	    'WF' => 'Wallis and Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen',
	    'ZM' => 'Zambia', 'ZW' => 'Zimbabwe'
    );
	public static $months = array(
	    '01' => 'January', '02' => 'February',
	    '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June',
	    '07' => 'July', '08' => 'August', '09' => 'September',
	    '10' => 'October', '11' => 'November', '12' => 'December'
    );
	public static $ccmonths = array(
	    '01' => '(01) January', '02' => '(02) February', '03' => '(03) March',
	    '04' => '(04) April', '05' => '(05) May', '06' => '(06) June',
	    '07' => '(07) July', '08' => '(08) August', '09' => '(09) September',
	    '10' => '(10) October', '11' => '(11) November', '12' => '(12) December'
    );



    /**
     * This create a free form dom element not applied to a dom document
     * So if you want to interact with html::element's children, you need
     * to add it to the dom document via:
     * $myelement->inject($myelement->owner->body);
     */
    final public static function element($tagname)
    {
        $dom = new simple_dom(null, false);
        $element = $dom->element($tagname);
        return $element;
    }

    final public static function envelop(simple_dom_node &$node)
    {
        $node->inject($node->owner->body);
    }

    /**
     * $selected['temp'] = 5;               //single select
     * <?= html::select(array(5 => 'hi', 6 => 'bye', 7 => 'cya'), 'temp', $selected) ?>
     *
     * $selected['temp'] = array(5, 7);     //multiple select
     * <?= html::select(array(5 => 'hi', 6 => 'bye', 7 => 'cya'), 'temp', $selected, array('multiple' => 'multiple')) ?>
     */
    final public static function select(array $values, $name, $selected = null, $attrs = array(), $native_dom = false)
    {
        if(arrays::is_assoc($selected)) {
            if(isset($selected[$name])) {
                $selected = $selected[$name];
            } else {
                $selected = null;
            }
        }

        // Filter value and name
        $name  = html::filter($name);

        $select = html::element('select');
        $select->set('name', $name);
        $select->set('id', $name);
        foreach((array) $attrs as $key => $attr)
        {
            $select->set($key, $attr);
        }

        foreach($values as $key => $value)
        {
            $option = html::option($key, $value, $selected, array(), true);
            $option->inject($select);
        }

        return ($native_dom) ? $select : $select->to_html() . "\n";
    }

    //Uses the first field as the option value (usually the id: $rows[*][0] = $rows[*]['***_id'])
    //
    //  html::select_from_query(businesses::get_account_types(), 'join_account_types_id', $business['join_account_types_id'], 'account_types_name')
    //
    //index = index of the html in the option tag (<option>this</option>)
    final public static function select_from_query(array $rows, $name, $selected = null, $index = 1, $first_option = null, $attrs = array(), $native_dom = false)
    {
        if(!empty($rows)){
            $first_key = key($rows[0]);

            //if the index hasn't been set, then use the second field
            if($index == 1){
                next($rows[0]);
                $index = key($rows[0]);
            }
        }else{
            $first_key = '';
        }

        $select = html::select(html::db_to_select_array($rows, $first_key, $index, $first_option), $name, $selected, $attrs, $native_dom);

        return $select;
    }

    final public static function selectfield_from_query($text, array $rows, $name, $selected = null, $index = 1, $first_option = null, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $select = self::select_from_query($rows, $name, $selected, $index, $first_option, $attrs, true);

        return self::_form_item($text, $select, $name, $errors, $native_dom);
    }

    final public static function option($value, $text, $selected = null, $attrs = array(), $native_dom = false)
    {
        // Filter value and name
        $value = html::filter($value);
        //$name  = html::filter($name);

        $option = html::element('option');
        $option->set('value', $value);
        $option->set('text', $text);
        foreach($attrs as $key => $attr)
        {
            $option->set($key, $attr);
        }

        if($value !== null){
            if(is_array($selected)){                         //multiple select
                if(in_array($value, $selected)){
                    $option->set('selected', 'selected');
                }
            }
            else if($value == html::filter($selected)){      //single select
                $option->set('selected', 'selected');
            }
        }

        return ($native_dom) ? $option : $option->to_html() . "\n";
    }

    final public static function datefield($text, $value, $name, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $value = !empty($value[$name]) && $value[$name]!='0000-00-00' ? date('n/j/Y', strtotime($value[$name])) : '';

        $attrs['class'] = $attrs['class'] . ' datepicker';

        $input = self::input($value, $name, $attrs, true);

        return self::_form_item($text, $input, $name, $errors, $native_dom);
    }

    final public static function date($value, $name, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $value = !empty($value[$name]) ? date('n/j/Y', strtotime($value[$name])) : '';

        $attrs['class'] = $attrs['class'] . ' datepicker';

        return self::input($value, $name, $attrs, $native_dom);
    }

    final public static function textfield($text, $value, $name, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $input = self::input($value, $name, $attrs, true);

        return self::_form_item($text, $input, $name, $errors, $native_dom);
    }

    final public static function passwordfield($text, $value, $name, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $password_input = self::password($value, $name, $attrs, true);

        return self::_form_item($text, $password_input, $name, $errors, $native_dom);
    }

    final public static function password($value, $name, $attrs = array(), $native_dom = false)
    {
        $attrs['type'] = 'password';

        return self::input($value, $name, $attrs, true);
    }

    final public static function textareafield($text, $value, $name, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $textarea = self::textarea($value, $name, $attrs, true);

        return self::_form_item($text, $textarea, $name, $errors, $native_dom);
    }

    final public static function selectfield($text, $values, $name, $selected = null, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $select = self::select($values, $name, $selected, $attrs, true);

        return self::_form_item($text, $select, $name, $errors, $native_dom);
    }

    final public static function errors($name, $errors, $native_dom = false){
        $errors = (is_array($errors) && isset($errors[$name])) ? $errors[$name] : $errors;

        if(is_string($errors))
        {
            $p = html::element('p');
            $p->set('html', $errors);
            $p->set('class', 'error form-error');
        } else {
            $p = html::element("span");
        }

        return ($native_dom) ? $p : $p->to_html() . "\n";
    }

    final public static function plainTextField($label, $value ,$id = null,$native_dom = false) {
        if($value == null) {
            $value = '';
        }
        $element = html::element('span')->set('html', $value);
        if($id != null) {
            $element->set('id', $id);
        }
        return self::_form_item($label, $element,  null, null, $native_dom);
    }

    final private static function _form_item($text, $element, $name, $errors, $native_dom){
        $div   = html::element('div');
        $br    = html::element('br');
        $div->set('class', 'form-field');

        $errors = (is_array($errors) && isset($errors[$name])) ? $errors[$name] : $errors;

        $label = html::element('label');
        $label->set('html', '<span>'.$text.'</span>');
        $label->set('for', $element->get('id'));
        $label->inject($div);

        $element->inject($div);

        /*$br->set('clear', 'all');
        $br->set('style', 'clear:both');
        $br->inject($div);*/

        if(is_string($errors))
        {
            $p = html::element('p');
            $p->set('html', $errors);
            $p->set('class', 'error form-error');
            $p->inject($div);
        }

        return ($native_dom) ? $div : $div->to_html() . "\n";
    }

    final public static function input($value, $name, $attrs = array(), $native_dom = false)
    {
        $value = is_array($value) && isset($value[$name]) ? $value[$name] : (!is_array($value) && strlen($value) > 0 ? $value : '');

        if($attrs === null){
            $attrs = array();
        }

        // Filter value and name
        //$value = html::filter($value);
        //$name = html::filter($name);

        $input = html::element('input');
        $input->set('value', $value);
        if($name){
            $input->set('name', $name);
            $input->set('id', $name);
        }

        if(!$attrs['type'] || $attrs['type'] == 'text'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-text' : 'input-text';
        }
        else if($attrs['type'] == 'password'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-password' : 'input-password';
        }
        else if($attrs['type'] == 'radio'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-radio' : 'input-radio';
        }
        else if($attrs['type'] == 'checkbox'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-checkbox' : 'input-checkbox';
        }
        else if($attrs['type'] == 'file'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-file' : 'input-file';
        }
        else if($attrs['type'] == 'submit'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-submit' : 'input-submit';
        }
        else if($attrs['type'] == 'reset'){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' input-reset' : 'input-reset';
        }

        foreach($attrs as $key => $attr)
        {
            $input->set($key, $attr);
        }

        return ($native_dom) ? $input : $input->to_html();
    }

    final public static function textarea($value, $name, $attrs = array(), $native_dom = false)
    {
        $value = is_array($value) ? $value[$name] : $value;
        
        // Filter value and name
        $name = html::xss($name);

        $input = html::element('textarea');
        $input->set('html', htmlentities($value, ENT_COMPAT, 'UTF-8'));
        $input->set('name', $name);
        $input->set('id', $name);
        foreach($attrs as $key => $attr)
        {
            $input->set($key, $attr);
        }
        return ($native_dom) ? $input : $input->to_html() . "\n";
    }

    final public static function submit($value, $attrs = array())
    {
        $attrs['type'] = 'submit';
        return self::input($value, null, $attrs);
    }

    final public static function reset($value, $attrs = array())
    {
        $attrs['type'] = 'reset';
        return self::input($value, null, $attrs);
    }

    final public static function checkbox($value, $name, $checked = null, $attrs = array())
    {
        $checked = is_array($checked) && isset($checked[$name]) ? $checked[$name] : $checked;

        $attrs['type'] = 'checkbox';
        if($value == $checked and $checked !== null)
        {
            $attrs['checked'] = 'checked';
        }

        //create a hidden input to deal with the checkbox not being selected
        $hidden_id = $attrs['id'] ? $attrs['id'] . '_hidden' : $name . '_hidden';
        $hidden_input = self::input('0', $name, array('type' => 'hidden', 'id' => $hidden_id), true);
        $input = self::input($value, $name, $attrs, true);

        $span = html::element('span');
        $hidden_input->inject($span);
        $input->inject($span);

        return $span;
    }

    final public static function checkboxfield($text, $value, $name, $checked = null, $attrs = array(), $errors = array(), $native_dom = false)
    {
        $checkbox = self::checkbox($value, $name, $checked, $attrs, true);

        return self::_form_item('<label for="'.$name.'">'.$text.'</label>', $checkbox, $name, $errors, $native_dom);
    }

    final public static function radio($value, $name, $checked = null, $attrs = array(), $native_dom = false)
    {
        $checked = is_array($checked) && isset($checked[$name]) ? $checked[$name] : $checked;

        $attrs['type'] = 'radio';
        if($value == $checked and $checked !== null)
        {
            $attrs['checked'] = 'checked';
        }
        return self::input($value, $name, $attrs, $native_dom);
    }

    /**
	 * <code>
     * <?= html::multiradio('Pick One', 'choice', array('yes' => 'I choose yes', 'no' => 'I choose no'), $_REQUEST, $errors) ?>
     * </code>
     */
    final public static function multiradio($text, $name, $buttons, $checked, $errors, $native_dom = false){
        $checked = is_array($checked) && isset($checked[$name]) ? $checked[$name] : $checked;

        $container = html::element('fieldset');
        $counter = 0;
        foreach($buttons as $btnvalue => $btntext){
            if(is_array($btntext)){                 //backwards compatibility
                list($btnvalue, $btntext) = $btntext;              //I didn't like this way...
            }

            $span = html::element('span');
            $radio = html::element('input');
            $radio
                ->set('type', 'radio')
                ->set('value', $btnvalue)
                ->set('id', $name.'_'.$counter)
                ->set('name', $name);
            $radio->inject($span);

            if($checked == $btnvalue){
                $radio->set('checked', 'checked');
            }

            $labeltxt = html::element('label');
            $labeltxt
                ->set('text', ' '.$btntext)
                ->set('style', 'width:auto')
                ->set('for', $name.'_'.$counter);
            $labeltxt->inject($span);

            $br = html::element('br');
            $br->set('clear', 'all');
            $br->inject($span);

            $span->inject($container);

            //$br = html::element('br');
            //$br->set('clear', 'all');
            //$br->inject($container);

            $counter++;
        }

        /*$br = html::element('br');
        $br->set('clear', 'all');
        $br->inject($container);*/

        return self::_form_item($text, $container, $name, $errors, $native_dom);
    }

    final public static function build_attributes(array $attrs)
    {
        $attrs = http::build_query($attrs);
        $attrs = urldecode($attrs);
        $attrs = str_replace('=', '="', $attrs);
        $attrs = str_replace('&', '" ', $attrs);
        if(!empty($attrs))
        {
            $attrs .= '"';
        }
        return $attrs;
    }

    final public static function filter($value, $strip_tags = false)
    {
        $value = trim(htmlentities($value, ENT_COMPAT, 'UTF-8', false));
        if($strip_tags)
        {
            $value = strip_tags($value);
        }
        $value = str_replace('&#10;', "\n", $value);
        $value = str_replace('&Acirc;', "", $value);
        $value = str_replace(array('<![CDATA[', ']]>'), "", $value);
        $value = str_replace("\xC3\x82", '&nbsp;', $value);
        $value = str_replace("\xC3\xA2\xC2\x80", '&ndash;', $value);
        $value = str_replace("ï¿½", '', $value);
        return $value;
    }

    final public static function xss($optval)
    {
        $tags = $optval;
        $optval = html_entity_decode($optval, null, "UTF-8");
        $optval = str_replace('[\xC0][\xBC]', '<', $optval);
        preg_match_all("/<([^>]+)>/i", $tags, $allTags, PREG_PATTERN_ORDER);
        foreach ($allTags[1] as $tag) {
            $tag = preg_quote($tag, '%');
            $replace = "%(<$tag.*?>)(.*?)(<\/$tag.*?>)%is";
            $replace2 = "%(<$tag.*?>)%is";
            //echo $replace.'<br />';
            $optval = preg_replace($replace,'',$optval);
            //echo $replace2.'<br />';
            $optval = preg_replace($replace2,'',$optval);
        }
        $optval = strip_tags($optval);
        $optval = htmlentities($optval, null, "UTF-8");
        $optval = str_replace(array(
            'javascript:'
        ), '', $optval);
        return $optval;
    }

    final public static function bbcode_format($str)
    {
        $str = htmlentities($str, null, null, false); // Don't double encode
        $str = nl2br($str);

        $search = array(
            '/\[br\]/is',
            '/\[b\](.*?)\[\/b\]/is',
            '/\[i\](.*?)\[\/i\]/is',
            '/\[u\](.*?)\[\/u\]/is',
            '/\[url\=(.*?)\](.*?)\[\/url\]/is',
            '/\[url\](.*?)\[\/url\]/is',
            '/\[align\=(left|center|right)\](.*?)\[\/align\]/is',
            '/\[img\](.*?)\[\/img\]/is',
            '/\[mail\=(.*?)\](.*?)\[\/mail\]/is',
            '/\[mail\](.*?)\[\/mail\]/is',
            '/\[font\=(.*?)\](.*?)\[\/font\]/is',
            '/\[size\=(.*?)\](.*?)\[\/size\]/is',
            '/\[color\=(.*?)\](.*?)\[\/color\]/is',
            '/\[codearea\](.*?)\[\/codearea\]/is',
            '/\[code\](.*?)\[\/code\]/is',
            '/\[p\](.*?)\[\/p\]/is',
        );

        $replace = array(
           '<br />',
            '<strong>$1</strong>',
            '<em>$1</em>',
            '<u>$1</u>',
            '<a target="_blank" href="$1" rel="nofollow" title="$2 - $1">$2</a>',
            '<a target="_blank" href="$1" rel="nofollow" title="$1">$1</a>',
            '<div style="text-align: $1;">$2</div>',
            '<img src="$1" alt="" />',
            '<a href="mailto:$1">$2</a>',
            '<a href="mailto:$1">$1</a>',
            '<span style="font-family: $1;">$2</span>',
            '<span style="font-size: $1;">$2</span>',
            '<span style="color: $1;">$2</span>',
	        '<textarea class="code_container" rows="30" cols="70">$1</textarea>',
	        '<pre class="code">$1</pre>',
	        '<p>$1</p>',
        );

        $str = preg_replace($search, $replace, $str);
        $str = self::bbcode_quote($str);

        // Smilies
        if(is_readable(SITE_PATH .'/smilies.txt'))
        {
            $smilies = @file_get_contents(SITE_PATH .'/smilies.txt');
            if($smilies)
            {
                $lines = explode("\n", $smilies);
                foreach($lines as $smilie)
                {
                    if($smilie)
                    {
                        list($key, $image_src) = explode(' ', $smilie);
                        $image = html::element('img');
                        $image->set('src', FULLURL . $image_src);
                        $str = str_replace(' '.$key.' ', $image->to_html(), $str);
                    }
                }
            }
        }
        return $str;
    }

    final public static function bbcode_prettify($str){
        // Get is in UTF8 to replace baddies
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        // Deals with Double spacing after .periods
        $str = str_replace("\xC3\x82", ' ', $str);
        $str = str_replace("\xEF\xBF\xBD", ' ', $str);
        // Deals with Pretty Quotes to Ugly
        $str = str_replace("\xE2\x80\x9C", '"', $str);
        $str = str_replace("\xE2\x80\x9D", '"', $str);
        // Convert UTF8 back to Ascii
        $str = utf8_decode($str);

        $str = strip_tags($str);

        return $str;
    }

    final public static function bbcode_quote($str)
    {
        $open = '<blockquote><div class="quote_container">';
        $close = '</div></blockquote>';

        preg_match_all('/\[quote\]/i', $str, $matches);
        $opentags = count($matches['0']);

        preg_match_all('/\[\/quote\]/i', $str, $matches);
        $closetags = count($matches['0']);

        $unclosed = $opentags - $closetags;
        for($i = 0; $i < $unclosed; $i++)
        {
            $str .= '</div></blockquote>';
        }

        $str = str_replace ('[' . 'quote]', $open, $str);
        $str = str_replace ('[/' . 'quote]', $close, $str);

        return $str;
    }


    final public static function entities_fix($text)
    {
    	//-- ...'s
    	$text = str_replace("â¦", '&#8230;', $text);
    	$text = str_replace("â¥", '&#8229;', $text);
    	$text = str_replace("â¤", '&#8228;', $text);

    	//-- Quotes
    	$text = str_replace("â", '&#8219;', $text);
        $text = str_replace('â', "&#8218;", $text);
        $text = str_replace('â', "&#8217;", $text);
        $text = str_replace('â?', '&#8221;', $text);
        $text = str_replace('â', '&#8220;', $text);
        $text = str_replace('â', '&#8223;', $text);

    	//-- Common Stuff
    	$text = str_replace("Â°", '&#176;', $text);
    	$text = str_replace("Â©", '&#169;', $text);
    	$text = str_replace("Â®", '&#174;', $text);
    	$text = str_replace("Â»", '&laquo;', $text);
    	$text = str_replace("Â«", '&raquo;', $text);

    	//-- Bullets
    	$text = str_replace("â¢", '&#8226;', $text);
    	$text = str_replace("â§", '&#8231;', $text);
    	$text = str_replace("â", '&#8901;', $text);

    	//-- Hyphens
    	$text = str_replace("â", '&#8209;', $text);
    	$text = str_replace("â", '&#8210;', $text);
    	$text = str_replace("â", '&#8211;', $text);
    	$text = str_replace("â", '&#8212;', $text);
    	$text = str_replace("â", '&#8213;', $text);
    	//$text = str_replace("Â", '&#8213;', $text);


    	//-- Fractions
    	$text = str_replace("â", '&#8531;', $text);
    	$text = str_replace("â", '&#8532;', $text);
    	$text = str_replace("â", '&#8533;', $text);
    	$text = str_replace("â", '&#8534;', $text);
    	$text = str_replace("â", '&#8535;', $text);
    	$text = str_replace("â", '&#8536;', $text);
    	$text = str_replace("â", '&#8537;', $text);
    	$text = str_replace("â", '&#8538;', $text);
    	$text = str_replace("â", '&#8539;', $text);
    	$text = str_replace("â", '&#8540;', $text);
    	$text = str_replace("â?", '&#8541;', $text);
    	$text = str_replace("â", '&#8542;', $text);
    	$text = str_replace("â", '&#8543;', $text);
    	$text = str_replace("Â¼", '&#188;', $text);
    	$text = str_replace("Â½", '&#189;', $text);
    	$text = str_replace("Â¾", '&#190;', $text);

    	return $text;
    }

    final static function anchor($href, $text, $attrs = array(), $ignore_params = false, $native_dom = false)
    {

        $anchor = html::element('a');
        $anchor->set('href', $href);
        $anchor->set('html', $text);

        $current_url = http::current_url();

        if($ignore_params){
            list($current_url) = explode('?', $current_url);
        }

        if($current_url == $href || (!strstr($href, '/') && $href == basename($current_url)) || ($href{0} == '/' && $current_url == FULLURL.$href)){
            $attrs['class'] = $attrs['class'] ? $attrs['class'].' highlighted' : 'highlighted';
        }else{

        }

        foreach($attrs as $key => $attr)
        {
            $anchor->set($key, $attr);
        }
        return ($native_dom) ? $anchor : $anchor->to_html() . "\n";
    }

    /**
     * converts a fetch_all result to an array to be used with html::select.
     * @param array $db_array array returned by fetch_all
     * @param string $value the key
     * @param string $html the html value
     * @return array
     */
    final static function db_to_select_array($db_array, $value, $html, $first_option = null)
    {
        $array = arrays::set_keys_values($db_array,$value,$html);

        if($first_option !== null){
            if(is_array($first_option)) {
                $array = $first_option + $array;
            } else {
                $array = array('' => $first_option) + $array;
            }
        }

        return $array;
    }

    final static function hidden($value, $name, $native_dom = false){
        $value = is_array($value) && isset($value[$name]) ? $value[$name] : $value;

        return self::input($value, $name, array('type' => 'hidden'), $native_dom);
    }

    static function paginate($params, $current_page, $max_pages, $pages, $org_url = '', $native_dom = false)
    {
        // Bert!!! um no!
        /*
        if($pages == 0){
            return '<strong>1</strong>';
        }
        */
        if($pages < 2)
        {
            return '';
        }

        if(!empty($org_url))
        {
            $url = explode('?', $org_url);
            $url = $url[0];

            $hash = explode('#', $org_url);
            $hash = $hash[1];
        }
        unset($params['p']);
        $query = http::build_query($params);
        $query = strlen($query) ? '&' . $query : '';
        $query .= strlen($hash) ? '#' . $hash : '';

        $paginated = html::element('div');
        //----------------------------------------------------------------------
        //-- Previous Pages
        //----------------------------------------------------------------------
        if ($current_page > 0)
        {
            $anchor = html::element('a');
            $anchor->set('text', 'PREVIOUS');
            $anchor->set('href', $url . '?p=' . ($current_page - 1) . $query);
            $anchor->set('class', 'pagenumber pageprev');
            $anchor->inject($paginated);

            $pagi_limit = $current_page - $max_pages;
            $pagi_limit = ($pagi_limit > 0) ? $pagi_limit : 0;
            foreach(range($pagi_limit, ($current_page - 1)) as $i)
            {
                $anchor = html::element('a');
                $anchor->set('text', $i + 1);
                $anchor->set('href', $url . '?p=' . $i . $query);
                $anchor->set('class', 'pagenumber');
                $anchor->inject($paginated);
            }
        }
        //----------------------------------------------------------------------
        //-- Current Pages
        //----------------------------------------------------------------------
        $strong = html::element('a');
        $strong->set('text', ($current_page + 1));
        $strong->set('class', 'pagenumberhighlight');
        $strong->inject($paginated);

        //----------------------------------------------------------------------
        //-- Next Pages
        //----------------------------------------------------------------------
        if ($current_page < $pages)
        {
            $pagi_limit = $current_page + $max_pages;
            $pagi_limit = ($pagi_limit < $pages) ? $pagi_limit : $pages;
            for($i = $current_page + 1; $i < $pagi_limit; $i++)
            {
                $anchor = html::element('a');
                $anchor->set('text', $i + 1);
                $anchor->set('href', $url . '?p=' . $i . $query);
                $anchor->set('class', 'pagenumber');
                $anchor->inject($paginated);
            }
            if($current_page + 1 != $pagi_limit)
            {
                $anchor = html::element('a');
                $anchor->set('text', 'NEXT');
                $anchor->set('href', $url . '?p=' . ($current_page + 1) . $query);
                $anchor->set('class', 'pagenumber pagenext');
                $anchor->inject($paginated);
            }
        }
        // Add to body
        if($native_dom){
            html::envelop($paginated);
        }
        return ($native_dom) ? $paginated : $paginated->get('html') . "\n";
    }

    static function paginate_pagerange($current_page, $per_page, $total){
        $from = (($current_page) * $per_page)+1;
        $to = ($current_page+1) * $per_page;
        $to = $to > $total ? $total : $to;
        return $from . ' - ' . $to . ' of ' . $total;
    }

    public static function active_location_picker($locations)
    {
        $sel_locs = array();
        foreach($locations as $location) {
            $sel_locs[$location['licensed_locations_id']] = $location['licensed_locations_name'];
        }
        asort($sel_locs);
        return html::select($sel_locs, 'members_last_licensed_location', ActiveMemberInfo::SingleLocationUserLocationId());
    }

    public static function location_picker($item, $errors, $item_name, $include_view_edit=true, $label='Location(s)')
    {
        if (ActiveMemberInfo::IsUserMultiLocation()) {
            $licensed_locations = arrays::sort(licensed_locations::get_licensed_locations_with_permission($item_name));
            if(ActiveMemberInfo::_IsAccountAdmin()) {
                array_unshift($licensed_locations, array('licensed_locations_id' => 'all', 'licensed_locations_name' => 'All Locations'));
            }

            $licensed_locations_chunks = array_chunk($licensed_locations, ceil((count($licensed_locations)) / min(3, ceil(count($licensed_locations) / 7))));

            $container = html::element('div');

            $formField = html::element('div')
                ->set('class', 'form-field');
            $formField->inject($container);

            $label = html::element('label')
                ->set('for', '')
                ->set('text', $label.':');
            $label->inject($formField);

            $multi_input = html::element('span')
                ->set('class', 'multiinput-container')
                ->set('id', 'licensed_locations');
            $multi_input->inject($formField);

            foreach ($licensed_locations_chunks as $licensed_locations_chunk) {
                $column = html::element('span')
                    ->set('class', 'num-' . count($licensed_locations_chunks));
                $column->inject($multi_input);

                foreach ($licensed_locations_chunk as $licensed_location) {

                    $label = html::element('label')
                        ->set('for', 'licensed_locations_' . http::encode_url($licensed_location['licensed_locations_id']));
                    $label->inject($column);

                    $input = html::element('input')
                        ->set('type', 'checkbox')
                        ->set('name', 'join_licensed_locations[]')
                        ->set('id', 'licensed_locations_' . http::encode_url($licensed_location['licensed_locations_id']))
                        ->set('value', $licensed_location['licensed_locations_id'])
                        ->set('text', $licensed_location['licensed_locations_name']);

                    if ($item[$item_name.'_all_locations'] == 1 ||
                        ($item['join_licensed_locations'] && in_array($licensed_location['licensed_locations_id'], $item['join_licensed_locations']))) {
                        $input->set('checked', 'checked');
                    }
                    $input->inject($label);

                    html::element('br')->inject($column);
                }
            }
            if ($errors['join_licensed_locations']) {
                html::errors('join_licensed_locations', $errors, true)->inject($formField);
            }

            if(ActiveMemberInfo::_IsAccountAdmin() && $include_view_edit) {
                html::checkboxfield('Location(s) can view: ', 1, $item_name.'_location_can_view', $item[$item_name.'_location_can_view'], array(), $errors, true)->inject($container);
                html::checkboxfield('Location(s) can edit: ', 1, $item_name.'_location_can_edit', $item[$item_name.'_location_can_edit'], array(), $errors, true)->inject($container);
            }
            return $container->to_html();
        }
        return '';
    }

    public static function location_filter($item_name, $current_filter = array())
    {
        if (ActiveMemberInfo::IsUserMultiLocation()) {
            $licensed_locations = arrays::sort(licensed_locations::get_licensed_locations_with_permission($item_name));
            array_unshift($licensed_locations, array('licensed_locations_id' => 'all', 'licensed_locations_name' => 'All Locations'));

            $container = html::element('div');

            foreach ($licensed_locations as $licensed_location) {
                $label = html::element('label')
                    ->inject($container);

                $input = html::element('input')
                    ->set('type', 'checkbox')
                    ->set('name', 'join_licensed_locations_id[]')
                    ->set('id', 'licensed_locations_' . http::encode_url($licensed_location['licensed_locations_id']))
                    ->set('value', $licensed_location['licensed_locations_id'])
                    ->set('text', $licensed_location['licensed_locations_name']);

                if (!$current_filter ||
                    (in_array($licensed_location['licensed_locations_id'], $current_filter))) {
                    $input->set('checked', 'checked');
                }
                $input->inject($label);
                html::element('br')
                    ->inject($container);
            }
            return $container->to_html();
        }
        return '';
    }
    
    public static function location_picker_dropdown($item_name, $html_name)
    {
        if (ActiveMemberInfo::IsUserMultiLocation()) {
            $licensed_locations = arrays::sort(licensed_locations::get_licensed_locations_with_permission($item_name));
          
            $sel_locs = array();
            foreach($licensed_locations as $location) {
                $sel_locs[$location['licensed_locations_id']] = $location['licensed_locations_name'];
            }
            
            asort($sel_locs);
            return html::select($sel_locs, $html_name);

        }
        return "<input type='hidden' name='". $html_name ."' value='" .ActiveMemberInfo::SingleLocationUserLocationId() . "'/>" ;
    }
    
    public static function help_button($name = null, $httplink = null, $content = null)
    {
        return '<a class="help-icon"' .
                ($name ? 'id="' . $name . '" name="' . $name . '" ' : '') .
                ($httplink ? 'href="' . $httplink . '"' : '') .
                ($content ? 'data-content="' . $content . '"' : '') .
                '><img alt="help?" src="/images/dialog-question.png"></a>';
    }

    public static function createHrLink($text, $url = null, $attrs = array(), $nativeDom = false) {
        $linkInfo = Services\Services::$hrProvider->getLinkInfo($url);

        if(!is_array($attrs)) {
            $attrs = array();
        }
        $attrs['target'] = $linkInfo['newWindow'] ? '_blank' : '_self';
        if(!isset($attrs['class'])) {
            $attrs['class'] = '';
        }
        $attrs['class'] .= ' docLibrary-format-hr360';
        return html::anchor('/members/hr/forward.php?url='.urlencode($url), $text, $attrs, $nativeDom);
    }
}
?>
