<?php
#REVISION: 1
final class http
{
    /**
     * Checks $_REQUEST for specific keys
     *
     * @param string | array $valid_keys
     * @return bool
     */
    static function in_request($valid_keys)
    {
        // Type Checking
        if(!is_array()) $case = array($valid_keys);

        $mount_keys = array_keys($_REQUEST);

        $valid_request = array_intersect($valid_keys, $mount_keys);
        return !empty($valid_request);
    }

    /**
     * If an ajax request is being processed
     *
     * @return bool
     */
    static function is_ajax()
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        }
        $accept = strtolower(str_replace(' ', '', $_SERVER['HTTP_ACCEPT']));
        if(strpos($accept, 'application/json') !== FALSE) {
            return true;
        }
        return false;
    }
    
    /**
     * If https protocal is being processed
     *
     * @return bool
     */
    public static function is_secure()
    {
        return isset($_SERVER['HTTPS']) ? true : false;
    }

    /**
     * If https protocol is being processed
     *
     * @return bool
     */
    static function redirect($page, $params = array())
    {
        if(!framework::is_from_call_action()){
            if($params){
                $query = http_build_query($params);
                
                $page .= strstr($page, '?') ? '&' . $query : '?' . $query;
            }
            if (!headers_sent())
            {
                header('Location: ' . $page);
            }
            else
            {
                echo '<script type="text/javascript">document.location.href="' . $page . '";</script>"'
                    . '<meta http-equiv="refresh" content="0;url=' . $page . '" />'
                    . '<noscript>Please click <a href="' . $page . '">' . $page . '</a> to continue.</noscript>';
            }
            exit;
        }
    }

    static function parse_query($url)
    {
        /*
         * TODO: parse multi array and deal with &amp;
         */
        $url   = parse_url($url);
        parse_str($url['query'], $query);
        return $query;
    }

    static function build_query(array $params)
    {
        return http_build_query($params);
    }

    static function protect($session_variable, $redirect)
    {
        if(!$_SESSION[$session_variable])
        {
            self::redirect($redirect);
        }
    }
    
    static function encode_url($string)
    {
        $string = trim($string);
        if (ctype_digit($string))
        {
                return $string;
        }
        else
        {
            // replace accented chars
            $accents = '/&([A-Za-z]{1,2})(grave|acute|circ|cedil|uml|lig);/';
            $string_encoded = htmlentities($string,ENT_NOQUOTES,'UTF-8');

            $string = preg_replace($accents,'$1',$string_encoded);

            // clean out the rest
            $replace = array('([\40])','([^a-zA-Z0-9-])','(-{2,})');
            $with = array('-','','-');
            $string = preg_replace($replace,$with,$string);
        }

        return strtolower($string);
    }  
    
    /*
    $post = array(
        'articles_id' => '',
        'publish_date' => date('m/d/Y', $date),
        'publish_time' => date('g:i:s a', $date),
        'articles_active' => '1',
        'join_articles_categories_id' => rand(40, 42),
        'join_articles_tags' => '',
        'articles_byline' => $wordlist[rand(0, count($wordlist))],
        'articles_title' => ucfirst($name),
        'articles_subtitle' => ucfirst($wordlist[rand(0, count($wordlist))]),
        'articles_body' => nl2br(str_replace("\n\n\n", "\n", strip_tags($desc))),
        'articles_teaser' => substr(strip_tags($desc), 0, 255) . '...',
        'articles_page_url' => '',
        'articles_page_title' => '',
        'articles_page_keywords' => '',
        'articles_page_description' => '',
        'articles_footnotes' => '',
        'articles_copyright' => '',
        'articles_order' => 0
    );
    $resp = http::request(FULLURL . 'admin/articles/edit.php', array(
        'POST' => $post,
        'Cookies' => SITE_PATH . '/data/cookies.txt'
    ));
    */
    static function request($request_url, array $params = array())
    {
        $data = $params;
        // create a new cURL resource
        $curl_resource = curl_init();

        // set URL and other appropriate options
        $options = array();
        $url = array();
        if(isset($request_url))
        {
            $options[CURLOPT_URL] = $request_url;
            $url = parse_url($options[CURLOPT_URL]);
        }
        
        if($data['timeout']){
            curl_setopt($curl_resource, CURLOPT_TIMEOUT, $data['timeout']);
            unset($data['timeout']);
        }
        
        if(!isset($data['exclude_header']) or !$data['exclude_header'])
        {
            $options[CURLOPT_HEADER] = 1;
        }
        else
        {
            $options[CURLOPT_HEADER] = 0;
        }
        unset($data['exclude_header']);
        
        if(isset($data['POST']) and !empty($data['POST']))
        {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = http::build_query($data['POST']);
        }
        else
        {
            $options[CURLOPT_POST] = 0;
        }
        unset($data['POST']);
        
        if(isset($data['User-Agent']) and !empty($data['User-Agent']))
        {
            $options[CURLOPT_USERAGENT] = $data['User-Agent'];
        }
        unset($data['User-Agent']);
        
        
        if(strtolower($url['scheme']) === 'https') {
            curl_setopt($curl_resource, CURLOPT_SSL_VERIFYPEER, false);
        }
        $options[CURLOPT_FAILONERROR] = 1; 
        $options[CURLOPT_FOLLOWLOCATION] = 1;
        if(isset($data['Follow-Location']) and !empty($data['Follow-Location']))
        {
            $options[CURLOPT_FOLLOWLOCATION] = $data[CURLOPT_FOLLOWLOCATION] = $data['Follow-Location'];
            unset($data['Follow-Location']);
        }
        
        $options[CURLOPT_ENCODING] = 'gzip,deflate';
        
        if(!empty($data))
        {
            //------------------------------------------------------------------
            //-- Rebuild Custom Header
            //------------------------------------------------------------------
            // Build Header
            $header = array();
            $request = $data['request'];
            unset($data['request']);
            foreach($data as $key => $value)
            {
                $header[strtolower($key)] = "{$key}: {$value}";
            }
            $header = array_values($header);
            
            //------------------------------------------------------------------
            //-- Set custom Header to Curl
            //------------------------------------------------------------------
            $options[CURLOPT_HTTPHEADER] = $header;
        }

        curl_setopt_array($curl_resource, $options);

        // grab URL and pass it to the browser
        ob_start(); 
        curl_exec($curl_resource);
        $response = ob_get_contents();
        ob_end_clean();
        
        // close cURL resource, and free up system resources
        curl_close($curl_resource);
        if($options[CURLOPT_HEADER] == 1) {
            $response = explode("\r\n\r\n", $response);
            if (count($response) > 1) {
                $header = array_shift($response);
                $response = implode("\r\n\r\n", $response);
            } else {
                $response = $response[0];
            }

            $tokens = explode("\n", $header);
            $header = array();
            foreach ($tokens as $token) {
                $parts = explode(':', $token);
                if (count($parts) > 1) {
                    $header[array_shift($parts)] = trim(implode(':', $parts));
                } else {
                    $header [] = trim(implode(':', $parts));
                }
            }

            if ($header['Content-Encoding']) {
                $g = tempnam('/tmp', 'ff');
                @file_put_contents($g, $response);
                ob_start();
                readgzfile($g);
                $d = ob_get_clean();
                $response = $d;
                unset($g);
                unset($d);
            }
        } else {
            $header = null;
        }
        return array(
            'response' => $response,
            'header' => $header
        );
    }
    
    static function current_url(){
        $url = 'http' . ($_SERVER["HTTPS"] == "on" ? 's' : '') . '://';
        
        if($_SERVER["SERVER_PORT"] != "80"){
            $url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        }else{
            $url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        
        return $url;
    }
    
    static function current_page(){
        $temp1 = explode('/', http::current_url());
        $end = array_pop($temp1);
        $temp2 = explode('?', $end);
        $file = array_shift($temp2);
        
        return $file;
    }
}
?>
