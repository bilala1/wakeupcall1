<?php
final class image 
{
    public $image;
    public $w; 
    public $h;
    public $format;
    
    public function __construct($filename)
    {
        $this->loadit($filename);
    }

    /**
     * Load a file into class
     * @param string $filename the path to the image to load
     */
    public function loadit($filename)
    {
        $image = @imagecreatefromjpeg($filename);
        
        if(!$image){
            $image = @imagecreatefrompng($filename);
        }
        if(!$image){
            $image = @imagecreatefromgif($filename);
        }
        
        $this->image = $image;
        $this->w = imagesx($this->image);
        $this->h = imagesy($this->image);
        
        return $this;
    }
    
    public function save_original($filename, $quality = 85){
        $this->save($this->image, $filename, $quality);
        
        return $this;
    }
    
    /**
     * Change the format that images are saved
     * @param string $ext The extension for the files saved
     */
    public function set_format($ext){
        $this->format = $ext;
    }
    
    public static function loadable($filename, $location = null)
    {
        // Find the extension...
        $loadable = self::valid($filename, $location);
        
        if($loadable and $location !== null)
        {
            $ext = self::extension($location);
            $loadable = self::is_loadable($location, $ext);
        }
        else if($loadable)
        {
            $loadable = self::is_loadable($filename);
        }
        
        return $loadable;
    }
    
    public function resize($dest_file, $width = null, $height = null)
    {
        $image = self::scale($this->image, $width, $height);
        $this->save($image, $dest_file, 85);           
        
        return $this;
    }
    
    public function crop($dest_file, $startx, $starty, $width, $height)
    {
        $dest_image = imagecreatetruecolor($width, $height);
        imagecopy($dest_image, $this->image, 0, 0, $startx, $starty, $width, $height);
        
        $this->save($dest_image, $dest_file, 85);
        
        return $this;
    }
    
    public function save($source, $dest_file, $quality)
    {
        switch($this->format)
        {
            default:
            case "jpg":
            case "jpeg":
                imagejpeg($source, $dest_file, $quality);
            break;
            
            case "gif":
                imagegif($source, $dest_file, $quality);
            break;
            
            case "png":
                imagepng($source, $dest_file, $quality);
            break;
        }
    }

    static function scale(&$image, $maxwidth=null, $maxheight=null, $filename=null)
    {
        $width = imagesx($image);
        $height = imagesy($image);
        if($maxwidth) {
            $newwidth  = $maxwidth;
            $newheight = $height * $newwidth / $width;
        } else if($maxheight) {
            $newheight = $maxheight;
            $newwidth  = $width * $newheight / $height;
        } else {
            throw new Exception('You never picked a width or height');
        }
        if($maxwidth and $maxheight and $maxheight < $newheight) {
            $newheight =& $maxheight;
            $newwidth  = $width * $newheight / $height;
        }
        if($maxwidth and $maxheight and $maxwidth < $newwidth) {
            $newwidth  =& $maxwidth;
            $newheight = $height * $newwidth / $width;
        }
        
        if($newwidth > $width){
            $newwidth = $width;
        }
        if($newheight > $height){
            $newheight = $height;
        }
        
        $resampled = imagecreatetruecolor($newwidth, $newheight);
        $extension =  self::extension($filename);
        if($extension == 'png')
        {
            self::transparency($resampled);
        }
        imagecopyresampled($resampled, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        return self::static_save($resampled, $filename);
    }
    
    static function extension($filename)
    {
     	$imagearray = explode("." , $filename);
    	$extension =  strtolower(array_pop($imagearray));
    	return $extension;
    }
    
    static function valid($filename, $location = null)
    {
        $valid = false;
        $filetypes = array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        );
        $mimetypes = array(
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png',
        );
        
        if(in_array(image::extension($filename), $filetypes))
        {
            $valid = true;
        }
        
        if($location !== null)
        {
            exec('file -i ' . escapeshellarg($location), $return);
            $return = explode(':', array_pop($return));
            $return = array_pop($return);
            $mimetype = strtolower(trim($return));
            if(in_array($mimetype, $mimetypes))
            {
                $valid = true;
            }
        }
        
        return $valid;
    }
    
    static function transparency(&$image)
    {
        imagealphablending($resampled, false);
        $color = imagecolorallocatealpha($resampled, 0, 0, 0, 127);
        imagefill($resampled, 0, 0, $color);
        imagesavealpha($resampled, true);
    }

    static function load($filename)
    {
        $image = @imagecreatefromjpeg($filename);
        if (!$image)
        {
            $image = @imagecreatefrompng($filename);
        }
        if (!$image)
        {
            $image = @imagecreatefromgif($filename);
        }
        return $image;
    }

    static function static_save($image, $filename=null)
    {
        if($filename == null)
        {
            return $image;
        }
        $extension =  self::extension($filename);
        if($extension == 'png')
        {
            imagepng($image, $filename, 9);
        }
        else
        {
            imagejpeg($image, $filename, 85);
        }
        return true;        
    }
    
    static function is_loadable($filename, $extension = null)
    {
        $pixel_max = 4;
        //-- If they upload a PNG then assume its a alpha mapped 6 million colors
        if(self::extension($filename) === 'png' or $extension === 'png')
        {
            $pixel_max = 5;
        }
    
        list($width, $height, $type, $attr) = getimagesize($filename);
        $image_memory = ($width * $height * $pixel_max);
        $current_memory = memory_get_usage(true);
        $needed_memory = ($image_memory + $current_memory) / 1024 / 1024;
        $max_memory = str_replace('M', '', ini_get('memory_limit'));
        $process_image = true;
        if($max_memory and $needed_memory > $max_memory) {
            $process_image = false;
        }
        return $process_image;
    }
    
    static function square_crop($image, $scale, $filename)
    {
    	$width = imagesx($image);    
    	$height = imagesy($image);    
    	$ratiox = $width / $height * $scale;    
    	$ratioy = $height / $width * $scale;    
    	$newheight = ($width <= $height) ? $ratioy : $scale;    
    	$newwidth = ($width <= $height) ? $scale : $ratiox;    
    	$cropx = ($newwidth - $scale != 0) ? ($newwidth - $scale) / 2 : 0;    
    	$cropy = ($newheight - $scale != 0) ? ($newheight - $scale) / 2 : 0;    
    	$resampled = imagecreatetruecolor($newwidth, $newheight);    
    	$cropped = imagecreatetruecolor($scale, $scale);    
    	imagecopyresampled($resampled, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);     
    	imagecopy($cropped, $resampled, 0, 0, $cropx, $cropy, $newwidth, $newheight);
        return self::static_save($cropped, $filename);
	}
}
?>
