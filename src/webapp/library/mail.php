<?php
$paths = array(
    realpath(SITE_PATH. '/library'),
    '.'
);
set_include_path(implode(PATH_SEPARATOR, $paths));
require_once 'Zend/Mail/Transport/Smtp.php';
use \Mailgun\Mailgun;

final class mail
{
    public static function generate_boundary()
    {
        return rand(0, 9) . rand(1000000000, 9999999999) . rand(1000000000, 9999999999) . rand(10000, 99999);
    }

    public static function batchSend($from, $tos, $subject, $message, $extra_headers = '')
    {
        if(DISABLE_EMAIL) {
            return true;
        }
        $mg = new Mailgun(MAILGUNAPIKEY);
        $domain = MAILGUN_DOMAIN;

        $batchMsg = $mg->BatchMessage($domain);

        $batchMsg->setSubject($subject);
        $batchMsg->setTextBody(strip_tags(nl2br($message)));
        $batchMsg->setHtmlBody($message);
        $batchMsg->setFromAddress($from);

        foreach ($tos as $email=>$vars) {
            $address = self::parse_address($email);
            if (filter_var($address['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($address['address'])) {
                $batchMsg->addToRecipient($email, $vars);
                $doSend = true;
            }
        }

        if(!$doSend) {
            return true;
        }

        if ($cc !== null) {
            $ccs = self::parse_addresses($cc);
            foreach ($ccs as $cc) {
                if (filter_var($cc['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($cc['address'])) {
                    $mail->addCc($cc['address'], $cc['name']);
                }
            }
        }
        if ($bcc !== null) {
            $bccs = self::parse_addresses($bcc);
            foreach ($bccs as $bcc) {
                if (filter_var($bcc['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($bcc['address'])) {
                    $mail->addBcc($bcc['address'], $bcc['name']);
                }
            }
        }
        if (!empty($extra_header)) {
            $headers = explode("\n", $extra_header);
            foreach ($headers as $header) {
                $parts = explode(':', $header);
                $key = array_shift($parts);
                $value = implode(':', $parts);
                $batchMsg->addCustomHeader($key, $value);
            }
        }

        $batchMsg->finalize();
        return $batchMsg->getMessageIds();
    }

    public static function send($from, $to, $subject, $message, $cc = null, $bcc = null, $extra_headers = '')
    {
        if(DISABLE_EMAIL) {
            return true;
        }

        $config = array('ssl' => 'tls',
            'port' => '587',
            'auth' => 'login',
            'username' => SMTP_USERNAME,
            'password' => SMTP_PASSWORD);

        $transport = new Zend_Mail_Transport_Smtp(SMTP_SERVER, $config);

		//-- Load Zend Mail
		require_once 'Zend/Mail.php';

		$mail = new Zend_Mail('UTF-8');
		$mail->setBodyText(strip_tags(nl2br($message)));
		$mail->setBodyHtml($message);
		$mail->setSubject($subject);
		$address = self::parse_address($from);
		$mail->setFrom($address['address'], $address['name']);

        $doSend = false;
        $tos = self::parse_addresses($to);
        foreach ($tos as $to) {
            if (filter_var($to['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($to['address'])) {
                $mail->addTo($to['address'], $to['name']);
                $doSend = true;
            }
        }

        if(!$doSend) {
            return true;
        }

		if ($cc !== null) {
			$ccs = self::parse_addresses($cc);
			foreach ($ccs as $cc) {
                if (filter_var($cc['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($cc['address'])) {
                    $mail->addCc($cc['address'], $cc['name']);
                }
			}
		}
		if ($bcc !== null) {
			$bccs = self::parse_addresses($bcc);
			foreach ($bccs as $bcc) {
                if (filter_var($bcc['address'], FILTER_VALIDATE_EMAIL) && mail::filter_test_email($bcc['address'])) {
                    $mail->addBcc($bcc['address'], $bcc['name']);
                }
            }
		}
		if (!empty($extra_header)) {
			$headers = explode("\n", $extra_header);
			foreach ($headers as $header) {
				$parts = explode(':', $header);
				$key = array_shift($parts);
				$value = implode(':', $parts);
				$mail->addHeader($key, $value);
			}
		}

        return $mail->send($transport);
    }

	public static function parse_addresses($emails) {
		$list = explode(',', $emails);
		$addresses = array();
		foreach ($list as $email) {
            $addresses[] = self::parse_address($email);
        }
		return $addresses;
	}

    public static function filter_test_email($email) {
        if(DEPLOYMENT == 'production') {
            return true;
        }
        if(!isset($_SESSION['testing_email_address_filter'])) {
            return true;
        }
        return $email == $_SESSION['testing_email_address_filter'];
    }


	public static function parse_address($email) {
                $preprocess = str_replace('"', '', $email);
                $preprocess = str_replace('<', '', $preprocess);
                $preprocess = str_replace('>', '', $preprocess);
		$parts = explode(' ', $preprocess);
		$address = trim(array_pop($parts));
		$name = trim(implode(' ', $parts));

		return array(
			'address' => $address,
			'name' => $name
		);
	}

    function auth_send($from, $to, $subject, $message){
        //Verify that configuration settings are set
        if(!(SMTP_SERVER && SMTP_PORT && SMTP_TIMEOUT && SMTP_USERNAME && SMTP_PASSWORD)){
            echo 'SMTP settings are not set<br />';
            return false;
        }

        //Connect to the host on the specified port
        $smtpConnect = fsockopen(SMTP_SERVER, SMTP_PORT, $errno, $errstr, SMTP_TIMEOUT);
        $smtpResponse = fgets($smtpConnect, 515);
        if(empty($smtpConnect)){
            //echo "Failed to connect: $smtpResponse";
            return false;
        }
        else{
            $logArray['connection'] = "Connected: $smtpResponse";
        }

        //Say Hello to SMTP
        fputs($smtpConnect, "HELO localhost\r\n" );
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['heloresponse'] = "$smtpResponse";

        //Request Auth Login
        fputs($smtpConnect,"AUTH LOGIN\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['authrequest'] = "$smtpResponse";

        //Send username
        fputs($smtpConnect, base64_encode(SMTP_USERNAME) . "\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['authusername'] = "$smtpResponse";

        //Send password
        fputs($smtpConnect, base64_encode(SMTP_PASSWORD) . "\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['authpassword'] = "$smtpResponse";

        //Email From
        fputs($smtpConnect, "MAIL FROM: <$from>\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['mailfromresponse'] = "$smtpResponse";

        //Email To
        fputs($smtpConnect, "RCPT TO: <$to>\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['mailtoresponse'] = "$smtpResponse";

        //The Email
        fputs($smtpConnect, "DATA\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['data1response'] = "$smtpResponse";

        //Construct Headers
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "To: $to\r\n";
        $headers .= "From: $from\r\n";

        fputs($smtpConnect, "Subject: $subject\r\n$headers\r\n\r\n$message\r\n.\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['data2response'] = "$smtpResponse";

        // Say Bye to SMTP
        fputs($smtpConnect,"QUIT\r\n");
        $smtpResponse = fgets($smtpConnect, 515);
        $logArray['quitresponse'] = "$smtpResponse";

        /*foreach($logArray as $key => $val){
            echo '<b>'.$key.':</b> '.$val."<br>";
        }*/

        return true;
    }

    /*
    mail::send_template(
        $from     = SALES_EMAIL,
        $to       = $member['members_email'],
        $subject  = 'Test Notification',
        $template = 'welcome.php',
        $vars     = get_defined_vars(),
        $cc       = array('jr@comentum.com', 'jm@comentum.com'),
        $bcc      = 'bk@comentum.com'
    );
    */
    static function send_template($_from, $_to, $_subject, $_template, $vars, $_cc = null, $_bcc = null, $_extra_headers = ''){
        extract($vars);

        ob_start();
        include VIEWS . '/emails/' . $_template;
        $message = ob_get_clean();

        mail::send($_from, $_to, $_subject, $message, $_cc, $_bcc, $_extra_headers);
    }
}
?>
