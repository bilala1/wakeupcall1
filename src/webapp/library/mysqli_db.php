<?php
class mysqli_db
{
    public $result;
    public $sql;
    public $params;
    public $version = '1.20beta2';
    protected $last_id;
    protected $count;
    protected $num_rows;
    protected $affected_rows;
    protected $debug;
    protected $link;
    protected $master_link;
    protected $query;
    protected $stmt;
    protected $error;
    protected $time;
    private $factory;
    private static $instance;

    function __construct($server=null, $user=null, $passwd=null, $db=null, $port=null, $socket=null)
    {
        $server = ($server !== null) ? $server : DB_SERVER;
        $user = ($user !== null) ? $user : DB_USER;
        $passwd = ($passwd !== null) ? $passwd : DB_PASSWD;
        $db = ($db !== null) ? $db : DB_NAME;
        $port = ($port !== null) ? $port : null;
        $port = ($port === null && defined('DB_PORT')) ? DB_PORT : $port;
        $socket = ($socket === null && defined('DB_SOCK')) ? DB_SOCK : $socket;
        if(($server !== null) and ($user !== null) and ($passwd !== null) and ($db !== null)) {
            $this->connect($server, $user, $passwd, $db, $port, $socket);
        }
    }

    function __destruct() 
    {
        global $CONSTANTS;
        if($CONSTANTS['queries'] !== null and $_REQUEST['mysqli_profile'])
        {
            
            ob_start(); 
            var_dump($_SERVER['REQUEST_URI']);
            var_dump($CONSTANTS['queries']);
            error_log(ob_get_contents(), 1, "jm@comentum.com"); 
            ob_end_clean();
        }
        $this->link->close();
        if($this->master_link !== null) 
        {
            $this->master_link->close();
        }
    }
    
    public function get_link() {
        return $this->link;
    }
     
    public function get_master($server=null, $user=null, $passwd=null, $db=null, $port=null)
    {
        if($this->master_link === null) {
            $server = ($server !== null) ? $server : DB_MASTER_SERVER;
            $user = ($user !== null) ? $user : DB_MASTER_USER;
            $passwd = ($passwd !== null) ? $passwd : DB_MASTER_PASSWD;
            $db = ($db !== null) ? $db : DB_MASTER_NAME;
            $port = ($port !== null) ? $port : null;
            $port = ($port === null && defined('DB_PORT')) ? DB_MASTER_PORT : $port;
            $this->set_master_link(new mysqli($server, $user, $passwd, $db));

            if (mysqli_connect_errno() and defined('DB_DEBUG') and constant('DB_DEBUG') === true)
            {
                echo '[MySQLi] Connection Error: '.mysqli_connect_errno();
            }
        }

        return $this->master_link;
    }

    public function set_master_link($link)
    {
        $this->master_link = $link;
        return $this;
    }

    /**
     * @param null $server
     * @param null $user
     * @param null $passwd
     * @param null $db
     * @param null $port
     * @param null $socket
     * @return mysqli_db
     */
    public static function init($server=null, $user=null, $passwd=null, $db=null, $port=null, $socket=null)
    {
        $server = ($server !== null) ? $server : DB_SERVER;
        $user = ($user !== null) ? $user : DB_USER;
        $passwd = ($passwd !== null) ? $passwd : DB_PASSWD;
        $db = ($db !== null) ? $db : DB_NAME;
        $port = ($port !== null) ? $port : null;
        $port = ($port === null && defined('DB_PORT')) ? DB_PORT : $port;
        $socket = ($socket === null && defined('DB_SOCK')) ? DB_SOCK : $socket;
        if (!isset(self::$instance))
        {
            $c = __CLASS__;
            self::$instance = new $c($server, $user, $passwd, $db, $port, $socket);
        }

        return self::$instance;
    }

    public function close() 
    {
        $this->link->close();
    }

    public function connect($server, $user=null, $passwd=null, $db=null, $port=null, $socket=null) 
    {
        $this->link = new mysqli($server, $user, $passwd, $db, $port, $socket);

        if (mysqli_connect_errno() and defined('DB_DEBUG') and constant('DB_DEBUG') === true)
        {
            echo '[MySQLi] Connection Error: '.mysqli_connect_errno();
        }
    }
    
    public function create_cache_table()
    {
        $this->query('
            CREATE TABLE IF NOT EXISTS `mysqli_db_cache` (
              `cache_key` varchar(32) NOT NULL,
              `serialized_php` text NOT NULL,
              PRIMARY KEY (`cache_key`)
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;        
        ');  
    }

    public function fetch_all($query=null, $params=null, $cached = false) 
    {
        global $CONSTANTS;
        $t = microtime(true);
        if($cached !== false)
        {
            $this->create_cache_table();
            if(!is_array($cached))
            {
                $cached = array($cached);
            }
            $memkey = '';
            foreach($cached as $table_name)
            {
                $row = $this->fetch_one('SHOW TABLE STATUS WHERE Name = ' . $this->filter($table_name));
                $memkey .= ' ' . $row['Update_time']; 
            }
            $memkey .= '' . $query;
            if($params !== null)
            {
                foreach($params as $key => $param) 
                {
                 	if(is_object($replacement) and get_class($param) == 'SQL')
                 	{
                 	    $memkey .= ' ' . $param->value;
                 	}
                 	else
                 	{
                 	    $memkey .= ' ' . $param;
                 	}
                }
            }
            $memkey = md5($memkey . $cached_key);
        }
        if($cached !== false and $results = $this->fetch_one('SELECT * FROM mysqli_db_cache WHERE cache_key = ?', array($memkey)))
        {
            $results = unserialize($results['serialized_php']);
            $this->result = $results;
        }
        else
        {
        	if($query !== null and $params !== null) 
        	{
        		$this->query($query, $params);
                $this->result = $this->fetch_all();
        	} 
        	else if($query !== null) 
        	{
        		$this->query($query);
                $this->result = $this->fetch_all();
        	}
        	else
        	{
                $results = array();
                for($i=0; $i < $this->count; $i++) 
                {
                    $results[] = $this->fetch_one();
                }
                $this->result = $results;
            }
            if($cached !== false)
            {
                $this->query('INSERT INTO mysqli_db_cache (cache_key, serialized_php) VALUES(?, ?)', array($memkey, serialize($this->result)));
            }
        }
        $this->time = microtime(true) - $t;
        $CONSTANTS['queries'] []= array(
            'query' => $this->debug(),
            'time' => $this->time()
        );
        return $this->result;
    }

    public function time()
    {
        return $this->time;
    }    

    public function fetch_one($query=null, $params=null) 
    {
    	if($query !== null and $params !== null) 
    	{
    		$this->query($query, $params);
			return $this->fetch_one();
    	} 
    	else if($query !== null) 
    	{
    		$this->query($query);
			return $this->fetch_one();
    	}
        if(is_object($this->stmt)) 
        {
            $params = array();
            $args = array();
            $data = $this->stmt->result_metadata();
            while ($field = mysqli_fetch_field($data)) 
            {
                ${$field->name} = false;
                $params[] = $field->name;
                $args[] = &${$field->name};
            }
            call_user_func_array(array($this->stmt,'bind_result'), $args);
            $result = false;
            if($this->stmt->fetch()) 
            {
                $result = array();
                foreach($params as $param) 
                {
                    if($$param === null) { continue; }
                    $result[$param] = $$param;
                }
                if(empty($result)) { $result = false; }
            }
            $this->result = $result;
        } 
        else 
        {
            $this->result = mysqli_fetch_assoc($this->query);
            if($this->link->error) 
            {
                $this->error = $this->link->error;
            }
        }
        return $this->result;
    }

    public function fetch_singlet($query=null, $params=null) 
    {
    	if($query !== null and $params !== null) 
    	{
    		$this->query($query, $params);
			return $this->fetch_singlet();
    	} 
    	else if($query !== null) 
    	{
    		$this->query($query);
			return $this->fetch_singlet();
    	}
        $singlet = $this->fetch_one();
        if($singlet) 
        {
            $keys = array_keys($singlet);
            return $singlet[$keys[0]];
        }
        return false;
    }
	
	public function fetch_singlets($query=null, $params=null)
	{
        global $CONSTANTS;
        $t = microtime(true);
    	if($query !== null and $params !== null) {
    		$this->query($query, $params);
			return $this->fetch_singlets();
    	} 
    	else if($query !== null) 
    	{
    		$this->query($query);
			return $this->fetch_singlets();
    	}
        $rows = $this->fetch_all();
		$variable_name = 'temp_' . uniqid();
        global $$variable_name;
        $$variable_name = array();
        array_walk_recursive($rows, 
            create_function(
                '$item, $key', 
                'global $' . $variable_name . ';
                    $' . $variable_name . ' []= $item;
                '
            )
        );
        
        $this->time = microtime(true) - $t;
        $CONSTANTS['queries'] []= array(
            'query' => $this->debug(),
            'time' => $this->time()
        );
		return $$variable_name;
	}

    public function decache() {
        if(is_object($this->stmt)) 
        {
            ob_start();
            $this->stmt->close();

            $message = ob_get_contents();
            ob_end_clean();
        }
        $this->stmt = null;
        $this->count = null;
        $this->params = array();
        $this->last_id = null;
        $this->num_rows = null;
        $this->affected_rows = null;
        $this->error = false;
        $this->query_link = false;
    }

    public function debug() 
    {
        return $this->debug;
    }

    public function error() 
    {
        return $this->error;
    }


    protected function sql_filter(&$item, $key) 
    {
        $item = $this->link->real_escape_string($item);
        if(!is_numeric($item)) 
        {
            $item = '"' . $item . '"';
        }
    }

    public function filter($args) {
        if(is_array($args)) 
        {
             array_walk_recursive($args, array($this, 'sql_filter'));
        } 
        else 
        {
            $this->sql_filter($args, null);
        }

        return $args;
    }
    
    public function filter_in($args){
        //an empty in clause causes an error, so this will make it not empty
        if(empty($args)){
            $args[] = 'some dummy text that will hopefully never match anything!@#';
        }
        
        return implode(', ', (array)$this->filter($args));
    }

    public function count() 
    {
        return $this->count;
    }

    public function last_id() 
    {
        $this->insert_id = (!is_object($this->stmt)) ? mysqli_insert_id($this->query_link) : mysqli_stmt_insert_id($this->stmt);
        return $this->insert_id;
    }

    public function prepare($stmt) 
    {
        $this->sql = $stmt;
        return $this;
    }

    public function bind($params) 
    {
        $nargs = func_num_args();
        if(is_array($params)) {
            $this->params = (!empty($this->params)) ? array_merge($params, $this->params) : $params;
        } else {
            $this->params[] = $params;
        }
        if($nargs > 1) {
        	$larg = func_get_args();
            for ($i = 1; $i < $nargs; $i++) {
                $this->bind($larg[$i]);
            }
        }
        return $this;
    }

    public function query($stmt=null, $params=null) 
    {
        $link = $this->link;
        $action = strtolower(substr(trim($stmt), 0, 6));
            
        if(defined('DB_MASTER_SERVER') and $action != 'select')
        {
            //var_dump($action);
            //echo "<br />USING MASTER";
            $link = $this->get_master();
        }
        else
        {
            //echo "<br />USING CONFIG: ", DB_TYPE;
        }
        
        $stmt = (empty($stmt)) ? $this->sql : $stmt;
        $params = (!empty($this->params)) ? $this->params : $params;
        
        $this->sql = $stmt;
        $this->debug = $stmt;
        //-- Clear last query results;
        $this->decache();

        if(!empty($params)) {
            $stmt_count = explode('?', $this->sql);

            //-- TODO: allow ?arg arguments

            //-- Allows pure non-filter SQL
            $stmt = $this->statment_parser($stmt, $params);
            //-- Rebuild the $param index
            $params = array_values($params);
            $this->debug = $stmt;

            //-- Prepare Statment & Save Debug
            $this->stmt = mysqli_stmt_init($link);
            $this->stmt->prepare($stmt);
            /* i - int, d - float, s - str, b - blob */
            $binds = '';
            foreach($params as $key=>$param) 
            {
                $reference = 'arg' . $key;
                global $$reference;
                
                if(is_float($param)) {
                    $param_type = 'd';
                } else if (is_int($param)) {
                    $param_type = 'i';
                } else {
                    $param_type = 's';
                }
                $binds .= $param_type;
            }
            if(!empty($params)) {
                $this->debug = $this->statment_parser($stmt, $params, false);
            }
            
            $tmp = array();
            foreach($params as $key => $param) {
                $reference = 'arg' . $key;
                global $$reference;
                $tmp[$key] = &$$reference;
                $$reference = $param;
            }
            $params = $tmp;
            
            $args = array($binds);
            $args = array_merge($args, $params);

            if(empty($params) and empty($binds)) {
            	$this->decache();
            	return $this->query($stmt);
            }
            
            ob_start();
            call_user_func_array(array($this->stmt,'bind_param'), $args);

            $message = ob_get_contents();
            ob_end_clean();
            
            ob_start();
            $this->stmt->execute();
            $this->stmt->store_result();

            $message = ob_get_contents();
            ob_end_clean();
            
            if(stristr($message, 'invalid'))
            {
                $backtrace = debug_backtrace();
                echo '<b>'.$backtrace[1]['file'].' &nbsp; <span style="color:red">LINE: '.$backtrace[1]['line']."</span></b>\n<br />";
                echo '<b>'.$backtrace[2]['file'].' &nbsp; <span style="color:red">LINE: '.$backtrace[2]['line']."</span></b>\n<br />";
            }

        } else {
            //-- Non-Prepared Statement
            $this->query = mysqli_query($link, $stmt);
        }
        
        $this->error = mysqli_error($link);
        $this->query_link = $link;

        if(false !== stristr(substr(trim($stmt), 0, 7), 'select'))
        {
            ob_start();
            $this->num_rows = (!is_object($this->stmt)) ? mysqli_num_rows($this->query) : mysqli_stmt_num_rows($this->stmt);
            $message = ob_get_contents();
            ob_end_clean();
            
            $this->count = $this->num_rows;
        } 
        else
        {
            $this->affected_rows = (!is_object($this->stmt)) ? mysqli_affected_rows($link) : mysqli_stmt_affected_rows($this->stmt);
            $this->count = $this->affected_rows;
        }
        
        if($this->error and defined('DB_DEBUG') and constant('DB_DEBUG') === true)
        {
            echo $this->error . "\n<br />SQL: " . $this->debug . "\n<br />\n<br />";
        }
        
        return $this;
    }
    
    public static function SQL($value) {
        return new SQL($value);
    }
    
    /**
     * Escapes a column to be used for order by, group by...
     * @param string $param can be table.column or just column
     * @return string
     */
    public function escape_column($column){
        $column = str_replace('`','``',$column);
        $sections = explode('.', $column);
        return '`' . implode('`.`', $sections) . '`';
    }

    /**
     * Only parses ?'s that are associated with the SQL class and removes them from the parameters
     * @param string $string
     * @param array $params
     * @param bool $only_classes
     * @return string
     */
    public function statment_parser($string, &$params = array(), $only_classes = true)
    {
        $matched = preg_match_all('/(\?)(?=(?:[^"]|"[^"]*")*$)/',
            $string, $matches, PREG_OFFSET_CAPTURE
        );

        if($matched === 0)
        {
            return $string;
        }

        // I only care about the first index of matches
        $matches = $matches[0];
        $total_matched = count($matches);

        //-- "$match" need be referenced so we can adjust the next match without
        //-- reinitializing the foreach
        foreach ($matches as $index => &$match)
        {
            $replacement = $params[$index];

            if ((!is_object($replacement) or (is_object($replacement) and get_class($replacement) !== 'SQL')) and $only_classes === true)
            {
                //unset($match);
                continue;
            }

            if (is_object($replacement) and get_class($replacement) === 'SQL')
            {
                $replacement = $replacement->value;
                unset($params[$index]);
            }
            else
            {
                $replacement = is_numeric($replacement) ? $replacement : '"' . $replacement . '"';
            }

            $match_index   = $match[1];
            $prefix        = '';
            $suffix        = '';

            // Sub String Logic
            if ($match_index !== 0)
            {
                $prefix = substr($string, 0, $match_index);
            }
            $suffix = substr($string, $match_index + 1, strlen($string));
            unset($match_index);

            $string = $prefix . $replacement . $suffix;

            //-- Adjust the next matches' indices to the string just added.
            for($next_index = $index + 1; $next_index < $total_matched; $next_index++)
            {
                $length = strlen($replacement) - 1;
                if($length < 0)
                {
                    $length = 1;
                }
                $matches[$next_index][1] += strlen($replacement) - 1;
            }

            unset($match);
        }
        return $string;
    }
    
    /**
     * Returns number of rows found ignoring LIMIT if SQL_CALC_FOUND_ROWS was used.
     * <code>
     *      SELECT SQL_CALC_FOUND_ROWS * FROM.......
     * </code>
     * @return int
     */
    public function found_rows()
    {
        return $this->fetch_singlet('SELECT FOUND_ROWS()');
    }
    
    //returns an array of all the ids in $rows assuming the id field is the first field
    public function get_all_ids($rows){
        $ids = array();
        
        $id_key = key((array)$rows[0]);
        
        foreach($rows as $row){
            $ids[] = (int)$row[$id_key];
        }
        
        return $ids;
    }
    
    public function get_fields($table){
        $explain = $this->fetch_all('EXPLAIN '.$this->escape_column($table));
        
        $fields = array();
        foreach($explain as $field){
            $fields []= $field['Field'];
        }
        
        return $fields;
    }
    
    //$columns = describe('table1', 'table2', 'table3');
    //$columns = describe(array('table1', 'table2', 'table3'));
    public function describe($table_name){
        if(is_array($table_name)){
            $table_names = $table_name;
        }else{
            $table_names = func_get_args();
        }
        
        $columns = array();
        
        foreach($table_names as $table_name){
            $columns = array_merge($columns, $this->fetch_all('describe ' . $this->escape_column($table_name)));
        }
        
        return $columns;
    }
    
    /*
        Returns the $wheres and $params variables created by using $params (usually is $_REQUEST) (For listing pages)
        index.php?field_one=hi&join_field_two_id=4&is_active=1   becomes:
            $wheres = array('field_one like %?%', 'field_two = ?', 'is_active = ?')
            $params = array('hi', '4', '1')
        
        'join' in the field name, or field of type tinyint(1) results in an equal sign.
        otherwise, LIKE is used.
        SECURITY RISK: A user can put any valid field name into uri query, and it will work
    */
    public function build_wheres_params($table_names, $values){
        $table_names = (array)$table_names;
        
        $wheres = array();
        $params = array();
        
        $fields = $this->describe($table_names);
        
        foreach($fields as $field){
            $field_name = $field['Field'];
            $field_value = $values[$field_name];
            if(isset($field_value) && $field_value != ''){
                if(stristr($field_name, 'join') || $field['Type'] == 'tinyint(1)'){
                    $wheres[] = $field_name.' = ?';
                    $params[] = $field_value;
                }else{
                    $wheres[] = $field_name.' LIKE ?';
                    $params[] = '%'.$field_value.'%';
                }
            }
        }
        
        return array(
            $wheres,
            $params
        );
    }
    
    //params are default order
	public function orderby($order = null, $by = 'ASC'){
	    if(!isset($_REQUEST['order'])){
            $_REQUEST['order'] = $order;
            $_REQUEST['by'] = $by;
        }
        
        $by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
        $orderby = ' ORDER BY ' . $this->escape_column($_REQUEST['order']) . ' ' . $by . ' ';
        
        return $orderby;
    }
    
    public function get_enum_values($table, $column){
        $column_type = $this->fetch_singlet('
            SELECT COLUMN_TYPE
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_SCHEMA = ?
            AND TABLE_NAME = ?
            AND COLUMN_NAME = ?'
            , array(DB_NAME, $table, $column));
        
        $column_type = substr($column_type, 6, -2);
        return explode("','", $column_type);
    }
}

function SQL($value) 
{
	return new SQL($value);
}

class SQL 
{
	public $value;
	public function __construct($sql) 
	{
		$this->value = $sql;
	}
}
?>
