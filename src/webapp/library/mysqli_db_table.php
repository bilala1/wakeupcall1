<?php
#REVISION: 3
#REVISION: 2
class mysqli_db_table
{
    private $_options = array(
        'type' => NULL,
        'collate' => NULL,
        'charset' => NULL
    );
    private $_columns = array();
    private $_table = '';
    private $_selects = array();
    private $_wheres = array();
    private $_primary;
    public $version = '1.20beta1';
    public $last_id;
    
    function __construct($table)
    {
        $this->set_table($table);
        $db = mysqli_db::init();
        $db->query("EXPLAIN " . $table);
        foreach($db->fetch_all() as $field) 
        {
            $type = explode(' ', $field['Type']);
            $type = array_shift($type);
            $type = explode('(', $type);
            $type = array_shift($type);
            $this->setColumn(array('name'=>$field['Field'],'type'=>$type));
            if($field['Key'] == 'PRI') 
            {
                $this->_primary = $field['Field'];
            }
        }
    }
    
    public function insert($data=null) 
    {
        if($data !== null) 
        {
            $this->set($data);
        }
        $columns = array();
        $values = array();
        $params = array();
        foreach($this->_columns as $column) {
            if ($column->modified) {
                $columns[] = '`' . $column->name . '`';
                $values[] = '?';
                $params[] = $column->value;
            } else if ($column->unfiltered) {
                $columns[] = '`' . $column->name . '`';
                $values[] = $column->unfiltered;
            }
        }
        $stmt = 'INSERT INTO '.$this->_table.' ('.implode(', ', $columns).') VALUES('.implode(', ', $values).')';
        
        $this->decache();
		
        $db = mysqli_db::init();
        $db->query($stmt, $params);
        $this->last_id = $db->last_id();
		return $this;
    }

    public function update($data=null, $id=null) 
    {
        if($data !== null) {
            $this->set($data);
        }
        $values = array();
        $params = array();
        
        //-- Find chnaged variables
        foreach($this->_columns as $column) {
            if ($column->modified) {
                $values[] = '`' . $column->name . '` = ?';
                $params[] = $column->value;
            } else if ($column->unfiltered) {
                $columns[] = '`' . $column->name . '` = ' . $column->unfiltered;
            }
        }
        
        if($id !== null) {
            $this->where($this->_primary . ' = ' . $this->filter($id));
        }
        else
        {
            return false;       //so it doesn't set each record in the table
        }
        
        //-- Setup Where
        $where = (!empty($this->_wheres)) ? ' WHERE ' . implode(' AND ', $this->_wheres) : '';
        $stmt = 'UPDATE ' . $this->_table . ' SET ' . implode(', ', $values) . $where;
        
        $this->decache();
        
        $db = mysqli_db::init();
        $db->query($stmt, $params);
		return this;
    }
    
    public function delete($id = null) 
    {
        $params = array();
        if($id === null) {
            //-- Setup Where
            $where = (!empty($this->_wheres)) ? ' WHERE ' . implode(' AND ', $this->_wheres) : '';
        } else {
            //-- TODO: find out primary key
            $where = ' WHERE `' . $this->_primary . '` = "' . $this->filter($id) . '"';
        }
        $stmt = 'DELETE FROM ' . $this->_table . $where;
        
        $this->decache();
        
        $db = mysqli_db::init();
        $db->query($stmt, $params);
		return this;
    }
	
    public function get($id = null) 
    {
        $db = mysqli_db::init();
        return $db->fetch_one('SELECT * FROM ' . $this->_table . ' WHERE `' . $this->_primary . '` = ?', array($id));
    }
    
    public function where($where) 
    {
        $this->_wheres[] = $where;
        return $this;
    }
	
    public function select($select) 
    {
        $this->_selects[] = $select;
        return $this;
    }
    
    public function fetch() {
        //----------------------------------------------------------------------
        //-- Init variables
        //----------------------------------------------------------------------
        $columns = array();
        $params = array();
        //----------------------------------------------------------------------
        //-- Set filtered columns
        //----------------------------------------------------------------------
        foreach($this->_columns as $column) {
            if ($column->modified) {
                $columns[] = '`'.$column->name.'` = ?';
                $params[] = $column->value;
            }
        }
        //----------------------------------------------------------------------
        //-- Setup statement
        //----------------------------------------------------------------------
        $where = (!empty($columns)) ? ' WHERE ' . implode(' AND ', $columns) : '';
        $select = (!empty($this->_selects)) ? implode(', ', $this->_selects) : '*';
        $where = (!empty($this->_wheres)) ? ' WHERE ' . implode(' AND ', $this->_wheres) : $where;
	    $params = (!empty($this->_wheres)) ? null : $params;
		
        $stmt = 'SELECT ' . $select . ' FROM ' . $this->_table . ' ' . $where;
        return array($stmt, $params);
    }
    
    public function fetch_singlet($query=null, $params=null) 
    {
    	if($stmt === null and $params === null) 
    	{
    		list($stmt, $params) = $this->fetch();
    	} 
        $db = mysqli_db::init();
        $db->query($stmt, $params);
        $this->decache();
        return $db->fetch_singlet();
    }
    
    public function fetch_singlets($query=null, $params=null) 
    {
    	if($query === null and $params === null) 
    	{
    		list($query, $params) = $this->fetch();
    	} 
        $rows = $this->fetch_all($query, $params);
		$variable_name = 'temp_' . uniqid();
        global $$variable_name;
        $$variable_name = array();
        array_walk_recursive($rows, 
            create_function(
                '$item, $key', 
                'global $' . $variable_name . ';
                    $' . $variable_name . ' []= $item;
                '
            )
        );
        $this->decache();
		return $$variable_name;    	
    }
    
    public function fetch_one($query=null, $params=null) 
    {
    	if($stmt === null and $params === null) 
    	{
    		list($stmt, $params) = $this->fetch();
    	}
        $db = mysqli_db::init();
        $db->query($stmt, $params);
        $this->decache();
        return $db->fetch_one();
    }
    
    public function fetch_all($stmt=null, $params=null, $cached = false) 
    {
    	if($stmt === null and $params === null) 
    	{
    		list($stmt, $params) = $this->fetch();
    	} 
        $db = mysqli_db::init();
        $db->query($stmt, $params);
        $this->decache();
        return $db->fetch_all(null, null, $cached);
    }
    
    protected function decache() {
        $this->_wheres = array();
        $this->_selects = array();
    }
    
    public function count() {
        $db = mysqli_db::init();
        return $db->count();
    }
    
    public function last_id() 
    {
        $db = mysqli_db::init();
        return $db->last_id();
    }
    
    public function debug() 
    {
        $db = mysqli_db::init();
        return $db->debug();
    }

    public function error() 
    {
        $db = mysqli_db::init();
        return $db->error();
    }  

    public function setColumn(array $column) 
    {
        $this->_columns[$column['name']] = new Column($column);
    }
    public function set_column(array $column) 
    {
        $this->setColumn($column);
    }
    
    public function getColumn($column_key) 
    {
        return $this->_columns[$column_key];
    }
    public function get_column($column_key) 
    {
        $this->getColumn($column);
    }

    public function filter($args) 
    {
        $db = mysqli_db::init();
        return $db->filter($args);
    }

    public function set($var, $val=null) 
    {
    	if(is_array($var)) {
            $keys = array_keys($this->_columns);
            foreach($var as $key=>$value) {
                if (in_array($key, $keys)) {
                    $this->__set($key, $value);
                }
            }
        } else {
        	$this->__set($var, $val);
        }
        return $this;
    }
	
    public function raw($var, $val) 
    {
        $this->_columns[$var]->unfiltered = $val;
        return $this;
    }
    
    public function set_table($table) 
    {
        $this->_table = $table;
    }
    
    public function get_table() 
    {
        return $this->_table;
    }
    
    public function __get($var) 
    {
        $column = $this->_columns[$var];
        return $column->value;
    }
    
    public function __set($var, $val) 
    {
        switch ($this->_columns[$var]->type) 
        {
            case 'int':
            case 'tinyint':
                $val = intval($val);
                break;
            case 'double':
            case 'float':
                $val = floatval($val);
                break;
            case 'varchar':
            case 'text':
            case 'datetime':
                break;
        }
        $this->_columns[$var]->value = $val;
        $this->_columns[$var]->modified = true;
    }
}

function mysqli_db_table($table) {
    return new mtsqli_db_table($able);
}

class Column
{
    public $name;
    public $value;
    public $unfiltered;
    public $default;
    public $modified = false;
    public $alias;
    public $type;
    public $length;
    public $key;
    public $validators;
    public $filters;
    
    function __construct(array $column)
    {
        foreach($column as $key => $val) 
        {
            $this->$key = $val;
        }
    }
}
?>