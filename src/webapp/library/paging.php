<?php
#REVISION: 3

final class paging {
    var $firstlast, $show_left, $show_right, $total, $perpage, $page, $uri, $params;
    var $multiple = false;     //true if more than one page
    var $page_param;           //the uri page param (usually 'page'). Set when doing multiple paging on one page.

    function __construct($perpage = 10, $page = null, $uri = null, $page_param = 'page'){
        $this->firstlast  = false;
        $this->show_left  = 3;
        $this->show_right = 3;
        $this->perpage = $perpage;
        $this->page    = $page ? $page : ($_GET['p'] ? $_GET['p'] : 1);
        $this->uri     = ($uri) ? $uri : $_SERVER['REQUEST_URI'];
        $this->params  = array();
        $this->page_param = $page_param;

        if(strstr($this->uri, '?')){
            list($uri, $addr_query) = explode('?', $this->uri);
            parse_str($addr_query, $this->params);

            $this->uri = $uri;
        }

        $this->uri = str_ireplace(BASEURL, '', $this->uri);
    }

    function set_total($total){
        $this->total = $total;
        $this->multiple = ($total > $this->perpage);

        return $this;
    }

    function get_total(){
        return $this->total;
    }

    function set_firstlast($firstlast){
        $this->firstlast = (bool) $firstlast;

        return $this;
    }

    function set_leftright($left, $right){
        $this->show_left  = $left;
        $this->show_right = $right;

        $this->show_displayed = 0;

        return $this;
    }

    function set_displayed($displayed){
        $this->show_left  = 0;
        $this->show_right = 0;

        $this->show_displayed = $displayed;

        return $this;
    }

    function get_limit(){
        $string = 'LIMIT '.($this->perpage * ($this->page - 1)).', '. $this->perpage;

        return $string;
    }

    var $pages;

    function get_html($attrs = array()){
        $container = html::element('div');
		$container->set('class', 'paging');

		if (!empty($attrs)) {
			foreach ($attrs as $attr => $val) {
				$container->set($attr, $val);
			}
		}

        $pages = ceil($this->total / $this->perpage);
        $this->pages = $pages;

        if($this->pages <= 1){
            return '';
        }

        if($this->firstlast){
            $this->params[$this->page_param] = 1;

            if($this->page > 1){
                $first = html::element('a');
                $first
                    ->set('class', 'pagenumber pagefirst')
                    ->set('href', $this->uri . '?' . http_build_query($this->params))
                    ->set('text', 'First');
            }
            else {
                $first = html::element('span');
                $first
                    ->set('class', 'pagenumber pagefirst')
                    ->set('text', 'First');
            }

            $first->inject($container);
        }

        if($this->page > 1){
            $this->params[$this->page_param] = $this->page - 1;

            $prev = html::element('a');
            $prev
                ->set('class', 'pagenumber pageprev')
                ->set('href', $this->uri . '?' . http_build_query($this->params))
                ->set('text', 'Previous');
            $prev->inject($container);
        }
        else {
            /*$prev = html::element('span');
            $prev
                ->set('class', 'pagenumber pageprev')
                ->set('text', 'Previous');
            $prev->inject($container);*/
        }

        // Loop through the pages...
        if($this->show_displayed){
            $half_left = $half_right = floor($this->show_displayed / 2);
            if($half_left % 2 !== 0){
                $half_right = $half_left - 1;
            }

            $pad_left   = ($this->page - ($half_left + 1) < 0) ? 1 : $this->page - $half_left;
            $pages_left = $this->page - $pad_left;
            if($pages_left < $half_left){
                $pad_right = $half_right + ($half_left - $pages_left) + $this->page;
            }
            else {
                $pad_right = ($this->page + $half_right < $pages) ? $this->page + $half_right : $pages;
            }

            if($pad_right - $this->page < $pad_left){
                $pad_left = $pad_left - ($half_left - ($pad_right - $this->page)) - 1;
            }

            if($pad_right > $pages){
                $pad_right = $pages;
            }

            if($pad_left < 0){
                $pad_left = $pad_left - $half_left;
            }
        }
        else {
            $pad_left  = ($this->page - $this->show_left > 0) ? ($this->page - $this->show_left) : 1;
            $pad_right = ($this->page + $this->show_right <= $pages) ? ($this->page + $this->show_right) : $pages;
        }

        for($i = $pad_left; $i <= $pad_right; $i++){
            if($i != $this->page){
                $this->params[$this->page_param] = $i;
                $link = html::element('a');
                $link
                    ->set('class', 'pagenumber')
                    ->set('href', $this->uri . '?' . http_build_query($this->params))
                    ->set('text', $i);
            }
            else {
                $link = html::element('span');
                $link
                    ->set('class', 'pagenumberhighlight')
                    ->set('text', $i);
            }

            $link->inject($container);
        }

        if($this->page < $pages){
            $this->params[$this->page_param] = $this->page + 1;

            $next = html::element('a');
            $next
                ->set('class', 'pagenumber pagenext')
                ->set('href', $this->uri . '?' . http_build_query($this->params))
                ->set('text', 'Next');
            $next->inject($container);
        }
        else {
            /*$next = html::element('span');
            $next
                ->set('class', 'pagenumber pagenext')
                ->set('text', 'Next');
            $next->inject($container);*/
        }

        if($this->firstlast){
            $this->params[$this->page_param] = $pages;

            if($this->page < $pages){
                $last = html::element('a');
                $last
                    ->set('href', $this->uri . '?' . http_build_query($this->params))
                    ->set('text', 'Last');
            }
            else {
                $last = html::element('span');
                $last->set('text', 'Last');
            }

            $last->inject($container);
        }

        return $container->to_html();
    }

    public function __toString(){
        return $this->get_html();
    }
}

?>
