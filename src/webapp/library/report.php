<?php
#REVISION: 1

/*
$report = new report('stuff.xls', 'xls');

$report
    ->add_field('Stuff', 'column1')
    ->add_field('More Stuff', 'adsf');
    
$data[] = array('column1' => 'asdfadfadfasfd', 'adsf' => 'hablah');
$data[] = array('column1' => 'asdfadfadfasfd2');
$data[] = array('column1' => 'asdfadfadfasfd3');
$data[] = array('column1' => 'asdfadfadfasfd4');

$report->add_data($data);
$report->build()->export();
*/


final class report {
    var $fields = array();
    var $data = array();
    
    var $report_file, $type, $report_content;
    
    function __construct($report_file, $type = 'xls'){
        $this->report_file = $report_file;
        $this->type = $type;
        
        return $this;   
    }
    
    function add_field($field_name, $field_key, $separator = ' '){
        $this->fields[] = array($field_name, $field_key, $separator);
        
        return $this;
    }
    
    function add_data($data){
        $this->data = $data;
        
        return $this;
    }
    
    var $table, $colHeader;
    
    function build(){
        switch($this->type){
            default:
            case "xls":
            
            $dom = new simple_dom();
            
            $this->table = $dom->element('table');
            $tbody = $dom->element('tbody');
            
            // Create the column headers...
            $this->colHeader = $dom->element('tr');
            foreach($this->fields as $field){
                $td = $dom->element('td')->set('text', $field[0])->inject($this->colHeader);
            }
            $this->colHeader->inject($tbody);
            $tbody->inject($this->table);
            
            // Now we need create the data into the table...
            foreach($this->data as $data){
                $row = $dom->element('tr');
                foreach($this->fields as $field){
                    if(is_array($field[1])){
                        $text = array();
                        foreach($field[1] as $field_key){
                            $text[] = $data[$field_key];
                        }
                        
                        $text = implode($field[2], $text);
                    }
                    else {
                        $text = $data[$field[1]];
                    }
                    
                    $td = $dom->element('td')->set('text', $text)->inject($row);
                }
                $row->inject($tbody);
            }
            
            $this->report_content = $this->table;

            break;
            
            case "csv":
            $rows   = array();
            $fields = array();
            
            // Setup the first row manually...
            foreach($this->fields as $field){
                $fields[] = '"' . $field[0] . '"';
            }
            $rows[] = implode(',', $fields);
            unset($fields);
            
            foreach($this->data as $data){
                foreach($this->fields as $field){
                    if(is_array($field[1])){
                        $text = array();
                        foreach($field[1] as $field_key){
                            $text[] = $data[$field_key];
                        }
                        
                        $text = implode($field[2], $text);
                    }
                    else {
                        $text = $data[$field[1]];
                    }
                    $fields[] = '"' . $text . '"';
                }
                $rows[] = implode(',', $fields);
                unset($fields);
            }
            
            $this->report_content = implode("\r\n", $rows);
            unset($rows);
            
            break;
        }
        
        return $this;
    }
    
    function export(){
        switch($this->type){
            default:
            case "xls":
            
            header('Content-type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="'.$this->report_file.'"');
            
            print $this->report_content;
            
            break;
        }
    }
    
    function __toString(){   
        return $this->report_content;
    }
}

?>
