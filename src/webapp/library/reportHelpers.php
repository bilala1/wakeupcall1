<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 12/24/14
 * Time: 8:36 AM
 */

final class reportHelpers {
    // creates a dropdown list of all locations for a corp account
    final public static function locations()
    {
        return html::select_from_query(licensed_locations::get_licensed_locations_for_member(ActiveMemberInfo::GetMemberId()), 'licensed_locations_id', $_REQUEST, 'licensed_locations_name', '-all-');
    }

    final public static function fromDate($startTime, $name = "report_start_time") {
        return self::dateInput($startTime,$name);
    }

    final public static function toDate($endTime, $name = "report_end_time") {
        return self::dateInput($endTime,$name);
    }

    final public static function dateInput($initialTime, $name)
    {
        return sprintf('<input style="width:100px" type="text" id="%s" name="%s" value="%s" /> ',
            $name,
            $name,
            date('n/j/Y', ( $initialTime ? strtotime($initialTime) : strtotime(date('n/j/Y', time()) . ' -1 year'))));
    }

    final public static function claimTypes() {
        // the keys for this array must match the values
        // in claims.php $claim_types. Yeah, this really sucks
        // but only when we replace claim type with a code, this will
        // have to be it.
        $claimTypes = array(
            "" => "-all-",
            "workers" => "Worker's Comp Claims",
            "general" => "General Liability/Guest Property",
            "auto" => "Auto Claims",
            "property" => "Property Claims",
            "labor" => "Labor Claims",
            "other" => "Other Claims"
        );
        if(!UserPermissions::UserCanAccessScreen('workers_comp')){
            unset($claimTypes['workers']);
        }
        if(!UserPermissions::UserCanAccessScreen('labor_claims')){
            unset($claimTypes['labor']);
        }
        
        return html::select($claimTypes, 'claim_type');

    }

    final public static function includeHidden($checked)
    {
        $attributes = Array("type" => "checkbox");
        if ($checked) {
            $attributes["checked"] = "";
        }
        return html::input($checked,"includeHidden",$attributes);
    }
    final public static function status($join_claims_status_codes_id)
    {
        $claims_status_values = array(
        '' => '-all-',
        claims::$SUBMIT_OPEN => 'Submitted - Open',
        claims::$SUBMIT_CLOSE => 'Submitted - Closed',
        claims::$RECORD_ONLY => 'Record Only - Not submitted',
        claims::$FIRST_AID =>'First Aid Only - Not submitted');
        return html::select($claims_status_values, 'join_claims_status_codes_id',$join_claims_status_codes_id);
       
    }

    final public static function getTitle($title, $startTime, $endTime, $locationId, $showHidden) {
        $title = $title." : ".$startTime." to ".$endTime;
        if ( !empty($locationId) || $showHidden) {
            $title = $title."\n";
            if (!empty($locationId)) {
                $location = licensed_locations::get_by_id($locationId);
                if (isset($location['licensed_locations_name'])) {
                    $title = $title . " for location " . $location['licensed_locations_name'];
                }
            }
            if ($showHidden) {
                $title = $title." showing hidden claims ";
            }
        }
        return $title;
    }
}
?>