<?php
#REVISION: 1
/**
 * @package simple_dom_node 
 */
class simple_dom_node {
    function __construct(&$owner, &$node) 
    {
        /*if($this->node instanceof DOMText) {
            throw new Exception('Element you want create is not a DomNode its a DOMText');
        } else {*/
            $this->owner = $owner;
            $this->node = $node;
            $this->mooid = uniqid();
            $this->set_property('mooid', $this->mooid);
        /*}*/
    }

    public function toCssSelector($value) 
    {
        if(strstr($value, '/')) {
            return $value;
        } else {
            $value = preg_replace('/(\.+)/i', '[@${1}]', $value);
        }
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return array
     * @see function get_element_by_attribute
     */
    private function get_elements_by_attribute($attribute, $value) 
    {
        return $this->get_elements('//*[@' . $attribute . '="' . $value . '"]');
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return simple_dom_node 
     * @see function get_elements_by_attribute
     */
    private function get_element_by_attribute($attribute, $value) 
    {
        $elements = $this->get_elements_by_attribute($attribute, $value);
        if(!empty($elements)) {
            return $elements[0];
        } else {
            return false;
        }
    }

    /**
     * @param string $id
     * @return simple_dom_node 
     */
    public function get_element_by_id($id) 
    {
        return $this->get_element_by_attribute('id', $id);
    }


    /**
     * @param string $attribute
     * @return simple_dom_node 
     */
    public function erase($attribute) 
    {
        $this->node->removeAttribute($attribute);
        return $this;
    }

    /**
     * get_elements
     *
     * Using XPath:
     * <code>
     * <?php
#REVISION: 1
     * // Get all <div> tags in the body
     * $elements = $document->body->get_elements('//div');
     * ?>
     * </code>
     * Using Css Selectors:
     * <code>
     * <?php
#REVISION: 1
     * // Get all <div> tags in the body
     * $elements = $document->body->get_elements('div');
     * ?>
     * </code>
     * @param string $selector The XPath Query
     * @return array
     */
    public function get_elements($selector) 
    {
        $query = '';
        $query .= '//' . $this->node->tagName . '[@mooid="' . $this->mooid . '"]';
        $query .= $selector;
        $xpath = new DOMXPath($this->owner->html);
        $rows = $xpath->query($query);
        $nodes = array();
        for($i = 0; $i < $rows->length; $i++) {
            $row = $rows->item($i);
            $nodes []= new simple_dom_node($this->owner, $row);
        }
        return $nodes;
    }

    /**
     * @param string $selector
     * @return simple_dom_node 
     */
    public function get_element($selector) 
    {
        $elements = $this->get_elements($selector);
        $element = $elements[0];
        if(!$element) {
            $htmls = $this->owner->html->getElementsByTagName('html');
            $node = new simple_dom_node($this->owner, $htmls->item(0));
            $elements = $node->get_elements($q);
            $element = $elements[0];
        }
        if(!$element) {
            throw new Exception('Element you want is not found');
        }
        return $element;
    }

    public function set($name, $value) 
    {
        if($name == 'text') 
        {
            $newtext = new DOMText($value);
            $childs = $this->node->childNodes;
            if($childs) {
                $nth = 0;
                $text = $childs->item($nth);
                while(!$childs->item($nth) instanceof DOMText) {
                    $nth++;
                    $text = $childs->item($nth);
                    if($text === null) {
                        $text = $childs->item($nth - 1);
                        break;
                    }
                }

                if($text === null) {
                    $this->node->appendChild($newtext);
                } else {
                    $this->node->replaceChild($newtext, $text);
                }
            } else {
                $this->node->appendChild($newtext);
            }
        } 
        else if($name == 'html') 
        {
            $this->clear();
            if(is_string($value) and !strstr($value, '<') and !strstr($value, '>'))
            {
                return $this->set('text', $value);
            } 
            else 
            {
                $node = $this->owner->eval_html($value);
                if(is_array($node))
                {
                    $nodes = $node;
                    foreach($nodes as $node) 
                    {
                        $node->inject($this);
                    }
                } 
                else 
                {
                    $node->inject($this);
                }
            }
        } 
        else 
        {
            $this->set_property($name, $value);
        }
        return $this;
    }

    public function get($name) 
    {
        if($name == 'html') {
            $html = '';
            if($this->node->childNodes) {
                foreach($this->node->childNodes as $node) {
                    $html .= $node->ownerDocument->saveXML($node);
                }
            } else {
            }
            simple_dom::fix_utf($html);
            return $html;
        } else if($name == 'text') {
            return $this->node->textContent;
        } else if($name == 'tag') {
            return $this->node->tagName;
        } else {
            return $this->get_property($name);
        }
    }

    public function get_parent() 
    {
        if($this->node->parentNode) {
            $parent = new simple_dom_node($this->owner, $this->node->parentNode);
            $this->parent = $parent;
            return $parent;
        }
        return false;

    }

    public function copy($contents = true, $keepid = false) 
    {
        $dom = new Simple_Dom(trim($this->to_html()));
        $parnode = $dom->html->getElementsByTagName('body')->item(0);
        $chlnode = $parnode->childNodes->item(0);
        $i= 0;
        while($chlnode instanceof DOMText) {
            $i++;
            $chlnode = $parnode->childNodes->item($i);
        }
        $domnode = new simple_dom_node($dom, $chlnode);
        if(!$keepid) {
            $domnode->set('id', '');
        }
        if(!$contents) {
            $domnode->clear();
        }
        $domnode->mooid = uniqid();
        $domnode->set_property('mooid', $domnode->mooid);
        return new simple_dom_node($domnode->owner, $domnode->node);

    }

    public function set_property($prop, $value) 
    {
        if(!$this->node) {
            throw new Exception('Element you want is not found');
        }
        if($this->node instanceof DOMText or $this->node instanceof DOMComment) {
            return $this;
            //throw new Exception('Element you want not a DomNode its a DOMText');
        }
        $this->node->setAttribute($prop, $value);
        return $this;
    }

    public function get_property($prop, $ns = null) 
    {
        if($ns !== null)
        {
            return $this->node->getAttributeNS($ns, $prop);
        }
        else
        {
            return $this->node->getAttribute($prop);
        }
    }

    public function get_document() 
    {
        return $this->node->ownerDocument;
    }

    public function to_html() 
    {
        $html = $this->get_document()->saveXML($this->node);
        simple_dom::fix_utf($html);
        /* 
         * Remove moo ids
         */
        //$html = preg_replace('/<(.*)? ?mooid=["\'].+?["\']\s?(.*?)>/i', "<$1$2>", $html);
        $html = preg_replace('/ mooid=[\'"](.*?)[\'"]/', '', $html);
        return $html;
    }

    public function inject($node, $location = 'bottom') 
    {
        if($this->node->ownerDocument !== $node->node->ownerDocument) {
            if($node->node === null) {
                throw new Exception('Element you want to inject into is null');
            };
            $this->node = $node->node->ownerDocument->importNode($this->node, true);
        }
        $this->parent = $node;
        if($location == 'bottom') {
            $this->node = $node->node->appendChild($this->node);
        } else if($location == 'top') {
            $first = $node->node->firstChild;
            if($first) {
                $this->node = $node->node->insertBefore($this->node, $first);
            } else {
                $this->node = $this->inject($this->node, 'bottom');
            }
        } else if($location == 'after') {
            $sibling = $node->node->nextSibling;
            if($sibling) {
                $this->node = $node->node->parentNode->insertBefore($this->node, $sibling);
            } else {
                $this->node = $this->inject($node->get_parent());
            }
        }
        return $this;
    }

    /**
     * @param string $selector
     * @return simple_dom_node 
     */
    public function get_first($selector='') 
    {
        $first = $this->node->childNodes->item(0);
        if($first) {
            return new simple_dom_node($this->owner, $first);
        } else {
            return false;
        }
    }

    /**
     * @param string $selector
     * @return simple_dom_node 
     */
    public function get_last() 
    {
        $last = $this->node->childNodes->item($this->node->childNodes->length - 1);
        if($last) {
            return new simple_dom_node($this->owner, $last);
        } else {
            return false;
        }
    }

    /**
     * @param string $selector
     * @return simple_dom_node 
     */
    public function get_next() 
    {
        $next = $this->node->nextSibling;
        if($next) {
            return new simple_dom_node($this->owner, $next);
        } else {
            return false;
        }
    }

    /**
     * @param string $selector
     * @return simple_dom_node 
     */
    public function get_previous() 
    {
        $prev = $this->node->previousSibling;
        if($prev) {
            return new simple_dom_node($this->owner, $prev);
        } else {
            return false;
        }
    }

    public function dispose() 
    {
        return $this->node->parentNode->removeChild($this->node);
    }

    public function clear() 
    {
        if($this->node->hasChildNodes()) {
            while($this->node->childNodes->length) {
                $this->node->removeChild($this->node->firstChild);
            }
        }
        return $this;
    }
    
    public function __toString()
    {
        return $this->to_html();
    }
}
?>
