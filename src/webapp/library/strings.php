<?php
#REVISION: 7
#REVISION: 6
#REVISION: 5
#REVISION: 4

final class strings {

	function br2nl($string){
		return preg_replace('#<br />#i', "\n", $string);
	}

    // Takes a string and converts it to an integer phone number, if not, return false.
    final public static function phone_int($number){
        $number = strtoupper($number);
        $number = str_replace('.', '', $number);
        $number = str_replace('-', '', $number);
        $number = str_replace('/', '', $number);
        $number = str_replace(' ', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);

        // If they put in a string for numbers (ABC = 2, etc...) replace it.
        $search = array(
                    "A,B,C" => 2,
                    "D,E,F" => 3,
                    "G,H,I" => 4,
                    "J,K,L" => 5,
                    "M,N,O" => 6,
                    "P,Q,R,S" => 7,
                    "T,U,V" => 8,
                    "W,X,Y,Z" => 9,
                    );

        foreach($search as $alpha => $num){
            $letters = explode(',', $alpha);

            foreach($letters as $letter){
                $number = str_replace($letter, $num, $number);
            }
        }

        if(is_numeric($number) && strlen($number) == 10){
            return $number;
        }
        else {
            return false;
        }
    }

    // Formats 10 digit integers into a phone number string.
    final public static function phone_str($number){
        $ac = substr($number, 0, 3);
        $digit[3] = substr($number, 3, 3);
        $digit[4] = substr($number, 6, 4);

        $number = '('.$ac.') '.$digit[3].'-'.$digit[4];

        return $number;
    }

    // Check to see if an email is properly formatted...
    final public static function is_email($email){
        $email = strtolower($email);

        if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$/", $email, $matches)){
            return false;
        }
        else {
            return true;
        }
    }

    // Check to see if an URL is properly formatted...
    final public static function is_url($url){
        if(!eregi("^(https?|ftp)\:\/\/([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*(\:[0-9]{2,5})?(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$", $url)){
            return false;
        }
        else {
            return true;
        }
    }

    // Format credit card (accepts all credit cards and will only display the last division on the credit card
    /* ***************************************************************************
        American Express	3400 0000 0000 009
        Carte Blanche	    3000 0000 0000 04 (only will display 04)
        Discover	        6011 0000 0000 0004
        Diners Club	        3852 0000 0232 37 (only will display 37)
        enRoute	            2014 0000 0000 009
        JCB	                2131 0000 0000 0008
        MasterCard	        5500 0000 0000 0004
        Solo	            6334 0000 0000 0004
        Switch	            4903 0100 0000 0009
        Visa	            4111 1111 1111 1111
        Laser	            6304 1000 0000 0008
    ***************************************************************************/
    final public static function format_cc($cc){
        $cclen = strlen($cc);

        $div = array();
        $reali = 0;
        for($i=0;$i<ceil($cclen /4);$i++){
        	$div[] = substr($cc, $reali, 4);
        	$reali = $reali + 4;
    	}
    	$lastdiv = array_pop($div);

    	foreach($div as $thisdiv){
    		$newdivs []= str_repeat('*', strlen($thisdiv));
		}
		$newdivs[] = $lastdiv;
        // $cc = preg_replace('/([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{1,4})/', '****-****-****-\\4', $cc);

        return implode('-', $newdivs);
    }

	final public static function merge($token, $params)
	{
	    $params = func_get_args();
	    $token = array_shift($params);

	    foreach($params as $key => $param)
	    {
	        if(empty($param) && $param !== '0' && $param !== 0)
	        {
	            unset($params[$key]);
	        }
	    }

	    return implode($token, $params);
	}

    final public static function CreateInClauseForIds($ids, $object_type)
    {
        return $ids ? ("(" . $object_type . "_id in (" . implode(', ', $ids) . "))") : "FALSE";
    }

	final public static function where($where_ary){
		$where_ary = (!is_array($where_ary)) ? array() : $where_ary;

		$where = (!empty($where_ary)) ? ' WHERE ' . implode(' AND ', $where_ary) : '';

		return $where;
	}

    final public static function having($having_ary){
		$having_ary = (!is_array($having_ary)) ? array() : $having_ary;

		$having = (!empty($having_ary)) ? ' HAVING ' . implode(' AND ', $having_ary) : '';

		return $having;
	}
	final public static function join($join_ary, $type = ''){
		$typejoin = ($type) ? $type . ' JOIN' : '';

		$join = (!empty($join_ary)) ? $typejoin . ' ' . implode(' '.$typejoin.' ', $join_ary) : '';

		return $join;
	}

    //$table = one table or array of many
	final public static function orderby($default, $table = null, $otherfields = array()){
        if(null === $table){        //old way (backwards compatibility)
            $by = ($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
            $orderby = $default . ' ' . $by;

            return $orderby;
        }
            if($_REQUEST['by']){
                $by = $_REQUEST['by'];//($_REQUEST['by'] == 'ASC') ? 'ASC' : 'DESC';
            }
            elseif($_SESSION['sort_' . $table . '_by']){
               $by =  $_SESSION['sort_' . $table . '_by'] ;
            }else{
                $by = 'ASC';
            }
            $_SESSION['sort_' . $table . '_by'] = $by;
            
            
            if($_REQUEST['order']){
	    $order = $_REQUEST['order'];// ? $_REQUEST['order'] : $default;
            
            }
            elseif($_SESSION['sort_' . $table . '_on']){
                $order = $_SESSION['sort_' . $table . '_on'];
            }else{
                $order =  $default;
            }
            $_SESSION['sort_' . $table . '_on'] = $order;

	    $db    = mysqli_db::init();

	    $tables = (array) $table;
	    $fields = array();
	    foreach($tables as $table){
	        $fields = array_merge($fields, $db->get_fields($table));
	    }
	    $fields = array_merge($fields, $otherfields);

        $order  = ($order) ? $order : $default;
        $order  = (in_array($order, $fields)) ? $order : $default;

        $orderby = ' ORDER BY ' . $order . ' ' . $by . ' ';

        $_REQUEST['order'] = $order;

        return $orderby;
    }

	final public static function putcsv($input, $delimiter = ',', $enclosure = '"') {
          $fp = fopen('php://temp', 'r+');
          fputcsv($fp, $input, $delimiter, $enclosure);
          rewind($fp);
          $data = fread($fp, 1048576);
          fclose($fp);
          return rtrim( $data, "\n" );
    }

	final public static function sort($field, $str, $url = '', $params = null, $hash = null, $default_sort = 'ASC') {
	    $order = $_REQUEST['order'] ? $_REQUEST['order'] :
                    ($_GET['order'] ? $_GET['order'] :
                        ($_POST['order'] ? $_POST['order'] : null));

        $by = $_REQUEST['by'] ? $_REQUEST['by'] :
                    ($_GET['by'] ? $_GET['by'] :
                        ($_POST['by'] ? $_POST['by'] : null));

        if($params === null){
	        $params = $_GET;
	    }

	    if($hash !== null){
	        $hash = '#' . $hash;
	    }

        if($order == $field){
            $by = $by === 'ASC' ? 'DESC' : 'ASC';
        }else{
            $by = $default_sort;
        }

		$query = http::build_query(array_merge(
            $params,
            array(
                'order' => $field,
                'by' => $by
                )
            ), '', '&amp;'
        );

        if($order == $field){
        	//$str = ''.$str.'';

        	if($by == 'ASC'){
	    		$image = '/images/sort-up.png';
    		}else {
    			$image = '/images/sort-down.png';
			}
    	}else{
    		$image = '/images/sort.png';
		}

		// Build the string to return...
		$return = '<nobr><a href="' . $url . '?' . $query . $hash . '" class="sortLink">' . $str . '</a> <img width="11" height="12" style="vertical-align: middle; padding: 1px;" alt="sort" src="' . $image . '" /></nobr>';

		return $return;
	}

	final public static function json_compress($arrays){
		foreach($arrays as $array)
		{
		    foreach($array as $key => $value)
		    {
		        if(!is_array($results[$key]))
		        {
		            $results[$key] = array();
		        }
		        $results[$key][] = $value;
		    }
		}

		return $results;
	}

    final public static function getrandchar($type, $length = 1) {
        for($i = 0; $i < $length; $i++){
            // get number - ASCII characters (0:48 through 9:57)
            if ($type=='num') {
                $s .= chr(mt_rand(48,57));
            }

            // get uppercase letter - ASCII characters (a:65 through z:90)
            if ($type=='uc') {
                $s .= chr(mt_rand(65,90));
            }

            // get lowercase letter - ASCII characters (A:97 through Z:122)
            if ($type=='lc') {
                $s .= chr(mt_rand(97,122));
	        }
        }

	    return $s;
    }

    final public static function pre_dump($variable){
        echo '<pre style="font-size:10pt; background-color:white">';
        var_dump($variable);
        echo '</pre>';
    }

    //encodes all strings in array and sub arrays
    final public static function utf8_encode_array($array){
        foreach($array as &$element){
            if(is_string($element)){
                $element = utf8_encode($element);
            }
            else if(is_array($element)){
                $element = strings::utf8_encode_array($element);
            }
        }

        return $array;
    }

    final public static function linkify($string, $target = ''){

    	if ($target !== '') {
    		$target = "_" . $target;
    	}

        $string = preg_replace("/(^|[\n ])([\w]*?)([\w]*?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" target=\"$target\">$3</a>", $string);
        $string = preg_replace("/(^|[\n ])([\w]*?)((www)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" target=\"$target\">$3</a>", $string);
        $string = preg_replace("/(^|[\n ])([\w]*?)((ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ftp://$3\" target=\"$target\">$3</a>", $string);
        $string = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $string);

        return $string;
    }

    final public static function limit_words($string, $num_words){
        $words = explode(' ', $string);
        return implode(' ', array_splice($words, 0, $num_words)) . (count($words) > $num_words ? ' ...' : '');
    }

    //same as native word_wrap function, but only breaks words between spaces.
    final public static function wordwrap($str, $width = 75, $break = "\n", $cut = false){
        $parts = explode(' ', $str);
        foreach($parts as &$part){
            $part = wordwrap($part, $width, $break, $cut);
        }

        $str = implode(' ', $parts);

        return $str;
    }
}

?>
