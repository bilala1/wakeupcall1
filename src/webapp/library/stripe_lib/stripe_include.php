<?php
require_once('Stripe.php');

$stripe = array(
    "secret_key"      => PAYMENT_ID,
    "publishable_key" => PAYMENT_KEY
);

Stripe::setApiKey($stripe['secret_key']);
?>