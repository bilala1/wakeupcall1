<?php
include '../../script-test-include.php';

require_once('./stripe_include.php');

function get_string_or_null($string, $required)
{
    if(!$string || empty($string))
    {
        if($required) throw new Exception('required string cannot be null, skipping user');
        return null;
    }


    return $string;
}

function get_month($date, $required)
{
    if(!$date || empty($date))
    {
        if($required) throw new Exception('required dates cannot be null, skipping user');
        return null;
    }

    $month = date('n', strtotime($date));

    if(!$month || empty($month))
    {
        if($required) throw new Exception('required dates cannot be null, skipping user');
        return null;
    }

    return $month;
}

function get_year($date, $required)
{
    if(!$date || empty($date))
    {
        if($required) throw new Exception('required dates cannot be null, skipping user');
        return null;
    }

    $year = date('Y', strtotime($date));

    if(!$year || empty($year))
    {
        if($required) throw new Exception('required dates cannot be null, skipping user');
        return null;
    }

    return $year;
}

$db = mysqli_db::init();

$members = $db->fetch_all('select *, AES_DECRYPT(members_card_num, \''. AES_KEY .'\') as members_card_num1 from members');
$members_table = new mysqli_db_table('members');


foreach($members as $member)
{
    try
    {
        $token = Stripe_Token::create(array( "card" => array( "number" => get_string_or_null($member['members_card_num1'], true),
            "exp_month" => get_month($member['members_card_expire'], true),
            "exp_year" => get_year($member['members_card_expire'], true),
            "address_line1" =>  get_string_or_null($member['members_billing_addr1'], false),
            "address_line2" =>  get_string_or_null($member['members_billing_addr2'], false),
            "address_city" =>  get_string_or_null($member['members_billing_city'], false),
            "address_state" =>  get_string_or_null($member['members_billing_state'], false),
            "address_zip" =>  get_string_or_null($member['members_billing_zip'], true)
            )));

        if(!$member['members_stripe_id'])
        {
            $customer = Stripe_Customer::create(array( "description" => $member['members_email'],
                "card" => $token->id));

            $member['members_stripe_id'] = $customer->id;

            $members_table->update($member, $member['members_id']);
        }

    }
    catch(Exception $e)
    {
        echo "Card Error for account :" . $member['members_email'] . $e->getMessage(). "<br/>";
    }
}

?>