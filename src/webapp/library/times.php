<?php
#REVISION: 11
#REVISION: 7
#REVISION: 2

final class times {

    function __construct(){
        
    }

    // Function must take a UTC timestamp
    public static function tz_convert($timestamp, $strformat = DATE_FORMAT, $nice = true){
        $date = new DateTime($timestamp, new DateTimeZone("UTC"));
        if(!$_SESSION['timezone']){
            $_SESSION['timezone'] = date_default_timezone_get();
        }
        
        if(!$strformat){
            $strformat = DATE_FORMAT;
        }

        $date->setTimezone(new DateTimeZone($_SESSION['timezone']));
        
        $datetime = new DateTime('now');
        $utc = new DateTimeZone("UTC");
        $datetime->setTimezone($utc);
        
        $interval = $date->diff($datetime);
        $secs  = $interval->format('%s');
        $mins  = $interval->format('%i');
        $days  = $interval->days;
        $hours = $interval->format('%h');
        
        $sameday = ($datetime->format('%m%d%Y') == $date->format('%m%d%Y')) ? true : false;
        
        if($nice){
            if(!$mins and !$hours and !$days){
                $format = ($secs <= 1) ? $secs . ' Second Ago' : $secs . ' Seconds Ago';
            }
            else if($hours < 1 and $mins and !$days){
                $format = ($mins <= 1) ? $mins .' Minute Ago' : $mins .' Minutes Ago';
            }
            else if($hours >= 1 and $hours < 24 and !$days){
                $format = (floor($hours) > 1) ? floor($hours) .' Hours Ago' : floor($hours) . ' Hour ago';
            }
            else if($days < 2){
                $format = '1 Day Ago';
            }
            else {
                $format = $date->format($strformat);
            }
        }
        else {
            $format = $date->format($strformat);
        }
        
        return $format;
    }

    public static function to_utc($datetime, $timezone = null, $format = DateTime::ATOM)
    {
        $datetime = $timezone !== null ? 
            new DateTime($datetime, $timezone) : 
            new DateTime($datetime);
        #pre($datetime);exit;
        $utc = new DateTimeZone('UTC');
        $datetime->setTimezone($utc);
        $datetime = $datetime->format($format);
        
        return $datetime;
    }
    
    /**
     * Given a date string (eg. mm/dd/yyyy hh:mm:ss), returns a mysql formatted date string in UTC time
     * @param string $date_string (eg. mm/dd/yyyy hh:mm:ss)
     * @param string $timezone_str the timezone that $date_string is in (eg. 'America/Los_Angeles') (if left null, uses timezone set in bootstrap.. date_default_timezone_set(...))
     * @return string mysql formatted date string: 'Y-m-d H:i:s'
     */
    public static function to_mysql_utc($date_string, $timezone_str = null){
        $timezone = $timezone_str !== null ? 
            new DateTimeZone($timezone) : 
            null;
            
        return times::to_utc($date_string, $timezone, 'Y-m-d H:i:s');
    }
    
    /**
     * Given a UTC date string from a db select (Y-m-d H:i:s), returns a formatted date string in given timezone
     * @param string $date_string UTC from database (Y-m-d H:i:s)
     * @param string $format the format to return the date in (default: DATE_FORMAT_FULL)
     * @param string $timezone_str the timezone that $date_string is in (eg. 'America/Los_Angeles') (if left null, uses timezone set in bootstrap.. date_default_timezone_set(...))
     * @return string mysql formatted date string: 'Y-m-d H:i:s'
     */
    public static function from_mysql_utc($date_string, $format = DATE_FORMAT_FULL, $timezone_str = null){
        $date = new DateTime($date_string, new DateTimeZone('UTC'));
        
        //use default timezone if one not passed in
        $timezone_str = $timezone_str !== null ? $timezone_str : date_default_timezone_get();
        
        return $date->setTimezone(new DateTimeZone($timezone_str))->format($format);
    }
    
    public static function to_mysql($date_string, $time = true){
        $format = $time ? 'Y-m-d H:i:s' : 'Y-m-d';
        
        return date($format, strtotime($date_string));
    }
    
    public static function days_between($date1, $date2){
        return (strtotime($date1) - strtotime($date2)) / (60 * 60 * 24);
    }
    
    public static function timezones(){
        $dateTimeZoneGMT = new DateTimeZone("Etc/GMT");
        
        foreach(timezone_identifiers_list() as $timezone){
            $dateTimeZone = new DateTimeZone($timezone);
            $dateTime = new DateTime("now", $dateTimeZone);
            $dateTimeGMT = new DateTime("now", $dateTimeZoneGMT);
            $timeOffset = $dateTimeZone->getOffset($dateTimeGMT);
            $timeOffset = $timeOffset / 60 / 60;
            
            $options[$timeOffset . ' ' . $timezone] = array(
                'offset' => $timeOffset, 
                'timezone' => $timezone,
                'str' => '(GMT '.$timeOffset.') '.$timezone
            );
        }
        
        ksort($options, SORT_NUMERIC);
        
        return $options;
    }
    
    //Converts a string to # of hours
    //Only use decimals if $str is # of hours
    public static function strtohours($str){
        if(stristr($str, 'h') && !stristr($str, 'm')){
            $str = preg_replace('/[A-z]/', '', $str);
        }
        else if(stristr($str, 'm') && !stristr($str, 'h')){
            $str = preg_replace('/[A-z]/', '', $str)/60;
        }
        
        if(is_numeric($str)){
            $hours = $str;
        }else{
            $str .= ' ';
            $str = str_replace('hr', 'hour ', $str);
            $str = preg_replace('/h([^o])/i', 'hour $1', $str);
            $str = preg_replace('/m([^i])/i', 'min $1', $str);
            echo $str;
            $time = strtotime($str) - time();
            $hours = $time/60/60;
        }
        
        return $hours;
    }
}

?>
