<?php
#REVISION: 5

final class validate {
    var $inputs  = array();
    var $data    = array();
    var $uniques = array();
    var $body    = false;
    
    var $matches = array();
    
    var $passmin = 6;
    
    var $data_to_test = null;
    
    //var $db = null;
    
    var $errors = array();
    var $fileerrors = array();
    var $errors_strings = array();
    
    //static $email_regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$/';
    //static $email_regex = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/';
    static $email_regex = '/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD';
    
    function __construct($data, $inputfile = false){
        //$this->db = mysqli_db::init();
        $data_to_test = $data;
        
        foreach($data as $key => $val){
            $this->data[$key] = array(
                'value' => $val
            );
        }
        
        if($inputfile){        
            ob_start();
            include($inputfile);
            $html = ob_get_contents();
            ob_end_clean();
            
            $filedom = new simple_dom($html);
            $this->body    = $filedom->body;
            
            $this->buildDataTypes();
        }
    }

    function addMultiLocationSelectionValidation(&$data, $objectType, $object) {
        if(!$this->data['join_licensed_locations']) {
            $licensedLocationData = $this->data['join_licensed_locations'] = array(
                'value' => null
            );
        }
        // Account admins can set the all location flag, and if the all locations flag is set for not account admins leave it
        // If account admin didn't set flag, or it wasn't already set for non-admin, make sure to clear it
        if ((ActiveMemberInfo::_IsAccountAdmin() && in_array('all', $this->data['join_licensed_locations']['value']))
            || (!ActiveMemberInfo::_IsAccountAdmin() && $object[$objectType . 's_all_locations'] == 1)) {
            $this->data['join_licensed_locations']['value'] = $data['join_licensed_locations'] = array();
            $data[$objectType . 's_all_locations'] = 1;
        } else {
            $data[$objectType . 's_all_locations'] = 0;
        }

        // If all location flag isn't set, and a single location user, set the location id.
        // Otherwise, location ID(s) is(are) required
        if($object[$objectType . 's_all_locations'] == 0 && !ActiveMemberInfo::IsUserMultiLocation()) {
            $this->data['join_licensed_locations']['value'] = $data['join_licensed_locations'] = array(ActiveMemberInfo::SingleLocationUserLocationId());
        } else {
            $this->unsetOptionals(array(
                'join_licensed_locations' => 'Location'
            ));
        }

        return $this;
    }
    
    function addUserPermissionValidation(&$data, $form_field,$db_field, $object) {
        // If account admin didn't set flag, or it wasn't already set for non-admin, make sure to clear it
        if ((ActiveMemberInfo::_IsAccountAdmin() &&  $this->data[$form_field]['value']=='on')
            || (!ActiveMemberInfo::_IsAccountAdmin() && $object[$db_field] == 1)) {
            $data[$form_field ] = 'on';
        } else {
            unset($data[$form_field]);
        }
        if(!ActiveMemberInfo::IsUserMultiLocation()) {
            if($object[$db_field] == 1) $data[$form_field ] = 'on';
        }
        return $this;
    }   
    
    function setText($field){
        $this->data[$field]['type'] = 'text';
        return $this;
    }
    
    function setEmail($field, $title = null){
        $this->data[$field]['type'] = 'email';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setPhoneNum($field, $title = null){
        $this->data[$field]['type'] = 'phone';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setInteger($field, $title = null){
        $this->data[$field]['type'] = 'int';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setFloat($field, $title = null){
        $this->data[$field]['type'] = 'float';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setMinLength($field, $length, $title = null){
        $this->data[$field]['minlength'] = $length;
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setMaxLength($field, $length, $title = null){
        $this->data[$field]['maxlength'] = $length;
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    //$allowed = value(s) that are allowed even if it is a duplicate
    function setUnique($field, $table, $title = null, $allowed = null){
        $this->uniques[] = array(
            'field' => $field,
            'table' => $table,
            'title' => $title,
            'allowed' => (array)$allowed);
        
        return $this;
    }
    
    function setDate($field, $title = null){
        $this->data[$field]['type'] = 'date';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function setMatch($match1, $match2, $title = null){
        $match = array($match1, $match2);
        
        if($title){
            $match['title'] = $title;
        }
        
        $this->matches[] = $match;
        
        return $this;
    }
    
    function setCreditCard($field, $title = null){
        $this->data[$field]['type'] = 'credit card';
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    
    //one field or array of fields
    function setOptional($fields){
        foreach((array)$fields as $field){
            $this->data[$field]['optional'] = true;
        }
        
        return $this;
    }
    
    function unsetOptional($field, $title = null){
        $this->data[$field]['optional'] = false;
        unset($this->data[$field]['type']);
        
        if($title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    function unsetOptionals($array){
        foreach($array as $field => $title){
            $this->unsetOptional($field, $title);
        }
        
        return $this;
    }
    
    function setAllOptional(){
        foreach($this->data as &$key){
            $key['optional'] = true;
        }
        unset($key);
        return $this;
    }
    
    function setTitle($field, $title){
        $this->data[$field]['title'] = $title;
        return $this;
    }
    
    function setTitles($array){
        foreach($array as $field => $title){
            $this->setTitle($field, $title);
        }
        
        return $this;
    }
    
    
    var $files = array(), $mimetypes = array(), $exts = array();
    
    function setFile($file, $key){
        #$file['type'] = files::get_mime_type($file['name']);
        $this->files[$key] = $file;
        
        return $this;
    }
    
    function setMimeTypes($_mimetypes = null){
        $mimetypes = is_array($_mimetypes) ? $_mimetypes : func_get_args();
        
        foreach($mimetypes as $mimetype){
            $this->mimetypes[] = $mimetype;
        }
       
        return $this;
    }
    
    function setExtensions($_extensions = null){
        $extensions = is_array($_extensions) ? $_extensions : func_get_args();
        foreach($extensions as $ext){
            $this->exts[] = $ext;
        }
        
        return $this;
    }
    
    function setFileType($type){
        switch($type){
            case 'both':
                $this
                    ->setExtensions('doc', 'docx', 'pdf','jpg','jpeg','png','gif','xls','xlsx')
                    ->setMimeTypes(
                        'application/msword',
                        'application/excel',
                        'application/x-excel',
                        'application/vnd.ms-excel',
                        'application/x-msexcel',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-office',
                        'application/pdf',
                        'application/octet-stream',
                        'image/jpeg',
                        'image/jpg',
                        'image/png',
                        'image/gif' );
            case 'doc':
                $this
                    ->setExtensions('doc', 'docx', 'pdf', 'xls', 'xlsx')
                    ->setMimeTypes(
                        'application/msword',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.ms-office',
                        'application/x-excel',
                        'application/vnd.ms-excel',
                        'application/x-msexcel',
                        'application/pdf',
                        'application/octet-stream' );
            break;
            
            case 'image':
                $this
                    ->setExtensions('jpg', 'jpeg', 'png', 'gif')
                    ->setMimeTypes(
                        'image/jpeg',
                        'image/jpg',
                        'image/png',
                        'image/gif'
                    );
            break;
        }

        return $this;
    }
    
    function buildDataTypes(){
        $elements = $this->body->get_elements('//input', '//select', '//textarea');
        
        foreach($elements as $element){
            $type  = $element->get('tag');
            $name  = $element->get('name');
            $title = $element->get('title');
            $itype = $element->get('type');
            
            $this->setText($name);
            if($title){
                $this->setTitle($name, $title);
            }
            
            if($type == 'input'){
                if(stristr($name, 'email')){
                    $this->setEmail($name);
                }
                else if($itype == 'password'){
                    $this->setMinLength($name, $this->passmin);
                }
                else if($itype == 'submit'){
                    $this->setOptional($name);
                }
            }
        }
    }
    
    //set specific error strings
    function setErrors($errors){
        $this->errors_strings = $errors;

        return $this;
    }
    



    function test(){
        foreach($this->matches as $match){
            if($this->data[$match[0]]['value'] != $this->data[$match[1]]['value']){
                $this->errors[$match[1]] = 'Must match other '.($match['title'] ?: 'value');
            }
        }
        
        foreach($this->uniques as $unique){
            if(!in_array($this->data[$unique['field']]['value'], $unique['allowed'])){
                $is_unique = self::is_unique($unique['field'], $unique['table'], $this->data[$unique['field']]['value']);
                
                if(!$is_unique){
                    if($unique['title']){
                        $error = $unique['title'] . ' is already in use';
                    }else if($this->data[$unique['field']]['title']){
                        $error = $this->data[$unique['field']]['title'] . ' is already in use';
                    }else{
                        $error = 'This is already in use';
                    }
                    
                    $this->errors[$unique['field']] = $error;
                }
            }
        }
        
        foreach($this->data as $key => $val){
            if(isset($data_to_test[$key])){
                continue;
            }
            
            if((!isset($val['type']) || $val['value'] == '') && $val['optional']){
                continue;
            }
            
            // First run generic tests...
            if($val['minlength'] and strlen($val['value']) < $val['minlength']){
                if($val['title']){
                    $msg = $val['title'] .' must have a minimum of '.$val['minlength'].' characters';
                }else{
                    $msg = 'You must enter a minimum of '.$val['minlength'] .' characters';
                }
                
                $this->errors[$key] = $msg;
            }
            else if($val['maxlength'] and strlen($val['value']) > $val['maxlength']){
                if($val['title']){
                    $msg = $val['title'] .' must have a maximum of '.$val['maxlength'].' characters';
                }else{
                    $msg = 'You must enter a maximum of '.$val['maxlength'] .' characters';
                }
                
                $this->errors[$key] = $msg;
            }
            else if($val['value'] == ''){
                if($val['title']){
                    $msg = 'You must enter a value for '.$val['title'];
                }else{
                    $msg = 'Please enter something';
                }
                $this->errors[$key] = $msg;
            }
            
            switch($val['type']){
                case "email":
                    if(!preg_match(self::$email_regex, $val['value'], $email)){
                        $this->errors[$key] = 'You must enter a properly formatted email';
                    }
                break;
                
                case "phone":
                    $string = preg_replace("/[\(\)\-\s]/","", $val['value']);
                    #echo strlen($string);exit;
                    if($val['value'] == '') {
                        if($val['title']) {
                            $this->errors[$key] = 'You must enter a value for ' . $val['title'];
                        }
                        else {
                            $this->errors[$key] = 'Please enter something';
                        }
                    }
                    elseif(!is_numeric($string)) {
                        $this->errors[$key] = $val['title'] . ' cannot contain non-numeric characters except (, ), and -';
                    }
                    elseif(strlen($string) != 10) {
                        $this->errors[$key] = 'Please enter your ten digit phone number';
                    }
                break;
                case "int":
                    if((string)(int) $val['value'] !== (string) $val['value']){
                        if($val['title']){
                            $this->errors[$key] = 'You must enter a whole number for '.$val['title'];
                        }else{
                            $this->errors[$key] = 'You must enter a whole number';
                        }
                    }
                break;

                case "float":
                    if(!is_numeric($val['value'])){
                        if($val['title']){
                            $this->errors[$key] = 'You must enter a decimal for '.$val['title'];
                        }else{
                            $this->errors[$key] = 'You must enter a decimal';
                        }
                    }
                break;

                case "date":
                    if(!strtotime($val['value'])){
                        if($val['title']){
                            $this->errors[$key] = 'You must enter a date for '.$val['title'];
                        }else{
                            $this->errors[$key] = 'You must enter a date';
                        }
                    }
                break;
                
                case "credit card":
                    if(!self::is_valid_credit_card($val['value'])){
                        $this->errors[$key] = 'Credit Card number is incorrect';
                    }
                break;
            }
        }
        
        foreach($this->files as $key => $file){
            if($file['name'] == ''){
                $this->errors[$key] = 'Please upload a file';
            }
            
            #pre($file);pre($this->mimetypes);
            #pre(!in_array($file['type'], $this->mimetypes));  echo '<br />'; pre(!strpos($file['type'],'not open `'));exit;
            
            if($this->mimetypes){
                if(!in_array($file['type'], $this->mimetypes) && !strpos($file['type'],'not open `')){

                    $this->errors[$key] = $file['name'] .' is not a valid file';
                    $this->fileerrors[$key] = $this->errors[$key];
                }
            }
            
            if($this->exts){
                $ext = strtolower($file['name']);
                $exploded_array =  explode('.', $ext);
                $ext = array_pop($exploded_array);
                
                if(!in_array($ext, $this->exts) && !strpos($file['type'],'not open `')){
                    $this->errors[$key] = $file['name'] .' is not a valid file';
                    $this->fileerrors[$key] = $this->errors[$key];
                }
            }
        }
        
        //replace with given error strings
        foreach((array)$this->errors as $key => $error){
            $this->errors[$key] = $this->errors_strings[$key] ?: $this->errors[$key];
        }
        
        return $this->errors;
    }
    
    function getFileErrors(){
        return $this->fileerrors;
    }
    
    final public static function is_unique($field, $table, $value){
        $db = mysqli_db::init();
        
        $result = $db->fetch_one('
            select *
            from ' . $db->escape_column($table) . ' 
            where '.$db->escape_column($field).' = ?',
            array($value)
        );
        
        return !$result;       //if result, return false
    }
    
    final public static function is_email($email){
        $email = strtolower($email);

        if(!preg_match(self::$email_regex, $email, $matches)){
            return false;
        }
        else {
            return true;
        }
    }
    
    final public static function is_valid_credit_card($card_number){
        if(empty($card_number)){
            return false;
        }
        
        //perform mod10 test
        $card_number = strrev($card_number);
        $number_sum = 0;
        for($i = 0; $i < strlen($card_number); $i++){
            $current_number = substr($card_number, $i, 1);
            // Double every second digit
            if($i % 2 == 1){
                $current_number *= 2;
            }
            // Add digits of 2-digit numbers together
            if($current_number > 9){
                $first_number = $current_number % 10;
                $second_number = ($current_number - $first_number) / 10;
                $current_number = $first_number + $second_number;
            }
            $number_sum += $current_number;
        }
        
        return ($number_sum % 10 == 0);
    }
}

?>
