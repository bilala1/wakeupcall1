<?php

final class DiscountCodes
{
    public static function getById($discountCodesId) {
        $db = mysqli_db::init();
        return $db->fetch_one('
			SELECT * FROM discount_codes WHERE discount_codes_id = ?', array($discountCodesId));
    }

    public static function getByIdIfValid($discountCodesId) {
        $db = mysqli_db::init();
        return $db->fetch_one('
			SELECT * FROM discount_codes WHERE discount_codes_id = ?
			AND discount_codes_start <= NOW()
			AND discount_codes_end >= NOW()', array($discountCodesId));
    }
}