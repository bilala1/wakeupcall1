<?php
include(SITE_PATH . '/' . MODELS . '/DynamicEmail/DynDBEmail.php');

//see base class for documentation.
class CertRequestDynamicEmail extends IDynDBEmail
{
    public function __construct()
    {
        $this->add_item("Vendor Name", "vendors_name", "{:vendors_name}");
        $this->add_item("Vendor Address", "vendors_street", "{:vendors_street}");
        
        $this->add_item("Certificate Coverages", "certificates_coverages", "{:certificates_coverages}");
        
        $this->add_item("Location Name", "loc_name", "{:location_name}");        
        $this->add_item("Location Fax #", "members_fax", "{:location_fax}");
        $this->add_item("Location Address", "members_billing_address", "{:location_billing_address}");
        $this->add_item("Additional Insured", "additional_insured", "{:additional_insured}");

        $this->add_item("Send Certificate To... (from selections on Edit Certificate page)", "send_cert_to", "{:send_certificate_to}");
    }
    
    public function generate_html($email_html, $data)
    { 
        $newData = $data;
        $newData['members_billing_address'] =  $data['members_billing_addr1']. '<br/>'.
            ($data['members_billing_addr2'] ? $data['members_billing_addr2'] . '<br />' : '') .
             $data['members_billing_city'] . ', ' . $data['members_billing_state'] . ' ' . $data['members_billing_zip']. '<br/>';

        $sendCertTo = "";
        if($data['certificates_request_cert_to_email']) {
            $sendCertTo .= '<li>Email to ' . $data['certificates_request_cert_to_email'] . '</li>';
        }
        if($data['certificates_request_cert_to_fax'] && $data['members_fax']) {
            $sendCertTo .= '<li>Fax to ' . $data['members_fax'] . '</li>';
        }
        if($data['certificates_request_cert_to_address']) {
            $sendCertTo .= '<li>Mail to ' . $newData['members_billing_address'] . '</li>';
        }

        if($sendCertTo) {
            $sendCertTo = '<ul>' . $sendCertTo . '</ul>';
        } else {
            $sendCertTo = '<p>Please contact us.</p>';
        }

        $newData['send_cert_to'] = $sendCertTo;

        $additional_request = '';
        if($data['certificates_include_additional_insured_request'] == 1){
            $additional_request="<p>In addition, please include the following names as Additional Insureds:</p><ul>";
            if($data['certificate_additional_insured'] != ''){
                //$additional_request_text = preg_split('/\r\n|[\r\n]/', $data['certificate_additional_request_text']);
                $additional_request .= '<li>'.implode('<li>', $data['certificate_additional_insured']);
            }
            $additional_request .= '</ul>';
        }
        $newData['additional_insured'] = $additional_request;

        if ($data['certificates_coverages']) {
            $coverage = '';
            foreach ($data['certificates_coverages'] as $coverages => $amount) {
                $coverage_amount = '';
                if ($amount > 0 && $coverages != "Worker's Compensation (W/C)") {
                    $coverage_amount = '<br>Minimum coverage ' . money_format('%n', $amount);
                }
                if ($amount > 1000000 && $coverages == 'General Liability (GL)') {
                    $coverage_amount = '<br>Minimum coverage ' . money_format('%n', $amount) .
                        ' - can be combination of General Liability and Umbrella/Excess Liability';
                }
                if ($coverages == "Worker's Compensation (W/C)") {
                    $amount1 = $amount['amount1'];
                    $amount2 = $amount['amount2'];
                    $amount3 = $amount['amount3'];
                    if ($amount1 > 0) {
                        $coverage_amount = '<br>Minimum coverage for Injury each Accident ' . money_format('%n', $amount1);
                    }
                    if ($amount2 > 0) {
                        $coverage_amount .= '<br>Minimum coverage for Injury Disease ' . money_format('%n', $amount2);
                    }
                    if ($amount3 > 0) {
                        $coverage_amount .= '<br>Minimum coverage for Injury Policy Limit ' . money_format('%n', $amount3);
                    }
                }
                $coverage .= '<li>' . $coverages . $coverage_amount . '</li>';
            }
        }
        if ($data['certificates_coverage_other']) {
            $coverage .= '<li>' . $data['certificates_coverage_other'];
            if ($data['certificates_coverage_other_amount'] > 0) {
                $coverage .= '<br>Minimum coverage ' . money_format('%n', $data['certificates_coverage_other_amount']);
            }
            $coverage .= '</li>';
        }

        $newData['certificates_coverages'] = '<ul>' . $coverage. '</ul>';
        
        return parent::generate_html($email_html, $newData);
    }
}
