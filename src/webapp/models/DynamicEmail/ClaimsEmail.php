<?php
include(SITE_PATH . '/' . MODELS . '/DynamicEmail/DynDBEmail.php');

//see base class for documentation.
class ClaimsRequestDynamicEmail extends IDynDBEmail
{
    public function __construct()
    {
        $this->add_item("Date", "todays_date", "{:todays_date}");
        $this->add_item("Carriers Name", "carriers_name", "{:carriers_name}");
        $this->add_item("Claim Location", "claim_location", "{:claim_location}");
        $this->add_item("Carrier's Policy Number", "carriers_policy_number", "{:policy_number}");
        $this->add_item("Claimant", "claimant", "{:claimant}");
        $this->add_item("Date of Incident", "date_of_incident", "{:date_of_incident}");
        $this->add_item("Date Notified", "date_notified", "{:date_notified}");
        $this->add_item("Brief Description", "brief_description", "{:brief_description}");
        $this->add_item("Member Firstname", "members_firstname", "{:members_firstname}");
        $this->add_item("Member Lastname", "members_lastname", "{:members_lastname}");        
    }
}
