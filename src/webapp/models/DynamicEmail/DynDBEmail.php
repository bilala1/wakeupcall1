<?php

/// class: DynDBDataField
/// This class handles storing a data field, it's name/description, and markup
/// text.  It also handles replacing the data field in the HTML, as well as
/// generating the java script for it's tinyMCE menu item.
class DynDBDataField
{
    //*
    //The Text that is displayed by the TinyMCE UI Item
    //*
    public $UIFieldName = "";
    
    //*
    //The data field that is pulled from the $Data param of
    //the GenerateEmail function if IDynDBEmail classes
    //*
    public $DataFieldName = "";
    
    //*
    //The Escaped text that is found and replaced in the e-mail HTML
    //with dynamic data
    //*
    public $EscapedFieldText = "";
    
    
    /**
    // function init
    // @param $uifieldname - the name of the field to be displayed in UI
    // @param $datafieldname - the array index in the data array passed into replace_in_html
    // @param $escapedfieldtext - escape text that is replaced with dynamic DB data in the final HTML
    */
    public function init($uifieldname, $datafieldname, $escapedfieldtext)
    {
        $this->UIFieldName = $uifieldname;
        $this->DataFieldName = $datafieldname;
        $this->EscapedFieldText = $escapedfieldtext;
    }
    
    /**
    // function serialize_jscript_item()
    // @return (string) - returns javascript to be used in a tinyMCE menu
    */
    public function serialize_jscript_item()
    {
        $jscript_str = "{";
        
        $jscript_str = $jscript_str . 'DisplayText: "' . $this->UIFieldName . '",'; 
                
        $jscript_str = $jscript_str .'MarkupText: "' . $this->EscapedFieldText .'"';         
        
        $jscript_str = $jscript_str . "}";
        
        return $jscript_str;
    }
    
    /**
    // function replace_in_html
    // @param $html_str - email HTML string
    // @param $data - data array
    // @return (string) email HTML with dynamic fields replaced with database data
    */
    public function replace_in_html($html_str, $data)
    {
        $datavalue = $data[$this->DataFieldName];
        
        $returnstr = str_replace($this->EscapedFieldText, $datavalue, $html_str);
                
        return $returnstr;
    }
}

/// Interface - IDynDBEMail
/// Interface and base functions for building an array of Dynamic Field Types
/// and replacing them in an e-mail.  The best way to use it is to implement it,
/// add your items from the child class' constructor, and override any function
/// for customization as needed.
class IDynDBEmail
{
    private $DynamicFieldTypes =  array();
    
    /**
    // function add_item  - Adds another dynamic field to the collection.
    // @param $uifieldname - the name of the field to be displayed in UI
    // @param $datafieldname - the array index in the data array passed into replace_in_html
    // @param $escapedfieldtext - escape text that is replaced with dynamic DB data in the final HTML 
    */
    public function add_item($uifieldname, $datafieldname, $escapedfieldtext)
    {
        $item = new DynDBDataField();
        $item->init($uifieldname, $datafieldname, $escapedfieldtext);
        
        $this->DynamicFieldTypes[] = $item;
        
        return $item;
    }
    
    /**
    // function generate_html
    // @param $html_str - email HTML string
    // @param $data - data array
    // @returns returns (string) email HTML with dynamic fields replaced with database data
    */
    public function generate_html($email_html, $data)
    {
        if(count($this->DynamicFieldTypes.get) == 0)
            return $email_html;
        
        $return_str = $email_html;
        
        foreach($this->DynamicFieldTypes as $fieldtype)
        {
            $return_str =  $fieldtype->replace_in_html($return_str, $data);
        }
                
        return $return_str;
    }
        
    /**
    // function: serialize_jscript
    // @return: Javascript Menu text of all child Dynamic Fields
    */
    public function serialize_jscript()
    {
        if(count($this->DynamicFieldTypes.get) == 0)
            return "null";
        
        $jscript_str = "[";
        
        foreach($this->DynamicFieldTypes as $fieldtype)
        {
            $jscript_str = $jscript_str . $fieldtype->serialize_jscript_item() . ", ";   
        }
        
        $jscript_str = $jscript_str . "]";
        
        return $jscript_str;
    }
}
?>