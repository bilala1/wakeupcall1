<?php

/// class TmpEmailFileHandler
///
/// This class handles getting, retrieving, and cleaning up temporary email files
/// used primarily for on-the-fly editing.
///
class TmpEmailFileHandler
{
    public static $dyndb_website_file_root = SITE_PATH;
    public static $dyndb_imageFolder = "/data/emails/";
        
    /**
    // function GetTempEmailFile
    // @param $fileName - file name to get
    // @param $cleanUpTmpFile (boolean) - true if you want to delete the temp file after access
    // 
    // @return (string) email file text.
    */
    public static function GetTempEmailFile($fileName, $cleanUpTmpFile = false)
    {
         $filetoread = TmpEmailFileHandler::$dyndb_website_file_root . TmpEmailFileHandler::$dyndb_imageFolder . $fileName;
         
        if (file_exists($filetoread)) 
        {
            ob_start();
            include($filetoread);
            $message = ob_get_clean();
            
            if($cleanUpTmpFile == true)
                unlink($filetoread);
            
            return $message;
        }
        
        return NULL;
    }
    
    /**
    // function WriteTempEmailFile
    // 
    // @param $fileName (string) - filename to save the file in predefined temp directory 
    // @param $html (string) - html text of e-mail
    */
    public static function WriteTempEmailFile($fileName, $html)
    {    
        $filetowrite = TmpEmailFileHandler::$dyndb_website_file_root . TmpEmailFileHandler::$dyndb_imageFolder . $fileName;
        
        if (file_exists($filetowrite)) 
        {
            unlink($filetowrite);
        }
        
        $myfile = fopen($filetowrite, "w");
        fwrite($myfile, $html);
        fclose($myfile);
    }
}
?>