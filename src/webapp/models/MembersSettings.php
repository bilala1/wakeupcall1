<?php

/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 10/4/15
 * Time: 10:14 AM
 */
class MembersSettings
{
    /**
     * @param $area
     * @return bool
     */
    public static function showInfoBoxFor($area) {
        $db = mysqli_db::init();
        $infobox = $db->fetch_singlet('SELECT members_settings_infobox_'.$area.' FROM members_settings WHERE join_members_id = ?', array(ActiveMemberInfo::GetMemberId()));
        if ( $db->count() == 0 ) {
            $infobox = true;
        }
        return $infobox;
    }
}