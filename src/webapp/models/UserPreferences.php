<?php

/**
 * Created by PhpStorm.
 * User: jpatula
 * Date: 8/3/16
 * Time: 10:29 AM
 */
class UserPreferences
{
    public static function getForMember($membersId) {
        $db = mysqli_db::init();
        $userPrefs = $db->fetch_one('SELECT * FROM user_prefs WHERE join_members_id = ?', array($membersId));
        if(!$userPrefs) {
            $db->query('INSERT INTO user_prefs (join_members_id) VALUES (?)', array($membersId));
            $userPrefs = $db->fetch_one('SELECT * FROM user_prefs WHERE join_members_id = ?', array($membersId));
        }
        return $userPrefs;
    }

    public static function setForMember($membersId, $preference, $value) {
        $userPrefs = UserPreferences::getForMember($membersId);

        $userPrefsTable = new mysqli_db_table('user_prefs');
        $userPrefsTable->update(array($preference => $value), $userPrefs['user_prefs_id']);
    }
}