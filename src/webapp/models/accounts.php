<?

class accounts
{

    static function create($account) {
        $accounts_table = new mysqli_db_table('accounts');
       
        $accounts_table->insert($account);
        
        $account_id = $accounts_table->last_id();

        return $account_id;
    }

    static function get_by_id($id)
    {
        $db = mysqli_db::init();

        $account = $db->fetch_one('
            SELECT *
            FROM accounts
            WHERE accounts_id = ?', array($id));

        return $account ? $account : false;
    }

    static function isLocked($id)
    {
        $db = mysqli_db::init();

        $accountIsLocked = $db->fetch_singlet('
            SELECT accounts_is_locked
            FROM accounts
            WHERE accounts_id = ?', array($id));

        return !!$accountIsLocked;
    }

    public static function delete_account($accounts_id){
        $db = mysqli_db::init();

        echo '<br /><br />Deleting account: '.$accounts_id;

        // Delete claims
        $claims_to_remove = $db->fetch_all('SELECT claims_files_file
                                              FROM claims_files AS cf
                                              JOIN claims AS c ON(cf.join_claims_id = c.claims_id)
                                              JOIN licensed_locations AS ll ON(c.join_licensed_locations_id = ll.licensed_locations_id)
                                              WHERE ll.join_accounts_id = ? ', array($accounts_id));

        foreach($claims_to_remove as $claim_remove){
            //remove file from system
            $uploaddir = SITE_PATH .'/data/claims/';
            unlink($uploaddir.$claim_remove['claims_files_file']);
            //remove claims rows
            $db->query('DELETE
                            FROM claims_files
                            WHERE claims_files_file = ? ', array($claim_remove['claims_files_file']));
        }
        $db->query('DELETE FROM claims c
                      JOIN licensed_locations AS ll ON(c.join_licensed_locations_id = ll.licensed_locations_id)
                      WHERE ll.join_accounts_id = ? ', array($accounts_id));

        //delete all licensed_locations
        $licensed_locations = $db->fetch_singlets('SELECT licensed_locations_id FROM licensed_locations WHERE join_accounts_id = ? ', array($accounts_id));
        foreach($licensed_locations as $licensed_locations_id){
            licensed_locations::delete($licensed_locations_id);
        }

        //-------------------------------------------------------------------------------------
        // -- delete Certificates
        //-------------------------------------------------------------------------------------
        $certificates_to_remove = $db->fetch_all('SELECT certificates_id, certificates_file
                                                  FROM certificates
                                                  WHERE join_accounts_id = ? ', array($accounts_id));
        foreach($certificates_to_remove as $certificate_remove){
            //remove file from system
            $uploaddir = SITE_PATH .'/data/certificates/';
            unlink($uploaddir.$certificate_remove['certificates_file']);
            //delete download stats
            $db->query('DELETE FROM downloads WHERE downloads_type = "certificate" AND join_id = ?', array($certificate_remove['certificates_id']));
        }
        $db->query('DELETE FROM certificates
                           WHERE join_accounts_id = ? ', array($accounts_id));


        //-------------------------------------------------------------------------------------
        // -- delete Claim Files
        //-------------------------------------------------------------------------------------
        //claim files to be deleted
        $claims_to_remove = $db->fetch_all('SELECT claims_files_file
                                                  FROM claims_files AS cf
                                                  JOIN claims AS c ON(cf.join_claims_id = c.claims_id)
                                                  JOIN licensed_locations AS ll ON(c.join_licensed_locations_id = ll.licensed_locations_id)
                                                  WHERE ll.join_accounts_id = ? ', array($accounts_id));
        foreach($claims_to_remove as $claim_remove){
            //remove file from system
            $uploaddir = SITE_PATH .'/data/claims/';
            unlink($uploaddir.$claim_remove['claims_files_file']);
            //remove claims rows (claims_files_file should be as good as id)
            $db->query('DELETE
                        FROM claims_files
                        WHERE claims_files_file = ? ', array($claim_remove['claims_files_file']));
        }
        $db->query('DELETE claims
                      FROM claims
                      JOIN licensed_locations ON join_licensed_locations_id = licensed_locations_id
                      WHERE join_accounts_id = ? ', array($accounts_id));

        //-------------------------------------------------------------------------------------
        // -- delete Vendors
        //-------------------------------------------------------------------------------------
        $db->query('DELETE FROM vendors
                           WHERE join_accounts_id = ? ', array($accounts_id));

        //TODO accounts Not sure what all tables are in this model
        //-------------------------------------------------------------------------------------
        // -- delete Carriers
        //-------------------------------------------------------------------------------------
//        $db->query('DELETE FROM carriers
//                           WHERE join_accounts_id = ? ', array($accounts_id));

        //TODO accounts What else needs to be deleted?  contracts, bills, ???

        $corporations_id = $db->fetch_singlet('SELECT corporations_id FROM corporations WHERE join_accounts_id = ?', array($accounts_id));
        if($corporations_id) {
            //-------------------------------------------------------------------------------------
            // -- delete corporation forum, and forum activities
            //-------------------------------------------------------------------------------------

            //------------------------------------------------------------------
            // -- delete files per forum posts
            //------------------------------------------------------------------
            $forum_posts_files = $db->fetch_all('   SELECT forums_posts_files_filename
                                                        FROM forums_posts_files
                                                        WHERE join_posts_id IN (SELECT posts_id
                                                                                FROM `forums_posts`
                                                                                WHERE `join_topics_id` IN (SELECT topics_id
                                                                                                           FROM forums_topics
                                                                                                           WHERE join_forums_id = (SELECT join_forums_id
                                                                                                                                   FROM corporations
                                                                                                                                   WHERE corporations_id = ?)) ) ', array($corporations_id));
            foreach($forum_posts_files as $forum_posts_file){
                //remove posts files
                unlink(SITE_PATH .'/data/'.$forum_posts_file['forums_posts_files_filename']);
            }
            //delete
            $db->query('DELETE
                                FROM forums_posts_files
                                WHERE join_posts_id IN (SELECT posts_id
                                                        FROM forums_posts
                                                        WHERE `join_topics_id` IN (SELECT topics_id
                                                                                   FROM forums_topics
                                                                                   WHERE join_forums_id = (SELECT join_forums_id
                                                                                                           FROM corporations
                                                                                                           WHERE corporations_id = ?)) ) ', array($corporations_id));
            //-----------------------------------------------------------------
            //delete where posts id matches
            $db->query('DELETE
                                FROM forums_posts_reported
                                WHERE join_posts_id IN (SELECT posts_id
                                                        FROM `forums_posts`
                                                        WHERE `join_topics_id` IN (SELECT topics_id
                                                                                   FROM forums_topics
                                                                                   WHERE join_forums_id = (SELECT join_forums_id
                                                                                                           FROM corporations
                                                                                                           WHERE corporations_id = ?)) ) ', array($corporations_id));

            $db->query('DELETE
                                FROM forums_posts
                                WHERE join_topics_id IN (SELECT topics_id
                                                           FROM forums_topics
                                                           WHERE join_forums_id = (SELECT join_forums_id
                                                                                   FROM corporations
                                                                                   WHERE corporations_id = ?)) ', array($corporations_id));
            //delete where forum groups id matches
            $db->query('DELETE
                                FROM forums_groups_x_forums
                                WHERE join_groups_id IN(SELECT groups_id
                                                        FROM forums_groups
                                                        WHERE join_members_id = (SELECT join_members_id
                                                                                 FROM corporations
                                                                                 WHERE corporations_id = ?))', array($corporations_id));
            //delete where forum id matches
            $db->query('DELETE
                                FROM forums_groups_x_forums
                                WHERE join_forums_id =(SELECT join_forums_id
                                                       FROM corporations
                                                       WHERE corporations_id = ?)', array($corporations_id));
            //delete where forum groups id matches
            $db->query('DELETE
                                FROM forums_groups_x_members
                                WHERE join_groups_id IN(SELECT groups_id
                                                        FROM forums_groups
                                                        WHERE join_members_id = (SELECT join_members_id
                                                                                 FROM corporations
                                                                                 WHERE corporations_id = ?))', array($corporations_id));
            //delete where members id matches
            $db->query('DELETE
                                FROM forums_watchdogs
                                WHERE join_members_id = (SELECT join_members_id
                                                           FROM corporations
                                                           WHERE corporations_id = ?)', array($corporations_id));
            // delete where topics id matches
            $db->query('DELETE
                                FROM forums_watchdogs
                                WHERE join_topics_id IN(SELECT topics_id
                                                        FROM forums_topics
                                                        WHERE join_forums_id = (SELECT join_forums_id
                                                                                FROM corporations
                                                                                WHERE corporations_id = ?))', array($corporations_id));
            $db->query('DELETE
                                FROM forums_topics
                                WHERE join_forums_id = (SELECT join_forums_id
                                                        FROM corporations
                                                        WHERE corporations_id = ?) ', array($corporations_id));
            $db->query('DELETE
                                FROM forums_groups
                                WHERE join_members_id = (SELECT join_members_id
                                                           FROM corporations
                                                           WHERE corporations_id = ?)', array($corporations_id));
            $db->query('DELETE
                                FROM forums
                                WHERE forums_id = (SELECT join_forums_id
                                                   FROM corporations
                                                   WHERE corporations_id = ?) ', array($corporations_id));

            //-------------------------------------------------------------------------------------
            // -- delete corporation
            //-------------------------------------------------------------------------------------
            $db->query('DELETE FROM corporations WHERE corporations_id = ? ', array($corporations_id));
        }

        $members = $db->fetch_singlets('SELECT members_id FROM members WHERE join_accounts_id = ?');
        foreach($members as $members_id) {
            members::remove_all_data($members_id);
        }
    }

    static function getParentAccounts($except = null) {
        $db = mysqli_db::init();
        $params = array();
        $query = 'SELECT * FROM accounts where accounts_type = "parent"';
        if($except != null) {
            $query .= ' AND accounts_id != ?';
            $params[] = $except;
        }
        $query .= ' ORDER BY accounts_name';
        return $db->fetch_all($query, $params);
    }

    static function getAll($except = null) {
        $db = mysqli_db::init();
        $params = array();
        $query = 'SELECT * FROM accounts';
        if($except != null) {
            $query .= ' WHERE accounts_id != ?';
            $params[] = $except;
        }
        $query .= ' ORDER BY accounts_name';
        return $db->fetch_all($query, $params);
    }

    static function getAccountsThatCanGainALocation() {
        $db = mysqli_db::init();
        $params = array();
        $query = 'SELECT * FROM accounts WHERE
          accounts_type != "single"
          OR NOT EXISTS (SELECT 1 from licensed_locations WHERE join_accounts_id = accounts_id)';
        if($except != null) {
            $query .= ' WHERE accounts_id != ?';
            $params[] = $except;
        }
        $query .= ' ORDER BY accounts_name';
        return $db->fetch_all($query, $params);
    }

    static function getChildAccounts($parentAccountId) {
        $db = mysqli_db::init();
        $params = array();
        $query = 'SELECT * FROM accounts where join_accounts_id_parent = ?';
        return $db->fetch_all($query, array($parentAccountId));
    }

    static function accountHasPaidBill($accountId) {
        $db = mysqli_db::init();

        $query = '
          SELECT bills_id FROM bills b
           INNER JOIN accounts a on b.join_members_id = a.join_members_id
           WHERE accounts_id = ?
             AND bills_paid = 1';

        return !!($db->fetch_singlet($query, array($accountId)));
    }


    /**
     * Gets the main admin of the account of the current user.  Used for billing, etc.
     */
    static function getMainAdmin($accountsId)
    {
        $db = mysqli_db::init();
        return $db->fetch_one(
            'SELECT m.* from members m
             INNER JOIN accounts a ON a.join_members_id = m.members_id
             WHERE a.accounts_id = ?'
            , array($accountsId));
    }
}

?>