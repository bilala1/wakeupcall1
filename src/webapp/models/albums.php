<?php

final class albums {
    
    public static function get_all_albums($albums_id = 0, $homepage_album = true){
        $db = mysqli_db::init();
        
        $where  = array();
        $params = array();
        
        if($albums_id){
            $where  []= 'albums_id = ?';
            $params []= (int) $albums_id;
        }
        
        if(!$homepage_album){
            $where  []= 'albums_name != "homepage"';
        }
        
        $albums = $db->fetch_all('SELECT * FROM albums
            '.strings::where($where).'
            ORDER BY albums_id', $params);
        return $albums;
    }
    
    public static function get_homepage_photos(){
        $db = mysqli_db::init();
        
        return $db->fetch_all('
            SELECT *
            FROM albums_photos
            JOIN albums ON albums_id = join_albums_id
            WHERE albums_name = "homepage"
            AND albums_photos_published = 1
            ORDER BY albums_photos_name');
    }
    
    public static function get_photos($albums_id){
        $db = mysqli_db::init();
        
        return $db->fetch_all('
            SELECT *
            FROM albums_photos
            WHERE join_albums_id = ?
            AND albums_photos_published = 1', array($albums_id));
    }
    
    public static function get_by_photo($albums_photos_id){
        $db = mysqli_db::init();
        
        return $db->fetch_one('
            SELECT a.*
            FROM albums AS a
            JOIN albums_photos AS ap ON 
                ap.join_albums_id = a.albums_id AND
                ap.albums_photos_id = ?', array($albums_photos_id));
    }
    
    public static function get_photo($albums_photos_id){
        $db = mysqli_db::init();
        
        return $db->fetch_one('
            SELECT *
            FROM albums_photos
            WHERE albums_photos_id = ?', array($albums_photos_id));
    }
}

?>
