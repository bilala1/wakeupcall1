<?php
final class article_categories extends mysqli_db_table
{
    function __construct()
    {
        parent::__construct('articles_categories');
    }
    
    public static function _search($props)
    {
        $categories = new article_categories();
        $result = $categories->search($props);
        
        return $result['categories'];
    }
    
    public function search($props)
    {
        $db = mysqli_db::init();
        //----------------------------------------------------------------------
        //-- Pagination Settings
        //----------------------------------------------------------------------
        $current_page = ($props['p']) ? intval($props['p']) : 0;
        $per_page = ($props['perpg']) ? intval($props['perpg']) : 10;
        $max_pages = 5; // only should 5 pages left or right
        //----------------------------------------------------------------------
        //-- Get Categories
        //----------------------------------------------------------------------
        $where = array();
        $params = array();
        
        //----------------------------------------------------------------------
        // Only get category
        //----------------------------------------------------------------------
        if(isset($props['has_posts']) and $props['has_posts'])
        {    
            $where []= 'a.articles_id IS NOT NULL';
        }
        
        $where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';
        $categories = $db->fetch_all('SELECT ac.*, COUNT(DISTINCT a.articles_id) AS total_articles
            FROM articles_categories AS ac 
            LEFT JOIN articles AS a ON a.join_articles_categories_id = ac.articles_categories_id
            ' . $where . '
            GROUP BY ac.articles_categories_id
            ORDER BY articles_categories_order, articles_categories_name
        ', $params);
        //----------------------------------------------------------------------
        //-- Get Total Posts
        //----------------------------------------------------------------------
        $total = $db->fetch_singlet('SELECT COUNT(DISTINCT articles_categories_id) 
            FROM articles_categories AS ac 
            LEFT JOIN articles AS a ON a.join_articles_categories_id = ac.articles_categories_id
            ' . $where, $params);
        $pages = ceil($total / $per_page);
        //----------------------------------------------------------------------
        //-- Get Pagination
        //----------------------------------------------------------------------
        $pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/articles/news.php');
        
        return array(
            'categories' => $categories,
            'total' => $total,
            'pages' => $pages
        );
    }
    
    static function get_ordered($params = array()) 
    {
        $db = mysqli_db::init();
        $categories = $db->fetch_all('SELECT * FROM articles_categories ORDER BY articles_categories_order');
        return $categories;
    }
}
?>
