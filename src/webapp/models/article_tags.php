<?php
final class article_tags
{
    static function get_ordered($params = array()) 
    {
        $db = mysqli_db::init();
        $tags = $db->fetch_all('SELECT * FROM articles_tags ORDER BY articles_tags_name');
        return $tags;
    }

    static function calculate_tag_depth()
    {
        $db = mysqli_db::init();

        $tags = $db->fetch_all('SELECT articles_tags.*, COUNT(articles_id) AS total_articles FROM articles_tags 
            LEFT JOIN articles ON articles.join_articles_tags_id = articles_tags.articles_tags_id
            GROUP BY articles_tags_id
        ');

        foreach($tags as $tag)
        {
            $tag['total_articles'];
            $tagtbl = new mysqli_db_table('tags');
            $tagtbl->update(array(
                'articles_tags_total_articles' => $tag['total_articles']
            ), $tag['articles_tags_id']);
        }
    }
}
?>
