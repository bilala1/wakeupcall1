<?php

class articles extends mysqli_db_table
{
    function __construct()
    {
        parent::__construct('articles');
    }

    public function search($props)
    {
        $db = mysqli_db::init();
        //----------------------------------------------------------------------
        //-- Pagination Settings
        //----------------------------------------------------------------------
        $current_page = ($props['p']) ? intval($props['p']) : 0;
        $per_page = ($props['perpg']) ? intval($props['perpg']) : 10;
        $max_pages = 5; // only should 5 pages left or right
        //----------------------------------------------------------------------
        //-- Query
        //----------------------------------------------------------------------
        $where  = array();
        $params = array();

        $where  []= 'articles_active = ?';
        $params []= '1';

        if($props['global_tags_id'] and is_array($props['global_tags_id'])){
            $ors = array();
            foreach($props['global_tags_id'] as $global_tags_id)
            {
                $ors    []= 'FIND_IN_SET(?, join_articles_tags)';
                $params []= $global_tags_id;
            }
            $where  []= '(' . implode(' OR ', $ors) . ')';
        }

        if(isset($props['articles_categories_id']))
        {
            $where  []= 'articles.join_articles_categories_id = ?';
            $params []= $props['articles_categories_id'];
        }

        $where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';
        //------------------------------------------------------------------------------
        //-- Order
        //------------------------------------------------------------------------------
        switch($props['order']) {
            case 'articles_newsdate':
                $order = 'articles.articles_newsdate';
            break;

            case 'articles_title':
                $order = 'articles.articles_title';
            break;

            case 'articles_teaser':
                $order = 'articles.articles_teaser';
            break;

            case 'articles_views':
                $order = 'articles.articles_views';
            break;

            case 'total_comments':
                $order = 'total_comments';
            break;

            case 'articles_rating':
                $order = 'articles.articles_rating';
            break;

            default:
                $_REQUEST['order'] = 'articles_newsdate';
                $_REQUEST['by'] = 'DESC';
            case 'articles_newsdate':
                $order = 'articles.articles_order, articles.articles_newsdate';
            break;
        }

        $by = ($props['by'] == 'ASC') ? 'ASC' : 'DESC';
        $orderby = $order . ' ' . $by;
        //----------------------------------------------------------------------
        //-- Get Articles
        //----------------------------------------------------------------------
        $articles = $db->fetch_all('SELECT articles.*, DATE_FORMAT(articles_newsdate, "%M %d, %Y") AS articles_newsdate,
                COUNT(articles_comments_id) AS articles_comments
            FROM articles
            LEFT JOIN articles_comments ON articles_comments.join_articles_id = articles.articles_id
            ' . $where . '
            GROUP BY articles_id
            ORDER BY ' . $orderby . '
            LIMIT ' . ($current_page * $per_page) . ', ' . $per_page,
            $params
        );
        //----------------------------------------------------------------------
        //-- Get Total Posts
        //----------------------------------------------------------------------
        $total = $db->fetch_singlet('SELECT COUNT(*) FROM articles ' . $where, $params);
        $pages = ceil($total / $per_page);
        //----------------------------------------------------------------------
        //-- Get Pagination
        //----------------------------------------------------------------------
        $pages = html::paginate($_GET, $current_page, $max_pages, $pages, FULLURL . '/articles/news.php');

        foreach($articles as &$article)
        {
            $params = array();
            if(empty($article['articles_page_url']))
            {
                $params['articles_id'] = $article['articles_id'];
            }
            $article['articles_page_url'] = $article['articles_page_url'] ? $article['articles_page_url'] : 'articles/detail.php';
            if(!empty($params))
            {
                $article['articles_page_url'] .= '?' . http::build_query($params);
            }
            unset($article);
        }

        return array(
            'articles' => $articles,
            'total' => $total,
            'pages' => $pages
        );
    }

    public static function get_article_category_name($articles_categories_id){
        $db = mysqli_db::init();

        return $db->fetch_singlet('select articles_categories_name from articles_categories where articles_categories_id = ?', array($articles_categories_id));
    }

    public static function get_article_title($articles_id){
        $db = mysqli_db::init();

        return $db->fetch_singlet('select articles_title from articles where articles_id = ?', array($articles_id));
    }

    public static function category_has_articles($articles_categories_id){
        $db = mysqli_db::init();

        $articles_id = $db->fetch_singlet('select articles_id from articles where join_articles_categories_id = ? limit 1', array($articles_categories_id));

        return !!($articles_id);
    }

    public static function format($articles)
    {
        foreach($articles as &$article)
        {
            $params = array();
            if(empty($article['articles_page_url']))
            {
                $params['articles_id'] = $article['articles_id'];
            }
            $article['articles_page_url'] = $article['articles_page_url'] ? $article['articles_page_url'] : 'articles-detail.php';
            if(!empty($params))
            {
                $article['articles_page_url'] .= '?' . http::build_query($params);
            }
            unset($article);
        }
        return $articles;
    }

    public static function get_recent($limit = 10)
    {
        $db = mysqli_db::init();

        $articles = $db->fetch_all('SELECT * FROM articles WHERE articles_active = 1 ORDER BY articles_order, articles_newsdate DESC LIMIT ?', array($limit));
        $articles = self::format($articles);

        return $articles;
    }

    public static function get_rating_bg_position($user_rating, $max_rating, $max_size)
    {
        return ($max_size - (($user_rating / $max_rating) * $max_size)) * -1;
    }

    public static function recalc_rating($articles_id)
    {
        $db = mysqli_db::init();

        $db->query('UPDATE articles SET articles_rating = (
            SELECT AVG(articles_comments_rating)
            FROM articles_comments
            WHERE join_articles_id = ?) WHERE articles_id = ?', array($articles_id,$articles_id));
    }

    public static function get_top_contributors($num)
    {
        $db = mysqli_db::init();

        return $db->fetch_all('SELECT * FROM members ORDER BY members_totalpoints DESC LIMIT '.$num);
    }

    //return comma separated list of usernames
    public static function get_top_contributor_names($num)
    {
        $members = articles::get_top_contributors($num);
        $names = array();
        foreach($members as $member){
            $names[] = $member['members_username'];
        }
        return implode(', ',$names);
    }

    public static function get_top_articles($num)
    {
        $result = self::search(array(
            'order' => 'articles_rating',
            'by' => 'DESC',
            'per_page' => $num
        ));
        $articles = $result['articles'];
        $articles = self::format($articles);
        return $articles;
    }
}

?>
