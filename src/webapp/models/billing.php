<?php

final class Billing
{

    public static function calculateNextBillForAccount($accountId)
    {

        $member = array_merge(accounts::getMainAdmin($accountId), accounts::get_by_id($accountId));

        $locations = licensed_locations::get_for_account($accountId);
        $num_locations = 0;

        foreach($locations as $location) {
            if($location['licensed_locations_delete_datetime'] == null) {
                $num_locations++;
            }
        }

        $num_billed_locations = max(1, $num_locations);
        
        if($member['accounts_type'] == 'multi') {
            $num_billed_locations = max(1, $num_billed_locations - 1);
        }

        $bills_description = "Subscription";

        if ("yearly" == $member['members_billing_type']) {
            $location_cost = YEARLY_COST;
            $bills_description = "Yearly Subscription";
        } else {
            if ("monthly" == $member['members_billing_type']) {
                $location_cost = MONTHLY_COST;
                $bills_description = "Monthly Subscription";
            }
        }

        $total_cost = $location_cost * $num_billed_locations;
        $initial_amount = $total_cost;
        $discount_code = null;
        $discount_percent = 0;
        $num_discounted = 0;

        if (isset($member['join_discount_codes_id'])) {
            $code = DiscountCodes::getByIdIfValid($member['join_discount_codes_id']);
            if (!empty($code)) {
                $num_discounted = $num_billed_locations;
                if ($code['discount_codes_max_uses'] > 0) {
                    // If there is a maximum allowable number of uses for the code, use the smaller or the number of locations, or the number of uses left.
                    $num_discounted = min($num_billed_locations,
                        $code['discount_codes_max_uses'] - $code['discount_codes_uses']);
                }
                if ($num_discounted < $num_billed_locations) {
                    $bills_description .= " - Discount applied to " . $num_discounted . " of " . $num_billed_locations . " location(s)";
                }
                $num_full_price = $num_billed_locations - $num_discounted;
                $total_cost = $location_cost * $num_full_price;
                if ($code['discount_codes_amount'] > 0) {
                    // If discount is bigger than location cost, we don't want to actually refund money - just make the cost 0
                    $total_cost += max(0, ($location_cost - $code['discount_codes_amount']) * $num_discounted);
                } else {
                    // If discount is bigger than location cost, we don't want to actually refund money - just make the cost 0
                    $total_cost += max(0,
                        $location_cost * ((100 - $code['discount_codes_percent']) / 100) * $num_discounted);
                }
                $discount_code = $member['join_discount_codes_id'];
            }
            $discount_percent = (1 - ($total_cost / $initial_amount)) * 100;
            $discount_percent = floor($discount_percent);
        }

        return array(
            'bills_amount' => $total_cost,
            'initial_amount' => $initial_amount,
            'discount_codes_id' => $discount_code,
            'discount_percent' => $discount_percent,
            'num_discounted' => $num_discounted,
            'num_locations' => $num_locations,
            'num_billed_locations' => $num_billed_locations,
            'location_cost' => $location_cost,
            'accounts_type' => $member['accounts_type'],
            'bills_description' => $bills_description
        );
    }

}