<?
final class carriers
{
    const HIDDEN_BOTH = "both";
    const HIDDEN_HIDDEN = "hidden";
    const HIDDEN_VISIBLE = "visible";

    public static $carriers_new = array();

    public static function create($carrier)
    {
        $carriers_table = new mysqli_db_table('carriers');
        $carrier['carriers_effective_start_date'] = date('Y-n-j', strtotime($carrier['carriers_effective_start_date']));
        $carrier['carriers_effective_end_date'] = date('Y-n-j', strtotime($carrier['carriers_effective_end_date']));

        $carriers_table->insert($carrier);
        $carriers_id = $carriers_table->last_id();

        return $carriers_id;
    }

    public static function delete($carrier_id)
    {
        if(!$carrier_id) return false;

        $carriers_table = new mysqli_db_table('carriers');

        $carriers_table->delete($carrier_id);

        $db = mysqli_db::init();

        //unlink claims
        $db->query('update claims set join_carriers_id = 0 where join_carriers_id = ?', array($carrier_id));

        return true;
    }

    public static function update($carrier, $data)
    {
        $carriers_table = new mysqli_db_table('carriers');

        $data['carriers_effective_start_date'] = date('Y-n-j', strtotime($data['carriers_effective_start_date']));
        $data['carriers_effective_end_date'] = date('Y-n-j', strtotime($data['carriers_effective_end_date']));

        $carriers_table->update($data, $carrier['carriers_id']);
        $carriers_id = $carrier['carriers_id'];

        return $carriers_id;
    }

    public static function get_carrier($carriers_id)
    {
        $db = mysqli_db::init();

        $carrier = $db->fetch_one('SELECT * FROM carriers WHERE carriers_id = ?', array($carriers_id));

        return $carrier;
    }
    public static function get_carrier_all_details($carriers_id){
        $carrier = carriers::get_carrier($carriers_id);
        if($carrier['carriers_recipient'] == 1){
            $location_ids =  carriers::get_all_email_recipient_locations_ids($carriers_id);
        }else{
            $location_ids =  carriers::get_all_carrier_location_ids($carriers_id);
        }
        $carrier['join_licensed_locations'] = $location_ids;
        return $carrier;
    }
    public static function get_all_email_recipient_locations_ids($carriers_id){
        $db = mysqli_db::init();
        $query = 'SELECT * FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ?';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
           $ids[$location['join_licensed_locations_id']]['always_email'] = $location['always_email'];
           $ids[$location['join_licensed_locations_id']]['always_email_on_save'] = $location['always_email_on_save'];
           $ids[$location['join_licensed_locations_id']]['always_email_on_submit'] = $location['always_email_on_submit'];
           $ids[$location['join_licensed_locations_id']]['location_can_edit'] = $location['location_can_edit'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }
    public static function get_all_carrier_location_ids($carriers_id){
        $db = mysqli_db::init();
        $query = 'SELECT * FROM licensed_locations_x_carriers WHERE join_carriers_id = ?';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[$location['join_licensed_locations_id']]['location_can_edit']= $location['location_can_edit'];
        }
        if($ids == null) $ids[] = '-1';
        return $ids;
    }
    
    public static function link_carrier($locations_id, $carriers_id, $can_edit)
    {
        carriers::unlink_carrier($locations_id, $carriers_id);
                
        $carriers_link_table = new mysqli_db_table('licensed_locations_x_carriers');
        
        $link = array('join_licensed_locations_id' => $locations_id,
                      'join_carriers_id' => $carriers_id,
                      'location_can_edit' => $can_edit);
        
        $carriers_link_table->insert($link);
         $carriers_link_id = $carriers_link_table->last_id();
         return $carriers_link_id;
    }
    
    public static function unlink_carrier($locations_id, $carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'DELETE FROM licensed_locations_x_carriers WHERE join_licensed_locations_id = ? AND join_carriers_id = ?';
        
        $db->query($query, array($locations_id, $carriers_id));
    }
    
     public static function link_email_recipient($locations_id, $carriers_id, $always_email, $can_edit,$always_email_on_save,$always_email_on_submit)
    {
        carriers::unlink_email_recipient($locations_id, $carriers_id);
         
        $recipient_link_table = new mysqli_db_table('licensed_locations_x_email_recipients');
        
        $link = array('join_licensed_locations_id' => $locations_id,
                      'join_carriers_id' => $carriers_id,
                      'always_email' => $always_email,
                      'location_can_edit' => $can_edit,
                      'always_email_on_save'=>$always_email_on_save,
                      'always_email_on_submit'=>$always_email_on_submit);
        
        $recipient_link_table->insert($link);
         $carriers_link_id = $recipient_link_table->last_id();
         return $carriers_link_id;
    }
    
    public static function unlink_email_recipient($locations_id, $carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'DELETE FROM licensed_locations_x_email_recipients WHERE join_licensed_locations_id = ? AND join_carriers_id = ?';
        
        $db->query($query, array($locations_id, $carriers_id));
    }
    
    public static function set_all_locations($carriers_id, $visble)
    {
        $db = mysqli_db::init();
        $query = 'UPDATE carriers SET carriers_all_locations = ? where carriers_id = ?;';
        
        $db->query($query, array($visble, $carriers_id));
    }
    
    public static function set_all_recieve($carriers_id, $visble)
    {
        $db = mysqli_db::init();
        $query = 'UPDATE carriers SET carriers_all_recieve = ? where carriers_id = ?;';

        $db->query($query, array($visble, $carriers_id));
    }

    public static function get_carrier_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_carriers WHERE join_carriers_id = ?';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }

        if($ids == null) $ids[] = '-1';

        return $ids;
    }

    public static function get_carrier_location_can_edit($carriers_id, $licensed_locations_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT location_can_edit FROM licensed_locations_x_carriers WHERE join_carriers_id = ? and join_licensed_locations_id = ?';
        $can_edit = $db->fetch_singlet($query, array($carriers_id, $licensed_locations_id));

        return $can_edit;
    }

    public static function get_recipient_location_can_edit($carriers_id, $licensed_locations_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT location_can_edit FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? and join_licensed_locations_id = ?';
        $can_edit = $db->fetch_singlet($query, array($carriers_id, $licensed_locations_id));

        return $can_edit;
    }

     public static function get_linked_email_recipient_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ?';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }

    public static function get_always_email_recipient_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? AND always_email = 1';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }
    public static function get_always_email_recipient_onsave_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? AND always_email_on_save = 1';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }
    public static function get_always_email_recipient_onsubmission_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? AND always_email_on_submit = 1';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }

    public static function get_edit_email_recipient_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? AND location_can_edit = 1';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }

    public static function get_edit_carrier_locations_ids($carriers_id)
    {
        $db = mysqli_db::init();
        $query = 'SELECT join_licensed_locations_id FROM licensed_locations_x_carriers WHERE join_carriers_id = ? AND location_can_edit = 1';
        $carrier_locations = $db->fetch_all($query, array($carriers_id));

        foreach($carrier_locations as $location)
        {
            $ids[] = $location['join_licensed_locations_id'];
        }
        if($ids == null) $ids[] = '-1';

        return $ids;
    }

    /**
     * @param $carriers_id
     * @param $location_id
     */
    public static function member_can_edit_carrier($carriers_id, $members_id) {
        if (ActiveMemberInfo::_IsAccountAdmin()) {
            return true;
        }

        $db = mysqli_db::init();

        // first get all the locations associated with this carrier...
        $query_loc_ids = 'select join_licensed_locations_id from licensed_locations_x_carriers
                                        where join_carriers_id = ?';
        $carriers_locations = $db->fetch_all($query_loc_ids,array($carriers_id));
        $carriers_locations_ids = array();
        foreach($carriers_locations as $carriers_locations) {
            $carriers_locations_ids[] = $carriers_locations['join_licensed_locations_id'];
        }

        // now get all the permissions for all the locations for this carrier
        $query = 'select join_members_access_levels_id from licensed_locations_x_members_access
                  where join_members_id = ?
                  and join_licensed_locations_id in ('.implode(",",$carriers_locations_ids).')';
        $join_member_access_level = $db->fetch_one($query, array($members_id));
        return count($join_member_access_level) > 0;
    }

    public static function can_user_edit_carrier_recipient($carriers_id)
    {
        $db = mysqli_db::init();
        // If an admin, just check that the carrier is in the right account
        if (ActiveMemberInfo::_IsAccountAdmin()) {
            $query = 'SELECT * FROM carriers WHERE carriers_id = ? AND join_accounts_id = ?';
            $results = $db->fetch_all($query, array($carriers_id, ActiveMemberInfo::GetAccountId()));

            if (count($results) > 0) {
                return true;
            }
            return false;
        }

        // Otherwise, find locations where the user can do claims stuff and check for a location can edit flag on carriers and recipients
        $location_in_clause = strings::CreateInClauseForIds(UserPermissions::LocationIdsWithPermission('claims'), 'join_licensed_locations');

        $query = 'SELECT * FROM licensed_locations_x_carriers WHERE join_carriers_id = ? AND location_can_edit = 1 AND ' . $location_in_clause;
        $results = $db->fetch_all($query, array($carriers_id));

        if (count($results) > 0) {
            return true;
        }

        $query = 'SELECT * FROM licensed_locations_x_email_recipients WHERE join_carriers_id = ? AND location_can_edit = 1 AND ' . $location_in_clause;
        $results = $db->fetch_all($query, array($carriers_id));

        if (count($results) > 0) {
            return true;
        }

        return false;
    }

    public static function add_carrier($accounts_id, $carriers_name,
                    $carriers_contact, $carriers_email, $carriers_phone, $carriers_fax,
                    $carriers_policy_number, $carriers_address, $carriers_coverages_list,
                    $carriers_coverages_other, $carriers_hidden, $carriers_recipient,
                    $carriers_effective_start_date, $carriers_effective_end_date,$all_location,
                    $receive_on_all_update,$receive_on_all_save,$receive_on_all_submit)
    {
        if($accounts_id == NULL) { return -1; }

        $carrier = array('join_accounts_id' => $accounts_id,
                        'carriers_name' => $carriers_name,
                        'carriers_contact' => $carriers_contact,
                        'carriers_email' => $carriers_email,
                        'carriers_phone' => $carriers_phone,
                        'carriers_fax' => $carriers_fax,
                        'carriers_policy_number' => $carriers_policy_number,
                        'carriers_address' => $carriers_address,
                        'carriers_coverages_list' => $carriers_coverages_list,
                        'carriers_coverages_other' => $carriers_coverages_other,
                        'carriers_hidden' => $carriers_hidden,
                        'carriers_recipient' => $carriers_recipient,
                        'carriers_effective_start_date' => $carriers_effective_start_date,
                        'carriers_effective_end_date' => $carriers_effective_end_date,
                        'carriers_all_locations' => $all_location,
                        'carriers_all_receive' => $receive_on_all_update,
                        'carriers_notify_on_save_all' => $receive_on_all_save,
                        'carriers_notify_on_submission_all' => $receive_on_all_submit);

        foreach($carrier as $key=>$value)
        {
            if(is_null($value))
                $carrier[$key] = "";
        }
        $carrier_id = carriers::create($carrier);

        return $carrier_id;
    }

     public static function build_query_hidden_filter($is_hidden = carriers::HIDDEN_VISIBLE)
     {
        if($is_hidden == carriers::HIDDEN_HIDDEN)
        {
            return ' AND carriers_hidden = 1';
        }
        else if ($is_hidden == carriers::HIDDEN_VISIBLE)
        {
            return ' AND carriers_hidden <> 1';
        }
        else
        {
            return '';
        }
     }

     public static function build_query_date_filter($carriers_date_filter_start_time = NULL, $carriers_date_filter_end_time = NULL)
     {
        $date_filter = '';

        if($carriers_date_filter_start_time && $carriers_date_filter_end_time)
        {
            $date_filter = ' AND (carriers_recipient = 1 OR (carriers_effective_end_date >= STR_TO_DATE(\''.
                $carriers_date_filter_start_time.'\', \'%m/%d/%Y\') AND carriers_effective_start_date <= STR_TO_DATE(\''
                . $carriers_date_filter_end_time.'\', \'%m/%d/%Y\')) OR carriers_effective_end_date = STR_TO_DATE(\''
                . '1/1/1972' .'\', \'%m/%d/%Y\') OR carriers_effective_start_date = STR_TO_DATE(\''
                . '1/1/1972' .'\', \'%m/%d/%Y\'))';
        }
        else
        {
            $date_filter = ' AND (carriers_recipient = 1 OR (carriers_effective_end_date >= STR_TO_DATE(\''.
                date('n/j/Y', strtotime(date('n/j/Y', time()) . ' -1 year')).'\', \'%m/%d/%Y\') AND carriers_effective_start_date <= STR_TO_DATE(\''
                . date('n/j/Y', time()).'\', \'%m/%d/%Y\')) OR carriers_effective_end_date = STR_TO_DATE(\''
                . '1/1/1972' .'\', \'%m/%d/%Y\') OR carriers_effective_start_date = STR_TO_DATE(\''
                . '1/1/1972' .'\', \'%m/%d/%Y\'))';
        }

        return $date_filter;
     }

     public static function build_query_paging_filter($orderBy, $paging = NULL)
     {
        $paging_query = '';
        if($orderBy) {
            $paging_query .= strings::orderby($orderBy, 'carriers');
        }
        if($paging)
        {
            $paging_query .= $paging->get_limit();
        }

        return $paging_query;
     }

    public static function build_query_common($query,
                                              $is_hidden = carriers::HIDDEN_VISIBLE,
                                              $recipients_and_carriers = true,
                                              $recipients_only = false,
            $do_datefilter = false, $carriers_date_filter_start_time = NULL,
            $carriers_date_filter_end_time = NULL, $orderBy = NULL, $paging= NULL)
    {
        if(!$recipients_and_carriers) {
            if($recipients_only == true)
            {
                $query = $query . ' AND carriers_recipient = 1';
            }
            else
            {
                $query = $query . ' AND carriers_recipient <> 1';
            }
        }

        $query = $query . carriers::build_query_hidden_filter($is_hidden);

       if($do_datefilter)
       {
           $query = $query . carriers::build_query_date_filter($carriers_date_filter_start_time, $carriers_date_filter_end_time);
       }

       $query = $query . " GROUP BY carriers.carriers_id ";

       $query = $query . carriers::build_query_paging_filter($orderBy, $paging);

        return $query;
    }

    private static function build_query($filter_location_id = null)
    {
        $query = 'SELECT * FROM carriers WHERE join_accounts_id = ' . ActiveMemberInfo::GetAccountId() . ' ';
        if (!ActiveMemberInfo::_IsAccountAdmin() || $filter_location_id) {
            $location_ids = UserPermissions::LocationIdsWithPermission('claims');

            if (is_array($filter_location_id)) {
                $location_ids = array_intersect($location_ids, $filter_location_id);
            } elseif ($filter_location_id != null) {
                $location_ids = array_intersect($location_ids, array($filter_location_id));
            }

            $locationClause = strings::CreateInClauseForIds($location_ids, 'join_licensed_locations');
            $query .=
                'AND (carriers_all_locations = 1 OR EXISTS (
                    SELECT 1 from licensed_locations_x_carriers
                    WHERE carriers_id = join_carriers_id
                    AND ' . $locationClause .
                ') OR EXISTS (
                    SELECT 1 from licensed_locations_x_email_recipients
                    WHERE carriers_id = join_carriers_id
                    AND ' . $locationClause .
                ')) ';
        }
        return $query;
    }

    public static function get_carriers_for_location($licensed_locations_id, $is_recipients_only) {
        $query = carriers::build_query($licensed_locations_id);
        $query = carriers::build_query_common($query, carriers::HIDDEN_VISIBLE, false, $is_recipients_only, true);

        $db = mysqli_db::init();
        return $db->fetch_all($query,array());
    }

    public static function get_always_email_recipients($licensed_locations_id)
    {
        $query = carriers::build_query($licensed_locations_id);
        $query .= ' AND EXISTS(
                select 1 from licensed_locations_x_email_recipients
                where carriers_id = join_carriers_id
                 and join_licensed_locations_id = ?
                 and always_email = 1
        )';
        $query = carriers::build_query_common($query, false, false, true);
        $db = mysqli_db::init();

        $carriers = $db->fetch_all($query, array($licensed_locations_id));

        return $carriers;
    }
    public static function get_recipients_tobe_notified($licensed_locations_id,$notify_type)
    {
        $query = carriers::build_query($licensed_locations_id);
        $query .= ' AND EXISTS(
                select 1 from licensed_locations_x_email_recipients
                where carriers_id = join_carriers_id
                 and join_licensed_locations_id = ?
                 and ('.$notify_type.')
        )';
        $query = carriers::build_query_common($query, false, false, true);
        $db = mysqli_db::init();
        $carriers = $db->fetch_all($query, array($licensed_locations_id));
        return $carriers;
    }
    
    public static function hide_carrier($carriers_id)
    {
        $carrier = carriers::get_carrier($carriers_id);
        $data = $carrier;

        $data['carriers_hidden'] = 1;

        carriers::update($carrier, $data);
    }

    public static function unhide_carrier($carriers_id)
    {
        $carrier = carriers::get_carrier($carriers_id);
        $data = $carrier;

        $data['carriers_hidden'] = 0;

        carriers::update($carrier, $data);
    }

    /**
     * @param $members_id the current member
     * Get the list of carriers for the member. To determine the carriers for a member
     * you need to get
     * @param $filter_location_id
     * @param $start_time
     * @param $end_time
     * @param int $show_hidden
     * @return array|mixed
     */
    public static function getCarriersForMember($members_id, $filter_location_id, $start_time, $end_time, $orderBy_request, $show_hidden = 0)
    {
        $orderBy = 'carriers_name';
        if ($orderBy_request && !empty($orderBy_request)) {
            $orderBy = $orderBy_request;
        }

        $query = carriers::build_query($filter_location_id);
        $query = carriers::build_query_common($query, $show_hidden == 0 ? carriers::HIDDEN_VISIBLE : carriers::HIDDEN_HIDDEN,
            false, false, true, $start_time, $end_time, $orderBy);

        $db = mysqli_db::init();
        return $db->fetch_all($query);
    }

    public static function getRecipientsForMember($members_id, $filter_location_id, $orderBy_request, $show_hidden = 0)
    {
        $orderBy = 'carriers_name';
        if ($orderBy_request && !empty($orderBy_request)) {
            $orderBy = $orderBy_request;
        }

        $query = carriers::build_query($filter_location_id);
        $query = carriers::build_query_common($query, $show_hidden == 0 ? carriers::HIDDEN_VISIBLE : carriers::HIDDEN_HIDDEN,
            false, true, false, null, null, $orderBy);

        $db = mysqli_db::init();
        return $db->fetch_all($query);
    }
     
}
