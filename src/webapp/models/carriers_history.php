<?
final class carriers_history
{
    public static function create_carriers_entry($members_id, $carriers_id, $carriers_history_description)
    {
        $carriers_history_table = new mysqli_db_table('carriers_history');

        $carriers_history = array('join_members_id' => $members_id,
            'join_carriers_id' => $carriers_id,
            'carriers_history_datetime' => date('Y-m-d H:i:s'),
            'carriers_history_description' => $carriers_history_description);

        $carriers_history_table->insert($carriers_history);
        $carriers_history_id = $carriers_history_table->last_id();

        return $carriers_history_id;
    }

    public static function new_carriers_entry($members_id, $carriers_id, $history_text)
    {
        return carriers_history::create_carriers_entry($members_id, $carriers_id, $history_text);
    }

    public static function check_field_update($members_id, $field_name, $field_text, $carrier, $old_carriers)
    { 
        if ($carrier[$field_name] != $old_carriers[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $old_carriers[$field_name] . '<b> To: </b>' . $carrier[$field_name];
            return carriers_history::create_carriers_entry($members_id, $carrier['carriers_id'], $history_text);
        }
    }

    public static function check_bit_field_update($members_id, $field_name, $field_text, $carrier, $old_carriers)
    {
        $values = array('False', 'True');
        if ($carrier[$field_name] != $old_carriers[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $values[$old_carriers[$field_name]] . '<b> To: </b>' . $values[$carrier[$field_name]];
            return carriers_history::create_carriers_entry($members_id, $carrier['carriers_id'], $history_text);
        }
    }

    public static function check_date_field_update($members_id, $field_name, $field_text, $carrier, $old_carriers)
    {
        if ($carrier[$field_name] != $old_carriers[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . carriers_history::format_date($old_carriers[$field_name]) . '<b> To: </b>' . carriers_history::format_date($carrier[$field_name]);
            return carriers_history::create_carriers_entry($members_id, $carrier['carriers_id'], $history_text);
        }
    }

    private static function format_date($date)
    {
        return empty($date) ? 'null' : date('n/j/Y', strtotime($date));
    }

    public static function carriers_updated_entry($members_id, $carrier, $old_carriers,$updateType)
    { 
        if ($carrier && $old_carriers) {
            carriers_history::check_field_update($members_id, 'carriers_name', $updateType, $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_contact', 'Contact Name', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_phone', 'Phone number', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_email', 'Email Address', $carrier, $old_carriers);
            carriers_history::check_date_field_update($members_id, 'carriers_effective_start_date', 'Effective Date', $carrier, $old_carriers);
            carriers_history::check_date_field_update($members_id, 'carriers_effective_end_date', 'Expiration Date', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_fax', 'Fax number to submit claim', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_policy_number', 'Policy Number', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_address', 'Address', $carrier, $old_carriers);
            carriers_history::check_field_update($members_id, 'carriers_coverages_list', 'Coverage', $carrier, $old_carriers);
            carriers_history::check_bit_field_update($members_id, 'carriers_no_email', 'Carrier doesnt have Email', $carrier, $old_carriers);

            $history_text = '';
            $licensed_locations = licensed_locations::get_for_account(ActiveMemberInfo::GetAccountId());
            $licensed_locations_names = array();
            foreach ($licensed_locations as $licensed_location) {
                $licensed_locations_names[$licensed_location['licensed_locations_id']] = $licensed_location['licensed_locations_name'];
            }
            if ($carrier['carriers_all_locations']) {
                $carrier['licensed_locations'] = 'All Locations';
            } else {
                $join_licensed_locations = array();
                foreach ($carrier['join_licensed_locations'] as $join_licensed_locations_id=>$value) {
                    $join_licensed_locations[$join_licensed_locations_id] = $join_licensed_locations_id;
                }
            }
            $new_locations = $carrier['join_licensed_locations'];
            $old_locations = $old_carriers['join_licensed_locations'];

            if ($old_carriers['carriers_all_locations']) {
                $old_carriers['licensed_locations'] = 'All Locations';
            } else {
                $old_join_licensed_locations = array();
                foreach ($old_carriers['join_licensed_locations'] as $join_licensed_locations_id=>$value) {
                    $old_join_licensed_locations[$join_licensed_locations_id] = $join_licensed_locations_id;
                }
            }
            if($carrier['licensed_locations'] == 'All Locations' && $old_carriers['licensed_locations'] != 'All Locations' ){
                $history_text .= 'All Locations turned on';
            }elseif($carrier['licensed_locations'] != 'All Locations' && $old_carriers['licensed_locations'] == 'All Locations' ){
                $history_text .= 'All Locations turned off';
            }
            $removed_locations = array_diff($old_join_licensed_locations,$join_licensed_locations);
            $added_locations = array_diff($join_licensed_locations,$old_join_licensed_locations);
            foreach ($removed_locations as $key => $value){
                $history_text .= '<br>Location <b>'.$licensed_locations_names[$removed_locations[$value]]."</b> Removed";
            }
            foreach ($added_locations as $key => $value){
                $receive_claim = '';
                $edit_permission = '';
                
                $history_text .= '<br>Location <b>'.$licensed_locations_names[$added_locations[$key]]."</b> Added:";
                if($carrier['carriers_recipient'] == 1){
                    $receive_claim = $carrier['join_licensed_locations'][$added_locations[$key]]['always_email'];
                    $history_text .= $receive_claim ?' recipient will ':' recipient will not ';
                    $history_text .= ' be copied on all claims, ';
                    $receive_claim = $carrier['join_licensed_locations'][$added_locations[$key]]['always_email_on_save'];
                    $history_text .= $receive_claim ?' recipient will ':' recipient will not ';
                    $history_text .= ' be copied on all initial save of claims, ';
                    $receive_claim = $carrier['join_licensed_locations'][$added_locations[$key]]['always_email_on_submit'];
                    $history_text .= $receive_claim ?' recipient will ':' recipient will not ';
                    $history_text .= ' be copied on all submission of claims, ';
                }
                $edit_permission = $carrier['join_licensed_locations'][$added_locations[$key]]['location_can_edit'];
                $history_text .= ' non-admins ';
                $history_text .=($edit_permission)?' can ':' can not ';
                $history_text .=' edit';
                unset($new_locations[$key]);
            }
            foreach($new_locations as $key => $value){
                $email_flag = 0;
                if($value['always_email'] != $old_locations[$key]['always_email'] ){
                    $history_text .= '<br>Location <b>'. $licensed_locations_names[$key]."</b> changed: recipient";
                    if($value['always_email']) {
                        $history_text .= " will be copied on all claims";
                    }else{
                        $history_text .= " will not be copied on all claims";
                    }
                    $email_flag = 1;
                }
                if($value['always_email_on_save'] != $old_locations[$key]['always_email_on_save'] ){
                    if(!$email_flag){
                        $history_text .= '<br>Location <b>'. $licensed_locations_names[$key]."</b> changed: recipient";
                    }
                    if($value['always_email_on_save']) {
                        $history_text .= " will be copied on all initial save";
                    }else{
                        $history_text .= " will not be copied on all initial save";
                    }
                    $email_flag = 1;
                }
                if($value['always_email_on_submit'] != $old_locations[$key]['always_email_on_submit'] ){
                    if(!$email_flag){
                        $history_text .= '<br>Location <b>'. $licensed_locations_names[$key]."</b> changed: recipient";
                    }
                    if($value['always_email_on_submit']) {
                        $history_text .= " will be copied on all submission";
                    }else{
                        $history_text .= " will not be copied on all submission";
                    }
                    $email_flag = 1;
                }
        
                if($value['location_can_edit'] != $old_locations[$key]['location_can_edit'] ){
                    if(!$email_flag){
                        $history_text .= '<br> Location <b>'. $licensed_locations_names[$key]."</b> changed: non-admins";
                    }else{
                        $history_text .=', non-admins';
                    }
                    if($value['location_can_edit']) {
                        $history_text .= " can edit";
                    }else{
                        $history_text .= " can not edit";
                    }
                }
            }
            if($history_text) {
                carriers_history::create_carriers_entry($members_id, $carrier['carriers_id'], $history_text);
            }
        }
    }


    public static function hidden_changed($members_id, $carriers_id, $new_value)
    {
        $history_text = "<b>Carrier " . ( $new_value ? "Hidden" : "Unhidden" ) . "</b>";

        return carriers_history::create_carriers_entry($members_id, $carriers_id, $history_text);
    }

    public static function get_carriers_history_with_data($members_id, $carriers_id)
    {
        $db = mysqli_db::init();

        $carriers_history = $db->fetch_all('SELECT * FROM carriers_history WHERE join_carriers_id = ? order by carriers_history_datetime', array($carriers_id));

        return $carriers_history;
    }
}

?>