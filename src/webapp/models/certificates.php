<?
use Services\Services;

class certificates {

    public static function get_certificate($certificates_id)
    {
        if(!UserPermissions::UserCanAccessObject('certificates', $certificates_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_one('
            SELECT c.*, v.*
            FROM certificates as c
            LEFT JOIN vendors AS v ON v.vendors_id = c.join_vendors_id
            WHERE
            certificates_id = ?',
            array($certificates_id));
    }

    public static function get_files($certificates_id)
    {
        if(!UserPermissions::UserCanAccessObject('certificates', $certificates_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_all('
            SELECT *
            FROM certificates_files
            inner join files on files_id = join_files_id
            WHERE
            join_certificates_id = ?',
            array($certificates_id));
    }

    public static function get_file($certificates_files_id)
    {
        $db = mysqli_db::init();
        $certFile = $db->fetch_one('
            SELECT *
            FROM certificates_files
            inner join files on files_id = join_files_id
            WHERE
            certificates_files_id = ?',
            array($certificates_files_id));

        if(!UserPermissions::UserCanAccessObject('certificates', $certFile['join_certificates_id'], array('checkView' => true))) {
            return null;
        }

        return $certFile;
    }

    private static function build_query($wheres, $orderby, $paging, $do_paging)
    {
        $query = '
        SELECT SQL_CALC_FOUND_ROWS c.*, ll.licensed_locations_name, v.*, cf.*, f.*
        FROM certificates AS c
        INNER JOIN licensed_locations AS ll on c.join_licensed_locations_id = ll.licensed_locations_id
        LEFT JOIN certificates_files cf on c.certificates_id = cf.join_certificates_id AND cf.certificates_files_active = 1
        LEFT JOIN files f on cf.join_files_id = f.files_id
        LEFT JOIN vendors AS v ON c.join_vendors_id = v.vendors_id
        '.strings::where($wheres).
          strings::orderby($orderby, array('certificates','licensed_locations','vendors'));

        if($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
    }

    public static function get_paged_certificates_list(&$paging, $certificates_start_time=null, $certificates_end_time=null, $orderby_request=null,
                                                    $show_hidden=null, $licensed_locations_id=null)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $orderby = 'certificates_expire';
        $params = array();

        if($certificates_start_time)
        {
            $wheres[] = 'certificates_expire >= STR_TO_DATE(?, \'%m/%d/%Y\')';
            $params[] = $certificates_start_time;
        }

        if($certificates_end_time)
        {
            $wheres[] = 'certificates_expire <= STR_TO_DATE(?, \'%m/%d/%Y\')';
            $params[] = $certificates_end_time;
        }

        if($orderby_request) {
            $orderby = $orderby_request;
        } 

        if($show_hidden == '0')
        {
            $_SESSION['certificates_show_hidden'] = 0;
            $wheres[] = '(certificates_hidden <> ?)';
            $params[] = '1';
        }
        else if($show_hidden == 1 || $_SESSION['certificates_show_hidden'] == 1)
        {
            $_SESSION['certificates_show_hidden'] = 1;
            $wheres[] = '(certificates_hidden = ?)';
            $params[] = '1';
        }
        else
        {
            $wheres[] = '(certificates_hidden <> ?)';
            $params[] = '1';
        }

        if(ActiveMemberInfo::IsUserMultiLocation() && $licensed_locations_id)
        {
            if($licensed_locations_id)
                {
                    $filter_location_ids = array();
                    foreach($licensed_locations_id as $id) {
                        if($id != 'all')
                            $filter_location_ids[] = $id;
                    }
                }
             $wheres[] =   strings::CreateInClauseForIds($filter_location_ids, 'join_licensed_locations');
        }
        $wheres[] = UserPermissions::CreateInClause('certificates', array('checkView' => true));

        //get count
        $db->fetch_all(certificates::build_query($wheres, $orderby, $paging, false), $params);
        $paging->set_total($db->found_rows());

        //get page data
        $certificates = $db->fetch_all(certificates::build_query($wheres, $orderby, $paging, true), $params);

        return $certificates;
    }

    public static function delete($certificates_id) {
        $db = mysqli_db::init();
        $status = array();
        $certificate = certificates::get_certificate($certificates_id);
        $perms = UserPermissions::UserObjectPermissions('certificates', $certificates_id);

        if ($certificate && $perms['delete']) {

            $files = certificates::get_files($certificates_id);
            foreach($files as $file) {
                certificates::deleteFile($file['certificates_files_id']);
            }

            $certificates_table = new mysqli_db_table('certificates');
            $certificates_table->delete($certificates_id);

            //delete download stats
            $db->query('DELETE FROM downloads WHERE downloads_type = "certificate" AND join_id = ?', array($certificates_id));

            members::log_action(ActiveMemberInfo::GetMemberId(), 'Certificate Deleted', $certificates_id);

            $status['notice'] = 'The certificate has been deleted';
        } else {
            $status['notice'] = 'You do not have permissions to delete this certificate.';
        }

        return $status;
    }

    public static function deleteFile($certificates_files_id) {
        $status = array();
        $db = mysqli_db::init();

        $file = certificates::get_file($certificates_files_id);
        $perms = UserPermissions::UserObjectPermissions('certificates', $file['join_certificates_id']);
        if($file && $perms['delete']) {

            $deleteSuccess = Services::$fileSystem->deleteFile($file['files_id']);
            if ($deleteSuccess) {
                $query = $db->query('DELETE FROM certificates_files WHERE certificates_files_id = ?',
                    array($file['certificates_files_id']));
                $deleteSuccess = is_array($query->result);
            }

            if ($deleteSuccess) {
                members::log_action(ActiveMemberInfo::GetMemberId(), 'Certificate File Deleted: ' . $file['files_name'], $file['join_certificates_id']);

                $status['notice'] = 'The certificate file has been deleted';
            } else {
                $status['notice'] = 'There was a problem deleting the certificate file';
            }
        } else {
            $status['notice'] = 'You do not have permissions to delete this certificate file.';
        }

        return $status;
    }

    public static function setActiveFile($certificates_files_id) {
        $status = array();

        $file = certificates::get_file($certificates_files_id);
        $perms = UserPermissions::UserObjectPermissions('certificates', $file['join_certificates_id']);
        if($file && $perms['edit']) {
            $db = mysqli_db::init();

            //Deactivate all files for the cert
            $db->query("
              UPDATE certificates_files SET certificates_files_active = 0
              WHERE join_certificates_id = ?",
                array($file['join_certificates_id']));

            //Activate requested file
            $db->query("
              UPDATE certificates_files SET certificates_files_active = 1
              WHERE certificates_files_id = ?",
                array($certificates_files_id));

            members::log_action(ActiveMemberInfo::GetMemberId(),
                'Certificate File made active: ' . $file['files_name'], $file['join_certificates_id']);

            $status['notice'] = $file['files_name'] . ' has been set as the active certificate file.';
        } else {
            $status['notice'] = 'You do not have permissions to edit this certificate.';
        }

        return $status;
    }

    public static function get_expired_remind_members($certificates_id)
    {
        if(!UserPermissions::UserCanAccessObject('certificates', $certificates_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_singlets('
            SELECT join_members_id
            FROM certificate_remind_members
            WHERE
            join_certificates_id = ? AND certificates_remind_before_expiry = ?',
            array($certificates_id,1));
    }

    public static function get_expiring_remind_members($certificates_id)
    {
        if(!UserPermissions::UserCanAccessObject('certificates', $certificates_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_singlets('
            SELECT join_members_id
            FROM certificate_remind_members
            WHERE
            join_certificates_id = ? AND certificates_remind_after_expiry = ?',
            array($certificates_id,1));
    }

    static function delete_remind_members_by_certificates_id($certificates_id)
    {
        $db = mysqli_db::init();

        $db->query('DELETE FROM certificate_remind_members WHERE '
                . 'join_certificates_id = ?', array($certificates_id));
    }
    
    static function getCertificateDetails($certificates_id){
         $db = mysqli_db::init();
         $cert = $db->fetch_one('SELECT *
                            FROM certificates AS c
                            LEFT JOIN vendors AS v ON c.join_vendors_id = v.vendors_id
                            left join licensed_locations ll on licensed_locations_id = join_licensed_locations_id
                            WHERE certificates_id = ?',array($certificates_id));
         return $cert;
    }

    static function remove_certificates_email_templates($certificates_id){
        $db = mysqli_db::init();
        $db->query('UPDATE certificates set join_certificates_email_templates_id = "" WHERE '
                . 'certificates_id = ?', array($certificates_id));
    }

    static function get_certificates_x_legal_entity_names($certificates_id){
        $db = mysqli_db::init();
        $join_legal_entity_names_ids = $db->fetch_singlets('SELECT join_legal_entity_names_id
                            FROM certificates_x_legal_entity_names 
                            WHERE join_certificates_id = ?',array($certificates_id));
         return $join_legal_entity_names_ids;
    }

    static function delete_certificates_x_legal_entity_by_certificates_id($certificates_id){
        $db = mysqli_db::init();
        $db->query('DELETE FROM certificates_x_legal_entity_names 
                            WHERE join_certificates_id = ?',array($certificates_id));
    }
    static function delete_coverags_by_certificates_id($certificates_id)
    {
        $db = mysqli_db::init();
        $db->query('DELETE FROM certificate_coverages WHERE '
                . 'join_certificates_id = ?', array($certificates_id));
    }
    static function get_coverags_by_certificates_id($certificates_id){
        $db = mysqli_db::init();
        $certificate_coverages = $db->fetch_all('SELECT * FROM certificate_coverages WHERE '
                . 'join_certificates_id = ?', array($certificates_id));
         return $certificate_coverages;
    }
}
?>