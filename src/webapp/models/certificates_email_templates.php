<?php

class certificates_email_templates
{
    /**
     * Get a certificate email template to generate a email
     * 
     * @param $id - account ID to fetch email template for
     * @return template for corporation ID
     */
    static function get_for_corporation($id)
    {
        return certificates_email_templates::get_for_account($id);
    }
    
    /**
     * Get a certificate email template to generate a email
     * 
     * @param $account_id - account ID to fetch email template for
     * @return template for account ID
     */
    static function get_for_account($account_id)
    {
        return certificates_email_templates::get_for_location($account_id, 0);
    }
    
    /**
     * Get a certificate email template to generate a email for an account
     * 
     * @param $account_id - account ID to fetch email template for
     * @param $location_id - location ID to fetch email template for
     * @return template for account ID
     */
    static function get_for_location($account_id, $location_id )
    {
        $db = mysqli_db::init();
        $template = $db->fetch_one('SELECT * FROM certificates_email_templates as cet
                               LEFT JOIN licensed_locations as ll ON ll.licensed_locations_id = cet.join_licensed_locations_id
                               WHERE cet.join_accounts_id = ? AND cet.join_licensed_locations_id = ?
                               ORDER BY licensed_locations_name ', array($account_id, $location_id));

        if(!$template) return null;
        
        $template['email_html'] = stripslashes($template['email_html']);
        
        return $template;
    }
    
    /**
     * Get the best fallback template for a given location and account ID
     * This is the most likely point of access for fetching a template without the
     * individual templates ID.
     * 
     * Fallback works like this:  If the location has a template send that over,
     * if not get the corporate template.
     * 
     * @param $account_id to get template for
     * @param $location_id to get template for
     * @return template item 
     */
    static function get_best_fallback_for_location($account_id, $location_id)
    {
        if(true) //only try this if we have permission to use it.
        {
            $template = certificates_email_templates::get_for_location($account_id, $location_id);

            if($template) return $template;
        }
        
        $template = certificates_email_templates::get_for_account($account_id);
        
        if($template) return $template;
        
        return null;
    }
    
    static function add_or_update_for_corporation($account_id,  $email_code)
    {
        return certificates_email_templates::add_or_update_for_account($account_id,  $email_code);
    }
    
     /**
     * Add an account template
     * 
     * @param type $account_id - account ID for corporate account to add.
     * @param type $email_code - html code of template to add
     */   
    static function add_or_update_for_account($account_id, $email_code)
    {
        return certificates_email_templates::add_or_update_for_location($account_id, 0, $email_code);
    }
    
     /**
     * Adds or updates an existing entry for a specific location given an account ID
     * 
     * @param type $account_id - account fo add for
     * @param type $location_id - location to add
     * @param type $email_code - html code of template to add
     */   
    static function add_or_update_for_location($account_id, $location_id, $email_code,$certificates_email_templates_name,$certificates_email_templates_id)
    {
        $db = mysqli_db::init();
        
        $html = addslashes($email_code);
        $html = trim($html);
        $html = preg_replace( "/\r|\n/", "", $html );
        
        if($certificates_email_templates_id == 0 || !$certificates_email_templates_id){
             $db->query('INSERT INTO certificates_email_templates(join_accounts_id, join_licensed_locations_id, email_html,certificates_email_templates_name) '
                 . 'VALUES(?, ?, ?,?)', array($account_id, $location_id, $html,$certificates_email_templates_name));
             
        }else{
             $db->query('UPDATE certificates_email_templates set email_html = ?,certificates_email_templates_name = ?'
                    . ' WHERE certificates_email_templates_id = ?',
                    array($html,$certificates_email_templates_name, $certificates_email_templates_id));
        }
        
    }
    
   /**
     * delete a master template for a given account
     * 
     * @param type $account_id to delete template for
     */    
    static function delete_for_corporation($account_id)
    {
        return certificates_email_templates::delete_for_account($account_id);
    }
    
    /**
     * delete a master template for a given account
     * 
     * @param type $account_id to delete template for
     */    
    static function delete_for_account($account_id)
    {
        return certificates_email_templates::delete_for_location($account_id, 0);
    }
    
    /**
     * Delete an entry for a given location's template under an account
     * @param type $account_id - account ID to delete template for
     * @param type $location_id - location ID to delete template for
     */
    static function delete_for_location($account_id, $location_id)
    {
        $db = mysqli_db::init();
       
        $db->query(
            'DELETE FROM certificates_email_templates '
          . 'WHERE join_accounts_id = ? AND join_licensed_locations_id = ?', array($account_id, $location_id));

    }
  
    /**
     * 
     * @return all template entries
     */
    static function get_all()
    {
        $db = mysqli_db::init();

        return $db->fetch_all('SELECT * FROM certificates_email_templates');
    }
    
    /**
     * 
     * @param type $id row ID to fetch data for
     * @return entry for given row ID
     */    
    static function get_by_id($id)
    {
        $db = mysqli_db::init();
        
        $template = $db->fetch_one('SELECT * FROM certificates_email_templates WHERE '
                . 'certificates_email_templates_id = ?', array($id));
        
        if(!$template) return null;
        
        $template['email_html'] = stripslashes($template['email_html']);
        
        return $template;
    }
    
    /**
     * Delete template at given ID
     * 
     * @param type $id - ID to delete
     */
    static function delete_by_id($id)
    {
        $db = mysqli_db::init();
        
        
        $db->query('DELETE FROM certificates_email_templates WHERE '
                . 'certificates_email_templates_id = ?', array($id));
    }

    static function check_email_templates_exists($account_id,$email_template_name,$certificates_email_templates_id){
        $db = mysqli_db::init();
        
        
        $result = $db->fetch_one('select * FROM certificates_email_templates WHERE '
                . 'join_accounts_id = ? and certificates_email_templates_name = ? and certificates_email_templates_id != ?', array($account_id,$email_template_name,$certificates_email_templates_id));
        return $result;
    }

    static function get_email_template_name($certificates_email_templates_id){
        $db = mysqli_db::init();
        
        $result = $db->fetch_singlet('select certificates_email_templates_name FROM certificates_email_templates WHERE '
                . 'certificates_email_templates_id = ?', array($certificates_email_templates_id));
        return $result;
    }

    static function getAllTemplatesForAccount(){
        $db = mysqli_db::init();
        $locationIds = UserPermissions::LocationIdsWithPermission('certificates');
        $locationIds[] = 0;
        $inClause = strings::CreateInClauseForIds($locationIds, 'cet.join_licensed_locations');

        $template = $db->fetch_all('select * from certificates_email_templates cet '
                . 'LEFT JOIN licensed_locations  ll ON (ll.licensed_locations_id = cet.join_licensed_locations_id)'
                . ' where cet.join_accounts_id = ? and ' 
                . $inClause, array(ActiveMemberInfo::GetAccountId()));
        if(!$template) return null;
       for($i=0;$i<count($template);$i++){
           $template[$i]['email_html'] = stripslashes($template[$i]['email_html']);
       }
        
        return $template;
    }

    static function get_template($certificates_email_templates_id){
        $db = mysqli_db::init();
        $template = $db->fetch_one('select * FROM certificates_email_templates WHERE '
                . 'certificates_email_templates_id = ?', array($certificates_email_templates_id));
        return $template;
    }

     static function getTemplateForLocation($locationId){
        $db = mysqli_db::init();
        
        $template = $db->fetch_all(
            'select certificates_email_templates_id, certificates_email_templates_name, join_certificates_id from certificates_email_templates cet
                where cet.join_accounts_id = ?
                and cet.join_licensed_locations_id IN(?,?)
                and cet.join_certificates_id is null
                ORDER BY certificates_email_templates_name'
                , array(ActiveMemberInfo::GetAccountId(), $locationId, 0));

        return $template;
    }

    static function get_template_for_certificate($certificates_id){
        $db = mysqli_db::init();
        $template = $db->fetch_one('select * FROM certificates_email_templates WHERE '
                . 'join_certificates_id = ?', array($certificates_id));
        return $template;
    }

    static function add_or_update_email_template($licensed_locations_id,$template_html,$certificates_id,$certificates_email_templates_id){
        $db = mysqli_db::init();
        if($certificates_email_templates_id){
            $db->query('UPDATE certificates_email_templates set email_html = ?,certificates_email_templates_name = ?'
                    . ' WHERE certificates_email_templates_id = ?',
                    array($html,$certificates_email_templates_name, $certificates_email_templates_id));
        }
    }

    static function update_default_template($certificates_email_templates_id){
        $db = mysqli_db::init();
        $account_id = ActiveMemberInfo::GetAccountId();
        $db->query('UPDATE certificates_email_templates set certificates_email_templates_default = ? WHERE join_accounts_id = ?',
                array(0,$account_id));
        $db->query('UPDATE certificates_email_templates set certificates_email_templates_default = ? WHERE certificates_email_templates_id = ?',
                array(1,$certificates_email_templates_id));
        
    }

    static function getDefaultTemplate(){
        $db = mysqli_db::init();
        $account_id = ActiveMemberInfo::GetAccountId();
        $default_template = $db->fetch_one('SELECT * FROM certificates_email_templates WHERE certificates_email_templates_default = ? AND join_accounts_id =? ',array(1,$account_id));
        return $default_template;
    }
    private static function build_query($wheres, $orderby, $paging, $do_paging)
    {
        $query = 'select SQL_CALC_FOUND_ROWS cet.* , cet.*,ll.* from certificates_email_templates cet '
                . 'LEFT JOIN licensed_locations  ll ON (ll.licensed_locations_id = cet.join_licensed_locations_id)'
                .strings::where($wheres).
                strings::orderby($orderby, array('certificates_email_templates','licensed_locations'));

        if($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
        
    }

    public static function get_paged_certificates_templates_list(&$paging, $orderby_request,
                                                     $licensed_locations_id)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $orderby = 'certificates_email_templates_id';//licensed_locations_name
        $params = array();
        if($orderby_request) {
            $orderby = $orderby_request;
        } 

        if(ActiveMemberInfo::IsUserMultiLocation())
        {
            $wheres[] = '(cet.join_accounts_id = ?)';
            $params[] = ActiveMemberInfo::GetAccountId();
        }
        $locationIds = UserPermissions::LocationIdsWithPermission('certificates');
        $locationIds[] = 0;
        $inClause = strings::CreateInClauseForIds($locationIds, 'cet.join_licensed_locations');
        $wheres[] = $inClause;
        //get count
        $db->fetch_all(certificates_email_templates::build_query($wheres, $orderby, $paging, false), $params);
        $paging->set_total($db->found_rows());
        //get page data
        $templates = $db->fetch_all(certificates_email_templates::build_query($wheres, $orderby, $paging, true), $params);
        if(!$templates) return null;
        for($i=0;$i<count($templates);$i++){
            $templates[$i]['email_html'] = stripslashes($templates[$i]['email_html']);
        }
        return $templates;
    }
}