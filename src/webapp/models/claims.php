<?php

class claims {
    static $coverages = array(
        'Auto',
        'Crime',
        'General Liability',
        'Package (Property/GL/Crime/Auto)',
        'Property',
        'Workers Compensation',
        'Broker',
        'Umbrella',
        'EPLi',
        'D&O',
        'E&O' 
    );
    
    static $departments = array (
        'Administrative',
        'Bar',
        'Catering',
        'Fitness/Recreation',
        'Front Desk',
        'Guest Relations',
        'Housekeeping',
        'Kitchen',
        'Laundry',
        'Maintenance/Engineering',
        'Restaurant',
        'Retail/Shop',
        'Sales',
        'Security',
        'Spa',
        'Valet/Garage',
    );

    static $illness_types = array(
        'Skin Disorder',
        'Respiratory Condition',
        'Poisoning',
        'Hearing Loss',
        'Stress'
    );

    static $injury_types = array(
        'Was struck by',
        'Came in contact with',
        'Was trapped in or by',
        'Was caught between',
        'Slip/Trip/Fall',
        'Strain/Overexertion',
        'Stress'
    );
    
    static $body_parts = array(
        'Head',
        'Forehead',
        'Face',
        'Eye',
        'Nose',
        'Ear',
        'Lip',
        'Teeth',
        'Chin',
        'Neck',
        'Shoulder',
        'Cheek',
        'Chest',
        'Arm',
        'Elbow',
        'Wrist',
        'Hand',
        'Finger(s)',
        'Lung(s)',
        'Back',
        'Hip',
        'Pelvis',
        'Stomach',
        'Buttocks',
        'Groin',
        'Leg',
        'Knee',
        'Foot',
        'Ankle',
        'Toe(s)'
    );

    static $case_classifications = array (
        'G' => 'G - Death',
        'H' => 'H - Days away from work',
        'I' => 'I - Job transfer or restriction (remained at work)',
        'J' => 'J - Other reportable cases (remained at work)'
    );
    
    static $loss_areas = array(
        'Room',
        'Hallway',
        'Kitchen',
        'Dining Room',
        'Parking Lot',
        'Garage',
        'Pool Area',
        'Elevator',
        'Escalator',
        'Lobby',
        'Stairs',
        'Conference Room',
        'Business Center',
        'Laundry',
        'Fitness Center',
        'Recreation Area (other than Fitness Center)',
        'Bar/Lounge',
        'Off Premises',
        'Valet',
        'Front Desk',
        'Bathroom',
        'Shower',
        'Golf Course'
    );
    
    static $loss_types_liab = array(
        'Guest/Third Party Bodily Injury',
        'Guest/Third Party Property Missing',
        'Guest/Third Party Property Damaged'
    );
    
    static $natures = array(
        'Accident/Owned Auto/Third Party Auto',
        'Accident/Owned Auto/Third Party Property',
        'Accident/Owned Auto/Pedestrian',
        'Accident/Non-Owned Auto',
        'Valet'
    );
    
    static $loss_types_property = array(
        'Building',
        'Personal Property (Contents)',
        'Loss Of Income'
    );

    static $PENDING = 'PENDING';
    static $SUBMIT_CLOSE = 'SUBMIT_CLOSE';
    static $SUBMIT_OPEN = 'SUBMIT_OPEN';
    static $RECORD_ONLY = 'RECORD_ONLY';
    static $FIRST_AID = 'FIRST_AID';
    static $FAILED = 'FAILED';

    public static function getStatusCodeDescription($statusCodeId) {
        $db = mysqli_db::init();
        return $db->fetch_singlet('SELECT claims_status_codes_description FROM claims_status_codes WHERE claims_status_codes_id = ?',
            array($statusCodeId));
    }

    public static function setStatusCodeAndDescription($claim, $statusCodeId) {
        $claim['join_claims_status_codes_id'] = $statusCodeId;
        $claim['claims_status_codes_description'] = Claims::getStatusCodeDescription($statusCodeId);
    }

    const ClAIMS_DATE_FILTER = '(claims_datetime >= STR_TO_DATE(?, \'%m/%d/%Y\') && claims_datetime <= STR_TO_DATE(?, \'%m/%d/%Y\'))';

    public static function get_claim($claims_id)
    {
        $db = mysqli_db::init();

        $claim_data = $db->fetch_one('
        SELECT c.*, ca.*, cgl.*, cp.*, cwc.*, ll.*, csc.*,ce.*
        FROM claims as c
        JOIN licensed_locations as ll on ll.licensed_locations_id = c.join_licensed_locations_id
        left join claim_auto as ca on ca.join_claims_id = claims_id
        left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
        left join claim_property as cp on cp.join_claims_id = claims_id
        left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
        left join claim_employment as ce on ce.join_claims_id = claims_id
        left join claims_status_codes as csc on c.join_claims_status_codes_id = csc.claims_status_codes_id
        WHERE
        claims_id = ?',
            array($claims_id));

        return $claim_data;
    }

    /**
     * @param $wheres
     * @param $orderBy
     * @param paging $paging
     * @param $do_paging
     * @return string
     */
    private static function build_query($wheres, $orderBy, $paging, $do_paging)
    {
        $query = '
        SELECT SQL_CALC_FOUND_ROWS *
        FROM claims
        JOIN claims_status_codes csc on claims.join_claims_status_codes_id = csc.claims_status_codes_id
        JOIN licensed_locations as ll on ll.licensed_locations_id = claims.join_licensed_locations_id
        left join claim_auto as ca on ca.join_claims_id = claims_id
        left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
        left join claim_property as cp on cp.join_claims_id = claims_id
        left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
        left join claim_employment as ce on ce.join_claims_id = claims_id
        LEFT JOIN carriers on carriers_id = join_carriers_id
        left join claims_faxes cf on claims.claims_id = cf.join_claims_id and cf.claims_faxes_status < 0';
        $query = $query.strings::where($wheres).
            strings::orderby($orderBy, 'claims');

        if($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
    }

    static $claim_types = array(
            'property' => 'Your Property',
            'workers' => 'Workers Comp',
            'general' => 'General Liability / Property of Others',
            'labor' => 'Labor Law',
            'auto' => 'Auto',
            'other' => 'Other'
    );


    /**
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_claim_percentages($claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id)
    {
        $wheres = array();
        $params = array();
        
        claims::set_claims_filters(null, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = 'SELECT claims.claims_type as claims_type, count(claims.claims_id) as claimCount
                  from claims
                  left join licensed_locations as ll on claims.join_licensed_locations_id = ll.licensed_locations_id ';
        $query = $query.strings::where($wheres).
                ' group by claims.claims_type';

        $db = mysqli_db::init();
        $claims = $db->fetch_all($query, $params);
        return $claims;
    }

    /**
     * @param $members_id
     * @param $type
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return mixed
     */
    public static function get_claim_counts_by_location_and_type($members_id, $type, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id) {

        $wheres = array();
        $params = array();
        $params[] = self::$claim_types[$type];
        $params[] = $claims_start_time;
        $params[] = $claims_end_time;

        $claims_hidden_values = array();
        $claims_hidden_values[] = '0';
        if ( $includeHidden ) {
            $claims_hidden_values[] = '1';
        }
        if(!empty($join_claims_status_codes_id)){
            $wheres[] = "claims.join_claims_status_codes_id = '$join_claims_status_codes_id'";
        }

        if (isset($licensed_locations_id) && !empty($licensed_locations_id)) {
            $wheres[] = "(ll.licensed_locations_id = " . $licensed_locations_id . ")";
        } else {
            $ids = licensed_locations::get_licensed_locations_ids_for_member($members_id);
            $wheres[] = "(ll.licensed_locations_id in (" . implode(', ', $ids) . "))";
        }

        $query = 'SELECT ll.licensed_locations_name, count(claims.claims_id) as claimCount
            from licensed_locations as ll
            left join claims on claims.join_licensed_locations_id = ll.licensed_locations_id
              and claims.claims_type = ? and (claims.claims_hidden in ('.implode(', ', $claims_hidden_values).')) 
              and claims_datetime >= STR_TO_DATE(?, \'%m/%d/%Y\') and claims_datetime <= STR_TO_DATE(?, \'%m/%d/%Y\')';
        $query = $query.strings::where($wheres).' group by ll.licensed_locations_name';
        
        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);

    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_comp_claim_counts_by_event_type($licensed_locations_id, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id) {

        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = '
        select claim_workers_comp_event_type,
               if(claim_workers_comp_event_type = \'Injury\', claim_workers_comp_injury_type,
               claim_workers_comp_illness_type) as SubType, count(*) as claimCount
               from claims join claim_workers_comp cwc on claims.claims_id = cwc.join_claims_id
               join licensed_locations on licensed_locations.licensed_locations_id = claims.join_licensed_locations_id ';
        $query = $query.strings::where($wheres).
            ' group by claim_workers_comp_event_type,
              if(claim_workers_comp_event_type = \'Injury\', claim_workers_comp_injury_type, claim_workers_comp_illness_type)';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);

    }


    ///
    /// Slight tricky based on the data. For worker's comp claims you can denote multiple body parts
    /// for example, head, foot, ankle. We want the data broken down by body part so we need to get the data
    /// parse the body part field and then do the aggregation.
    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array
     * @throws Exception
     */
    public static function get_comp_claim_body_part($licensed_locations_id, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id)
    {
        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = '
        select claim_workers_comp_body_parts
               from claims join claim_workers_comp cwc on claims.claims_id = cwc.join_claims_id
               join licensed_locations on licensed_locations.licensed_locations_id = claims.join_licensed_locations_id ';
        $query = $query.strings::where($wheres);

        $db = mysqli_db::init();
        $claims = $db->fetch_all($query, $params);

        $results = Array();
        foreach($claims as $claim) {
            $parts = explode(",",$claim['claim_workers_comp_body_parts']);
            foreach($parts as $part) {
                if (empty($part)) continue;

                if (!array_key_exists($part, $results)) {
                    $results[$part] = 1;
                } else {
                    $results[$part] = $results[$part] + 1;
                }
            }
        }
        return $results;
    }


    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_comp_claim_counts_by_dept($licensed_locations_id, $claims_start_time,
                                                         $claims_end_time, $includeHidden,$join_claims_status_codes_id)
    {
        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = 'select claim_workers_comp_employee_department, count(*) as claimCount
               from claims join claim_workers_comp cwc on claims.claims_id = cwc.join_claims_id
               join licensed_locations on licensed_locations.licensed_locations_id = claims.join_licensed_locations_id ';
        $query = $query.strings::where($wheres).
                ' group by claim_workers_comp_employee_department';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);
    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_auto_by_nature($licensed_locations_id, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id) {

        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = '
          select claim_auto_nature, count(*) as claimCount
               from claims join claim_auto ca on claims.claims_id = ca.join_claims_id
               join licensed_locations on licensed_locations.licensed_locations_id = claims.join_licensed_locations_id ';
        $query = $query
                .strings::where($wheres).
                 ' group by claim_auto_nature';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);
    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_property_by_loss($licensed_locations_id, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id)
    {
        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = 'select claim_property_loss_type, count(*) as claimCount
               from claims join claim_property cp on claims.claims_id = cp.join_claims_id
                join licensed_locations on claims.join_licensed_locations_id = licensed_locations.licensed_locations_id ';
        $query = $query.strings::where($wheres).
                ' group by claim_property_loss_type';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);
    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_gen_claim_by_type_of_loss($licensed_locations_id, $type, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id) {
        $wheres = array();
        $params = array();

        $wheres[] = 'claims_type = ?';
        $params[] = self::$claim_types[$type];

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);
        $query = '
          select claim_general_liab_loss_type, count(*) as claimCount
               from claims join claim_general_liab cgl on claims.claims_id = cgl.join_claims_id
               join licensed_locations on licensed_locations.licensed_locations_id = claims.join_licensed_locations_id ';
         $query = $query.strings::where($wheres).
               ' group by claim_general_liab_loss_type';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);
    }


    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_gen_claim_by_area_of_loss($licensed_locations_id, $type, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id) {

        $wheres = array();
        $params = array();

        $wheres[] = 'claims_type = ?';
        $params[] = self::$claim_types[$type];
        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = '
        select claim_general_liab_loss_area
               from claims join claim_general_liab on claims.claims_id = claim_general_liab.join_claims_id ';
        $query = $query.strings::where($wheres);

        $db = mysqli_db::init();
        $things = $db->fetch_singlets($query, $params);
        return $things;
    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeVisible
     * @param $includeHidden
     * @param $wheres
     * @param $params
     * @throws Exception
     */
    public static function set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, $includeVisible, $includeHidden,$join_claims_status_codes_id, &$wheres, &$params)
    {
        if (!$includeVisible && !$includeHidden) {
            throw new Exception("must be viewing visible or hidden (or both)");
        }
        $claims_hidden_values = array();
        if ( $includeHidden ) {
            $claims_hidden_values[] = '1';
        }
        if ( $includeVisible ) {
            $claims_hidden_values[] = '0';
        }

        $wheres[] = '(claims.claims_hidden in ('.implode(', ', $claims_hidden_values).'))';

        $wheres[] = 'claims_datetime >= STR_TO_DATE(?, \'%m/%d/%Y\') and claims_datetime <= STR_TO_DATE(?, \'%m/%d/%Y\')';
        $params[] = $claims_start_time;
        $params[] = $claims_end_time;
        $wheres[] = UserPermissions::CreateInClause("claims");

        if(!UserPermissions::UserCanAccessScreen('workers_comp')) {
            $wheres[] = "claims.claims_type != 'Workers Comp'";
        }
        if(!UserPermissions::UserCanAccessScreen('labor_claims')) {
            $wheres[] = "claims.claims_type != 'Labor Law'";
        }
        if(!UserPermissions::UserCanAccessScreen('other_claims')) {
            $wheres[] = "claims.claims_type != 'Other'";
        }

        if (isset($licensed_locations_id) && !empty($licensed_locations_id)) {
            if(!is_array($licensed_locations_id) ){
                 $wheres[] = "(claims.join_licensed_locations_id = $licensed_locations_id)";
            }else{
                $locations = array();
                foreach($licensed_locations_id as $locations_id){
                    if($locations_id != 'all'){
                        $locations[] = $locations_id;
                    }
                }
                $wheres[] = "(claims.join_licensed_locations_id IN (" . implode(",",$locations) . "))";
            }
        }
        if(isset($join_claims_status_codes_id) && !empty($join_claims_status_codes_id)){
            $wheres[] = "(claims.join_claims_status_codes_id = ?)" ;
            $params[] =  $join_claims_status_codes_id ;
        }
    }

    /**
     * @param $licensed_locations_id
     * @param $claims_start_time
     * @param $claims_end_time
     * @param $includeHidden
     * @return array|mixed
     * @throws Exception
     */
    public static function get_comp_claim_counts_by_event_dept($licensed_locations_id, $claims_start_time, $claims_end_time, $includeHidden,$join_claims_status_codes_id)
    {
        $wheres = array();
        $params = array();

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, true, $includeHidden, $join_claims_status_codes_id, $wheres, $params);

        $query = '
          select IF(claim_workers_comp_event_department = \'\',\'Unknown\',claim_workers_comp_event_department)
          as claim_workers_comp_event_department, count(*) as claimCount
          from claims join claim_workers_comp on claims.claims_id = claim_workers_comp.join_claims_id ';
         $query = $query.strings::where($wheres).
            ' group by claim_workers_comp_event_department';

        $db = mysqli_db::init();
        return $db->fetch_all($query, $params);

    }

    /**
     * @param paging $paging
     * @param $claims_start_time
     * @param $claims_end_time
     * @param string $orderBy_request
     * @param $show_hidden
     * @param $show_visible
     * @param $members_id
     * @param $licensed_locations_id
     * @return array quick note of hidden/visible claims. There are three options:
     *
     * quick note of hidden/visible claims. There are three options:
     *
     * quick note of hidden/visible claims. There are three options:
     * return only visible claims: $show_hidden = false, $show_visible = true
     * return only hidden claims: $show_hidden = true, $show_visible = false
     * return claims, both hidden and visible: $show_hidden = true, $show_visible = true
     * @throws Exception
     */
    public static function get_paged_claims_list(&$paging, $claims_start_time, $claims_end_time, $orderBy_request,
                                                 $show_hidden, $show_visible, $members_id, $licensed_locations_id,$join_claims_status_codes_id='')
    {
        if (!isset($claims_start_time) ) {
            throw new InvalidArgumentException("claim start time parameter invalid");
        }

        if (!isset($claims_end_time) ) {
            throw new InvalidArgumentException("claim start time parameter invalid");
        }

        if (!isset($members_id) ) {
            throw new InvalidArgumentException("member id parameter invalid");
        }

        if (!is_bool($show_hidden) ) {
            throw new InvalidArgumentException("show hidden parameter invalid");
        }

        if (!is_bool($show_visible) ) {
            throw new InvalidArgumentException("show hidden parameter invalid");
        }

        if( !$show_hidden && !$show_visible ) {
            throw new InvalidArgumentException("need to show hidden or visible or both");
        }

        $db = mysqli_db::init();

        $wheres = array();
        $params = array();
        $orderBy = 'claims_datetime';
        if($orderBy_request && !empty($orderBy_request)) {
            $orderBy = $orderBy_request;
        }

        claims::set_claims_filters($licensed_locations_id, $claims_start_time, $claims_end_time, $show_visible, $show_hidden, $join_claims_status_codes_id, $wheres, $params);


        $query = claims::build_query($wheres, $orderBy, $paging, false);
        $db->fetch_all($query, $params);
        $paging->set_total($db->found_rows());

        //get page data
        $claims = $db->fetch_all(claims::build_query($wheres, $orderBy, $paging, true), $params);

        return $claims;
    }
    static function remove_claims_email_templates($claims_id){
        $db = mysqli_db::init();
        $db->query('UPDATE claims set join_claims_email_templates_id = "" WHERE '
                . 'claims_id = ?', array($claims_id));
    }
    static function get_claim_employment_involved_parties($claims_id){
        $db = mysqli_db::init();
        $claim_employment_involved_parties = $db->fetch_all('SELECT * FROM  claim_employment_involved_parties WHERE '
                . 'join_claims_id = ?', array($claims_id));
        return $claim_employment_involved_parties;
        
    }
    static function delete_claim_employment_involved_parties_by_claims_id($claims_id){
        $db = mysqli_db::init();
        $db->query('DELETE from claim_employment_involved_parties WHERE '
                . 'join_claims_id = ?', array($claims_id));
    }
    static function get_claim_x_carriers($claims_id){
        $db = mysqli_db::init();
        $carriers = $db->fetch_all('SELECT * from claims_x_carriers'
                . ' join carriers on (join_carriers_id = carriers_id) WHERE '
                . 'join_claims_id = ?', array($claims_id));
        return $carriers;
    }
    static function delete_claim_x_carriers_by_id($deleted_carriers){
        $db = mysqli_db::init();
        $db->query('DELETE from claims_x_carriers WHERE '
                . 'claims_x_carriers_id IN('.implode(",",$deleted_carriers).')');
        
    }
    static function get_claims_x_carriers_dates($claims_id){
        $db = mysqli_db::init();
        $claims_x_carriers = $db->fetch_singlets('SELECT min(claims_x_carriers_datetime_submitted),max(claims_x_carriers_datetime_closed) from claims_x_carriers'
                . ' WHERE join_claims_id = ?', array($claims_id));
        return $claims_x_carriers;
    }
}