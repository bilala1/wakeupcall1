<?
final class claims_history
{
    public static function create_claim_entry($members_id, $claims_id, $claims_history_name)
    {
        $claims_history_table = new mysqli_db_table('claims_history');

        $claims_history = array( 'join_members_id' => $members_id,
                                'join_claims_id' => $claims_id,
                                'claims_history_name' => $claims_history_name);

        $claims_history_table->insert($claims_history);
        $claims_history_id = $claims_history_table->last_id();

        return $claims_history_id;
    }

    public static function new_claim_entry($members_id, $claims_id)
    {
        return claims_history::create_claim_entry($members_id, $claims_id, 'Claim created.');
    }

    public static function claim_location_updated_entry($members_id, $claim, $old_claims)
    {
        if($old_claims['licensed_locations_name'] == $claim['licensed_locations_name']) return;

        $history_text = '<b>Location Changed From: </b>' . $old_claims['licensed_locations_name'] . '<b> To: </b>' . $claim['licensed_locations_name'];

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    private static function format_date($date) {
        return empty($date) ? 'null' : date('n/j/Y', strtotime($date));
    }

    public static function claim_incident_date_updated_entry($members_id, $claim, $old_claims)
    {
        if($claim['claims_datetime'] == $old_claims['claims_datetime']) return;

        if(claims_history::format_date($claim['claims_datetime']) == 'null') return;

        if(claims_history::format_date($old_claims['claims_datetime']) == 'null')
        {
            $history_text = '<b>Incident Date Changed To: </b>' . claims_history::format_date($claim['claims_datetime']);
        }
        else
        {
            $history_text = '<b>Incident Date Changed From: </b>' .  claims_history::format_date($old_claims['claims_datetime'])
                . '<b> To: </b>' .  claims_history::format_date($claim['claims_datetime']);
        }
        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_submitted_date_updated_entry($members_id, $claim)
    {
        if(claims_history::format_date($claim['claims_datetime_submitted']) == 'null') return;

        $history_text = '<b>Submitted Date: </b>' . claims_history::format_date($claim['claims_datetime_submitted']);

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_reserved_updated_entry($members_id, $claim, $old_claims)
    {
        if($old_claims['claims_amount_reserved'] == $claim['claims_amount_reserved']) return;

        $history_text = '';
        if(empty($old_claims['claims_amount_reserved']))
        {
            $history_text = '<b>Reserved Amount Changed To:</b> ' . money_format('%n', $claim['claims_amount_reserved']);
        }
        else
        {
            $history_text = '<b>Reserved Amount Changed From: </b>' . money_format('%n', $old_claims['claims_amount_reserved'])
                . ' <b>To:</b> ' . money_format('%n', $claim['claims_amount_reserved']);
        }


        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_paid_updated_entry($members_id, $claim, $old_claims)
    {
        if($old_claims['claims_amount_paid'] == $claim['claims_amount_paid']) return;
        $history_text = '';
        if(empty($old_claims['claims_amount_paid']))
        {
            $history_text = '<b>Amount Paid Changed To:</b> ' . money_format('%n', $claim['claims_amount_paid']);
        }
        else
        {
            $history_text = '<b>Amount Paid Changed From:</b> ' . money_format('%n', $old_claims['claims_amount_paid'])
                . ' <b>To:</b> ' . money_format('%n', $claim['claims_amount_paid']);
        }


        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_status_updated_entry($members_id, $claim, $old_claims)
    {
        if( $old_claims['join_claims_status_codes_id'] == $claim['join_claims_status_codes_id']) return;

        $history_text = '';
        if(empty($old_claims['join_claims_status_codes_id']))
        {
            $history_text = '<b>Status Changed To:</b> ' . $claim['claims_status_code_description'];
        }
        else
        {
            $history_text = '<b>Status Changed From:</b> ' . $old_claims['claims_status_code_description'] . ' <b>To:</b> ' . $claim['claims_status_code_description'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_comp_update($members_id, $claim, $old_claim)
    {
        if($claim['claim_workers_comp_employee_name'] != $old_claim['claim_workers_comp_employee_name'])
        {
            claims_history::comp_name_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_workers_comp_department'] != $old_claim['claim_workers_comp_department'])
        {
            claims_history::comp_department_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_workers_comp_injury_type'] != $old_claim['claim_workers_comp_injury_type'])
        {
            claims_history::comp_injury_type_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_workers_comp_body_parts'] != $old_claim['claim_workers_comp_body_parts'])
        {
            claims_history::comp_body_part_update($members_id, $claim, $old_claim);
        }
    }

    public static function comp_name_update($members_id, $claim, $old_claims)
    {
        if($claim['claim_workers_comp_employee_name'] == $old_claims['claim_workers_comp_employee_name']) return;

        $history_text = '';
        if(empty($old_claims['claim_workers_comp_employee_name']))
        {
            $history_text = '<b>Claimant Name Changed To:</b> ' . $claim['claim_workers_comp_employee_name'];
        }
        else
        {
            $history_text = '<b>Claimant Name Changed From:</b> ' . $old_claims['claim_workers_comp_employee_name'] . ' <b>To:</b> ' . $claim['claim_workers_comp_employee_name'];

        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function comp_department_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_workers_comp_department'] == $claim['claim_workers_comp_department']) return;
        $history_text = '';
        if(empty($old_claims['claim_workers_comp_department']))
        {
            $history_text = '<b>Department Changed To:</b> ' . $claim['claim_workers_comp_department'];
        }
        else
        {
            $history_text = '<b>Department Changed From:</b> ' . $old_claims['claim_workers_comp_department'] . ' <b>To:</b> ' . $claim['claim_workers_comp_department'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function comp_injury_type_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_workers_comp_injury_type'] == $claim['claim_workers_comp_injury_type']) return;
        $history_text = '';
        if(empty($old_claims['claim_workers_comp_injury_type']))
        {
            $history_text = '<b>Injury Type Changed To:</b> ' . $claim['claim_workers_comp_injury_type'];
        }
        else
        {
            $history_text = '<b>Injury Type Changed From: </b>' . $old_claims['claim_workers_comp_injury_type'] . ' <b>To:</b> ' . $claim['claim_workers_comp_injury_type'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function comp_body_part_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_workers_comp_body_parts'] == $claim['claim_workers_comp_body_parts']) return;
        $history_text = '';
        if(empty($old_claims['claim_workers_comp_body_parts']))
        {
            $history_text = '<b>Body Parts Changed To:</b> (' . $claim['claim_workers_comp_body_parts'] . ')';
        }
        else
        {
            $history_text = '<b>Body Parts Changed From:</b> (' . $old_claims['claim_workers_comp_body_parts'] . ') <b>To:</b> (' . $claim['claim_workers_comp_body_parts'] . ')';
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function general_liab_claimant_update($members_id, $claim, $old_claims)
    {
        if( $old_claims['claim_general_liab_claimant'] == $claim['claim_general_liab_claimant']) return;
        $history_text = '';
        if(empty($old_claims['claim_general_liab_claimant']))
        {
            $history_text = '<b>Claimant Changed To: </b>' . $claim['claim_general_liab_claimant'];
        }
        else
        {
            $history_text = '<b>Claimant Changed From: </b>' . $old_claims['claim_general_liab_claimant'] . '<b> To: </b>' . $claim['claim_general_liab_claimant'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function general_liab_loss_area_update($members_id, $claim, $old_claims)
    {
        if( $old_claims['claim_general_liab_loss_area'] == $claim['claim_general_liab_loss_area']) return;
        $history_text = '';
        if(empty($old_claims['claim_general_liab_loss_area']))
        {
            $history_text = '<b>Loss Area Changed To: </b>' . $claim['claim_general_liab_loss_area'];
        }
        else
        {
            $history_text = '<b>Loss Area Changed From: </b>' . $old_claims['claim_general_liab_loss_area'] . '<b> To: </b>' . $claim['claim_general_liab_loss_area'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function general_liab_loss_type_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_general_liab_loss_type'] == $claim['claim_general_liab_loss_type']) return;
        $history_text = '';
        if(empty($old_claims['claim_general_liab_loss_type']))
        {
            $history_text = '<b>Loss Type Changed To: </b>' . $claim['claim_general_liab_loss_type'];
        }
        else
        {
            $history_text = '<b>Loss Type Changed From: </b>' . $old_claims['claim_general_liab_loss_type'] . '<b> To: </b>' . $claim['claim_general_liab_loss_type'];
        }


        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_liability_update($members_id, $claim, $old_claim)
    {
        if($claim['claim_general_liab_claimant'] != $old_claim['claim_general_liab_claimant'])
        {
            claims_history::general_liab_claimant_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_general_liab_loss_area'] != $old_claim['claim_general_liab_loss_area'])
        {
            claims_history::general_liab_loss_area_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_general_liab_loss_type'] != $old_claim['claim_general_liab_loss_type'])
        {
            claims_history::general_liab_loss_type_update($members_id, $claim, $old_claim);
        }
    }

    public static function auto_claimant_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_auto_claimant'] == $claim['claim_auto_claimant']) return;

        $history_text = '';
        if(emtpy($old_claims['claim_auto_claimant']))
        {
            $history_text = '<b>Claimant Changed To: </b>' . $claim['claim_auto_claimant'];
        }
        else
        {
            $history_text = '<b>Claimant Changed From: </b>' . $old_claims['claim_auto_claimant'] . '<b> To: </b>' . $claim['claim_auto_claimant'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function auto_nature_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_auto_nature'] == $claim['claim_auto_nature']) return;

        $history_text = '';
        if(empty($old_claims['claim_auto_nature']))
        {
            $history_text = '<b>Nature Changed To: </b>' . $claim['claim_auto_nature'];
        }
        else
        {
            $history_text = '<b>Nature Changed From: </b>' . $old_claims['claim_auto_nature'] . '<b> To: </b>' . $claim['claim_auto_nature'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_auto_update($members_id, $claim, $old_claim)
    {
        if($claim['claim_auto_claimant'] != $old_claim['claim_auto_claimant'])
        {
            claims_history::auto_claimant_update($members_id, $claim, $old_claim);
        }

        if($claim['claim_auto_nature'] != $old_claim['claim_auto_nature'])
        {
            claims_history::auto_nature_update($members_id, $claim, $old_claim);
        }
    }

    public static function property_loss_type_update($members_id, $claim, $old_claims)
    {
        if($old_claims['claim_property_loss_type'] == $claim['claim_property_loss_type']) return;
        $history_text = '';
        if(empty($old_claims['claim_property_loss_type']))
        {
            $history_text = '<b>Loss Type  To: </b>' . $claim['claim_property_loss_type'];

        }
        else
        {
            $history_text = '<b>Loss Type From: </b>' . $old_claims['claim_property_loss_type'] . '<b> To: </b>' . $claim['claim_property_loss_type'];
        }

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function claim_property_update($members_id, $claim, $old_claim)
    {
        if($claim['claim_property_loss_type'] != $old_claim['claim_property_loss_type'])
        {
            claims_history::property_loss_type_update($members_id, $claim, $old_claim);
        }
    }
    public static function claim_labor_update($members_id, $claim, $old_claim)
    {
        if($claim['claim_employment_claimant'] != $old_claim['claim_employment_claimant'])
        {
            claims_history::labor_field_update($members_id, $claim, $old_claim,'Claimant Name','claim_employment_claimant');
        }

        if($claim['claim_employment_department'] != $old_claim['claim_employment_department'])
        {
            claims_history::labor_field_update($members_id, $claim, $old_claim,'Department','claim_employment_department');
        }

        if($claim['claim_employment_job_title'] != $old_claim['claim_employment_job_title'])
        {
            claims_history::labor_field_update($members_id, $claim, $old_claim,'Job Title','claim_employment_job_title');
        }

        if($claim['claim_employment_incident_location'] != $old_claim['claim_employment_incident_location'])
        {
            claims_history::labor_field_update($members_id, $claim, $old_claim,'Incident Location','claim_employment_incident_location');
        }
        if(count($claim['employment_involved_parties'])>0){
            foreach($claim['employment_involved_parties'] as $involved_parties){
                $new_employment_involved_parties[]= $involved_parties['claim_employment_involved_parties_name'];
            }
        }
        if(count($old_claim['employment_involved_parties'])>0){
            foreach($old_claim['employment_involved_parties'] as $involved_parties){
                $old_employment_involved_parties[] = $involved_parties['claim_employment_involved_parties_name'];
            }
        }
        $new_employment_involved_parties = implode(', ', $new_employment_involved_parties);
        $old_employment_involved_parties = implode(", ", $old_employment_involved_parties);
        if($old_employment_involved_parties != $new_employment_involved_parties){
            if($old_employment_involved_parties == '') $old_employment_involved_parties = 'None';
            if($new_employment_involved_parties == '') $new_employment_involved_parties = 'None';
            $history_text = "Other parties changed from <b>$old_employment_involved_parties</b> to <b>$new_employment_involved_parties </b> ";
            return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
        }
    }
    public static function labor_field_update($members_id, $claim, $old_claims,$field_name,$field)
    {
        if($claim[$field] == $old_claims[$field]) return;

        $history_text = '';
        if(empty($old_claims[$field]))
        {
            $history_text = "<b>$field_name Changed To:</b> " . $claim[$field];
        }
        else
        {
            $history_text = "<b>$field_name Changed From:</b> " . $old_claims[$field] . ' <b>To:</b> ' . $claim[$field];

        }
        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }
   
    public static function claim_updated_entry($members_id, $claim, $old_claim)
    {
        if($claim && $old_claim)
        {
            if($claim['licensed_locations_id'] != $old_claim['licensed_locations_id'])
            {
                claims_history::claim_location_updated_entry($members_id, $claim, $old_claim);
            }
            if($claim['claims_datetime'] != $old_claim['claims_datetime'])
            {
                claims_history::claim_incident_date_updated_entry($members_id, $claim, $old_claim);
            }
            if($claim['claims_amount_reserved'] != $old_claim['claims_amount_reserved'])
            {
                claims_history::claim_reserved_updated_entry($members_id, $claim, $old_claim);
            }
            if($claim['claims_amount_paid'] != $old_claim['claims_amount_paid'])
            {
                claims_history::claim_paid_updated_entry($members_id, $claim, $old_claim);
            }
            if(stristr($claim['claims_type'], 'Comp'))
            {
                claims_history::claim_comp_update($members_id, $claim, $old_claim);
            }
            else if(stristr($claim['claims_type'], 'Liability'))
            {
                claims_history::claim_liability_update($members_id, $claim, $old_claim);
            }
            else if(stristr($claim['claims_type'], 'Auto'))
            {
                claims_history::claim_auto_update($members_id, $claim, $old_claim);
            }
            else if(stristr($claim['claims_type'], 'Property'))
            {
                claims_history::claim_property_update($members_id, $claim, $old_claim);
            }
            else if(stristr($claim['claims_type'], 'Labor'))
            {
                claims_history::claim_labor_update($members_id, $claim, $old_claim);
            }

            claims_history::claim_status_updated_entry($members_id, $claim, $old_claim);
        }
    }

    public static function claim_submitted_entry($members_id, $claim, $carrier, $recipient, $files)
    {
        if($carrier)
            $history_text = "<b>Claim Submitted To: </b>" . $carrier['carriers_name'] . "<br>";

        if($recipient)
            $history_text = "<b>Claim Copied To: </b>" . $recipient['carriers_name'] . "<br>";

        $file_submitted_text = "<br>" . "<b>Submitted Files: </b>";

        $writeFile = false;
        if(!empty($files))
        {
            foreach($files as $file)
            {
                if(is_array($file) && $file['files_name'])
                {
                    $file_submitted_text = $file_submitted_text . $file['files_name'] . " , ";
                    $writeFile = true;
                }
            }

            $file_submitted_text = substr($file_submitted_text, 0, -3);
        }

        if($writeFile)
            $history_text = $history_text . $file_submitted_text;

        return claims_history::create_claim_entry($members_id, $claim['claims_id'], $history_text);
    }

    public static function file_added_entry($members_id, $claims_id,  $file_name)
    {
        $history_text = "<b>File Added: </b>" . $file_name;

        return claims_history::create_claim_entry($members_id, $claims_id, $history_text);
    }

    public static function file_deleted_entry($members_id, $claims_id, $file_name)
    {
        $history_text = "<b>File Deleted: </b>" . $file_name;

        return claims_history::create_claim_entry($members_id, $claims_id, $history_text);
    }

    public static function note_added_entry($members_id, $claims_id, $claims_note)
    {
        $history_text = "<b>Note Added:</b> ". $claims_note['claims_notes_subject'];

        return claims_history::create_claim_entry($members_id, $claims_id, $history_text);
    }

    public static function note_updated_entry($members_id, $claims_id, $claims_note)
    {
        $history_text = "<b>Note Updated:</b> ". $claims_note['claims_notes_subject'];

        return claims_history::create_claim_entry($members_id, $claims_id, $history_text);
    }

    public static function note_deleted_entry($members_id, $claims_id, $claims_note_name)
    {
        $history_text = "<b>Note Deleted: </b>". $claims_note_name;

        return claims_history::create_claim_entry($members_id, $claims_id, $history_text);
    }

    public static function get_claims_history_with_data($members_id, $claims_id)
    {
        $db = mysqli_db::init();

        $claims_history = $db->fetch_all('SELECT * FROM claims_history WHERE join_claims_id = ?', array($claims_id));

        return $claims_history;
    }
}
?>