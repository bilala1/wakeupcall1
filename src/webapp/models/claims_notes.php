<?
final class claims_notes
{

    public static function create($claims_note)
    {
        $claims_notes_table = new mysqli_db_table('claims_notes');
        $claims_notes_table->insert($claims_note);
        $claims_notes_id = $claims_notes_table->last_id();
        
        $claims_id = $claims_note['join_claims_id'];
        $db = mysqli_db::init();
        $licensed_location_id = $db->fetch_singlet("SELECT join_licensed_locations_id FROM claims where claims_id = ?",array($claims_id));
        $notify_recipients_on_update = carriers::get_recipients_tobe_notified($licensed_location_id,'always_email');
        $status = 'added';
        foreach ($notify_recipients_on_update as $recipient) {
            ob_start();
            include VIEWS . '/emails/notes_updated.php';
            $message = ob_get_clean();

            mail::send(
                $from = AUTO_EMAIL,
                $to = $recipient['carriers_email'],
                $subject = 'A Claims note has been added in ' . SITE_NAME,
                $message = $message
            );
            emails_history::add(ActiveMemberInfo::GetAccountId(), $licensed_location_id, $subject,
                $message, $claims_id, null, null, $to, 'email', 'pending');
        }
        claims_history::note_added_entry(ActiveMemberInfo::GetMemberId(), $claims_note['claims_id'], $claims_note);

        return $claims_notes_id;
    }

    public static function delete($claims_notes_id)
    {
        if(!$claims_notes_id) return false;

        $claims_note = claims_notes::get_claims_note_with_data($claims_notes_id);

        $claims_notes_table = new mysqli_db_table('claims_notes');

        $claims_notes_table->delete($claims_notes_id);

        claims_history::note_deleted_entry(ActiveMemberInfo::GetMemberId(), $claims_note['claims_id'], $claims_note['claims_notes_subject']);

        $db = mysqli_db::init();

        return true;
    }

    public static function update($claims_note, $data)
    {
        $claims_notes_table = new mysqli_db_table('claims_notes');
        $db = mysqli_db::init();
        $claims_notes_id = $claims_note['claims_notes_id'];
        $old_notes = $db->fetch_one("SELECT * from claims_notes where claims_notes_id = ?",array($claims_notes_id));

        $claims_notes_table->update($data, $claims_note['claims_notes_id']);
        //send mail and update history only if there is any change
        if( ($old_notes['claims_notes_subject'] != $data['claims_notes_subject']) || ($old_notes['claims_notes_note'] != $data['claims_notes_note'])){
            $claims_id = $old_notes['join_claims_id'];
            $licensed_location_id = $db->fetch_singlet("SELECT join_licensed_locations_id FROM claims where claims_id = ?",array($claims_id));
            $notify_recipients_on_update = carriers::get_recipients_tobe_notified($licensed_location_id,'always_email');
            $status = 'updated';
            foreach ($notify_recipients_on_update as $recipient) {
                ob_start();
                include VIEWS . '/emails/notes_updated.php';
                $message = ob_get_clean();

                mail::send(
                    $from = AUTO_EMAIL,
                    $to = $recipient['carriers_email'],
                    $subject = 'A Claims note has been updated in ' . SITE_NAME,
                    $message = $message
                );
                emails_history::add(ActiveMemberInfo::GetAccountId(), $licensed_location_id, $subject,
                    $message, $claims_id, null, null, $to, 'email', 'pending');
            }
            claims_history::note_updated_entry(ActiveMemberInfo::GetMemberId(), $claims_note['join_claims_id'], $claims_note);
        }
        return $claims_notes_id;
    }

    public static function get_claims_notes_with_data($claims_id)
    {
        $db = mysqli_db::init();

        $claims_notes = $db->fetch_all('SELECT * FROM claims_notes WHERE join_claims_id = ?', array($claims_id));

        return $claims_notes;
    }

    public static function get_claims_note_with_data($claims_note_id)
    {
        $db = mysqli_db::init();

        $claims_note = $db->fetch_one('SELECT * FROM claims_notes WHERE claims_notes_id = ?', array($claims_note_id));

        return $claims_note;
    }
}
?>