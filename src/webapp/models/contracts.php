<?

class contracts {

    public static function get_contract($contracts_id)
    {
        if (!UserPermissions::UserCanAccessObject('contracts', $contracts_id)) {
            return null;
        }

        $db = mysqli_db::init();
        $contract_data = $db->fetch_one('
            SELECT v.*,
            (
                SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
                FROM contracts_x_licensed_locations vll
                WHERE vll.join_contracts_id = v.contracts_id
                ) as `join_licensed_locations`
            FROM contracts as v
            WHERE
            contracts_id = ?',
            array($contracts_id));

        if($contract_data['join_licensed_locations']) {
            $contract_data['join_licensed_locations'] = explode(',', $contract_data['join_licensed_locations']);
        }

        return $contract_data;
    }

    private static function build_query($wheres, $orderby, $paging, $do_paging)
    {
        $query = '
        SELECT SQL_CALC_FOUND_ROWS *,
        (
        IF(contracts_all_locations, \'All Locations\', (SELECT GROUP_CONCAT(licensed_locations_name SEPARATOR \', \')
            FROM contracts_x_licensed_locations cll
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE cll.join_contracts_id = c.contracts_id
            ))) as `licensed_locations_name`,
        (SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
            FROM contracts_x_licensed_locations cll
            WHERE cll.join_contracts_id = c.contracts_id
            ) as `licensed_locations_ids`,
        (SELECT COUNT(1)
            FROM contracts_x_licensed_locations cll
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE cll.join_contracts_id = c.contracts_id
            ) as `licensed_locations_count`
        FROM contracts AS c
        LEFT JOIN contracts_files cf on c.contracts_id = cf.join_contracts_id AND cf.contracts_files_active = 1
        LEFT JOIN files f on cf.join_files_id = f.files_id
        '.strings::where($wheres).
            strings::orderby($orderby, 'contracts');

        if($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
    }

    public static function get_paged_contracts_list(&$paging, $contracts_start_time=null, $contracts_end_time=null, $orderby_request=null,
                                                 $show_hidden=null, $licensed_locations_id=null)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $orderby = 'contracts_effective_date';
        $params = array();

        if($contracts_start_time)
        {
            $wheres[] = '(contracts_expiration_date >= STR_TO_DATE(?, \'%m/%d/%Y\'))';
            $params[] = $contracts_start_time;
        }

        if($contracts_end_time)
        {
            $wheres[] = '(contracts_expiration_date <= STR_TO_DATE(?, \'%m/%d/%Y\'))';
            $params[] = $contracts_end_time;
        }

        if($orderby_request) {
            $orderby = $orderby_request;
        }

        if($show_hidden == '0')
        {
            $_SESSION['contracts_show_hidden'] = 0;
            $wheres[] = '(contracts_hidden <> ?)';
            $params[] = '1';
        }

        else if($show_hidden == 1 || $_SESSION['contracts_show_hidden'] == 1)
        {
            $_SESSION['contracts_show_hidden'] = 1;
            $wheres[] = '(contracts_hidden = ?)';
            $params[] = '1';
        }
        else
        {
            $wheres[] = '(contracts_hidden <> ?)';
            $params[] = '1';
        }

        if(ActiveMemberInfo::IsUserMultiLocation())
        {
            if($licensed_locations_id)
            {
                $questions = array();
                foreach($licensed_locations_id as $id) {
                    $questions[] = '?';
                    $params[] = $id;
                }
                $questions = implode(',', $questions);
                $wheres[] = "(contracts_all_locations = 1 OR exists(select 1 from contracts_x_licensed_locations where join_contracts_id = contracts_id and join_licensed_locations_id in ($questions)))";
            }
        }

        $wheres[] = UserPermissions::CreateInClause('contracts', array('checkView' => true));

        //get count
        $db->fetch_all(contracts::build_query($wheres, $orderby, $paging, false), $params);
        $paging->set_total($db->found_rows());

        //get page data
        $contracts = $db->fetch_all(contracts::build_query($wheres, $orderby, $paging, true), $params);

        return $contracts;
    }

    public static function getNotificationEmailsForContract($contract) {
        $db = mysqli_db::init();

        $emailMembers = $db->fetch_all(
            'select members.* from contract_remind_members
              inner join members on join_members_id = members_id
              WHERE join_contracts_id = ?
                AND members_status != \'locked\'', array($contract['contracts_id']));

        foreach($emailMembers as $emailMember) {
            $emailAddresses[] = $emailMember['members_email'];
        }
        $additionalRecipients = $db->fetch_all('
            select car.*
            from contracts c
            inner join contracts_additional_recipients car on c.contracts_id = car.join_contracts_id
            inner join accounts on join_accounts_id = accounts_id
            where contracts_id = ? and accounts_is_locked = 0', array($contract['contracts_id']));

        foreach($additionalRecipients as $recipient) {
            if(in_array($recipient['contracts_additional_recipients_email'], $emailAddresses)) {
                continue;
            }
            $emailMembers[] = array(
                'members_email' => $recipient['contracts_additional_recipients_email']
            );
        }

        return $emailMembers;
    }
    public static function getNotificationEmailsForContractsMilestones($contracts_milestones_id) {
        $db = mysqli_db::init();

        $emailMembers = $db->fetch_all(
            'select members.* from contracts_milestones_remind_members inner join members on join_members_id = members_id
              WHERE join_contracts_milestones_id = ?
                AND members_status != \'locked\'', array($contracts_milestones_id));

        foreach($emailMembers as $emailMember) {
            $emailAddresses[] = $emailMember['members_email'];
        }

        $additionalRecipients = $db->fetch_all('
            select car.*
            from contracts c
            inner join contracts_milestones cm on c.contracts_id = cm.join_contracts_id
            inner join contracts_milestones_additional_recipients car on cm.contracts_milestones_id = car.join_contracts_milestones_id
            inner join accounts on join_accounts_id = accounts_id
            where join_contracts_milestones_id = ? and accounts_is_locked = 0', array($contracts_milestones_id));

        foreach($additionalRecipients as $recipient) {
            if(in_array($recipient['contracts_milestones_additional_recipients_email'], $emailAddresses)) {
                continue;
            }
            $emailMembers[] = array(
                'members_email' => $recipient['contracts_milestones_additional_recipients_email']
            );
        }
        return $emailMembers;
    }
    public static function get_files($contracts_id)
    {
        if(!UserPermissions::UserCanAccessObject('contracts', $contracts_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_all('
            SELECT *
            FROM contracts_files
            INNER JOIN files ON join_files_id = files_id
            WHERE
            join_contracts_id = ?',
            array($contracts_id));
    }
    public static function get_file($contracts_files_id)
    {
        $db = mysqli_db::init();
        $contractFile = $db->fetch_one('
            SELECT *
            FROM contracts_files
            INNER JOIN files ON join_files_id = files_id
            WHERE
            contracts_files_id = ?',
            array($contracts_files_id));

        if(!UserPermissions::UserCanAccessObject('contracts', $contractFile['join_contracts_id'], array('checkView' => true))) {
            return null;
        }

        return $contractFile;
    }
    public static function setActiveFile($contracts_files_id) {
        $status = array();

        $file = contracts::get_file($contracts_files_id);
        $perms = UserPermissions::UserObjectPermissions('contracts', $file['join_contracts_id']);
        if($file && $perms['edit']) {
            $db = mysqli_db::init();

            //Deactivate all files for the cert
            $db->query("
              UPDATE contracts_files SET contracts_files_active = 0
              WHERE join_contracts_id = ?",
                array($file['join_contracts_id']));

            //Activate requested file
            $db->query("
              UPDATE contracts_files SET contracts_files_active = 1
              WHERE contracts_files_id = ?",
                array($contracts_files_id));

            members::log_action(ActiveMemberInfo::GetMemberId(),
                'Contracts File made active: ' . $file['files_name']);

            contracts_history::active_file_changed(ActiveMemberInfo::GetMemberId(), $file['join_contracts_id'], $file['files_name']);

            $status['notice'] = $file['files_name'] . ' has been set as the active contracts file.';
        } else {
            $status['notice'] = 'You do not have permissions to edit this contract.';
        }

        return $status;
    }
    public static function delete_remind_members_by_contracts_id($contracts_id){
        if(!UserPermissions::UserCanAccessObject('contracts', $contracts_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        $db->query('DELETE FROM contract_remind_members WHERE '
            . 'join_contracts_id = ?', array($contracts_id));
    }
    public static function get_expired_remind_members($contracts_id)
    {
        if (!UserPermissions::UserCanAccessObject('contracts', $contracts_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_singlets('
            SELECT join_members_id
            FROM contract_remind_members
            WHERE
            join_contracts_id = ? AND contracts_remind_expiry = ?',
            array($contracts_id, 1));
    }
    public static function get_contracts_milestones($contracts_id)
    {
        if (!UserPermissions::UserCanAccessObject('contracts', $contracts_id, array('checkView' => true))) {
            return null;
        }

        $db = mysqli_db::init();
        $contracts_milestones = $db->fetch_all('
            SELECT *
            FROM contracts_milestones
            WHERE
            join_contracts_id = ?',
            array($contracts_id));
        return($contracts_milestones);
    }
    static function delete_contracts_milestones_by_id($deleted_milestones){
        $db = mysqli_db::init();
        $db->query('DELETE from contracts_milestones WHERE '
                . 'contracts_milestones_id IN('.implode(",",$deleted_milestones).')');
        
    }
    public static function get_contracts_milestones_remind_days($contracts_milestones_id)
    { 
        $db = mysqli_db::init();
        $contracts_milestones_remind_days = $db->fetch_singlets('
            SELECT contracts_milestones_remind_days
            FROM contracts_milestones_remind_days
            WHERE
            join_contracts_milestones_id = ?',
            array($contracts_milestones_id));
        return($contracts_milestones_remind_days);
    }
    public static function get_contracts_milestones_remind_members($contracts_milestones_id)
    { 
        $db = mysqli_db::init();
        $contracts_milestones_remind_members = $db->fetch_singlets('
            SELECT join_members_id
            FROM contracts_milestones_remind_members
            WHERE
            join_contracts_milestones_id = ?',
            array($contracts_milestones_id));
        return($contracts_milestones_remind_members);
    }
    static function delete_milestones_remind_days_by_contract_id($contract_id){
        $db = mysqli_db::init();
        $db->query('DELETE from contracts_milestones_remind_days WHERE '
                . 'join_contracts_id = ?',array($contract_id));
        
    }
    public static function get_contracts_milestones_additional_recipients($contracts_milestones_id)
    { 
        $db = mysqli_db::init();
        $contracts_milestones_additional_recipients = $db->fetch_all('
            SELECT *
            FROM contracts_milestones_additional_recipients
            WHERE
            join_contracts_milestones_id = ?',
            array($contracts_milestones_id));
        return($contracts_milestones_additional_recipients);
    }
    static function delete_milestones_remind_members_by_contract_id($contract_id){
        $db = mysqli_db::init();
        $db->query('DELETE from contracts_milestones_remind_members WHERE '
                . 'join_contracts_id = ?',array($contract_id));
        
    }
    static function delete_milestones_additional_recipients_by_contract_id($contract_id){
        $db = mysqli_db::init();
        $db->query('DELETE from contracts_milestones_additional_recipients WHERE '
                . 'join_contracts_id = ?',array($contract_id));
        
    }
}
?>