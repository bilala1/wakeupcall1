<?
final class contracts_history
{
    public static function create_contract_entry($members_id, $contracts_id, $contracts_history_description)
    {
        $contracts_history_table = new mysqli_db_table('contracts_history');

        $contracts_history = array('join_members_id' => $members_id,
            'join_contracts_id' => $contracts_id,
            'contracts_history_description' => $contracts_history_description);

        $contracts_history_table->insert($contracts_history);
        $contracts_history_id = $contracts_history_table->last_id();

        return $contracts_history_id;
    }

    public static function new_contract_entry($members_id, $contracts_id)
    {
        return contracts_history::create_contract_entry($members_id, $contracts_id, 'Contract created.');
    }

    public static function contract_location_updated_entry($members_id, $contract, $old_contracts)
    {
        $history_text = '<b>Location Changed From: </b>' . $old_contracts['licensed_locations_name'] . '<b> To: </b>' . $contract['licensed_locations_name'];

        return contracts_history::create_contract_entry($members_id, $contract['contracts_id'], $history_text);
    }

    public static function check_field_update($members_id, $field_name, $field_text, $contract, $old_contract)
    {
        if ($contract[$field_name] != $old_contract[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $old_contract[$field_name] . '<b> To: </b>' . $contract[$field_name];
            return contracts_history::create_contract_entry($members_id, $contract['contracts_id'], $history_text);
        }
    }

    public static function check_bit_field_update($members_id, $field_name, $field_text, $contract, $old_contract)
    {
        $values = array('False', 'True');
        if ($contract[$field_name] != $old_contract[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $values[$old_contract[$field_name]] . '<b> To: </b>' . $values[$contract[$field_name]];
            return contracts_history::create_contract_entry($members_id, $contract['contracts_id'], $history_text);
        }
    }

    public static function check_date_field_update($members_id, $field_name, $field_text, $contract, $old_contract)
    {
        if ($contract[$field_name] != $old_contract[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . contracts_history::format_date($old_contract[$field_name]) . '<b> To: </b>' . contracts_history::format_date($contract[$field_name]);
            return contracts_history::create_contract_entry($members_id, $contract['contracts_id'], $history_text);
        }
    }

    private static function format_date($date)
    {
        return empty($date) ? 'null' : date('n/j/Y', strtotime($date));
    }

    public static function contract_updated_entry($members_id, $contract, $old_contract)
    {
        if ($contract && $old_contract) {
            contracts_history::check_field_update($members_id, 'contracts_contracted_party_name', 'Contracted Party', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_contact_name', 'Contact Name', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_contact_phone', 'Contact Phone', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_contact_email', 'Contact Email', $contract, $old_contract);
            contracts_history::check_date_field_update($members_id, 'contracts_effective_date', 'Effective Date', $contract, $old_contract);
            contracts_history::check_date_field_update($members_id, 'contracts_expiration_date', 'Expiration Date', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_description', 'Description', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_additional_details', 'Additional details', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_status', 'Status', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_transition_status_to', 'Transition status to', $contract, $old_contract);
            contracts_history::check_field_update($members_id, 'contracts_expiration_notify_lead_days', 'Expiration notification lead time', $contract, $old_contract);
            contracts_history::check_bit_field_update($members_id, 'contracts_expired_notification', 'Notify on contract expiration', $contract, $old_contract);
            contracts_history::check_bit_field_update($members_id, 'contracts_location_can_view', 'Locations can view', $contract, $old_contract);
            contracts_history::check_bit_field_update($members_id, 'contracts_location_can_edit', 'Locations can edit', $contract, $old_contract);


            $licensed_locations = licensed_locations::get_for_account(ActiveMemberInfo::GetAccountId());
            $licensed_locations_names = array();
            foreach ($licensed_locations as $licensed_location) {
                $licensed_locations_names[$licensed_location['licensed_locations_id']] = $licensed_location['licensed_locations_name'];
            }

            if ($contract['contracts_all_locations']) {
                $contract['licensed_locations'] = 'All Locations';
            } else {
                $join_licensed_locations = array();
                foreach ($contract['join_licensed_locations'] as $join_licensed_locations_id) {
                    $join_licensed_locations[] = $licensed_locations_names[$join_licensed_locations_id];
                }
                sort($join_licensed_locations);
                $contract['licensed_locations'] = implode(', ', $join_licensed_locations);
            }

            if ($old_contract['contracts_all_locations']) {
                $old_contract['licensed_locations'] = 'All Locations';
            } else {
                $join_licensed_locations = array();
                foreach ($old_contract['join_licensed_locations'] as $join_licensed_locations_id) {
                    $join_licensed_locations[] = $licensed_locations_names[$join_licensed_locations_id];
                }
                sort($join_licensed_locations);
                $old_contract['licensed_locations'] = implode(', ', $join_licensed_locations);
            }
            contracts_history::check_field_update($members_id, 'licensed_locations', 'Location', $contract, $old_contract);
        }
    }

    public static function file_added_entry($members_id, $contracts_id, $file_name)
    {
        $history_text = "<b>File Added: </b>" . $file_name;

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function active_file_changed($members_id, $contracts_id, $file_name)
    {
        $history_text = "<b>Active File Set To: </b>" . $file_name;

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function hidden_changed($members_id, $contracts_id, $new_value)
    {
        $history_text = "<b>Contract " . ( $new_value ? "Hidden" : "Unhidden" ) . "</b>";

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function file_deleted_entry($members_id, $contracts_id, $file_name)
    {
        $history_text = "<b>File Deleted: </b>" . $file_name;

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function note_added_entry($members_id, $contracts_id, $contracts_note)
    {
        $history_text = "<b>Note Added:</b> " . $contracts_note['contracts_notes_subject'];

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function note_updated_entry($members_id, $contracts_id, $contracts_note)
    {
        $history_text = "<b>Note Updated:</b> " . $contracts_note['contracts_notes_subject'];

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function note_deleted_entry($members_id, $contracts_id, $contracts_note_name)
    {
        $history_text = "<b>Note Deleted: </b>" . $contracts_note_name;

        return contracts_history::create_contract_entry($members_id, $contracts_id, $history_text);
    }

    public static function get_contracts_history_with_data($members_id, $contracts_id)
    {
        $db = mysqli_db::init();

        $contracts_history = $db->fetch_all('SELECT * FROM contracts_history WHERE join_contracts_id = ? order by contracts_history_datetime', array($contracts_id));

        return $contracts_history;
    }
}

?>