<?

class contracts_milestones {

    public static function addUpdateMilestone($contracts_id,$existing_milestone_ids,$data){
        $contracts_milestones = new mysqli_db_table('contracts_milestones');
        $contracts_milestones_remind_days_table = new mysqli_db_table('contracts_milestones_remind_days');
        $contracts_milestones_remind_members_table = new mysqli_db_table('contracts_milestones_remind_members');
        $contracts_milestones_additional_recipients_table = new mysqli_db_table('contracts_milestones_additional_recipients');
        $contracts_milestones_array = array();
        foreach($data['contracts_milestones_id'] as $key => $contracts_milestones_id){
            $contracts_milestones_array[$key]['contracts_milestones_id'] = $contracts_milestones_id;
            }
        foreach($data['contracts_milestones_date'] as $key => $contracts_milestones_date){
            $contracts_milestones_array[$key]['contracts_milestones_date'] = date('Y-m-d', strtotime($contracts_milestones_date));
            }
        foreach($data['contracts_milestones_description'] as $key => $contracts_milestones_description){
            $contracts_milestones_array[$key]['contracts_milestones_description'] = $contracts_milestones_description;
        }
        $milestone_updated = array();
        $contracts_milestones_remind_days = array(); 
        $contracts_milestones_remind_members = array();
        $contracts_milestones_additional_recipients = array();
        foreach($contracts_milestones_array as $key => $contracts_milestone){
            $contracts_milestone['join_contracts_id'] = $contracts_id;
            $contracts_milestones_id = $contracts_milestone['contracts_milestones_id'];
            if(empty($contracts_milestones_id)){
                $contracts_milestone['contracts_milestones_id'] = '';
                if(!empty($contracts_milestone['contracts_milestones_date']) && !empty($contracts_milestone['contracts_milestones_description'])){
                    $contracts_milestones->insert($contracts_milestone);
                    $contracts_milestones_id = $contracts_milestones->last_id();
                }
            }else{
               if(!empty($contracts_milestone['contracts_milestones_date']) && !empty($contracts_milestone['contracts_milestones_description'])){
                    $milestone_updated[] =  $contracts_milestones_id;
                    $contracts_milestones->update($contracts_milestone, $contracts_milestones_id);
               }
            }
            foreach ($data["contracts_milestones_remind_days_$key"] as $key1 => $milestones_remind_days ){
                $contracts_milestones_remind_days[$key][$key1]['join_contracts_id'] = $contracts_id;
                $contracts_milestones_remind_days[$key][$key1]['join_contracts_milestones_id'] = $contracts_milestones_id;
                $contracts_milestones_remind_days[$key][$key1]['contracts_milestones_remind_days'] = $milestones_remind_days;
            }
            foreach ($data["contracts_milestones_remind_members_$key"] as $key1 => $milestone_remind_members ){
                $contracts_milestones_remind_members[$key][$key1]['join_contracts_id'] = $contracts_id;
                $contracts_milestones_remind_members[$key][$key1]['join_contracts_milestones_id'] = $contracts_milestones_id;
                $contracts_milestones_remind_members[$key][$key1]['join_members_id'] = $milestone_remind_members;
            }
            foreach ($data["contracts_milestones_additional_recipients_email_$key"] as $key1 => $milestones_additional_recipients_email ){
                $contracts_milestones_additional_recipients[$key][$key1]['join_contracts_id'] = $contracts_id;
                $contracts_milestones_additional_recipients[$key][$key1]['join_contracts_milestones_id'] = $contracts_milestones_id;
                $contracts_milestones_additional_recipients[$key][$key1]['contracts_milestones_additional_recipients_email'] = $milestones_additional_recipients_email;
            }

        }
        contracts::delete_milestones_remind_days_by_contract_id($contracts_id);
        foreach($contracts_milestones_remind_days as $key => $milestones_remind_days){
            foreach($milestones_remind_days as $remind_days){
               $contracts_milestones_remind_days_table->insert($remind_days);
            }
        }
        contracts::delete_milestones_remind_members_by_contract_id($contracts_id);
        foreach($contracts_milestones_remind_members as $key => $milestone_remind_members){
            foreach($milestone_remind_members as $remind_members){
               $contracts_milestones_remind_members_table->insert($remind_members);
            }
        }
        contracts::delete_milestones_additional_recipients_by_contract_id($contracts_id);
        foreach($contracts_milestones_additional_recipients as $key => $milestones_additional_recipients){
            foreach($milestones_additional_recipients as $additional_recipients){
               $contracts_milestones_additional_recipients_table->insert($additional_recipients);
            }
        }

        $deleted_milestones = array_diff($existing_milestone_ids,$milestone_updated);
        if(count($deleted_milestones) > 0){
            contracts::delete_contracts_milestones_by_id($deleted_milestones);
        }
    }
}
?>