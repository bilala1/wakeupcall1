<?php

class corporate_locations{
    static function get_all(){
        $db = mysqli_db::init();

        return $db->fetch_all('select * from corporate_locations');
    }

    static function create($new_office){
        $db = mysqli_db::init();

        $corporate_locations_table = new mysqli_db_table('corporate_locations');
        $corporate_locations_table->insert($new_office);
        $corporate_locations_id = $corporate_locations_table->last_id();

        return $corporate_locations_id;
    }

    static function get_by_id($id){
        $db = mysqli_db::init();
        return $db->fetch_one('select * from corporate_locations
                               where corporate_locations_id = ? ', array($id));
    }

    static function get_by_licensed_location_id($id){
        $db = mysqli_db::init();
        return $db->fetch_one('select * from corporate_locations
                               where join_licensed_locations_id = ? ', array($id));
    }
}

?>