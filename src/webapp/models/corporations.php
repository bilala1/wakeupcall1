<?php

class corporations
{
    static function get_all()
    {
        $db = mysqli_db::init();

        return $db->fetch_all('select * from corporations order by corporations_name');
    }

    static function get_hotels($corporations_id)
    {
        $db = mysqli_db::init();

        return $db->fetch_all('SELECT * FROM licensed_locations AS ll JOIN members AS m ON (ll.join_members_id = m.members_id)  where join_corporations_id = ? ORDER BY licensed_locations_name', array($corporations_id));
    }

    static function get_by_id($id)
    {
        $db = mysqli_db::init();

        return $db->fetch_one('select * from corporations where corporations_id = ? ', array($id));
    }

    static function get_licensed_locations_for_corp_id($id)
    {
        $db = mysqli_db::init();

        return $db->fetch_all('SELECT * FROM licensed_locations AS l JOIN members AS m ON (l.join_members_id = m.members_id)  where join_corporations_id = ? ORDER BY licensed_locations_name', array($id));
    }

    static function create($new_corp)
    {
        $corporation_table = new mysqli_db_table('corporations');
        $corporation_table->insert($new_corp);
        $corporations_id = $corporation_table->last_id();

        return $corporations_id;
    }

    static function get_forum_groups_id($corporations_id) {
        $db = mysqli_db::init();

        $corporation = corporations::get_by_id($corporations_id);

        return  $db->fetch_singlet('select groups_id from forums_groups where groups_name = ?', array($corporation['corporations_name'].' - Location'));
    }
}

?>