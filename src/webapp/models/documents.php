<?php
class documents{
    static function describe(){
        $db = mysqli_db::init();
        
        return $db->fetch_all('describe documents');
    }
    
    //returns list of folders a person has
    static function get_my_folders(){
        $db = mysqli_db::init();
        $folders = $db->fetch_all('SELECT members_library_categories_id, members_library_categories_name 
                                   FROM members_library_categories
                                   WHERE join_members_id = ? ', array(ActiveMemberInfo::GetMemberId()));
        foreach($folders as $k => $folder){
            $f[$folder['members_library_categories_id']] = $folder['members_library_categories_name'];
         
        }
        return $f;
    }
}
?>