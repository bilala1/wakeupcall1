<?php


///
/// This clas manages the per location over-ride permissions for email templates.
/// The fall back works like this:
///
/// Corporate Template -> if a location can override they can either have a location template
//  or do a one time per item on the fly change as well.  If there is no corporate or location
//  email template then we use the default wakeupcall templates.
//
class emails_default_templates
{
    static function get_certificate_request_template()
    {
        $db = mysqli_db::init();
        $template = $db->fetch_one('SELECT * FROM default_email_templates '.
                               'WHERE email_template_description = "Certificates_Request"');

        if(!$template) return null;
        
        return $template['email_html'];
    }
    
    static function get_claims_email_template()
    {
        $db = mysqli_db::init();
        $template = $db->fetch_one('SELECT * FROM default_email_templates '.
                               'WHERE email_template_description = "Claims_Email"');

        if(!$template) return null;
        
        return $template['email_html'];
    }
    
}