<?php

/// This class manages the sent e-mail history for claims, and certificate requests.
///
/// JR TODO: E-Mail storage is currently set up to be placed in database, it should
/// either be file based or stored to a blob type cloud provider.
///
///
class emails_history
{
    /**
     * 
     * @param $account_id - account id to fetch emails for.
     * @param $locations_id - locations_id, omit or null for all locations for an account.
     * @param $email_desc - email description type
     * @param $orderby - orderby column name
     * @param $startTime - start time to filter by (optional)
     * @param $endTime - end time to filter by (optional)
     * @param $paging - paging object
     * @param $do_paging - Boolean determine if we should or shouldn't do a paged query
     * @return meta data items (line items missing actual HTML) for each query.
     */
    static function get_all_metadatas($account_id, $locations_ids = null, $email_desc = null, $orderby = 'sent_emails_date', $startTime = null, $endTime = null, $paging = null, $do_paging = false, $table = null)
    {
        $db = mysqli_db::init();

        $wheres = " WHERE seh.join_accounts_id = ?";
        $params[] = $account_id;

        if($locations_ids != null)
        {
            $wheres .= " AND seh.join_licensed_locations_id in ";
            foreach($locations_ids as $id) {
                if($id != 'all')
                    $filter_location_ids[] = $id;
            }
            $wheres .= '(' . implode($filter_location_ids, ',') . ')';
            
        }

        if($email_desc != null)
        {
            $wheres = $wheres . " AND sent_emails_desc = ?";
            $params[] = $email_desc;
        }
        if($table == 'claims'){
            
            $wheres = $wheres . " AND claims_id != ?";
            $params[] = '';
            
        }elseif($table == 'certificates'){
            
            $wheres = $wheres . " AND certificates_id != ?";
            $params[] = '';
        }
        
        if($startTime != null && $endTime != null)
        {
            $wheres = $wheres . " AND sent_emails_date >= STR_TO_DATE(?, '%m/%d/%Y') and DATE_SUB(sent_emails_date, INTERVAL 1 DAY) <= STR_TO_DATE(?, '%m/%d/%Y')";
            $params[] = $startTime;
            $params[] = $endTime;
        }
        
        if($orderby == '') $orderby = 'sent_emails_date';
        
        $orderby_query = ' ' . strings::orderby($orderby, 'sent_emails_history');

        $paging_query = '';
        //if($do_paging)
        //    $paging_query = ' ' . $paging->get_limit();
        
        $emails = $db->fetch_all(
            'SELECT sent_emails_history_id, seh.join_accounts_id, seh.join_licensed_locations_id, sent_emails_date, sent_emails_desc,
                vendors_name, claims_type, join_certificates_id, certificates_id, join_claims_id, claims_id,
                sent_emails_email,sent_emails_filename,sent_emails_history_status,sent_emails_history_status_reason,sent_emails_communication_type
              FROM sent_emails_history seh
              LEFT JOIN claims ON join_claims_id = claims_id
              LEFT JOIN certificates ON join_certificates_id = certificates_id
              LEFT JOIN vendors on join_vendors_id = vendors_id ' . $wheres . $orderby_query . $paging_query,
            $params);

        if(!$emails) return null;

        return $emails;
    }

    /**
     * 
     * @param $email_id - email ID to get metadata for
     * @return single metadata item for the ID, null if not found
     */
    static function get_metadata_for_id($email_id)
    {
        $db = mysqli_db::init();

        $emails = $db->fetch_one(
            'SELECT sent_emails_history_id, join_accounts_id, join_licensed_locations_id, sent_emails_date, sent_emails_desc FROM sent_emails_history WHERE sent_emails_history_id = ? AND ' . UserPermissions::CreateInClause('licensed_locations')
            , array($email_id));

        if(!$emails) return null;

        return $emails;
    }

    /**
     *
     * @param $claims_id - The id of the claim to get history for
     * @return meta data items (line items missing actual HTML) for each query.
     */
    static function get_metadata_for_claim($claims_id)
    {
        $db = mysqli_db::init();

        $wheres = " WHERE join_claims_id = ?";
        $params[] = $claims_id;

        $emails = $db->fetch_all(
            'SELECT sent_emails_history_id, seh.join_accounts_id, seh.join_licensed_locations_id, sent_emails_date, 
                sent_emails_desc, sent_emails_email,sent_emails_filename,sent_emails_communication_type,
                sent_emails_history_status,sent_emails_history_status_reason,
                claims_type, join_claims_id, claims_id
              FROM sent_emails_history seh
              LEFT JOIN claims ON join_claims_id = claims_id ' . $wheres .
            ' ORDER BY sent_emails_date desc',
            $params);

        if(!$emails) return null;

        return $emails;
    }

    /**
     *
     * @param $certificates_id - The id of the certificate to get history for
     * @return meta data items (line items missing actual HTML) for each query.
     */
    static function get_metadata_for_certificate($certificates_id)
    {
        $db = mysqli_db::init();

        $wheres = " WHERE join_certificates_id = ?";
        $params[] = $certificates_id;

        $emails = $db->fetch_all(
            'SELECT sent_emails_history_id, seh.join_accounts_id, seh.join_licensed_locations_id, sent_emails_date, sent_emails_email,
                sent_emails_desc, vendors_name, join_certificates_id, certificates_id
              FROM sent_emails_history seh
              LEFT JOIN certificates ON join_certificates_id = certificates_id
              LEFT JOIN vendors on join_vendors_id = vendors_id ' . $wheres .
            ' ORDER BY sent_emails_date desc',
            $params);

        if(!$emails) return null;

        return $emails;
    }

    /**
     * Get the actual HTML for the email id
     * 
     * @param $email_id - email ID to get html for.
     * @return html email with metadata
     */
    static function get_html_for_id($email_id)
    {
        $db = mysqli_db::init();

        $emails = $db->fetch_one(
            'SELECT * FROM sent_emails_history inner join licensed_locations on join_licensed_locations_id = licensed_locations_id
              WHERE sent_emails_history_id = ? AND '. UserPermissions::CreateInClause('licensed_locations'),
            array($email_id));

        if(!$emails) return null;

        $emails['sent_emails_html'] = stripslashes($emails['sent_emails_html']);
        
        return $emails;
    }

    /**
     * Add a new e-mail to the history class
     * 
     * @param $account_id - Account ID to add history for
     * @param $locations_id - Location ID to add history for
     * @param $email_desc - email description type
     * @param $email_html - html of sent email
     */
    static function add($account_id, $locations_id, $email_desc, $email_html, $join_claims_id, $join_certificates_id, $filename, $email_to, $communication_type='email',$sent_emails_history_status = null,$sent_emails_history_status_reason = null , $sent_emails_history_submission_id = null)
    {
        $html = addslashes($email_html);
        $html = trim($html);
        $html = preg_replace( "/\r|\n/", "", $html );
        
         $db = mysqli_db::init();
         $now = $date = date('Y/m/d G:i:s', time());
         $db->query('INSERT INTO sent_emails_history(join_accounts_id, join_licensed_locations_id, sent_emails_desc, sent_emails_date, sent_emails_html, join_claims_id, join_certificates_id,sent_emails_filename,sent_emails_email,sent_emails_communication_type,sent_emails_history_status,sent_emails_history_status_reason,sent_emails_history_submission_id) '
                     . 'VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', array($account_id, $locations_id, $email_desc, $now, $html, $join_claims_id, $join_certificates_id, $filename, $email_to, $communication_type, $sent_emails_history_status, $sent_emails_history_status_reason, $sent_emails_history_submission_id));
        return($db->last_id());
    }
}