<?php


///
/// This clas manages the per location over-ride permissions for email templates.
/// The fall back works like this:
///
/// Corporate Template -> if a location can override they can either have a location template
//  or do a one time per item on the fly change as well.  If there is no corporate or location
//  email template then we use the default wakeupcall templates.
//
class emails_templates_permissions
{
    /**
     * 
     * @param $location_id - Location ID to get permissions for
     * @return permissions item
     */
    static function get_for_location( $location_id )
    {
        $db = mysqli_db::init();
        $permission = $db->fetch_one('SELECT * FROM licensed_locations_x_email_permissions '.
                               'WHERE join_licensed_locations_id = ?', array($location_id));

        if(!$permission) return null;
        
        return $permission;
    }
    
    /**
     * @function add_or_update_certs_for_location - 
     * adds if necessary or updates existing line for a location for per-locaiton certs email template useage permission.
     * 
     * @param $location_id - location_id of location to add entry for.
     * @param $can_override - permission of if location can override the corporate template or not.
     */
    static function add_or_update_certs_for_location($location_id, $can_override)
    {
        $db = mysqli_db::init();
     
        $value = $can_override ? 1 : 0;
        
        $permission = emails_templates_permissions::get_for_location($location_id);
        
        if(count ($permission) > 0)
        {
            $db->query('UPDATE licensed_locations_x_email_permissions SET location_can_override_certificates_email = ? WHERE join_licensed_locations_id = ?',
                        array($value, $location_id));
        }
        else
        {
            $db->query('INSERT INTO licensed_locations_x_email_permissions(join_licensed_locations_id, location_can_override_certificates_email, location_can_override_claims_email) '
                 . 'VALUES(?, ?, ?, ?)', array($location_id, $value ,0));
        }
    }
    
    /**
     * @function add_or_update_claims_email_for_location - 
     * adds if necessary or updates existing line for a location for per-locaiton claims email template useage permission.
     * 
     * @param $location_id - location_id of location to add entry for.
     * @param $can_override - permission of if location can override the corporate template or not.
     */
    static function add_or_update_claims_email_for_location($location_id, $can_override)
    {
        $db = mysqli_db::init();
     
        $value = $can_override ? 1 : 0;
        
        $permission = emails_templates_permissions::get_for_location($location_id);
        
        if(count ($permission) > 0)
        {
            $db->query('UPDATE licensed_locations_x_email_permissions SET location_can_override_claims_email = ? WHERE join_licensed_locations_id = ?',
                        array($value, $location_id));
        }
        else
        {
            $db->query('INSERT INTO licensed_locations_x_email_permissions(join_licensed_locations_id, location_can_override_claims_email, location_can_override_certificates_email) '
                 . 'VALUES(?, ?, ?)', array($location_id, $value, 0));
        }
    }
    
    /**
     * @function delete_for_location - delete permissions for goiven location
     * @param $location_id - location_id to delete permissions for.
     */
    static function delete_for_location($location_id)
    {
        $db = mysqli_db::init();
       
        $db->query(
            'DELETE FROM licensed_locations_x_email_permissions  '
          . 'WHERE join_licensed_locations_id = ?', array($location_id));

    }
    
    /** @function get_all - Get all licensed_locations_x_email items
     * 
     * @return licensed_locations_x_email item
     */
    static function get_all()
    {
        $db = mysqli_db::init();

        return $db->fetch_all('SELECT * FROM licensed_locations_x_email_permissions');
    }
    
    /** @function get_by_id - Get one permission entry by id.
    //  @param $id - licensed_locations_x_email_id of item to fetch.
    //  @return - licensed_locations_x_email item 
    */
    static function get_by_id($id)
    {
        $db = mysqli_db::init();
        
        return $db->fetch_all('SELECT * FROM licensed_locations_x_email_permissions WHERE '
                . 'licensed_locations_x_email_permissions_id = ?', array($id));
    }
    
}