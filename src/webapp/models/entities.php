<?php

class entities {

    public static function get_entity_type($entity) {
        $entityType = 'business';
        if($entity['business_entities_id']) {
            $entityType = 'business';
        }
        return $entityType;
    }

    public static function get_business_files($business_entities_id)
    {
        if(!UserPermissions::UserCanAccessObject('business_entities', $business_entities_id)) {
            return null;
        }

        $db = mysqli_db::init();
        return $db->fetch_all('
            SELECT f.*, ef.*
            FROM entities_files ef
            inner join files f on files_id = join_files_id
            inner join business_entities be on be.join_entities_id = ef.join_entities_id
            WHERE
            business_entities_id = ?',
            array($business_entities_id));
    }

    public static function get_file($entities_files_id)
    {
        $db = mysqli_db::init();
        $entityFile = $db->fetch_one('
            SELECT *
            FROM entities_files ef
            inner join files f on files_id = join_files_id
            inner join business_entities be on be.join_entities_id = ef.join_entities_id
            WHERE
            entities_files_id = ?',
            array($entities_files_id));

        if(!UserPermissions::UserCanAccessObject('business_entities', $entityFile['business_entities_id'])) {
            return null;
        }

        return $entityFile;
    }

    public static function get_business_entity($business_entities_id)
    {
        $db = mysqli_db::init();
        $entity_data = $db->fetch_one('
        SELECT *,
            (
                SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
                FROM business_entities_x_licensed_locations bell
                WHERE bell.join_business_entities_id = business_entities_id
                ) as `join_licensed_locations`
        FROM entities
        JOIN business_entities be on be.join_entities_id = entities_id
        WHERE
        business_entities_id = ?',
            array($business_entities_id));
        if($entity_data['join_licensed_locations']) {
            $entity_data['join_licensed_locations'] = explode(',', $entity_data['join_licensed_locations']);
        }
        return $entity_data;
    }

    public static function get_business_entity_by_entity_id($entities_id)
    {
        $db = mysqli_db::init();

        $entity_data = $db->fetch_one('
        SELECT *,
            (
                SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
                FROM business_entities_x_licensed_locations bell
                WHERE bell.join_business_entities_id = business_entities_id
                ) as `join_licensed_locations`
        FROM entities
        JOIN business_entities be on be.join_entities_id = entities_id
        WHERE
        entities_id = ?',
            array($entities_id));
        if($entity_data['join_licensed_locations']) {
            $entity_data['join_licensed_locations'] = explode(',', $entity_data['join_licensed_locations']);
        }
        return $entity_data;
    }

    /**
     * @param $wheres
     * @param $orderBy
     * @param paging $paging
     * @param $do_paging
     * @return string
     */
    private static function build_query($wheres, $orderBy, $paging, $do_paging)
    {
        //LEFT JOIN licensed_locations on licensed_locations_id = join_licensed_locations_id
        $wheres[] = 'entities_delete_datetime is null';
        $query = '
        SELECT SQL_CALC_FOUND_ROWS *,
        (
        IF(business_entities_all_locations, \'All Locations\', (SELECT GROUP_CONCAT(licensed_locations_name SEPARATOR \', \')
            FROM business_entities_x_licensed_locations ell
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE ell.join_business_entities_id = business_entities_id
            ))) as `licensed_locations_name`,
        (SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
            FROM business_entities_x_licensed_locations ell
            WHERE ell.join_business_entities_id = business_entities_id
            ) as `licensed_locations_ids`,
        (SELECT COUNT(1)
            FROM business_entities_x_licensed_locations ell
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE ell.join_business_entities_id = business_entities_id
            ) as `licensed_locations_count`
        FROM entities
        LEFT JOIN business_entities be on be.join_entities_id = entities_id       
        ';
        $query = $query.strings::where($wheres).
            strings::orderby($orderBy, 'entities');

        if($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
    }

    /**
     * @param paging $paging
     * @param string $orderBy_request
     * @param $show_hidden
     * @param $licensed_locations_id
     * @return array quick note of hidden/visible entities. There are three options:
     *
     * @throws Exception
     */
    public static function get_paged_businesses_list(&$paging, $orderBy_request,
                                                 $show_hidden, $licensed_locations_id)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $params = array();
        $orderBy = 'entities_name';
        if($orderBy_request && !empty($orderBy_request)) {
            $orderBy = $orderBy_request;
        }

        if($show_hidden == '1')
        {
            $wheres[] = '(entities_hidden = ?)';
            $params[] = '1';
        }
        else
        {
            $wheres[] = '(entities_hidden <> ?)';
            $params[] = '1';
        }

        if(ActiveMemberInfo::IsUserMultiLocation() && $licensed_locations_id)
        {
            if($licensed_locations_id)
            {
                $questions = array();
                foreach($licensed_locations_id as $id) {
                    $questions[] = '?';
                    $params[] = $id;
                }
                $questions = implode(',', $questions);
                $wheres[] = "(business_entities_all_locations = 1 OR exists(select 1 from business_entities_x_licensed_locations where join_entities_id = entities_id and join_licensed_locations_id in ($questions)))";
                //$wheres[] = "join_licensed_locations_id in ($questions)";
            }
        }

        $wheres[] = UserPermissions::CreateInClause('business_entities');


        $query = entities::build_query($wheres, $orderBy, $paging, false);
        $db->fetch_all($query, $params);
        $paging->set_total($db->found_rows());

        //get page data
        $entities = $db->fetch_all(entities::build_query($wheres, $orderBy, $paging, true), $params);

        if($entities['join_licensed_locations']) {
            $entities['join_licensed_locations'] = explode(',', $entities['join_licensed_locations']);
        }
        return $entities;
    }

    public static function get_entity($entities_id){
        $db = mysqli_db::init();
        $entity = $db->fetch_one('
            SELECT *
            FROM entities
            left join business_entities on join_entities_id = entities_id
            WHERE entities_id = ?',array($entities_id));
        return $entity;
    }

    static function delete_notes_by_id($deleted_notes){
        $db = mysqli_db::init();
        $db->query('DELETE from entities_notes WHERE '
                . 'entities_notes_id IN('.implode(",",$deleted_notes).')');
    }
    static function fetch_location_for_business_entity($business_entities_id){
        $db = mysqli_db::init();
        $business_entities_locations = $db->fetch_all('SELECT *
            FROM business_entities_x_licensed_locations 
            WHERE join_business_entities_id = ?',array($business_entities_id));
        $locations = array();
        foreach($business_entities_locations as $business_entities_location){
            $locations[] = $business_entities_location['join_licensed_locations_id'];
        }
        return $locations;
    }
}