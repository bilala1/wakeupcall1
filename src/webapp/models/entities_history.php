<?
final class entities_history
{
    public static function create_entity_entry($members_id, $entities_id, $entities_history_description)
    {
        $entities_history_table = new mysqli_db_table('entities_history');

        $entities_history = array('join_members_id' => $members_id,
                                'join_entities_id' => $entities_id,
                                'entities_history_datetime' => date('Y-m-d H:i:s'),
                                'entities_history_description' => $entities_history_description);

        $entities_history_table->insert($entities_history);
        $entities_history_id = $entities_history_table->last_id();

        return $entities_history_id;
    }

    public static function new_entity_entry($members_id, $entities_id)
    {
        return entities_history::create_entity_entry($members_id, $entities_id, 'Entity created.');
    }

    public static function entity_location_updated_entry($members_id, $entity, $old_entities)
    {
        if($old_entities['licensed_locations_name'] == $entity['licensed_locations_name']) return;

        $history_text = '<b>Location Changed From: </b>' . $old_entities['licensed_locations_name'] . '<b> To: </b>' . $entity['licensed_locations_name'];

        return entities_history::create_entity_entry($members_id, $entity['entities_id'], $history_text);
    }

    private static function format_date($date) {
        return empty($date) ? 'null' : date('n/j/Y', strtotime($date));
    }

    public static function entity_updated_entry($members_id, $entity, $old_entity)
    {
        if($entity && $old_entity)
        {
            if($entity['licensed_locations_id'] != $old_entity['licensed_locations_id'])
            {
                entities_history::entity_location_updated_entry($members_id, $entity, $old_entity);
            }
            if($entity['entities_datetime'] != $old_entity['entities_datetime'])
            {
                entities_history::entity_incident_date_updated_entry($members_id, $entity, $old_entity);
            }
            if(stristr($entity['entities_type'], 'Comp'))
            {
                entities_history::entity_comp_update($members_id, $entity, $old_entity);
            }
            else if(stristr($entity['entities_type'], 'Liability'))
            {
                entities_history::entity_liability_update($members_id, $entity, $old_entity);
            }
        }
    }

    public static function hidden_changed($members_id, $entities_id, $new_value)
    {
        $history_text = "<b>Entity " . ( $new_value ? "Hidden" : "Unhidden" ) . "</b>";

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }

    public static function file_added_entry($members_id, $entities_id,  $file_name)
    {
        $history_text = "<b>File Added: </b>" . $file_name;

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }

    public static function file_deleted_entry($members_id, $entities_id, $file_name)
    {
        $history_text = "<b>File Deleted: </b>" . $file_name;

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }

    public static function note_added_entry($members_id, $entities_id, $entities_note)
    {
        $history_text = "<b>Note Added:</b> ". $entities_note['entities_notes_subject'];

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }

    public static function note_updated_entry($members_id, $entities_id, $entities_note)
    {
        $history_text = "<b>Note Updated:</b> ". $entities_note['entities_notes_subject'];

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }

    public static function note_deleted_entry($members_id, $entities_id, $entities_note_name)
    {
        $history_text = "<b>Note Deleted: </b>". $entities_note_name;

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }
    public static function item_deleted_entry($members_id, $entities_id, $entities_item_name)
    {
        $history_text = "<b>Tracked Item Deleted: </b>". $entities_item_name;

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }
    public static function item_added_entry($members_id, $entities_id, $entities_item_name)
    {
        $history_text = "<b>Tracked Item Added: </b>". $entities_item_name;

        return entities_history::create_entity_entry($members_id, $entities_id, $history_text);
    }
    public static function get_entities_history_with_data($members_id, $entities_id)
    {
        $db = mysqli_db::init();

        $entities_history = $db->fetch_all('SELECT * FROM entities_history WHERE join_entities_id = ?', array($entities_id));

        return $entities_history;
    }
}
?>