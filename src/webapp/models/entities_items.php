<?php

class entities_items {

    public static function get_entities_items_files($entities_items_id)
    {

        $db = mysqli_db::init();
        return $db->fetch_all('
            SELECT f.*, ef.*
            FROM entities_items_files ef
            inner join files f on files_id = join_files_id
            inner join entities_items be on be.entities_items_id = ef.join_entities_items_id
            WHERE
            entities_items_id = ?',
            array($entities_items_id));
    }
    
    public static function get_entities_item($entities_items_id)
    {
        $db = mysqli_db::init();

        $entity_data = $db->fetch_one('
        SELECT *
        FROM entities
        JOIN entities_items be on be.join_entities_id = entities_id
        WHERE
        entities_items_id = ?',
            array($entities_items_id));

        return $entity_data;
    }
    public static function get_entities_items_by_entity_id($entities_id)
    {
        $db = mysqli_db::init();

        $entity_item_data = $db->fetch_all('
        SELECT * FROM
        entities_items 
        WHERE
        join_entities_id = ?',
            array($entities_id));

        return $entity_item_data;
    }
    public static function get_entities_items_note_with_data($entities_items_note_id)
    {
        $db = mysqli_db::init();

        $entities_items_note = $db->fetch_one('SELECT * FROM entities_items_notes WHERE entities_items_notes_id = ?', array($entities_items_note_id));

        return $entities_items_note;
    }
    public static function delete_entities_items_notes($entities_items_notes_id)
    {
        if(!$entities_items_notes_id) return false;

        $entities_items_note = entities_items::get_entities_items_note_with_data($entities_items_notes_id);

        $entities_items_notes_table = new mysqli_db_table('entities_items_notes');

        $entities_items_notes_table->delete($entities_items_notes_id);
        entities_items_history::note_deleted_entry(ActiveMemberInfo::GetMemberId(), $entities_items_note['entities_items_id'], $entities_items_note['entities_notes_subject']);

        return true;
    }
    public static function delete_entities_items($entities_items_id)
    {
        if(!$entities_items_id) return false;

        $entities_items = entities_items::get_entities_item($entities_items_id);

        $entities_items_table = new mysqli_db_table('entities_items');

        $entities_items_table->delete($entities_items_id);
        entities_history::item_deleted_entry(ActiveMemberInfo::GetMemberId(), $entities_items['entities_items_id'], $entities_items['entities_items_name']);

        return true;
    }
    static function delete_item_notes_by_id($deleted_notes){
        $db = mysqli_db::init();
        $db->query('DELETE from entities_items_notes WHERE '
                . 'entities_items_notes_id IN('.implode(",",$deleted_notes).')');
        
    }
    public static function get_entities_items_milestones($entities_items_id)
    {
        $db = mysqli_db::init();
        $entities_items_milestones = $db->fetch_all('
            SELECT *
            FROM entities_items_milestones
            WHERE
            join_entities_items_id = ?',
            array($entities_items_id));
        return($entities_items_milestones);
    }
    static function delete_entities_items_milestones_by_id($deleted_milestones){
        $db = mysqli_db::init();
        $db->query('DELETE from entities_items_milestones WHERE '
                . 'entities_items_milestones_id IN('.implode(",",$deleted_milestones).')');
    }
    public static function delete_milestones_remind_members_by_entities_items_id($entities_items_id){
        $db = mysqli_db::init();
        $db->query('DELETE FROM entities_items_milestones_remind_members WHERE '
            . 'join_entities_items_id = ?', array($entities_items_id));
    }
    static function get_existing_remind_members_for_milestone($entities_items_id){
        $db = mysqli_db::init();
        $members = $db->fetch_all('SELECT join_members_id from entities_items_milestones_remind_members where join_entities_items_id = ?',array($entities_items_id));
        $existing_members = array();
        foreach($members as $member){
            $existing_members[]= $member['join_members_id'];
        }
        return $existing_members;
    }
   static function getNotificationEmailsForEntityItemMilestone($entities_items_milestones_id){
       $db = mysqli_db::init();

       $emailMembers = $db->fetch_all(
           'select members.* from entities_items_milestones_remind_members inner join members on join_members_id = members_id
              WHERE join_entities_items_milestones_id = ?
                AND members_status != \'locked\'', array($entities_items_milestones_id));

       foreach($emailMembers as $emailMember) {
           $emailAddresses[] = $emailMember['members_email'];
       }

       $additionalRecipients = $db->fetch_all('
            select ear.*
            from entities_items c
            inner join entities on join_entities_id = entities_id
            inner join entities_items_milestones em on c.entities_items_id = em.join_entities_items_id
            inner join entities_items_milestones_additional_recipients ear on em.entities_items_milestones_id = ear.join_entities_items_milestones_id
            inner join accounts on join_accounts_id = accounts_id
            where join_entities_items_milestones_id = ? and accounts_is_locked = 0', array($entities_items_milestones_id));

        foreach($additionalRecipients as $recipient) {
            if(in_array($recipient['entities_items_milestones_additional_recipients_email'], $emailAddresses)) {
                continue;
            }
            $emailMembers[] = array(
                'members_email' => $recipient['entities_items_milestones_additional_recipients_email']
            );
        }

        return $emailMembers;
               
   }
    static function get_next_milestone_for_entity_item($entities_items_id){
        $db = mysqli_db::init();
        $entities_items_milestones = $db->fetch_one('
            SELECT *,DATEDIFF(entities_items_milestones_date,CURDATE()) as days
            FROM entities_items_milestones
            WHERE
            join_entities_items_id = ? and DATEDIFF(entities_items_milestones_date,CURDATE()) >= 0 order by days ASC ',
            array($entities_items_id));
        if(!empty($entities_items_milestones)){
            return $entities_items_milestones;
        }else{
            return null;
        }          
    }
    static function get_next_milestone_for_entity($entities_id){
        $db = mysqli_db::init();
        $entities_milestones = $db->fetch_one('
            SELECT *,DATEDIFF(entities_items_milestones_date,CURDATE()) as days
            FROM  entities  
            JOIN entities_items on(join_entities_id = entities_id )
            JOIN entities_items_milestones  ON (join_entities_items_id = entities_items_id )
            WHERE
            join_entities_id = ? and DATEDIFF(entities_items_milestones_date,CURDATE()) >= 0 order by days ASC ',
            array($entities_id));
        if(!empty($entities_milestones)){
            return $entities_milestones;
        }else{
            return null;
        } 
    }
    static function delete_milestones_remind_days_by_entities_items_id($entities_items_id){
        $db = mysqli_db::init();
        $db->query('DELETE from entities_items_milestones_remind_days WHERE '
                . 'join_entities_items_id = ?',array($entities_items_id));
    }
    static function delete_milestones_additional_recipients_by_entities_items_id($entities_items_id){
        $db = mysqli_db::init();
        $db->query('DELETE from entities_items_milestones_additional_recipients WHERE '
                . 'join_entities_items_id = ?',array($entities_items_id));
    }
    public static function get_entities_items_milestones_remind_days($entities_items_milestones_id)
    { 
        $db = mysqli_db::init();
        $entities_items_milestones_remind_days = $db->fetch_singlets('
            SELECT entities_items_milestones_remind_days
            FROM entities_items_milestones_remind_days
            WHERE
            join_entities_items_milestones_id = ?',
            array($entities_items_milestones_id));
        return($entities_items_milestones_remind_days);
    }
    public static function get_entities_items_milestones_remind_members($entities_items_milestones_id)
    { 
        $db = mysqli_db::init();
        $entities_items_milestones_remind_members = $db->fetch_singlets('
            SELECT join_members_id
            FROM entities_items_milestones_remind_members
            WHERE
            join_entities_items_milestones_id = ?',
            array($entities_items_milestones_id));
        return($entities_items_milestones_remind_members);
    }
    public static function get_entities_items_milestones_additional_recipients($entities_items_milestones_id)
    { 
        $db = mysqli_db::init();
        $entities_items_milestones_additional_recipients = $db->fetch_all('
            SELECT *
            FROM entities_items_milestones_additional_recipients
            WHERE
            join_entities_items_milestones_id = ?',
            array($entities_items_milestones_id));
        return($entities_items_milestones_additional_recipients);
    }

    static  function get_milestone_members_for_business_entity($entities_items_id){
        $db = mysqli_db::init();
        $milestone_members = $db->fetch_all("
            SELECT *
            FROM entities_items_milestones_remind_members 
            WHERE join_entities_items_id = ? ", array($entities_items_id));
        return $milestone_members;
    }
    static function checkadditional_recipient($entities_items_milestones_id,$additional_recipients_email){
        $db = mysqli_db::init();
        $milestone_members = $db->fetch_all("
            SELECT *
            FROM entities_items_milestones_additional_recipients 
            WHERE join_entities_items_milestones_id = ? and entities_items_milestones_additional_recipients_email = ?", array($entities_items_milestones_id,$additional_recipients_email));
        return $milestone_members;
    }
    static function remove_remind_members($entities_items_milestones_id,$members_id){
       $db = mysqli_db::init();
       $db->query("DELETE FROM entities_items_milestones_remind_members where join_entities_items_milestones_id = ? and join_members_id =?",array($entities_items_milestones_id,$members_id));
    }
}