<?
final class entities_items_history
{
    public static function create_entity_items_entry($members_id, $entities_id, $entities_history_description)
    {
        $entities_history_table = new mysqli_db_table('entities_items_history');

        $entities_history = array('join_members_id' => $members_id,
                                'join_entities_items_id' => $entities_id,
                                'entities_items_history_datetime' => date('Y-m-d H:i:s'),
                                'entities_items_history_description' => $entities_history_description);

        $entities_history_table->insert($entities_history);
        $entities_history_id = $entities_history_table->last_id();

        return $entities_history_id;
    }

    public static function new_entity_entry($members_id, $entities_id)
    {
        return entities_items_history::create_entity_items_entry($members_id, $entities_id, 'Entity item created.');
    }


    private static function format_date($date) {
        return empty($date) ? 'null' : date('n/j/Y', strtotime($date));
    }

    public static function entity_updated_entry($members_id, $entity, $old_entity)
    {
        if($entity && $old_entity)
        {
            $entities_id = $entity['entities_items_id'];
            if($entity['entities_items_name'] != $old_entity['entities_items_name'])
            {
                $history_text = 'Entity item name changed from '.$old_entity['entities_items_name'].' to '.$entity['entities_items_name'];
                entities_items_history::create_entity_items_entry($members_id, $entities_id,$history_text );
            }
            if($entity['entities_items_details'] != $old_entity['entities_items_details'])
            {
                $history_text = 'Entity item details changed from '.$old_entity['entities_items_details'].' to '.$entity['entities_items_details'];
                entities_items_history::create_entity_items_entry($members_id, $entities_id,$history_text );
            }
            if($entity['entities_items_effactive_date'] != $old_entity['entities_items_effactive_date'])
            {
                $history_text = 'Entity effactive date changed from '.$old_entity['entities_items_effactive_date'].' to '.$entity['entities_items_effactive_date'];
                entities_items_history::create_entity_items_entry($members_id, $entities_id,$history_text );
            }
           
        }
    }

    public static function hidden_changed($members_id, $entities_id, $new_value)
    {
        $history_text = "<b>Entity " . ( $new_value ? "Hidden" : "Unhidden" ) . "</b>";

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }

    public static function file_added_entry($members_id, $entities_id,  $file_name)
    {
        $history_text = "<b>File Added: </b>" . $file_name;

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }

    public static function file_deleted_entry($members_id, $entities_id, $file_name)
    {
        $history_text = "<b>File Deleted: </b>" . $file_name;

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }

    public static function note_added_entry($members_id, $entities_id, $entities_note)
    {
        $history_text = "<b>Note Added:</b> ". $entities_note['entities_items_notes_subject'];

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }

    public static function note_updated_entry($members_id, $entities_id, $entities_note,$entities_items_note_new)
    {
        if($entities_note['entities_items_notes_subject'] != $entities_items_note_new['entities_items_notes_subject'] || 
        $entities_note['entities_items_notes_note'] != $entities_items_note_new['entities_items_notes_note']) {
            $history_text = "<b>Track Note Updated:</b> ". $entities_note['entities_items_notes_subject'];
            return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
        }
    }

    public static function note_deleted_entry($members_id, $entities_id, $entities_note_name)
    {
        $history_text = "<b>Note Deleted: </b>". $entities_note_name;

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }
    public static function item_deleted_entry($members_id, $entities_id, $entities_item_name)
    {
        $history_text = "<b>Track Deleted: </b>". $entities_item_name;

        return entities_items_history::create_entity_items_entry($members_id, $entities_id, $history_text);
    }

    public static function get_entities_items_history_with_data($members_id, $entities_items_id)
    {
        $db = mysqli_db::init();

        $entities_history = $db->fetch_all('SELECT * FROM entities_items_history WHERE join_entities_items_id = ?', array($entities_items_id));

        return $entities_history;
    }
}
?>