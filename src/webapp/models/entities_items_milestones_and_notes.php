<?php

class entities_items_milestones_and_notes {

    public static function addUpdateMilestone($entities_items_id,$existing_milestone_ids,$data){
        $entities_items_milestones = new mysqli_db_table('entities_items_milestones');
        $entities_items_milestones_remind_members_table = new mysqli_db_table('entities_items_milestones_remind_members');
        $entities_items_milestones_remind_days_table = new mysqli_db_table('entities_items_milestones_remind_days');
        $entities_items_milestones_additional_recipients_table = new mysqli_db_table('entities_items_milestones_additional_recipients');
        
        $entities_items_milestones_remind_days = array();
        $entities_items_milestones_remind_members = array();
        $entities_items_milestones_additional_recipients = array();
        $entities_items_milestones_array = array();
        
        foreach($data['entities_items_milestones_id'] as $key => $entities_items_milestones_id){
            $entities_items_milestones_array[$key]['entities_items_milestones_id'] = $entities_items_milestones_id;
        }
        foreach($data['entities_items_milestones_date'] as $key => $entities_items_milestones_date){
            $entities_items_milestones_array[$key]['entities_items_milestones_date'] = date('Y-m-d', strtotime($entities_items_milestones_date));
        }
        foreach($data['entities_items_milestones_description'] as $key => $entities_items_milestones_description){
            $entities_items_milestones_array[$key]['entities_items_milestones_description'] = $entities_items_milestones_description;
        }
        $milestone_updated = array();
        foreach($entities_items_milestones_array as $key => $entities_items_milestone){
            $entities_items_milestone['join_entities_items_id'] = $entities_items_id;
            if(empty($entities_items_milestone['entities_items_milestones_id'])){
                $entities_items_milestone['entities_items_milestones_id'] = '';
                if(!empty($entities_items_milestone['entities_items_milestones_date']) && !empty($entities_items_milestone['entities_items_milestones_description'])){
                    $entities_items_milestones->insert($entities_items_milestone);
                    $entities_items_milestones_id = $entities_items_milestones->last_id();
                }
            }else{
               if(!empty($entities_items_milestone['entities_items_milestones_date']) && !empty($entities_items_milestone['entities_items_milestones_description'])){
                    $entities_items_milestones_id = $entities_items_milestone['entities_items_milestones_id'];
                    $milestone_updated[] =  $entities_items_milestones_id;
                    $entities_items_milestones->update($entities_items_milestone,$entities_items_milestones_id);
               }
            }
            foreach ($data["entities_items_milestones_remind_days_$key"] as $key1 => $milestones_remind_days ){
                $entities_items_milestones_remind_days[$key][$key1]['join_entities_items_id'] = $entities_items_id;
                $entities_items_milestones_remind_days[$key][$key1]['join_entities_items_milestones_id'] = $entities_items_milestones_id;
                $entities_items_milestones_remind_days[$key][$key1]['entities_items_milestones_remind_days'] = $milestones_remind_days;
            }
            foreach ($data["entities_items_milestones_remind_members_$key"] as $key1 => $milestone_remind_members ){
                $entities_items_milestones_remind_members[$key][$key1]['join_entities_items_id'] = $entities_items_id;
                $entities_items_milestones_remind_members[$key][$key1]['join_entities_items_milestones_id'] = $entities_items_milestones_id;
                $entities_items_milestones_remind_members[$key][$key1]['join_members_id'] = $milestone_remind_members;
            }
            foreach ($data["entities_items_milestones_additional_recipients_email_$key"] as $key1 => $milestones_additional_recipients_email ){
                if($milestones_additional_recipients_email != ''){
                    $entities_items_milestones_additional_recipients[$key][$key1]['join_entities_items_id'] = $entities_items_id;
                    $entities_items_milestones_additional_recipients[$key][$key1]['join_entities_items_milestones_id'] = $entities_items_milestones_id;
                    $entities_items_milestones_additional_recipients[$key][$key1]['entities_items_milestones_additional_recipients_email'] = $milestones_additional_recipients_email;
                }
            }
        }

        entities_items::delete_milestones_remind_days_by_entities_items_id($entities_items_id);
        foreach($entities_items_milestones_remind_days as $key => $milestones_remind_days){
            foreach($milestones_remind_days as $remind_days){
               $entities_items_milestones_remind_days_table->insert($remind_days);
            }
        }
        entities_items::delete_milestones_remind_members_by_entities_items_id($entities_items_id);
        foreach($entities_items_milestones_remind_members as $key => $milestone_remind_members){
            foreach($milestone_remind_members as $remind_members){
               $entities_items_milestones_remind_members_table->insert($remind_members);
            }
        }
        entities_items::delete_milestones_additional_recipients_by_entities_items_id($entities_items_id);
        foreach($entities_items_milestones_additional_recipients as $key => $milestones_additional_recipients){
            foreach($milestones_additional_recipients as $additional_recipients){
               $entities_items_milestones_additional_recipients_table->insert($additional_recipients);
            }
        }
        $deleted_milestones = array_diff($existing_milestone_ids,$milestone_updated);
        if(count($deleted_milestones) > 0){
            entities_items::delete_entities_items_milestones_by_id($deleted_milestones);
        }
    }
    public static function addUpdateNotes($entitiesItemsId, $existingNoteArray, $data)
    {
        $existingNotes = array();
        foreach ($existingNoteArray as $note){
            $existingNotes[$note['entities_items_notes_id']] = $note;
        }

        $entity_items_notes = new mysqli_db_table('entities_items_notes');
        $trackItemNotesArray = array();
        foreach ($data['entities_items_notes_id'] as $key => $entities_items_notes_id) {
            $trackItemNotesArray[$key]['entities_items_notes_id'] = $entities_items_notes_id;
        }
        foreach ($data['entities_items_notes_subject'] as $key => $entities_items_notes_subject) {
            $trackItemNotesArray[$key]['entities_items_notes_subject'] = $entities_items_notes_subject;
        }
        foreach ($data['entities_items_notes_note'] as $key => $entities_items_notes_note) {
            $trackItemNotesArray[$key]['entities_items_notes_note'] = $entities_items_notes_note;
        }
        $notesPresent = array();
        foreach ($trackItemNotesArray as $trackItemNotes) {
            if (!empty($trackItemNotes['entities_items_notes_subject'])
                && !empty($trackItemNotes['entities_items_notes_note'])) {

                if (empty($trackItemNotes['entities_items_notes_id'])) {
                    $trackItemNotes['entities_items_notes_id'] = '';
                    $trackItemNotes['join_entities_items_id'] = $entitiesItemsId;
                    $trackItemNotes['entities_items_notes_datetime'] = date('Y-m-d H:i:s');
                    $entity_items_notes->insert($trackItemNotes);
                } else {
                    $existingNote = $existingNotes[$trackItemNotes['entities_items_notes_id']];
                    if ($existingNote &&
                        ($existingNote['entities_items_notes_subject'] != $trackItemNotes['entities_items_notes_subject'] ||
                            $existingNote['entities_items_notes_note'] != $trackItemNotes['entities_items_notes_note'])) {
                        $trackItemNotes['entities_items_notes_datetime'] = date('Y-m-d H:i:s');
                        $entity_items_notes->update($trackItemNotes, $trackItemNotes['entities_items_notes_id']);
                    }
                    unset($existingNotes[$trackItemNotes['entities_items_notes_id']]);
                }
            }
        }
        $deletedNotes = array_diff(array_keys($existingNotes), $notesPresent);
        if (count($deletedNotes) > 0) {
            entities_items::delete_item_notes_by_id($deletedNotes);
        }
    }
}