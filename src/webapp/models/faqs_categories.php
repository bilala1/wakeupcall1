<?php

final class faqs_categories {
    
    static function get_categories_tree($parent_id = 0, $level = 0, $omit_id = -1, $show_hidden = true){
        if($level >= 30) return array();      //infinite recursion protection
        
        $db = mysqli_db::init();
        
        $wheres = array();
        $params = array();
        
        $wheres[] = 'fc.join_faqs_categories_id = ?';
        $params[] = $parent_id;
        
        $wheres[] = 'faqs_categories_id != ?';
        $params[] = $omit_id;
        
        // ignore unpublished
        $wheres[] = '(faqs_published = 1 OR faqs_published IS NULL)';
        
        /*
        if (!$show_hidden) {
        	$wheres[] = 'faqs_categories_visible = 1';
        }
        */
        
        $faqs_categories = $db->fetch_all('
            SELECT fc.*, "'.$db->filter($level).'" AS sub_level, COUNT(DISTINCT f.faqs_id) as num_faqs
            FROM faqs_categories as fc
            LEFT JOIN faqs as f on f.join_faqs_categories_id = fc.faqs_categories_id' .
            strings::where($wheres) . '
            GROUP BY fc.faqs_categories_id', $params); 
       //echo $db->debug().'<br />';
           
        $all_faqs_categories = array();
        foreach($faqs_categories as $faqs_category){
            $all_faqs_categories[] = $faqs_category;
            $all_faqs_categories = array_merge($all_faqs_categories, self::get_categories_tree($faqs_category['faqs_categories_id'], $level + 1, $omit_id));
        }
        
        return $all_faqs_categories;
    }
    
    static function get_category_name($faqs_categories_id){
        $db = mysqli_db::init();
        
        return $db->fetch_singlet('select faqs_categories_name from faqs_categories where faqs_categories_id = ?', array($faqs_categories_id));
    }
}
