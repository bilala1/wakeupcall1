<?

final class forums extends mysqli_db_table{

    function __construct()
    {
        parent::__construct('forums');
    }

    public function get_forums($parent_id){
        $db = mysqli_db::init();
        $parent_id = (int) $parent_id;

        $forums = $db->fetch_all('SELECT * FROM forums WHERE join_forums_id_parent = ?', array($parent_id));

        // Find children forums... (subs)
        foreach($forums as &$forum){
            $forum['children'] = $this->get_forums($forum['forums_id']);
            //$forum['num_topics']   = $this->get_topics_total($forum['forums_id']);
            $forum['topics'] = $this->get_topics($forum['forums_id'], 0, 999);

            unset($forum);
        }

        return $forums;
    }

    public function get_all_forums(){
        $db = mysqli_db::init();
        $forums = $db->fetch_all('SELECT * FROM forums ORDER BY forums_name');
        return $forums;
    }

    public function get_forum($forums_id){
        $db = mysqli_db::init();

        $forum = $db->fetch_one('SELECT * FROM forums WHERE forums_id = ?', array($forums_id));

        return $forum;
    }

    public function get_topics($forums_id, $start, $perpage, $show_unapproved = false){
        $db = mysqli_db::init();

        $approved_query = $show_unapproved ? '' : ' and topics_approved = 1';

        $topics = $db->fetch_all('
            SELECT t.*, m.members_id, ftv.forums_topics_visits_datetime AS topic_visit_datetime
            FROM forums_topics AS t
            LEFT JOIN members AS m ON m.members_id = t.join_members_id
            LEFT JOIN forums_topics_visits ftv ON(ftv.join_forums_topics_id = t.topics_id AND ftv.join_members_id = ?)
            WHERE join_forums_id = ?' . $approved_query . '
            GROUP BY t.topics_id
            ORDER BY topics_stickied desc, t.topics_last_posts_date DESC
            LIMIT '.$start. ', '.$perpage, array(ActiveMemberInfo::GetMemberId(), $forums_id));
        
        foreach($topics as &$topic){
            $topic['topics_replies'] = $db->fetch_singlet('
                SELECT COUNT(*)
                FROM forums_posts
                WHERE
                    join_topics_id = ? and
                    posts_approved = 1 and
                    posts_hidden = 0 ', array($topic['topics_id'])) - 1; // subtract 1 because the first post doesn't count as a reply

            $topic['last_post'] = $db->fetch_one('
                SELECT p.*, m.members_id, CONCAT(m.members_firstname, " ", m.members_lastname) as members_username
                FROM forums_posts AS p
                LEFT JOIN members AS m ON m.members_id = p.join_members_id
                WHERE p.posts_id = ?', array($topic['topics_last_posts_id']));
            
            //$topic['not_read'] = (strtotime($_SESSION['last_visit']) < strtotime($topic['topics_last_posts_date']) and $topic['last_post']['join_members_id'] != ActiveMemberInfo::GetMemberId()) ? true : false;
            $topic['not_read'] = (strlen($topic['topic_visit_datetime']) > 0 && 
                                  (strtotime($topic['topic_visit_datetime']) > strtotime($topic['topics_last_posts_date'])) ||
                                 $topic['last_post']['join_members_id'] == ActiveMemberInfo::GetMemberId()) ? false : true;

			if ($forums_id != 1 && !self::is_in_message_forum($forums_id)) {
				$topic['has_attachments'] = self::topic_attachment($topic['topics_id']);
			} else {
				$topic['has_attachments'] = false;
			}

            $member = members::get_by_id($topic['join_members_id']);
            if($member){
                $topic = array_merge($topic, $member);
            }
        }
        unset($topic);

        return $topics;
    }

	public static function topic_attachment($topics_id) {
		$db = mysqli_db::init();

		$attachments = $db->fetch_singlet('
			SELECT COUNT(*) FROM forums_posts_files
			WHERE join_posts_id IN (
				SELECT posts_id FROM forums_posts
				WHERE join_topics_id = ?
			)
		', array($topics_id));

		return !empty($attachments);
	}

    public function get_topics_total($forums_id){
        $db = mysqli_db::init();

        $total = $db->fetch_singlet('SELECT COUNT(*) FROM forums_topics WHERE join_forums_id = ?', array($forums_id));

        return $total;
    }

    public function get_posts_total($topics_id, $show_unapproved = false){
        $db = mysqli_db::init();

        if($show_unapproved){
            $total = $db->fetch_singlet('SELECT COUNT(*) FROM forums_posts WHERE join_topics_id = ? AND posts_hidden = 0', array($topics_id));
        }else{
            $total = $db->fetch_singlet('SELECT COUNT(*) FROM forums_posts WHERE join_topics_id = ? AND posts_hidden = 0 and posts_approved = 1', array($topics_id));
        }
        return $total;
    }

    public function get_posts($topics_id, $start, $perpage, $show_unapproved = false){
        $db = mysqli_db::init();

        $approved_query = $show_unapproved ? '' : ' and p.posts_approved = 1';

        $posts = $db->fetch_all('
            SELECT
                p.*,
                m.members_id,
                CONCAT(m2.members_firstname, " ", m2.members_lastname) as editor
            FROM forums_posts AS p
            LEFT JOIN members AS m ON m.members_id = p.join_members_id
            LEFT JOIN members AS m2 ON m2.members_id = p.posts_modified_join_members_id
            WHERE p.join_topics_id = ? ' . $approved_query . '
            AND posts_hidden = 0   
            GROUP BY p.posts_id
            LIMIT '.$start. ', '.$perpage, array($topics_id));

        foreach($posts as &$post){
            $member = members::get_by_id($post['join_members_id']);
            $post['enabled'] = (array) unserialize($post['posts_enabled']);

            $post['tags'] = tags::parse_tags($post['posts_tags']);
            $post['files'] = $this->get_files($post['posts_id']);
            $post = array_merge($post, $member);

            unset($post);
        }

        return $posts;
    }

    public function get_posts_by_tag($tags_id, $start, $perpage){
        $db = mysqli_db::init();

        $posts = $db->fetch_all('
            SELECT p.*, t.topics_id, t.topics_title
            FROM forums_posts AS p
            LEFT JOIN forums_topics AS t ON t.topics_id = p.join_topics_id
            WHERE
                FIND_IN_SET(?, p.posts_tags)
            GROUP BY p.posts_id
            LIMIT '.(int)$start. ', '.(int)$perpage, array($tags_id));

        foreach($posts as &$post){
            $member = members::get_by_id($post['join_members_id']);
            $post['enabled'] = (array) unserialize($post['posts_enabled']);

            $post['tags'] = tags::parse_tags($post['posts_tags']);
            $post['files'] = $this->get_files($post['posts_id']);
            $post = array_merge($post, $member);

            unset($post);
        }

        return $posts;
    }

    public function get_posts_by_forum_tag($forums_id, $tags_id, $start, $perpage){
        $db = mysqli_db::init();

        $posts = $db->fetch_all('
            SELECT p.*, t.topics_id, t.topics_title
            FROM forums_posts AS p
            LEFT JOIN forums_topics AS t ON t.topics_id = p.join_topics_id
            WHERE
                FIND_IN_SET(?, p.posts_tags) and
                join_forums_id = ?
            GROUP BY p.posts_id
            LIMIT '.(int)$start. ', '.(int)$perpage, array($tags_id, $forums_id));

        foreach($posts as &$post){
            $member = members::get_by_id($post['join_members_id']);
            $post['enabled'] = (array) unserialize($post['posts_enabled']);

            $post['tags'] = tags::parse_tags($post['posts_tags']);
            $post['files'] = $this->get_files($post['posts_id']);
            $post = array_merge($post, $member);

            unset($post);
        }

        return $posts;
    }

    public static function get_recent_posts($limit = 10, $forums_id, $members_id = 0, $just_public = false){
        $db = mysqli_db::init();

        $where  = array();
        $params = array();

        //if($forums_id){
            $where  []= '(t.join_forums_id = ? || f.join_forums_id_parent = ?)';
            $params []= $forums_id;
            $params []= $forums_id;
        //}

        if($members_id){
            $where  []= 'm.members_id = ?';
            $params []= $members_id;
        }

        if($just_public){
            $where  []= 'f.forums_public_permission_view = ?';
            $params []= 1;
        }

        $where[] = 'posts_approved = 1';

        $posts = $db->fetch_all('
            SELECT p.*, t.topics_title, t.topics_id, f.forums_id, f.forums_name, m.members_id,
                CONCAT(m.members_firstname, " ", members_lastname) as members_username
            FROM forums_posts AS p
            LEFT JOIN forums_topics AS t ON t.topics_id = p.join_topics_id
            LEFT JOIN forums AS f ON f.forums_id = t.join_forums_id
            LEFT JOIN members AS m ON m.members_id = p.join_members_id
            '.strings::where($where).'
            GROUP BY p.posts_id
            ORDER BY p.posts_postdate DESC
            LIMIT '.$db->filter((int) $limit), $params);

        return $posts;
    }

    public function get_forum_parent($parent_id){
        $db = mysqli_db::init();

        $parent = $db->fetch_one('SELECT * FROM forums WHERE forums_id = ?', array($parent_id));
        if($parent['join_forums_id_parent']){
            $parent['parent'] = $this->get_forum_parent($parent['join_forums_id_parent']);
        }

        return $parent;
    }

    public function get_topic_breadcrumb($topics_id){
        $db = mysqli_db::init();
        $topic = $db->fetch_one('SELECT * FROM forums_topics WHERE topics_id = ?', array($topics_id));
        $crumbs []= array(
            'str' => $topic['topics_title']
        );

        $forum = $parent = $this->get_forum_parent($topic['join_forums_id']);
        $crumbs []= array(
            'page_url' => FULLURL .'/members/forums/view-forum.php?forums_id='.$parent['forums_id'],
            'str' => $forum['forums_name']
        );

        while(1){
            if(!$parent['parent']){ // if no parent, stop the loop...
                break;
            }
            $parent   = $parent['parent'];

            $crumbs []= array(
                'page_url' => FULLURL .'/members/forums/view-forum.php?forums_id='.$parent['forums_id'],
                'str'      => $parent['forums_name']
            );
        }

        return array_reverse($crumbs);
    }

    public function get_forum_breadcrumb($forums_id){
        $db = mysqli_db::init();

        $forum = $parent = $this->get_forum_parent($forums_id);
        $crumbs []= array(
            'page_url' => $parent['join_forums_id_parent'] ? FULLURL .'/members/forums/view-forum.php?forums_id='.$parent['forums_id'] : null,
            'str' => $forum['forums_name']
        );
        while(1){
            if(!$parent['parent']){ // if no parent, stop the loop...
                break;
            }
            $parent = $parent['parent'];

            $crumbs []= array(
                'page_url' => $parent['join_forums_id_parent'] ? FULLURL .'/members/forums/view-forum.php?forums_id='.$parent['forums_id'] : null,
                'str'      => $parent['forums_name']
            );
        }

        return array_reverse($crumbs);
    }

    public function get_forum_breadcrumb_admin($forums_id){
        $db = mysqli_db::init();

        $forum = $parent = $this->get_forum_parent($forums_id);
        $crumbs []= array(
            'page_url' => FULLURL .'/admin/forums/list.php?parent_id='.$parent['forums_id'],
            'str' => $forum['forums_name']
        );
        while(1){
            if(!$parent['parent']){ // if no parent, stop the loop...
                break;
            }
            $parent = $parent['parent'];

            $crumbs []= array(
                'page_url' => FULLURL .'/admin/forums/list.php?parent_id='.$parent['forums_id'],
                'str'      => $parent['forums_name']
            );
        }

        return array_reverse($crumbs);
    }

    public function build_trail($crumbs, $spacer = ' » '){
        $container = html::element('span');
        $container->set('class', 'breadcrumb');

        foreach($crumbs as $crumb){
            $thisspacer = html::element('span');
            $thisspacer->set('class', 'breadcrumb-spacer');
            $thisspacer->set('html', $spacer);
            $thisspacer->inject($container);

            if($crumb['str'] and !$crumb['page_url']){
                $thiscrumb = html::element('span');
                $thiscrumb->set('html', $crumb['str']);
                $thiscrumb->inject($container);
            }
            else {
                $thiscrumb = html::element('a');
                $thiscrumb->set('href', $crumb['page_url'])
                    ->set('html', $crumb['str']);
                $thiscrumb->inject($container);
            }
        }

        return $container->to_html();
    }

    public function get_topic($topics_id){
        $db = mysqli_db::init();

        $topic = $db->fetch_one('
            SELECT t.*, p.*, MIN(p.posts_id) AS first_post
            FROM forums_topics AS t
            LEFT JOIN forums_posts AS p ON p.join_topics_id = t.topics_id
            WHERE t.topics_id = ?', array($topics_id));

        $topic['settings'] = unserialize($topic['posts_enabled']);

        return $topic;
    }

    public function get_groups($forums_id){
        $db = mysqli_db::init();

        //HEY! If you see muliple listings of each group when editting a forum, do not use group by, check forums_groups_x_forums for join_forums_id = 0
        //OK, so they were right ^ (probably me), but why is this happening? idk...
        $groups = $db->fetch_all('
            SELECT g.*, gxf.*, f.*,
                gxf2.forums_groups_x_forums_permission_view AS parent_view,
                gxf2.forums_groups_x_forums_permission_reply AS parent_reply,
                gxf2.forums_groups_x_forums_permission_new_topic AS parent_new_topic
            FROM forums_groups AS g
            LEFT JOIN forums_groups_x_forums as gxf ON
                gxf.join_groups_id = g.groups_id AND
                gxf.join_forums_id = ?
            LEFT JOIN forums AS f ON f.forums_id = gxf.join_forums_id
            LEFT JOIN forums_groups_x_forums AS gxf2 ON
                gxf2.join_forums_id = f.join_forums_id_parent AND
                gxf2.join_groups_id = g.groups_id
            ORDER BY g.groups_name', array($forums_id));

        return $groups;
    }

    //given a forum, creates permissions for a group(s) if it does not already exist, using forums' public permissions
    public function sync_groups($forums_id){
        $db = mysqli_db::init();

        $forum = $db->fetch_one('SELECT * FROM forums WHERE forums_id = ?', array($forums_id));

        $no_permission_groups = $db->fetch_all('
            SELECT g.*
            FROM forums_groups AS g
            LEFT JOIN forums_groups_x_forums as fxf ON
                fxf.join_groups_id = g.groups_id AND
                fxf.join_forums_id = ?
            WHERE fxf.join_groups_id IS NULL', array($forums_id));

        $forum_permissions_table = new mysqli_db_table('forums_groups_x_forums');

        foreach($no_permission_groups as $group){
            $forum_permissions_table->insert(array(
                'join_groups_id' => $group['groups_id'],
                'join_forums_id' => $forums_id,
                'forums_groups_x_forums_permission_view' => $forum['forums_public_permission_view'],
                'forums_groups_x_forums_permission_reply' => $forum['forums_public_permission_reply'],
                'forums_groups_x_forums_permission_new_topic' => $forum['forums_public_permission_new_topic']));
        }
    }

    public static function get_forum_id_by_topic($topics_id){
        $db = mysqli_db::init();

        return $db->fetch_singlet('SELECT join_forums_id FROM forums_topics AS ft WHERE ft.topics_id = ?', array($topics_id));
    }

    public static function get_forum_id_by_post($posts_id){
        $db = mysqli_db::init();
        $forum_id = $db->fetch_singlet('SELECT join_forums_id
                                        FROM forums_posts AS fp
                                        LEFT JOIN forums_topics AS ft ON ( fp.join_topics_id = ft.topics_id )
                                        WHERE fp.posts_id = ? ', array($posts_id));
        return $forum_id;
    }

    public function get_topic_by_post($post_id){
        $db = mysqli_db::init();

        return $db->fetch_one('SELECT ft.* FROM forums_topics AS ft JOIN forums_posts AS fp ON fp.join_topics_id = ft. topics_id WHERE fp.posts_id = ?', array($post_id));
    }

    public function get_files($forums_id){
        $db = mysqli_db::init();

        return $db->fetch_all('
            SELECT *
            FROM forums_posts_files AS pf
            WHERE pf.join_posts_id = ?
            ORDER BY forums_posts_files_datetime', array($forums_id));
    }

    public function delete_post($posts_id){
        $db = mysqli_db::init();

        //delete file too?
        return $db->query('DELETE FROM forums_posts WHERE posts_id = ?', array($posts_id));
    }

    /*array(18) {
        ["forums_id"]=> int(1)
        ["join_forums_id_parent"]=> int(0)
        ["forums_name"]=> string(13) "Flying Things"
        ["subforums"]=> array(4) {
          array(18) {
            ["forums_id"]=> int(4)
            ["join_forums_id_parent"]=> int(1)
            ["forums_name"]=> string(5) "Birds"
            ["subforums"]=> array(1) {
              rray(17) {
                ["forums_id"]=> int(10)
                ["join_forums_id_parent"]=> int(4)
                ["forums_name"]=> string(8) "Blue Jay"
              }
            }
          }
        }
      }*/
    public function get_subsetted_forums($parent_id){
        $db = mysqli_db::init();

        $forums = $db->fetch_all('
            SELECT *
            FROM forums AS f
            WHERE join_forums_id_parent = ?
            ORDER BY f.forums_name', array($parent_id));

        if($forums){
            foreach($forums as &$forum){
                $subforums = groups::get_subsetted_forums($forum['forums_id']);
                if($subforums){
                    $forum['subforums'] = $subforums;
                }
            }

            return $forums;
        }else{
            return false;
        }
    }

    //get forums unsubsetted, but their level of subness
    public static function get_forums_levels($parent_id,$level = 0, $orderby = 'f.forums_name'){
        $db = mysqli_db::init();

        $forums = $db->fetch_all('
            SELECT *, COUNT(DISTINCT t.topics_id) AS forums_topics
            FROM forums AS f
            LEFT JOIN forums_topics AS t ON
                t.join_forums_id = f.forums_id AND
                t.topics_approved = 1
            WHERE join_forums_id_parent = ?
            GROUP BY f.forums_id
            ORDER BY ' . $orderby, array($parent_id));

        if($forums){
            for($i=count($forums)-1; $i>=0; $i--){          //loop through backwards so array_splice doesn't screw up loop
                $forums[$i]['sub_level'] = $level;
                if($subforums = forums::get_forums_levels($forums[$i]['forums_id'],$level+1,$orderby)){
                    array_splice($forums,$i+1,0,$subforums);
                }
            }

            return $forums;
        }else{
            return false;
        }
    }

    //gets the first post of the topic (the one that started it)
    public static function get_topic_post($topics_id){
        $db = mysqli_db::init();

        return $db->fetch_one('select * from forums_posts where join_topics_id = ? having min(posts_postdate) = posts_postdate', array($topics_id));
    }

    public static function post_needs_approval($forums_id){
        $db = mysqli_db::init();

        return $db->fetch_singlet('select forums_post_approval from forums where forums_id = ?', array($forums_id));
    }

    public static function topic_needs_approval($forums_id){
        $db = mysqli_db::init();

        return $db->fetch_singlet('select forums_topic_approval from forums where forums_id = ?', array($forums_id));
    }

    public static function approve_topic($topics_id){
        $db = mysqli_db::init();

        $db->query('UPDATE forums_topics SET topics_approved = 1 WHERE topics_id = ?', array($topics_id));

        //approve first post too
        $post = forums::get_topic_post($topics_id);
        return forums::approve_post($post['posts_id']);
    }

    public static function approve_post($posts_id){
        $db = mysqli_db::init();

        return $db->query('UPDATE forums_posts SET posts_approved = 1 WHERE posts_id = ?', array($posts_id));
    }

    //specific to wakeupcall
    public static function is_in_message_forum($forums_id){
        if($forums_id == 1){
            return true;
        }

        $db = mysqli_db::init();

        $parent_id = $db->fetch_singlet('select join_forums_id_parent from forums where forums_id = ?', array($forums_id));

        return ($parent_id == 1);
    }

    //top topic = most posts in last week
    public static function get_top_topics($forums_id, $num = 5){
        $db = mysqli_db::init();

        $topics = $db->fetch_all('
            select forums_topics.*
            from forums_topics
            join forums_posts on join_topics_id = topics_id
            where
                join_forums_id = ? and
                posts_postdate > NOW() - INTERVAL 1 WEEK
            group by join_topics_id
            order by count(join_topics_id) desc, topics_last_posts_date
            limit ?',
            array($forums_id, $num)
        );

        return $topics;
    }

    //top topic = most posts in last week
    public static function get_top_wakeupcall_topics($num = 5){
        $db = mysqli_db::init();

        $topics = $db->fetch_all('
            select forums_topics.*, CONCAT_WS(" ", members_firstname, members_lastname) AS members_name
            from forums_topics
            join forums on forums_id = join_forums_id
            join forums_posts on join_topics_id = topics_id
            LEFT JOIN members ON (forums_topics.join_members_id = members.members_id)
            where
                join_forums_id = 1 and
                posts_postdate > NOW() - INTERVAL 3 WEEK
            group by join_topics_id
            order by count(join_topics_id) desc, topics_last_posts_date
            limit ?',
            array($num)
        );
        
        return $topics;
    }

    //when a member gets deleted his posts remain in forum.
    //those orphan posts needs to be reassigned to different member - "Former Member"
    //
    public static function set_orphan_posts_to_former_member(){
        $db = mysqli_db::init();
        
        //there is a reason why this is NOT in the same SQL statement.
        // using forum_posts in both select and update cannot be done at the same time...
        $orphan_posts = $db->fetch_singlets('SELECT posts_id
                                             FROM forums_posts AS fp
                                             WHERE join_members_id NOT IN (SELECT members_id
                                                                           FROM members)');
        if($orphan_posts && FORUM_FORMER_MEMBER){
            foreach($orphan_posts as $post_id){
                $db->query('UPDATE forums_posts SET join_members_id = ? WHERE posts_id = ?',array(FORUM_FORMER_MEMBER,$post_id));
            }
        }
         // reassign tipics too... 
        $orphan_topics = $db->fetch_singlets('SELECT topics_id
                                              FROM forums_topics AS fp
                                              WHERE join_members_id NOT IN (SELECT members_id
                                                                            FROM members)');
        if($orphan_topics && FORUM_FORMER_MEMBER){
            foreach($orphan_topics as $topic_id){
                $db->query('UPDATE forums_topics SET join_members_id = ? WHERE topics_id = ?',array(FORUM_FORMER_MEMBER,$topic_id));
            }
        }
    }
    
    
    // creating new forum for corporations
    // on verify page for trial members, and on 
    public static function create_corporate_forum($members_id){
        // DB
        $db = mysqli_db::init();
        $corporations_table = new mysqli_db_table('corporations');
        $forums_table = new mysqli_db_table('forums');
        $forums_groups_table = new mysqli_db_table('forums_groups');
        $forums_groups_x_forums_table   = new mysqli_db_table('forums_groups_x_forums');
        $forums_groups_x_members_table  = new mysqli_db_table('forums_groups_x_members');
        
        $member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', array($members_id));
        
        $account= $db->fetch_one('SELECT * FROM accounts WHERE join_members_id = ? ', array($member['members_id']));
        
        //get corporation name
        $corporation = $db->fetch_one('SELECT * FROM corporations WHERE join_accounts_id = ? ', array($account['accounts_id']));
        if(!$account || !$corporation['corporations_id']) {
            return;
        }
        $accounts_name = $account['accounts_name'];
        
        //-- forums table
        $ft['forums_name']              =   $accounts_name;
        $ft['forums_createdate']        =   SQL('NOW()');
        $ft['forums_status']            =   'active';
        $forums_table->insert($ft);
        $forums_table_last_id           =   $forums_table->last_id();
        
        //-- forums_groups table for corporation
        $fgt['groups_name']             =   $accounts_name.' - Corporation';
        $fgt['groups_datetime']         =   SQL('NOW()');
        $fgt['join_members_id']	        =   $member['members_id'];
        $fgt['groups_type']             =   'open';
        $forums_groups_table->insert($fgt);
        $forums_groups_table_last_id_corp    =   $forums_groups_table->last_id();
        
        //-- forums_groups_x_forums table for corporation
        $fgxft['join_groups_id']        =	$forums_groups_table_last_id_corp;
        $fgxft['join_forums_id']        =   $forums_table_last_id ;
        $fgxft['forums_groups_x_forums_permission_view']        =	1;
        $fgxft['forums_groups_x_forums_permission_reply']       =   1;
        $fgxft['forums_groups_x_forums_permission_new_topic']   =   1;
        $forums_groups_x_forums_table->insert($fgxft);
        
        //-- forums_groups_x_members table for corporation
        $fgxmt['join_groups_id']        = 	$forums_groups_table_last_id_corp;
        $fgxmt['join_members_id']       =   $member['members_id'];
        $forums_groups_x_members_table->insert($fgxmt);
        
        
        //-- forums_groups table for locations
        $fgt['groups_name']             =   $accounts_name.' - Location';      //if changing, also change in hotels/edit.php and corp delete cron job script
        $fgt['groups_datetime']         =   SQL('NOW()');
        $fgt['join_members_id']	        =   $member['members_id'];
        $fgt['groups_type']             =   'open';
        $forums_groups_table->insert($fgt);
        $forums_groups_table_last_id_loc    =   $forums_groups_table->last_id();
        
        //-- forums_groups_x_forums table for locations
        $fgxft['join_groups_id']        =	$forums_groups_table_last_id_loc;
        $fgxft['join_forums_id']        =   $forums_table_last_id ;
        $fgxft['forums_groups_x_forums_permission_view']        =	1;
        $fgxft['forums_groups_x_forums_permission_reply']       =   1;
        $fgxft['forums_groups_x_forums_permission_new_topic']   =   1;
        $forums_groups_x_forums_table->insert($fgxft);
        
        //-- forums_groups_x_members table for locations
        $location_members_ids = $db->fetch_singlets('select members_id from members where join_accounts_id = ?', array($account['accounts_id']));
        foreach($location_members_ids as $members_id){
            if($members_id == $account['join_members_id']) {
                continue; // don't add account admin member to location group
            }
            $fgxmt['join_groups_id']        = 	$forums_groups_table_last_id_loc;
            $fgxmt['join_members_id']       =   $members_id;
            $forums_groups_x_members_table->insert($fgxmt);
        }
        
        $corporations_table->update(array('join_forums_id' => $forums_table_last_id), $corporation['corporations_id']);
    }

}

?>
