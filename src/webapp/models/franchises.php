<?php

class franchises{
    static function get_all(){
        $db = mysqli_db::init();

        return $db->fetch_all('select * from franchises order by franchises_name');
    }

    public static function get_franchises_for_member($members_id){
        $db = mysqli_db::init();

        return $db->fetch_all('
            SELECT *
            FROM franchises
            WHERE join_members_id = ?', array($members_id));
    }

    public static function get_franchise_for_code($franchise_code){
        $db = mysqli_db::init();

        $franchise= $db->fetch_one('
            SELECT *
            FROM franchises
            WHERE franchises_code = ?', array($franchise_code));
    }

}

?>