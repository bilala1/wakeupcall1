<?php

final class groups{
    public static function by_id($groups_id){
        $groups = new mysqli_db_table('forums_groups');
        $group = $groups->get($_REQUEST['groups_id']);
        
        return $group;
    }
    
    public static function get_forums_permissions($group_id){
        $db = mysqli_db::init();
        
        $forums = $db->fetch_all('
            SELECT *
            FROM forums AS f
            JOIN forums_groups_x_forums AS gxf ON
                gxf.join_forums_id = f.forums_id AND
                gxf.join_groups_id = ?
            ORDER BY f.forums_name', array($group_id));
        
        return $forums;
    }
    
    public static function get_subsetted_forums_permissions($parent_id,$group_id){
        $db = mysqli_db::init();
        
        $forums = $db->fetch_all('
            SELECT *
            FROM forums AS f
            JOIN forums_groups_x_forums AS gxf ON
                gxf.join_forums_id = f.forums_id AND
                gxf.join_groups_id = ?
            WHERE join_forums_id_parent = ?
            ORDER BY f.forums_name', array($group_id,$parent_id));
        
        if($forums){
            foreach($forums as &$forum){
                if($subforums = groups::get_subsetted_forums_permissions($forum['forums_id'],$group_id)){
                    $forum['subforums'] = $subforums;
                }
            }
            
            return $forums;
        }else{
            return false;
        }
    }
    
    //get forums unsubsetted, but their level of subness
    public static function get_forums_levels_permissions($parent_id,$group_id,$level = 0){
        $db = mysqli_db::init();
        
        $forums = $db->fetch_all('
            SELECT *
            FROM forums AS f
            JOIN forums_groups_x_forums AS gxf ON
                gxf.join_forums_id = f.forums_id AND
                gxf.join_groups_id = ?
            WHERE join_forums_id_parent = ?
            ORDER BY f.forums_name', array($group_id,$parent_id));
        
        if($forums){
            for($i=count($forums)-1; $i>=0; $i--){          //loop through backwards so array_splice doesn't screw up loop
                $forums[$i]['sub_level'] = $level;
                if($subforums = groups::get_forums_levels_permissions($forums[$i]['forums_id'],$group_id,$level+1)){
                    array_splice($forums,$i+1,0,$subforums);
                }
            }
            
            return $forums;
        }else{
            return false;
        }
    }
    
    //given a group, creates permissions for a forum(s) if it does not already exist, using forums' public permissions
    public static function sync_forums($groups_id){
        $db = mysqli_db::init();
        
        $no_permission_forums = $db->fetch_all('
            SELECT f.*
            FROM forums AS f
            LEFT JOIN forums_groups_x_forums as fxf ON 
                fxf.join_forums_id = f.forums_id AND
                fxf.join_groups_id = ?
            WHERE fxf.join_forums_id IS NULL', array($groups_id));
        
        $forum_permissions_table = new mysqli_db_table('forums_groups_x_forums');
        
        foreach($no_permission_forums as $forum){
            $forum_permissions_table->insert(array(
                'join_groups_id' => $groups_id,
                'join_forums_id' => $forum['forums_id'],
                'forums_groups_x_forums_permission_view' => $forum['forums_public_permission_view'],
                'forums_groups_x_forums_permission_reply' => $forum['forums_public_permission_reply'],
                'forums_groups_x_forums_permission_new_topic' => $forum['forums_public_permission_new_topic']));
        }
    }
    
    public static function get_groups(){
        $db = mysqli_db::init();
        
        $groups = $db->fetch_all('SELECT * FROM forums_groups order by groups_name');
        
        return $groups;
    }
    
    public static function update_permission($forums_id, $groups_id, $data){
        $db = mysqli_db::init();
        
        $db->query('UPDATE forums_groups_x_forums SET
            forums_groups_x_forums_permission_view = ?,
            forums_groups_x_forums_permission_reply = ?,
            forums_groups_x_forums_permission_new_topic = ?
            WHERE join_forums_id = ? AND join_groups_id = ?', array(
                $data['forums_groups_x_forums_permission_view'],
                $data['forums_groups_x_forums_permission_reply'],
                $data['forums_groups_x_forums_permission_new_topic'],
                $forums_id,
                $groups_id));
        
        //set sub forums if a permission has been disabled
        $subforums = $db->fetch_all('
            SELECT *
            FROM forums AS f
            LEFT JOIN forums_groups_x_forums AS gxf ON
                gxf.join_forums_id = f.forums_id AND
                gxf.join_groups_id = ?
            WHERE f.join_forums_id_parent = ?', array($groups_id, $forums_id));
        
        if($subforums){
            foreach($subforums as $subforum){
                //set subforum permission to 0 if parent's permission is 0
                $subforum['forums_groups_x_forums_permission_view'] *= $data['forums_groups_x_forums_permission_view'];
                $subforum['forums_groups_x_forums_permission_reply'] *= $data['forums_groups_x_forums_permission_reply'];
                $subforum['forums_groups_x_forums_permission_new_topic'] *= $data['forums_groups_x_forums_permission_new_topic'];
                
                groups::update_permission($subforum['forums_id'], $groups_id, $subforum);
            }
        }//else echo 'no subforums for '.$forums_id.'<br />';
    }
    
    public static function get_user_permissions($forums_id, $members_id = null){
        $db = mysqli_db::init();
        
        $members_id = $members_id ? $members_id : ActiveMemberInfo::GetMemberId();
        
        //get forum's public permissions
        $public_permissions = $db->fetch_one('
            SELECT
                forums_public_permission_view AS view,
                forums_public_permission_reply AS reply,
                forums_public_permission_new_topic AS new_topic
            FROM forums AS f
            WHERE f.forums_id = ?', array($forums_id));
      
        $permissions = $db->fetch_one('
            SELECT gxf.*,
                SUM(forums_groups_x_forums_permission_view) AS view,
                SUM(forums_groups_x_forums_permission_reply) AS reply,
                SUM(forums_groups_x_forums_permission_new_topic) AS new_topic
            FROM forums_groups_x_forums AS gxf
            LEFT JOIN forums_groups_x_members AS gxg ON gxg.join_groups_id = gxf.join_groups_id
            WHERE gxf.join_forums_id = ? AND gxg.join_members_id = ?', array($forums_id, $members_id));
   
        //give user at least public permissions
        $permissions['view'] += $public_permissions['view'];
        $permissions['reply'] += $public_permissions['reply'];
        $permissions['new_topic'] += $public_permissions['new_topic'];
         
        return $permissions;
    }
    
    static function get_members($groups_id){
        $db = mysqli_db::init();
        
        $members = $db->fetch_all('
            select *
            from forums_groups_x_members
            join members on members_id = join_members_id
            where join_groups_id = ?
            order by members_lastname, members_firstname',
            array($groups_id)
        );
        
        return $members;
    }
}
