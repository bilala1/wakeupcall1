<?php

class hotels{

    static function get_by_id($id){
        $db = mysqli_db::init();
        return $db->fetch_one('select * from hotels
                               where hotels_id = ? ', array($id));
    }


    static function get_by_licensed_location_id($id){
        $db = mysqli_db::init();
        return $db->fetch_one('select * from hotels
                               where licensed_locations_id = ? ', array($id));
    }

    static function get_licensed_location($hotel){
        return licensed_locations::get_by_id($hotel['hotels_id']);
    }

    static function create($new_hotel){
        mysqli_db::init();

        $hotels_table = new mysqli_db_table('hotels');
        $hotels_table->insert($new_hotel);
        $hotels_id = $hotels_table->last_id();

        return $hotels_id;
    }
}

?>