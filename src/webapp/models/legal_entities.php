<?php
class legal_entities
{ 
    static function getAllLegalEntities($show_hidden = 0){
        $db = mysqli_db::init();
        $locationIds = UserPermissions::LocationIdsWithPermission('certificates');
        $locationIds[] = 0;
        $inClause = strings::CreateInClauseForIds($locationIds, 'le.join_locations');
        
        $where = " and legal_entity_names_hidden = $show_hidden";

        $legal_entities = $db->fetch_all('select *, (select count(*)  from certificates_x_legal_entity_names cl 
                   where cl.join_legal_entity_names_id = le.legal_entity_names_id ) as certificates_count
                   from legal_entity_names le '
                . 'LEFT JOIN licensed_locations  ll ON (ll.licensed_locations_id = le.join_locations_id)'
                . ' where le.join_accounts_id = ? and ' 
                . $inClause.$where, array(ActiveMemberInfo::GetAccountId()));
        if(!$legal_entities) return null;
       
        return $legal_entities;
    }
    static function getAssignedToLocation($locationId){
         $db = mysqli_db::init();
        $locationIds[] = $locationId;
        $locationIds[] = 0;
        $inClause = strings::CreateInClauseForIds($locationIds, 'le.join_locations');

        $legal_entities = $db->fetch_all('select * from legal_entity_names le '
                . 'LEFT JOIN licensed_locations  ll ON (ll.licensed_locations_id = le.join_locations_id)'
                . ' where le.join_accounts_id = ? and ' 
                . $inClause, array(ActiveMemberInfo::GetAccountId()));
        if(!$legal_entities) return null;
       
        return $legal_entities;
    }
    static function getById($legal_entity_names_id){
         $db = mysqli_db::init();
        
        $legal_entities = $db->fetch_one('SELECT * FROM legal_entity_names le '
                . 'LEFT JOIN licensed_locations  ll ON (ll.licensed_locations_id = le.join_locations_id) WHERE '
                . 'legal_entity_names_id = ?', array($legal_entity_names_id));
        
        if(!$legal_entities) return null;
        return $legal_entities;
    }
    static function delete_by_id($legal_entity_names_id){
        $db = mysqli_db::init();
        $db->query("DELETE FROM legal_entity_names WHERE legal_entity_names_id = ?", array($legal_entity_names_id));
        $db->query("DELETE FROM certificates_x_legal_entity_names WHERE join_legal_entity_names_id = ?", array($legal_entity_names_id));
    }
    static function get_legal_entity_names_for_certificate($join_certificates_id){
        $db = mysqli_db::init();
        $legal_entities = $db->fetch_all('SELECT * FROM certificates_x_legal_entity_names cl '
                . 'JOIN legal_entity_names le ON (cl.join_legal_entity_names_id = le.legal_entity_names_id )'
                . 'WHERE cl.join_certificates_id = ?',array($join_certificates_id));
        if(!$legal_entities) return null;
        return $legal_entities;
    }
}


