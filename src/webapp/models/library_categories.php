<?php

final class library_categories {
    static function get_all(){
        $db = mysqli_db::init();
        
        return $db->fetch_all('select * from library_categories order by library_categories_name');
    }

    public static function get_categories($parent){
        $db = mysqli_db::init();
        
        $categories = $db->fetch_all('SELECT * FROM library_categories WHERE join_library_categories_id = ?', array($parent));
        
        foreach($categories as &$category){
            $category['branch'] = self::get_categories($category['library_categories_id']);
        }
        unset($category);
        
        return $categories;
    }
    
    public static function get_parent_ids($category_id){
        $db = mysqli_db::init();
        
        $parent_ids   = array();
        
        while(1){
            $current_id  = $category_id;
            $category_id = $db->fetch_singlet('SELECT join_library_categories_id FROM library_categories WHERE library_categories_id = ?', array($category_id));
            if(!$category_id){
                break;
            }
            
            $parent_ids []= array(
                'parent_id' => $category_id,
                'current_id' => $current_id
            );
        }
        
        return $parent_ids;
    }
    
    static function get_category_name($library_categories_id){
        $db = mysqli_db::init();
        
        return $db->fetch_singlet('select library_categories_name from library_categories where library_categories_id = ?', array($library_categories_id));
    }
    
    public static function get_categories_tree($parent_id, $level = 0){
        $db = mysqli_db::init();
        
        $cats = $db->fetch_all('
            SELECT * FROM library_categories
            WHERE join_library_categories_id = ?
            ', array($parent_id));
        
        if($cats){
            for($i=count($cats)-1; $i>=0; $i--){          //loop through backwards so array_splice doesn't screw up loop
                $cats[$i]['sub_level'] = $level;
                if($subcategories = library_categories::get_categories_tree($cats[$i]['library_categories_id'],$level+1)){
                    array_splice($cats,$i+1,0,$subcategories);
                }
            }
            
            return $cats;
        }else{
            return false;
        }
    }
    
    //with level of subness
    static function get_library_categories_levels($parent_id = 0, $level = 0, $omit_id = -1){
        $db = mysqli_db::init();
        
        $wheres = array();
        
        $wheres[] = 'lc.join_library_categories_id  = ?';
        $params[] = $parent_id;
        
        $wheres[] = 'library_categories_id != ?';
        $params[] = $omit_id;
        
        $library_categories = $db->fetch_all('
            SELECT lc.*, "'.$db->filter($level).'" AS sub_level, COUNT(documents_id) as num_documents
            FROM library_categories as lc
            LEFT JOIN documents as d on d.join_library_categories_id = library_categories_id' .
            strings::where($wheres) . '
            GROUP BY library_categories_id
            ORDER BY library_categories_name', $params); //echo $db->debug();
            
        $all_library_categories = array();
        foreach($library_categories as $library_category){
            $all_library_categories[] = $library_category;
            $all_library_categories = array_merge($all_library_categories, self::get_library_categories_levels($library_category['library_categories_id'], $level + 1, $omit_id));
        }
        foreach($all_library_categories as &$cat) {
            $count = $db->fetch_singlet('SELECT COUNT(*) AS count FROM library_categories AS lc
                                WHERE join_library_categories_id = ?',
                                array($cat['library_categories_id']));
            if($count > 0) {
                $cat['has_children'] = 1;
            }
        }
        unset($cat);
        
        return $all_library_categories;
    }
    
    //with level of subness
    static function get_member_library_categories_levels($members_id, $parent_id = 0, $level = 0, $omit_id = -1){
        $db = mysqli_db::init();
        
        $wheres = array();
        
        $params[] = ActiveMemberInfo::GetMemberId();
        
        $wheres[] = 'lc.join_library_categories_id  = ?';
        $params[] = $parent_id;
        
        $wheres[] = 'library_categories_id != ?';
        $params[] = $omit_id;
        
        $library_categories = $db->fetch_all('
            SELECT lc.*, "'.$db->filter($level).'" AS sub_level, COUNT(documents_id) as num_documents
            FROM library_categories as lc
            LEFT JOIN (
                select d.*
                from documents as d
                JOIN members_x_documents as mxd on
                    mxd.join_documents_id = documents_id and
                    mxd.join_members_id = ?) as d on d.join_library_categories_id = library_categories_id' .
            strings::where($wheres) . '
            GROUP BY library_categories_id
            ORDER BY library_categories_name', $params); //echo $db->debug().'<br />';
            
        $all_library_categories = array();
        foreach($library_categories as $library_category){
            $all_library_categories[] = $library_category;
            $all_library_categories = array_merge($all_library_categories, self::get_library_categories_levels($library_category['library_categories_id'], $level + 1, $omit_id));
        }
        
        return $all_library_categories;
    }
}

?>
