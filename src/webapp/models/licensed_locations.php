<?php

class licensed_locations
{

    static function get_location_types()
    {
        return array('Hotel - Full Service/Resort' => 'Hotel',
            'Hotel - Limited Service' => 'Hotel',
            'Spa - Destination Spa' => 'Hotel',
            'Other' => 'Other',
            'Corporate Office' => 'Corporate Office');
    }

    static function get_legacy_location_types()
    {
        return array('Hotel - Full Service' => 'Hotel');
    }

    static function get_all()
    {
        $db = mysqli_db::init();

        return $db->fetch_all('select * from licensed_locations order by licensed_locations_name');
    }

    static function create($new_loc)
    {
        $db = mysqli_db::init();

        //validate?

        $licensed_locations_table = new mysqli_db_table('licensed_locations');
        $licensed_locations_table->insert($new_loc);
        $licensed_locations_id = $licensed_locations_table->last_id();

        return $licensed_locations_id;
    }

    static function get_by_id($id)
    {
        $db = mysqli_db::init();
        return $db->fetch_one('select * from licensed_locations
                               where licensed_locations_id = ? ', array($id));
    }

    static function get_by_id_with_details($id)
    {
        $db = mysqli_db::init();
        $location = $db->fetch_one(
            'select * from licensed_locations ll
              left join hotels as h on ll.licensed_locations_id = h.join_licensed_locations_id
              left join corporate_locations as cl on ll.licensed_locations_id = cl.join_licensed_locations_id
              where licensed_locations_id = ? ', array($id));

        licensed_locations::add_types_to_location($location);

        return $location;
    }

    static function add_types_to_location(&$location) {
        if ($location["corporate_locations_id"]) {
            $location["licensed_locations_full_type"] = "Corporate Office";
        } else if ($location["hotels_id"]) {
            $location["licensed_locations_full_type"] = $location["hotels_industry_type"];
        } else {
            $location["licensed_locations_full_type"] = "Other";
        }

        $location["licensed_locations_type"] = licensed_locations::get_type_from_full_type($location["licensed_locations_full_type"]);
    }

    static function get_type_from_full_type($full_type)
    {
        $types = licensed_locations::get_location_types();
        $old_types = licensed_locations::get_legacy_location_types();
        $all_types = array_merge($types, $old_types);
        return $all_types[$full_type];
    }

    /**
     * @param $members_id
     * @return array|mixed
     */
    static function get_licensed_locations_for_member($members_id) {
        $db = mysqli_db::init();
        $memberInfo = $db->fetch_one("select members_type, join_accounts_id from members where members_id = ?", array($members_id));
        if ( $memberInfo['members_type'] == 'admin') {
            return $db->fetch_all("select * from licensed_locations where licensed_locations_delete_datetime is null AND join_accounts_id = ?", array($memberInfo['join_accounts_id']));
        } else {
            return $db->fetch_all("select * from licensed_locations
                                   where licensed_locations_delete_datetime is null AND exists (
                                       SELECT 1 from licensed_locations_x_members_access llxm
                                       WHERE licensed_locations_id = join_licensed_locations_id
                                       AND llxm.join_members_id = ?
                                   )", array($members_id));
        }
    }

    /**
     * @param $members_id
     * @return array|mixed
     */
    static function get_licensed_locations_with_permission($permission) {
        $db = mysqli_db::init();
        $licensed_locations_ids = UserPermissions::LocationIdsWithPermission($permission);
        $clause = strings::CreateInClauseForIds($licensed_locations_ids, 'licensed_locations');
        return $db->fetch_all("select * from licensed_locations where licensed_locations_delete_datetime is null AND " . $clause, array());
    }

    static function get_licensed_locations_ids_for_member($members_id) {
        $db = mysqli_db::init();
        $memberInfo = $db->fetch_one("select members_type, join_accounts_id from members where members_id = ?", array($members_id));
        if ( $memberInfo['members_type'] == 'admin') {
            $results = $db->fetch_all("select licensed_locations_id from licensed_locations where join_accounts_id = ?", array($memberInfo['join_accounts_id']));
        } else {
            $results = $db->fetch_all("select distinct licensed_locations_id from licensed_locations
                                   join licensed_locations_x_members_access llxm on licensed_locations_id = join_licensed_locations_id
                                   where llxm.join_members_id = ?", array($members_id));
        }
        $ids = array();
        foreach($results as $row ) {
            $ids[] = $row['licensed_locations_id'];
        }
        return $ids;
    }

    static function get_for_corp($id)
    {
        return licensed_locations::get_for_account($id);
    }

    static function get_for_account($id)
    {
        $db = mysqli_db::init();
        $licensed_locations = $db->fetch_all('select * from licensed_locations as ll
                               left join hotels as h on ll.licensed_locations_id = h.join_licensed_locations_id
                               left join corporate_locations as cl on ll.licensed_locations_id = cl.join_licensed_locations_id
                               where join_accounts_id = ?
                               order by licensed_locations_name ', array($id));

        $return_locations = array();
        foreach($licensed_locations as $licensed_location )
        {
            licensed_locations::add_types_to_location($licensed_location);

            if($licensed_location["licensed_locations_full_type"] == "Corporate Office")
            {
                $licensed_location["licensed_locations_name"] = $licensed_location["licensed_locations_name"] . " (Corporate Office)";
            }

            $return_locations[] = $licensed_location;
        }

        return $return_locations;
    }

    public static function is_corporate_location($id)
    {
       $db = mysqli_db::init();
        $licensed_location = $db->fetch_all('select * from licensed_locations as ll
                               left join hotels as h on ll.licensed_locations_id = h.join_licensed_locations_id
                               left join corporate_locations as cl on ll.licensed_locations_id = cl.join_licensed_locations_id
                               where licensed_locations_id = ? ', array($id));

        if($licensed_location[0])
        {
            $licensed_location = $licensed_location[0];
            licensed_locations::add_types_to_location($licensed_location);

            if($licensed_location["licensed_locations_full_type"] == "Corporate Office")
            {
                return true;
            }
        }
        return false;
    }

    public static function delete($licensed_locations_id){
        $db = mysqli_db::init();

        //-------------------------------------------------------------------------------------
        // -- delete user permissions
        //-------------------------------------------------------------------------------------
        //TODO accounts should members that only have access to this location be deleted?
        $db->query('DELETE FROM licensed_locations_x_members_access
                           WHERE join_licensed_locations_id = ? ', array($licensed_locations_id));

        // Delete licensed_location
        $db->query('DELETE FROM hotels WHERE join_licensed_locations_id = ? ', array($licensed_locations_id));
        $db->query('DELETE FROM corporate_locations WHERE join_licensed_locations_id = ? ', array($licensed_locations_id));
        $db->query('DELETE FROM licensed_locations WHERE licensed_locations_id = ? ', array($licensed_locations_id));

        //TODO accounts Delete claims, certs, contracts etc that are only for this location?  What other objects?
    }

    public static function getPermissionForMemberLocation($members_id, $licensed_locations_id) {
        $db = mysqli_db::init();

        $row = $db->fetch_one("select join_members_access_levels_id from licensed_locations_x_members_access
                        join members_access_levels
                        on licensed_locations_x_members_access.join_members_access_levels_id = members_access_levels.members_access_levels_id
                        where join_members_id = ? and join_licensed_locations_id = ? ", array($members_id, $licensed_locations_id));
        return $row['members_access_level_type'];
    }

    private static function getConfig($object_type)
    {
        $config = array();

        if($object_type === 'vendors' ||
            $object_type === 'contracts' ||
            $object_type == 'carriers') {
            $config['multiLocation'] = true;
        }

        return $config;
    }

    public static function getObjectsAssignedToLocation($object_type, $location_id) {
        $config = self::getConfig($object_type);

        $wheres = array();
        $params = array();

        if($config['multiLocation']) {
            $wheres[] = "
                (
                    ({$object_type}_all_locations = 1 AND join_accounts_id = (SELECT join_accounts_id FROM licensed_locations WHERE licensed_locations_id = ?))
                    OR
                    EXISTS(SELECT 1 from {$object_type}_x_licensed_locations
                     WHERE join_licensed_locations_id = ?
                       AND ${object_type}_id = join_{$object_type}_id)
                )
                ";
            $params[] = $location_id;
            $params[] = $location_id;
        } else {
            $wheres[] = "
                    join_licensed_locations_id = ?
                ";
            $params[] = $location_id;
        }

        $sql = "SELECT * FROM $object_type WHERE " . implode($wheres, ' AND ');

        $db = mysqli_db::init();
        return $db->fetch_all($sql, $params);
    }
    public static function getLocationNames($location_ids){
        $db = mysqli_db::init();
        $clause = strings::CreateInClauseForIds($location_ids, 'licensed_locations');
        $locations = $db->fetch_singlets ("SELECT licensed_locations_name from licensed_locations WHERE $clause");
        $location_names = implode('<br>',$locations);
        return $location_names;
    }
}

