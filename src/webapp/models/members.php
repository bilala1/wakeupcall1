<?php
final class members {
    public static $members = array();

    static function create($member) {
        $members_table = new mysqli_db_table('members');
        $notifications_table = new mysqli_db_table('notifications');
        $members_settings_table = new mysqli_db_table('members_settings');
        $forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');
        $member['members_billing_type'] = 'yearly';
        $members_table->insert($member);
        $members_id = $members_table->last_id();

        $notifications_table->insert(array('join_members_id' => $members_id));

        $forums_groups_x_members_table->insert(array(
            'join_groups_id' => 6,      //Message Forum
            'join_members_id' => $members_id
        ));

        // collapsable yellow information boxes
        $members_settings_table->insert(array('join_members_id' => $members_id));
        return $members_id;
    }

    static function join_account($members_id, $accounts_id)
    {
        $db = mysqli_db::init();
        
        $db->query('UPDATE members SET join_accounts_id = ? WHERE members_id = ?', array($accounts_id, $members_id));
    }
    
    static function get_all_names(){
        $db = mysqli_db::init();

        return $db->fetch_all("select members_id, members_firstName, members_lastname, CONCAT(members_lastname,', ', members_firstname) AS members_fullname from members order by members_lastname, members_firstname");
    }


    static function get_members_show_startup_video($members_id){
        $db = mysqli_db::init();
        
        $members_show_startup_video = $db->fetch_singlet('select members_show_startup_video from members where members_id = ?', array($members_id));
        
        return $members_show_startup_video;
    }

    static function get_by_id($members_id){
        if($member = self::$members[$members_id]){
            return $member;
        }

        $db = mysqli_db::init();

        $member = $db->fetch_one('
            SELECT m.*
            FROM members AS m
            WHERE m.members_id = ?
            GROUP BY m.members_id', array($members_id));

        if(!$member){
            return false;
        }

        // Find the forum posts for this member...
        $member['forum_posts'] = $db->fetch_singlet('SELECT COUNT(*) FROM forums_posts WHERE join_members_id = ?', array($members_id));

        $member['members_username'] = $member['members_firstname'].' '.$member['members_lastname'];

        // Store them so we don't query the database a lot
        self::$members[$members_id] = $member;

        return $member;
    }

    static function by_group($groups_id){
        $db = mysqli_db::init();

        $members = $db->fetch_all('SELECT m.*
            FROM members AS m
            JOIN forums_groups_x_members AS gxg ON gxg.join_members_id = m.members_id
            WHERE gxg.join_groups_id = ?', array($groups_id));

        return $members;
    }

    //$groups_list = 'group1,group2'
    //if group does not exist, it will not be added
    static function set_groups($members_id,$groups_list){
        $db = mysqli_db::init();

        $db->query('DELETE FROM forums_groups_x_members WHERE join_members_id = ?', array($members_id));

        $names = explode(',',$groups_list);
        $wheres = $params = array();
        foreach($names as $name){
            $wheres[] = '?';
            $params[] = $name;
        }

        $groups = $db->fetch_all('SELECT groups_id FROM forums_groups WHERE groups_name in('.implode(',',$wheres).')', $params);

        $gxg_table = new mysqli_db_table('forums_groups_x_members');
        foreach($groups as $group){
            $gxg_table->insert(array(
                'join_groups_id' => $group['groups_id'],
                'join_members_id' => $members_id));
        }
    }

    static function set_forum_groups($members_id, $groups_ids){
        $db = mysqli_db::init();
        $forums_groups_x_members_table = new mysqli_db_table('forums_groups_x_members');

        $db->query('delete from forums_groups_x_members where join_members_id = ?', array($members_id));
        foreach($groups_ids as $groups_id){
            $forums_groups_x_members_table->insert(array('join_groups_id' => $groups_id, 'join_members_id' => $members_id));
        }
    }

    public static function log_member_in($member){
        session_unset();
        $db = mysqli_db::init();

        // Get member, account, etc.
        $member = $db->fetch_one('
            SELECT *, a.join_members_id as accounts_admin_member_id
            FROM members m
            INNER JOIN accounts a on a.accounts_id = m.join_accounts_id
            LEFT JOIN corporations c on a.accounts_id = c.join_accounts_id
            WHERE members_id = ?',
            array($member['members_id'])
        );

        // TODO ACCOUNTS revisit what should be made "private" (currently prefixed with an underscore,
        // should never be accessed in view/action code)
        $_SESSION['accounts_id'] = $member['accounts_id'];
        $_SESSION['parent_account_id'] = $member['join_accounts_id_parent'];
        $_SESSION['_is_account_admin'] = $member['members_type'] == 'admin';
        $_SESSION['_is_main_account_admin'] = $member['accounts_admin_member_id'] == $member['members_id'];
        $_SESSION['_accounts_type'] = $member['accounts_type'];
        $_SESSION['members_id'] = $member['members_id'];

        $_SESSION['last_visit'] = $member['members_last_visit'];
		//-- set last login to now!
		$db->query('UPDATE members 
                    SET members_last_login = NOW(),
                    members_lastsession = ?
                    WHERE members_id = ?', array(session_id(),$member['members_id']));

		//-- insert login record!
		$login_info = array(
			'join_members_id' => $member['members_id'],
			'members_logins_datetime' => date('Y-m-d H:i:s')
		);
		$members_logins = new mysqli_db_table('members_logins');
		$members_logins->insert($login_info);

        members::log_action($member['members_id'], "Logged in to " . SITE_NAME);

        // TODO ACCOUNTS delete or at least privatize these
        $_SESSION['members_status'] = $member['members_status'];
        $_SESSION['deleted_check'] = date(DATE_FORMAT_FULL, strtotime('now')); // when it checked last time whether entity is deleted
    }

    public static function logout(){
        $db = mysqli_db::init();
        $db->query('UPDATE members 
                    SET members_last_visit = UTC_TIMESTAMP(),
                    members_lastsession = ""
                    WHERE members_id = ?',
                    array(ActiveMemberInfo::GetMemberId()));

        members::log_action(ActiveMemberInfo::GetMemberId(), "Logged out of " . SITE_NAME);

        unset($_SESSION);
    }

    public static function get_corporation_forum_id(){
        $db = mysqli_db::init();
        return $db->fetch_singlet('select join_forums_id from corporations where join_accounts_id = ?', array(ActiveMemberInfo::GetAccountId()));
    }

    //add one forum group to a member
    static function add_forum_group($members_id, $groups_id){
        $db = mysqli_db::init();

        $groups_ids = $db->fetch_singlets('select join_groups_id from forums_groups_x_members where join_members_id = ?', array($members_id));

        $groups_ids = arrays::values_as_keys($groups_ids);

        $groups_ids[$groups_id] = $groups_id;        //to avoid duplicates

        self::set_forum_groups($members_id, $groups_id);
    }

    static function get_recently_viewed($num = 10){
        $db = mysqli_db::init();

        //oh yeah, one query baby! (well, one $db->fetch call...)
        $documents = $db->fetch_all('
            select distinct join_id, name, file, filename, downloads_type
            from (
                select *
                from (
                    (select downloads_datetime, join_id, documents_title as name, documents_file as file, documents_filename as filename, downloads_type
                    from downloads as d
                    join documents on documents_id = join_id
                    where downloads_type = "file" and d.join_members_id = ?)
                        union
                    (select downloads_datetime, join_id, chemicals_name as name, chemicals_file as file, "none" as filename, downloads_type
                    from downloads as d
                    join chemicals on chemicals_id = join_id
                    where downloads_type = "msds" and d.join_members_id = ?)
                        union
                    (select downloads_datetime, join_id, certificates_filename as name, certificates_file as file, certificates_filename as filename, downloads_type
                    from downloads as d
                    join certificates on certificates_id = join_id
                    where downloads_type = "certificate" and d.join_members_id = ?)
                        union
                    (select downloads_datetime, join_id, forums_posts_files_original_filename as name, forums_posts_files_filename as file, forums_posts_files_original_filename as filename, downloads_type
                    from downloads as d
                    join forums_posts_files on forums_posts_files_id = join_id
                    where downloads_type = "forum file" and d.join_members_id = ?)
                        union
                    (select downloads_datetime, join_id, contracts_files_filename as name, contracts_files_file as file, contracts_files_filename as filename, downloads_type
                    from downloads as d
                    join contracts_files on contracts_files_id = join_id
                    where downloads_type = "contract" and d.join_members_id = ?)
                ) as downloads
                order by downloads_datetime desc
            ) as downloads
            limit ?',
            array(ActiveMemberInfo::GetMemberId(),
                ActiveMemberInfo::GetMemberId(),
                ActiveMemberInfo::GetMemberId(),
                ActiveMemberInfo::GetMemberId(),
                ActiveMemberInfo::GetMemberId(),
                $num)
        );

        return $documents;
    }

    static function has_notification($members_id, $notification){
        $db = mysqli_db::init();

        $notification = $db->fetch_singlet('select '.$db->escape_column($notification).' from notifications where join_members_id = ?', array($members_id));

        return $notification;
    }

    // returns status for a member's id [free,member,locked]
    static function get_members_status($members_id){
        $db = mysqli_db::init();

        $status = $db->fetch_singlet('SELECT members_status FROM members WHERE members_id = ? ', array($members_id));
        if($status == 'exempt') {
            $status = 'member';
        }
        return $status;
    }

    // sets a member's status, and all their sub users (if corp, then sub hotels and staff. if hotel, then staff)
    static function set_members_status($members_id, $members_status){
        $db = mysqli_db::init();
        
        $member = $db->fetch_one('select * from members where members_id = ?', array($members_id));
        
        if($member){
            //update member's account's members status
            $db->query('update members set members_status = ? where join_accounts_id = ?', array($members_status, $member['join_accounts_id']));

            $account_locked = $members_status == 'locked' ? 1 : 0;
            $db->query('update accounts set accounts_is_locked = ? where accounts_id = ?', array($account_locked, $member['join_accounts_id']));
        }
    }

    static function log_action($members_id, $action, $certificates_id = 0){
        $actions_table = new mysqli_db_table('actions');

        $actions_table->insert(array(
            'join_members_id' => $members_id,
            'join_certificates_id' => $certificates_id,
            'actions_name' => $action,
            'actions_datetime' => SQL('NOW()')
        ));
    }
    // log MSDS downloads
    static function log_msds_download($members_id, $action, $msds_id = null){
        $actions_table = new mysqli_db_table('actions');

        $actions_table->insert(array(
            'join_members_id' => $members_id,
            'actions_actio_msds' => $msds_id,
            'actions_name' => $action,
            'actions_datetime' => SQL('NOW()')
        ));
    }    

    // function to check if an entitiy to which this member belongs is deleted
    // deleteion status is good for 60 days after which all files will be deleted
    // during this period entitiy may be reinstated/renewed. Also during this period no member from the entity can login
    static function get_entity_deleted($members_id){
        $db = mysqli_db::init();

        $delete_datetime = $db->fetch_singlet('
          SELECT accounts_delete_datetime FROM accounts
          INNER JOIN members on join_accounts_id = accounts_id
          WHERE members_id = ?', array($members_id));

        if($delete_datetime){
            return 'Account was canceled: please contact us';
        }

        return false;
    } //  end - get_entity_deleted_tf()

    static function get_customer_credit_card_data($member)
    {
        if(!$member['members_stripe_id'])
        {
            return $member;
        }

        require_once(LIBS . "/stripe_lib/stripe_include.php");

        try
        {
            $data_customer = Stripe_Customer::retrieve($member['members_stripe_id']);
            $cards = $data_customer['cards'];
            $card = $cards->data[0];

            $member['members_billing_addr1'] = $card['address_line1'];
            $member['members_billing_addr2'] = $card['address_line2'];
            $member['members_card_expire_month'] = $card['exp_month'];
            $member['members_card_expire_year'] = $card['exp_year'];
            $member['members_billing_city'] = $card['address_city'];
            $member['members_billing_state'] = $card['address_state'];
            $member['members_billing_zip'] = $card['address_zip'];
            $member['members_card_num'] =  '************' . $card['last4'];
            $member['members_card_type'] =  $card['brand'];
            $member['last4'] = $card['last4'];

            return $member;
        }
        catch(Exception $e)
        {
            return $member;
        }
    }

    static function get_customer_credit_card($member_stripe_id)
    {
        if(!$member_stripe_id)
        {
            return null;
        }

        require_once(LIBS . "/stripe_lib/stripe_include.php");

        try
        {
            $data_customer = Stripe_Customer::retrieve($member_stripe_id);
            $cards = $data_customer['cards'];
            $card = $cards->data[0];
            return $card;
        }
        catch(Exception $e)
        {
            return null;
        }
    }

    static function get_credit_last4($member_stripe_id)
    {
        if(!$member_stripe_id)
        {
            return "0000";
        }

        require_once(LIBS . "/stripe_lib/stripe_include.php");

        try
        {
            $data_customer = Stripe_Customer::retrieve($member_stripe_id);
            $cards = $data_customer['cards'];
            $card = $cards->data[0];
            return $card['last4'];
        }
        catch(Exception $e)
        {
            return "0000";
        }
    }

    // KO changed this function on: Oct, 3, 2011.
    // Instead of optional cvv parameter, it will take an optional array of values from submitted form OR cvv string.
    // IF cvv string is supplied, member data is taken from DB, otherwise if array is given, it's used for payment processing instead
    // returns array((bool)$approved, (string)$transaction_error)
    static function process_payment($members_id, $amount, $description){
        $db = mysqli_db::init();
        $payments_table = new mysqli_db_table('payments');
        $cvv = null;

        $member = $db->fetch_one('
            select *
            from members
            where members_id = ?',
            array($members_id)
        );

        if(!$member){
            return array(false, 'Member does not exist');
        }

        if(!$member['members_stripe_id'])
        {
            return array(false, 'No Acceptable Payment Token Found, Please alert customer to update billing info.');
        }

        require_once(LIBS . "/stripe_lib/stripe_include.php");

        $approved = false;
        try
        {
            $charge = Stripe_Charge::create(array(
                    "amount" => $amount * 100, // Multiply by 100, stripe takes payments in cents
                    "currency" => "usd",
                    "customer" => $member['members_stripe_id'],
                    "description" => "Yearly Subscription Renewal Payment for member". $member['members_email']));

            $approved = true;
        }
        catch(Exception $e)
        {
            $approved = false;
            $transaction_error = $e->getMessage();
        }

        if($approved){
            //record payment
            $payments_table->insert(array(
                'join_members_id' => $member['members_id'],
                'payments_amount' => $amount,
                'payments_datetime' => SQL('NOW()'),
                'payments_transaction_id' => $charge->id
            ));
        }

        return array($approved, $transaction_error);
    }

    public static function delete($members_id) {
        members::remove_all_data($members_id);
    }

    // remove all member's data
    static function remove_all_data($id){ //members id

        $db = mysqli_db::init();
        //-----------------------------------------------------------------------------------------
        // -- delete files (and rows from tables related to files)
        //-----------------------------------------------------------------------------------------

            //-------------------------------------------------------------------------------------
            // -- delete Documents
            //-------------------------------------------------------------------------------------
                //if files are accepted into library - do not delete
                $documents_to_remove = $db->fetch_all('SELECT documents_id, documents_file
                                                       FROM documents
                                                       WHERE join_members_id = ?
                                                       AND documents_type != "library" ', array($id));
                foreach($documents_to_remove as $document_remove){
                    //remove file from system

                    unlink(SITE_PATH . DOCUMENT_DIR.$document_remove['documents_file']);
                    //delete download stats
                    $db->query('DELETE FROM downloads WHERE downloads_type = "file" AND join_id = ?', array($document_remove['documents_id']));
                    //delete from 'My Files'
                    $db->query('DELETE FROM members_x_documents WHERE join_documents_id = ?', array($document_remove['documents_id']));
                }
                $db->query('DELETE FROM documents
                                   WHERE join_members_id = ?
                                   AND documents_type != "library" ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete 'My Folders'
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members_library_categories
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete from chemicals_x_members
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM chemicals_x_members
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete from members_x_documents
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members_x_documents
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete from downloads
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM downloads
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete actions/log
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM actions
                                   WHERE join_members_id = ? ', array($id));


            //-------------------------------------------------------------------------------------
            // -- delete notifications options
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM notifications
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete personalized member settings
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members_settings
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete forum permissions
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM forums_groups_x_members
                                   WHERE join_members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- delete Member from members table
            //-------------------------------------------------------------------------------------
                $db->query('DELETE FROM members
                                   WHERE members_id = ? ', array($id));

            //-------------------------------------------------------------------------------------
            // -- set orphanan forum posts to "Former Member" [id = 137 ]
            //-------------------------------------------------------------------------------------
                forums::set_orphan_posts_to_former_member();

    }// end of function remove_all_data

    //returns contact information per member
    static function get_contact_info($member_id) {
        $db = mysqli_db::init();
        $member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', array($member_id));
        $members_company = array();

        //If they are a user and only associated with one location, use that.  Otherwise use the account's info
        if($member['members_type'] == 'user') {
            $licensed_locations_id = $db->fetch_all('SELECT DISTINCT join_licensed_locations_id
                                           FROM licensed_locations_x_members_access WHERE join_members_id = ?', array($member_id));
            if(count($licensed_locations_id) == 1) {
                $licensed_location = $db->fetch_one('SELECT * FROM licensed_locations WHERE licensed_locations_id = ?', array($licensed_locations_id[0]['join_licensed_locations_id']));

                $location['address']    = $licensed_location['licensed_locations_address'];
                $location['city']       = $licensed_location['licensed_locations_city'];
                $location['state']      = $licensed_location['licensed_locations_state'];
                $location['zip']        = $licensed_location['licensed_locations_zip'];
                $location['name']       = $licensed_location['licensed_locations_name'];
                $location['id']         = $licensed_location['licensed_locations_id'];
                $location['top']        = $licensed_location['join_accounts_id'];
            }
        }

        if(!array_key_exists('name', $members_company)) {
            $account = $db->fetch_one('SELECT accounts_name, accounts_id, accounts_address,	accounts_city, 	accounts_state,	accounts_zip
                                           FROM accounts WHERE accounts_id = ?',array($member['join_accounts_id']));

            $location['address']    = $account['accounts_address'];
            $location['city']       = $account['accounts_city'];
            $location['state']      = $account['accounts_state'];
            $location['zip']        = $account['accounts_zip'];
            $location['name']       = $account['accounts_name'];
            $location['id']         = $account['accounts_id'];
            $location['top']        = $account['accounts_id'];
        }

        $location['entity_name']        = $location['name']; // company name corporation or hotel
        $location['members_fullname']   = $member['members_firstname'].' '.$member['members_lastname'];
        $location['members_title']      = $member['members_title'];
        $location['members_email']      = $member['members_email'];


        return $location;
    } // end -- get_contact_info()

    static function get_members_email($member_id) {
        $db = mysqli_db::init();
        return $db->fetch_singlet('SELECT members_email FROM members WHERE members_id = ?', array($member_id));
    }

    // whether a member can renew or not. Corporate accounts and autonomous locations can renew themselves,
    // while locations that belong to corporate accounts are paid by corporate accounts
    // return true or false
    static function get_renewable_tf($member_id) {
        $db = mysqli_db::init();
        $owned_account = $db->fetch_singlet('SELECT accounts_id FROM accounts WHERE join_members_id = ?', array($member_id));
        return !!$owned_account;
    }
	
	// Get access permissions
	static function get_access_levels($member_id) {
		$db = mysqli_db::init();
		$member_access_levels = $db->fetch_all(
			'SELECT 
				MAL.members_access_levels_type
			 FROM members_access_levels MAL
				JOIN members_access MA ON MA.members_access_levels_id = MAL.members_access_levels_id
			 WHERE 
				MA.join_members_id = ?', array($member_id)
		);
		$access_level_array = array();
		foreach ($member_access_levels as $access_level) {
			$access_level_array[] = $access_level['members_access_levels_type'];
		}
		return $access_level_array;
	}
	
	// Get access permissions
	static function get_access_levels_using_path($member_id, $path) {
		$allow_staff = false;
		
		$db = mysqli_db::init();
		$member_access_levels = $db->fetch_one(
			'SELECT 
				MAL.members_access_levels_type
			 FROM members_access_levels MAL
				JOIN members_access MA ON MA.members_access_levels_id = MAL.members_access_levels_id
			 WHERE 
				MA.join_members_id = ? AND MAL.members_access_levels_type = ?' , array($member_id, substr($path, 0, strpos($path, '?')))
		);
		if (count($member_access_levels) > 0) {
			$allow_staff = true;
		}
		return $allow_staff;
	}
}

?>
