<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/8/15
 * Time: 11:42 AM
 */

abstract class ClaimReportTypes {
    const WorkersComp = 0;
    const General = 1;
    const Auto = 2;
    const Property = 3;
    const Other = 4;
    const Labor = 5;
    static function getClaimType($claimString) {
        switch($claimString) {
            case 'WorkersComp': return ClaimReportTypes::WorkersComp; break;
            case 'General': return ClaimReportTypes::General; break;
            case 'Auto': return ClaimReportTypes::Auto; break;
            case 'Property': return ClaimReportTypes::Property; break;
            case 'Other': return ClaimReportTypes::Other; break;
            case 'Labor': return ClaimReportTypes::Labor; break;
            default: return -1; break;
        }
    }
}

abstract class ClaimReports
{
     public static $list = array(
         // WORKERS COMP REPORTS
         "claimpercentage" => array(
             "reportType" => ClaimReportTypes::WorkersComp,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
        "compbylocation" => array(
            "reportType" => ClaimReportTypes::WorkersComp,
            "title" => "Worker's Comp Claims Counts by Location",
            "params" => array(
                "fromDate",
                "toDate",
                "filterHidden",
                "chartType",
                "Status"
            ),
            "staticParams" => array(
                'claim_type' => 'workers'
            ),
            "url" => "reports/claims_counts.php",
            "chartType" => "column",
            "corpOnly" => true
        ),
        "compclaimtype" => array(
            "reportType" => ClaimReportTypes::WorkersComp,
            "title" => "Worker's Comp by Claim Type",
            "params" => array(
                "locations",
                "fromDate",
                "toDate",
                "filterHidden",
                "chartType",
                "Status"
            ),
            "url" => "reports/claims_comp_by_type.php",
            "chartType" => "pie"
        ),
         "compbybodypart" => array(
             "reportType" => ClaimReportTypes::WorkersComp,
             "title" => "Worker's Comp by Body Part",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_comp_by_body_part.php",
             "chartType" => "column"
         ),

        "compbydepartment" => array(
            "reportType" => ClaimReportTypes::WorkersComp,
            "title" => "Worker's Comp by Claims by Employee Dept",
            "params" => array(
                "locations",
                "fromDate",
                "toDate",
                "filterHidden",
                "chartType",
                "Status"
            ),
            "url" => "reports/claims_comp_by_dept.php",
            "chartType" => "column"
        ),
         "compbyeventdepar" => array(
             "reportType" => ClaimReportTypes::WorkersComp,
             "title" => "Worker's Comp by Claims by Event Dept",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_comp_by_event_dept.php",
             "chartType" => "column"
         ),
        // GENERAL CLAIM REPORTS
         "claimpercentageGen" => array(
             "reportType" => ClaimReportTypes::General,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
        "genbylocation" => array(
            "reportType" => ClaimReportTypes::General,
            "title" => "General Liability Claims Counts by Location",
            "params" => array(
                "fromDate",
                "toDate",
                "filterHidden",
                "chartType",
                "Status"
            ),
            "staticParams" => array(
                'claim_type' => 'general'
            ),
            "url" => "reports/claims_counts.php",
            "chartType" => "column",
            "corpOnly" => true
        ),
         "generalAreaOfLoss" => array(
             "reportType" => ClaimReportTypes::General,
             "title" => "Claims by Area of Loss",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'general'
             ),
             "url" => "reports/claims_gen_by_area_of_loss.php",
             "chartType" => "column"
         ),
         "generalTypeOfLossPercentage" => array(
             "reportType" => ClaimReportTypes::General,
             "title" => "Claims Type Of Loss Percentage",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'general'
             ),
             "url" => "reports/claims_gen_by_type_of_loss_percentage.php",
             "chartType" => "pie"
         ),
         "generalTypeOfLoss" => array(
             "reportType" => ClaimReportTypes::General,
             "title" => "Claims Type Of Loss",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'general'
             ),
             "url" => "reports/claims_gen_by_type_of_loss.php",
             "chartType" => "column"
         ),
         // AUTO CLAIMS REPORTS
         "claimpercentageauto" => array(
             "reportType" => ClaimReportTypes::Auto,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
         "autobylocation" => array(
             "reportType" => ClaimReportTypes::Auto,
             "title" => "Auto Claims Counts by Location",
             "staticParams" => array(
                 'claim_type' => 'auto'
             ),
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                "Status"
             ),
             "url" => "reports/claims_counts.php",
             "chartType" => "column",
             "corpOnly" => true
         ),
         "autobynature" => array(
             "reportType" => ClaimReportTypes::Auto,
             "title" => "Auto Claims by Nature",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                "Status"
             ),
             "url" => "reports/claims_auto_by_nature.php",
             "chartType" => "column"
         ),
         // PROPERTY CLAIMS REPORTS
         "propertyclaimpie" => array(
             "reportType" => ClaimReportTypes::Property,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
         "propclaimsbylocation" => array(
             "reportType" => ClaimReportTypes::Property,
             "title" => "Property Claims Counts by Location",
             "staticParams" => array(
                 'claim_type' => 'property'
             ),
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_counts.php",
             "chartType" => "column",
             "corpOnly" => true
         ),
         "properyclaimbylosstype" => array(
             "reportType" => ClaimReportTypes::Property,
             "title" => "Property Claims by Loss Type",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_property_by_loss_type.php",
             "chartType" => "column"
         ),
         // Labor claims
         "laborclaimpie" => array(
             "reportType" => ClaimReportTypes::Labor,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
         "laborbylocation" => array(
             "reportType" => ClaimReportTypes::Labor,
             "title" => "Labor Law Claims Counts by Location",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'labor'
             ),
             "url" => "reports/claims_counts.php",
             "chartType" => "column",
             "corpOnly" => true
         ),
          // Other CLAIM REPORTS
         "claimpercentageOther" => array(
             "reportType" => ClaimReportTypes::Other,
             "title" => "All Claims Pie Chart",
             "params" => array(
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "url" => "reports/claims_percentage.php",
             "chartType" => "pie"
         ),
        "otherbylocation" => array(
            "reportType" => ClaimReportTypes::Other,
            "title" => "Other Claims Counts by Location",
            "params" => array(
                "fromDate",
                "toDate",
                "filterHidden",
                "chartType",
                "Status"
            ),
            "staticParams" => array(
                'claim_type' => 'other'
            ),
            "url" => "reports/claims_counts.php",
            "chartType" => "column",
            "corpOnly" => true
        ),
         "otherAreaOfLoss" => array(
             "reportType" => ClaimReportTypes::Other,
             "title" => "Claims by Area of Loss",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'other'
             ),
             "url" => "reports/claims_gen_by_area_of_loss.php",
             "chartType" => "column"
         ),
         "otherTypeOfLossPercentage" => array(
             "reportType" => ClaimReportTypes::Other,
             "title" => "Claims Type Of Loss Percentage",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'other'
             ),
             "url" => "reports/claims_gen_by_type_of_loss_percentage.php",
             "chartType" => "pie"
         ),
         "otherTypeOfLoss" => array(
             "reportType" => ClaimReportTypes::Other,
             "title" => "Claims Type Of Loss",
             "params" => array(
                 "locations",
                 "fromDate",
                 "toDate",
                 "filterHidden",
                 "chartType",
                 "Status"
             ),
             "staticParams" => array(
                 'claim_type' => 'other'
             ),
             "url" => "reports/claims_gen_by_type_of_loss.php",
             "chartType" => "column"
         ),
    );
}