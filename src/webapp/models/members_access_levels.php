<?php

/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 9/25/15
 * Time: 5:32 AM
 */
class members_access_levels
{
    const certificates = "certificates";
    const claims = "claims";
    const contracts = "contracts";
    const elaw = "elaw";
    const hr = "hr";
    const forum = "forum";
    const manageUsers = "manageUsers";
    const editLocation = "editLocation";
    const locationAdmin = "locationAdmin";
    const manageAdmins = "manageAdmins";
    const shareDocuments = "shareDocuments";

}