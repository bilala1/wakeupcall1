<?php

class myDocuments {
    public static function get_locations($members_x_documents_id){
        $db = mysqli_db::init();
        $members_x_documents_locations = $db->fetch_singlets('SELECT join_licensed_locations_id from members_x_documents_x_licensed_locations WHERE '
                . 'join_members_x_documents_id = ? ',array($members_x_documents_id));
        return $members_x_documents_locations;
    }
    
    public static function delete_locations_by_members_x_documents_id($members_x_documents_id){
        $db = mysqli_db::init();
         $db->query('DELETE from members_x_documents_x_licensed_locations WHERE '
                . 'join_members_x_documents_id = ?',array($members_x_documents_id));
    }
}