<?

class notify {

    static function notify_admin_member_validated_email($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-validated-email.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' New member validated email | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_validated_email()


    static function notify_admin_member_free_trial_expired($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-trial-expired.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' Member\'s trial account expired | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_free_trial_expired()


    static function notify_admin_member_free_trial_reactivation($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-trial-expired-reactivated.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' Member reactivated expired account (trial member became full member) | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_free_trial_reactivation()


    static function notify_admin_member_account_expired_payment_failed($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-membership-expired-payment-failed.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' Member\'s account expired: payment failed | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_account_expired_payment_failed()


    static function notify_admin_member_account_canceled($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-canceled-account.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' Member has canceled an account | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_account_canceled()


    static function notify_admin_member_full_account_reactivated($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
		ob_start();
		include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-full-reactivated-account.php');
		$message = ob_get_contents();
		ob_end_clean();

		mail::send(
			$from = AUTO_EMAIL, // noreply@wakeupcall.net
			$to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
			$subject =' Member has reactivated an account | ' . SITE_NAME,
			$message = $message
		);
    } // end - notify_admin_member_account_canceled()


    static function notify_admin_new_trial_registration($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
        ob_start();
        include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-new-trial-registration.php');
        $message = ob_get_contents();
        ob_end_clean();

        mail::send(
            $from = AUTO_EMAIL, // noreply@wakeupcall.net
            $to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
            $subject =' Member has created new paid account | ' . SITE_NAME,
            $message = $message
        );
    } // end - notify_admin_new_full_registration()


    static function notify_admin_new_full_registration($member_id){
        $db = mysqli_db::init();

        //$member = $db->fetch_one('SELECT * FROM members WHERE members_id = ?', $member_id);
        $location = members::get_contact_info($member_id);

        // Email Admin
        ob_start();
        include(VIEWS . DIRECTORY_SEPARATOR . 'emails/admin-member-new-full-registration.php');
        $message = ob_get_contents();
        ob_end_clean();

        mail::send(
            $from = AUTO_EMAIL, // noreply@wakeupcall.net
            $to = MEMBERSHIPS_EMAIL, //memberships@wakeupcall.net
            $subject =' Member has created new paid account | ' . SITE_NAME,
            $message = $message
        );
    } // end - notify_admin_new_full_registration()

    public static function david($subject, $message) {
		$db = mysqli_db::init();

		$admin_notification = array(
			'admin_notifications_subject' => $subject,
			'admin_notifications_message' => $message,
			'admin_notifications_datetime' => date('Y-m-d H:i:s'),
			'admin_notifications_read' => 0
		);
		$an_table = new mysqli_db_table('admin_notifications');
		$an_table->insert($admin_notification);
	}

} // end - class notify


?>