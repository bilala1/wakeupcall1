<?php

final class permissions
{
    //You can just pass in the permission name ($type_name), and it uses $_SESSION['join_global_permission_groups_id']
    static function has_type($global_permission_groups_id, $type_name = null)
    {
        return true;
        
        /*//allows you to just pass in permission name
        if(is_string($global_permission_groups_id)){
            $type_name = $global_permission_groups_id;
            $global_permission_groups_id = $_SESSION['join_global_permission_groups_id'];
        }
        
        $db = mysqli_db::init();
        $permission = $db->fetch_one('SELECT gpt.*
            FROM global_permission_groups_x_global_permission_types AS gpgxgpt
            LEFT JOIN global_permission_types AS gpt ON global_permission_types_id = join_global_permission_types_id
            WHERE gpgxgpt.join_global_permission_groups_id = ? AND LOWER(global_permission_types_name) = LOWER(?)
            ', array($global_permission_groups_id, $type_name)
        );
        if(is_array($permission)) 
        {
            return true;
        }
        return false;*/
    }
    
    static function check($type_name){

        if(!permissions::has_type($type_name)){ 
            http::redirect(HTTP_FULLURL . '/login.php?notice=' . urlencode('you do not have permissions'));
            /*echo 'I\'m sorry.  I cannot let you do that ' . $_SESSION['global_members_username'];
            exit;*/
            
            /*framework::set_view('no-access.php');
            return;*/
        }
    }
}

?>
