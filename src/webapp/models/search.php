<?php

final class search {
    public static function get_types(){
        $types = array(
            'all' => 'Entire Site',
            'docs' => 'Documents',
            'mydocs' => 'My WAKEUP CALL Files',
            'forum' => 'Message Forum',
            'faqs' => 'FAQ\'s'/*,
            'vendors' => 'Vendors'*/
        );
        
        return $types;
    }
    
    public static function searchtype($q, $type = 'all'){
        $db     = mysqli_db::init();
        $types  = self::get_types();
        $search = array();
        
        if($type == 'all'){
            unset($types['all']);
            $search = $types;
        }
        else {
            $search [$type]= $types[$type];
        }
        
        $results = array();
        foreach($search as $key => $searchtype){
            $where = $params = array();
            $return = array();
            
            switch($key){
                case "docs":
                    $words =  search::getWordsInString($q);
                    for($i = 0; $i< count($words); $i++){
                       $where[] = '(d.documents_title like ? || d.documents_keywords like ?)'; 
                       $params[] = '%'.$words[$i].'%';
                       $params[] = '%'.$words[$i].'%';
                    }
                        
                    $where[] = 'd.documents_status = "active"';
                        
                    $where []= 'FIND_IN_SET("library", d.documents_type)';

                    $params []= $q;
                    $return = $db->fetch_all('
                        SELECT d.*, MATCH(d.documents_title, d.documents_keywords) AGAINST (?) AS score, mxd.members_x_documents_id AS in_library
                        FROM documents AS d
                        LEFT JOIN members_x_documents AS mxd ON (mxd.join_documents_id = d.documents_id)
                        '.strings::where($where).'
                        ORDER BY score DESC, documents_title
                        ', $params
                    );
                    
                    $file = 'docs.php';
                break;
                
                case "mydocs":
                    
                    $words =  search::getWordsInString($q);
                    for($i = 0; $i< count($words); $i++){
                       $where[] = '(d.documents_title like ? || d.documents_keywords like ?)'; 
                       $params[] = '%'.$words[$i].'%';
                       $params[] = '%'.$words[$i].'%';
                    }
                    
                    $where []= '(mxd.join_members_id = ? OR d.join_members_id = ?)';
                    $params[]= ActiveMemberInfo::GetMemberId();
                    $params[]= ActiveMemberInfo::GetMemberId();
                    
                    $where []= '(d.documents_type = "library" OR (d.documents_type = "personal" AND d.join_members_id = '.ActiveMemberInfo::GetMemberId().'))';
                    
                    $params[]= $q;
                    $return = $db->fetch_all('
                        SELECT d.*, MATCH(d.documents_title, d.documents_keywords) AGAINST (?) AS score
                        FROM documents AS d
                        JOIN members_x_documents AS mxd ON mxd.join_documents_id = d.documents_id
                        '.strings::where($where).'
                        ORDER BY score DESC
                        ', $params
                    );
                    
                    $file = 'my-docs.php';
                break;

                case "faqs":
                    $words =  search::getWordsInString($q);
                    for($i = 0; $i< count($words); $i++){
                       $where[] = '((faqs_question like ? || faqs_answer like ? || faqs_keywords like ?))'; 
                       $params[] = '%'.$words[$i].'%';
                       $params[] = '%'.$words[$i].'%';
                       $params[] = '%'.$words[$i].'%';
                    }
                    
                    $where []= 'faqs_answer_datetime != 0';
                    $where []= 'faqs_published = 1';

                    $params []= $q;
                    $return = $db->fetch_all('
                        SELECT *, MATCH(faqs_question, faqs_answer, faqs_keywords) AGAINST (?) AS score 
                        FROM faqs AS f
                        '.strings::where($where).'
                        ORDER BY score DESC, faqs_answer_datetime desc
                        ', $params
                    );
                    
                    
                    $file = 'faqs.php';
                break;
                case "forum":
                    
                    $words =  search::getWordsInString($q);
                    for($i = 0; $i< count($words); $i++){
                       $where[] = '(ft.topics_title like ? || fp.posts_body like ?)'; 
                       $params[] = '%'.$words[$i].'%';
                       $params[] = '%'.$words[$i].'%';
                    }
                    
                    $where []= '(ft.topics_status = "active"  )';
                    
                    
                    $return = $db->fetch_all('
                        SELECT  ft.*,fp.*
                        FROM forums f
                        JOIN forums_groups fg ON (fg.join_members_id = '.ActiveMemberInfo::GetMemberId().')
                        JOIN forums_groups_x_forums fgxf ON (fgxf.join_groups_id = fg.groups_id)
                        JOIN forums_groups_x_members fgxm ON (fgxm.join_groups_id = fgxf.join_groups_id)
                        JOIN forums_topics AS ft ON (ft.join_forums_id = f.forums_id)
                        JOIN forums_posts AS fp ON fp.join_topics_id = ft.topics_id 
                        '.strings::where($where).'
                        Group by posts_id
                        ', $params
                    );
                   
                    $file = 'forum.php';
                break;
            }
            
            $results []= array(
                'name' => $searchtype,
                'results' => $return,
                'file' => $file
            );
        }
        
        return $results;
    }
    public static function getWordsInString($string){
        $n_words = preg_match_all('/([a-zA-Z]|\xC3[\x80-\x96\x98-\xB6\xB8-\xBF]|\xC5[\x92\x93\xA0\xA1\xB8\xBD\xBE]){4,}/', $string, $match_arr);
        if(empty($match_arr[0])){
            return (array)$string;
        }
        return $match_arr[0] ;
    }
}
