<?php

final class surveys {

    public static function create_chart($surveys_id){
        $chartData   = new pData();
        $chartRender = new pChart(300,175);
        
        $db = mysqli_db::init();
        
        $options = $db->fetch_all('SELECT so.*, COUNT(DISTINCT sr.survey_responses_id) AS votes FROM survey_options AS so
            LEFT JOIN survey_responses AS sr ON sr.join_survey_options_id = so.survey_options_id
                WHERE so.join_surveys_id = ?
                GROUP BY so.survey_options_id
                HAVING votes > 0', 
            array($surveys_id)
        );
        
        if(!$options){
            return false;
        }
        
        foreach($options as $key => $option){
            $option_responses = $option['votes'];
            
            list($r, $g, $b) = explode(',', $option['survey_options_color']);
            
            $chartData->AddPoint($option_responses, "Serie1");
            $chartRender->setColorPalette($key, $r, $g, $b);
        }
        $chartData->AddAllSeries();
        
        $chartRender->drawFilledRoundedRectangle(7,7,293,168,5,240,240,240);
        $chartRender->drawPieGraph($chartData->GetData(), $chartData->GetDataDescription(), 150, 80, 110, PIE_NOLABEL, TRUE, 30, 15, 0);
        $chartRender->Render(SITE_PATH .'/images/surveys/survey_'.$surveys_id.'.png');
    }
    
    public static function available($members_id){
        $db = mysqli_db::init();
        
        $survey = $db->fetch_singlet('
            SELECT COUNT(*)
            FROM surveys AS s
            LEFT JOIN survey_responses AS sr ON sr.join_surveys_id = s.surveys_id AND sr.join_members_id = ?
            WHERE sr.survey_responses_id IS NULL
            GROUP BY s.surveys_id',
            array($members_id)
        );
        
        return (bool) $survey;
    }
    
    static function get_unanswered($members_id){
        $db = mysqli_db::init();
        
        $surveys = $db->fetch_all('
            select *
            from surveys
            left join survey_responses on
                join_surveys_id = surveys_id and
                join_members_id = ?
            where
                survey_responses_id is null and
                surveys_status = "active"',
            array($members_id)
        );
        
        return $surveys;
    }
}

?>
