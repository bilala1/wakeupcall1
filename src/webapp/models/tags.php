<?php

final class tags {
    public static function get($global_tags_id) 
    {
        $db = mysqli_db::init();
        return $db->fetch_one('SELECT * FROM global_tags WHERE global_tags_id = ?', array($global_tags_id));
    }
    
    public static function add_tags($tags_str, $breaker = ',')
    {
        $db = mysqli_db::init();
        
        $global_tags = new mysqli_db_table('global_tags');
        
        $tags = explode($breaker, $tags_str);
        $tag_ids = array();
        foreach($tags as $idx => $tag){
            $tag = trim($tag);
            if($tag){ // Make sure its not an empty tag...
                if(!$tag_id = $db->fetch_singlet('SELECT global_tags_id FROM global_tags WHERE global_tags_name = ?', array($tag))){
                    $global_tags->insert(array(
                        'global_tags_name' => $tag,
                        'global_tags_datetime' => SQL('UTC_TIMESTAMP()')
                    ));
                    
                    $tag_ids[] = $global_tags->last_id();
                }
                else {
                    $tag_ids[] = $tag_id;
                }
            }
        }
        return $tag_ids;
    }
    
    // Return the store tags...
    public static function add_tags_store($tags_str, $breaker = ',')
    {
        $db = mysqli_db::init();
        
        $store_tags = new mysqli_db_table('store_tags');
        
        $tags = explode($breaker, $tags_str);
        foreach($tags as $idx => $tag)
        {
            $tag = trim($tag);
            if($tag)
            { // Make sure its not an empty tag...
                if(!$tag_id = $db->fetch_singlet('SELECT store_tags_id FROM store_tags WHERE store_tags_tag = ?', array($tag)))
                {
                    $store_tags->insert(array(
                        'store_tags_tag' => $tag,
                        'store_tags_datetime' => SQL('UTC_TIMESTAMP()'),
                        'store_tags_total' => '1'
                    ));
                    
                    $tag_ids[] = $store_tags->last_id();
                }
                else 
                {
                    $tag_ids[] = $tag_id;
                }
            }
        }
        return $tag_ids;
    }
    
    public static function recalculate_forums()
    {
        $db = mysqli_db::init();
        
        $tags = $db->fetch_all('SELECT * FROM global_tags');
        foreach($tags as $tag)
        {
            $tag_id = $tag['global_tags_id'];
            
            $total_used = $db->fetch_singlet('SELECT COUNT(*) FROM forums_posts WHERE FIND_IN_SET(?, posts_tags)', array($tag_id));
            $db->query('UPDATE global_tags SET global_tags_forums = ? WHERE global_tags_id = ?', array($total_used, $tag_id));
        }
    }
    
    public static function recalculate_articles()
    {
        $db = mysqli_db::init();
        
        $tags = $db->fetch_all('SELECT * FROM global_tags');
        foreach($tags as $tag)
        {
            $tag_id = $tag['global_tags_id'];
            
            $total_used = $db->fetch_singlet('SELECT COUNT(*) FROM articles WHERE FIND_IN_SET(?, join_articles_tags)', array($tag_id));
            $db->query('UPDATE global_tags SET global_tags_articles = ? WHERE global_tags_id = ?', array($total_used, $tag_id));
        }
    }
    
    public static function recalculate_inventories(){
        $db = mysqli_db::init();
        
        $tags = $db->fetch_all('SELECT * FROM store_tags');
        foreach($tags as $tag)
        {
            $tag_id = $tag['store_tags_id'];
            
            $total_used = $db->fetch_singlet('SELECT COUNT(*) FROM inventories WHERE FIND_IN_SET(?, join_store_tags)', array($tag_id));
            $db->query('UPDATE store_tags SET store_tags_total = ? WHERE store_tags_id = ?', array($total_used, $tag_id));
        }
    }
    
    // Take a string of tag ID's and return the corresponding tags
    public static function parse_tags($tags_str){
        $db = mysqli_db::init();
        
        $tag_ids = explode(',', $tags_str);
        foreach($tag_ids as $tag_id){
            if($tag_id){ // Make sure it's not empty...
                if($thistag = $db->fetch_one('SELECT * FROM global_tags WHERE global_tags_id = ?', array($tag_id))){
                    $tags []= $thistag;
                }
            }
        }
        
        return $tags;
    }
    
    // Return a comma delimeted string of tag ID's
    public static function parse_tags_str($tags_str)
    {
        $db = mysqli_db::init();
        
        $tag_ids = explode(',', $tags_str);
        foreach($tag_ids as $tag_id)
        {
            if($tag_id)
            { // Make sure it's not empty...
                if($thistag = $db->fetch_singlet('SELECT global_tags_name FROM global_tags WHERE global_tags_id = ?', array($tag_id)))
                {
                    $tags []= $thistag;
                }
            }
        }
        
        return implode(',',(array)$tags);
    }
    
    public static function parse_tags_store($tags_str){
        $db = mysqli_db::init();
        
        $tag_ids = explode(',', $tags_str);
        foreach($tag_ids as $tag_id){
            if($tag_id){ // Make sure it's not empty...
                if($thistag = $db->fetch_singlet('SELECT store_tags_tag FROM store_tags WHERE store_tags_id = ?', array($tag_id))){
                    $tags []= $thistag;
                }
            }
        }
        
        return implode(',',(array)$tags);
    }
    
    public static function get_size($count, $count_max, $font_min = 11, $font_max = 22)
    {
        $ratio     = ($count / $count_max);
        $font_diff = ($font_max - $font_min) * $ratio;
        $font_size = $font_min + $font_diff;
        
        return floor($font_size);
    }
    
    public static function get_tags($tag_type, $options = array())
    {
        $db = mysqli_db::init();
        
        switch($tag_type)
        {
            case 'articles':
                $tag_type = 'global_tags_articles';
                break;
            case 'forums':
                $tag_type = 'global_tags_forums';
                break;
            default:
                return false;
        }
        
        $limit = 'LIMIT ' . ($options['limit'] ? (int)$options['limit'] : '10');
        
        $orderby = 'ORDER BY ' . ($options['order'] ? $db->escape_column($options['order']) : 'RAND()');
        
        $tags = $db->fetch_all('
            SELECT *
            FROM global_tags
            WHERE ' . $tag_type . ' > 0 ' 
            . $orderby . ' ' 
            . $limit
        );
        
        return $tags;
    }
    
    public static function get_forum_tags($forums_id, $options = array()){
        $db = mysqli_db::init();
        
        //get all topic ids in forum
        $topics_ids = $db->fetch_singlets('select topics_id from forums_topics where join_forums_id = ?', array($forums_id));
        
        //get all tag ids from posts in topics
        $tags_ids = $db->fetch_all('select GROUP_CONCAT(posts_tags) from forums_posts where join_topics_id in(' . $db->filter_in($topics_ids) . ')');
        $tags_ids = explode(',', $tags_ids);
        $tags_ids = array_unique($tags_ids);
        
        $limit = 'LIMIT ' . ($options['limit'] ? (int)$options['limit'] : '10');
        
        $orderby = 'ORDER BY ' . ($options['order'] ? $db->escape_column($options['order']) : 'RAND()');
        
        $tags = $db->fetch_all('
            SELECT *
            FROM global_tags
            WHERE
                global_tags_forums > 0 and
                global_tags_id in (' . $db->filter_in($tags_ids) . ') ' 
            . $orderby . ' ' 
            . $limit
        );
        
        return $tags;
    }
    
    public static function get_count($tag_type)
    {
        switch($tag_type)
        {
            case 'articles':
                $tag_type = 'global_tags_articles';
                break;
            case 'forums':
                $tag_type = 'global_tags_forums';
                break;
            default:
                return false;
        }
        $db = mysqli_db::init();
        return $db->fetch_singlet('SELECT MAX(' . $tag_type . ') FROM global_tags GROUP BY NULL');
    }
    
    
}

?>
