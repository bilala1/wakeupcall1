<?

class vendors
{

    public static function get_vendor($vendors_id)
    {
        if (!UserPermissions::UserCanAccessObject('vendors', $vendors_id, array('multiLocation' => true))) {
            return null;
        }

        $db = mysqli_db::init();

        $vendor_data = $db->fetch_one('
            SELECT v.*,
            (
                SELECT GROUP_CONCAT(join_licensed_locations_id SEPARATOR \',\')
                FROM vendors_x_licensed_locations vll
                WHERE vll.join_vendors_id = v.vendors_id
                ) as `join_licensed_locations`
            FROM vendors as v
            WHERE
            vendors_id = ?',
            array($vendors_id));

        if ($vendor_data['join_licensed_locations']) {
            $vendor_data['join_licensed_locations'] = explode(',', $vendor_data['join_licensed_locations']);
        }

        return $vendor_data;
    }

    private static $baseQuery = '
        SELECT SQL_CALC_FOUND_ROWS *,
        (SELECT count(1)
            FROM certificates
            WHERE join_vendors_id = vendors_id and certificates_hidden != 1)  as `certificates_count`,
        (
            IF(vendors_all_locations, \'All Locations\', (SELECT GROUP_CONCAT(licensed_locations_name SEPARATOR \', \')
            FROM vendors_x_licensed_locations vll
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE vll.join_vendors_id = v.vendors_id
            ))) as `licensed_locations_name`,
        (SELECT COUNT(1)
            FROM vendors_x_licensed_locations vll
            INNER JOIN licensed_locations ll on join_licensed_locations_id = licensed_locations_id
            WHERE vll.join_vendors_id = v.vendors_id
            ) as `licensed_locations_count`
        FROM vendors as v
        ';

    private static function build_query($wheres, $orderby, $paging, $do_paging)
    {
        $query = vendors::$baseQuery . strings::where($wheres) . strings::orderby($orderby, array('vendors','licensed_locations'));

        if ($do_paging)
            $query = $query . $paging->get_limit();

        return $query;
    }

    public static function get_paged_vendors_list(&$paging, $orderby_request, $searchTerm, $show_hidden, $licensed_locations_id)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $orderby = 'vendors_name';
        $params = array();

        if ($orderby_request) {
            $orderby = $orderby_request;
        } 

        if ($searchTerm) {
            $wheres[] = '(vendors_name LIKE ?)';
            $params[] = '%' + $searchTerm + '%';
        }

        if ($show_hidden == '0') {
            $_SESSION['vendors_show_hidden'] = 0;
            $wheres[] = '(vendors_hidden <> ?)';
            $params[] = '1';
        } else if ($show_hidden == 1 || $_SESSION['vendors_show_hidden'] == 1) {
            $_SESSION['vendors_show_hidden'] = 1;
            $wheres[] = '(vendors_hidden = ?)';
            $params[] = '1';
        } else {
            $wheres[] = '(vendors_hidden <> ?)';
            $params[] = '1';
        }

        $wheres[] = '(join_accounts_id = ?)';
        $params[] = $_SESSION['accounts_id'];

        if(ActiveMemberInfo::IsUserMultiLocation() && $licensed_locations_id)
        {
            if($licensed_locations_id)
            {
                $questions = array();
                foreach($licensed_locations_id as $id) {
                    $questions[] = '?';
                    $params[] = $id;
                }
                $questions = implode(',', $questions);
                $wheres[] = "(vendors_all_locations = 1 OR exists(select 1 from vendors_x_licensed_locations where join_vendors_id = vendors_id and join_licensed_locations_id in ($questions)))";
            }
        }

        $wheres[] = UserPermissions::CreateInClause('vendors', array('multiLocation' => true, 'checkView' => true));

        //get count
        $db->fetch_all(vendors::build_query($wheres, $orderby, $paging, false), $params);
        $paging->set_total($db->found_rows());

        //get page data
        $vendors = $db->fetch_all(vendors::build_query($wheres, $orderby, $paging, true), $params);

        return $vendors;
    }

    public static function get_all($include_id, $include_hidden = false)
    {
        $db = mysqli_db::init();

        $wheres = array();
        $params = array();

        $hidden = $include_hidden ? 1 : 0;

        if ($include_id) {
            $wheres[] = '(vendors_hidden = ? or vendors_id = ?)';
            $params[] = $hidden;
            $params[] = $include_id;
        } else {
            $wheres[] = '(vendors_hidden = ?)';
            $params[] = $hidden;
        }

        $ids = UserPermissions::AllObjectIdsForUser('vendors', array('multiLocation' => true, 'checkView' => true));
        if($include_id) {
            $ids[] = $include_id;
        }
        if(count($ids) < 1) {
            return array();
        }
        $wheres[] = "(vendors_id in (" . implode(', ', $ids) . "))";

        $query = str_replace('SQL_CALC_FOUND_ROWS', '', vendors::$baseQuery . strings::where($wheres));

        return $db->fetch_all($query, $params);
    }

    public static function delete($vendors_id) {
        $db = mysqli_db::init();
        $perms = UserPermissions::UserObjectPermissions('vendors', $vendors_id);

        if (UserPermissions::UserCanAccessObject('vendors', $vendors_id) && $perms['delete']) {
            $vendors_table = new mysqli_db_table('vendors');
            $vendor = $vendors_table->get($vendors_id);

            $vendors_table->delete($vendors_id);

            members::log_action(ActiveMemberInfo::GetMemberId(),
                'Vendor Deleted: ' . $vendor['vendors_name']);

            //delete certificates
            $cert_ids = $db->fetch_singlets('SELECT certificates_id FROM certificates WHERE join_vendors_id = ?', array($vendors_id));
            foreach($cert_ids as $cert_id) {
                certificates::delete($cert_id);
            }

            $notice = 'The vendor has been deleted';
            if(count($cert_ids) > 0) {
                $notice .= ', along with their certificates.';
            } else {
                $notice .= '.';
            }
        } else {
            $notice = 'You do not have permissions to delete this vendor.';
        }

        return array(
            'notice' => $notice
        );
    }
    public static function get_vendor_certificates($vendors_id){
        $db = mysqli_db::init();
        $certificates = $db->fetch_all('
        SELECT SQL_CALC_FOUND_ROWS c.*, ll.licensed_locations_name, cf.*, f.*
        FROM certificates AS c
        INNER JOIN licensed_locations AS ll on c.join_licensed_locations_id = ll.licensed_locations_id
        LEFT JOIN certificates_files cf on c.certificates_id = cf.join_certificates_id AND cf.certificates_files_active = 1
        LEFT JOIN files f on cf.join_files_id = f.files_id 
        WHERE c.certificates_hidden <> 1 and c.join_vendors_id = ?',array($vendors_id));
        return $certificates;
    }
    public static function get_vendor_locations_by_vendorid($vendors_id){
        $db = mysqli_db::init();
        $vendor_locations = $db->fetch_singlets('SELECT join_licensed_locations_id FROM vendors_x_licensed_locations WHERE join_vendors_id = ?',array($vendors_id));
        return $vendor_locations;
    }
}

?>