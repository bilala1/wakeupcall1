<?
final class vendors_history
{
    public static function create_vendors_entry($members_id, $vendors_id, $vendors_history_description)
    {
        $vendors_history_table = new mysqli_db_table('vendors_history');

        $vendors_history = array('join_members_id' => $members_id,
            'join_vendors_id' => $vendors_id,
            'vendors_history_datetime' => date("Y-m-d H:i:s"),
            'vendors_history_description' => $vendors_history_description);

        $vendors_history_table->insert($vendors_history);
        $vendors_history_id = $vendors_history_table->last_id();

        return $vendors_history_id;
    }

    public static function new_vendor_entry($members_id, $vendors_id)
    {
        return vendors_history::create_vendors_entry($members_id, $vendors_id, 'Vendor created.');
    }

    public static function vendor_location_updated_entry($members_id, $vendor, $old_vendors)
    {
        $history_text = '<b>Location Changed From: </b>' . $old_vendors['licensed_locations_name'] . '<b> To: </b>' . $vendor['licensed_locations_name'];

        return vendors_history::create_vendors_entry($members_id, $vendor['vendors_id'], $history_text);
    }

    public static function check_field_update($members_id, $field_name, $field_text, $vendor, $old_vendors)
    {
        if ($vendor[$field_name] != $old_vendors[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $old_vendors[$field_name] . '<b> To: </b>' . $vendor[$field_name];
            return vendors_history::create_vendors_entry($members_id, $vendor['vendors_id'], $history_text);
        }
    }

    public static function check_bit_field_update($members_id, $field_name, $field_text, $vendor, $old_vendors)
    {
        $values = array('False', 'True');
        if ($vendor[$field_name] != $old_vendors[$field_name]) {
            $history_text = '<b>' . $field_text . ' Changed From: </b>' . $values[$old_vendors[$field_name]] . '<b> To: </b>' . $values[$vendor[$field_name]];
            return vendors_history::create_vendors_entry($members_id, $vendor['vendors_id'], $history_text);
        }
    }

    public static function vendors_updated_entry($members_id, $vendor, $old_vendors)
    {

        if ($vendor && $old_vendors) {
            vendors_history::check_field_update($members_id, 'vendors_name', 'Vendors Name', $vendor, $old_vendors);
            vendors_history::check_field_update($members_id, 'vendors_contact_name', 'Contact Name', $vendor, $old_vendors);
            vendors_history::check_field_update($members_id, 'vendors_email', 'Email', $vendor, $old_vendors);
            vendors_history::check_field_update($members_id, 'vendors_phone', 'Phone', $vendor, $old_vendors);
            vendors_history::check_field_update($members_id, 'vendors_street', 'Street Address', $vendor, $old_vendors);
            vendors_history::check_field_update($members_id, 'vendors_services', 'Services Provided', $vendor, $old_vendors);
            vendors_history::check_bit_field_update($members_id, 'vendors_location_can_view', 'Locations can view', $vendor, $old_vendors);
            vendors_history::check_bit_field_update($members_id, 'vendors_location_can_edit', 'Locations can edit', $vendor, $old_vendors);

            $licensed_locations = licensed_locations::get_for_account(ActiveMemberInfo::GetAccountId());
            $licensed_locations_names = array();
            foreach ($licensed_locations as $licensed_location) {
                $licensed_locations_names[$licensed_location['licensed_locations_id']] = $licensed_location['licensed_locations_name'];
            }

            if ($vendor['vendors_all_locations']) {
                $vendor['licensed_locations'] = 'All Locations';
            } else {
                $join_licensed_locations = array();
                foreach ($vendor['join_licensed_locations'] as $join_licensed_locations_id) {
                    $join_licensed_locations[] = $licensed_locations_names[$join_licensed_locations_id];
                }
                sort($join_licensed_locations);
                $vendor['licensed_locations'] = implode(', ', $join_licensed_locations);
            }

            if ($old_vendors['vendors_all_locations']) {
                $old_vendors['licensed_locations'] = 'All Locations';
            } else {
                $join_licensed_locations = array();
                foreach ($old_vendors['join_licensed_locations'] as $join_licensed_locations_id) {
                    $join_licensed_locations[] = $licensed_locations_names[$join_licensed_locations_id];
                }
                sort($join_licensed_locations);
                $old_vendors['licensed_locations'] = implode(', ', $join_licensed_locations);
            }
            vendors_history::check_field_update($members_id, 'licensed_locations', 'Location', $vendor, $old_vendors);
        }
    }

    public static function hidden_changed($members_id, $vendors_id, $new_value)
    {
        $history_text = "<b>Vendor " . ( $new_value ? "Hidden" : "Unhidden" ) . "</b>";

        return vendors_history::create_vendors_entry($members_id, $vendors_id, $history_text);
    }

    public static function get_vendors_history_with_data($members_id, $vendors_id)
    {
        $db = mysqli_db::init();

        $vendors_history = $db->fetch_all('SELECT * FROM vendors_history WHERE join_vendors_id = ? order by vendors_history_datetime', array($vendors_id));

        return $vendors_history;
    }
}

?>