<?php
class webinars{
    static function is_upcoming($webinar){
        return (strtotime($webinar['webinars_datetime']) > strtotime(times::to_mysql_utc(date(DATE_FORMAT_FULL))));
    }
}