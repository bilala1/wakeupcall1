<link type="text/css" rel="stylesheet" href="/css/multiBox.css" />
<style type="text/css">
	.MultiBoxDescription, .MultiBoxTitle, .MultiBoxControlsContainer, .MultiBoxControls {
		height: 0px; 
		line-height: 0px; 
		padding: 0px; 
		margin: 0px; 
		display: none;
	}
</style>
<!--[if lte IE 6]>
    <link type="text/css" rel="stylesheet" href="/css/multiBoxIE6.css" />
<![endif]-->
<!--
<script type="text/javascript" src="/js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="/js/mootools-more-1.3.2.1.js"></script>
<script type="text/javascript" src="/js/multiBox.js"></script>
<script type="text/javascript" src="/js/swfobject/src/swfobject.js?v=1"></script>
<script type="text/javascript" src="AC_RunActiveContent.js"></script>
-->
<script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>

<script type="text/javascript">
	var BASEURL = '<?= BASEURL ?>';
	var FULLURL = '<?= FULLURL ?>';
/*	 
window.addEvent('domready', function(){
   
    //call multiBox
    var initMultiBox = new multiBox({
        mbClass: '.mb',//class you need to add links that you want to trigger multiBox with (remember and update CSS files)
        container: $(document.body),//where to inject multiBox
        descClassName: 'multiBoxDesc',//the class name of the description divs
        path: '/Files/',//path to mp3 and flv players
        //useOverlay: true,//use a semi-transparent background. default: false;
        //maxSize: {w:600, h:400},//max dimensions (width,height) - set to null to disable resizing
        addDownload: false,//do you want the files to be downloadable?
        //addRollover: true,//add rollover fade to each multibox link
        //addOverlayIcon: true,//adds overlay icons to images within multibox links
        addChain: true,//cycle through all images fading them out then in
        recalcTop: true,//subtract the height of controls panel from top position
        addTips: true,//adds MooTools built in 'Tips' class to each element (see: http://mootools.net/docs/Plugins/Tips)
        autoOpen: 0//to auto open a multiBox element on page load change to (1, 2, or 3 etc)
    });
   

 
$('mb1').addEvent('click',function(){
    $('video').setStyle('display','block');
	$f("video", {
			src: "flowplayer-3.2.7.swf", 
			wmode: "transparent"
		}, {
		wmode: 'transparent',
		key: '#$4902077e83495bce003',
		clip: {
			url: 'wcc.flv',
			onFinish: function() {
				$('video').setStyle('display','none');
			} 
		}
	});
    return false;
 })
     
});
	 */
	/*
swfobject.registerObject("FLVPlayer", "9.0.0", "<?= FULLURL ?>/js/swfobject/expressInstall.swf", function (e) {
	if (e.success === false) {
		document.getElementById('fallback').set('data', '');
	}
});
	 */
</script>




<img src="/images/img-signup.jpg" class="left"/>
<div class="w400 left" style="margin-left:20px">
    <h2 class="headline">Your Membership Provides:</h2>


    <div id="accordion">    
        <h3 class="toggler t1">The One-Stop Site for Online Hospitality Risk Management</h3>
        <div class="element e1">
            <p><strong>Intuitive Navigation &amp; Interface</strong> requires minimal computer skills to access all areas. Ease of 
				use is a priority throughout the design process of the entire WAKEUP CALL site.</p>

            <p><strong>Human Resources Services</strong> including unlimited access to speak with live specialists 
				providing state-specific information and forms.</p>

            <p><strong>Employment Law Email Hotline</strong>, at no additional cost you can contact our law firm, 
				with offices coast to coast, about employment law issues and concerns. </p>

            <p><strong>Live Message Boards/Forums</strong> allow you to discuss general and specific topics with all WAKEUP CALL 
				members. Attorneys and Risk Managers provide answers to your questions, specifically for you and your location. </p>

            <p><strong>Intranet Option Included for Owners of Multiple Locations</strong> It facilitates private corporate postings 
				and data storage for Quick &amp; Easy communications between you and all your properties.</p>

            <p><strong>The Claims Management</strong> function allows claim submissions directly to your carriers, while WAKEUP CALL
				provides a log for your records. Helps to manage losses and trending and keeps claims from getting out 
				of hand while updating your entire team. </p>

            <p><strong>Certificate of Insurance Monitoring</strong> keeps your risk transfer program up-to-date and easy to manage.</p>

            <p>WAKEUP CALL is your single source for <strong>Up-to- Date News</strong> on the hospitality industry, 
				from multiple sources. </p>
        </div>

        <h3 class="toggler t2">Finally, the Solutions You Need are All in One Place</h3>
        <div class="element e2">
            <p><strong>24/7 Online Training</strong>, including the California required AB1825 Sexual Harassment, designed for you and your team's 
				convenience. Users can login and take the training courses you select for them, at their own pace. You are provided with 
				reports on the progress and completion of everyone's training, including Certificates of Completion.</p>

            <p>The extensive <strong>Library of Industry-Specific, Downloadable Documents and Forms; Includes All Required OSHA Documents, </strong>
				incident reports, HR forms, EE handouts, sample waivers and more.</p>

            <p><strong>No More Reliance on a Carrier or Broker</strong> requiring you to contact their risk management phone representative to 
				locate and access their limited, generic resources. With WAKEUP CALL as your sole resource, you are able to manage 
				and locate all resources, on all topics, 24/7.</p>

            <p>The WAKEUP Call <strong>Searchable SDS Library</strong> provides the tools you need to remain in compliance with SDS OSHA requirements.</p>

            <p><strong>The FAQ Section</strong>, a continuously populated, up-to-date feature provides you access to the answers to the most commonly 
				asked risk management questions.</p>

            <p><strong>Maintain an Open Line of Communication</strong> with WAKEUP CALL to learn of recommended current actions, or to 
				learn about future services. WAKEUP CALL is a dynamic site, changing as the hospitality industry changes. 
				In addition to its own development team, WAKEUP CALL is constantly seeking input from members to keep current on 
				your most critical issues.</p>
        </div>

        <h3 class="toggler t3">Free and Discounted Valuable Services</h3>
        <div class="element e3">
            <p>You will have access to national vendors and service-providers, free or at discounted rates exclusively 
				for WAKEUP CALL members. Services include background search, drug testing, MVR's, site inspections and direct 
				access to attorneys at our members-only discounted rates. </p>

            <p>WAKEUP CALL has timely Webinars on a regular basis to provide valuable insight for your team on the 
				hottest topics and areas of concern in the hospitality industry.</p>        
        </div>

        <h3 class="toggler t4">Member Testimonials</h3>
        <div class="element e4">

            <div class="divider">
                <p><em>&quot;WAKEUP CALL is becoming our only &quot;go-to&quot; resource. If we have any questions 
						regarding any risk management issues, we go to WAKEUP CALL.&quot;</em></p>

                <p class="title">U. Anderson- Orlando, FL</p>
            </div>

            <div class="divider">
                <p><em>&quot;The WAKEUP CALL website is making it easy for us to stay on top of our safety program. We login and get what we need quickly.&quot;</em></p>

                <p class="title">J. Patel - Las Vegas, NV</p>
            </div>

            <div class="divider">	
                <p><em>&quot;Having an easy to use resource like WAKEUP CALL available to my staff 24 hours a day has made 
						it easy to implement a safety program, which we couldn't manage before&quot;</em></p>

                <p class="title">S. Patula - Pittsburgh, PA</p>
            </div>

            <div class="divider">
                <p><em>&quot;Running 6 locations, we needed a solution to ensure that all of our hotels utilize the same, up 
						to date procedures, tools and materials. WAKEUP CALL has made managing our hotels much easier by using their 
						Corporate Forum and Training Programs.&quot;</em></p>
                <p class="title">S. Correa - Petaluma, CA </p>
            </div>                                                            

            <div class="divider">
                <p><em>&quot;As a smaller property, we don't have the budget for a full time HR Director. WAKEUP CALL has saved 
						us Attorney costs by providing us access to HR professionals and Attorneys, properly addressing our concerns. &quot; </em></p>

                <p class="title">R. Stevens - Austin, TX</p>
            </div>
        </div>

        <h3 class="toggler t5" id="included-free-tab">What's included in my WAKEUP CALL 15-day trial? </h3>
        <div class="element e5">
            <p>Our free trial provides you the opportunity to view the features and functions of the 
				WAKEUP CALL site before joining. The free trial limitations are:</p>

            <ul>
                <li>You will see the volume of content and how it's organized. However, only members are able to open or download any content.</li> 
                <li>You will be able to see all the training topics, but you will not have access to all the online training. </li>
            </ul>

            <p>Those are the only limitations because we are confident that you will see the value of WAKEUP CALL as an 
				ongoing valuable resource so that you will want to join. There are no obligations for the free trial other than 
				we ask you to take full advantage of the 15-day free period to thoroughly explore WAKEUP CALL. You will see for 
				yourself how it fulfills your company's needs and become your most relied upon resource.</p>
        </div>

        <h3 class="toggler t6" id="pricing-tab">Introductory Membership Pricing</h3>
        <div class="element e6">
            <h4>Is value more important than a low price in this economy? </h4>
            <p>WAKEUP CALL accommodates both of your concerns. We designed the site to provide unique value and kept our costs in-check 
				by employing our unique knowledge of Hospitality-Specific Risk Management. We built the site to accommodate your needs.</p>

            <h4>What other single site provides you:</h4>

            <ul>
                <li>An HR specialist to speak with </li>
                <li>An Employment Law Attorney to answer your questions</li>
                <li>A constantly updated library containing current documents for your OSHA, Safety and HR needs</li>
                <li>35 online training courses, including sexual harassment</li>
                <li>Message Boards/Forums with many participating hospitality risk managers and attorneys</li>
                <li>A complete and up-to-date SDS library</li>
                <li>The latest breaking industry news</li>
                <li>An Intranet option to serve multiple locations</li>
            </ul>

            <h4>Your single source is only $<?= YEARLY_COST ?> PER YEAR. </h4>

            <strong><span style="color:#E03D30">(LIMITED TIME INTRODUCTORY PRICING)</span></strong> 
            <br />
            <small>(Options available for multiple locations)</small>

            <p>For multiple locations: Your corporate office is included at no additional charge if at least two locations are part of the 
				group and the corporate office is not located in one of the Hotels/Resorts/Spas. </p>

            <h3>WAKEUP CALL is the unique website containing your entire needs, specifically for hospitality risk management!</h3> 

        </div>        

	</div>

    <div align="center">
        <a href="what-is-wuc.php" id="mb1" class="mb block blue-btn-long" rel="[images,noDesc]" data-mb-props="{movieSize: {w:480,h:270}}" style="margin:15px 0 5px 30px">What is WAKEUP CALL?</a>
        <p class="clear" style="color:#000">Play video</p>
    </div>

	<div id="video">

	</div>

</div>

<script type="text/javascript">
	//<![CDATA[
	FULLURL = '<?= FULLURL ?>';
    
	window.addEvent('domready', function() {
		//-- 
		
		var mb_props = $$('[data-mb-props]');
		if (mb_props.length > 0) {
			mb_props.addEvent('mousedown', function (evt) {
				var props = JSON.decode(mb_props.get('data-mb-props').pop());
				initMultiBox.setOptions(props);
			});
		}
	
		var myAccordion = new Accordion($('accordion'), 'h3.toggler', 'div.element', {
			opacity: false,
			onActive: function(toggler, element) {
				toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle_down.jpg") no-repeat scroll 0 10px transparent');
			},
			onBackground: function(toggler, element) {
				toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle.jpg") no-repeat scroll 0 10px transparent');
			},
			display: -1
		});
    	
		$$('h3[class^=toggler]').each(function(e) {
			e.addEvent('mouseover',function(){
				this.setStyle('background-color','#ffe9c9');
			});
			e.addEvent('mouseout',function(){
				this.setStyle('background-color','white');
			});
        
			e.addEvent('click', function(){
				var number = parseInt(e.get('class').match(/[0-9]/gi));
				$$('.element').each(function(e) {
					if(e.get('class').match(number)){
						if(e.get('style').match(/height: auto/gi)){
							myAccordion.display(-1);
						}
					}
				});     
			})
		});
	});
	//]]>
</script>
