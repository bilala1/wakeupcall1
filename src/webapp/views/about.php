<?  
$page_title  = 'WAKEUP CALL Risk Management Tools - About WAKEUP CALL';
$page_description = 'The founders of the WAKEUP CALL website have served the hospitality industry, in all aspects of risk management, for over 20 years. We provide tools, training and resources for risk management industry such as, property inspections, onsite training, OSHA compliance resources, HR services and other areas requested by our clients.';

include VIEWS. '/header.php';
?>
    <div id="content">
<!--    	 <img src="/images/header-about.jpg" alt="About Us" id="top-image" />-->
    
        <div class="w540 left" id="leftcol">
            <img src="/images/img-about.jpg" class="left"/>
            
            <div class="w400 left" style="margin-left:20px">
            	<h2 class="headline">About Us</h2>
                <p>The founders of the WAKEUP CALL website have served the hospitality industry, in all aspects of 
                risk management, for over 20 years. We began as an insurance brokerage, dedicated to hotels. We 
                extended our national services beyond hotels to include resorts and spas. This expansion provided the 
                increased expertise in risk management services such as, property inspections, onsite training, OSHA 
                compliance resources, HR services and other areas requested by our clients.</p> 
                
                <p>We recognized the need for risk management services, when the department began to grow faster than 
                the brokerage. By providing 20-plus years of in depth service to a single industry, our team has become 
                nimble and knowledgeable about virtually all hotel, resort and spa risk exposures. Our decades of expertise 
                in hospitality risk management lead to the creation of the WAKEUP CALL website.</p>
                
                <p>Our collective decades of experience and expertise provided a unique foundation on which to build a 
                powerful website, allowing you to do online what we have been doing in onsite, in person. Until now the 
                hospitality industry has been under serviced by a couple of general resources, providing minimal support. 
                No other single-source provides complete, real-time information and tools to manage emerging issues. The 
                WAKEUP CALL website content was created for the hospitality industry's specific risk management needs.</p>
                
                <p>WAKEUP CALL, it's all here except for insurance - you have a broker for that. </p>
            </div>
        
        </div>
        
         <? include  VIEWS. '/register.php'; ?>

        <br class="clear" />
    	
    </div>
    
<? include  VIEWS. '/footer.php'; ?>
