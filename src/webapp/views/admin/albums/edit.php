<?
$page_title = 'Edit Album | Admin';
$page_name = 'albums';
$sub_name = 'edit';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <? /*<form id="form" method="post" action="/admin/albums/edit.php" enctype="multipart/form-data" style="width:100%">
    <input type="hidden" name="albums_id" value="<?= $album['albums_id'] ?>" />
    
    
    <fieldset class="alt1">
    	<legend>Group Informaiton</legend>     
    <table class="form">
        <tr>
            <td>
                <label for="forums_name">Album Category:</label>
            </td>
            <td>
                <?= html::select($album_list, 'albums_parent', $album['albums_parent']) ?>
            </td> 
        </tr>
        <tr>
            <td>
                <label for="albums_name">Album Name:</label>
            </td>
            <td>
                <input type="text" id="albums_name" name="albums_name" value="<?= html::filter($album['albums_name']) ?>" size="30" maxlength="100" />
            </td> 
        </tr>
        <tr>
            <td>
                <label for="forums_desc">Album Icon:</label>
            </td>
            <td>
                <input type="file" name="albums_icon" />
            </td> 
        </tr>
    </table>
    
    <?= html::submit('Save Album', array('class' => 'submit center')) ?>
    </fieldset>
    </form>*/ ?>
    
     <? /* if(isset($_REQUEST['albums_id'])): */ ?>
	<?

	/*
    <a class="add" href="/admin/albums/photos/edit.php?albums_id=<?= $_REQUEST['albums_id'] ?>">Add Photo</a><br clear="right"/>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('albums_photos_name', 'Name') ?></td>
          <td class="thead"><?= strings::sort('albums_photos_datestamp', 'Upload Date',null,null,null, 'DESC') ?></td>
          <td class="thead" width="100">Actions</td> 
        </tr> 
    </thead> 
    <tbody>
        <? if(empty($photos)): ?>
            <tr><td colspan="3" align="center">
                <br/>Sorry, there are no items found.
            </td></tr>
        <? endif ?>
        <? foreach($photos as $photo): ?>
        <tr> 
            <td class="tbody"><?= $photo['albums_photos_name'] ?></td>
            <td class="tbody"><?= $photo['albums_photos_datestamp'] ?></td>
            <td class="tbody">
            <a href="/admin/albums/photos/edit.php?albums_photos_id=<?= $photo['albums_photos_id'] ?>" title="Edit">Edit</a> |
            <a onclick="if(!confirm('Are you sure you want to remove this photo')) { return false; }" href="/admin/albums/photos/delete.php?albums_id=<?= $album['albums_id'] ?>&albums_photos_id=<?= $photo['albums_photos_id'] ?>" title="Delete">Delete</a>
          </td>
        </tr> 
        <? endforeach ?>
    </tbody>
    </table>
	*/ ?>
	<style type="text/css">
		.photo-thumb-wrapper {
			width:215px;
			height:80px;
			border:1px solid #c4dae3;
			background:#fff;
			padding:2px;
			padding:2px;
			margin:5px;
			float:left;
			font-size:12px;
		}
		.photo-thumb-container {
			width:100%;
			height:60%;
			overflow:hidden;
			cursor:move;
		}
		.photo-thumb {
			width:100%;
		}
		.today {
			background-color:#faeebe;
			border: 1px solid #e3b709;
		}
	</style>

    <?php include VIEWS .'/admin/notice.php' ?>

    <form id="form" action="/admin/albums/edit.php" method="post" enctype="multipart/form-data" style="width:auto;">

        <fieldset class="alt1">
            <legend>Album Detail</legend>
            <div class="clear">
                <?= html::textfield('Name:', $album['albums_name'], 'albums_name', array(), $errors) ?>
                <?= html::textfield('Folder:', $album['albums_root_folder'], 'albums_root_folder', array(), $errors) ?>
                <input type="hidden" name="albums_id" value="<?= $_REQUEST['albums_id'] ?>" />


                <?//= html::checkboxfield('Active:', 1, 'franchises_active', $franchise['franchises_active']) ?>

                <input type="hidden" name="albums_id" value="<?= $_REQUEST['albums_id'] ?>" />
            </div>
            <div class="clear">
                <input value="Save" class="submit" type="submit">
            </div>

        </fieldset>

    </form>

    <?  if(isset($_REQUEST['albums_id'])):  ?>

	<form action="batch-upload.php?join_photos_albums_id=<?= $_REQUEST['albums_id'] ?>" method="post" enctype="multipart/form-data">
		<fieldset class="alt1">
			<legend>Upload Photos</legend>
			<input type="file" name="photos[]" multiple="multiple" />
			<input type="submit" class="submit" value="Upload" />
		</fieldset>
	</form>
	<form action="edit.php?albums_id=<?= $_REQUEST['albums_id'] ?>" method="post">
		<fieldset class="alt1">
			<legend>Sort Photos</legend>
			<div class="clear">
				<? if($_REQUEST['albums_id'] == 13): ?>
                <a href="#today">Jump to today's banner</a>
				<? endif ?>
                <input type="submit" class="submit" style="float:right;" value="Save" />
			</div>	
			<div id="photo-list" class="clear">
			<? foreach($photos as $photo): ?>
				<div class="photo-thumb-wrapper draggable <?= $photo['display_date'] == $today ? 'today' : ''?>" <?= $photo['display_date'] == $today ? 'id="today"' : ''?>>
					<div class="photo-thumb-container">
						<img class="photo-thumb" src="<?=$photo_directory?><?=$photo['albums_photos_filename']?>.org.jpg" />
					</div>
                    <div style="padding:0 10px 5px 10px;">
					<strong class="block"><?=$photo['albums_photos_name']?></strong><br/>
					<em class="photo-thumb-date"><?=get_display_date($photo['albums_photos_sort'])?></em>
					<span style="float:right;">
						<a href="photos/edit.php?albums_photos_id=<?=$photo['albums_photos_id']?>">Edit</a> | 
						<a href="photos/delete.php?albums_photos_id=<?=$photo['albums_photos_id']?>&albums_id=<?=$_REQUEST['albums_id']; ?>" onclick="return confirm('Delete this image?')">Delete</a>
					</span>
					<input type="hidden" class="photo-thumb-input" name="photo[<?=$photo['albums_photos_sort']?>]" value="<?=$photo['albums_photos_id']?>" />
                    </div>
                    <br class="clear" />
				</div>
			<? endforeach ?>
			</div>
			<div class="clear">
				<input type="submit" class="submit" style="float:right;" value="Save" />
			</div>
		</fieldset>
	</form>
    <? endif ?>
    
    
</div>

<script type="text/javascript">
	var date_list = <?= json_encode($date_list); ?>;
	var today = "<?=date('m/d/Y')?>";
	
	window.addEvent('domready', function() {
		var myDrag = new Sortables($('photo-list'), {
			clone: true,
			onComplete: function() {
				$$('.photo-thumb-wrapper').each(function(item, index) {
					item.getElements('.photo-thumb-date')[0].set('html', date_list[index]);
					item.getElements('.photo-thumb-input')[0].set('name', 'photo[' + index + ']');
					if (date_list[index] == today) {
						item.addClass('today');
						item.set('id', 'today');
					} else {
						item.removeClass('today');
						item.removeAttribute('id');
					}
				});
			}
		});
	});
</script>

<? include(VIEWS . '/admin/footer.php'); ?>
