<?
$page_title = 'Albums List | Admin';
$page_name = 'albums';
$sub_name = 'albums';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? // include(VIEWS . '/admin/members/header.php') ?>
        <a href="/albums/list.php" class="<?= $sub_name == 'albums' ? 'navbar_active' : '' ?>">Albums</a><br clear="left"/>
    </div>
    <a href="/admin/albums/edit.php" class="add">Add Album</a><br clear="right"/>

    <? if($notice): ?>
        <br /><p class="notice">
            <?= $notice ?>
        </p>
    <? endif ?>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <? /*if(permissions::has_type('Album Admin Access')): ?>
    <a href="/admin/albums/edit.php" class="add">Add Album</a><br clear="right"/>
    <? endif*/ ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('albums_name', 'Album') ?></td> 
          <td class="thead"><?= strings::sort('photos', 'Photos', null,null,null,'DESC') ?></td> 
          <td class="thead" width="100">Actions</td>
        </tr> 
    </thead> 
    <tbody>
        <? if(empty($albums)): ?>
            <tr><td colspan="3" align="center">
                <br/>Sorry, there are no albums found.
            </td></tr>
        <? endif ?>
        <? foreach($albums as $album): ?>
        <tr> 
          <td class="tbody"><?= $album['albums_name'] ?></td>
          <td class="tbody"><?= $album['photos'] ?></td>
          <td class="tbody">
            <? if(permissions::has_type('Album Admin Access')): ?>
            <a href="/admin/albums/edit.php?albums_id=<?= $album['albums_id'] ?>" title="Edit">Edit</a>
            <? endif ?>
    
            <? /*if(permissions::has_type('Album Admin Access')): ?>
            | <a onclick="if(!confirm('Are you sure you want to remove this album')) { return false; }" href="/admin/albums/delete.php?albums_id=<?= $album['albums_id'] ?>" title="Delete">Delete</a>
            <? endif*/ ?>
          </td>
        </tr> 
        <? endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="3"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
