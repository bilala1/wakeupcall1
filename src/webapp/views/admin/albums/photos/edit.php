<?
$page_title = 'Edit Photo | Admin';
$page_name = 'edit';
$sub_name = 'edit';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? // include(VIEWS . '/admin/forums/header.php'); ?>
         <a href="/admin/albums/edit.php?albums_id=<?= $album['albums_id'] ?>" class="navbar_active"><?= $album['albums_name'] ?></a>
    </div>
    <h2><?= isset($_REQUEST['albums_photos_id']) ? 'Edit Photo in' : 'Add Photo to' ?> <a href="/admin/albums/edit.php?albums_id=<?= $album['albums_id'] ?>"><?= $album['albums_name'] ?></a></h2>
    <form id="form" method="post" action="/admin/albums/photos/edit.php" enctype="multipart/form-data">
        <? if(isset($_REQUEST['albums_photos_id'])): ?>
        <input type="hidden" name="albums_photos_id" value="<?= $_REQUEST['albums_photos_id'] ?>" />
        <? else: ?>
        <input type="hidden" name="join_albums_id" value="<?= $_REQUEST['albums_id'] ?>" />
        <? endif ?>
        
        <? include(VIEWS.'/notice.php') ?>

        <fieldset class="alt1">
            <legend>Photo Informaiton</legend>        
        <table class="form">
            <? /*<tr>
                <td><label for="albums_photos_name">Album:</label></td>
                <td><?= html::select($album_list, 'join_albums_id', $join_albums_id) ?> </td> 
            </tr>*/ ?>
            <tr>
                <td><label for="forums_name">Photo Name:</label></td>
                <td><input type="text" id="albums_photos_name" name="albums_photos_name" value="<?= html::filter($photo['albums_photos_name']) ?>" size="30" maxlength="100" /></td> 
            </tr>
            <tr>
                <td><label for="forums_name">Photo:</label> </td>
                <td>
                    <img src="<?=empty($album['albums_root_folder'])? '/data/albums/':$album['albums_root_folder']."/"?>/<?= html::filter($photo['albums_photos_filename']) ?>.org.jpg" alt="photo" /><br />
                    <input type="file" name="albums_photos_filename">
                    (recommended size: 640px x 116px)
                </td> 
            </tr>
        </table>
        
        <?= html::submit('Save Photo', array('class' => 'submit center')) ?>
        </fieldset>
    </form>

</div>
<script type="text/javascript">
window.addEvent('load', function () {
    var myTips = new Tips('.moreinfo');
});
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
