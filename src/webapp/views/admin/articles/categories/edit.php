<?php
$page_title = 'Edit > Blog Comments | ' . SITE_NAME;
$page_name = 'blog';
$sub_name = 'categories';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php'); ?>
    </div>
     <h2><?= $_REQUEST['articles_categories_id'] ? 'Edit' : 'Add' ?> Blog Category</h2>
    <form id="form" class="appnitro"  method="post" action="/admin/articles/categories/edit.php">
        <input type="hidden" name="articles_categories_id" value="<?= $articles_category['articles_categories_id'] ?>" />
        <?php if($notice): ?>  
        <p class="notice"><?= $notice ?></p>
        <?php endif ?>  
	<fieldset class="alt1">
    	<legend>Blog Category</legend>               
        <table>
        <tr>
            <td>
                <label class="description" for="articles_categories_name">Name</label>
            </td>
            <td>
                <?= html::input($articles_category['articles_categories_name'], 'articles_categories_name', array('class' => 'moreinfo', 'title' => 'Name of the category', 'size' => '80')) ?>
            </td> 
        </tr>
        
        <tr>
            <td>
                <label class="description" for="articles_categories_description">Description</label>
            </td>
            <td>
                <?= html::textarea(
                    $articles_category['articles_categories_description'],
                    'articles_categories_description', 
                    array(
                    'class' => 'moreinfo', 
                    'title' => 'Description of the category', 
                    'cols'  => '80',
                    'rows'  => '8'
                    )
                ) ?>
            </td> 
        </tr>
    
        <tr>
            <td>
                <label class="description" for="articles_categories_active">Published:&nbsp;</label>
            </td>
            <td>
                <select class="moreinfo" title="If the blog is viewable to the public" id="articles_active" name="articles_categories_active"> 
                    <?= html::option(1, 'Published', $article['articles_categories_active']) ?>
                    <?= html::option(0, 'Unpublished', $article['articles_categories_active']) ?>
                </select>
            </td>        
        </tr>  
        
        <tr>
            <td></td>
            <td><?= html::submit('Save Category', array('class' => 'submit center')) ?></td>
        </tr>
        </table>
        </fieldset>
    </form> 
</div>
<script language="javascript" type="text/javascript">
window.addEvent('domready', function () {
    var myTips = new Tips('.moreinfo');
});
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
