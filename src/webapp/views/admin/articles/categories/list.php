<?php
$page_title = 'Blog | Admin';
$page_name = 'blog';
$sub_name = 'categories';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php') ?>
    </div>
    <?php include(VIEWS . '/admin/notice.php') ?>
    
    <form action="/admin/articles/categories/list.php" method="get">
        <p><fieldset class="alt1">
        <legend>Search</legend>
        Term or Keyword:
        <?= html::input($_REQUEST['term'], 'term', array('class' => 'element text small')) ?>
        <?= html::submit('Search', array('class' => 'submit')) ?>
        <a href="/admin/articles/categories/list.php" class="reset">Clear Search</a>
        </fieldset></p>
    </form>

    <a href="/admin/articles/categories/edit.php" class="add">Add Category</a><br clear="right"/>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead>
        <tr>
          <td class="thead"><?= strings::sort('articles_categories_name', 'Name') ?></td>
          <td class="thead moreinfo" title="View all blogs from this category"><?= strings::sort('total_articles', 'Articles', null, null, null, 'DESC') ?></td>
          <td class="thead" width="100"></td>
        </tr>
    </thead>
    <tbody>
        <?php if(empty($categories)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no categories found.
            </td></tr>
        <?php endif ?>
        <?php foreach($categories as $category): ?>
        <tr> 
          <td class="tbody"><?= $category['articles_categories_name'] ?></td>
          <td class="tbody"><a href="/admin/articles/list.php?join_articles_categories_id=<?= $category['articles_categories_id'] ?>" style="display: block; height: 100%" title="This category's blogs">View Blogs (<?= $category['total_articles'] ?>)</a></td> 
          <td class="tbody">
            <a href="/admin/articles/categories/edit.php?articles_categories_id=<?= $category['articles_categories_id'] ?>" title="Edit">Edit</a> |
            <?php if($category['total_articles'] == 0): ?>
            <a onclick="if(!confirm('Are you sure you want to remove this blog')) { return false; }" href="/admin/articles/categories/delete.php?articles_categories_id=<?= $category['articles_categories_id'] ?>" title="Delete">Delete</a>
            <?php else: ?>
            <a onclick="alert('You must delete the blogs in this category first'); return false;" href="#" title="Delete">Delete</a>
            <?php endif ?>
          </td>
        </tr> 
        <?php endforeach ?>
    </tbody>
    	<tr>
        	<td colspan="3">
            	<img src="/images/admin/table-bottom.jpg" width="960"/>
            </td>
        </tr>
    </table>
        <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
