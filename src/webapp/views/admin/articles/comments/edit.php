<?php
$page_title = 'Edit > Blog Comments | ' . SITE_NAME;
$page_name = 'blog';
$sub_name = 'comments';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/MooTime.js"></script>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php'); ?>
    </div>
	<h2><?= $_REQUEST['articles_comments_id'] ? 'Edit' : 'Add' ?> Blog</h2>
    <form id="form" class="appnitro"  method="post" action="/admin/articles/comments/edit.php">
        <input type="hidden" name="articles_comments_id" value="<?= $article_comment['articles_comments_id'] ?>" />
        <?php include(VIEWS . '/admin/notice.php') ?>
	<fieldset class="alt1">
    	<legend>Blog Information</legend>        
        <table>
    <tr>
        <td>
            <label for="date_mont">Publish Date</label>
        </td>
        <td>
        <span>
            <input type="text" id="publish_date" name="publish_date" value="<?= times::tz_convert($article_comment['articles_comments_post_date'], 'Y-m-d',false) ?>" />
        </span>
        </td>
    </tr> 
          
    <tr>
        <td>
            <label class="description" for="hour">Publish Time</label>
        </td>
        <td>
            <input type="text" name="publish_time" id="publish_time" value="<?= times::tz_convert($article_comment['articles_comments_post_date'], 'h:i a', false) ?>" id="timepick" />
        </td>
    </tr> 

        <tr>
            <td>
                <label class="description" for="article_id">Blog</label>
            </td>
            <td>
                <select class="select" name="join_articles_id">
                    <?php foreach($articles as $i => $article): ?>
                        <?= html::option($article['articles_id'], $article['articles_title'], $article_comment['join_articles_id']) ?>
                    <?php endforeach ?>
                </select>
            </td> 
        </tr>

        <tr>
            <td>
                <label class="description" for="articles_comments_author">Author</label>
            </td>
            <td>
                <?= html::input($article_comment['articles_comments_author'], 'articles_comments_author', array('class' => 'moreinfo', 'title' => 'Author of this comment', 'size' => '80')) ?>
            </td> 
        </tr>

        <tr>
            <td>
                <label class="description" for="articles_comments_email">Email</label>
            </td>
            <td>
                <?= html::input($article_comment['articles_comments_email'], 'articles_comments_email', array('class' => 'moreinfo', 'title' => 'Email of this author', 'size' => '80')) ?>
            </td> 
        </tr>

        <tr>
            <td>
                <label class="description" for="articles_comments_title">Title</label>
            </td>
            <td>
                <?= html::input($article_comment['articles_comments_title'], 'articles_comments_title', array('class' => 'moreinfo', 'title' => 'Title of the comment', 'size' => '80')) ?>
            </td> 
        </tr>

        <tr>
            <td>
                <label class="description" for="articles_comments_body">Body</label>
            </td>
            <td>
                <textarea name="articles_comments_body" id="articles_comments_body" cols="60" rows="10"><?= utf8_decode($article_comment['articles_comments_body']) ?></textarea>
            </td> 
        </tr>  
        
        <tr>
            <td></td>
            <td><?= html::submit('Save Comment', array('class' => 'submit center')) ?></td>
        </tr>
      
        </table>
        </fieldset>
    </form> 
</div>

<script language="javascript" type="text/javascript">
window.addEvent('domready', function () {
    var myTips = new Tips('.moreinfo');
    
    
    
    var cal = new MooCal('publish_date', {
        width: 200,
        leaveempty: false
    });
    
    var time = new MooTime('publish_time', {
        multiplier: 5,
        format: 'h:m a'
    });
});
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
