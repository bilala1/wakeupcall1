<?php
$page_title = 'Blog Comment Listing | Admin';
$page_name = 'blog';
$sub_name = 'comments';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php') ?>
    </div>
    <?php if($notice): ?>
        <br /><p class="notice">
            <?= $notice ?>
        </p>
    <?php endif ?>
    
    <form action="/admin/articles/comments/list.php" method="get">
        <p><fieldset class="alt1">
        <legend>Search</legend>
        <table width="100%">
        <tr>
            <td width="400">
                Title:<br />
                <?= html::input($_REQUEST['articles_comments_title'], 'articles_comments_title', array('class' => 'moreinfo', 'size' => '60')) ?>
            </td>
            <td>
                Body:<br />
                <?= html::input($_REQUEST['articles_comments_body'], 'articles_comments_body', array('class' => 'moreinfo', 'style' => 'width: 98%')) ?>
            </td>
        </tr>
        </table>
        <div style="text-align: right">
            <?= html::submit('Search', array('class' => 'submit')) ?>
            <a href="/admin/articles/comments/list.php" class="reset gray-btn">Clear Search</a>
        </div>
        </fieldset></p>
    </form>
	<a href="/admin/articles/comments/edit.php" class="add">Add Blog Comment</a><br clear="right"/>

    
    <?php if(!empty($_REQUEST['articles_id'])): ?>
    <h2>Blog: <?= articles::get_article_title($_REQUEST['articles_id']) ?></h2>
    <?php endif ?>

    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('articles_comments_post_date', 'Posted On') ?></td>
          <td class="thead"><?= strings::sort('articles_comments_author', 'Author') ?></td>
          <td class="thead"><?= strings::sort('articles_comments_email', 'Email') ?></td>
          <td class="thead"><?= strings::sort('articles_comments_title', 'Title') ?></td>
          <td class="thead"><?= strings::sort('articles_comments_body', 'Comment') ?></td> 
          <td class="thead"></td> 
        </tr> 
    </thead> 
    <tbody>
        <?php if(empty($article_comments)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no blog comments found.
            </td></tr>
        <?php endif ?>
	    <?php foreach($article_comments as $article_comment): ?> 
        <tr> 
          <td class="tbody"><?= date('M. j, Y \a\t g:ia', strtotime($article_comment['articles_comments_post_date'])) ?></td>
          <td class="tbody"><?= $article_comment['articles_comments_author'] ?></td>
          <td class="tbody"><?= $article_comment['articles_comments_email'] ?></td>
          <td class="tbody"><?= $article_comment['articles_comments_title'] ?></td>
          <td class="tbody"><?= $article_comment['articles_comments_body'] ?></td> 
          <td class="tbody" width="80">
            <a href="/admin/articles/comments/edit.php?articles_comments_id=<?= $article_comment['articles_comments_id'] ?>" title="Edit">Edit</a> |
            <a onclick="if(!confirm('Are you sure you want to remove this blog\'s comment')) { return false; }" href="/admin/articles/comments/delete.php?articles_comments_id=<?= $article_comment['articles_comments_id'] ?>" title="Delete">Delete</a>
          </td>
        </tr> 
        <?php endforeach ?>
    </tbody>
    	<tr>
        	<td colspan="6">
            	<img src="/images/admin/table-bottom.jpg" width="960"/>
            </td>
        </tr>
    </table>
        <span style="float: right; padding-top: 20px;">
        <?php if($pages): ?>Pages: <?= $pages; ?><?php endif; ?>
    </span><br clear="right"/><br />
</div>
<?php include(VIEWS . '/admin/footer.php') ?>
