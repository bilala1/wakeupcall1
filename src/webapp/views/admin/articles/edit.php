<?php
$page_title = 'Edit Blogs | Admin';
$page_name = 'blog';
$sub_name = 'posts';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>

<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/MooTime.js"></script>

<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php'); ?>        
    </div>
    <h2><?= $_REQUEST['articles_id'] ? 'Edit' : 'Add' ?> Post</h2>
    <form id="form" method="post" action="/admin/articles/edit.php">
    <input type="hidden" name="articles_id" value="<?= $article['articles_id'] ?>" />
    <?php if($notice): ?>  
        <p class="notice"><?= $notice ?></p>            
    <?php endif ?>
<fieldset class="alt1"> 
	<legend>Post</legend>     
    <table>
    <tr>
        <td>
            <label for="date_mont">Publish Date</label>
        </td>
        <td>
        <span>
            <input type="text" id="publish_date" name="publish_date" value="<?= times::tz_convert($article['articles_newsdate'], 'm/d/Y',false) ?>" />
        </span>
        </td>
    </tr> 
          
    <tr>
        <td>
            <label class="description" for="hour">Publish Time:&nbsp;</label>
        </td>
        <td>
            <input type="text" name="publish_time" value="<?= times::tz_convert($article['articles_newsdate'], 'h:i a', false) ?>" id="timepick" />
        </td>
    </tr>
    
    <tr>
        <td>
            <label class="description" for="articles_active">Published:&nbsp;</label>
        </td>
        <td>
            <select class="moreinfo" title="If the blog is viewable to the public" id="articles_active" name="articles_active" style="margin-bottom:10px"> 
                <?= html::option(1, 'Published', $article['articles_active']) ?>
                <?= html::option(0, 'Unpublished', $article['articles_active']) ?>
            </select>
        </td>
    </tr>

    <tr>
        <td>
	        <label class="description" for="join_articles_categories_id">Categories:&nbsp;</label>
	    </td>
	    <td>
            <select name="join_articles_categories_id" id="join_articles_categories_id" style="margin-bottom:10px">
                <option value="0">-- No Category --</option>
                <?php foreach($article_categories as $article_category): ?>
                    <?= html::option($article_category['articles_categories_id'], $article_category['articles_categories_name'], $article['join_articles_categories_id']) ?>
                <?php endforeach ?>
            </select>
	    </td>
    </tr>
    
    <tr>
        <td>
	        <label class="description" for="join_articles_tags">Tags:&nbsp;</label>
	    </td>
	    <td>
	        Type your keyword, and press "enter" to add it.<br />
            <?= html::input(tags::parse_tags_str($article['join_articles_tags']), 'join_articles_tags', array('class' => 'moreinfo', 'size' => '60', 'title' => 'Tags for this blog', 'id' => 'join_articles_tags')) ?>
            <br />
	    </td> 
    </tr>

    <tr>
        <td>
            <label class="description" for="articles_byline">Author:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_byline'], 'articles_byline', array('class' => 'moreinfo', 'size' => '60', 'title' => 'Who wrote the blog')) ?>
        </td> 
    </tr>	

    <?php if($article_groups): ?>
    <tr>
        <td>
	    <label class="description" for="join_articles_category_id">Categories:&nbsp;</label>
	    </td>
	    <td>
	    <?php foreach(array_chunk($article_groups, 2, true) as
    $group_set):?>
        <?php foreach($group_set as $i => $article_group): ?>
            <div style="float: left; width: 265px">
                <input class="checkbox" type="checkbox" name="<?= 'article_groups[' . $article_group['article_group_id'] . ']' ?>" value="<?= $article_group['article_group_id'] ?> <?=  (in_array($article_group['article_group_id'], explode(',', $article['article_groups'])) ? html::checked : null) ?>" />
            <?= $article_group['name'] ?>
            </div>
        <?php endforeach ?>
        <?php endforeach ?>
	    </td> 
    </tr>       
    <?php endif ?>       

    <tr>
        <td>
            <label class="description" for="articles_title">Title:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_title'], 'articles_title', array('class' => 'moreinfo', 
                'title' => 'Title of blog to display on top of the web page',
                'size' => '60')) ?>
        </td> 
    </tr>   

    <tr>
        <td>
            <label class="description" for="articles_subtitle">Sub-Title:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_subtitle'], 'articles_subtitle', array('class' => 'title', 'size' => '60')) ?>
        </td> 
    </tr>   

    <tr>
        <td>
            <label class="description" for="articles_body">Body:&nbsp;</label>
        </td>
        <td>
            <textarea name="articles_body" id="articles_body"><?= utf8_decode($article['articles_body']) ?></textarea>
        </td> 
    </tr>
    <tr>
        <td>
            <label class="description" for="articles_teaser">Teaser:&nbsp;</label>
        </td>
        <td>
            <textarea id="teaser" name="articles_teaser" class="moreinfo" cols="80" rows="4" title="In 2-4 sentences provide a snippet or summary of this blog."><?= html::filter($article['articles_teaser']) ?></textarea> 
        </td>
    </tr>      
    </table>
    </fieldset>
    
    <fieldset class="alt1">
    <legend>Search Engine Optimization Attribute</legend>
    <table>
    <tr>
        <td>
            <label class="description" for="articles_page_url">Page URL:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_page_url'], 'articles_page_url', array('class' => 'moreinfo', 
                'title' => 'Page Url is for urls so instead of:<br /> 
            &raquo; <strong>' . FULLURL . '/articles-detail.php?article_id=nn</strong><br />
            it would be:<br /> 
            &raquo; <strong>' . FULLURL . '/my-custom-you</strong><br />
            This is great for SEO and useful for hard to remember
            urls.', 'size' => 80)) ?>
        </td> 
    </tr>
     
     
    <tr>
        <td>
            <label class="description" for="page_title">Page Title:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_page_title'], 'articles_page_title', array('class' => 'moreinfo', 'title' => 'Page Title is what shows no the top bar of you web browser, and the tab its opened in. Make sure to use keywords in your titles and do note any title longer then 66 characters long (includes spaces).', 'size' => 80)) ?>
        </td> 
    </tr>
    
    <tr>
        <td>
            <label class="description" for="articles_page_keywords">Meta Keywords:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_page_keywords'], 'articles_page_keywords', array('class' => 'moreinfo', 'title' => 'Not used for SEO but interal search engine', 'size' => '80')) ?>
        </td>
    </tr> 
          
    <tr>
        <td>
            <label class="description" for="articles_page_description">Meta Description:&nbsp;</label>
        </td>
        <td>
            <textarea id="articles_page_description" name="articles_page_description" class="moreinfo" cols="80" rows="3" title="For SEO/Search Purposes. Try to make this no longer then 160 characters as search engines will ignore the rest of the description."><?= $article['articles_page_description'] ?></textarea> 
        </td>
    </tr>
    </table>
    </fieldset>
    
    <fieldset class="alt1">
    <legend>Extra Metadata</legend>
    <table>
    <tr>
        <td>
            <label class="description" for="articles_footnotes">Footnotes:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_footnotes'], 'articles_footnotes', array('class' => 'moreinfo', 'title' => 'Notes to go at the bottom of the body of blog', 'size' => '80')) ?>
        </td> 
    </tr>  
         
    <tr>
        <td>
            <label class="description" for="articles_copyright">Copyright:&nbsp;</label>
        </td>
        <td>
            <?= html::input($article['articles_copyright'], 'articles_copyright', array('class' => 'moreinfo')) ?>
        </td> 
    </tr>
         
    <tr>
        <td>
            <label class="description" for="articles_order">Blog Position:&nbsp;</label>
        </td>
        <td>
            <input id="articles_order" name="articles_order" class="moreinfo" title="Used to order a category to your liking. The numbers are ordered ascending by the lowest number, i.e 1,2,3,4,5,6,7" type="text" size="2" maxlength="9" value="<?= $article['articles_order'] ?>"/> 
        </td>
    </tr>
    </table>
    </fieldset><br />
    <div align="center">
		 <?= html::submit('Preview Blog', array('id' => 'preview', 'class' => 'gray-btn')) ?>
        <?= html::submit('Save Blog', array('class' => 'submit')) ?>
    </div>
   
    </form> 
</div>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
window.addEvent('load', function () {
    tinyMCE.init({
        menubar: "",
        toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect | bullist numlist | outdent indent | undo redo | link unlink image | table | html | removeformat | subscript superscript  | code",
        toolbar_items_size: 'small',
        selector: "#articles_body",
        plugins: "table link image textcolor code",
        target_list: false,
        default_link_target: "_blank",
        link_title: false
    });
    var myTips = new Tips('.moreinfo');
    
    $('preview').addEvent('click', function(){
        var old_action = $('form').get('action');
        var old_target = $('form').get('target');
        
        $('form')
            .set('action','<?= FULLURL ?>/members/news/detail.php?previewing_article=true&pwd=q34t90g5gmn5y')
            .set('target','_blank');
        $('form').submit();
        $('form')
            .set('action',old_action)
            .set('target',old_target);
        
        return false;
    });
    
    var taglist = new TextboxList('join_articles_tags', {
        unique: true,
        plugins: {
            autocomplete: {
                minLength: 3,
                queryRemote: true,
                remote: {
                    url: BASEURL + '/ajax-tags.php'
                }
            }
        }
    });
    
    var cal = new MooCal('publish_date', {
        width: 200,
        leaveempty: false
    });
    
    var time = new MooTime('timepick', {
        multiplier: 5,
        format: 'h:m a'
    });
});
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
