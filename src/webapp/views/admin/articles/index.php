<?php
$page_title = 'Blog | Admin';
$page_name = 'blog';
$sub_name = 'dashboard';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php') ?>
    </div>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
