<?php
$page_title = 'Blog | Admin';
$page_name = 'blog';
$sub_name = 'posts';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/articles/header.php') ?>
    </div>
    <?php include(VIEWS . '/admin/notice.php') ?>

    <form action="/admin/articles/list.php" method="get">
        <p><fieldset class="alt1">
        <legend>Search</legend>
        Term or Keyword:
        <?= html::input($_REQUEST['term'], 'term', array('class' => 'element text small')) ?>
        <?= html::submit('Search', array('class' => 'submit')) ?>
        <a href="/admin/articles/list.php" class="reset gray-btn">Clear Search</a>
        </fieldset></p>
    </form>

    <a href="/admin/articles/edit.php" class="add">Add Blog</a><br class="clear"/>

    <?php if(!empty($_REQUEST['join_articles_categories_id'])): ?>
    <h2>Category: <?= articles::get_article_category_name($_REQUEST['join_articles_categories_id']) ?></h2>
    <?php endif ?>

    <table class="tborder" cellpadding="0" cellspacing="0" border="0" align="center">
    <thead>
        <tr>
          <td class="thead" width="100"><?= strings::sort('articles_newsdate', 'Publish Date', null, null, null, 'DESC') ?></td>
          <td class="thead"><?= strings::sort('articles_title', 'Title') ?></td>
          <td class="thead"><?= strings::sort('articles_teaser', 'Teaser') ?></td>
          <td class="thead"><?= strings::sort('articles_views', 'Views', null, null, null, 'DESC') ?></td>
          <!--<td class="thead"><?= strings::sort('total_comments', 'Comments', null, null, null, 'DESC') ?></td>-->
          <td class="thead" width="100">Actions</td>
        </tr>
    </thead>
    <tbody>
        <?php if(empty($articles)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no blogs found.
            </td></tr>
        <?php endif ?>
        <?php foreach($articles as $article): ?>
        <tr>
          <td class="tbody"><?= date('M. j, Y \a\t g:ia', strtotime($article['articles_newsdate'])) ?></td>
          <td class="tbody"><?= $article['articles_title'] ?></td>
          <td class="tbody"><?= $article['articles_teaser'] ?></td>
          <td class="tbody"><?= $article['articles_views'] ?></td>
          <!--<td class="tbody"><a href="/admin/articles/comments/list.php?articles_id=<?= $article['articles_id'] ?>" style="display: block; height: 100%" title="See this post's comments">View Comments&nbsp;(<?= $article['total_comments'] ?>)</a></td>-->
          <td class="tbody">
            <a href="/admin/articles/edit.php?articles_id=<?= $article['articles_id'] ?>" title="Edit">Edit</a>

            | <a onclick="if(!confirm('Are you sure you want to remove this blog')) { return false; }" href="/admin/articles/delete.php?articles_id=<?= $article['articles_id'] ?>" title="Delete">Delete</a>
          </td>
        </tr>
        <?php endforeach ?>
    </tbody>
    <tr>
        	<td colspan="5">
            	<img src="/images/admin/table-bottom.jpg" width="960"/>
            </td>
        </tr>
    </table>

	<?php if($pages): ?>
		<span class="pages">
			Pages: <?= $pages; ?>
		</span><br clear="right"/>
	<?php endif ?>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
