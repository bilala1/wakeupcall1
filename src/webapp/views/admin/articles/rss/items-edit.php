<?php
$page_title = 'Admin Dashboard';
$page_name = 'rss-items';
?>
<?php include(VIEWS . '/admin/header.php'); ?> 
<div class="page">
    <div class="navbar">
        <div class="navtrail">
            <a href="/admin/index.php">Admin</a> &raquo; 
            <a href="/admin/rss/items.php">RSS Items</a> &raquo; Edit
        </div>
        <strong>Items</strong>
    </div><br />
    <?php if($notice): ?>
        <p class="notice"><?= $notice ?></p>
    <?php endif ?>
    <div class="left">
        <form id="addon_form" method="post" action="/admin/rss/items-edit.php">
        <fieldset class="alt1">
        <legend>Item Editor</legend>
        <input type="hidden" name="rss_items_id" value="<?= $item['rss_items_id'] ?>" />
        <table>
        <tr>
            <td>
                <label for="rss_items_link">Item Link:&nbsp;</label><br />
                <span class="error"><?= $errors['rss_items_link'] ?></span>
            </td> 
            <td><input type="text" id="rss_items_link" name="rss_items_link" value="<?= html::filter($item['rss_items_link']) ?>" maxlength="255" /></td>
        </tr>
        <tr>
            <td>
                <label for="rss_items_title">Item Title:&nbsp;</label><br />
                <span class="error"><?= $errors['rss_items_title'] ?></span>
            </td> 
            <td><input type="text" name="rss_items_title" value="<?= html::filter($item['rss_items_title']) ?>" maxlength="255" /></td>
        </tr>
        <tr>
            <td>
                <label for="rss_items_description">Item Description:&nbsp;</label><br />
                <span class="error"><?= $errors['rss_items_description'] ?></span>
            </td> 
            <td><input type="text" name="rss_items_description" value="<?= html::filter($item['rss_items_description']) ?>" /></td>
        </tr>
        <tr>
            <td>Item Publish Date:&nbsp;</td> 
            <td><input type="text" name="rss_items_publish_date" value="<?= html::filter($item['rss_items_publish_date']) ?>" /></td>
        </tr>
        <tr>
            <td>Item Active:&nbsp;</td> 
            <td>
                <?= 
                html::select(array(
                    '1' => 'Yes',
                    '0' => 'No',
                ), 'rss_items_active', $item['rss_items_active']) 
                ?>
            </td>
        </tr>
        <tr>
            <td></td> 
            <td><input class="submit" type="submit" value="Save Item" /></td>
        </tr>
        </table>
        </fieldset>
        </form>
    </div>
    <div class="clearing"></div>
    <div class="clear"></div>
</div><br />
<?php include(VIEWS . '/admin/footer.php'); ?>
