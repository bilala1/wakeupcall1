<?php
$page_title = 'RSS Items | Admin';
$page_name = 'rss-items';
?>
<?php include(VIEWS . '/admin/header.php'); ?> 
<div class="page">
    <div class="navbar">
        <div class="navtrail"><a accesskey="1" href="/admin/index.php">Admin</a> &raquo; Items</div>
        <strong>Items</strong>
    </div>
    <?php if($notice): ?>
        <br /><p class="notice">
            <?= $notice ?>
        </p>
    <?php endif ?>
    <a href="/admin/rss/items-edit.php" class="submit">Add Rss Item</a>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
	    <tr> 
	      <td class="thead">Publish Date</td>
	      <td class="thead">Title</td>
	      <td class="thead">Description</td> 
	      <td class="thead">Link</td> 
	      <td class="thead"></td> 
	    </tr> 
    </thead> 
    <tbody>
        <?php foreach($items as $item): ?>
	    <tr> 
	      <td class="tbody"><?= date('M. j, \a\t Y g:ia', strtotime($item['rss_items_publish_date'])) ?></td>
	      <td class="tbody"><?= $item['rss_items_title'] ?></td>
	      <td class="tbody"><?= $item['rss_items_description'] ?></td> 
	      <td class="tbody"><?= $item['rss_items_link'] ?></td> 
	      <td class="tbody">
	        <a href="/admin/rss/items-edit.php?rss_items_id=<?= $item['rss_items_id'] ?>" title="Edit">Edit</a> |
	        <a href="/admin/rss/items-delete.php?rss_items_id=<?= $item['rss_items_id'] ?>" title="Delete">Delete</a>
	      </td>
	    </tr> 
	    <?php endforeach ?>
    </tbody>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
