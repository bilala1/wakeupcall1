<?php
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'documents';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
	</fieldset>
    
    <a href="/admin/library/documents/edit.php" class="add">Add Document</a><br clear="right"/>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr> 
              <td class="thead" width="400"><?= strings::sort('title', 'Title') ?></td> 
            </tr> 
        </thead>
        <tbody>
        <?php foreach($documents as $document): ?>
        <tr>
            
        </tr>
        <?php endforeach; ?>
            <tr>
            	<td colspan="4"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>     
        </tbody>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
