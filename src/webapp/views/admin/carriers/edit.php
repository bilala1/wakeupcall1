<?
$page_title = 'Edit Carrier | Admin';
$page_name = 'carriers';
$sub_name = 'edit';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<style type="text/css">
p.form-error{
    margin-left:150px;
}
</style>
<div class="page">
    <? include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/carriers/edit.php" method="post">
    <input type="hidden" name="carriers_id" value="<?= $_REQUEST['carriers_id'] ?>" />
    
    <fieldset class="alt1">
    	<legend>Carrier Details</legend>
    	
        <?= html::textfield('Name', $carrier, 'carriers_name', array(), $errors) ?>
        <?= html::textfield('Email', $carrier, 'carriers_email', array(), $errors) ?>
        <?= html::textfield('Phone', $carrier, 'carriers_phone', array(), $errors) ?>
        <?= html::textfield('Fax #', $carrier, 'carriers_fax', array(), $errors) ?>
        <?= html::textfield('Policy #', $carrier, 'carriers_policy_number', array(), $errors) ?>
        <?= html::textfield('Address', $carrier, 'carriers_address', array(), $errors) ?>
        
        <input value="Save" class="submit center" type="submit">
    </fieldset>
    
    </form>
</div>

<? include(VIEWS . '/admin/footer.php'); ?>
