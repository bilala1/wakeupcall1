<?
$page_title = 'Carriers List | Admin';
$page_name = 'carriers';
$sub_name = 'carriers';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <? if($notice): ?>
        <br /><p class="notice">
            <?= $notice ?>
        </p>
    <? endif ?>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <a href="/admin/carriers/edit.php" class="add">Add Carrier</a><br clear="right"/>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('carriers_name', 'Name') ?></td> 
          <td class="thead"><?= strings::sort('carriers_email ', 'Email') ?></td> 
          <td class="thead"><?= strings::sort('carriers_phone', 'Phone') ?></td> 
          <td class="thead"><?= strings::sort('carriers_fax', 'Fax') ?></td> 
          <td class="thead"><?= strings::sort('carriers_policy_number', 'Policy #') ?></td> 
          <td class="thead"><?= strings::sort('carriers_address ', 'Address') ?></td> 
          <td class="thead" width="100">Actions</td>
        </tr> 
    </thead> 
    <tbody>
        <? if(empty($carriers)): ?>
            <tr><td colspan="7" align="center"><br/>Sorry, there are no carriers found.</td></tr>
        <? endif ?>
        <? foreach($carriers as $carrier): ?>
            <tr> 
                <td class="tbody"><?= $carrier['carriers_name'] ?></td>
                <td class="tbody"><?= $carrier['carriers_email'] ?></td>
                <td class="tbody"><?= $carrier['carriers_phone'] ?></td>
                <td class="tbody"><?= $carrier['carriers_fax'] ?></td>
                <td class="tbody"><?= $carrier['carriers_policy_number'] ?></td>
                <td class="tbody"><?= $carrier['carriers_address'] ?></td>
                <td class="tbody">
                    <a href="/admin/carriers/edit.php?carriers_id=<?= $carrier['carriers_id'] ?>" title="Edit">Edit</a>
                    | <a onclick="if(!confirm('Are you sure you want to remove this carrier')) { return false; }" href="/admin/carriers/delete.php?carriers_id=<?= $carrier['carriers_id'] ?>" title="Delete">Delete</a>
                </td>
            </tr> 
        <? endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="7"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
