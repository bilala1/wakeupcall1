<? include VIEWS . '/admin/header.php' ?>

<div class="page">
    <div class="navbar">
        <? include 'header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>

	<h2><?= empty($_REQUEST['id']) ? 'Add' : 'Edit' ?> Discount Code</h2>
	<form action="edit.php" method="post">
		<? if (!empty($_REQUEST['id'])): ?>
			<input type="hidden" name="id" value="<?=$_REQUEST['id']?>" />
		<? endif; ?>
		<fieldset class="alt1">
			<legend>Code Details</legend>
			<table>
				<tr>
					<td>Discount Code:</td>
					<td><input type="text" name="discount_codes_code" value="<?=$code['discount_codes_code']?>" /></td>
					<td class="error"><?=$errors['discount_codes_code']?></td>
				</tr>
				<tr>
					<td>Start Date:</td>
					<td><input type="text" id="discount_codes_start" name="discount_codes_start" value="<?=$code['discount_codes_start']?>" /></td>
					<td class="error"><?=$errors['discount_codes_start']?></td>
				</tr>
				<tr>
					<td>End Date:</td>
					<td><input type="text" id="discount_codes_end" name="discount_codes_end" value="<?=$code['discount_codes_end']?>" /></td>
					<td class="error"><?=$errors['discount_codes_end']?></td>
				</tr>
				<tr>
					<td>Discount amount:<br />"$xx.xx" or "xx%"</td>
					<td><input type="text" name="value" value="<?=$code['value']?>" /></td>
					<td class="error"><?=$errors['value']?></td>
				</tr>
                <tr>
                    <td>Max Uses:</td>
                    <td><input type="text" name="discount_codes_max_uses" value="<?=$code['discount_codes_max_uses']?>" /></td>
                    <td class="error"><?=$errors['discount_codes_max_uses']?></td>
                </tr>
                <tr>
                    <td>Uses:</td>
                    <td><input type="text" name="discount_codes_uses" value="<?=$code['discount_codes_uses']?>" /></td>
                    <td class="error"><?=$errors['discount_codes_uses']?></td>
                </tr>
                <tr>
                    <td>Type:</td>
                    <td><?= html::select($code_types, "discount_codes_type", $code['discount_codes_type']) ?></td>
                    <td class="error"><?=$errors['discount_codes_type']?></td>
                </tr>
				<tr>
					<td colspan="3">
						<input class="submit input-submit" type="submit" value="Save" />
						<a class="gray-btn" href="list.php">Cancel</a>
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
</div>
<script type="text/javascript">
window.addEvent('load', function () {
    var start_cal = new MooCal('discount_codes_start', {
        width: 200,
        leaveempty: false
    });
    var end_cal = new MooCal('discount_codes_end', {
        width: 200,
        leaveempty: false
    });	
});
</script>
<? include VIEWS . '/admin/footer.php' ?>

