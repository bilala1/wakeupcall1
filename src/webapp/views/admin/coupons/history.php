<? include VIEWS . '/admin/header.php' ?>
<div class="page">
    <div class="navbar">
        <? include 'header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>


	<h2>Discount Code History</h2>

	<table class="tborder clear" cellspacing="1" cellpadding="6" width="100%">
		<thead>
			<tr>
				<td class="thead"><?= strings::sort('discount_code_logs_code', 'Code') ?></td>
				<td class="thead"><?= strings::sort('discount_code_logs_id', 'Discount') ?></td>
				<td class="thead"><?= strings::sort('discount_code_logs_dateused', 'Date Used') ?></td>
                <td class="thead"><?= strings::sort('discount_code_logs_final_balance', 'Final Balance') ?></td>
                <td class="thead"><?= strings::sort('discount_code_logs_members_lastname', 'Member Name') ?></td>
			</tr>
		</thead>
		<tbody>
			<? foreach($discount_code_logs as $log): ?>
			<tr>
				<td class="tbody"><?=$log['discount_codes_logs_code']?></td>
                <td class="tbody"><?=$log['discount_code_logs_amount'] > 0 ? money_format('%n', $log['discount_code_logs_amount']) : $log['discount_code_logs_percent'] . '%'?></td>
                <td class="tbody"><?=date('m/d/Y', strtotime($log['discount_code_logs_dateused']))?></td>
                <td class="tbody"><?=$log['discount_code_logs_final_balance']?></td>
                <td class="tbody"><?=$log['discount_code_logs_members_lastname']?>,<?=$log['discount_code_logs_members_firstname']?></td>
			</tr>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6"><img src="/images/admin/table-bottom.jpg"/></td>
			</tr>
		</tfoot>	
	</table>
</div>
<? include VIEWS . '/admin/footer.php' ?>