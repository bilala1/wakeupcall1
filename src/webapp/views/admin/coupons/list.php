<? include VIEWS . '/admin/header.php' ?>
<div class="page">
    <div class="navbar">
        <? include 'header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>


	<h2>Discount Codes</h2>
	<a class="add" href="edit.php">Add Discount Code</a>

	<table class="tborder clear" cellspacing="1" cellpadding="6" width="100%">
		<thead>
			<tr>
				<td class="thead"><?= strings::sort('discount_codes_code', 'Code') ?></td>
				<td class="thead"><?= strings::sort('discount_codes_created', 'Created on') ?></td>
				<td class="thead"><?= strings::sort('discount_codes_start', 'Start Date') ?></td>
				<td class="thead"><?= strings::sort('discount_codes_end', 'End Date') ?></td>
                <td class="thead"><?= strings::sort('discount_codes_max_uses', 'Max Uses') ?></td>
                <td class="thead"><?= strings::sort('discount_codes_uses', 'Uses') ?></td>
                <td class="thead">Discount</td>
                <td class="thead">Type</td>
				<td class="thead">Actions</td>
			</tr>
		</thead>
		<tbody>
			<? foreach($discount_codes as $code): ?>
			<tr>
				<td class="tbody"><?=$code['discount_codes_code']?></td>
				<td class="tbody"><?=date('m/d/Y', strtotime($code['discount_codes_created']))?></td>
				<td class="tbody"><?=date('m/d/Y', strtotime($code['discount_codes_start']))?></td>
				<td class="tbody"><?=date('m/d/Y', strtotime($code['discount_codes_end']))?></td>
                <td class="tbody"><?=$code['discount_codes_max_uses']?></td>
                <td class="tbody"><?=$code['discount_codes_uses']?></td>
                <td class="tbody"><?=$code['discount_codes_amount'] > 0 ? money_format('%n', $code['discount_codes_amount']) : $code['discount_codes_percent'] . '%'?></td>
                <td class="tbody"><?=$code['discount_codes_type']?></td>
				<td class="tbody">
					<a href="edit.php?id=<?=$code['discount_codes_id']?>">Edit</a> | 
					<a href="delete.php?id=<?=$code['discount_codes_id']?>" class="delete-link" onclick="confirm('Delete this discount code?')">Delete</a>
				</td>
			</tr>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6"><img src="/images/admin/table-bottom.jpg"/></td>
			</tr>
		</tfoot>	
	</table>
</div>
<? include VIEWS . '/admin/footer.php' ?>