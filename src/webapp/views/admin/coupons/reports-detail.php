<? include VIEWS . '/admin/header.php' ?>

<div class="page">
    <div class="navbar">
        <? include 'header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>
	
	<h2>Discount Code Usage Details: <?= $codes[0]['discount_codes_code'] ?></h2>

	<table class="tborder" cellspacing="1" cellpadding="6" width="100%">
		<thead>
			<tr>
				<td class="thead"><?= strings::sort('bills_datetime', 'Date Used') ?></td>
				<td class="thead"><?= strings::sort('members_lastname', 'Member Name') ?></td>
				<td class="thead"><?= strings::sort('corporations_name', 'Account') ?></td>
			</tr>
		</thead>
		<tbody>
			<? foreach($codes as $code): ?>
				<tr>
					<td class="tbody"><?= $code['bills_datetime_formatted'] ?></td>
					<td class="tbody">
						<a href="/admin/members/edit.php?members_id=<?=$code['members_id']?>">
							<?=$code['members_lastname'] . ', ' . $code['members_firstname'] ?>
						</a>
					</td>
					<td class="tbody">
						<a href="/admin/members/accounts/edit.php?accounts_is=<?=$code['accounts_id']?>">
							<?=$code['accounts_name']?>
						</a>
					</td>
				</tr>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3"><img src="/images/admin/table-bottom.jpg"/></td>
			</tr>
		</tfoot>
	</table>
</div>

<? include VIEWS . '/admin/footer.php' ?>