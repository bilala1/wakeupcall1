<? include VIEWS . '/admin/header.php' ?>

<div class="page">
    <div class="navbar">
        <? include 'header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>
	
	<h2>Discount Code Usage</h2>

	<table class="tborder" cellspacing="1" cellpadding="6" width="100%">
		<thead>
			<tr>
				<td class="thead"><?= strings::sort('discount_codes_code', 'Discount Code') ?></td>
				<td class="thead"><?= strings::sort('discount_codes_count', 'Times Used') ?></td>
			</tr>
		</thead>
		<tbody>
			<? foreach($codes as $code): ?>
				<tr>
					<td class="tbody"><a href="reports-detail.php?id=<?=$code['discount_codes_id']?>"><?=$code['discount_codes_code'] ?></a></td>
					<td class="tbody"><?=$code['discount_codes_count'] ?></td>
				</tr>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2"><img src="/images/admin/table-bottom.jpg"/></td>
			</tr>
		</tfoot>
	</table>
</div>

<? include VIEWS . '/admin/footer.php' ?>