<?php
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'categories';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    
    <?php include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/faqs/categories/edit.php" method="post">
    
    <fieldset class="alt1">
    	<legend>Category Information</legend>
    	
        <label for="faqs_categories_name">Category Name:</label>
        <?= html::input($category['faqs_categories_name'], 'faqs_categories_name') ?><br />
        
        <label for="faqs_categories_status">Category Status:</label>
        <?= html::select(array('active' => 'Active', 'inactive' => 'Inactive'), 'faqs_categories_status', $category['faqs_categories_status']) ?><br />
        
        <label for="join_faqs_categories_id">Category Parent:</label>
        <select name="join_faqs_categories_id">
            <option value="0">No Parent (Top Level)</option>
            <?php foreach($categories as $lib_category): ?>
            <option value="<?= $lib_category['faqs_categories_id'] ?>" <?= ($category['join_faqs_categories_id'] == $lib_category['faqs_categories_id']) ? html::selected : null ?>><?= $lib_category['faqs_categories_name'] ?></option>
            <?php endforeach; ?>
        </select><br />
        <br />
        
        <input type="hidden" name="faqs_categories_id" value="<?= $_REQUEST['faqs_categories_id'] ?>" />
        <input value="Save Category" class="submit center" type="submit">
    </fieldset>
    
    </form>
    
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
