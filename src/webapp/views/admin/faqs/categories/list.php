<?php
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'categories';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    <? include VIEWS .'/admin/notice.php' ?>

    <a href="/admin/faqs/categories/edit.php?join_faqs_categories_id=<?= (int) $_REQUEST['faqs_categories_id'] ?>" class="add">Add Category</a><br clear="right"/>

    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>

    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
        <thead>
            <tr>
              <td class="thead">Category</td>
              <td class="thead">Sub-Categories</td>
              <td class="thead" width="100">Actions</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach($categories as $category): ?>
        <tr>
            <td class="tbody" style="padding-left:<?= 15+($category['sub_level']*15) ?>px"><img src="<?= HTTP_FULLURL ?>/images/arrow-right.png" /> <?= $category['faqs_categories_name'] ?></td>
            <td class="tbody"><?= $category['subcategories'] ?> <a href="/admin/faqs/list.php?category=<?= $category['faqs_categories_id'] ?>">(<?= $category['num_faqs'] ?>) FAQS</a></td>
            <td class="tbody">
                <a href="/admin/faqs/categories/edit.php?faqs_categories_id=<?= $category['faqs_categories_id'] ?>">Edit</a>
                <? if (!$category['cannot_delete']): ?>
                | <a onClick="return confirm('Are you sure you want to delete this category?');" href="/admin/faqs/categories/delete.php?faqs_categories_id=<?= $category['faqs_categories_id'] ?>">Delete</a>
            	<? endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
            <tr>
            	<td colspan="3"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tbody>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
