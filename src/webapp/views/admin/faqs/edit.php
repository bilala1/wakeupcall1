<?
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'faqs';
?>
<? include(VIEWS . '/admin/header.php'); ?>

<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>

<div class="page">
    <div class="navbar">
        <? include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    
    <? include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/faqs/edit.php<?//= $_REQUEST['faqs_id'] ? '?faqs_id='.$_REQUEST['faqs_id'] : ''; ?>" method="post">
    
    <fieldset class="alt1">
    	<legend>FAQ Details</legend>
    	
        Question:<br />
		<textarea name="faqs_question" id="faqs_question" style="width:480px"><?= utf8_decode($faq['faqs_question']) ?></textarea>        	
        <br />
        Answer:<br />
        <textarea name="faqs_answer"id="faqs_answer" style="height: 300px;width:480px"><?= utf8_decode($faq['faqs_answer']) ?></textarea>
        <? if($errors['faqs_answer']): ?> <br /><span class="error">Please enter answer</span><br /><? endif; ?>
        <br /> 
        <?= html::textfield('Keywords: <span style="font-size: 65%">Separate multiple key words with commas</span>', $faq['faqs_keywords'], 'faqs_keywords', array(), $errors) ?>
        <br />
        <label>Category</label>
        <select name="join_faqs_categories_id">
            <option value="">Please Select</option>
            <? foreach($categories as $category): ?>
            <option value="<?= $category['faqs_categories_id'] ?>" <?= $category['faqs_categories_id'] == $faq['join_faqs_categories_id'] ? html::selected : null ?>><?= str_repeat('-', $category['sub_level']) ?> <?= $category['faqs_categories_name'] ?></option>
            <? endforeach; ?>
        </select><br />         
        <? if($errors['join_faqs_categories_id']): ?>
        <p class="error"><?= $errors['join_faqs_categories_id'] ?></p>
        <? endif ?>
        <?= html::checkboxfield('Published:', 1, 'faqs_published', $faq['faqs_published'], array(), $errors) ; ?>    
        <br />
        <input type="hidden" name="faqs_id" value="<?= $_REQUEST['faqs_id'] ?>" />
        <input value="Save" class="submit center" type="submit">
    </fieldset>
    
    </form>
    
</div>
<br />
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
window.addEvent('load', function () {
    tinyMCE.init({
        menubar: "",
        toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect | bullist numlist | outdent indent | undo redo | link unlink image | removeformat | subscript superscript  | code",
        toolbar_items_size: 'small',
        selector: "#faqs_answer",
        plugins: "link image textcolor code",
        target_list: false,
        default_link_target: "_blank",
        link_title: false
    });
    
        tinyMCE.init({
        menubar: "",
        toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect | bullist numlist | outdent indent | undo redo | link unlink image | removeformat | subscript superscript | code",
        toolbar_items_size: 'small',
        selector: "#faqs_question",
        plugins: "link image textcolor code",
            target_list: false,
            default_link_target: "_blank",
            link_title: false
    });
});


</script>



</script>

<? include(VIEWS . '/admin/footer.php'); ?>
