<?
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'faqs';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    
    <? include VIEWS . '/admin/notice.php' ?>
    
    <fieldset class="alt1">
        <legend>Search / Filter</legend>
        
        <form action="/admin/faqs/list.php" method="get">
            <label>Search</label>
            <input type="text" name="q" value="<?= html::filter($_GET['q']) ?>" /><br />
            
            <label>Include</label>
            <input type="checkbox" name="submitted" value="1" id="filter_submitted" <?= $_GET['submitted'] ? html::checked : null ?> /> <label for="filter_submitted" style="width: auto;">Submitted</label> |
            <input type="checkbox" name="published" value="1" id="filter_added" <?= $_GET['published'] ? html::checked : null ?> /> <label for="filter_added" style="width: auto;">Published</label><br />
            <label for="category">Category:</label>
            <select name="category" id="category">
                <option value="">-all-</option>
                <? foreach(faqs_categories::get_categories_tree() as $category): ?>
                <option value="<?= $category['faqs_categories_id'] ?>" <?= $category['faqs_categories_id'] == $_REQUEST['category'] ? html::selected : null ?>><?= str_repeat('-', $category['sub_level']) ?> <?= $category['faqs_categories_name'] ?></option>
                <? endforeach; ?>
            </select><br />
            
            <input type="submit" class="submit" value="Filter" />
        </form>
    </fieldset>
    
    <a href="/admin/faqs/edit.php" class="add">Add FAQ</a><br clear="right"/>
    
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <br />
    <form action="list.php" method="post" id="set_orders_form">
    <?= html::hidden($_GET, 'q') ?>
    <?= html::hidden($_GET, 'category') ?>
    <?= html::hidden($_GET, 'submitted') ?>
    <?= html::hidden($_GET, 'published') ?>
    <?= html::submit('Set Orders', array('style' => 'float:right')) ?>
    </form>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr>
                <td class="thead">order</td>
                <td class="thead"><?= strings::sort('faqs_question_datetime', 'Date Added') ?></td>
                <td class="thead"><?= strings::sort('faqs_answer_datetime', 'Date Answered') ?></td>
                <td class="thead"><?= strings::sort('faqs_categories_name', 'Category') ?></td>
                <td class="thead"><?= strings::sort('faqs_question', 'Question') ?></td>
                <td class="thead"><?= strings::sort('member', 'Submitted By') ?></td>
                <td class="thead"><?= strings::sort('faqs_published', 'Published') ?></td>
                <td class="thead" width="100">Actions</td>
            </tr> 
        </thead>
        <tbody>
        <? foreach($faqs as $faq): ?>
        <tbody class="faq-row">
        <tr class="faq-row2" id="row-<?= $faq['faqs_id'] ?>">
            <td class="tbody handle"><?= $faq['faqs_order'] ?></td>
            <td class="tbody"><?= date('m/d/Y h:ia', strtotime($faq['faqs_question_datetime'])) ?></td>
            <td class="tbody"><?= strtotime($faq['faqs_answer_datetime']) > 1 ? date('m/d/Y h:ia', strtotime($faq['faqs_answer_datetime'])) : 'Not Answered' ?></td>
            <td class="tbody"><?= $faq['faqs_categories_name'] ?></td>
            <td class="tbody"><?= $faq['faqs_question'] ?></td>
            <td class="tbody"><?= $faq['members_firstname'] . ' ' . $faq['members_lastname'] ?></td>
            <td class="tbody"><?= $faq['faqs_published']? '<span style="color: green">Y</span>' : '<span style="color: red">N</span>' ?></td>
            <td class="tbody">
                <a href="/admin/faqs/edit.php?faqs_id=<?= $faq['faqs_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this?');" href="/admin/faqs/delete.php?faqs_id=<?= $faq['faqs_id'] ?>">Delete</a>
            </td>
        </tr>
        </tbody>
        <? endforeach; ?>
        <tfoot>
            <tr>
            	<td colspan="7"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
        </tbody>
    </table>
</div>
<br />
<script type="text/javascript">
new Sortables('.faq-row', {
    handle: '.handle'
});

$('set_orders_form').addEvent('submit', function(){
    var inputs_html = '';
    
    $$('.faq-row2').each(function(el, i){
        var id = el.get('id').split('-')[1];
        
        inputs_html += '<input name="faqs_orders['+id+']" value="'+i+'" type="hidden" />';
    });
    
    this.grab(new Element('div', {html: inputs_html}));
});
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
