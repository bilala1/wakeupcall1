<?php
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'terms';
?>
<?php include(VIEWS . '/admin/header.php'); ?>

<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    
    <?php include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/faqs/terms/edit.php" method="post">
    
    <fieldset class="alt1">
    	<legend>Term Details</legend>
        
        <?= html::textfield('Term:', $term['definitions_word'], 'definitions_word', array(), $errors) ?>
    	
        <?= html::textareafield('Description:', $term['definitions_description'], 'definitions_description', array('cols'=>'60','rows'=>'15'), $errors) ?>
        
        <?= html::checkboxfield('Pending?', 1,'definitions_pending',$term['definitions_pending'],array(),$errors); ?>
        
        <input type="hidden" name="definitions_id" value="<?= $_REQUEST['definitions_id'] ?>" />
        <input value="Save" class="submit center" type="submit">
    </fieldset>
    
    </form>
    
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
