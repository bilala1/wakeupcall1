<?php
$page_title = 'Admin';
$page_name = 'faqs';
$sub_name = 'terms';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/faqs/header.php' ?>
    </div>
    <? include VIEWS . '/admin/notice.php' ?>    
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	<form action="/admin/faqs/terms/list.php" method="get">
        	<label>Term or Description</label>
        	<input type="text" name="search_string" value="<?= html::filter($_REQUEST['search_string']) ?>" />
        	<br />
    	    <label>Pending?</label>
    	    <input type="checkbox" name="pending" value="1" <?= $_REQUEST['pending'] ? 'checked="checked"' : '' ?> />
    	    <br />
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>    
    
    <a href="/admin/faqs/terms/edit.php" class="add">Add Definition</a><br clear="right"/>
        
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr> 
                <td class="thead" ><?= strings::sort('definitions_description', 'Description') ?></td>
                <? if ($_REQUEST['pending'] == 1): /* if vieweing pending definitions */ ?>
                    <td class="thead" ><?= strings::sort('members_lastname', 'Submitter') ?></td>
                    <td class="thead" ><?= strings::sort('definitions_requested_datetime', 'Date of request') ?></td>
                <? endif; ?>
                <td class="thead" width="100"><?= strings::sort('definitions_word', 'Term') ?></td>
                <td class="thead" width="100">Actions</td>
            </tr> 
        </thead>
        <tbody>
        <?php foreach($definitions as $definition): ?>
        <tr>
            <td class="tbody"><?= $definition['definitions_description'] ?></td>
            <? if ($_REQUEST['pending'] == 1): /* if vieweing pending definitions */ ?>
            <td class="tbody"><?= $definition['members_firstname'].' '.$definition['members_lastname']; ?></td>
            <td class="tbody"><?= date('F d Y',strtotime($definition['definitions_requested_datetime'])); ?></td>
            <? endif; ?>
            <td class="tbody"><?= $definition['definitions_word'] ?></td>
            <td class="tbody">
                <a href="/admin/faqs/terms/edit.php?definitions_id=<?= $definition['definitions_id'] ?>">Edit</a>
                | <a href="/admin/faqs/terms/delete.php?definitions_id=<?= $definition['definitions_id'] ?>" onclick="if(!confirm('Are you sure you want to remove this definition?')){ return false; }" >Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
            <tr>
            	<td colspan="<?= ($_REQUEST['pending'] == 1) ? '5' : '3' ; ?>"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>     
        </tbody>
    </table>
    <?= $paging ?>
    <br /><br />
    Total: <?= $paging->total ?> Definitions
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
