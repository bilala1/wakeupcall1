<?php
$page_title = "Bad Word";
$page_name = 'forums';
$sub_name = 'badwords';
include VIEWS . '/admin/header.php';
?>

<div class="page">
	<div class="navbar">
		<? include VIEWS . '/admin/forums/header.php' ?>
	</div>

	<h2><?= isset($bad_word) ? 'Edit' : 'Add' ?> Bad Word</h2>

	<form action="" method="post">
		<fieldset class="alt1">

			<?= html::textfield('Bad Word', $bad_word, 'bad_words_word', array(), $errors); ?>

			<label></label>
			<?= html::submit('Save', array('class' => 'submit input-submit')); ?>

		</fieldset>
	</form>
</div>

<?php include VIEWS. '/admin/footer.php'; ?>