<?php $page_title = "Bad Words";
$page_name = 'forums';
$sub_name = 'badwords';
include(VIEWS . '/admin/header.php');
?>

<div class="page">
	<div class="navbar">
		<? include(VIEWS . '/admin/forums/header.php'); ?>
	</div>
	<h2>Bad Words List</h2>

	<?php include(VIEWS . '/admin/notice.php') ?>

	<form action="" method="get">
		<fieldset class="alt1">
			<legend>Search</legend>
			Term or Keyword:
			<?= html::input($_REQUEST['term'], 'term', array('class' => 'element text small')) ?>
			<?= html::submit('Search', array('class' => 'submit')) ?>
		</fieldset>
	</form>

	<a href="/admin/forums/badwords/edit.php" class="add">Add Bad Word</a><br class="clear"/>

	<?php if ($pages): ?>
	<span class="pages">
            Pages: <?= $pages; ?>
        </span>
	<?php endif ?>
	<table class="tborder" width="100%">
		<thead>
			<tr>
				<td class="thead">Word</td>
				<td class="thead">Actions</td>
			</tr>
		</thead>
		<tbody>
			<? foreach ($bad_words as $bad_word): ?>
			<tr>
				<td class="tbody"><?= $bad_word['bad_words_word'] ?></td>
				<td class="tbody">
					<a href="/admin/forums/badwords/edit.php?bad_words_id=<?= $bad_word['bad_words_id'] ?>">Edit</a> |
					<a href="/admin/forums/badwords/delete.php?bad_words_id=<?= $bad_word['bad_words_id'] ?>">Delete</a>
				</td>
			</tr>
			<? endforeach ?>
		</tbody>
	</table>

	<?php if ($pages): ?>
	<span class="pages">
			Pages: <?= $pages; ?>
		</span><br clear="right"/>
	<?php endif ?>
</div>

<?php include VIEWS . '/admin/footer.php'; ?>