<?
$page_title = 'Edit Forum | Admin';
$page_name = 'forums';
$sub_name = 'forums';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
    <h2><?= $_REQUEST['forums_id'] ? 'Edit' : 'Add' ?> Forum</h2>
    <form id="theform" method="post" action="/admin/forums/edit.php">
        <input type="hidden" name="forums_id" value="<?= $forum['forums_id'] ?>" />
        <? include(VIEWS.'/admin/notice.php') ?>
        <fieldset class="alt1">
            <legend>Forum Information</legend>
            <table class="form">
                <? /*
                <tr>
                    <td>
                        <label for="forums_name">Forum Parent:</label>
                    </td>
                    <td>
                        <?= html::select($forum_list, 'join_forums_id_parent', $_REQUEST['parent_id'] ? $_REQUEST['parent_id'] : $forum['join_forums_id_parent'], array('style' => 'margin-bottom:10px')) ?>
                    </td>
                </tr>
                */ ?>
                <tr>
                    <td>
                        <label for="forums_name">Forum Name:</label>
                    </td>
                    <td>
                        <input type="text" id="forums_name" name="forums_name" value="<?= html::filter($forum['forums_name']) ?>" size="30" maxlength="255" />
                    </td> 
                </tr>
                <tr>
                    <td>
                        <label for="forums_desc">Forum Description:</label>
                    </td>
                    <td>
                        <textarea name="forums_desc" rows="4" cols="40" style="margin-bottom:10px"><?= html::filter($forum['forums_desc']) ?></textarea>
                    </td> 
                </tr>
                <tr>
                    <td>
                        <label for="forums_status">Forum Status:</label>
                    </td>
                    <td>
                        <?= html::select(array('active' => 'Active', 'locked' => 'Locked'), 'forums_status', $forum['forums_status'], array('style' => 'margin-bottom:10px')); ?>
                    </td> 
                </tr>
                <tr>
                    <td>
                        <label>Topics Need Admin Approval:</label>
                    </td>
                    <td>
                        <?= html::checkbox(1,'forums_topic_approval',$forum['forums_topic_approval']) ?>
                    </td> 
                </tr>
                <tr>
                    <td>
                        <label>Posts Need Admin Approval:</label>
                    </td>
                    <td>
                        <?= html::checkbox(1,'forums_post_approval',$forum['forums_post_approval']) ?>
                    </td> 
                </tr>
                <? /*
                <tr>
                    <td>
                        <label>Publicly Viewable:</label>
                    </td>
                    <td>
                        <?= html::checkbox(1, 'forums_public_permission_view', $forum) ?>
                    </td> 
                </tr>
                */ ?>
            </table>
            <? if(!empty($groups) && $_REQUEST['forums_id'] != 1): ?>
            <br />
            <div class="center">Members in these groups have these permissions</div>
            <table id="permissions" class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
            <thead> 
                <tr> 
                    <td class="thead" style="width:100px">Group Name</td> 
                    <td class="thead"><input type="checkbox" id="permission_view" checked="checked" /> View</td>
                    <td class="thead"><input type="checkbox" id="permission_reply" checked="checked" /> Reply</td>
                    <td class="thead"><input type="checkbox" id="permission_new_topic" checked="checked" /> New Topic</td>
                </tr> 
            </thead> 
            <tbody>
                <? foreach($groups as $group): ?>
                <tr> 
                    <td class="tbody"><?= $group['groups_name'] ?></td>
                    <td class="tbody">
                        <? if($group['parent_view'] === 0): ?>
                        <input type="checkbox" disabled="disabled" />
                        <a href="edit.php?forums_id=<?= $group['join_forums_id_parent'] ?>">Parent Forum</a> permission not set
                        <? else: ?>
                        <?= html::checkbox(1, 'forums_groups_x_forums_permission_view['.$group['groups_id'].']',$group['forums_groups_x_forums_permission_view'],array('class' => 'permission_view')) ?>
                        <? endif ?>
                    </td>
                    <td class="tbody">
                        <? if($group['parent_reply'] === 0): ?>
                        <input type="checkbox" disabled="disabled" />
                        <a href="edit.php?forums_id=<?= $group['join_forums_id_parent'] ?>">Parent Forum</a> permission not set
                        <? else: ?>
                        <?= html::checkbox(1, 'forums_groups_x_forums_permission_reply['.$group['groups_id'].']',$group['forums_groups_x_forums_permission_reply'],array('class' => 'permission_reply')) ?>
                        <? endif ?>
                    </td>
                    <td class="tbody">
                        <? if($group['parent_new_topic'] === 0): ?>
                        <input type="checkbox" disabled="disabled" />
                        <a href="edit.php?forums_id=<?= $group['join_forums_id_parent'] ?>">Parent Forum</a> permission not set
                        <? else: ?>
                        <?= html::checkbox(1, 'forums_groups_x_forums_permission_new_topic['.$group['groups_id'].']',$group['forums_groups_x_forums_permission_new_topic'],array('class' => 'permission_new_topic')) ?>
                        <? endif ?>
                    </td>
                </tr> 
                <? endforeach ?>
            </tbody>
            </table><br />
            <? endif ?>
            <?= html::submit('Save Forum', array('class' => 'submit center')) ?>
        </fieldset>
    </form> 
</div>
<script type="text/javascript">
window.addEvent('domready', function(){
    var permissions = ['permission_view','permission_reply','permission_new_topic'];
    for(var i in permissions){
        $$('.'+permissions[i]).each(function(checkbox){
            if(!checkbox.checked) $(permissions[i]).checked = false;
        });
    }
    
    $$('#permissions thead input[type="checkbox"]').addEvent('change',function(){
        $$('.'+this.id).set('checked',this.checked);
    });
    
    <? /*
    $('forums_public_permission_view').addEvent('change', function(){
        if(this.checked){
            $('permission_view')
                .set('checked',true)
                .fireEvent('change')
                .set('disabled',true);
            
            $$('.permission_view').set('disabled',true);
        }else{
            $('permission_view').set('disabled',false);
            $$('.permission_view').set('disabled',false);
        }
    }).fireEvent('change');
    */ ?>
    
    $('theform').addEvent('submit',function(){
        $$(':disabled').set('disabled',false);
    });
});
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
