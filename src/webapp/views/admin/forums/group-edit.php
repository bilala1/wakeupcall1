<?php
$page_title = 'Edit Group | Admin';
$page_name = 'groups';
$sub_name = 'groups';
?>
<style type="text/css">
#members-list ul{
    float:left;
}
#members-list li{
    list-style:disc inside;
    width:140px;
}
</style>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
     <h2><?= $_REQUEST['groups_id'] ? 'Edit' : 'Add' ?> Group</h2>
    <form id="form" method="post" action="/admin/forums/group-edit.php">
        <input type="hidden" name="groups_id" value="<?= $group['groups_id'] ?>" />
        <?php include(VIEWS.'/admin/notice.php') ?>
        <fieldset class="alt1">
            <legend>Group Informaiton</legend>          
            <table class="form">
                <tr>
                    <td>
                        <label for="groups_name">Group Name:</label>
                    </td>
                    <td>
                        <input type="text" id="groups_name" name="groups_name" value="<?= html::filter($group['groups_name']) ?>" size="30" maxlength="255" />
                    </td> 
                </tr>
                <tr>
                    <td>
                        <label for="groups_name">Group Description:</label>
                    </td>
                    <td>
                        <textarea cols="40" rows="4" name="groups_desc" id="groups_desc"><?= html::filter($group['groups_desc']) ?></textarea>
                    </td> 
                </tr>
            </table>
            
            <table id="permissions" class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
            <thead> 
                <tr> 
                    <td class="thead">Forum Name</td> 
                    <td class="thead"><label><input type="checkbox" id="permission_view" checked="checked" /> View</label></td>
                    <td class="thead"><label><input type="checkbox" id="permission_reply" checked="checked" /> Reply</label></td>
                    <td class="thead"><label><input type="checkbox" id="permission_new_topic" checked="checked" /> New Topic</label></td>
                </tr> 
            </thead> 
            <tbody>
                <?php foreach($forums as $forum): ?>
                <tr> 
                    <td class="tbody" style="padding-left:<?= 10+($forum['sub_level']*25) ?>px">
                        <?php //if($forum['sub_level']>0): ?>
                            <img src="<?= HTTP_FULLURL ?>/images/arrow-right.png" />
                        <?php //endif ?>
                        <?= $forum['forums_name'] ?>
                    </td>
                    <td class="tbody"><?= html::checkbox(1, 'forums_groups_x_forums_permission_view['.$forum['forums_id'].']',$forum['forums_groups_x_forums_permission_view'],array('class' => 'permission_view parent_view_forums_id_'.$forum['join_forums_id_parent'], 'id' => 'view_forums_id_'.$forum['forums_id']), false) ?></td>
                    <td class="tbody"><?= html::checkbox(1, 'forums_groups_x_forums_permission_reply['.$forum['forums_id'].']',$forum['forums_groups_x_forums_permission_reply'],array('class' => 'permission_reply parent_reply_forums_id_'.$forum['join_forums_id_parent'], 'id' => 'reply_forums_id_'.$forum['forums_id']), false) ?></td>
                    <td class="tbody"><?= html::checkbox(1, 'forums_groups_x_forums_permission_new_topic['.$forum['forums_id'].']',$forum['forums_groups_x_forums_permission_new_topic'],array('class' => 'permission_new_topic parent_new_topic_forums_id_'.$forum['join_forums_id_parent'], 'id' => 'new_topic_forums_id_'.$forum['forums_id']), false) ?></td>
                </tr> 
                <?php endforeach ?>
            </tbody>
            </table><br />
            <?= html::submit('Save Group', array('class' => 'submit center')) ?>
        </fieldset>
        <fieldset class="alt1" id="members-list">
            <legend>Members</legend>
            <? foreach(array_chunk(groups::get_members($_REQUEST['groups_id']), ceil(count(groups::get_members($_REQUEST['groups_id']))/4)) as $member_chunk): ?>
                <ul>
                <? foreach($member_chunk as $member): ?>
                    <li><a href="/admin/members/edit.php?members_id=<?= $member['members_id'] ?>"><?= $member['members_firstname'].' '.$member['members_lastname'] ?></li>
                <? endforeach ?>
                </ul>
            <? endforeach ?>
            <br clear="all" />
        </fieldset>
    </form>
</div>
<script type="text/javascript">
window.addEvent('domready', function(){
    //initially set checkboxes
    var permissions = ['permission_view','permission_reply','permission_new_topic'];
    for(var i in permissions){
        $$('.'+permissions[i]).each(function(checkbox){
            if(!checkbox.checked) $(permissions[i]).checked = false;
        });
    }
    
    $$('#permissions thead input[type="checkbox"]').addEvent('change',function(){
        $$('.'+this.id).set('checked',this.checked).fireEvent('change');
    });
    
    $$('#permissions tbody input[type="checkbox"]').addEvent('change',function(){
        if(this.checked){
            $$('.parent_'+this.id).set({/*'checked':true, */disabled:false}).fireEvent('change');
        }else{
            $$('.parent_'+this.id).set({'checked':false, disabled:true}).fireEvent('change');
        }
    }).fireEvent('change');
});
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
