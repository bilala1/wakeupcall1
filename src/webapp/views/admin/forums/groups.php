<?
$page_title = 'Groups | Admin';
$page_name = 'forums';
$sub_name = 'groups';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php') ?>
    </div>
    <? include(VIEWS.'/admin/notice.php') ?>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <a href="/admin/forums/group-edit.php?groups_id=<?= (int) $_REQUEST['groups_id'] ?>" class="add">Add Group</a><br clear="right"/>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('groups_name', 'Group Name') ?></td> 
          <td class="thead"><?= strings::sort('groups_datetime', 'Created On') ?></td>
          <td class="thead"><?= strings::sort('groups_members', 'Members') ?></td>
          <td class="thead" width="200">Actions</td> 
        </tr> 
    </thead> 
    <tbody>
        <? if(empty($groups)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no groups found.
            </td></tr>
        <? endif ?>
        <? foreach($groups as $group): ?>
        <tr> 
          <td class="tbody">
            <a href="/admin/forums/group-edit.php?groups_id=<?= $group['groups_id'] ?>"><?= $group['groups_name'] ?></a>
        </td>
          <td class="tbody"><?= times::tz_convert($group['groups_datetime']) ?></td>
          <td class="tbody"><?= $group['groups_members'] ?></td>
          <td class="tbody">
            <a href="/admin/forums/group-edit.php?groups_id=<?= $group['groups_id'] ?>" title="Edit">Edit</a> |
            <a onclick="if(!confirm('Are you sure you want to remove this group?')) { return false; }" href="/admin/forums/group-delete.php?groups_id=<?= $group['groups_id'] ?>" title="Delete">Delete</a>
          </td>
        </tr> 
        <? endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
