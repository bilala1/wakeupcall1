<?php
$page_title = 'Forums | Admin';
$page_name = 'forums';
$sub_name = 'forums';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/forums/header.php') ?>
    </div>
    <?php include(VIEWS.'/admin/notice.php') ?>
    <h2><?= $forum_path ?></h2>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <a href="/admin/forums/edit.php?parent_id=<?= (int) $_REQUEST['parent_id'] ?>" class="add">Add Forum</a><br clear="right"/>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('forums_name', 'Forum Name', null, null, null) ?></td> 
          <td class="thead"><?= strings::sort('forums_date', 'Created On') ?></td>
          <td class="thead"><?= strings::sort('forums_topics', 'Topics') ?></td>
          <td class="thead" width="200">Actions</td> 
        </tr> 
    </thead> 
    <tbody>
        <?php if(empty($forums)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no forums found.
            </td></tr>
        <?php else: ?>
        <?php foreach($forums as $key => $forum): ?>
        <tr> 
          <td class="tbody" style="padding-left:<?= 10+($forum['sub_level']*25) ?>px">
            <?php if($forum['children']): ?>
            <a href="/admin/forums/list.php?parent_id=<?= $forum['forums_id'] ?>"><?= $forum['forums_name'] ?></a>
            <?php else: ?>
                <?php //if($forum['sub_level']>0): ?>
                    <img src="<?= HTTP_FULLURL ?>/images/arrow-right.png" />
                <?php //endif ?>
                <?= $forum['forums_name'] ?>
            <?php endif; ?>
        </td>
          <td class="tbody"><?= times::tz_convert($forum['forums_createdate']) ?></td>
          <td class="tbody"><?= $forum['forums_topics'] ?> (<a href="/admin/forums/view-forum.php?forums_id=<?= $forum['forums_id'] ?>">View Topics</a>)</td>
          <td class="tbody">
            <a href="/admin/forums/edit.php?forums_id=<?= $forum['forums_id'] ?>" title="Edit">Edit</a>
            
            <?php if($forum['forums_id'] != 1 && $forums[$key+1]['sub_level'] <= $forum['sub_level']): ?>
            <!--| <a onclick="if(!confirm('Are you sure you want to remove this forum, and all topics and posts in it?')) { return false; }" href="/admin/forums/delete.php?forums_id=<?= $forum['forums_id'] ?>" title="Delete">Delete</a>-->
            | <a onClick="if(!confirm('Are you sure you want to empty this forum, you will be deleting all topics and posts in it?')) { return false; }" href="/admin/forums/empty.php?forums_id=<?= $forum['forums_id'] ?>" title="Empty">Empty</a>
            <?php elseif($forum['forums_id'] != 1): ?>
            | <a onclick="alert('You must delete this forum\'s subforums first'); return false;" href="#" title="Delete">Delete</a>
            <?php endif ?>
            
            <?php if($forums[$key+1]['sub_level'] > $forum['sub_level']): ?>
            | <a href="/admin/forums/list.php?parent_id=<?= $forum['forums_id'] ?>">Sub Forums</a>
            <?php endif ?>
          </td>
        </tr> 
        <?php endforeach ?>
        <?php endif ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
