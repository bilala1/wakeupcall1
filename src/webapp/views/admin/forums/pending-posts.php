<?
$page_title = 'Pending Posts | Admin';
$page_name = 'forums';
$sub_name = 'pending-posts';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
    <? include(VIEWS.'/admin/notice.php') ?>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <br clear="right" />
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr>
            <td class="thead"><?= strings::sort('post_date', 'Post Date') ?></td>
            <td class="thead"><?= strings::sort('topics_title', 'Topic') ?></td>
            <td class="thead"><?= strings::sort('members_lastname', 'Name') ?></td> 
            <td class="thead" width="100">Actions</td> 
        </tr>
    </thead>
    <tbody>
        <? if(!$posts): ?>
        <tr>
            <td colspan="5" align="center">No posts to be listed</td>
        </tr>
        <? endif; ?>
        <? foreach($posts as $post): ?>
        <tr>
            <td class="tbody"><?= times::tz_convert($post['posts_postdate']) ?></td>
            <td class="tbody"><?= $post['topics_title'] ?></td>
            <td class="tbody"><?= $post['members_firstname'].' '.$post['members_lastname'] ?></td>
            <td class="tbody">
                <a href="/admin/forums/view-post.php?posts_id=<?= $post['posts_id'] ?>">View Post</a>
            </td>
        </tr>
        <? endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot> 
    </table>
</div><br />

<? include(VIEWS . '/admin/footer.php'); ?>
