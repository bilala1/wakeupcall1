<?php
$page_title = 'Pending Topics | Admin';
$page_name = 'forums';
$sub_name = 'pending-topics';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
    <?php include(VIEWS.'/admin/notice.php') ?>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    <br clear="right" />
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr>
            <td class="thead"><?= strings::sort('topics_createdate', 'Topic Date') ?></td>
            <td class="thead"><?= strings::sort('topics_title', 'Topic') ?></td>
            <td class="thead"><?= strings::sort('members_email', 'Username') ?></td> 
            <td class="thead" width="100">Actions</td> 
        </tr> 
    </thead>
    <tbody>
        <?php if(!$topics): ?>
        <tr>
            <td colspan="5" align="center">No topics to be listed</td>
        </tr>
        <?php endif; ?>
        <?php foreach($topics as $topic): ?>
        <tr>
            <td class="tbody"><?= times::tz_convert($topic['topics_createdate']) ?></td>
            <td class="tbody"><?= $topic['topics_title'] ?></td>
            <td class="tbody"><?= $topic['members_email'] ?></td>
            <td class="tbody">
                <a href="/admin/forums/view-topic.php?topics_id=<?= $topic['topics_id'] ?>">View Topic</a>
            </td>
        </tr>
        <?php endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>  
    </table>
</div><br />

<?php include(VIEWS . '/admin/footer.php'); ?>
