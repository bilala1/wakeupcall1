<?php
$page_title = 'Offense Reports | Admin';
$page_name = 'forums';
$sub_name = 'reports';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
    <?php include(VIEWS.'/admin/notice.php') ?>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    <br clear="right" />
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
            <td class="thead"><?= strings::sort('topics_name', 'Topic') ?></td> 
            <td class="thead"><?= strings::sort('post_date', 'Post Date', null, null, null, 'DESC') ?></td>
            <td class="thead"><?= strings::sort('report_date', 'Report Date', null, null, null, 'DESC') ?></td>
            <td class="thead"><?= strings::sort('status', 'Status') ?></td>
            <td class="thead"><?= strings::sort('reason', 'Reason') ?></td>
            <td class="thead" width="100">Actions</td> 
        </tr> 
    </thead>
    <tbody>
        <?php if(!$reports): ?>
        <tr>
            <td colspan="5" align="center">No reports to be listed</td>
        </tr>
        <?php endif; ?>
        <?php foreach($reports as $report): ?>
        <tr>
            <td class="tbody"><?= $report['topics_title'] ?></td>
            <td class="tbody"><?= times::tz_convert($report['posts_postdate']) ?></td>
            <td class="tbody"><?= times::tz_convert($report['posts_reported_datetime']) ?></td>
            <td class="tbody"><?= ucfirst($report['posts_reported_status']) ?></td>
            <td class="tbody"><?= $report['report_reasons_name'] ?></td>
            <td class="tbody">
                <a href="/admin/forums/view-report.php?posts_reported_id=<?= $report['posts_reported_id'] ?>">View Report</a>
            </td>
        </tr>
        <?php endforeach ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td colspan="6"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
    </tfoot>
    </table>
</div><br />

<?php include(VIEWS . '/admin/footer.php'); ?>
