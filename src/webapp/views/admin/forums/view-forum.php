<?
$page_title = 'Forums | Admin';
$page_name = 'forums';
$sub_name = 'forums';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php') ?>
    </div>
    <? include(VIEWS.'/admin/notice.php') ?>
    <h2>Topics in <?= $forum['forums_name'] ?>:</h2>
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
    <thead> 
        <tr> 
          <td class="thead"><?= strings::sort('title', 'Topic Title', null, null, null) ?></td> 
          <td class="thead"><?= strings::sort('posts', 'Replies') ?></td>
          <td class="thead"><?= strings::sort('date', 'Date Started') ?></td>
          <td class="thead" width="200">Actions</td>
        </tr> 
    </thead> 
    <tbody>
        <? if(empty($topics)): ?>
            <tr><td colspan="5" align="center">
                <br/>Sorry, there are no topics found.
            </td></tr>
        <? endif ?>
        <? foreach($topics as $topic): ?>
        <tr> 
            <td class="tbody"><?= $topic['topics_title'] ?></td>
            <td class="tbody"><?= $topic['topics_replies'] ?></td>
            <td class="tbody"><?= times::tz_convert($topic['topics_createdate']) ?></td>
            <td class="tbody">
                <a href="/admin/forums/view-topic.php?topics_id=<?= $topic['topics_id'] ?>">View Topic</a> |
                <a href="/admin/forums/delete-topic.php?topics_id=<?= $topic['topics_id'] ?>" onClick="return confirm('Are you sure you want to delete this topic, and all of the posts attached to it?');">Delete</a>
            </td>
        </tr> 
        <? endforeach ?>
    </tbody>
    </table>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
