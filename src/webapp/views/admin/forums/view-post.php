<?
$page_title = 'View/Edit Post | Admin';
$page_name = 'forums';
$sub_name = 'pending-posts';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>    
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#posts_body",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        plugins: "bbcode link image textcolor",
    });
/*
    var taglist = new TextboxList('posts_tags', {
        unique: true,
        plugins: {
            autocomplete: {
                minLength: 3,
                queryRemote: true,
                remote: {
                    url: BASEURL + '/ajax-tags.php'
                }
            }
        }
    }); */
});
</script>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php'); ?>
    </div>
    <h2>Edit Post</h2>
    <? include(VIEWS.'/admin/notice.php') ?>
    
    <form action="/admin/forums/view-post.php" method="post">
        <strong>Post Body</strong><br />
        <textarea id="posts_body" name="posts_body" style="width: 100%; resize: none; height: 200px;"><?= $post['posts_body'] ?></textarea><br />
        <br />
        <? /*
        <strong>Post Tags</strong> Type your keyword, and press "enter" to add it.<br />
        <?= html::input(tags::parse_tags_str($post['posts_tags']), 'posts_tags', array('class' => 'moreinfo', 'size' => '60', 'title' => 'Tags for this post', 'id' => 'posts_tags')) ?><br />
        <br />
        */ ?>
        <? if(count($post['files'])): ?>
            <div class="postfiles">
                <strong class="blue">Files:</strong> 
                <? foreach($post['files'] as $index => $file): ?>
                <a target="_blank" href="/data/<?= $file['forums_posts_files_filename'] ?>"><?= html::filter($file['forums_posts_files_original_filename']) ?></a><?= count($post['files'])-1 > $index ? ', ' : '' ?>
                <? endforeach; ?>
            </div>
        <? endif; ?>

        <div class="posts_enabled">
            <?/*<h2>Post Settings</h2>
            <table>
                <tbody>
                    <tr>
                        <td><?= html::checkbox('1','enable_bbcode',$posts_enabled['bbcode'],array('id'=>'enable_bbcode')) ?></td>
                        <td><label for="enable_bbcode">&nbsp;Enable BBCode</label></td>
                    </tr>
                    <tr>
                        <td><?= html::checkbox('1','enable_signature',$posts_enabled['signature'],array('id'=>'enable_signature')) ?></td>
                        <td><label for="enable_signature">&nbsp;Enable Forum Signature</label></td>
                    </tr>
                </tbody>
            </table>*/?>
        </div>
        <br />
        <strong>Reason for Change</strong><br />
        <input type="text" name="posts_modified_reason" value="<?= $post['posts_modified_reason'] ?>" size="60" /><br />
        <input type="hidden" name="posts_id" value="<?= $post['posts_id'] ?>" />
        <input type="submit" name="submit" value="Edit Post"  class="gray-btn"/>
        <? if(!$post['posts_approved']): ?>
        <input type="submit" name="submit"  class="submit" value="Approve Post" />
        <? endif ?>
    </form>
    <br />
    <div class="actions">
        <strong>Actions</strong>
        <? if(!$post['posts_approved']): ?>
        <a href="/admin/forums/post-act.php?mode=approve&posts_id=<?= $post['posts_id'] ?>">Approve Post</a> |
        <? endif ?>
        <a href="/admin/forums/post-act.php?mode=hidepost&posts_id=<?= $post['posts_id'] ?>">Hide Post</a> |
        <a href="/admin/forums/post-act.php?mode=deletepost&posts_id=<?= $post['posts_id'] ?>">Delete Post</a> |
        <a href="/admin/forums/post-act.php?mode=suspendmember&posts_id=<?= $post['posts_id'] ?>">Suspend Member</a>
    </div><br />
</div>

<? include(VIEWS . '/admin/footer.php'); ?>
