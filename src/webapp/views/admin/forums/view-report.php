<?php
$page_title = 'View Offense Report | Admin';
$page_name = 'reports';
$sub_name = 'reports';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>    
<<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#posts_body",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        plugins: "bbcode link image textcolor",
    });

/*
    var taglist = new TextboxList('posts_tags', {
        unique: true,
        plugins: {
            autocomplete: {
                minLength: 3,
                queryRemote: true,
                remote: {
                    url: BASEURL + '/ajax-tags.php'
                }
            }
        }
    }); */
});
</script>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/forums/header.php'); ?><br />
    </div>
    <?php include(VIEWS.'/admin/notice.php') ?>
    <label>Date Reported</label><?= times::tz_convert($report['posts_reported_datetime']); ?><br />
    <label>Reported By</label><?= $report['reporter_username'] ?><br />
    <label>Offender</label><?= $report['reported_username'] ?><br />
    <label>Status</label><?= ucfirst($report['posts_reported_status']) ?><br />
    <label>Report Reason</label><?= $report['report_reasons_name'] ?><br />
    <br />
    <strong>Report Details</strong><br />
    <?= html::filter($report['posts_reported_desc']); ?><br />
    <br />
    <form action="/admin/forums/view-report.php" method="post">
        <strong>Post Body</strong><br />
        <textarea id="posts_body" name="posts_body" style="width: 100%; resize: none; height: 200px;"><?= $report['posts_body'] ?></textarea><br />
        <br />
<? /*    
        <strong>Post Tags</strong> Type your keyword, and press "enter" to add it.<br />
        <?= html::input(tags::parse_tags_str($report['posts_tags']), 'posts_tags', array('class' => 'moreinfo', 'size' => '60', 'title' => 'Tags for this post', 'id' => 'posts_tags')) ?><br />
        <br />
*/ ?>
        <div class="posts_enabled">
            <?/*<h2>Post Settings</h2>
            <table>
                <tbody>
                    <tr>
                        <td><?= html::checkbox('1','enable_bbcode',$posts_enabled['bbcode'],array('id'=>'enable_bbcode')) ?></td>
                        <td><label for="enable_bbcode">&nbsp;Enable BBCode</label></td>
                    </tr>
                    <tr>
                        <td><?= html::checkbox('1','enable_signature',$posts_enabled['signature'],array('id'=>'enable_signature')) ?></td>
                        <td><label for="enable_signature">&nbsp;Enable Forum Signature</label></td>
                    </tr>
                </tbody>
            </table>*/?>
        </div>
        <br />
        <strong>Reason for Change</strong><br />
        <input type="text" name="posts_modified_reason" size="60" /><br />
        <input type="hidden" name="posts_id" value="<?= $report['posts_id'] ?>" />
        <input type="hidden" name="posts_reported_id" value="<?= $report['posts_reported_id'] ?>" />
        <input type="submit" value="Edit Post" />
    </form>
    <br />
    <strong>Actions</strong><br />
    <a href="/admin/forums/report-act.php?mode=ignore&fpr_id=<?= $report['posts_reported_id'] ?>">Ignore Report</a> |
    <a href="/admin/forums/report-act.php?mode=hidepost&fpr_id=<?= $report['posts_reported_id'] ?>">Hide Post</a> |
    <a href="/admin/forums/report-act.php?mode=deletepost&fpr_id=<?= $report['posts_reported_id'] ?>">Delete Post</a> |
    <a href="/admin/forums/report-act.php?mode=suspendmember&fpr_id=<?= $report['posts_reported_id'] ?>">Suspend Member</a>
</div>

<?php include(VIEWS . '/admin/footer.php'); ?>
