<?
$page_title = 'Forums | Admin';
$page_name = 'forums';
$sub_name = 'pending-topics';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include(VIEWS . '/admin/forums/header.php') ?>
    </div>
    <? include(VIEWS.'/admin/notice.php') ?>
    
    <? if(!$topic['topics_approved']): ?>
    <a href="/admin/forums/view-topic.php?topics_id=<?= $_REQUEST['topics_id'] ?>&action=approve" class="submit-link">Approve Topic</a>
    <? endif ?>
    
    <h2>Topic Title: <?= $topic['topics_title'] ?></h2>
    <br />
    
    <? foreach($posts as $post): ?>
    <div class="topicpost">
		<div class="<?= $post['posts_id'] == $topic['first_post'] ? 'topicinfo' : 'postinfo' ?>">Posted: <?= times::tz_convert($post['posts_postdate'], DATE_FORMAT_FULL) ?></div>
	
	    <div class="authorinfo">
        	<ul>
			    <li><?= $post['members_email'] ?></li>
			    <li><?= $post['avatar_small'] ?></li>
			    <li><?= $post['countries_name'] ?></li>
			    <li><strong class="blue">Posts:</strong> <?= $post['forum_posts'] ?></li>
			    <li><strong class="blue">Points:</strong> <?= $post['points']['points'] ?></li>
			    <li><?= $post['points']['bracket'] ?></li>
            </ul>
	    </div>
	
	    <div class="postbody">
			    <p class="postbodytext"><?= $post['enabled']['bbcode'] ? html::bbcode_format(html::bbcode_prettify($post['posts_body'])) : $post['posts_body']; ?></p>
                <? if($post['posts_modified_join_members_id']): ?>
                	<div class="postedit">
                        <strong class="blue">Edit:</strong> 
                        <strong>By:</strong> <?= $post['editor'] ?> 
                        <? if($post['posts_modified_reason']): ?>- <strong>Reason:</strong> <?= $post['posts_modified_reason'] ?><? endif; ?>
                    </div>
                <? endif; ?>
                <? if(sizeof($post['tags'])): ?>
               	
                <div class="posttags">
                    <strong class="blue">Tags:</strong> 
                    <? foreach($post['tags'] as $index => $tag): ?>
                    <? extract($tag) ?>
                    <a href="/forums/tag/<?= http::encode_url($global_tags_name) . '/' . $global_tags_id ?>/"><?= html::filter($global_tags_name) ?></a>
                    <?= count($post['tags'])-1 > $index ? ',' : '' ?>
                    <? endforeach; ?>
                </div>
          
                <? endif; ?>
                <? if(count($post['files'])): ?>
                    <div class="postfiles">
                        <strong class="blue">Files:</strong> 
                        <? foreach($post['files'] as $index => $file): ?>
                        <a target="_blank" href="/data/<?= $file['forums_posts_files_filename'] ?>"><?= html::filter($file['forums_posts_files_original_filename']) ?></a>
                        <?= count($post['files'])-1 > $index ? ',' : '' ?>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
      			
			    <div class="postsig">
        		<?= html::bbcode_format(html::bbcode_prettify($post['members_forum_signature'])); ?>
    			</div>
	    </div>
	    <br clear="all" />
	    <div class="postactions right">
            <a href="/admin/forums/view-post.php?posts_id=<?= $post['posts_id'] ?>" class="flag">Edit Post</a> | 
            <a href="/admin/forums/delete-post.php?posts_id=<?= $post['posts_id'] ?>" class="flag">Delete Post</a>
	    </div>
	    <br clear="right">
    </div>
    <? endforeach; ?>
    <? if($pages): ?>
    <?= $pages ?>
    <? endif ?>
</div><br />
<script type="text/javascript">
    window.addEvent('domready', function () {
        var myTips = new Tips('.moreinfo');
    });
</script>
<? include(VIEWS . '/admin/footer.php'); ?>
