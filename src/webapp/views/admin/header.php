<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title><?= $page_title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/admin/layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/mootools-core-1.4.0-full-compat-yc.js"></script>
    <script type="text/javascript" src="/js/mootools-more-1.4.0.1.js"></script>
    <script type="text/javascript" src="/js/clickout.js"></script>
    <script type="text/javascript" src="/js/MooCal.js"></script>
    <script type="text/javascript">
        var BASEURL = '<?= BASEURL ?>';
        var FULLURL = '<?= FULLURL ?>';
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        jQuery.noConflict();
        // jQuery is now an alias to the jQuery functions; creating the new alias is optional.
    </script>
    <script src="/js/global.js?v=20180618"></script>
</head>
<body>
<div id="header">
	<div class="margin">
        <a href="/admin/index.php"><img src="/images/admin-logo.png" alt="WAKEUP CALL" style="margin-bottom:1px;"/></a>

		<div class="right" style="border: 1px solid #000; background-color: #DDD; color: #000; padding: 5px; margin: 5px; font-size: 12px;">
			<a href="/admin/notifications.php">
				<strong>Notifications:</strong> <?= $db->fetch_singlet('SELECT COUNT(*) FROM admin_notifications'); ?>
			</a>
		</div>

        <ul class="navigation">
            <? if($_SESSION['admins_id']): ?>
                <li class="log" style="background:none"><a rel="external" href="/admin/logout.php" style="color: #EC9007;">Logout</a></li>
            <? else: ?>
                <li class="log"><a rel="external" id="activepage" href="/admin/login.php" style="color: #EC9007;">Login</a></li>
            <? endif ?>
            <li><a href="/admin/reports/site-use.php" <?= $page_name == 'reports' ? 'id="activepage"' : '' ?>>Reports</a></li>
            <li><a href="/admin/articles/list.php" <?= $page_name == 'blog' ? 'id="activepage"' : '' ?>>News</a></li>
            <li><a href="/admin/forums/list.php" <?= $page_name == 'forums' ? 'id="activepage"' : '' ?>>Forum</a></li>
            <li><a href="/admin/library/list.php" <?= $page_name == 'library' ? 'id="activepage"' : '' ?>>Library</a></li>
            <li><a href="/admin/surveys/list.php" <?= $page_name == 'surveys' ? 'id="activepage"' : '' ?>>Surveys</a></li>
            <li><a href="/admin/members/list.php" <?= $page_name == 'members' ? 'id="activepage"' : '' ?>>Members</a></li>
            <li><a href="/admin/webinars/list.php" <?= $page_name == 'webinars' ? 'id="activepage"' : '' ?>>Webinars</a></li>
            <li><a href="/admin/faqs/list.php" <?= $page_name == 'faqs' ? 'id="activepage"' : '' ?>>FAQ's</a></li>
            <li><a href="/admin/albums/list.php" <?= $page_name == 'albums' ? 'id="activepage"' : '' ?>>Albums</a></li>
            <li><a href="/admin/site/features.php" <?= $page_name == 'site' ? 'id="activepage"' : '' ?>>Site</a></li>
			<li><a href="/admin/coupons/list.php" <?= $page_name == 'coupons' ? 'id="activepage"' : '' ?>>Coupons</a></li>
            <li style="clear:both;display:none"></li>
        </ul>
    </div>
</div> <!--header-->
<div style="background:#fff;padding-bottom:15px; ">
<div id="wrapper">
    <div id="container">
