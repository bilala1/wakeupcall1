<?php
$page_title = 'Admin';
$page_name = 'home';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        Welcome <?= $_SESSION['admins_firstname'] ?>!
    </div>
</div>
<?php include(VIEWS .'/admin/footer.php'); ?>
