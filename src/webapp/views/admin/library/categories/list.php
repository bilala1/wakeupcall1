<?php
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'categories';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <a href="/admin/library/categories/edit.php?join_library_categories_id=<?= (int) $_REQUEST['library_categories_id'] ?>" class="add">Add Category</a><br clear="right"/>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr> 
              <td class="thead">Category</td> 
              <td class="thead">Sub-Categories</td> 
              <td class="thead" width="100">Actions</td> 
            </tr> 
        </thead>
        <tbody>
        <?php foreach(library_categories::get_library_categories_levels() as $category): ?>
        <tr>
            <td class="tbody" style="padding-left:<?= 15+($category['sub_level']*15) ?>px"><img src="<?= HTTP_FULLURL ?>/images/arrow-right.png" /> <?= $category['library_categories_name'] ?></td>
            <td class="tbody"><a href="/admin/library/documents/list.php?category=<?= $category['library_categories_id'] ?>"><?= $category['num_documents'] ?> Documents</a></td>
            <td class="tbody">
                <a href="/admin/library/categories/edit.php?library_categories_id=<?= $category['library_categories_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this category?');" href="/admin/library/categories/delete.php?library_categories_id=<?= $category['library_categories_id'] ?>">Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
            <tr>
            	<td colspan="3"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>     
        </tbody>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
