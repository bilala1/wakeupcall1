<?php
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'documents';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr>
            	<td class="thead">Document Name</td>
            	<td class="thead">Times Downloaded</td>
            </tr> 
        </thead>
        <tbody>
        <?php foreach($documents as $document): ?>
        <tr>
        	<td class="tbody"><?= $document['documents_title'] ?></td>
        	<td class="tbody"></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="2"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>     
        </tfoot>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
