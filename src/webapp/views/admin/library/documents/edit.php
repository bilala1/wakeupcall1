<?
$page_title = 'Admin';
$page_name = 'library';
$sub_name = $document['documents_type'] == 'submitted' ? 'pending-documents' : 'documents';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<style>
    input:not([type]), input[type=text] {
        width: 75%;
    }
</style>
<div class="page">
    <div class="navbar">
        <? include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <? include VIEWS .'/admin/notice.php' ?>

    <form action="/admin/library/documents/edit.php" method="post" enctype="multipart/form-data">
    
    <fieldset class="alt1">
    	<legend>Document Information</legend>
    	
        <?= html::textfield('Document Title:', $document['documents_title'], 'documents_title', array(), $errors) ?>
        
        <label>Category</label><select name="join_library_categories_id">
            <option value="">Please Select</option>
            <? foreach($categories as $category): ?>
            <option value="<?= $category['library_categories_id'] ?>" <?= $category['library_categories_id'] == $document['join_library_categories_id'] ? html::selected : null ?>><?= str_repeat('-', $category['sub_level']) ?> <?= $category['library_categories_name'] ?></option>
            <? endforeach; ?>
        </select><br />
        <? if($errors['join_library_categories_id']): ?>
        <p class="error"><?= $errors['join_library_categories_id'] ?></p>
        <? endif ?>

        <? if(!$document['documents_id']) { ?>
            <?= html::selectfield("Document Type", $documentTypes, 'documents_format', $document) ?>
        <? } else { ?>
            <?= html::hidden($document, 'documents_format') ?>
        <? } ?>

        <div class="j-documentFormatDetails j-documentFormatDetails-file">
            <? if($document['files_id']): ?>
                <a href="<?= \Services\Services::$fileSystem->getFileDownloadUrl($document) ?>"><?= $document['files_name'] ?></a><br />
            <? endif ?>

            <? if(!$_REQUEST['documents_id']): ?>
                <?= html::textfield('Document File(s):', null, 'documents_file[]', array('type' => 'file','multiple' => '','id'=>'documents_file'), $errors['documents_file']) ?>
            <? else: ?>
                <?= html::textfield('Document File:', null, 'documents_file', array('type' => 'file'), $errors) ?>
            <? endif; ?>
        </div>

        <div class="j-documentFormatDetails j-documentFormatDetails-url j-documentFormatDetails-hr360">
             <?= html::textfield('Document Link:', $document, 'documents_link', array(), $errors) ?>
        </div>
        
        <?= html::selectfield('Document Status: ', array('active' => 'Active', 'inactive' => 'Inactive'), 'documents_status', $document['documents_status'], array(), $errors) ?>
        <br />
        
        <label>Document Keywords: <span style="font-size: 65%">Separate multiple key words with commas</span>
        </label>
        <input type="text" name="documents_keywords" value="<?= html::filter($document['documents_keywords']) ?>" /><br />
        
        <?= html::checkboxfield('Free Trial Access:', 1, 'documents_freetrial', $document, array(), $errors) ?>

        <input type="hidden" name="documents_id" value="<?= $_REQUEST['documents_id'] ?>" />
        <? if($document['documents_type'] == 'submitted'): ?>
        <?= html::submit('Approve', array('name' => 'approve_action', 'class' => 'submit center')) ?>
        <?= html::submit('Do Not Approve', array('name' => 'approve_action', 'class' => 'submit center')) ?>
        <? else: ?>
        <input value="Save Document(s)" class="submit center" type="submit">
        <? endif ?>
    </fieldset>
    
    </form>
    
</div>

<script type="text/javascript" src="/js/documentLibrary.js?v=20180618"></script>

<? include(VIEWS . '/admin/footer.php'); ?>
