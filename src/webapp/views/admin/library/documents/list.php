<?php
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'documents';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	<form action="/admin/library/documents/list.php" method="get">
            <div class="search-field">
                <label for="documents_title">Title:</label><br />
                <?= html::input($_REQUEST['documents_title'], 'documents_title') ?>
            </div>
            <div class="search-field">
                <label for="category">Category:</label><br />
                <select name="category" id="category">
                    <option value="">-all-</option>
                    <?php foreach(library_categories::get_library_categories_levels() as $category): ?>
                    <option value="<?= $category['library_categories_id'] ?>" <?= $category['library_categories_id'] == $_REQUEST['category'] ? html::selected : null ?>><?= str_repeat('-', $category['sub_level']) ?> <?= $category['library_categories_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <br clear="both" />
            <div align="right">
                <input type="submit" class="submit " value="Search">
                <a class="reset gray-btn" href="/admin/library/documents/list.php">Clear Search</a>
            </div>
        </form>
	</fieldset>
    
    <a href="/admin/library/documents/edit.php" class="add">Add Document</a><br clear="right"/>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr>
              <td class="thead" width="400"><?= strings::sort('documents_title', 'Title') ?></td>
              <td class="thead" width="250"><?= strings::sort('library_categories_name', 'Category') ?></td>
              <td class="thead" width="200"><?= strings::sort('files_datetime', 'Date Uploaded') ?></td>
              <td class="thead" width="100"><?= strings::sort('documents_status', 'Status') ?></td>
              <td class="thead" width="100"><?= strings::sort('documents_format', 'Type') ?></td>
              <td class="thead" width="100">Actions</td>
            </tr> 
        </thead>
        <tbody>
        <?php foreach($documents as $document): ?>
        <tr>
            <td class="tbody"><?= $document['documents_title'] ?></td>
            <td class="tbody"><?= $document['library_categories_name'] ?></td>
            <td class="tbody"><?= date('m/d/Y h:ia', strtotime($document['files_datetime'])) ?></td>
            <td class="tbody"><?= ucfirst($document['documents_status']) ?></td>
            <td class="tbody"><?= ucfirst($document['documents_format']) ?></td>
            <td class="tbody">
                <a href="/admin/library/documents/edit.php?documents_id=<?= $document['documents_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this document?');" href="/admin/library/documents/delete.php?documents_id=<?= $document['documents_id'] ?>">Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="6"><img src="/images/admin/table-bottom.jpg" width="100%"/></td>
            </tr>     
        </tfoot>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
