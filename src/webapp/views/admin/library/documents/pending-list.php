<?
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'pending-documents';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS .'/admin/library/header.php' ?>
    </div>
    
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	<form action="/admin/library/documents/pending-list.php" method="get">
            <div class="search-field">
                <label for="documents_title">Title:</label><br />
                <?= html::input($_REQUEST['documents_title'], 'documents_title') ?>
            </div>
            <br clear="both" />
            <div align="right">
                <input type="submit" class="submit " value="Search">
                <a class="reset gray-btn" href="/admin/library/documents/list.php">Clear Search</a>
            </div>
        </form>
	</fieldset>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr>
              <td class="thead"><?= strings::sort('documents_title', 'Title') ?></td>
              <td class="thead"><?= strings::sort('members_lastname', 'Member') ?></td>
              <td class="thead"><?= strings::sort('files_datetime', 'Date Uploaded') ?></td>
              <td class="thead" width="100"><?= strings::sort('documents_status', 'Status') ?></td>
              <td class="thead" width="100">Actions</td> 
            </tr> 
        </thead>
        <tbody>
        <? foreach($documents as $document): ?>
        <tr>
            <td class="tbody"><?= $document['documents_title'] ?></td>
            <td class="tbody"><?= $document['members_firstname'].' '.$document['members_lastname'] ?></td>
            <td class="tbody"><?= date('m/d/Y h:ia', strtotime($document['files_datetime'])) ?></td>
            <td class="tbody"><?= ucfirst($document['documents_status']) ?></td>
            <td class="tbody">
                <a href="/admin/library/documents/edit.php?documents_id=<?= $document['documents_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this document?');" href="/admin/library/documents/delete.php?documents_id=<?= $document['documents_id'] ?>">Delete</a>
            </td>
        </tr>
        <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>     
        </tfoot>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
