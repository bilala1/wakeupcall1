<?
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'downloads';
?>
<? include(VIEWS . '/admin/header.php'); ?>

<script type="text/javascript">
window.addEvent('load', function(){
    new MooCal($('date_start'), {
        leaveempty: true,
        clickout: true
    });
    new MooCal($('date_end'), {
        leaveempty: true,
        clickout: true
    });
});
</script>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/library/header.php' ?>
    </div>
    
    <h2><?= $member['firm'].' >> '.$member['name']; ?> <?= $member['file_name']; ?></h2>
	    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead>
            <tr> 
              <td class="thead"><?= strings::sort('downloads_datetime', 'Date and Time') ?></td>  
              <td class="thead">File Name</td>
            </tr> 
        </thead>
        <tbody>
        <? foreach($downloads_per_member as $download): ?>
                    <tr>
                        <td class="tbody">
                            <?= $download['downloads_datetime'] ?>
                        </td>     
                        <td class="tbody">
                            <?= $download['file_name'] ?>
                        </td> 
                    </tr>
        <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="2"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
        </tbody>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
