<?
$page_title = 'Admin';
$page_name = 'library';
$sub_name = 'downloads';
?>
<? include(VIEWS . '/admin/header.php'); ?>

<script type="text/javascript">
window.addEvent('load', function(){
    new MooCal($('date_start'), {
        leaveempty: true,
        clickout: true
    });
    new MooCal($('date_end'), {
        leaveempty: true,
        clickout: true
    });
});
</script>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/library/header.php' ?>
    </div>
    
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="" method="get">
        	<label>Document Name</label>
        	<input type="text" name="name" value="<?= html::filter($_REQUEST['name']) ?>" />
            <?= html::select(array('all'=>'-All-','file'=>'Library Document','msds'=>'Chemical Document','certificate'=>'Certificate','forum file'=>'Forum File'),'type',$_REQUEST); ?>
        	Start:<input type="text" name="date_start" id="date_start" value="<?= $_REQUEST['date_start']; ?>" />
            End:<input type="text" name="date_end" id="date_end" value="<?= $_REQUEST['date_end']; ?>"/>
            
            <br /><br />
    	    <input type="submit" class="submit" value="Search" />
            <a href="/admin/library/statistics/downloads/list.php" class="reset gray-btn">Clear Search</a>
    	</form>
	</fieldset>
	    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead>
            <tr> 
              <td class="thead"><?= strings::sort('join_id', 'ID') ?></td>  
              <td class="thead"><?= strings::sort('downloads_type', 'Type') ?></td>
              <td class="thead"><?= strings::sort('join_id', 'Document Name') ?></td>
              <td class="thead"><?= strings::sort('cnt', 'Times Downloaded') ?></td>
              <td class="thead"></td>
            </tr> 
        </thead>
        <tbody>
        <? foreach($documents as $document): ?>
            
            <? switch($document['downloads_type']){
                case 'file': ?>
                    <tr>
                        <td class="tbody">
                            <?= $document['join_id'] ?>
                        </td>                        
                        <td class="tbody">
                            Library Document
                        </td>
                        <td class="tbody">
                            <a href="<?= FULLURL;?>/admin/library/statistics/downloads/detail.php?downloads_type=<?= $document['downloads_type']; ?>&join_id=<?= $document['join_id']; ?>"><?= ($document['file_title'] ? $document['file_title']: 'Document No longer Exists'); ?></a>
                        </td>
                        <td class="tbody">
                            <?= $document['cnt'] ?>
                        </td>
                        <td class="tbody">

                        </td>
                    </tr>
            <?        break;
                case 'msds': ?>
                    <tr>
                        <td class="tbody">
                            <?= $document['join_id'] ?>
                        </td>                        
                        <td class="tbody">
                            Chemical Document
                        </td>
                        <td class="tbody">
                            <a href="<?= FULLURL;?>/admin/library/statistics/downloads/detail.php?downloads_type=<?= $document['downloads_type']; ?>&join_id=<?= $document['join_id']; ?>"><?= ($document['chemical_title'] ? $document['chemical_title']: 'Document No longer Exists'); ?></a>
                        </td>
                        <td class="tbody">
                            <?= $document['cnt'] ?>
                        </td>
                        <td class="tbody">

                        </td>
                    </tr>
            <?        break;
                case 'certificate': ?>
                    <tr>
                        <td class="tbody">
                            <?= $document['join_id'] ?>
                        </td>                        
                        <td class="tbody">
                            Certificate
                        </td>
                        <td class="tbody">
                            <a href="<?= FULLURL;?>/admin/library/statistics/downloads/detail.php?downloads_type=<?= $document['downloads_type']; ?>&join_id=<?= $document['join_id']; ?>"><?= ($document['certificate_title'] ? $document['certificate_title']: 'Certificate No longer Exists'); ?></a>
                        </td>
                        <td class="tbody">
                            <?= $document['cnt'] ?>
                        </td>
                        <td class="tbody">

                        </td>
                    </tr>                    
            <?        break;
                case 'forum file': ?>
                    <tr>
                        <td class="tbody">
                            <?= $document['join_id'] ?>
                        </td>                        
                        <td class="tbody">
                            Forum File
                        </td>
                        <td class="tbody">
                            <a href="<?= FULLURL;?>/admin/library/statistics/downloads/detail.php?downloads_type=<?= $document['downloads_type']; ?>&join_id=<?= $document['join_id']; ?>"><?= ($document['forum_post_title'] ? $document['forum_post_title']: 'File No longer Exists'); ?></a>
                        </td>
                        <td class="tbody">
                            <?= $document['cnt'] ?>
                        </td>
                        <td class="tbody">

                        </td>
                    </tr>  
            <?    break; 
            } //end of switch ?>        
        <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="7"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
        </tbody>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
