<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title><?= $page_title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="/admin/layout.css" rel="stylesheet" type="text/css" />
    <style type="">
    	body{ font-size:12px;height:auto;min-height:100px;}
        
        #lgoin h1{ 
        	font-size:22px; 
            margin:0 0 10px 0; 
            padding:0;
            color:#089cd7;
        }
        table td{ color:#fff}
         a.big:hover{color:#fff;text-decoration:underline}
    </style>
 
</head>
<body>
<div id="login">
	<img src="/images/admin-logo.png" alt="WAKEUP CALL"  style="margin:0 auto; display:block"/>
    <?php if(!empty($errors)): ?>
        <span class="hint"><?= implode($errors, '<br />') ?></span><br />
    <?php endif ?>
    	
    <form method="post" action="/admin/login.php" style="width:220px;margin:0 auto;">
    
        <input name="redirect" type="hidden" value="<?= $_REQUEST['redirect'] ? html::filter($_REQUEST['redirect']) : FULLURL . '/admin/members/list.php' ?>"/>
  			<h1 style="font-size:22px; color:#089cd7;margin:0 0 10px 0; padding:0;">Account Login</h1>
            <table>
            <tr>
                <td style="color:#fff">Username:&nbsp;</td> 
                <td><input name="admins_username" type="text" maxlength="30" value="<?= html::filter($_REQUEST['admins_username']) ?>" /></td>
            </tr>
            <tr>
                <td style="color:#fff">Password:&nbsp;</td> 
                <td><input name="admins_password" type="password" maxlength="65" value="" /> </td>
            </tr>
            <tr>
                <td></td> 
                <td><input class="submit" type="submit" value="Login" /></td>
            </tr>
            <tr>
            	<td colspan="2"  align="center" style="padding-top:20px;color:#fff">Powered by Comentum CMS&trade;</td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
