<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'accounts';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>

<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>
    
    <?php include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/members/accounts/edit.php" method="post" enctype="multipart/form-data" style="width: auto;">
    
    <fieldset class="alt1" style="float: left; margin-left: 10px;">
    	<legend>Account Detail</legend>

        <?= html::selectfield(
            'Account Type: ',
            $accountTypeOptions,
            'accounts_type',
            $account['accounts_type']
            ) ?>

        <?= html::checkboxfield(
            'Test Account?',
            1,
            'accounts_is_test',
            $account
        ) ?>

        <? if($_REQUEST['accounts_id']) { ?>
            <?= html::checkboxfield(
                'Account Locked?',
                1,
                'accounts_is_locked',
                $account
            ) ?>
        <? } ?>

        <?= html::selectfield_from_query(
            $text = 'Parent Account:',
            $rows = accounts::getParentAccounts($account['accounts_id']),
            $name = 'join_accounts_id_parent',
            $selected = $account['join_accounts_id_parent'],
            $index = 'accounts_name',
            $first_option = 'None') ?>

        <?= html::textfield('Name:', $account['accounts_name'], 'accounts_name', array(), $errors) ?>
        <?= html::textfield('Address:', $account['accounts_address'], 'accounts_address', array(), $errors) ?>
        <?= html::textfield('City:', $account['accounts_city'], 'accounts_city', array(), $errors) ?>
        <?= html::textfield('State:', $account['accounts_state'], 'accounts_state', array(), $errors) ?>
        <?= html::textfield('Zip:', $account['accounts_zip'], 'accounts_zip', array(), $errors) ?>
        <?= html::textfield('Discount Code:', $account['discount_codes_code'], 'discount_codes_code', array(), $errors) ?>
        <?php if($account['discount_codes_id']) { ?>
            <div class="form-field">
                <h3>Discount Code Info:</h3>
                <label>Start Date:</label><?=$account['discount_codes_start']?>
                <?php if(strtotime($account['discount_codes_start']) > strtotime($account['next_bill'])) { ?>
                    <span class="error">Start date is after next bill</span>
                <?php } ?><br>
                <label>End Date:</label><?=$account['discount_codes_end']?>
                <?php if(strtotime($account['discount_codes_end']) < strtotime($account['next_bill'])) { ?>
                    <span class="error">End date is before next bill</span>
                <?php } ?><br>
                <label>Discount amount:</label><?=$account['discount_codes_amount'] > 0 ? money_format('%i', $account['discount_codes_amount']) : ($account['discount_codes_percent'] . '%') ?><br>
                <label>Max Uses:</label><?=$account['discount_codes_max_uses']?><br>
                <label>Uses:</label><?=$account['discount_codes_uses']?>
                <?php if($account['discount_codes_max_uses'] > 0 && $account['discount_codes_max_uses'] < $account['discount_codes_uses'] + $account['location_count'] - $account['corporate_office_count']) { ?>
                    <span class="error">Code has been used too many times</span>
                <?php } ?>
            </div>
        <?php } ?>

        <? if($_REQUEST['accounts_id']) { ?>
            <div class="form-field">
                <label>Main Account Admin:</label><a href="/admin/members/edit.php?members_id=<?= $account['members_id'] ?>"><?= $account['members_firstname'].' '.$account['members_lastname'] ?></a>
            </div>
        <? } ?>

        <? if($account['accounts_type'] == 'multi'): ?>
            <?= html::selectfield_from_query('Forum:', forums::get_all_forums(), 'join_forums_id', $account['join_forums_id'], 'forums_name') ?>
            <div class="form-field">
                <label>Number Locations (from signup):</label>
                <?= $account['corporations_num_locations'] ?>
            </div>
            <div class="form-field">
                <label>Number Locations (in WUC):</label>
                <?= $account['location_count'] ?>
            </div>
            <div class="form-field">
                <label>Number Corporate Offices (in WUC):</label>
                <?= $account['corporate_office_count'] ?>
            </div>
        <? endif; ?>
        
        <?= html::checkboxfield('Deleted:', 1, 'accounts_delete', ($account['accounts_delete_datetime']?1:0)) ?>
        <? if($account['accounts_delete_datetime']): ?>
            <span style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;Account Deleted: <?= date(DATE_FORMAT_FULL,strtotime($account['accounts_delete_datetime'])); ?></span><br />
            <i>&nbsp;&nbsp;&nbsp;&nbsp;Unchecking this checkbox will renew the account, billing them if necessary</i>
        <? endif; ?>        
    </fieldset>

        <? if(!$_REQUEST['accounts_id']) { ?>
            <fieldset class="alt1" style="float: left; margin-left: 10px;">
                <legend>Main Account Admin</legend>
                <?= html::textfield('First Name:', $account, 'members_firstname', array('maxlength' => 100), $errors) ?>

                <?= html::textfield('Last Name:', $account, 'members_lastname', array('maxlength' => 100), $errors) ?>

                <?= html::textfield('Position/Title:', $account, 'members_title', array( 'maxlength' => 40), $errors) ?>

                <?= html::textfield('Email:', $account, 'members_email', array('maxlength' => 255), $errors) ?>

                <?= html::textfield('Repeat Email:', $account, 'members_email2', array('maxlength' => 255), $errors) ?>

                <?= html::passwordfield('Password:', '', 'members_password', array('class' => 'input-text'), $errors) ?>

                <?= html::passwordfield('Repeat Password:', '', 'members_password2', array('class' => 'input-text'), $errors) ?>
            </fieldset>
        <? } ?>

        <fieldset class="alt1" style="float: left; margin-left: 10px;">
            <legend>Account Features</legend>
            <?php foreach($accountFeatures as $accountFeature) { ?>
                <label
                    style="display:block; width: auto"
                    for='<?= 'account_features[' . $accountFeature['account_features_type'] . ']' ?>'>
                    <?=
                    html::checkbox(
                        1,
                        'account_features[' . $accountFeature['account_features_type'] . ']',
                        $accountFeature['accounts_x_account_features_enabled']) ?>
                    <?= $accountFeature['account_features_name'] ?>
                </label>
            <?php } ?>
            <?php if($errors['account_features']) { ?>
                <p class="error"><?= $errors['account_features'] ?></p>
            <? } ?>
        </fieldset>

        <?php if($account['accounts_id']) { ?>
            <input type="hidden" name="accounts_id" value="<?= $_REQUEST['accounts_id'] ?>" />
        <?php } ?>

    <input value="Save" class="submit center clear" type="submit">

        <fieldset class="alt1">
            <legend>Account Admins (<?= count($admins) ?>)</legend>
            <? foreach($admins as $member): ?>
                <a href="/admin/members/edit.php?members_id=<?=$member['members_id']?>">
                    <?= $member['members_firstname'] ?> <?= $member['members_lastname'] ?>
                </a>
                <br/>
            <? endforeach; ?>
        </fieldset>


        <fieldset class="alt1">
            <legend>Billing History</legend>
            <style>
                #billing-table {
                    width: 100%;
                    border: solid 1px;
                }

                #billing-table tr {
                    border: solid 1px;
                }

                #billing-table th {
                    text-align: center;
                }

                #billing-table td {
                    text-align: center;
                    padding-top: 2px;
                    padding-bottom: 2px;
                }

            </style>
            <? if (empty($bills)) {
                echo "<div>No billing history.</div>";
            } else { ?>
                <table id="billing-table">
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Paid</th>
                        <th>Discount Code</th>
                        <th>Discount Amount</th>
                    </tr>
                    <? foreach ($bills as $bill): ?>
                        <tr align="center" style="border: solid 1px;">
                            <td><?= date(DATE_FORMAT, strtotime($bill['bills_datetime'])) ?></td>
                            <td><?= money_format('%n', $bill['bills_amount']) ?></td>
                            <td><?= $bill['bills_type'] ?></td>
                            <td><?= $bill['bills_description'] ?></td>
                            <td><?= $bill['bills_paid'] ? 'Yes' : 'No' ?></td>
                            <td><?= isset($bill['discount_codes_code']) ? $bill['discount_codes_code'] : "None" ?></td>
                            <td><?= isset($bill['discount_codes_code']) ?
                                    ($bill['discount_codes_amount'] > 0 ? "$".$bill['discount_codes_amount'] : $bill['discount_codes_percent']."%" )  : "N/A" ?>
                            </td>
                        </tr>
                    <? endforeach ?>
                </table>
            <? } ?>
        </fieldset>

        <?php if(!empty($childAccounts)) { ?>
        <fieldset class="alt1">
            <legend>Child Accounts (<?= count($childAccounts) ?>)</legend>
            <? foreach($childAccounts as $childAccount): ?>
                <a href="/admin/members/accounts/edit.php?accounts_id=<?=$childAccount['accounts_id']?>">
                    <?= $childAccount['accounts_name'] ?>
                </a>
                <br/>
            <? endforeach; ?>
        </fieldset>
        <? } ?>
    </form>
    
</div>
<br />

<script type="text/javascript">
window.addEvent('load', function () {
    var myTips = new Tips('.moreinfo');
});

$('accounts_delete').addEvent('click', function(){
    if($('accounts_delete').checked){
        if(!confirm('Are you sure you want to delete this account? It will remained "deleted" for 60 days before being removed from the databse completely.')) { return false; }
    }
    else {
        if(!confirm('Are you sure you want to renew this account?')) { return false; }
    }
})
</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
