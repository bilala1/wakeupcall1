<?
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'accounts';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/members/header.php' ?>
    </div>
    <? /*
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="/admin/members/list.php" method="get">
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>
    */ ?>
	
	<a href="/admin/members/accounts/edit.php" class="add">Add account</a><br clear="right"/>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr> 
              <th class="thead"><?= strings::sort('name', 'Name') ?></th>
              <th class="thead"><?= strings::sort('type', 'Type') ?></th>
                <th class="thead"><?= strings::sort('is_locked', 'Locked?') ?></th>
              <th class="thead"><?= strings::sort('parent_accounts_name', 'Parent') ?></th>
              <th class="thead"><?= strings::sort('num_locations', 'Total Locations') ?></th>
              <th class="thead">Child Accounts</th>
              <th class="thead"><?= strings::sort('next_bill', 'Next Bill') ?></th>
              <th class="thead">Discount Code</th>
              <th class="thead">Actions</th>
            </tr> 
        </thead>
        <tbody>
        <? foreach($accounts as $account): ?>
            <tr>
                <td class="tbody"><?= $account['accounts_name'] ?></td>
                <td class="tbody"><?= ucwords($account['accounts_type']) ?></td>
                <td class="tbody"><?= $account['accounts_is_locked'] ? 'Locked' : 'Active' ?></td>
                <td class="tbody"><?= $account['parent_accounts_name'] ?></td>
                <td class="tbody"><?= $account['num_locations'] ?></td>
                <td class="tbody"><?= $account['num_children'] ?></td>
                <td class="tbody"><?= $account['next_bill'] ?></td>
                <td class="tbody"><?= $account['discount_codes_code'] ?>
                    <?php if($account['discount_codes_code'] && (
                            strtotime($account['discount_codes_start']) > strtotime($account['next_bill']) ||
                            strtotime($account['discount_codes_end']) < strtotime($account['next_bill']) ||
                            ($account['discount_codes_max_uses'] > 0 && $account['discount_codes_max_uses'] < $account['discount_codes_uses'] + $account['num_locations']))
                    ){ ?>
                        <br><span class="error">Potential problem</span>
                    <?php } ?>
                </td>
                <td class="tbody">
                    <a href="/admin/members/accounts/edit.php?accounts_id=<?= $account['accounts_id'] ?>">Edit</a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="8"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
