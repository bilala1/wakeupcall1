<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = '';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>

<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>
    
    <?php include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/members/edit.php" method="post" enctype="multipart/form-data" style="width: auto;">
    
    <fieldset class="alt1" style="float: left;">
    	<legend>Member Detail</legend>
    	
        <?= html::textfield('First Name:', $member, 'members_firstname', array(), $errors) ?>
        
        <?= html::textfield('Last Name:', $member, 'members_lastname', array(), $errors) ?>
        
        <?= html::textfield('Position/Title:', $member, 'members_title', array(), $errors) ?>
        
        <?= html::textfield('Email:', $member, 'members_email', /*$_REQUEST['members_id'] ? array('readonly'=>'readonly','style'=>'background-color: #efefef'):*/array(), $errors) ?>

        <?= html::textfield('Phone:', $member, 'members_phone', array(), $errors) ?>

        <div class="form-field">
            <?= html::selectfield('Status:', $statuses, 'members_status', $member['members_status'], array(), $errors) ?>
        </div>

        <? if ($member['members_id'] == $member['join_members_id']): //member is the main account admin ?>
            <?= html::checkboxfield('Exempt:', 1, 'exempt', $member['members_status'] == 'exempt' ? 1 : 0, $member['members_status'] == 'exempt' ? array('disabled' => 'disabled') : null) ?>
        <? endif; ?>

        <?= html::checkboxfield('Accepted HR:', 1, 'members_hr_agreement_yn', $member['members_hr_agreement_yn'] == '1' ? 1 : 0, array()) ?>

        <?= html::checkboxfield('Accepted E-Law:', 1, 'members_elaw_agreement_yn', $member['members_elaw_agreement_yn'] == '1' ? 1 : 0, array()) ?>

        
        <?= html::textfield('Reset Password:', '', 'members_password', array('type' => 'password'), $errors) ?>

        <? if($franchise['franchises_name']): ?>
            <div class="form-field">
            <label>Franchise:</label><a href="/admin/members/franchises/edit.php?franchises_id=<?= $franchise['franchises_id'] ?>"><?= $franchise['franchises_name'] ?></a>
            </div>
        <? endif ?>
        <? if(!empty($member['members_verified_code'])): ?>
        <p/>
            <? if($member['members_status'] == 'free'): ?>
            <p/><a href="/admin/members/resend-15-day-trial-email.php?members_id=<?= $_REQUEST['members_id'] ?>" title="Resend">Resend Trial Verification Email</a>
            <? else: ?>
            <p/><a href="/admin/members/resend-joined-member-email.php?members_id=<?= $_REQUEST['members_id'] ?>" title="Resend">Resend Joined Member Verification Email</a>
            <? endif ?>
            <div>
                <a href="#" id="member-verify-email" data-id="<?= $_REQUEST['members_id'] ?>">Verify Email</a>
            </div>
        <? endif; ?>
    </fieldset>

    <fieldset class="alt1" style="float: left; margin-left: 10px; max-width:660px;">
        <legend>Permissions</legend>

        <div class="form-field">
            <?= ($member['members_id'] == $member['join_members_id'] ? 'Main ' : '') . ucwords($member['members_type']) ?> for account
            <a href="<?= FULLURL; ?>/admin/members/accounts/edit.php?accounts_id=<?= $member['accounts_id'] ;?>"><?= $member['accounts_name'] ?></a>
        </div>

        <? if(count($acc_perms)): ?>
            <div class="form-field">
                <label>Account/Admin level:</label>
                <span>
                    <? foreach($acc_perms as $acc_perm): ?>
                        <?= $acc_perm['members_access_levels_name'] ?>,
                    <? endforeach; ?>
                </span>
            </div>
        <? endif; ?>

        <? foreach($loc_perms as $loc_perm): ?>
            <div class="form-field">
                <label><a href="/admin/members/hotels/edit.php?licensed_locations_id=<?= $loc_perm['licensed_locations_id'] ?>"><?= $loc_perm['licensed_locations_name'] ?></a></label>
                <span><?= $loc_perm['perms'] ?></span>
            </div>
        <? endforeach; ?>

    </fieldset>

    <fieldset class="alt1" style="float: left; margin-left: 10px;">
    	<legend>Billing Detail</legend>
    	
        <?= html::textfield('Address Line 1:', $member['members_billing_addr1'], 'members_billing_addr1', array(), $errors) ?>
        
        <?= html::textfield('Address Line 2:', $member['members_billing_addr2'], 'members_billing_addr2', array(), $errors) ?>
        
        <?= html::textfield('City:', $member['members_billing_city'], 'members_billing_city', array(), $errors) ?>
        
        <?= html::selectfield('State:', html::$us_states, 'members_billing_state', $member['members_billing_state'], array(), $errors) ?>
        
        <?= html::textfield('Zipcode:', $member['members_billing_zip'], 'members_billing_zip', array(), $errors) ?>
    </fieldset>

    <br clear="all" />
    
    <fieldset class="alt1" style="">
    	<legend>Card Information</legend>
    	
        <?= html::textfield('Card Number:', strings::format_cc($member['members_card_num']), 'members_card_num', array(), $errors) ?>
        
        <?= html::textfield('Card Type:', $member['members_card_type'], 'members_card_type', array(), $errors) ?>
        
        <?= html::textfield('Card Expire Month:', $member['members_card_exp_month'], 'members_card_exp_month', array('size' => 1, 'maxlength' => 2), $errors) ?>
        
        <?= html::textfield('Card Expire Year:', $member['members_card_exp_year'], 'members_card_exp_year', array('size' => 1, 'maxlength' => 4), $errors) ?>
    </fieldset>
    <br />
    
    <fieldset class="alt1">
    <legend>Forum Specifics</legend>
    <table>
    <tr>
        <td>
            <label for="groups">Forum Groups:</label>
        </td>
        <td>
	        <?= html::select_from_query($_groups = forums::get_groups(0), 'groups_ids[]', $groups_ids, 'groups_name', null, array('multiple' => 'multiple', 'style' => 'margin-bottom:2px', 'size' => min(10, count($_groups)))) ?><br />
            (Hold Ctrl to select/deselect)
	    </td> 
    </tr>
    <tr>
        <td width="90"  style="padding-top:10px">
            <label class="description">Signature:</label>
        </td>
        <td style="padding-top:10px">
            <textarea name="members_forum_signature" id="members_forum_signature" title="Shows below each post" class="moreinfo"><?= utf8_decode($member['members_forum_signature']) ?></textarea>
        </td> 
    </tr>
    </table><br />
    </fieldset>

        <fieldset class="alt1">
            <legend>Billing History</legend>
            <style>
                #billing-table {
                    width: 100%;
                    border: solid 1px;
                }

                #billing-table tr {
                    border: solid 1px;
                }

                #billing-table th {
                    text-align: center;
                }

                #billing-table td {
                    text-align: center;
                    padding-top: 2px;
                    padding-bottom: 2px;
                }

            </style>
            <? if (empty($bills)) {
                echo "<div>No billing history.</div>";
            } else { ?>
                <table id="billing-table">
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Paid</th>
                        <th>Discount Code</th>
                        <th>Discount Amount</th>
                    </tr>
                        <? foreach ($bills as $bill): ?>
                            <tr align="center" style="border: solid 1px;">
                                <td><?= date(DATE_FORMAT, strtotime($bill['bills_datetime'])) ?></td>
                                <td><?= money_format('%n', $bill['bills_amount']) ?></td>
                                <td><?= $bill['bills_type'] ?></td>
                                <td><?= $bill['bills_description'] ?></td>
                                <td><?= $bill['bills_paid'] ? 'Yes' : 'No' ?></td>
                                <td><?= isset($bill['discount_codes_code']) ? $bill['discount_codes_code'] : "None" ?></td>
                                <td><?= isset($bill['discount_codes_code']) ?
                                        ($bill['discount_codes_amount'] > 0 ? "$".$bill['discount_codes_amount'] : $bill['discount_codes_percent']."%" )  : "N/A" ?>
                                </td>
                            </tr>
                        <? endforeach ?>
                </table>
            <? } ?>
        </fieldset>
<div style="padding-top:10px;">
    <input type="hidden" name="members_id" value="<?= $_REQUEST['members_id'] ?>" />
    <input value="Save" class="submit center" type="submit">
</div>

    </form>


</div>
<br />
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#members_forum_signature",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        plugins: "bbcode link image textcolor",
    });

    $('exempt').addEvent('click', function(){
        if(this.checked){
            this.checked = confirm('Are you sure? This action is not reversible. You cannot change a member from exempt to non-exempt');
        }
    });
});

// jQuery
jQuery(document).ready(function($){
    $('#member-verify-email').on('click',function(event)
    {
        var link = $(this);
        $.post("ajax-verify-user.php",{member_id : link.data("id")})
            .done(function(){
                link.detach();
                alert('Email Verified');
            })
            .fail(function(){ alert('Oops. Something went wrong.'); })
        event.preventDefault();
    });
});

</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
