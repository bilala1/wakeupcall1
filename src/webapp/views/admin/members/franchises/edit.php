<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'franchises';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="/js/GrowingInput.js"></script>

<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>

    <?php include VIEWS . '/admin/notice.php' ?>

    <form id="form" action="/admin/members/franchises/edit.php" method="post" enctype="multipart/form-data"
          style="width: 100%;">

        <fieldset class="alt1">
            <legend>Franchise Detail</legend>

            <table>
                <tr>
                    <td WIDTH="500">
                        <?= html::textfield('Name:', $franchise['franchises_name'], 'franchises_name', array(), $errors) ?>
                        <?= html::textfield('Code:', $franchise['franchises_code'], 'franchises_code', array(), $errors) ?>

                        <div class="form-field">
                            <?= html::selectfield_from_query('Admin Member:', members::get_all_names(), 'join_members_id', $franchise['join_members_id'], 'members_fullname', '- none -') ?>
                        </div>

                        <? if ($_REQUEST['franchises_id']): ?>


                        <div class="form-field">
                            <label>&nbsp;</label>
                            <a href="/admin/members/edit.php?members_id=<?= $franchise['members_id'] ?>">View Admin
                                Details</a>
                        </div>
                        <? endif ?>

                        <div class="form-field">
                            <label>Discount amount:<br/>"$xx.xx" or "xx%" </label>
                            <input type="text" name="code_value" value="<?=$franchise['code_value']?>"/>

                            <div class="error">
                                <?=$errors['code_value']?>
                            </div>
                        </div>
                        <?//= html::checkboxfield('Active:', 1, 'franchises_active', $franchise['franchises_active']) ?>
                        <?= html::checkboxfield('Deleted:', 1, 'franchises_delete', ($franchise['franchises_delete_datetime'] ? 1 : 0)) ?>
                        <? if ($franchise['franchises_delete_datetime']): ?>
                        <span style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;Franchise Deleted: <?= date(DATE_FORMAT_FULL, strtotime($franchise['franchises_delete_datetime'])); ?></span>
                        <br/>
                        <i>&nbsp;&nbsp;&nbsp;&nbsp;Unchecking this checkbox will renew the franchise, billing them if
                            necessary</i>
                        <? endif; ?>

                        <p></p>
                        <input type="hidden" name="franchises_id" value="<?= $_REQUEST['franchises_id'] ?>"/>
                        <input value="Save" class="submit center" type="submit">
                    </td>
                    <td WIDTH="50%">

                        <div class="center photo-thumb-container">
                            <? if ($_REQUEST['franchises_id']): ?>
                            <img src="<?=empty($album['albums_root_folder']) ? '/data/albums/' : $album['albums_root_folder'] . "/";?><?= html::filter($photo['albums_photos_filename']) ?>.org.jpg"
                                 alt="photo" width="400px"/>

                            <p></p>
                            <a class="center"
                               href="/admin/members/franchises/logoedit.php?franchises_id=<?= $franchise['franchises_id'] ?>&albums_id=<?= $album['albums_id'] ?>&albums_photos_id=<?= $photo['albums_photos_id'] ?>"
                               title="Edit">Update Logo</a>

                            <? else:?>
                            You must save the franchise before you can upload a logo.
                            <? endif; ?>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>

    </form>

</div>
<br/>
<script type="text/javascript">
    window.addEvent('load', function () {
        var myTips = new Tips('.moreinfo');
    });

    $('franchises_delete').addEvent('click', function () {
        if ($('franchises_delete').checked) {
            if (!confirm('Are you sure you want to delete this franchise? It will remaine "deleted" for 60 days before being removed from the database completely.')) {
                return false;
            }
        }
        else {
            if (!confirm('Are you sure you want to renew this franchise?')) {
                return false;
            }
        }
    })
</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
