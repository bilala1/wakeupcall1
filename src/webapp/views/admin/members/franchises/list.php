<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'franchises';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>
    <? /*
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="/admin/members/list.php" method="get">
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>
    */ ?>

    <? include VIEWS . '/admin/notice.php' ?>

    <a href="/admin/members/franchises/edit.php" class="add">Add Franchise</a><br clear="right"/>

    <?php if($pages): ?>
    <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>

    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
        <thead>
        <tr>
            <th class="thead"><?= strings::sort('name', 'Name') ?></th>
            <th class="thead"><?= strings::sort('members_lastname', 'Owner') ?></th>
            <th class="thead"><?= strings::sort('franchises_code', 'Code') ?></th>
            <th class="thead"><?= strings::sort('franchises_pct_discount', 'Percent Discount') ?></th>
            <th class="thead"><?= strings::sort('franchises_logo_filename', 'Logo') ?></th>
            <?/*<th class="thead" width="100"><?= strings::sort('active', 'Active') ?></th>*/?>
            <th class="thead">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($franchises as $franchise): ?>
        <tr>
            <td class="tbody"><?= $franchise['franchises_name'] ?></td>
            <td class="tbody"><?= $franchise['members_firstname'] . ' ' . $franchise['members_lastname'] ?></td>
            <td class="tbody"><?= $franchise['franchises_code'] ?></td>
            <td class="tbody"><?=$franchise['franchises_amount_discount'] > 0 ? money_format('%n', $franchise['franchises_amount_discount']) : $franchise['franchises_pct_discount'] . '%'?></td>
            <td class="tbody"><?= $franchise['franchises_logo_filename'] ?></td>
            <?/*<td class="tbody"><?= $franchise['franchises_active'] ? '<span style="color: #009900;">active</span>' : '<span style="color: #990000;">inactive</span>' ?></td>*/?>
            <td class="tbody">
                <a href="/admin/members/franchises/edit.php?franchises_id=<?= $franchise['franchises_id'] ?>">Edit</a>
            </td>
        </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
        </tr>
        </tfoot>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
