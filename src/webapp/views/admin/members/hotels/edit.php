<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'locations';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>

<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>
    
    <?php include VIEWS .'/admin/notice.php' ?>
    
    <form id="form" action="/admin/members/hotels/edit.php" method="post" enctype="multipart/form-data" style="width: auto;">
    
    <fieldset class="alt1">
    	<legend>Location Detail</legend>

        <? if(!$licensed_location['licensed_locations_id']) {
            echo html::selectfield_from_query('Account', accounts::getAccountsThatCanGainALocation(), 'join_accounts_id', $hotel['join_accounts_id'], 'accounts_name', 'Please Select', array('style' => 'width:255px'), $errors);
            echo html::selectfield('Location Type', arrays::values_as_keys(array_merge(array('Please Select'),array_keys(licensed_locations::get_location_types()))), 'licensed_locations_full_type', $hotel, array('style' => 'width:255px'), $errors);
        } else { ?>
            <input type="hidden" name="licensed_locations_id" value="<?= $_REQUEST['licensed_locations_id'] ?>" />
            <div class="form-field">
                <label>Account</label>
                <span><a href="/admin/members/accounts/edit.php?accounts_id=<?= $account['accounts_id'] ?>"><?= $account['accounts_name'] ?></a></span>
            </div>
            <div class="form-field">
                <label>Location Type</label>
                <span><?= $licensed_location['licensed_locations_full_type'] ?></span>
            </div>
        <? } ?>

        <?= html::textfield('Name:', $licensed_location['licensed_locations_name'], 'licensed_locations_name', array(), $errors) ?>
        <?= html::textfield('Address:', $licensed_location['licensed_locations_address'], 'licensed_locations_address', array(), $errors) ?>
        <?= html::textfield('City:', $licensed_location['licensed_locations_city'], 'licensed_locations_city', array(), $errors) ?>
        <?= html::textfield('State:', $licensed_location['licensed_locations_state'], 'licensed_locations_state', array(), $errors) ?>
        <?= html::textfield('Zip:', $licensed_location['licensed_locations_zip'], 'licensed_locations_zip', array(), $errors) ?>
        <?= html::textfield('Phone:', $licensed_location['licensed_locations_phone'], 'licensed_locations_phone', array('class' => 'phone'), $errors) ?>
        <?= html::textfield('Fax:', $licensed_location['licensed_locations_fax'], 'licensed_locations_fax', array('class' => 'phone'), $errors) ?>
        <?= '' /*html::selectfield_from_query('Corporation:', corporations::get_all(), 'join_corporations_id', $licensed_location['join_corporations_id'], 'corporations_name', '- none -')*/ ?>

        <?= html::selectfield_from_query('Franchise:', franchises::get_all(), 'join_franchises_id', $licensed_location['join_franchises_id'], 'franchises_name', '- none -') ?>
        
        <?//= html::checkboxfield('Active:', 1, 'hotels_active', $hotel['hotels_active']) ?>
        <?= html::checkboxfield('Deleted:', 1, 'licensed_locations_delete', ($licensed_location['licensed_locations_delete_datetime']?1:0)) ?>
        <? if($licensed_location['licensed_locations_delete_datetime']): ?>
            <span style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;Hotel Deleted: <?= date(DATE_FORMAT_FULL,strtotime($licensed_location['licensed_locations_delete_datetime'])); ?></span><br />
            <i>&nbsp;&nbsp;&nbsp;&nbsp;Unchecking this checkbox will renew the location, billing them if necessary</i>
        <? endif; ?>
    </fieldset>

    <input value="Save" class="submit center" type="submit">

        <fieldset class="alt1">
            <legend>Location Users</legend>
            <? foreach($members as $members_id => $member): ?>
            <a href="/admin/members/edit.php?members_id=<?=$members_id?>">
                <?= $member['members_firstname'] ?> <?= $member['members_lastname'] ?>
                </a>
            <br/>
            <? endforeach; ?>
        </fieldset>
    </form>
    
</div>
<br />
<script type="text/javascript">
window.addEvent('load', function () {
    var myTips = new Tips('.moreinfo');
});

$('licensed_locations_delete').addEvent('click', function(){
    if($('licensed_locations_delete').checked){
        if(!confirm('Are you sure you want to delete this location? It will remained "deleted" for 60 days before being removed from the database completely.')) {
            return false;
        }
    }
    else {
        if(!confirm('Are you sure you want to renew this location?')) {
            return false;
        }
    }
})
</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
