<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'locations';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>
    <? /*
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="/admin/members/list.php" method="get">
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>
    */ ?>
	
	<a href="/admin/members/hotels/edit.php" class="add">Add Location</a><br clear="right"/>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr>
              <th class="thead"><?= strings::sort('name', 'Name') ?></th>
              <th class="thead" width="80"><?= strings::sort('accounts_name', 'Account') ?></th>
              <th class="thead" width="80"><?= strings::sort('franchises_name', 'Franchise') ?></th>
              <?/*<th class="thead" width="100"><?= strings::sort('active', 'Active') ?></th>*/?>
              <th class="thead">Actions</th>
            </tr> 
        </thead>
        <tbody>
        <?php foreach($licensed_locations as $licensed_location): ?>
            <tr>
                <td class="tbody"><?= $licensed_location['licensed_locations_name'] ?></td>
                <td class="tbody"><?= $licensed_location['accounts_name'] ?></td>
                <td class="tbody"><?= $licensed_location['franchises_name'] ?></td>
                <?/*<td class="tbody"><?= $licensed_location['licensed_locations_active'] ? '<span style="color: #009900;">active</span>' : '<span style="color: #990000;">inactive</span>' ?></td>*/?>
                <td class="tbody">
                    <a href="/admin/members/hotels/edit.php?licensed_locations_id=<?= $licensed_location['licensed_locations_id'] ?>">Edit</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
