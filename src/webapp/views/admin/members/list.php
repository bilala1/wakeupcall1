<?
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'members';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/members/header.php' ?>
    </div>
    <? include VIEWS . '/admin/notice.php' ?>
    <!-- Add doesn't work, commenting out for now -->
    <!--a href="/admin/members/edit.php" class="add">Add Member</a--><br clear="right"/>

    <fieldset class="alt1">
    	<legend>Search / Filter</legend>

    	<form action="/admin/members/list.php" method="get">
        	<?/*<label>Corporation</label>
        	<select name="corporations_id">
        	    <option value="">-all-</option>
        	    <option value="none">-none-</option>
        	<? foreach($companies as $company): ?>
        	    <option value="<?= $company['corporations_id'] ?>" <?= $_REQUEST['corporations_id'] == $company['corporations_id'] ? html::selected : null ?>><?= $company['corporations_name'] ?></option>
        	<? endforeach; ?>
        	</select><br />*/?>

            <?= '';//html::selectfield_from_query('Corporation:', corporations::get_all(), 'corporations_id', $_REQUEST['corporations_id'], 'corporations_name', array('' => '- all -')) ?>

        	<label>Name</label>
        	<input type="text" name="name" value="<?= html::filter($_REQUEST['name']) ?>" />
        	<br />

    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>

	<?/*<a href="/admin/members/edit.php" class="add">Add Member</a><br clear="right"/>*/?>

    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
        <thead>
            <tr>
              <td class="thead"><?= strings::sort('type', 'Type') ?></td>
              <td class="thead"><?= strings::sort('name', 'Name') ?></td>
              <td class="thead"><?= strings::sort('company', 'Account') ?></td>
			  <td class="thead" width="100"><?= strings::sort('members_last_login', 'Last Login') ?></td>
              <td class="thead" width="100"><?= strings::sort('date', 'Date Added') ?></td>
              <td class="thead" width="70"><?= strings::sort('status', 'Status') ?></td>
              <td class="thead">Actions</td>
            </tr>
        </thead>
        <tbody>
        <? foreach($members as $member): ?>
            <tr>
                <td class="tbody"><?= ($member['members_id'] == $member['join_members_id'] ? 'Main ' : '') . ucwords($member['members_type']) ?></td>
                <td class="tbody"><?= $member['members_firstname'] . ' ' . $member['members_lastname'] ?></td>
                <td class="tbody"><a href="<?= FULLURL; ?>/admin/members/accounts/edit.php?accounts_id=<?= $member['accounts_id'] ;?>"><?= $member['accounts_name'] ?></a></td>
				<td class="tbody"><?= $member['members_last_login'] ? date('m/d/Y g:ia', strtotime($member['members_last_login'])) : 'N/A' ?></td>
                <td class="tbody"><?= date('m/d/Y', strtotime($member['members_datetime'])) ?></td>
                <td class="tbody"><?= $member['members_status'] ?></td>
                <td class="tbody">
                    <a href="<?= HTTPS_FULLURL ?>/admin/members/edit.php?members_id=<?= $member['members_id'] ?>">Edit</a> | <a onclick="if(!confirm('Are you sure you want to permanently remove this member?')){ return false; }else{ if(!confirm('If they are the admin for a corporation, then it will be removed along with all of its hotels. If they are the admin for a hotel, then it will be removed.')){ return false; }}" href="/admin/members/delete.php?members_id=<?= $member['members_id'] ?>" title="Delete">Delete</a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="7"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
        </tbody>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
