<?php
$page_title = 'Admin';
$page_name = 'members';
$sub_name = 'email';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>

<div class="page">
    <div class="navbar">
        <?php include VIEWS . '/admin/members/header.php' ?>
    </div>

    <?php include VIEWS .'/admin/notice.php' ?>

    <form id="form" action="" method="post" enctype="multipart/form-data" style="width: auto;">

		<fieldset style="padding: 5px;">
			<legend>From</legend>

			<select name="from_email">
				<option value="<?= CONCIERGE_EMAIL ?>"><?= CONCIERGE_EMAIL ?></option>
				<option value="info@wakeupcall.net">info@wakeupcall.net</option>
				<option value="<?= MEMBERSHIP_EMAIL ?>"><?= MEMBERSHIP_EMAIL ?></option>
                <option value="noreply@wakeupcall.net">noreply@wakeupcall.net</option>
				<option value="support@wakeupcall.net">support@wakeupcall.net</option>
			</select>
		</fieldset>

        <fieldset style="padding:5px">
            <legend>Whom to send?</legend>

			<div style="border: dotted #ccc thin; padding:5px;">
				<h2>Single person</h2>
				<input type="text" id="autocomplete" name="member_selected" style="width: 400px; font-size: 13px; padding: 5px" /><br />
			</div>

			<div class="clear"><br /></div>

            <div style="float:left; width:31% ;display:block;border: dotted #ccc thin; padding:5px; margin-right:5px; min-height: 120px;">
                <h2>Members</h2>
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <?= html::checkboxfield('Corporate', 1, 'members_corporate_locations', $post['members_corporate_locations'], array(), $errors); ?>
                        </td>
                        <td>
                            (<?= count(array_unique($members_corporate_locations)); ?>)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Single locations', 1, 'members_single_locations', $post['members_single_locations'], array(), $errors); ?>
                        </td>
                        <td>
                            (<?= count(array_unique($members_single_locations)); ?>)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Not Rejoined', 1, 'members_not_rejoined', $post['members_not_rejoined'], array(), $errors); ?>
                        </td>
                        <td>
                            (<?= count(array_unique($members_not_rejoined)); ?>)
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float:left; width:31% ;display:block;border: dotted #ccc thin; padding:5px; min-height: 120px;">
                <h2>Trial Members</h2>
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <?= html::checkboxfield('Current', 1, 'trial_current', $post['trial_current'], array(), $errors); ?>
                        </td>
                        <td>
                            (<?= count(array_unique($trial_current)); ?>)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Not Joined', 1, 'trial_not_joined', $post['trial_not_joined'], array(), $errors); ?>
                        </td>
                        <td>
                            (<?= count(array_unique($trial_not_joined)); ?>)
                        </td>
                    </tr>
                </table>
            </div>


            <div style=" margin-left: 5px; float:left; width:30% ;display:block; border: dotted #ccc thin; padding:5px; min-height: 120px;">
                <h2>Notifications</h2>
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <?= html::checkboxfield('Message Board Updates:', 1, 'notifications_forum_update', $post) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Weekly News Updates:', 1, 'notifications_news_update', $post) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Blog News Updates:', 1, 'notifications_blog_update', $post) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Additional Services Updates:', 1, 'notifications_additional_services', $post) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Webinar Events Updates:', 1, 'notifications_webinars_update', $post) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= html::checkboxfield('Training Courses Updates:', 1, 'notifications_training_update', $post) ?>
                        </td>
                    </tr>
                </table>
            </div>

            <br class="clear" />
            <?= ($errors['checkboxes'])? '<span class="error">'.$errors['checkboxes'].'</span>':''; ?>
            <span id="checkboxes_error" class="error" style="display:none">Please select at least one checkbox</span>
        </fieldset>

        <fieldset style="padding:10px">
            <legend>Mass Email <span id="email_count">0</span> Members</legend>
            <div style="width:70%; float:left; display: block">
            <table>
            <tr>
                <td colspan="2">
                   <?= html::textfield('Email Title:', $post['message_title'], 'message_title', array('maxlength'=>'200','size'=>'85'), $errors) ?>
                    <span id="message_title_error" class="error" style="display:none">Please enter Email Title</span>
                </td>
            </tr>
            <tr>
                <td width="90">
                    <label for="message_content" class="description">Message:</label>
                    <?= ($errors['message_content'])? '<span class="error">'.$errors['message_content'].'</span>':''; ?>
                    <span id="message_error" class="error" style="display:none"><br />Please enter a message</span>
                </td>
                <td style="padding-top:10px">
                    <textarea rows="10" cols="90" name="message_content" id="message_content"  class="moreinfo"><?= utf8_decode($post['message_content']) ?></textarea>
                </td>
            </tr>
            </table>
            </div>
            <br class="clear" />
            <div>
            <table>
                <tr id="attachment-row">
                    <td  colspan="2">
                        <?= html::textfield('Email Attachment:', '', 'attachment0', array('type' => 'file', 'class'=>'attachment-input'), $errors) ?>
                    </td>
                </tr>
                <tr id="attachment-last-row">
                    <td>
						<a id="add-new-attachment" href="" class="add left" >Add more</a>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                </tr>
            </table>
            </div>
            <br class="clear" />
            <br />
            <input name="send_email" value="Send Email" class="submit center" type="submit" />
        </fieldset>
    </form>

</div>
<script type="text/javascript" src="/js/AutoComplete.js"></script>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">

window.addEvent('domready', function() {
	var auto1 = new AutoComplete($('autocomplete'), 'members_ajax.php');
});


window.addEvent('load', function () {
    var myTips = new Tips('.moreinfo');
    tinyMCE.init({
        menubar: "",
        toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect | bullist numlist | outdent indent | undo redo | link unlink image | removeformat | subscript superscript | code",
        toolbar_items_size: 'small',
        selector: "#message_content",
        plugins: "link image textcolor code",
        target_list: false,
        default_link_target: "_blank",
        link_title: false
    });
});

window.addEvent('load', function(){
	var checkboxes = {};

	//-- update count
	$('autocomplete').addEvent('change', function() {
		$('email_count').set('text', '1');
	});

	$('form').getElements('input[type=checkbox]').addEvent('change', function(evt) {
		$('form').getElements('input[type=checkbox]').each(function(el) {
			var name = el.get('name');
			var checked = el.checked ? 1 : 0;

			checkboxes[name] = checked;
		});

		var req = new Request({
			url: '<?= http::current_url() ?>',
			method: "post",
			data: {
				check_members_count: true,
				checkboxes: checkboxes
			},
			onSuccess: function(response) {
				$('email_count').set('text', response);
			}
		}).send();
	});

    $('add-new-attachment').addEvent('click', function(){
        var count = $$('.attachment-input').length;
        var clone = $('attachment-row').clone();
        clone.getElement('input').set('name','attachment'+count);
        clone.getElement('input').set('value','');
        clone.inject('attachment-last-row','before');

        return false;
    })
});

    $$('[name=send_email]').addEvent('click', function(){
        var noerror = true;
        //validate title
        if($('message_title').get('value').length < 1){
           $('message_title_error').setStyle('display','inline');
           noerror = false;
        }
        else {
           $('message_title_error').setStyle('display','none');
        }
        //validate checkboxes
        if( !$('members_corporate_locations').checked &&
            !$('members_single_locations').checked &&
            !$('members_not_rejoined').checked &&
            !$('trial_current').checked &&
            !$('trial_not_joined').checked &&
			$('autocomplete').get('value') == ''
            /* &&
            !$('notifications_forum_update').checked &&
            !$('notifications_news_update').checked &&
            !$('notifications_blog_update').checked &&
            !$('notifications_additional_services').checked &&
            !$('notifications_webinars_update').checked &&
            !$('notifications_training_update').checked   */
          ){
            $('checkboxes_error').setStyle('display','inline');
            noerror = false;
        }
        else {
           $('checkboxes_error').setStyle('display','none');
        }

        //validate message
        var myIfr = $('message_content_ifr');
        var content = myIfr.contentWindow.document.body.innerHTML;
        if(content == '' || content == '<br _mce_bogus="1">') {
            $('message_error').setStyle('display','inline');
            noerror = false;
        }
        else {
            $('message_error').setStyle('display','none');
        }

        return noerror;
    })
</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
