<?php
if(!$notice && isset($_REQUEST['notice'])){
    $notice = urldecode($_REQUEST['notice']);
}
if($_REQUEST['error']){
    $errors[] = urldecode($_REQUEST['error']);
}
if($errors){
    $error_string = '';
    foreach($errors as $key => $error){
        $error_string .= is_int($key) ? $error : '';
    }
}
?>
<?php if($notice): ?>
<p class="notice"><?= $notice ?></p>
<?php endif ?>
<?php if($errors && !$error_string) { ?>
    <p class="errors">There are errors below</p>
<?php } ?>
<?php if($error_string): ?>
<p class="errors"><?= $error_string ?></p>
<?php endif ?>