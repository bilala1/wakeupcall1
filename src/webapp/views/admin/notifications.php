<?php
$page_title = 'Admin';
$page_name = 'home';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
	<div class="navbar">
		Welcome <?= $_SESSION['admins_firstname'] ?>!
	</div>

	<fieldset class="alt1">
		<legend>Notifications</legend>

		<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
			<thead>
				<tr>
					<td class="thead">Subject</td>
					<td class="thead">Message</td>
					<td class="thead">Date/Time</td>
					<td class="thead">Actions</td>
				</tr>
			</thead>
			<tbody>
				<? if (empty($notifications)): ?>
					<tr>
						<td colspan="4" class="tbody" align="center">
							Sorry, no notifications to list.
						</td>
					</tr>
				<? endif; ?>
				<? foreach ($notifications as $n): ?>
					<tr>
						<td class="tbody"><?= $n['admin_notifications_subject'] ?></td>
						<td class="tbody"><?= $n['admin_notifications_message'] ?></td>
						<td class="tbody"><?= date('m/d/Y g:ia', strtotime($n['admin_notifications_datetime'])) ?></td>
						<td class="tbody">
							<a style="cursor: pointer" onclick="deleteNotification('<?= $n['admin_notifications_id'] ?>')">Delete</a>
						</td>
					</tr>
				<? endforeach ?>
			</tbody>
		</table>
	</fieldset>
</div>

<script type="text/javascript">
	function deleteNotification(notifications_id) {
		if (confirm('Are you sure you want to delete this?')) {
			new Request({
				method: 'post',
				data: {
					ajax: 'delete_notif',
					notifications_id: notifications_id
				},
				onSuccess: function() {
					window.location.reload();
				}
			}).send();
		}
	}
</script>

<?php include(VIEWS . '/admin/footer.php'); ?>
