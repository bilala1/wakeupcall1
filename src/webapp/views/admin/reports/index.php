<?php
$page_title = 'Admin';
$page_name = 'reports';
$sub_name = 'msds';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<style type="text/css">
.page{
    font-size:14px;
}
fieldset{
    padding:10px;
}
.page fieldset label{
    width:180px;
    font-weight:bold;
}
.tbody, .thead{
    text-align:right;
}
tfoot .tbody{
    font-weight:bold;
    border-top:2px solid black;
    font-size:1.2em;
}
</style>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/reports/header.php') ?>
    </div>
    <? include VIEWS . '/admin/notice.php' ?>
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="/admin/reports/index.php" method="get">
        	<label>Date Range</label>
        	<?= html::input($_GET, 'from') ?> TO <?= html::input($_GET, 'to') ?>
    	    
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>
    
    <? if($days): ?>
    <fieldset class="alt1">
        <legend>SDS Stats</legend>
        <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="40%" align="center">
            <tr>
                <th class="thead" style="text-align:center">Date</th>
                <th class="thead">Number of Searches</th>
                <th class="thead">Number of Downloads</th>
            </tr>
            <tbody>
                <? foreach($days as $day): ?>
                <? if($day['num_searches'] || $day['num_downloads']): ?>
                <tr>
                    <td class="tbody" style="text-align:center"><?= $day['date'] ?></td>
                    <td class="tbody"><?= $day['num_searches'] ?></td>
                    <td class="tbody"><?= $day['num_downloads'] ?></td>
                </tr>
                <? endif ?>
                <? endforeach ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="tbody">Totals:</td>
                    <td class="tbody"><?= $total_searches ?></td>
                    <td class="tbody"><?= $total_downloads ?></td>
                </tr>
            </tfoot>
        </table>
    </fieldset>
    <? endif ?>
</div>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
new MooCal('from', {
    width: 200,
    readonly: false
});
new MooCal('to', {
    width: 200,
    readonly: false
});
</script>
<?php include(VIEWS .'/admin/footer.php'); ?>
