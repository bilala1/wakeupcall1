<?php
$page_title = 'Admin';
$page_name = 'reports';
$sub_name = 'logins';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
	<div class="navbar">
		<?php include(VIEWS . '/admin/reports/header.php') ?>
	</div>
    <? include VIEWS . '/admin/notice.php' ?>

	<fieldset class="alt1">
		<legend>Search / Filter</legend>

		<form action="" method="get">
			<label>Date Range</label>
			<?= html::input($_GET, 'from') ?> TO <?= html::input($_GET, 'to') ?>

			<input type="submit" class="submit" value="Search" />
		</form>
	</fieldset>

	<fieldset class="alt1">
		<legend>Member Stats</legend>

		<table class="tborder" cellpadding="6" cellspacing="1" border="0" width="40%" align="center">
			<tr>
				<th class="thead">Member Name</th>
				<th class="thead">Number of Logins</th>
			</tr>
			<tbody>
				<? foreach ($members_rows as $row): ?>
				<? if ($row['login_count']): ?>
					<tr>
						<td class="tbody"><?= $row['members_lastname'] ?>, <?= $row['members_firstname'] ?></td>
						<td class="tbody" align="center"><?= $row['login_count'] ?></td>
					</tr>
					<? endif ?>
				<? endforeach ?>
			</tbody>
		</table>
	</fieldset>

</div>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
	new MooCal('from', {
		width: 200,
		readonly: false
	});
	new MooCal('to', {
		width: 200,
		readonly: false
	});
</script>
<?php include(VIEWS . '/admin/footer.php'); ?>
