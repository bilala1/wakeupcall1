<?php
$page_title = 'Admin';
$page_name = 'reports';
$sub_name = 'msds-report';
?>
<?php include(VIEWS . '/admin/header.php'); ?>


<style type="text/css">
.page{
    font-size:14px;
}
fieldset{
    padding:10px;
}
.page fieldset label{
    width:180px;
    font-weight:bold;
}
.tbody, .thead{
    text-align:right;
}
tfoot .tbody{
    font-weight:bold;
    border-top:2px solid black;
    font-size:1.2em;
}
.multiradio-container span {
    display: block;

}
</style>
<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/reports/header.php') ?>
    </div>
    <? include VIEWS . '/admin/notice.php' ?>
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
    	<form action="/admin/reports/msds-report.php" method="get">
            <?//= html::multiradio('Select Report', 'select_report', array('1'=>'Members download more than once per membership term ','2'=>'Two'), $checked, $errors); ?>
            
            
        	<label>Date Range</label>
        	<?= html::input($_GET, 'from') ?> TO <?= html::input($_GET, 'to') ?>
    	    
    	    <input type="submit" class="submit" value="Search" />
    	</form>
	</fieldset>
    
    
    <? if($msds_downloads): ?>
    <fieldset class="alt1">
        <legend>SDS Dowloads Report</legend>
        <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
            <tr>
                <th class="thead" style="text-align: center">Entity</th>
                <th class="thead" style="text-align: center;">Downloads</th>
                <th class="thead" style="text-align: center;">Corporation</th>
                <th class="thead" style="text-align: center;">Last Bill</th>
                <th class="thead" style="text-align: center;">Next Bill</th>
            </tr>
            <tbody>
                <? foreach($msds_downloads as $k => $msds): ?>
                    <? if(!in_array($msds['entity_id'], $entities)): ?>
                        <tr>
                            <td class="tbody" style="text-align:center">
                                <? $name = members::get_contact_info($msds['entity_id']);?>
                                <? //todo test licensed locations ?>
                                <a href="<?= FULLURL; ?>/admin/members/<?= $name['type'] == 'corporation' ? 'corporations': 'hotels'; ?>/edit.php?<?= $name['type'] == 'corporation'  ? 'corporations_id': 'licensed_locations_id'; ?>=<?= $name['id'];?>"><?= $name['name'] ?></a>
                            </td>
                            <td class="tbody">
                                <?= $totals[$msds['entity_id']]; ?>
                            </td>
                            <td class="tbody">
                                <? $corp = members::get_contact_info($msds['corporation_id']); echo $corp['name'];  ?>
                            </td>
                            <td class="tbody">
                                <?= date('m/d/Y', strtotime($msds['last_bill'])); ?>
                            </td>
                            <td class="tbody">
                                <?= date('m/d/Y', strtotime($msds['next_bill'])); ?>
                            </td>
                        </tr>
                    <? $entities[] = $msds['entity_id']; ?>
                    <? endif; ?>    
               
                <? endforeach ?>
            </tbody>
            <tfoot>
                
            </tfoot>
        </table>
    </fieldset>
    <? elseif($_GET): ?>
    <fieldset class="alt1">
        Nothing was found
    </fieldset>
    <? endif ?>
</div>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
new MooCal('from', {
    width: 200,
    readonly: false
});
new MooCal('to', {
    width: 200,
    readonly: false
});
</script>
<?php include(VIEWS .'/admin/footer.php'); ?>
    
    
    