<?php
$page_title = 'Admin';
$page_name = 'reports';
$sub_name = 'site-use';
?>
<?php include(VIEWS . '/admin/header.php'); ?>

<div class="page">
    <div class="navbar">
        <?php include(VIEWS . '/admin/reports/header.php') ?>
    </div>
    <? include VIEWS . '/admin/notice.php' ?>

    <form class="j-report-form">
        <div class="form-field">
            <label>Report Type</label>
            <span style="display:inline-block">
                <label><input type="radio" name="reportType" value="byArea" checked> Total Feature Use</label><br>
                <label><input type="radio" name="reportType" value="byTime"> Feature Use Over Time</label>
           </span>
        </div>

        <?= html::selectfield('Account', $accounts, 'accounts_id', $selectedAccount) ?>
        <?= html::datefield('Start Date', $filters, 'start_date') ?>
        <?= html::datefield('End Date', $filters, 'end_date') ?>

        <div class="form-field">
            <label>Include Categories</label>
                <span style="display:inline-block">
                    <label><input type="checkbox" class="j-byTime-includeArea" value="certs" checked> Certificates</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="files" checked> Files</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="claims" checked> Claims</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="contracts" checked> Contracts</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="library" checked> Library</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="forums" checked> Forums</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="other" checked> Other</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="dashboard" checked> Dashboard</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="hr" checked> HR</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="elaw" checked> E-Law</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="businesses" checked> Businesses</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="webinars" checked> Webinars</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="search" checked> Search</label><br>
                    <label><input type="checkbox" class="j-byTime-includeArea" value="news" checked> News</label><br>
               </span>
        </div>

        <div class="j-chartOptions j-chartOptions-byArea" style="display:none">

        </div>

        <div class="j-chartOptions j-chartOptions-byTime" style="display:none">
            <div class="form-field">
                <label>Y-Axis</label>
                <span style="display:inline-block">
                    <label><input type="radio" name="byTimeStackType" value="absolute" checked> Total Hits</label><br>
                    <label><input type="radio" name="byTimeStackType" value="percent"> Percent of Hits</label>
               </span>
            </div>
            <div class="form-field">
                <label>Group By</label>
                <span style="display:inline-block">
                    <label><input type="radio" name="byTimeGroupType" value="day" checked> Day</label><br>
                    <label><input type="radio" name="byTimeGroupType" value="week"> Week</label>
                    <label><input type="radio" name="byTimeGroupType" value="month"> Month</label>
               </span>
            </div>
        </div>
        <button class="j-generate">Show Report</button>
    </form>
    <div id="j-usage-chart" style="width:100%; height:600px;"></div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    window.addEvent('domready', function () {
        <? /* open or collapse infobox? */ ?>
        <? if($infobox): ?>
        $$('.page-desc').reveal();
        $$('.info_text').set('html', 'Hide Text Box');
        <? endif; ?>
        google.charts.load('current', {packages: ['corechart', 'bar']});
    });

</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
<script type="text/javascript" src="/js/moment.min.js?v=20180618"></script>
<script type="text/javascript" src="/js/site-use.js?v=20180618"></script>
<?php include(VIEWS . '/admin/footer.php'); ?>
