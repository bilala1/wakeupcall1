<?php
$page_title = 'Admin';
$page_name = 'site';
$sub_name = 'features';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">

    <? include VIEWS . '/admin/notice.php' ?>
    <h2>Site Features</h2>

    <form method="POST">
    <?php foreach($siteFeatures as $siteFeature) { ?>
    <?= html::checkboxfield($siteFeature['site_features_name'], 1, 'site_features_active['.$siteFeature['site_features_id'].']', $siteFeature['site_features_active'] == '1' ? 1 : 0) ?>
    <?php } ?>
        <div class="form-actions text-center">
            <input value="Save Active Features" class="btn btn-primary" type="submit">
        </div>
    </form>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
