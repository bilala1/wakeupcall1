<?php
$page_title = 'Admin';
$page_name = 'surveys';
$sub_name = '';
?>
<?php include(VIEWS . '/admin/header.php'); ?>

<link href="/css/colorpicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/MooRainbow.js"></script>
<script type="text/javascript">

window.addEvent('load', function(){
    $$('.colorpicker').each(function(picker, idx){
        var current_input = picker.getParent('div.survey_option').getElement('input.colorinput');
        var current_color = current_input.get('value').split(',');

        new MooRainbow(picker, {
            id: 'picker_' + idx,
            startColor: current_color,
            wheel: false,
            onComplete: function(color){
                this.element.setStyle('background-color', 'rgb(' + color.rgb + ')');
                current_input.set('value', color.rgb);
            },
            onChange: function(color){
                this.element.setStyle('background-color', 'rgb(' + color.rgb + ')');
                current_input.set('value', color.rgb);
            }
        });
    });

    $$('.remove').addEvent('click', function(evt){
        evt.stop();

        this.getParent('div.survey_option').dispose();
    });

    $('add_option').addEvent('click', function(evt){
        evt.stop();

        var container = new Element('div', {
            'class': 'survey_option'
        });

        var option_name = new Element('input', {
            'name': 'new_options[]',
            'type': 'text'
        }).inject(container);

        var remove = new Element('a', {
            'href': '#',
            'class': 'remove',
            'text': 'Remove'
        }).inject(container);

        remove.addEvent('click', function(evt){
            evt.stop();

            this.getParent('div.survey_option').dispose();
        });

        var option_color = [Math.ceil(Math.random()*255),Math.ceil(Math.random()*255),Math.ceil(Math.random()*255)];

        var option_colorinput = new Element('input', {
            'type': 'hidden',
            'name': 'new_colors[]',
            'value': option_color.join(',')
        }).inject(container);

        var option_colorpicker = new Element('div', {
            'class': 'right colorpicker',
            'styles': {
                'background-color': 'rgb(' + option_color.join(',') + ')',
                'width': 20,
                'height': 20
            }
        }).inject(container);

        container.inject($('options'));

        new MooRainbow(option_colorpicker, {
            id: 'picker_' + $$('.colorpicker').length,
            startColor: option_color,
            wheel: false,
            onComplete: function(color){
                this.element.setStyle('background-color', 'rgb(' + color.rgb + ')');
                option_colorinput.set('value', color.rgb);
            },
            onChange: function(color){
                this.element.setStyle('background-color', 'rgb(' + color.rgb + ')');
                option_colorinput.set('value', color.rgb);
            }
        });
    });
});

</script>

<div class="page">
    <div class="navbar">
    </div>

    <?php include VIEWS .'/admin/notice.php' ?>

    <form id="form" action="/admin/surveys/edit.php" method="post" enctype="multipart/form-data">

    <fieldset class="alt1">
    	<legend>Survey</legend>

        <?= html::textfield('Title:', $survey['surveys_title'], 'surveys_title', array('type' => 'text', 'style' => 'width: 415px'), $errors) ?>

        <?= html::selectfield('Status:', array('active' => 'Active', 'inactive' => 'Inactive'), 'surveys_status', $survey['surveys_status'], array(), $errors) ?>
        <br />

        <img src="/images/admin/icon-add.jpg" style="vertical-align: middle;"/> <a href="#" id="add_option">Add Option</a><br /><br />
        <div id="options">
            <?php foreach((array) $options as $option): ?>
            <div class="survey_option">
                <input type="text" name="options[<?= $option['survey_options_id'] ?>]" value="<?= $option['survey_options_name'] ?>" style="width: 475px"/>
                <input type="hidden" class="colorinput" name="options_color[<?= $option['survey_options_id'] ?>]" value="<?= $option['survey_options_color'] ?>" />
                <a href="#" class="remove">Remove</a>
                <div class="right colorpicker" style="background-color: rgb(<?= $option['survey_options_color'] ?>); width: 20px; height: 20px; margin-top: 3px">&nbsp;</div>
            </div>
            <?php endforeach; ?>
        </div>

        <input type="hidden" name="surveys_id" value="<?= $_REQUEST['surveys_id'] ?>" />
        <input value="Save Survey" class="submit center" type="submit">
    </fieldset>

    <?php if($survey): ?>
    <fieldset class="alt1">
    	<legend>Survey Results</legend>

    	<?php if(file_exists(SITE_PATH .'/images/surveys/survey_'.$survey['surveys_id'].'.png')): ?>
	    <img src="/images/surveys/survey_<?= $survey['surveys_id'] ?>.png" alt="" />
    	<?php endif; ?>

    	<table>
    	    <tbody>
    	    <?php foreach($options as $option): ?>
    	        <tr>
    	            <td width="150" class="tbody"><?= $option['survey_options_name'] ?></td>
    	            <td class="tbody" align="center"><?= $total_votes ? ($option['votes'] / $total_votes) * 100 : 0 ?>%</td>
					<td class="tbody" align="center"><?= $option['votes'] ?></td>
	            </tr>
    	    <?php endforeach; ?>
				<tr>
					<td class="tbody" align="right" colspan="2">
						Total Responses:
					</td>
					<td class="tbody" align="center">
						<?= $total_votes ?>
					</td>
				</tr>
    	    </tbody>
	    </table>

	</fieldset>
    <?php endif; ?>

    </form>

</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
