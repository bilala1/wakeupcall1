<?php
$page_title = 'Admin';
$page_name = 'surveys';
$sub_name = 'documents';
?>
<?php include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
    </div>
    <? /*
    <fieldset class="alt1">
    	<legend>Search / Filter</legend>
    	
	</fieldset>
    */ ?>
    
    <a href="/admin/surveys/edit.php" class="add">Add Survey</a><br clear="right"/>
    
    <?php if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <?php endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center"> 
        <thead> 
            <tr> 
              <td class="thead" width="400"><?= strings::sort('title', 'Title') ?></td>
              <td class="thead"><?= strings::sort('status', 'Status') ?></td>
              <td class="thead"><?= strings::sort('date', 'Date Addded') ?></td>
              <td class="thead"><?= strings::sort('responses', 'Responses') ?></td>
              <td class="thead">Actions</td>
            </tr> 
        </thead>
        <tbody>
        <?php foreach($surveys as $survey): ?>
        <tr>
            <td class="tbody"><?= $survey['surveys_title'] ?></td>
            <td class="tbody"><?= $survey['surveys_status'] ?></td>
            <td class="tbody"><?= date('m/d/Y h:ia', strtotime($survey['surveys_datetime'])) ?></td>
            <td class="tbody"><?= $survey['responses'] ?></td>
            <td class="tbody">
                <a href="/admin/surveys/edit.php?surveys_id=<?= $survey['surveys_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this?');" href="/admin/surveys/delete.php?surveys_id=<?= $survey['surveys_id'] ?>">Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
    </table>
</div>
<br />

<?php include(VIEWS . '/admin/footer.php'); ?>
