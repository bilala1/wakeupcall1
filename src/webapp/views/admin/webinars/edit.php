<?
$page_title = 'Admin';
$page_name = 'webinars';
$sub_name = 'webinars';
?>
<? include(VIEWS . '/admin/header.php'); ?>

<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/MooTime.js"></script>
<script type="text/javascript">

window.addEvent('load', function(){
    new TextboxList($$('input[name="webinars_keywords"]')[0]);
});

</script>

<div class="page">
    <? include VIEWS .'/admin/notice.php' ?>

    <form id="form" action="/admin/webinars/edit.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="webinars_id" value="<?= $_REQUEST['webinars_id'] ?>" />

    <fieldset class="alt1">
    	<legend>Webinar Information</legend>

        <?= html::textfield('Name:', $webinar['webinars_name'], 'webinars_name', array(), $errors) ?>

        <?= html::textareafield('Description:', $webinar['webinars_description'], 'webinars_description', array('style' => 'width: 250px; height: 100px;'), $errors) ?>

        <?= html::textareafield('Connection Information:', $webinar['webinars_connection_info'], 'webinars_connection_info', array('style' => 'width: 250px; height: 100px;'), $errors) ?>

        <?= html::textfield('File 1 - Name:', $webinar, 'webinars_file1_name', array(), $errors) ?>

        <div class="form-field">
            <label for="webinars_file1">File 1- File:</label>

            <? if($webinar['webinars_file1']): ?>
            <a href="<?= WEBINAR_DIR . $webinar['webinars_file1'] ?>"><?= $webinar['webinars_file1'] ?></a><br />
            <label></label>
            <? endif ?>

            <input type="file" id="webinars_file1" name="webinars_file1">

            <? if($errors['webinars_file1']): ?>
            <p class="error form-error"><?= $errors['webinars_file1'] ?></p>
            <? endif ?>
        </div>

        <?= html::textfield('File 2 - Name:', $webinar, 'webinars_file2_name', array(), $errors) ?>

        <div class="form-field">
            <label for="webinars_file2">File 2 - File:</label>

            <? if($webinar['webinars_file2']): ?>
            <a href="<?= WEBINAR_DIR . $webinar['webinars_file2'] ?>"><?= $webinar['webinars_file2'] ?></a><br />
            <label></label>
            <? endif ?>

            <input type="file" id="webinars_file2" name="webinars_file2">
        </div>
		<?= html::textfield('Link:', $webinar['webinars_link'], 'webinars_link', array('style' => 'width:400px'), $errors) ?>
		<?= html::textfield('Date:', times::from_mysql_utc($webinar['webinars_datetime'], DATE_FORMAT), 'date', array(), $errors) ?><br />
		<?= html::textfield('Time:', times::from_mysql_utc($webinar['webinars_datetime'], DATE_FORMAT_TIME), 'time', array(), $errors) ?>

		<? if ($_REQUEST['webinars_id'] && !webinars::is_upcoming($webinar)): ?>
            <div class="form-field">
                <label>Recording:</label>

                <? if($webinar['webinars_file_recording']): ?>
                <a href="<?= WEBINAR_DIR . $webinar['webinars_file_recording'] ?>"><?= $webinar['webinars_file_recording'] ?></a><br />
                <label></label>
                <? endif ?>

                <input type="file" name="webinars_file_recording" /><br />
                <? if($errors['webinars_file_recording']): ?>
                <p class="error form-error"><?= $errors['webinars_file_recording'] ?></p>
                <? endif ?>
            </div>
        <? endif ?>

        <div>
            <label>Keywords:</label>
            <?= html::input($webinar['webinars_keywords'], 'webinars_keywords') ?>
            <em>(press enter between words)</em>
        </div>
        <br />

        <input value="Save" class="submit center" type="submit">
    </fieldset>

    </form>

	<? if (!empty($attendees)): ?>
    <fieldset class="alt1" style="width:600px">
    	<legend>Attendees</legend>
        <table class="tborder">
            <thead>
                <tr>
                    <td class="thead">Name</td>
                    <td class="thead">Register Date</td>
                </tr>
            </thead>
            <tbody>
                <? foreach($attendees as $attendee): ?>
                <tr>
                    <td class="tbody"><?= $attendee['members_firstname'].' '.$attendee['members_lastname'] ?></td>
                    <td class="tbody"><?= date(DATE_FORMAT_FULL, strtotime($attendee['webinars_x_members_datetime'])) ?></td>
                </tr>
                <? endforeach ?>
            </tbody>
        </table>
    </fieldset>
	<? endif; ?>
</div>
<br />
<script type="text/javascript">
window.addEvent('load', function () {
    var cal = new MooCal('date', {
        width: 200,
        leaveempty: false
    });

    var time = new MooTime('time', {
        multiplier: 5,
        format: 'h:m a'
    });
});
</script>

<? include(VIEWS . '/admin/footer.php'); ?>
