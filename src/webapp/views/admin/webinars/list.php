<?
$page_title = 'Admin';
$page_name = 'webinars';

$sub_name = $_REQUEST['type'] ? $_REQUEST['type'] : 'upcoming';
?>
<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/webinars/header.php' ?>
    </div>
    
    <? include VIEWS .'/admin/notice.php' ?>
    
    <a href="/admin/webinars/edit.php" class="add">Add Webinar</a><br clear="right"/>
    
    <? if($pages): ?>
        <span class="pages">
            Pages: <?= $pages; ?>
        </span>
    <? endif ?>
    
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
        <thead>
            <tr> 
                <td class="thead"><?= strings::sort('webinars_datetime', 'Date') ?></td>
                <td class="thead"><?= strings::sort('webinars_name', 'Webinar') ?></td>
                <td class="thead">Description</td>
                <td class="thead"><?= strings::sort('participants', '# Participants') ?></td>
                <td class="thead" style="width:100px">Actions</td>
            </tr> 
        </thead>
        <tbody>
        <? if(empty($webinars)): ?>
        <tr><td colspan="5" align="center"><br />Sorry, there are no webinars found.</td></tr>
        <? else: ?>
        <? foreach($webinars as $webinar): ?>
        <tr>
            <td class="tbody"><?= times::from_mysql_utc($webinar['webinars_datetime']) ?></td>
            <td class="tbody"><?= $webinar['webinars_name'] ?></td>
            <td class="tbody"><?= $webinar['webinars_description'] ?></td>
            <td class="tbody">
                <? if($webinar['participants'] > 0): ?>
                <a href="<?= FULLURL; ?>/admin/webinars/participants-list.php?webinars_id=<?= $webinar['webinars_id'] ?>">
                    <?= $webinar['participants'] ?> participants
                </a>
                <? else : ?>
                    none
                <? endif; ?>
            </td>
            <td class="tbody">
                <a href="/admin/webinars/edit.php?webinars_id=<?= $webinar['webinars_id'] ?>">Edit</a> |
                <a onClick="return confirm('Are you sure you want to delete this?');" href="/admin/webinars/delete.php?webinars_id=<?= $webinar['webinars_id'] ?>">Delete</a>
            </td>
        </tr>
        <? endforeach; ?>
        <? endif ?>
        </tbody>
        <tfoot>
            <tr>
            	<td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>
    </table>
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
