<?
$page_title = 'Admin';
$page_name = 'webinars';

$sub_name = $_REQUEST['type'] ? $_REQUEST['type'] : 'upcoming';
?>

<? include(VIEWS . '/admin/header.php'); ?>
<div class="page">
    <div class="navbar">
        <? include VIEWS . '/admin/webinars/header.php' ?>
    </div>
    
    <? include VIEWS .'/admin/notice.php' ?>
    <fieldset style="padding: 10px">
        <legend>Webinar Info</legend>    
        <h2>Webinar: <?= $webinar['webinars_name']; ?></h2>

        <p>
            <em>Description: </em> <br />
            <?= $webinar['webinars_description']; ?> 
        </p>
    </fieldset>
    <table class="tborder" cellpadding="6" cellspacing="1" border="0" width="100%" align="center">
        <thead>    
        <tr> 
            <td class="thead">
                ID
            </td>    
            <td class="thead">
                Name
            </td>
            <td class="thead">
                Type
            </td>
            <td class="thead">
                Entity
            </td>
            <td class="thead">
                Email
            </td>
        </tr>
        </thead>
        <? foreach($members as $member): ?>
        <tr>
            <td class="tbody">
                <?= $member['members_id']; ?>
            </td>
            <td class="tbody">
                <a href="<?= HTTPS_FULLURL; ?>/admin/members/edit.php?members_id=<?= $member['members_id']; ?>">
                    <?= $member['members_firstname'].' '.$member['members_lastname']; ?>
                </a>    
            </td>
            <td class="tbody">
                <?= $member['members_type']; ?>
            </td>
            <td  class="tbody">
                <?= $member['contact_info']['entity_name']; ?>
            </td>
            <td  class="tbody">
                <?= $member['members_email']; ?>
            </td>
        </tr>
        <? endforeach; ?>
        <tfoot>
            <tr>
            	<td colspan="5"><img src="/images/admin/table-bottom.jpg" width="960"/></td>
            </tr>
        </tfoot>        
    </table>
    
</div>
<br />

<? include(VIEWS . '/admin/footer.php'); ?>
