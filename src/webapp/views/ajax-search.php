<?php
    $db = mysqli_db::init();
    
    if ($_REQUEST['value']) {
        $term = $_REQUEST['value'] . '%';

        //todo test licensed locations
        $retVal = $db->fetch_all('SELECT licensed_locations_id,licensed_locations_name AS title FROM licensed_locations WHERE licensed_locations_name LIKE ?',array($term));
        
        echo json_encode($retVal);
    }
    else {
        if($_REQUEST['hotel_name']) {
            //todo test licensed locations
            $retVal = $db->fetch_singlet('SELECT COUNT(*) FROM licensed_locations WHERE licensed_locations_name = ?',
                                            array(trim($_REQUEST['hotel_name'])));
        }
        if($retVal) {
            echo json_encode('Already in db');
        }
        else {
            echo json_encode('Not in db');
        }
    }
?>
