<?
$sub_page = 'categories';
?>
<?php include(VIEWS . '/header.php') ?>

<div id="forum-container">
    
<div id="content" class="blog-bkg">
	<img src="/images/logo-vertical.jpg" alt="" id="hlogo" class="left" />
    <br class="clear"/>
	<?php include(VIEWS .'/articles/blognav.php'); ?>
        
	<div id="leftcol" class="spiral">  
		<img src="/images/compass-blog.jpg" alt="blog" style="margin-left:-15px"/>
    	<?php if (empty($categories)): ?>
            <div class="blog-entry">
                Sorry no categories listed.
            </div>
		<?php endif ?>

		<?php foreach($categories as $i => $category): ?>
        <? extract($category) ?>
		<div class="blog-entry">
            <div class="left" style="width:500px;position:relative">
            	<h2>
            	    <a href="/articles/categories/<?= http::encode_url($articles_categories_name) ?>/<?= $articles_categories_id ?>" class="non-underline blog-title"><?= $articles_categories_name ?></a>
        	    </h2>
                <p>
                    Blogs: <?= $total_articles ?>
                </p>
       		</div>
            <br  class="clear"/>
		</div>
		<?php endforeach ?>
            
		<?php if($pages): ?>
            Pages: <?= $pages ?>
		<?php endif ?>
   </div><!-- leftcol-->
        
	<?php include(VIEWS .'/articles/rightcol.php'); ?>
    
    <br class="clear"/>


<?php include(VIEWS .'/footer.php');?>
