<?
$sub_page = 'categories';
?>
<?php include(VIEWS . '/header.php') ?>
<div id="forum-container">
    
<div id="content" class="blog-bkg">
	<img src="/images/logo-vertical.jpg" alt="" id="hlogo" class="left" />
    <br class="clear"/>
	<?php include(VIEWS .'/articles/blognav.php'); ?>
        
	<div id="leftcol" class="spiral">  
		<img src="/images/compass-blog.jpg" alt="blog" style="margin-left:-15px"/>
		<h1><?= $category['articles_categories_name'] ?></h1>
    	<?php if (empty($articles)): ?>
            <div class="blog-entry">
                Sorry no blogs listed in this category.
            </div>
		<?php endif ?>

		<?php foreach($articles as $i=>$article): ?>
        		<div class="blog-entry">
            		<img src="/images/no-image-default.jpg" alt="" class="avatar left"/>
                    <div class="left" style="width:500px;position:relative">
                    	<h2><a href="/<?= $article['articles_page_url'] ?>" class="non-underline blog-title"> <?= $article['articles_title'] ?> </a></h2>
    					
                        <?php if($article['articles_byline']): ?>
                            <strong class="byline"><?= $article['articles_byline'] ?></strong><br />
                        <?php endif ?>
    
                        <?php if($article['articles_newsdate']): ?>
                            <span class="feature-date"><?= $article['articles_newsdate'] ?></span><br />
                         <?php endif ?>
    
                        <?= $article['articles_teaser'] ?><br />
    
                        <a href="/<?= $article['articles_page_url'] ?>" class="link">Read the full blog &raquo;</a>
                        
                        <p>
                        Viewed: <?= $article['articles_views'] ?>
                        Rating: <?= $article['articles_rating'] ?>
                        Comments: <?= $article['articles_comments'] ?>
                    </p>
           		</div>
                <br  class="clear"/>
			</div>
		<?php endforeach ?>
            
		<?php if($pages): ?>
            Pages: <?= $pages ?>
		<?php endif ?>
   </div><!-- leftcol-->
        
	<?php include(VIEWS .'/articles/rightcol.php'); ?>
    
    <br class="clear"/>
    </div>

<?php include(VIEWS .'/footer.php');?>
