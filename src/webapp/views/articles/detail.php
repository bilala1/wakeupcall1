<?php
    $page_title        = html::filter($article['page_title']) . ' < ' . html::filter($article_group['name']) . ' Blog | ' . SITE_NAME;
	$sub_page = 'blog';
    $page_keywords     = $article['page_keywords'];
    $page_description  = $article['page_description'];
?>


<?php include(VIEWS.'/header.php'); ?>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>

<div id="forum-container">
    
	<div id="content" class="blog-bkg">
		<img src="/images/logo-vertical.jpg" alt="" id="hlogo" class="left"/><br class="clear"/>	
		<?php include(VIEWS .'/articles/blognav.php'); ?>
        
		<div id="leftcol" class="spiral">  
			<img src="/images/compass-blog.jpg" alt="blog" style="margin-left:-15px"/>
            <div id="blog-body">
			    <?php if(ActiveMemberInfo::GetMemberId()): ?>
            		<?php if($watching): ?>
		            You are watching this blog | <a href="/articles/unwatch.php?articles_id=<?= $article['articles_id'] ?>">Unwatch</a><br />
		            <?php else: ?>
		            <a href="/articles/watch.php?articles_id=<?= $article['articles_id'] ?>">Watch Blog</a><br />
		            <?php endif; ?>
		        <?php endif; ?>
                <div id="rating" class="rating" title="<?= $article['articles_rating'] ?>" style="background-position:<?= articles::get_rating_bg_position($article['articles_rating'],5,124) ?>px 0">
                <?php for($i=0; $i<5; $i++): ?><img src="/images/rating-star.png" alt="rating" /><?php endfor ?>
                </div>
                <h1 class="blog-title"><?= $article['articles_title'] ?></h1>
                <strong><?= $article['articles_subtitle'] ?></strong>
    
                <?php if($article['articles_byline']): ?>
                    <p class="byline"><?= $article['articles_byline'] ?></p>
                <?php endif ?>
                <?php if($article['articles_newsdate']): ?>
                    <p class="feature-date"><?= $article['articles_newsdate'] ?></p>
                <?php endif ?>
                <?= $article['articles_body'] ?><br />
                
                <h2  id="comments">Comments</h2><br />
                <?php if(empty($article_comments)): ?>
                    Sorry there are no comments.
                <?php endif ?>
                
                <?php foreach($article_comments as $i=>$article_comment): ?>
            		<div id="blog-comments">
                    	<div class="authorinfo left" style="border:none">
							<a href="" class="username"><?= $article_comment['articles_comments_author'] ?></a><br />
                            <img src="/images/no-image-default.jpg" align="on-image"/>
                  		</div>
                        <div class="comment-box left">
                        	<?= $post['articles_comments_post_date'] ?><br />
                            <h3 class="lg"><?= $article_comment['articles_comments_title'] ?></h3>
                            <p><?= $article_comment['articles_comments_body'] ?></p>
                        </div><br class="clear"/>
             		</div>
            	<?php endforeach ?>
            
            <?php if($pages !== false): ?>
                <div style="text-align: right"><strong>Comment Pages:</strong> <?= $pages ?></div> 
            <?php endif ?>
            
            <?php if($comment_permission): ?>
            <h3 class="about" style="margin-top:15px;margin-bottom:10px;" id="comment-form"> Add Your Own Comment</h3>
            <form action="/<?= $article['articles_page_url'] ?>" method="post" class="comment">
                <?php include(VIEWS.'/notice.php') ?>
            	<input type="hidden" name="articles_id" value="<?= $article['articles_id'] ?>" />
         		<label>Your Name</label>
               	<input type="text" class="text" name="articles_comments_author" value="<?= html::filter($data['articles_comments_author'], true) ?>" maxlength="160" />
             
             	<label>Your Email:</label>
                <input type="text"  class="text" name="articles_comments_email" value="<?= html::filter($data['articles_comments_email'], true) ?>" maxlength="255" />
             
             	<label style="float:left">Blog Rating:</label>
                <div id="user-rating" class="rating" style="background-position:<?= articles::get_rating_bg_position(isset($data['articles_comments_author']) ? $data['articles_comments_rating'] : 5,5,84) ?>px 0">
                <?php for($i=0; $i<5; $i++): ?><img src="/images/rating-star-small.png" alt="rating" class="rating-star" /><?php endfor ?>
                </div><div class="left">&nbsp;(click to rate)</div>
                <input type="hidden" name="articles_comments_rating" value="<?= isset($data['articles_comments_author']) ? html::filter($data['articles_comments_rating'], true) : 5 ?>" />
                <br class="clear" />
             	
                <label>Comment Title:</label>
                <input type="text" class="text" name="articles_comments_title" value="<?= html::filter($data['articles_comments_title'], true) ?>" maxlength="60" /><br /><br />
         		
                <label>Comment Body:</label>
           		<textarea cols="70" rows="10" class="textarea" name="articles_comments_body" id="body"><?= html::filter($data['articles_comments_body']) ?></textarea>
      
        		<input type="submit" value="Post" class="forum_action blue-btn right" />

            </form>
            <?php endif ?>
            </div>
    	</div>
        
	<?php include(VIEWS .'/articles/rightcol.php'); ?>
    
    <br class="clear"/>
 
<script language="javascript" type="text/javascript">
var original_rating = $$('[name="articles_comments_rating"]').get('value');

$('user-rating').set('morph', {transition: 'pow:out'});

$$('.rating-star').each(function(elem,idx){
    elem.addEvents({
        'mouseenter': function(){
            set_rating(idx+1);
        },
        'mouseleave': function(){
            set_rating(original_rating);
        },
        'click': function(){
            original_rating = idx+1;
            set_rating(original_rating);
            $$('[name="articles_comments_rating"]').set('value',original_rating);
        }
    });
});

function set_rating(rating){
    $('user-rating').morph({
        'background-position': get_bg_position(rating, 5, $('user-rating').getSize().x)+'px'
    });
}

function get_bg_position(user_rating, max_rating, max_size){
    return ((max_size - ((user_rating / max_rating) * max_size)) * -1);
}

    tinyMCE.init({
        menubar: "",
        toolbar: "bold italic underline strikethrough | lignleft aligncenter alignright alignjustify | styleselect formatselect | bullist numlist | outdent indent | undo redo | link unlink image | removeformat | subscript superscript | code",
        toolbar_items_size: 'small',
        selector: "#body",
        width: "500px",
        plugins: "link image textcolor code",
        target_list: false,
        default_link_target: "_blank",
        link_title: false
    });
</script>

<?php include(VIEWS.'/footer.php'); ?>
