<div class="subbox recentnews">
	<img src="images/recent-news-title.jpg" />
    <ul>
        <?php foreach(articles::get_recent() as $recent_article): ?>
        <li>
            <a href="<?= $recent_article['articles_page_url'] ?>"><?= $recent_article['articles_title'] ?></a>
        </li>
        <?php endforeach ?>
    </ul>
    <div id="rss">
        <a href="/articles/rss.php" target="_new">Subscribe to Rss</a>
    </div>
</div>
