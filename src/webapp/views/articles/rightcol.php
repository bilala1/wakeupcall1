<div id="rightcol">
    <h3>Categories</h3>
    <div id="catgories" class="right-block list">
        <ul>
        <?php foreach(article_categories::_search(array('has_posts' => 1)) as $category): ?>
            <?php extract($category) ?>
            <li><a href="/articles/categories/<?= http::encode_url($articles_categories_name) . '/' . $articles_categories_id ?>/"><?= $articles_categories_name ?></a></li>
        <?php endforeach ?>
        </ul>
    </div>
    
    <h3>High Rated Blogs</h3>
    <div id="high-rated-articles" class="right-block list">
        <ul>
            <?php foreach(articles::get_top_articles(3) as $article): ?>
            <li><a href="/<?= $article['articles_page_url'] ?>"><?= $article['articles_title'] ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    
    <h3>Top Contributors</h3>
    <div id="top-contributors" class="right-block">
        <ul>
            <?php foreach(articles::get_top_contributors(3) as $member): ?>
            <li><a href="/members/view-profile.php?members_id=<?= $member['members_id'] ?>"><?= $member['members_username'] ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    
    <h3>Tag Cloud</h3>
    <div class="right-block tagcloud">
        <ul>
        <?php $tags = tags::get_tags('articles') ?>
        <?php foreach($tags as $tag): ?>
            <?php extract($tag) ?>
            <li><a href="/articles/tags/<?= http::encode_url($global_tags_name) . '/' . $global_tags_id ?>/" style="font-size:<?= tags::get_size($global_tags_articles, tags::get_count('articles', $tags), 8, 20) ?>px"><?= str_replace(' ','&nbsp;',$global_tags_name) ?></a></li>
        <?php endforeach ?>
        <ul>
    </div>
    
    <div class="join">
        <strong class="red">Join Here</strong><br />
        <a href="/register.php">Member Benefits</a>
    </div>
    <h4 align="center">Sponsors:</h4>
    <img src="/images/blog-ads.jpg" alt="ads"/>
</div>
