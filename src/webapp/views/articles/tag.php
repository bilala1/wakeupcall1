<?
$sub_page = 'tags';
?>
<?php include(VIEWS . '/header.php') ?>
<div id="forum-container">
    
<div id="content" class="blog-bkg">
	<img src="/images/logo-vertical.jpg" alt="" id="hlogo" class="left" />
    <br class="clear"/>
	<?php include(VIEWS .'/articles/blognav.php'); ?>
        
	<div id="leftcol" class="spiral">  
		<img src="/images/compass-blog.jpg" alt="blog" style="margin-left:-15px"/>
        <h1>Tag: <?= $tag['global_tags_name'] ?></h1>
        <h2><?= $tag['global_tags_name'] ?></h2>
        
        <table>
            <tr>
                <td>Subject</td>
                <td>Date</td>
            </tr>
            <? foreach($articles as $article): ?>
            <? extract($article) ?>
            <tr>
                <td>
                    <a href="/<?= $articles_page_url ?>"><?= $articles_title ?></a> 
                    <? if($join_articles_categories_id): ?>
                        in 
                        <a href="/<?= $join_articles_categories_id ?>">
                            <?= $articles_category ?>
                        </a>
                    <? endif ?>
                </td>
                <td>
                </td>
            </tr>
            <? endforeach ?>
        </table>
    
    </div><!-- leftcol-->
        
	<?php include(VIEWS .'/articles/rightcol.php'); ?>
    
    <br class="clear"/>
    </div>

<? include VIEWS.'/footer.php' ?>
