<?
$sub_page = 'tags';
?>
<?php include(VIEWS . '/header.php') ?>

<div id="forum-container">
    
<div id="content" class="blog-bkg">
	<img src="/images/logo-vertical.jpg" alt="" id="hlogo" class="left" />
    <br class="clear"/>
	<?php include(VIEWS .'/articles/blognav.php'); ?>
        
	<div id="leftcol" class="spiral">  
		<img src="/images/compass-blog.jpg" alt="blog" style="margin-left:-15px"/>
		<h1 class="blue">Blog Tags</h1>

        <ul style="padding-left:30px;">
            <? foreach($tags as $tag): ?>
            <? extract($tag) ?>
            <li>
                <a href="/articles/tags/<?= $global_tags_name ?>/<?= $global_tags_id ?>"><?= $global_tags_name ?></a> 
            </li>
            <? endforeach ?>
        </ul>
    
    </div><!-- leftcol-->
        
	<?php include(VIEWS .'/articles/rightcol.php'); ?>
    
    <br class="clear"/>


<? include VIEWS.'/footer.php' ?>
