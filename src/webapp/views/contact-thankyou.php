<?  include VIEWS. '/header.php'?>

<div id="content">
    <img src="/images/header-contact.jpg" alt="" id="top-image" />
    <div class="w260 left" style="margin-right:40px;">
    	<img src="/images/contact.jpg" />
    </div>
    
    <div class="w400 left">
        <h2 class="headline">Thank You!</h2>
        
        <p>We appreciate your comments and inquiries. We will respond to your message shortly.</p>
        
        <p>Thank you,</p>
        <h3>WAKEUP CALL Customer Service</h3>
    </div>
    <br class="clear" />
</div>

<? include  VIEWS. '/footer.php'; ?>