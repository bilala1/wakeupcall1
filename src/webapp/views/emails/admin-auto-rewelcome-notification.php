<? include(VIEWS . '/emails/header.php'); ?>
<p>Membership Renewed For: <?= $bill['members_firstname'].' '.$bill['members_lastname'] ?></p>
<p>Account Name: <?= $bill['accounts_name']?></p>
<p>Date: <?= date("Y-m-d") ?></p>
<p>Billing Type: <?= $bill['members_billing_type']?></p>
<p>Member Type: <?= $bill['accounts_type'] ?></p>


<?if($locations)
{
    echo '<p>Was Charged For Locations:</p><ul>';

    $freeOne = false;
    foreach($member_locations as $location)
    {
        $name = $location['licensed_locations_name'];
        if(!$freeOne && $location['corporate_locations_id']) {
            $name .= ' (Not charged - free corporate office)';
            $freeOne = true;
        }
        echo '<li>' . $name . '</li>';
    }
    echo '</ul>';
}
?>

<p>A total of: <?= money_format('%n', $bill['bills_amount']) ?></p>


<? include(VIEWS . '/emails/footer.php'); ?>
