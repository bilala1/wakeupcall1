<?php include(VIEWS . '/emails/header.php'); ?>

    <p>
        The following member has deleted a member from their account:
    <blockquote>
        ID: <strong><?php echo $current_member['members_id'] ?></strong><br />
        Email: <strong><?php echo $current_member['members_email'] ?></strong><br />
        Account: <string><?php echo $account['accounts_name']?></string><br />

        Deleted Member ID: <strong><?php echo $delete_member['members_id'] ?></strong><br />
        Deleted Member Email: <strong><?php echo $delete_member['members_id'] ?></strong><br />
    </blockquote>
    <br />
    </p>


    The <?= SITE_NAME ?> Support Team<br />

<?php include(VIEWS . '/emails/footer.php'); ?>