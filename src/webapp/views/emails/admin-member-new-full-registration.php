<?php include(VIEWS . '/emails/header.php'); ?>

<p>
    The following member has created a new full membership account:
    <blockquote>
        ID: <strong><?php echo $member_id ?></strong><br />
        Name: <strong><?php echo $location['members_fullname'] ?></strong><br />
        Title: <strong><?php echo $location['members_title'] ?></strong><br />
        Entity: <strong><?php echo $location['entity_name'] ?></strong><br />
        Email: <strong><?php echo $location['members_email'] ?></strong><br />
    </blockquote>
    <br />
</p>


The <?= SITE_NAME ?> Support Team<br />

<?php include(VIEWS . '/emails/footer.php'); ?>