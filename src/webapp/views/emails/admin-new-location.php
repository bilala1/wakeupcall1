<?php include(VIEWS . '/emails/header.php'); ?>

    <p>
        The following member has created a licensed location:
    <blockquote>
        ID: <strong><?php echo $current_member['members_id'] ?></strong><br />
        Email: <strong><?php echo $current_member['members_email'] ?></strong><br />
        Account: <string><?php echo $account['accounts_name']?></string><br /> 
        Location Name: <strong><?php echo $data['licensed_locations_name'] ?></strong><br />
    </blockquote>
    <br />
    </p>


    The <?= SITE_NAME ?> Support Team<br />

<?php include(VIEWS . '/emails/footer.php'); ?>