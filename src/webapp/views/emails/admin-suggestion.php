<?php include(VIEWS . '/emails/header.php'); ?>

A member has left a suggestion:<br />
<br />
<strong>Member:</strong> <?= $member['members_firstname'].' '.$member['members_lastname'] ?><br />
<strong>Type:</strong> <?= $data['suggestion_type'] ?><br />
<strong>Suggestion:</strong> <?= $data['suggestion'] ?><br />
<br />

<?php include(VIEWS . '/emails/footer.php'); ?>