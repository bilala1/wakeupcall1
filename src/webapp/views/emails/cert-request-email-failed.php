<?php include(VIEWS . '/emails/header.php'); ?>
<p>
    Dear <strong class="darkblue"><?= $members_firstname ?></strong>,<br/>
</p>
<p>

    An error occurred when attempting to email a vendor for a certification request to the following:
<ul>
    <li>Name: <?= $failed_name ?></li>
    <li>Email Address: <?= $failed_email ?></li>
</ul>
<p>
    Please click <a href="<?= FULLURL . '/members/vendors/edit.php?vendors_id=' . $vendors_id ?>">here to view and
        correct the vendor.</a>
</p>

<p>
    You can <a href="<?= FULLURL . '/members/my-documents/certificates/edit.php?certificates_id=' . $certificates_id ?>">click
        here to view the certificate.</a>
</p>
Please check the email address and correct the information.
If you need any additional help please contact WAKEUP CALL Support at support@wakeupcall.net.

<?php include(VIEWS . '/emails/footer.php'); ?>
