
<?
if ( !isset($certRequestModel)) {
    error_log("File: cert-request--model is not set--cannot continue.");
    return;
}

if ( !array_key_exists('vendors_name',$certRequestModel)) {
    error_log("File: cert-request--vendors_name doesn't exist in model.");
    return;
}

if ( !array_key_exists('vendors_street',$certRequestModel)) {
    error_log("File: cert-request--vendors_street doesn't exist in model.");
    return;
}

if ( !array_key_exists('loc_name',$certRequestModel)) {
    error_log("File: cert-request--loc_name doesn't exist in model.");
    return;
}

if ( !array_key_exists('certificates_coverages',$certRequestModel)) {
    error_log("File: cert-request--certificates_coverages doesn't exist in model.");
    return;
}

// $certificates_coverage_other is optional

if ( !array_key_exists('members_fax',$certRequestModel)) {
    error_log("File: cert-request--members_fax doesn't exist in model.");
    return;
}

if ( !array_key_exists('members_billing_addr1',$certRequestModel)) {
    error_log("File: cert-request--members_billing_addr1 doesn't exist in model.");
    return;
}
if ( !array_key_exists('members_billing_city',$certRequestModel)) {
    error_log("File: cert-request--members_billing_city doesn't exist in model.");
    return;
}
if ( !array_key_exists('members_billing_state',$certRequestModel)) {
    error_log("File: cert-request--members_billing_state doesn't exist in model.");
    return;
}
if ( !array_key_exists('members_billing_zip',$certRequestModel)) {
    error_log("File: cert-request--members_billing_zip doesn't exist in model.");
    return;
}


?>

<p><?= $certRequestModel['vendors_name'] ?></p>

<p><?= $certRequestModel['vendors_street'] ?></p>
<br/>

To Whom It May Concern,<br/><br/>

<p><?= $certRequestModel['loc_name'] ?> requires all of our business associates to provide certificates of
    insurance to protect us and you in our business together. You are required to provide
    a current certificate of insurance for the following lines of coverage:</p>

<ul>
    <? if (stristr($certRequestModel['certificates_coverages'], 'General Liability (GL)')): ?>
        <li>General Liability (naming <?= $certRequestModel['loc_name'] ?> as additional insured)</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Umbrella/Excess Liability (UMB)')): ?>
        <li>Umbrella/Excess Liability (UMB)</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Worker\'s Compensation (Comp)')): ?>
        <li>Worker's Compensation</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Automotive (Auto)')): ?>
        <li>Auto (naming <?= $certRequestModel['loc_name'] ?> as additional insured)</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Crime (CR)')): ?>
        <li>Crime (CR)</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Property (Prop)')): ?>
        <li>Property</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Professional')): ?>
        <li>Professional / E&O (P/E&O)</li>
    <? endif; ?>
    <? if (stristr($certRequestModel['certificates_coverages'], 'Builder\'s Risk (BR)')): ?>
        <li>Builder's Risk (BR)</li>
    <? endif; ?>
    <? if ($certRequestModel['certificates_coverage_other']): ?>
        <li><?= $certRequestModel['certificates_coverage_other'] ?></li>
    <? endif; ?>

</ul>

<p>Please submit the required certificate(s) as soon as possible by:</p>
<ul>
    <? if ($certRequestModel['members_fax']) { ?>
        <li>Fax to: <?= $certRequestModel['members_fax'] ?></li>
    <? } ?>
    <li>Mail to:<br/>
        <?= $certRequestModel['loc_name'] ?><br/>
        <?= $certRequestModel['members_billing_addr1'] ? $certRequestModel['members_billing_addr1'] . '<br />' : '' ?>
        <?= $certRequestModel['members_billing_city'] . ', ' . $certRequestModel['members_billing_state'] . ' ' . $certRequestModel['members_billing_zip'] ?><br/>
    </li>
</ul>

<p>If you have any questions, please contact us.</p>

<p>Sincerely,</p>
<p>Management</p>
<p><?= $certRequestModel['loc_name'] ?></p>
