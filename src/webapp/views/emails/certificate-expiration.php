<? include(VIEWS . '/emails/header.php'); ?>

<?php foreach ($expiredCertificates as $index => $certificate) { ?>
    <?php if ($index > 0) { ?>
        <hr>
    <?php } ?>
    <?php if($certificate['days'] == 0) { ?>
        A certificate expired today:<br/>
        <?php } else if($certificate['days'] == -1) { ?>
        A certificate expired yesterday:<br/>
        <?php } else { ?>
        A certificate has been expired for <?= $certificate['days'] * -1 ?> days:<br/>
    <?php } ?>
    <br/>
    <strong>Vendor Name:</strong> <?= $certificate['vendors_name'] ?><br/>

    <strong>Location Name:</strong> <?= $certificate['licensed_locations_name'] ?><brgit git add/>
    <strong>Certificate Expired:</strong> <?= date(DATE_FORMAT, strtotime($certificate['certificates_expire'])) ?><br/>
    <strong>Type of Coverage:</strong> <br/>
    <ul>
        <?php if ($certificate['certificates_coverages']) { ?>
            <? $coverages = explode(',', $certificate['certificates_coverages']); ?>
            <? foreach ($coverages as $coverage): ?>
                <li><?= $coverage; ?></li>
            <? endforeach; ?>
        <?php } ?>
        <?php if ($certificate['certificates_coverage_other']) { ?>
            <li><?= $certificate['certificates_coverage_other'] ?></li>
        <?php } ?>
    </ul>
    <br/>
    <? if ($certificate['certificates_remind_vendor_expired'] == 1 && $certificate['vendors_email']): ?>
        A request has been emailed to your vendor for an updated certificate.
    <? endif; ?>
    <? if($member['members_id'] > -1) { ?>
        <a href="<?= FULLURL . '/members/my-documents/certificates/edit.php?certificates_id=' . $certificate['certificates_id'] ?>">View
            or update this certificate of insurance in <?= SITE_NAME; ?></a>
    <? } ?>
    <br/>
    <br/>
<?php } ?>


<?php foreach ($expiringCertificates as $index => $certificate) { ?>
    <?php if ($index > 0 || !empty($expiredCertificates)) { ?>
        <hr>
    <?php } ?>
    <?php if($certificate['days'] == 0) { ?>
        A certificate expired today:<br/>
    <?php } else if($certificate['days'] == 1) { ?>
        A certificate is expiring tomorrow:<br/>
    <?php } else { ?>
        A certificate is expiring in <?= $certificate['days'] ?> days:<br/>
    <?php } ?>
    <br/>
    <strong>Vendor Name:</strong> <?= $certificate['vendors_name'] ?><br/>

    <strong>Location Name:</strong> <?= $certificate['licensed_locations_name'] ?><br/>
    <strong>Certificate Expiring:</strong> <?= date(DATE_FORMAT, strtotime($certificate['certificates_expire'])) ?><br/>
    <strong>Type of Coverage:</strong> <br/>
    <ul>
        <?php if ($certificate['certificates_coverages']) { ?>
            <? $coverages = explode(',', $certificate['certificates_coverages']); ?>
            <? foreach ($coverages as $coverage): ?>
                <li><?= $coverage; ?></li>
            <? endforeach; ?>
        <?php } ?>
        <?php if ($certificate['certificates_coverage_other']) { ?>
            <li><?= $certificate['certificates_coverage_other'] ?></li>
        <?php } ?>
    </ul>
    <br/>
    <? if ($certificate['certificates_remind_vendor_expired'] == 1 && $certificate['vendors_email']): ?>
        A request has been emailed to your vendor for an updated certificate.
    <? endif; ?>
    <? if($member['members_id'] > -1) { ?>
        <a href="<?= FULLURL . '/members/my-documents/certificates/edit.php?certificates_id=' . $certificate['certificates_id'] ?>">View
            or update this certificate of insurance in <?= SITE_NAME; ?></a>
    <? } ?>
    <br/>
    <br/>
<?php } ?>

<? include(VIEWS . '/emails/footer.php'); ?>