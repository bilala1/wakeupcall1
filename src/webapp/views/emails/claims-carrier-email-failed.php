<?php include(VIEWS . '/emails/header.php'); ?>
<p>
   Dear <strong class="darkblue"><?= $members_firstname?></strong>,<br />
  </p>
<p>

    An error occurred when attempting to email a claim to the following:
    <ul>
        <li>Name: <?= $failed_name?></li>
        <li>Email Address: <?= $failed_email?></li>
    </ul>
<p>
    Please click <a href="<?= FULLURL.'/members/claims/carriers/edit.php?carriers_id='.$carrier_id?>">here to view and correct the carrier/recipient.</a>
</p>

    <p>
        You can <a href="<?= FULLURL.'/members/claims/edit.php?claims_id='.$claim_id?>">click here to view the claim.</a>
    </p>
    Please check the email address and correct the information.
    If you need any additional help please contact WAKEUP CALL Support at support@wakeupcall.net.

<?php include(VIEWS . '/emails/footer.php'); ?>
