<? include(VIEWS . '/emails/header.php'); ?>

<? if ($location['members_lastname'] || $location['members_lastname']) { ?>
    Dear <?= $location['members_firstname'] ?> <?= $location['members_lastname'] ?>,<br><br>
<? } else { ?>
    To whom it may concern,<br><br>
<? } ?>

The following contract is due to expire in <?= $db4 ?> days:<br><br>

Contracted Party: <?= $contract['contracts_contracted_party_name'] ?><br>
<? if (count($licensed_locations) > 0) {
    $first = true;
    ?>
    Contracted Properties:
    <? foreach ($licensed_locations as $licensed_location){
        if ($first) {
            $first = false;
        } else {
            echo ', ';
        }
        echo $licensed_location['licensed_locations_name'];
    } ?>
    <br>
<? } else { ?>
    Contracted Properties: All Locations<br>
<? } ?>
Due to Expire: <?= date('n/j/Y', strtotime($contract['contracts_expiration_date'])) ?><br>
<? if ($contract['contracts_description']) { ?>
    General Description: <?= $contract['contracts_description'] ?><br>
<? } ?>
<br>

<a href="<?= FULLURL . '/members/contracts/edit.php?contracts_id='.$contract['contracts_id']?>">View or update this contract in <?= SITE_NAME ?></a> <br><br>

Thank you for using WAKEUP CALL!<br><br>

<? include(VIEWS . '/emails/footer.php'); ?>
