<? include(VIEWS . '/emails/header.php'); ?>

<? if ($emailRecipient['members_lastname'] || $emailRecipient['members_lastname']) { ?>
    Dear <?= $emailRecipient['members_firstname'] ?> <?= $emailRecipient['members_lastname'] ?>,<br><br>
<? } else { ?>
    To whom it may concern,<br><br>
<? } ?>
    <?=$contracts_milestone['contracts_milestones_description']?> milestone date is <?= $num_days == 0 ? 'today' : ($num_days . ' away') ?>.<br><br>

    <a href="<?= FULLURL . '/members/contracts/edit.php?contracts_id='.$contracts_milestone['join_contracts_id']?>">View or update this contract in <?= SITE_NAME ?></a> <br><br>

    Thank you for using WAKEUP CALL!<br><br>

<? include(VIEWS . '/emails/footer.php'); ?>