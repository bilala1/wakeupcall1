<?php include(VIEWS . '/emails/header.php'); ?>
<p>Hello,</p>
<p>Member <?= $member['members_fullname'] ?> (<?= $member['members_email'] ?>) has requested a new definition for:</p>

<p style="font-weight:bold"><?= strtoupper($term) ?></p>  

<p>Please click <a href="<?= FULLURL . '/admin/faqs/terms/edit.php?definitions_id=' . $def_id ?>">here</a> to set the definition.</p>

<?php include(VIEWS . '/emails/footer.php'); ?>
