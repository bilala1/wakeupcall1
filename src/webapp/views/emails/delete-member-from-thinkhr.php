<? include(VIEWS . '/emails/header.php'); ?>

This is an automated message from <?= SITE_NAME ?><br/>

The following user's account has been locked out - please disable their account:<br/><br/>
    Member: <?= $member['members_firstname'].' '.$member['members_lastname'] ?><br />
    Email: <?= $member['members_email'] ?><br/><br/>

Thank you!

<? include(VIEWS . '/emails/footer.php'); ?>