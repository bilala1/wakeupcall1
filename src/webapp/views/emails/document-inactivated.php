<? include(VIEWS . '/emails/header.php'); ?>

    <p>The following document has been made inactive, and is no longer part of the WAKEUP CALL Document Library:</p>

    <p><?= $documents_title ?></p>

    <p>Our records show that you have added this document to your My Files area. Our inactivation of this document does NOT remove it from your 'My Files' area.</p>

    <p>However, please be aware that if you decide to delete this document from your My Files area, it will no longer be available in the main Document Library on WAKEUP CALL.</p>

    <p>If you have any questions regarding this, please contact us at concierge@wakeupcall.net.</p>

<? include(VIEWS . '/emails/footer.php'); ?>