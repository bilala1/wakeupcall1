<?php include(VIEWS . '/emails/header.php'); ?>
<p>
   Dear <strong class="darkblue">WilsonElser Attorney</strong>,<br />
  </p>
<p>

    A member from <?= SITE_NAME ?>  has contacted you from our Employment Law Email Hotline.<br /><br />

	Member Information:<br /><br />
	<b>Member Name:</b> <?= $data['members_fullname'] ?><br />
    <b>Title:</b> <?= $data['members_title'] ?><br />
	<b>Location/Business Name:</b> <?= $data['entity_name'] ?><br />
	<b>Address:</b> <?= $data['address'] ?>,
		<?= $data['city'] ?>,
		<?= $data['state'] ?>,
		<?= $data['zip'] ?><br />
	<b>Member E-mail:</b> <?= $data['members_email'] ?><br />
    <b>Names of Parties Involved:</b> <?= $data['parties_involved'] ?>
    <br /><br />

	Member Inquiry:<br /><br />

	<b><?= htmlentities($data['question_content']); ?></b><br /><br />
</p>
Please contact the member directly. <br />

Thank you, <br /><br />

<?php include(VIEWS . '/emails/footer.php'); ?>
