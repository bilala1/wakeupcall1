<? include(VIEWS . '/emails/header.php'); ?>

    <p>
        <? if ($emailRecipient['members_lastname'] || $emailRecipient['members_lastname']) { ?>
            Dear <?= $emailRecipient['members_firstname'] ?> <?= $emailRecipient['members_lastname'] ?>,
        <? } else { ?>
            To whom it may concern,
        <? } ?>
    </p>
    <p>
        The following milestone is due <?= $remind_days == 0 ? 'today' : "in $remind_days days" ?> for
        <?= $entities_items_milestone['entities_name'] ?>.
    </p>

<ul>
    <li><?= $entities_items_milestone['entities_items_milestones_description'] ?> expires on <?= Formatting::date($entities_items_milestone['entities_items_milestones_date']) ?></li>
</ul>

    <a href="<?= FULLURL . '/members/entities/items/edit.php?entities_id=' . $entities_items_milestone['join_entities_items_id'] . '&entities_items_id=' . $entities_items_milestone['join_entities_items_id'] ?>">View
        or update this item in <?= SITE_NAME ?></a> <br><br>

    Thank you for using <?= SITE_NAME ?>!<br><br>

<? include(VIEWS . '/emails/footer.php'); ?>