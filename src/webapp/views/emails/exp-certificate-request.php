<? include(VIEWS . '/emails/header.php'); ?>

<h3 style="text-align:center">Expired Certificate Request</h3>

<?= $vendors_name ?><br /><br />
<?= $vendors_street ?><br /><br />

To Whom It May Concern,<br /><br />

<p>Our records indicate that your insurance coverage expired at least 15 days ago.  In order to continue to do business with <?= $loc_name ?> we
require that you submit (a) current certificate(s) for the following coverage within the next 15 days:</p>
    <ul>
    <? if(stristr($certificates_coverages,'General Liability (GL)')): ?>
        <li>General Liability (naming <?= $loc_name ?> as additional insured)</li>
    <? endif; ?>
    <? if(stristr($certificates_coverages,'Excess Liability (EL)')): ?>
        <li>Excess Liability (naming <?= $loc_name ?> as additional insured)</li>
    <? endif; ?>
    <? if(stristr($certificates_coverages,'Automotive (Auto)')): ?>
        <li>Auto (naming <?= $loc_name ?> as additional insured)</li>
    <? endif; ?>
    <? if(stristr($certificates_coverages,'Worker\'s Comensation (Comp)')): ?>
        <li>Worker's Compensation</li>
    <? endif; ?>
    <? if(stristr($certificates_coverages,'Property (Prop)')): ?>
        <li>Property</li>
    <? endif; ?>
    </ul>

<p>Please submit the required certificate(s) by</p>
<ul>
    <? if($members_fax != ''): ?>
    <li>Faxing it to <?= $members_fax ?>, or</li>
    <? endif; ?>
    <li>Sending it to:<br />
        <?= $members_billing_addr1 ?><br>
        <?= ($members_billing_addr2 != '') ? $members_billing_addr2 . '<br>' : '' ?>
        <?= $members_billing_city . ', ' . $members_billing_state . ' ' . $members_billing_zip ?>
    </li>
</ul>
<p>If you have any questions, please contact us.</p>
<br />
Sincerely,<br /><br />
Management<br /><br />
<?= $loc_name ?>

</td>
                <td width="20">&nbsp;</td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3" style="text-align:center;background:#eee; padding:15px; font-size:10px;color:#666">Copyright &copy; 2011 WAKEUP CALL</td></tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
