<?php include(VIEWS . '/emails/header.php'); ?>

<p>
    A new FAQ question was submitted:<br />
    <blockquote>
        Email: <strong><?php echo $member['members_email'] ?></strong><br />
        Name: <strong><?php echo $member['members_fullname'] ?></strong><br />
        Organization: <strong><?php echo $member['entity_name'] ?></strong><br />
        Question: <strong><?php echo $data['faqs_question'] ?></strong><br />
    </blockquote><br />

    To manage FAQ questions, please go to:<a href="<?php echo FULLURL ?>/admin/faqs/list.php" style="color:#005DAA" target="_blank"><?= FULLURL ?>/admin/faqs/list.php</a>
    <br /><br />

    The <?= SITE_NAME ?> Support Team<br />
</p>
<?php include(VIEWS . '/emails/footer.php'); ?>
