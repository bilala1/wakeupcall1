<?php include(VIEWS . '/emails/header.php'); ?>

<p>Dear <?= $members_firstname ?>,</p>

<p>The fax to <?= $carriers_name ?> was unsuccessful. Please verify that the fax number in this Carrier’s profile
    on WAKEUP CALL is correct, then resubmit the claim.</p>

<p>If you have questions regarding this notice, or if you continue to experience issues faxing to this Carrier, please
    contact the WAKEUP CALL Concierge for assistance at <a
        href="mailto:<?= CONCIERGE_EMAIL ?>"><?= CONCIERGE_EMAIL ?></a>.</p>

<p>Please see the fax transmission details below:</p>

<p style="text-decoration: underline; font-weight:bold">Transmission Results</p>
<table>
    <tr>
        <td>Destination Fax</td>
        <td><?= $carriers_fax ?></td>
    </tr>
    <tr>
        <td>Contact Name</td>
        <td><?= $carriers_contact ?></td>
    </tr>
    <tr>
        <td>Time</td>
        <td><?= $claims_faxes_submitted_datetime ?></td>
    </tr>
    <tr>
        <td>Transmission Result</td>
        <td><?= $fax_item->Status ?> - <?= $fax_item->Message ?></td>
    </tr>
    <tr>
        <td>Pages sent</td>
        <td>0</td>
    </tr>
    <tr>
        <td>Subject</td>
        <td>Claim Submission</td>
    </tr>
    <tr>
        <td>Claimant</td>
        <td><?= $carriers_fax ?></td>
    </tr>
</table>
<br><br>
<a href="<?= FULLURL . '/members/claims/edit.php?claims_id='.$claims_id?>">View or update this claim in WAKEUP CALL</a> <br><br>
<?php include(VIEWS . '/emails/footer.php'); ?>
