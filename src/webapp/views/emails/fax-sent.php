<?php include(VIEWS . '/emails/header.php'); ?>

<p>Dear <?= $members_firstname ?>,</p>

<p>The fax to <?= $carriers_name ?> was successfully sent. Please see the fax transmission details below:</p>

<p style="text-decoration: underline; font-weight:bold">Transmission Results</p>
<table>
    <tr>
        <td>Destination Fax</td>
        <td><?= $carriers_fax ?></td>
    </tr>
    <tr>
        <td>Contact Name</td>
        <td><?= $carriers_contact ?></td>
    </tr>
    <tr>
        <td>Time</td>
        <td><?= $claims_faxes_submitted_datetime ?></td>
    </tr>
    <tr>
        <td>Transmission Result</td>
        <td>Sent</td>
    </tr>
    <tr>
        <td>Pages sent</td>
        <td><?= $fax_item->PagesSent ?></td>
    </tr>
    <tr>
        <td>Subject</td>
        <td>Claim Submission</td>
    </tr>
    <tr>
        <td>Claimant</td>
        <td><?= $carriers_fax ?></td>
    </tr>
</table>
<br><br>
<a href="<?= FULLURL . '/members/claims/edit.php?claims_id='.$claims_id?>">View or update this claim in WAKEUP CALL</a> <br><br>
<?php include(VIEWS . '/emails/footer.php'); ?>
