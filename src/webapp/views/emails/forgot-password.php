<?php include(VIEWS . '/emails/header.php'); ?>
<p>
   Dear <strong class="darkblue"><?php echo $member['members_firstname'] ?></strong>,<br />
  </p>
<p>
    As requested, we have issued your account a new password:<br />
    <blockquote>
        Email: <strong><?php echo $member['members_email'] ?></strong><br />
        Password: <strong><?php echo $newpass ?></strong><br />
    </blockquote><br />

    To access your account, go to <a href="<?php echo FULLURL ?>" style="color:#005DAA" target="_blank"><?= FULLURL ?></a>
    and login using your new password.  To update your password at any time, go
    to "My WAKEUP CALL" and change it in the "Account" section.<br /><br />

    The <?= SITE_NAME ?> Support Team<br />
</p>
<?php include(VIEWS . '/emails/footer.php'); ?>
