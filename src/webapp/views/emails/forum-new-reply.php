<? include(VIEWS . '/emails/header.php'); ?>

Dear %recipient.members_firstname%,<br><br>

There's a new reply to the topic <strong><?= $topic['topics_title'] ?></strong> that you are watching.<br>
Please click the link below, or copy and paste into your browser to see the reply.<br><br>

<a href="<?= HTTP_FULLURL ?>/members/forums/view-topic.php?topics_id=<?= $topics_id ?>" style="color:#005DAA"><?= HTTP_FULLURL ?>/members/forums/view-topic.php?topics_id=<?= $topics_id ?></a><br><br>

<? /*
<p>If you no longer wish to receive updates regarding WAKEUP CALL Forums, please <a href="<?= HTTP_FULLURL ?>">log in</a> to your WAKEUP CALL account and update your account preferences.</p>
*/ ?>
<p>
If you no longer wish to receive updates regarding this topic, please <a href="<?= HTTP_FULLURL ?>/members/forums/view-topic.php?topics_id=<?= $topics_id ?>">click here</a> to visit the topic, and click the Unwatch Topic link. 
</p>
<? include(VIEWS . '/emails/footer.php'); ?>