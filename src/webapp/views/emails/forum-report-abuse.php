<? include(VIEWS . '/emails/header.php'); ?>

Dear Admin/Monitor,<br><br>

<p>A member has reported abuse in the forums.  You can take action by clicking
<a href="<?= FULLURL ;?>/admin/forums/view-post.php?posts_id=<?= $data['posts_id']; ?>">here</a>. <br />

    <blockquote>
        <strong>A member who reported abuse:</strong> <?php echo $member['members_firstname'].' '.$member['members_lastname']; ?><br />
        <strong>Topic: </strong> <?php echo $post['topics_title']; ?> <br />
        <strong>Post: </strong> <?php echo $post['posts_body'] ?><br />
        <strong>Reason given: </strong> <?php echo $data['report_desc'] ?><br />
    </blockquote>

</p>


<? include(VIEWS . '/emails/footer.php'); ?>