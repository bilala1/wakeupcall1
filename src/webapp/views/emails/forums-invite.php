<?php include(VIEWS . '/emails/header.php'); ?>
<h1 style="color:#005daa;margin:10px 0; font-weight: normal">You've been invited to the forum!</h1>

Hello, <?= $_SESSION['global_members_username'] ?> has invited you to the <?= SITE_NAME ?> forums.<br />

<blockquote>
    Message: <br />
    <?= $_POST['message'] ?>
</blockquote>

<br />

<a href="<?= FULLURL ?>/forums/index.php" target="_blank" style="color:#005DAA">Click here</a> to visit the forums.<br />
<br />
<?php include(VIEWS . '/emails/footer.php'); ?>
