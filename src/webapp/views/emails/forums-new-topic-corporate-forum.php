<?php include(VIEWS . '/emails/header.php'); ?>
<p>
    Dear <strong class="darkblue">%recipient.members_firstname%</strong>,<br />

    A new topic has been posted on the <?= $data['corporations_name'] ?> Forum:
    </p>
    <b><?= html::filter($data['topics_title']) ?></b>
    <p><?= $data['topics_body'] ?></p>

Please click the link below, or copy and paste into your browser to see the reply.<br><br>

<a href="<?= HTTP_FULLURL ?>/members/forums/view-topic.php?topics_id=<?= $topics_id ?>" style="color:#005DAA"><?= HTTP_FULLURL ?>/members/forums/view-topic.php?topics_id=<?= $topics_id ?></a><br><br>

<?php include(VIEWS . '/emails/footer.php'); ?>
