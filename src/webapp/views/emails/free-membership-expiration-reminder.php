<?php include(VIEWS . '/emails/header.php'); ?>

A free membership has expired:<br />
<br />
Member: <?= $member['members_firstname'].' '.$member['members_lastname'] ?><br />
Please become a full member. Full membership has many benefits.

<br />

<?php include(VIEWS . '/emails/footer.php'); ?>