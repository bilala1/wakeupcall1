<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>Your trial membership ended at <?= SITE_NAME?> and we noticed that you have not yet signed up to take full advantage of our membership benefits.  We hope you had a chance to experience all the resources that <?= SITE_NAME?> members enjoy all year long, including:</p>
<ul>
    <li>HR Phone/Email service</li>
    <li>Employment Law Hotline Email</li>
    <li>100's of OSHA and Safety documents</li>
    <li>Extensive Online Training</li>
    <li>Vast SDS Library</li>
    <li>Claims Management/Trending service</li>
    <li>Certificate Tracking</li>
    <li>Message Boards</li>
    <li>Special discounts on outside services and products</li>
</ul>
<p><?= SITE_NAME?> has a lot to offer and more is on the way. We would like to have you as a regular member. Don't wait to get a hospitality-based resource like <?= SITE_NAME?> for you and your staff any longer. Log In or click the link below to join <?= SITE_NAME?> for a full year.</p>
<p><a href="<?= FULLURL ?>/full-membership.php?members_id=<?= $member['members_id'] ?>" target="_blank" style="color:#005DAA"><?= FULLURL ?>/full-membership.php?members_id=<?= $member['members_id'] ?></a></p>
<p>If you're not interested in joining, we would be grateful if you would please take a couple of minutes and share with us why.  Your input on our 2 min survey will help us improve for the rest of the hospitality industry or for you in the future.</p>
<p><a href="http://www.surveymonkey.com/s/GX2BVX9">http://www.surveymonkey.com/s/GX2BVX9</a></p>
<p>We hope you will take advantage of the many services and benefits of a <?= SITE_NAME?> membership and we look forward to having you visit us again soon!</p>
<? include(VIEWS . '/emails/footer.php'); ?>