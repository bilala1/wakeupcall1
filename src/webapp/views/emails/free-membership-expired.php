<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>It's been 15 days and we hope you enjoyed  exploring <?= SITE_NAME?>.  We hope you had a chance to test or review one or more of the following features:</p>
<ul>
    <li>HR Phone/Email service</li>
    <li>Employment Law Hotline Email</li>
    <li>100's of OSHA and Safety documents</li>
    <li>Extensive Online Training</li>
    <li>Vast SDS Library</li>
    <li>Claims Management/Trending service</li>
    <li>Certificate Tracking</li>
    <li>Posting a discussion point on our Message Boards</li>
    <li>Special discounts on outside services and products</li>
    <li>Any Additional Services that we may have arranged for you</li>
</ul>
<p><?= SITE_NAME?> has a lot to offer and more is on the way. We would like to have you as a regular member. Don't wait to get a hospitality-based resource like <?= SITE_NAME?> for you and your staff any longer. Log In or click the link below to join <?= SITE_NAME?> for a full year.</p>
<p><a style="color: #005DAA" href="<?= FULLURL ?>/full-membership.php?members_id=<?= $member['members_id'] ?>"><?= FULLURL ?>/full-membership.php?members_id=<?= $member['members_id'] ?></a></p>
<p>We look forward to having you visit us again soon.</p>
<? include(VIEWS . '/emails/footer.php'); ?>