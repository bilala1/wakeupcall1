<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>Thank you for a great year with <?= SITE_NAME?>! 
    Our records show your membership expires on <?= date(DATE_FORMAT, strtotime($member['next_bills_datetime'])) ?>.
    We will be processing your credit card payment for your entire account on <?= date(DATE_FORMAT, strtotime($member['next_bills_datetime'])) ?>, so you can continue to take advantage of our one of a kind hospitality resource for you and your staff!
    Your <?= SITE_NAME?> renewal will be charged the amount of $<?= money_format('%i', $member['bills_amount']) ?> to the card on file: <?= strings::format_cc('424242424242'. $member['last4']) ?></p>
    <table cellspacing="0" cellpadding="2">
        <?php if($member['accounts_type'] == 'multi') { ?>
            <tr>
                <td>Free Corporate Office</td>
                <td style="text-align: right; padding-left:2em;"><?= money_format('%n', 0) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td><?= $member['num_billed_locations'] ?> location<?= $member['num_billed_locations'] == 1 ? '' : 's' ?> x
                <?= money_format('%n', $member['location_cost']) ?>
            </td>
            <td style="text-align: right; padding-left:2em;"><?= money_format('%n', $member['initial_amount']) ?></td>
        </tr>
        <?php if($member['num_discounted']) { ?>
            <tr>
                <td>Discount (<?= $member['discount_percent'] ?>%)</td>
                <td style="text-align: right; padding-left:2em;">(<?= money_format('%n', $member['initial_amount'] - $member['bills_amount']) ?>)</td>
            </tr>
        <?php } ?>
        <tr>
            <td style="border-top: 1px solid black">Total Amount Due</td>
            <td style="text-align: right; padding-left:2em; border-top: 1px solid black"><?= money_format('%n', $member['bills_amount']) ?></td>
        </tr>
    </table>
<p>If you prefer to pay by company check please contact us at Membership@wakeupcall.net.
    As a member, we always want to hear what you have to say.
    Please don't forget to use the Concierge Desk to contact us for anything you might need. 
    With regard to documents, training options or additional services, please share your feedback and let us know any additions that you would like to see.</p>
<p>Sincerely,</p>
<p>WAKEUP CALL</p>
<? include(VIEWS . '/emails/footer.php'); ?>
