<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>
    Thank you for a great year with <?= SITE_NAME?>! 
    Our records show your membership expires on <?= date(DATE_FORMAT, strtotime($member['bills_datetime'])) ?>.
    We are processing your renewal, so you can continue to take advantage of our one of a kind hospitality resource for you and your staff!  
    Your <?= SITE_NAME?> renewal will be charged the amount of $<?= money_format('%i', $member['bills_amount']) ?> to the card on file:  <?= strings::format_cc('424242424242'. $member['last4']) ?>
</p>
<p>
    Remember, as a member, we always want to hear what you have to say. 
    Please don't forget to use the Concierge Desk to contact us for anything you might need. 
    With regard to documents, training options or additional services, please share your feedback and let us know any additions that you would like to see.
</p>
<p>Thanks again,</p>
<? include(VIEWS . '/emails/footer.php'); ?>
