\<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>Welcome back to <?= SITE_NAME?>!  
   We are happy you are continuing to taking advantage of our one of a kind hospitality resource for you and your staff. 
   Remember, as a member, we always want to hear what you have to say.
   Please don't forget to use the Concierge Desk to contact us for anything you might need. 
   With regard to documents, training options or additional services, please share your feedback and let us know any additions that you would like to see.
</p>

<? include(VIEWS . '/emails/footer.php'); ?>
