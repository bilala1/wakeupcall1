<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= ($hotel['members_firstname'] ? $hotel['members_firstname']: $member['members_firstname']).' '.($hotel['members_lastname'] ? $hotel['members_lastname']: $member['members_lastname']) ?>,</p>
<p>Welcome to <?= SITE_NAME?>! 
   We are happy that you are taking advantage of this one of a kind hospitality resource for you and your staff.  
   Remember, as a member, we always want to hear what you have to say. 
   Please don't forget to use the Concierge Desk to contact us for anything you might need. 
   With regard to documents, training options or additional services, please share your feedback and let us know any additions that you would like to see.  
   You will receive email updates on changes and updates to the <?= SITE_NAME?> site and services. Please check your preferences for these emails the next time you log in.</p>

<? include(VIEWS . '/emails/footer.php'); ?>