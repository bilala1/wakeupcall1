<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>Your membership ended at <?= SITE_NAME?> and we noticed that you have not yet renewed.  So you don�t miss out on your full membership benefits, please renew right away!  We hope you remember all of the resources <?= SITE_NAME?> offers our members all year long, including:</p>
<ul>
    <li>HR Phone/Email service</li>
    <li>Employment Law Hotline Email</li>
    <li>100�s of OSHA and Safety documents</li>
    <li>Extensive Online Training</li>
    <li>Vast SDS Library</li>
    <li>Claims Management/Trending service</li>
    <li>Certificate Tracking</li>
    <li>Message Boards</li>
    <li>Special discounts on outside services and products</li>
</ul>
<p><?= SITE_NAME?> has a lot to offer and more is on the way. We would like to have you back as a member. Please don�t wait to get <?= SITE_NAME?> back for you and your staff.  Log In or click the link below to rejoin <?= SITE_NAME?> for another full year.</p>
<p><a href="<?= FULLURL ?>/full-membership.php" style="color:#1b98e8"><?= FULLURL ?>/full-membership.php</a></p>
<p>If you�re not interested in joining, we would be grateful if you would please take a couple of minutes and share with us why.  Your input on our 2 min survey will help us improve for the rest of the hospitality industry or for you in the future.</p>
<p>(survey link)</p>
<p>We hope you will take advantage of the many services and benefits of a <?= SITE_NAME?> membership and we look forward to having you visit us again soon!</p>

<? include(VIEWS . '/emails/footer.php'); ?>