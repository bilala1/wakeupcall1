<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>You have successfully added the following Corporate Location to the <?= $account['accounts_name'] ?> <?= SITE_NAME?> group:<br>
   Location: <?= $data['licensed_locations_name'] ?><br></p>
<p>As a reminder, this location will be automatically associated with any contracts, vendors and carriers that you have designated for ‘All Locations’.</p>
<p>Please take a minute to confirm, in the Contracts Management area, the Claims Management area and the Vendor Certificates area, that this location is associated with the appropriate contracts, carriers and vendors.</p>
<p>Please let us know if you have any questions, and thank you for using <?= SITE_NAME?>!</p>
<p>Sincerely,</p>
<? include(VIEWS . '/emails/footer.php'); ?>