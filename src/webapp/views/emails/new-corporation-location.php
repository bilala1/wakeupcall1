<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member_data['members_firstname'].' '.$member_data['members_lastname'] ?>,</p>
<p>Welcome to <?= SITE_NAME?>! You have been added to the <?= $corporation['corporations_name'] ?> <?= SITE_NAME?> group as the <? $data['licensed_locations_name'] ?> administrator.
   Please log in to review all of the services <?= SITE_NAME?> has to offer you and your staff. 
   Being part of the <?= $corporation['corporations_name'] ?> group, you will have access to the intranet forum just for your group. 
   There are tutorials for all aspects of the <?= SITE_NAME?> site to help you navigate the services the first time through.  
   Also, as a member, we always want to hear what you have to say. 
   Please don't forget to use the Concierge Desk to contact us for anything you might need. 
   With regard to documents, training options or additional services, please share your feedback and let us know any additions that you would like to see.
</p>

<p> Your user name is: <strong><?= $member_data['members_email'] ?></strong><br />
    Your Corporate assigned password is: <strong><?= $data['members_password'] ?></strong> (you can log in and change this)
</p>

<? if(isset($member_data['members_verified_code']) && $member_data['members_verified_code']): ?>
    <p>To activate your account, please click on the link below or copy and paste into your browser:</p>
    <p><a href="<?= FULLURL ?>/verify.php?code=<?= $member_data['members_verified_code'] ?>" target="_blank" style="color:#005DAA"><?= FULLURL ?>/verify.php?code=<?= $member['members_verified_code'] ?></a></p>
<? endif; ?>

<p>We look forward to having you at the site soon.</p>
<? include(VIEWS . '/emails/footer.php'); ?>