<?php include(VIEWS . '/emails/header.php'); ?>

    <p>
       The following member was added:
    <blockquote>
        Account: <strong><?php echo $account['accounts_name']?></strong><br />
        Email: <strong><?php echo $data['members_email'] ?></strong><br />
        Member Name: <strong><?php echo $data['members_firstname'] .' '.$data['members_lastname'] ?></strong><br />
    </blockquote>
    <br />
    </p>


    The <?= SITE_NAME ?> Support Team<br />

<?php include(VIEWS . '/emails/footer.php'); ?>