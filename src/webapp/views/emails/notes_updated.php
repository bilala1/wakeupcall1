<?php
$db = mysqli_db::init();
if(empty($claims_id)) $claims_id = $data['claims_id'];
$claim_location = $db->fetch_singlet('SELECT ll.licensed_locations_name FROM claims c JOIN licensed_locations ll ON c.join_licensed_locations_id = ll.licensed_locations_id  WHERE claims_id = ?',
        array($claims_id));
?>
<?php include(VIEWS . '/emails/header.php'); ?>

    <p>
        Dear <? echo $recipient['carriers_name']; ?>,

    <p>
        A Claims note has been <? echo $status ; ?> in <?= SITE_NAME?> involving the following:<br/>
        Location: <strong><?php echo $claim_location ;?></strong><br />

    <p>
        You can <a href="<?= FULLURL.'/members/claims/edit.php?claims_id='.$claims_id?>">click here to view the claim.</a>

    <p>
    The <?= SITE_NAME ?> Support Team<br />

<?php include(VIEWS . '/emails/footer.php'); ?>