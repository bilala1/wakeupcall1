<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $bill['members_firstname'].' '.$bill['members_lastname'] ?>,</p>
<p>
    Your membership recently ended at <?= SITE_NAME ?>.  
    We attempted to automatically renew your membership; unfortunately, the credit card on file was unable to be processed.  
    So you don't miss out on your membership benefits, please click the link below in order to process your renewal.  
    We hope you remember all of the resources <?= SITE_NAME ?> offers our members all year long, including:
</p>
<ul>
    <li>HR Phone/Email service</li>
    <li>Employment Law Hotline Email</li>
    <li>100's of OSHA and Safety documents</li>
    <li>Extensive Online Training</li>
    <li>Vast SDS Library</li>
    <li>Claims Management/Trending service</li>
    <li>Certificate Tracking</li>
    <li>Message Boards</li>
    <li>Special discounts on outside services and products</li>
</ul>
<p><?= SITE_NAME ?> has a lot to offer and more is on the way. We would like to have you as a member. Please don't wait to get <?= SITE_NAME ?> back for you and your staff.  Log in or click the link below to rejoin <?= SITE_NAME?> for another full year.</p>
<p><a href="<?= FULLURL ?>/member-payment-failed.php?members_id=<?= $bill['members_id'] ?>" style="color:#005DAA"><?= FULLURL ?>/full-membership.php?members_id=<?= $bill['members_id'] ?></a></p>
<p>If you're not interested in rejoining, we would be grateful if you would please take a couple of minutes and share with us why.  Your input on our 2 min survey will help us improve for the rest of the hospitality industry or for you in the future.</p>
<p><a href="http://www.surveymonkey.com/s/GX2BVX9">http://www.surveymonkey.com/s/GX2BVX9</a></p>
<p>We hope you will continue to take advantage of the many services and benefits of a <?= SITE_NAME ?> membership and we look forward to having you visit us again soon!</p>

<? include(VIEWS . '/emails/footer.php'); ?>