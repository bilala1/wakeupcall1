<? include(VIEWS . '/emails/header.php'); ?>
<p>Member Name: <?= $member_data['members_firstname'].' '.$member_data['members_lastname'] ?></p>
<p>Account Name: <?= $member_data['accounts_name']?></p>
<p>Date: <?= date("Y-m-d") ?></p>

<?if($member_locations)
{
    echo '<p>Locations:</p><ul>';

    $freeOne = false;
    foreach($member_locations as $location)
    {
        $name = $location['licensed_locations_name'];
        if(!$freeOne && $location['corporate_locations_id']) {
            $name .= ' (Free corporate office)';
            $freeOne = true;
        }
        echo '<li>' . $name . '</li>';
    }
    echo '</ul>';
}
?>

<p>A total of: <?= money_format('%n', $member_data['bills_amount']) ?> USD</p>
<p>Charged to card ending in: <?= strings::format_cc('424242424242'. $member_data['last4']) ?></p>

<? include(VIEWS . '/emails/footer.php'); ?>
