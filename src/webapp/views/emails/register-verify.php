<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'] ?>,</p>
<p>We at WAKEUP CALL would like to thank you for taking the opportunity to try our site with our 15 Day Free Trial.
    Please take this time to fully explore all the services included with membership and let us know if you have any questions. 
    Here at WAKEUP CALL, we are always looking for input from our members and guests, in order to continually improve and create the best hospitality resource we can.</p>

<p>Our records indicate that your 15 Day Free Trial will end on <?= date(DATE_FORMAT, strtotime('+ '.FREE_MEMBER_PERIOD.' day')) ?>. At that time, we hope you will join us as a member and enjoy having full access to the documents, training and other resources.</p>

<p>Your user name on record is: <?= $member['members_email'] ?></p>

<p>Thank you again, and we look forward to your input and membership.</p>

<? include(VIEWS . '/emails/footer.php'); ?>