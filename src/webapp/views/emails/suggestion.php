<?php include(VIEWS . '/emails/header.php'); ?>

A member has left a suggestion:<br />
<br />
Member: <?= $member['members_firstname'].' '.$member['members_lastname'] ?><br />
Type: <?= $data['suggestion_type'] ?><br />
Suggestion: <?= $data['suggestion'] ?><br />
<br />

<?php include(VIEWS . '/emails/footer.php'); ?>