<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_fullname'] ?>,</p>

<p>Thank you for your request!  Someone from our team will be in contact shortly.</p>
<? include(VIEWS . '/emails/footer.php'); ?>
