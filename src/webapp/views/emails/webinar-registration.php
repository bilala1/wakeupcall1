<? include(VIEWS . '/emails/header.php'); ?>
<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>Thank you for registering for the upcoming WAKEUP CALL Webinar "<?= $webinar['webinars_name'] ?>". </p>
<p>The Webinar information is below:</p>
<table border="0">
    <tr>
        <td>Name of Webinar:</td>
        <td><?= $webinar['webinars_name'] ?></td>
    </tr>
    <tr>
        <td>Date:</td>
        <td><?= date(DATE_FORMAT, strtotime($webinar['webinars_datetime'])) ?></td>
    </tr>
    <tr>
        <td>Time:</td>
        <td><?= date(DATE_FORMAT_TIME, strtotime($webinar['webinars_datetime'])) ?></td>
    </tr>
    <tr>
        <td>Link:</td>
        <td>
        <? if($webinar['webinars_link'] && ($webinar['webinars_link'] != 'Pending')): ?>
            <a href="<?= $webinar['webinars_link']; ?>"><?= $webinar['webinars_link']; ?></a>
        <? else : ?>
            <i>not yet available</i>
        <? endif; ?>    
        </td>        
    </tr>      
</table>
<p>Please log in to your WAKEUP CALL account in order to join in on the Webinar and please let us know if you have any questions.  As always, we appreciate your feedback on this Webinar and on any of the services that we offer.</p>
<p>Sincerely,</p>
<? include(VIEWS . '/emails/footer.php'); ?>