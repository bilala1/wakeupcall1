<? include(VIEWS . '/emails/header.php'); ?>

<p>Dear <?= $member['members_firstname'].' '.$member['members_lastname'] ?>,</p>
<p>The WAKEUP CALL Webinar that you registered for is just around the corner!  The Webinar information is below:</p>
<table border="0">
    <tr>
        <td>Name of Webinar:</td>
        <td><?= $webinar['webinars_name'] ?></td>
    </tr>
    <tr>
        <td>Date:</td>
        <td><?= times::from_mysql_utc($webinar['webinars_datetime'], DATE_FORMAT, 'America/Los Angeles') ?></td>
    </tr>
    <tr>
        <td>Time:</td>
        <td><?= times::from_mysql_utc($webinar['webinars_datetime'], DATE_FORMAT_TIME, 'America/Los Angeles') ?> Pacific Time</td>
    </tr>
    <tr>
        <td>Link:</td>
        <td>
        <? if($webinar['webinars_link'] && ($webinar['webinars_link'] != 'Pending')): ?>
            <a href="<?= $webinar['webinars_link']; ?>"><?= $webinar['webinars_link']; ?></a>
        <? else : ?>
            <i>not yet available</i>
        <? endif; ?>    
        </td>        
    </tr>      
</table>
<p>Please log in to your WAKEUP CALL account in order to join in on the Webinar and please let us know if you have any questions.  As always, we appreciate your feedback on this Webinar and on any of the services that we offer.</p>
<br />
<p>Sincerely,</p>
<? include(VIEWS . '/emails/footer.php'); ?>