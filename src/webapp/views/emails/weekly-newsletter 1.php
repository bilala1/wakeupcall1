<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>WakeUp Call Network Updates</title>
<style type="text/css">
html, *{
	font-family: Arial, Helvetica, sans-serif;
	color: #333333;
	font-size: 12px;
        }
.style5 {	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
	font-size: 12px;
	line-height: 16px;
}
.style6 {
	font-size: 11px;
	color: #333333;
}
.style30 {
	color: #05559a;
	font-size: 11px;
}
.style38 {
	color: #333333;
	font-size: 24px;
}
.style44 {
	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
	font-size: 12px;
	line-height: 16px;
	font-style: italic;
}
.style47 {color: #05559a; font-weight: bold; }
.style52 {color: #05559a; font-weight: bold; font-size: 16px; }
.style56 {
	color: #FF9900;
	font-size: 24px;
	font-weight: bold;
}
.style64 {color: #05559a; font-weight: bold; font-size: 14px; }
.style66 {color: #333333; font-size: 18px; font-weight: bold; }
.style68 {color: #003399}
.style71 {
	color: #666666;
	font-size: 24px;
	font-weight: bold;
}
.style72 {color: #FFFFFF; font-size: 12px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
p{color:#333}
td{ text-align:left}
</style>
</head>
<body>
<table width="650" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666"  bgcolor="#ffffff" style="font-family: Arial, Helvetica, sans-serif;
color: #333333;font-size: 12px; text-align:left">
  <tr>
    <td valign="top"><a href="http://www.wakeupcall.net" target="newwindow"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/wakeupcall-newsletter-masthead2.jpg" width="650" height="112" border="0"></a>
      <table width="650" height="27" border="0" cellpadding="0" cellspacing="0" bgcolor="#05559a">
        <tr>
          <td width="23">&nbsp;</td>
          <td width="451" class="style72"> <!--Issue <?= $newsletters_id; ?> --></td>
          <td width="176" class="style72" style="color: #fff"><?= date('F j, Y',  strtotime('now')); ?></td>
        </tr>
      </table>
      <br>
        <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
          <tr>
            <td width="396" align="center" valign="top" style="color:#333333">                 
            <? if($newsletter_hospitality_news): ?>      
            <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#FFFFFF" style="color:#333333">
              <tr>
                <td width="374" valign="top" style="text-align:left">
                  
                <table width="318" height="54" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="108" align="center" valign="top"><span class="style38"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-hospitalitynews2.jpg" width="84" height="81"></span></td>
                    <td width="226" valign="middle"><span class="style71" style="color: #666666;font-size: 24px;font-weight: 700;">Hospitality News</span></td>
                  </tr>
                </table>              
                    <? foreach($newsletter_hospitality_news as $hosp_news): ?>
                        <span class="style52"><?= $hosp_news['title']; ?></span>
                        <span class="style5"><br>
                          <?= $hosp_news['description']; ?>
                          For more, visit <a href="<?= $hosp_news['link']; ?>" target="newwindow"><?= $hosp_news['link']; ?></a><br>
                        </span>
                        <span class="style44">Source: <?= $hosp_news['type']; ?>.com    published <?= date('m/d/Y', strtotime($hosp_news['pubDate'])); ?></span>
                        <span class="style5"><br/><br/></span>
                    <? endforeach; ?>
                </td>
              </tr>
            </table>
             <br>
            <? endif; ?>      
            <? if($newsletter_hr_news): ?> 
            <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#999999" style="color:#333333; text-align:left">
            <tr>
              <td width="374" valign="top" style="text-align:left">
                <table width="318" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="108"><span class="style38"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-hrnews2.jpg" width="99" height="78"></span></td>
                      <td width="210"><span class="style71" style="color: #666666;font-size: 24px;font-weight: 700;">HR News</span></td>
                    </tr>
                </table>
                <? foreach($newsletter_hr_news as $hrn): ?>  
                    <span class="style52"><?= $hrn['title']; ?> </span><br>
                          <span class="style5"><?= $hrn['description']; ?> </span>
                    <p><span class="style44"> Source: hrdailyadvisor.blr.com	    published <?= date('m/d/Y', strtotime($hrn['pubDate'])); ?></span><br>
                    </p>
                <? endforeach; ?>
              </td>
            </tr>
            </table>
            <br>
            <? endif; ?>
            <? if($recent_articles): ?>      
            <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#FFFFFF">
              <tr>
                <td width="374" valign="top" style="text-align:left">
                  
                <table width="318" height="54" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="334" align="center" valign="top"><span class="style38"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-wakeup-call-news.png" width="324" height="46"></span></td>
                  </tr>
                </table>              
                    <? foreach($recent_articles as $wuc_news): ?>
                        <span class="style52" style="color: #05559a; font-weight: bold; font-size: 16px;"><?= $wuc_news['articles_title']; ?></span>
                        <span class="style5"><br>
                          <?= $wuc_news['articles_body']; ?>
                          For more, visit <a href="<?= HTTP_FULLURL; ?>/members/news/detail.php?articles_id=<?= $wuc_news['articles_id']; ?>" target="newwindow"><?= HTTP_FULLURL; ?>/members/news/detail.php?articles_id=<?= $wuc_news['articles_id']; ?></a><br>
                        </span>
                        <span class="style44"></span>
                        <span class="style5"><br/><br/></span>
                    <? endforeach; ?>
                </td>
              </tr>
            </table>
            <br>
            <? endif; ?>               
            </td>
            <td width="254" valign="top">
            <? if($recent_faqs): ?>    
                <table width="238" height="64" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="113" align="center" valign="top"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-didyouknow2.jpg" alt="Did You Know?" width="94" height="88"></td>
                    <td width="125"><span class="style71" style="font-size:24px; font-weight:bold; color:#666666
                    ">Did You Know?</span></td>
                  </tr>
                </table>
                  <table width="238" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#e7e7e7" bgcolor="#e7e7e7">
                  <tr>
                    <td width="374" valign="top">
                        <? foreach($recent_faqs as $faq): ?>
                            <p>
                                <span class="style47" style="color: #05559a"><b>Question:</b></span>
                                <span class="style5"> <?= html::xss($faq['faqs_question']) ?><br>
                                <br />
                                </span>
                                <span class="style47" style="color:#05559a"><b>Answer A:</b></span>
                                <span class="style44"> 
                                    <?= $faq['faqs_answer'] ?>
                                </span>
                                <br />
                            </p>
                            <hr size="1" noshade />    
                        <? endforeach; ?>
                    </td>
                  </tr>
                </table>
            <? endif; ?>      
            <br>
            <br>
            <? if($popular_forum_topics): ?>
                <table width="238" height="64" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="113" align="center" valign="bottom" bgcolor="#FFFFFF"><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-forum2.jpg" alt="Did You Know?" width="99" height="79"></td>
                    <td width="124" height="79"><span class="style71">Forum Updates</span></td>
                  </tr>
                </table>
                <table width="238" height="196" border="1" cellpadding="6" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#e7e7e7">
                  <tr>
                    <td width="374" valign="top">
                        <? foreach($popular_forum_topics as $topic): ?>
                            <p><span class="style64">
                                    <?= $topic['topics_title'] ; ?>
                                </span><br>
                            <span class="style44">posted by <?= $topic['members_name'] ?> on <?= date('n/j/Y', strtotime($topic['topics_last_posts_date'])); ?></span>
                            </p>
                        <? endforeach ?>
                    </td>
                  </tr>
                </table>
                <br>
            <? endif; ?>
                
            <? if($webinars): ?> 
            <table width="238" height="64" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="113"><span><img src="<?= HTTP_FULLURL; ?>/images/newsletter/icon-webinars2.jpg" alt="" width="99" height="80" bgcolor="#FFFFFF"></span></td>
                  <td width="124"><span class="style71" >Upcoming Webinars</span></td>
                </tr>
            </table>                
            <table width="238" height="200" border="1" cellpadding="6" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#e7e7e7">
            <tr>
              <td width="374" valign="top" style="text-align:left">
                  <? foreach($webinars as $webinar): ?>
                    <p><span class="style66" style="font-size:18px;color:#333333"><b><?= $webinar['webinars_name'] ?></b> </span>
                      <b class="style47" style="color:#05559a"><?= times::from_mysql_utc($webinar['webinars_datetime']) ?></b><br>
                       <?= $webinar['webinars_description'] ?>
                    </p>                      
                  <? endforeach; ?>                   
              </td>
            </tr>
            </table>
            <br />
            <? endif; ?>             
            
            </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
<br>
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><span class="style68"><a href="http://www.wakeupcall.net" target="newwindow" class="style47">www.wakeupcall.net</a></span><span class="style30"><br>
        <span class="style6"><br>
To
            unsubscribe from future updates, please reply with the word &quot;unsubscribe&quot; in
            the subject line.<br>
Copyright (c) <?= date('Y'); ?> WAKEUP CALL</span></span></td>
  </tr>
</table>
</body>
</html>



