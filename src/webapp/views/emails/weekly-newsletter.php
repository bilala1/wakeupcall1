<html xmlns="http://www.w3.org/1999/xhtml"
      style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

<head style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"
          style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

    <title style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">WakeUp Call Network
        Updates</title>


</head>

<body style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

<table width="650" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666" bgcolor="#ffffff"
       style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

<tr>

<td valign="top" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><a
    href="http://www.wakeupcall.net" target="newwindow"
    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><img
        src="<?= FULLURL; ?>/images/newsletter/wakeupcall-newsletter-masthead2.jpg" width="650" height="112" border="0"
        style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"></a>

<table width="650" height="27" border="0" cellpadding="0" cellspacing="0" bgcolor="#05559a"
       style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

    <tr>

        <td width="23" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">&nbsp;</td>

        <td width="451" class="style72"
            style="font-family: Arial, Helvetica, sans-serif;color: #FFFFFF;font-size: 12px;line-height: 16px;font-weight: bold;">
            <!--Issue <?= $newsletters_id; ?> --></td>

        <td width="176" class="style72"
            style="font-family: Arial, Helvetica, sans-serif;color: #FFFFFF;font-size: 12px;line-height: 16px;font-weight: bold;"><?= date('F j, Y', strtotime('now')); ?></td>

    </tr>

</table>

<br>

<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
       style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

<tr>

<td width="396" align="center" valign="top"
    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

    <? if ($newsletter_hospitality_news): ?>

        <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#FFFFFF"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">


                    <table width="318" height="54" border="0" cellpadding="0" cellspacing="0"
                           style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                        <tr>

                            <td width="108" align="center" valign="top"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                    class="style38"
                                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 24px;"><img
                                        src="<?= FULLURL; ?>/images/newsletter/icon-hospitalitynews2.jpg" width="84"
                                        height="81"
                                        style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"></span>
                            </td>

                            <td width="226" valign="middle"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                    class="style71"
                                    style="font-family: Arial, Helvetica, sans-serif;color: #666666;font-size: 24px;font-weight: bold;">Hospitality News</span>
                            </td>

                        </tr>

                    </table>

                    <? foreach ($newsletter_hospitality_news as $hosp_news): ?>

                        <span class="style52"
                              style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 16px;font-weight: bold;"><?= $hosp_news['title']; ?></span>

                        <span class="style5"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"><br>

                            <?= $hosp_news['description']; ?>

                            For more, visit <a href="<?= $hosp_news['link']; ?>" target="newwindow"
                                               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><?= $hosp_news['link']; ?></a><br>

                        </span>

                        <span class="style44"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;font-style: italic;">Source: <?= $hosp_news['type']; ?>
                            .com    published <?= date('m/d/Y', strtotime($hosp_news['pubDate'])); ?></span>

                        <span class="style5"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"><br><br></span>

                    <? endforeach; ?>

                </td>

            </tr>

        </table>

        <br>

    <? endif; ?>

    <? if ($newsletter_hr_news): ?>

        <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#999999"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                    <table width="318" border="0" cellspacing="0" cellpadding="0"
                           style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                        <tr>

                            <td width="108"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                    class="style38"
                                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 24px;"><img
                                        src="<?= FULLURL; ?>/images/newsletter/icon-hrnews2.jpg" width="99" height="78"
                                        style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"></span>
                            </td>

                            <td width="210"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                    class="style71"
                                    style="font-family: Arial, Helvetica, sans-serif;color: #666666;font-size: 24px;font-weight: bold;">HR News</span>
                            </td>

                        </tr>

                    </table>

                    <? foreach ($newsletter_hr_news as $hrn): ?>

                        <span class="style52"
                              style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 16px;font-weight: bold;"><?= $hrn['title']; ?> </span>
                        <br>

                        <span class="style5"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"><?= $hrn['description']; ?> </span>

                        <p style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                class="style44"
                                style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;font-style: italic;"> Source: hrdailyadvisor.blr.com	    published <?= date('m/d/Y', strtotime($hrn['pubDate'])); ?></span><br>

                        </p>

                    <? endforeach; ?>

                </td>

            </tr>

        </table>

    <? endif; ?>

    <br>

    <? if ($recent_articles): ?>

        <table width="361" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#FFFFFF"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">


                    <table width="318" height="54" border="0" cellpadding="0" cellspacing="0"
                           style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                        <tr>

                            <td width="334" align="center" valign="top"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                    class="style38"
                                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 24px;"><img
                                        src="<?= FULLURL; ?>/images/newsletter/icon-wakeup-call-news.png" width="324"
                                        height="46"
                                        style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"></span>
                            </td>

                        </tr>

                    </table>

                    <? foreach ($recent_articles as $wuc_news): ?>

                        <span class="style52"
                              style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 16px;font-weight: bold;"><?= $wuc_news['articles_title']; ?></span>

                        <span class="style5"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"><br>

                            <?= $wuc_news['articles_body']; ?>

                            For more, visit <a
                                href="<?= FULLURL; ?>/members/news/detail.php?articles_id=<?= $wuc_news['articles_id']; ?>"
                                target="newwindow"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><?= FULLURL; ?>
                                /members/news/detail.php?articles_id=<?= $wuc_news['articles_id']; ?></a><br>

                    </span>

                        <span class="style44"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;font-style: italic;"></span>

                        <span class="style5"
                              style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"><br><br></span>

                    <? endforeach; ?>

                </td>

            </tr>

        </table>

        <br>

    <? endif; ?>

</td>

<td width="254" valign="top" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

    <? if ($recent_faqs): ?>

        <table width="238" height="64" border="0" cellpadding="0" cellspacing="0"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="113" align="center" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><img
                        src="<?= FULLURL; ?>/images/newsletter/icon-didyouknow2.jpg" alt="Did You Know?" width="94"
                        height="88" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">
                </td>

                <td width="125" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                        class="style71"
                        style="font-family: Arial, Helvetica, sans-serif;color: #666666;font-size: 24px;font-weight: bold;">Did You Know?</span>
                </td>

            </tr>

        </table>

        <table width="238" height="317" border="1" cellpadding="6" cellspacing="0" bordercolor="#e7e7e7"
               bgcolor="#e7e7e7" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                    <? foreach ($recent_faqs as $faq): ?>

                        <p style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                            <span class="style47"
                                  style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 12px;font-weight: bold;">Question:</span>

                                <span class="style5"
                                      style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;"> <?= html::xss($faq['faqs_question']) ?>
                                    <br>

                                <br>

                                </span>

                            <span class="style47"
                                  style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 12px;font-weight: bold;">Answer:</span>

                                <span class="style44"
                                      style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;font-style: italic;">

                                    <?= $faq['faqs_answer'] ?>

                                </span>

                            <br>

                        </p>

                        <hr size="1" noshade
                            style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                    <? endforeach; ?>

                </td>

            </tr>

        </table>

    <? endif; ?>

    <br>

    <br>

    <? if ($popular_forum_topics): ?>

        <table width="238" height="64" border="0" cellpadding="0" cellspacing="0"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="113" align="center" valign="bottom" bgcolor="#FFFFFF"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><img
                        src="<?= FULLURL; ?>/images/newsletter/icon-forum2.jpg" alt="Did You Know?" width="99"
                        height="79" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">
                </td>

                <td width="124" height="79"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                        class="style71"
                        style="font-family: Arial, Helvetica, sans-serif;color: #666666;font-size: 24px;font-weight: bold;">Forum Updates</span>
                </td>

            </tr>

        </table>

        <table width="238" height="196" border="1" cellpadding="6" cellspacing="0" bordercolor="#CCCCCC"
               bgcolor="#e7e7e7" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                    <? foreach ($popular_forum_topics as $topic): ?>

                        <p style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                class="style64"
                                style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 14px;font-weight: bold;">

                                    <?= $topic['topics_title']; ?>

                                </span><br>

                            <span class="style44"
                                  style="font-family: Arial, Helvetica, sans-serif;color: #000000;font-size: 12px;line-height: 16px;font-style: italic;">posted by <?= $topic['members_name'] ?>
                                on <?= date('n/j/Y', strtotime($topic['topics_last_posts_date'])); ?></span>

                        </p>

                    <? endforeach ?>

                </td>

            </tr>

        </table>

        <br>

    <? endif; ?>

    <? if ($webinars): ?>

        <table width="238" height="64" border="0" cellpadding="0" cellspacing="0"
               style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="113" align="center" valign="bottom" bgcolor="#FFFFFF"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><img
                        src="<?= FULLURL; ?>/images/newsletter/icon-webinars2.jpg" alt="Did You Know?" width="99"
                        height="79" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">
                </td>

                <td width="124" height="79"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                        class="style71"
                        style="font-family: Arial, Helvetica, sans-serif;color: #666666;font-size: 24px;font-weight: bold;">Upcoming Webinars</span>
                </td>

            </tr>

        </table>

        <table width="238" height="196" border="1" cellpadding="6" cellspacing="0" bordercolor="#CCCCCC"
               bgcolor="#e7e7e7" style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

            <tr>

                <td width="374" valign="top"
                    style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">

                    <? foreach ($webinars as $webinar): ?>

                        <p style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;"><span
                                class="style66"
                                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 18px;font-weight: bold;"><?= $webinar['webinars_name'] ?> </span>

                            <span class="style47"
                                  style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 12px;font-weight: bold;"><?= times::from_mysql_utc($webinar['webinars_datetime']) ?></span><br>

                            <?= $webinar['webinars_description'] ?>

                        </p>

                    <? endforeach; ?>

                </td>

            </tr>

        </table>

        <br>

    <? endif; ?>

</td>

</tr>

</table>

</td>

</tr>

</table>

<br>

<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
        <td valign="top" bgcolor="#FFFFFF"
            style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">
            <a href="http://www.wakeupcall.net" target="_blank"
               style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 12px;font-weight: bold;">www.wakeupcall.net</a></span>
            <br>
            <br>
            <a href="<?= $notification_mgmt_url ?>" target="_blank"
               style="font-family: Arial, Helvetica, sans-serif;color: #05559a;font-size: 12px;font-weight: bold;">Manage email notifications</a></span>
            <br>
            <br>
            <span
                style="font-family: Arial, Helvetica, sans-serif;color: #333333;font-size: 12px;">Copyright (c) <?= date('Y'); ?>
                WAKEUP CALL</span>
        </td>

    </tr>

</table>

</body>

</html>









