<?php
if(DEPLOYMENT != 'development') {
    include VIEWS . '/404.php';
    exit;
}
?>
<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en-US"> <!--<![endif]-->
<head>
    <!-- un-comment and delete 2nd meta below to disable zoom (not cool)
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Benefits &ndash;  WAKEUP CALL: Online Hospitality Risk Management</title>
    <link rel="pingback" href="https://www.wakeupcall.net/wp/xmlrpc.php" />
    <link rel="alternate" type="application/rss+xml" title="WAKEUP CALL: Online Hospitality Risk Management" href="https://www.wakeupcall.net/wp/feed/" />


    <meta name='robots' content='noindex,follow' />
    <link rel="alternate" type="application/rss+xml" title="WAKEUP CALL: Online Hospitality Risk Management &raquo; Feed" href="https://www.wakeupcall.net/wp/feed/" />
    <link rel="alternate" type="application/rss+xml" title="WAKEUP CALL: Online Hospitality Risk Management &raquo; Comments Feed" href="https://www.wakeupcall.net/wp/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/www.wakeupcall.net\/wp\/wp-includes\/js\/wp-emoji-release.min.js"}};
        !function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='style-css'  href='https://www.wakeupcall.net/wp/wp-content/themes/Karma-Child-Theme/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='primary-color-css'  href='https://www.wakeupcall.net/wp/wp-content/themes/Karma/css/karma-cool-blue.css' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='https://www.wakeupcall.net/wp/wp-content/themes/Karma/css/_font-awesome.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mobile-css'  href='https://www.wakeupcall.net/wp/wp-content/themes/Karma/css/_mobile.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='https://www.wakeupcall.net/wp/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='https://www.wakeupcall.net/wp/wp-content/plugins/revslider/rs-plugin/css/settings.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
    </style>
    <link rel='stylesheet' id='rfw-style-css'  href='https://www.wakeupcall.net/wp/wp-content/plugins/rss-feed-widget/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rfw-slider-style-css'  href='https://www.wakeupcall.net/wp/wp-content/plugins/rss-feed-widget/jquery.bxslider.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wprss-et-styles-css'  href='https://www.wakeupcall.net/wp/wp-content/plugins/wp-rss-excerpts-thumbnails/css/styles.css' type='text/css' media='all' />
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/rss-feed-widget/functions.js'></script>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/rss-feed-widget/jquery.bxslider.js'></script>
    <link rel='canonical' href='https://www.wakeupcall.net/wp/services/benefits/' />
    <link rel='shortlink' href='https://www.wakeupcall.net/wp/?p=15' />
    <script type="text/javascript">
        jQuery(document).ready(function() {
            // CUSTOM AJAX CONTENT LOADING FUNCTION
            var ajaxRevslider = function(obj) {

                // obj.type : Post Type
                // obj.id : ID of Content to Load
                // obj.aspectratio : The Aspect Ratio of the Container / Media
                // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

                var content = "";

                data = {};

                data.action = 'revslider_ajax_call_front';
                data.client_action = 'get_slider_html';
                data.token = 'bec5e184e5';
                data.type = obj.type;
                data.id = obj.id;
                data.aspectratio = obj.aspectratio;

                // SYNC AJAX REQUEST
                jQuery.ajax({
                    type:"post",
                    url:"https://www.wakeupcall.net/wp/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    data:data,
                    async:false,
                    success: function(ret, textStatus, XMLHttpRequest) {
                        if(ret.success == true)
                            content = ret.data;
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });

                // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                return content;
            };

            // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
            var ajaxRemoveRevslider = function(obj) {
                return jQuery(obj.selector+" .rev_slider").revkill();
            };

            // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
            var extendessential = setInterval(function() {
                if (jQuery.fn.tpessential != undefined) {
                    clearInterval(extendessential);
                    if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                        jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});
                        // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                        // func: the Function Name which is Called once the Item with the Post Type has been clicked
                        // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                        // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                    }
                }
            },30);
        });
    </script>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <!--styles generated by site options-->
    <style type='text/css'>
        #menu-main-nav  li.wuc-login a strong {color:#87F900 !important;}
        h5 {margin-bottom:0px; font-weight:bold;}
        h1 {color:#003769; font-weight:bold;}

        p {line-height:1.8em !important;}

        #menu-main-nav li a span strong {font-weight:normal !important; font-size:12px !important;}

        body.karma-menu-no-description #menu-main-nav { margin-top:30px;}

        #wrapper .tabset li {border:1px solid #999 !important; border-radius:5px !important;}

        .tabset {text-align:center !important;}

        #menu-main-nav li {  padding: 5px 25px 5px 13px; }
        #menu-main-nav li:before { height:20px !important;}
        #menu-main-nav .drop { top:26px !important;}

        .message_karma_politicalblue {background-image:none !important; background-color:#00366A !important;}
        .message_karma_coolblue {background-image:none !important; background-color:#0083C5 !important;}

        .colored_box {padding:15px !important;}

        #footer-callout {
            padding-top:5px !important;
        }

        #footer-callout-content:before {
            content: "“";
            float:left;
            font-size:228px;
            margin-top:113px;
            margin-left:-70px;
        }

        #footer-callout-content:after {
            content: "”";
            float:right;
            font-size:228px;
            margin-top:-23px;
            margin-right:-70px;
        }

        #footer-callout-content {
            max-width:65%;
            margin:0 auto;
            text-align:center;

        }
        h2.wuc {
            font-size:12px;
            margin-top: 30px;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            z-index: 1;
            color:#FFFFFF;
        }

        h2.wuc:before {
            border-top: 2px solid #dfdfdf;
            content:"";
            margin: 0 auto;
            position: absolute;
            top: 5px; left: 0; right: 0; bottom: 0;
            width: 70%;
            z-index: -1;
        }

        h2.wuc span { background: #003869; padding: 0 10px; }
        #menu-main-nav li#item-154 strong {color:#F8EA29 !important;}

        #wrapper .tabs-area, #wrapper .tabset { padding:0 0 30px 0 !important;}

        p {text-align:justify;}

        .contact-form input, .contact-form textarea, .wpcf7 input[type="text"], .wpcf7 input[type="email"], .wpcf7 input[type="tel"], .wpcf7 input[type="url"], .wpcf7 input[type="number"], .wpcf7 input[type="date"], .wpcf7 textarea {width:90%;}

        #request-trial-submit {width:370px; margin:0 auto !important; margin-top:25px !important; display:block;}
        .rt-submit {width:370px; margin-top:20px;}

        .wpcf7-submit {
            background-image: -webkit-linear-gradient(top, #3B6893, #164B78);
        }

        li a.rsswidget {display:block; color:#FFF;}
        a.rsswidget img {display:none;}
        .rss-date {color: #4385B0;}
        cite {display:none;}

        ul.rss-aggregator ul li, .rss-aggregator ul {list-style:none !important ; margin-left:0 !important; padding-left:0 !important;}
        li.feed-item  {list-style: none !important; margin-left:0 !important; padding-left:0 !important; margin-bottom:20px !important;}
        #content ul.rss-aggregator, .content_full_width ul.rss-aggregator {margin:0 0 20px 0  !important;}
        .feed-source {display:block;}

        .social_icons .rss {display:none;}

        .content-style-default .modern_img_frame .img-preload {
            /* background-image: url() !important; */
        }

        #header .tt-retina-logo {
            width: ;
            height: ;
            url: "";
        }


        #tt-boxed-layout {
            -moz-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.5);
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.5);
        }
        body{background-image:url(https://www.wakeupcall.net/wp/wp-content/themes/Karma/images/body-backgrounds/darkdenim3.png) !important;background-position:left top !important;background-repeat:repeat !important;}
        #main{font-size:14px!important;}
        #menu-main-nav, #menu-main-nav li a span strong{font-size:12px!important;}
    </style>
    <link rel="icon" href="https://www.wakeupcall.net/wp/wp-content/uploads/cache/2015/10/WAKEUP-CALL-exclamation-point/3890554079.png" sizes="32x32" />
    <link rel="icon" href="https://www.wakeupcall.net/wp/wp-content/uploads/cache/2015/10/WAKEUP-CALL-exclamation-point/2666216850.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://www.wakeupcall.net/wp/wp-content/uploads/cache/2015/10/WAKEUP-CALL-exclamation-point/612637658.png">
    <meta name="msapplication-TileImage" content="https://www.wakeupcall.net/wp/wp-content/uploads/cache/2015/10/WAKEUP-CALL-exclamation-point/2106824210.png">

    <!--[if IE 9]>
    <style media="screen">
        #footer,
        .header-holder
        {
            behavior: url(https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/PIE/PIE.php);
        }
    </style>
    <![endif]-->

    <!--[if lte IE 8]>
    <script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/html5shiv.js'></script>
    <style media="screen">
        /* uncomment for IE8 rounded corners
        #menu-main-nav .drop ul a,
        #menu-main-nav .drop,
        #menu-main-nav ul.sub-menu,
        #menu-main-nav .drop .c,
        #menu-main-nav li.parent, */

        a.button,
        a.button:hover,
        ul.products li.product a img,
        div.product div.images img,
        span.onsale,
        #footer,
        .header-holder,
        #horizontal_nav ul li,
        #horizontal_nav ul a,
        #tt-gallery-nav li,
        #tt-gallery-nav a,
        ul.tabset li,
        ul.tabset a,
        .karma-pages a,
        .karma-pages span,
        .wp-pagenavi a,
        .wp-pagenavi span,
        .post_date,
        .post_comments,
        .ka_button,
        .flex-control-paging li a,
        .colored_box,
        .tools,
        .karma_notify
        .opener,
        .callout_button,
        .testimonials {
            behavior: url(https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/PIE/PIE.php);
        }
    </style>
    <![endif]-->

    <!--[if IE]>
    <link rel="stylesheet" href="https://www.wakeupcall.net/wp/wp-content/themes/Karma/css/_internet_explorer.css" media="screen"/>
    <![endif]-->




</head>

<body class="page page-id-15 page-child parent-pageid-7 page-template-default karma-no-post-date">
<div id="tt-boxed-layout" class="content-style-default">
    <div id="wrapper">
        <header role="banner" id="header" >

            <div class="header-holder ">
                <div class="header-overlay">
                    <div class="header-area">

                        <a href="https://www.wakeupcall.net/wp" class="logo"><img src="https://www.wakeupcall.net/wp/wp-content/uploads/2015/05/logo.png" alt="WAKEUP CALL: Online Hospitality Risk Management" /></a>



                        <nav role="navigation">
                            <ul id="menu-main-nav" class="sf-menu">
                                <li id="item-27"  class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="https://www.wakeupcall.net/wp/about/"><span><strong>About</strong></span></a>
                                    <ul class="sub-menu">
                                        <li id="item-30"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/about/executives/"><span>Executives</span></a></li>
                                        <li id="item-29"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/about/videos/"><span>Videos</span></a></li>
                                        <li id="item-28"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/about/testimonials/"><span>Testimonials</span></a></li>
                                    </ul>
                                </li>
                                <li id="item-22"  class=" menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor current-menu-ancestor current-menu-parent current-page-parent current_page_parent current_page_ancestor menu-item-has-children"><a href="https://www.wakeupcall.net/wp/services/"><span><strong>Services</strong></span></a>
                                    <ul class="sub-menu">
                                        <li id="item-26"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/services/services/"><span>Services</span></a></li>
                                        <li id="item-25"  class=" menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-15 current_page_item"><a href="https://www.wakeupcall.net/wp/services/benefits/"><span>Benefits</span></a></li>
                                        <li id="item-24"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/services/valuepricing/"><span>Value/Pricing</span></a></li>
                                        <li id="item-23"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/services/faqs/"><span>FAQs</span></a></li>
                                    </ul>
                                </li>
                                <li id="item-310"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/wakeup-call-news/"><span><strong>News</strong></span></a></li>
                                <li id="item-289"  class=" menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="http://hospitalityriskupdate.com/"><span><strong>Blog</strong></span></a></li>
                                <li id="item-19"  class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.wakeupcall.net/wp/contact-us/"><span><strong>Contact Us</strong></span></a></li>
                                <li id="item-290"  class=" wuc-login menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="https://www.wakeupcall.net/login.php"><span><strong>Login</strong></span></a></li>
                                <li id="item-291"  class=" menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="http://www.wakeupcall.net/signup-direct.php"><span><strong>Register</strong></span></a></li>
                            </ul>
                        </nav>
                    </div><!-- END header-area -->
                </div><!-- END header-overlay -->
            </div><!-- END header-holder -->
        </header><!-- END header -->


        <div id="main" class="tt-slider-null">
            <section id="tt-parallax-banner" data-type="background" data-speed="5" style="padding:0 0; background-color:#;;">
                <div class="tt-parallax-text" style="display:none;"><p><a href="https://www.wakeupcall.net/signup-direct.php"><img class="alignnone size-full wp-image-113" src="https://www.wakeupcall.net/wp/wp-content/uploads/2015/05/WebsiteMockup_WUC_BENEFITS.jpg" alt="WebsiteMockup_WUC_BENEFITS" width="1400" height="324" /></a></p>
                </div></section>	<div class="main-area">

                <main role="main" id="content" class="content_full_width">
                    <h1>BENEFITS</h1>
                    <p>WAKEUP CALL’s unique Cloud-based platform was developed specifically for the hotel industry, providing easy remote access to a full range of risk management services from any Internet-enabled device. This easy and affordable solution serves as a single source platform for streamlining a range of operational and risk management processes. We provide:</p>
                    <h5>Savings On Investment (SOI)™</h5>
                    <p>When it comes to SOI, no other proactive system available gives hoteliers the potential savings as WAKEUP CALL.  The small investment in your risk management system with WAKEUP CALL can save you hundreds of thousands in lawsuits, fines, claims and premiums.</p>
                    <h5>Incredible Value</h5>
                    <p>WAKEUP CALL is full of valuable tools and resources — all available to you at a fraction of what it would cost if you paid for these services individually elsewhere. (See pricing information for more details.)</p>
                    <div class="modern_img_frame modern_four_col_large tt-img-right"><div class="img-preload"><img src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/framework/extended/timthumb/timthumb.php?src=https://www.wakeupcall.net/wp/wp-content/uploads/2015/05/bene-1.jpg&amp;h=111&amp;w=190' alt='' class="attachment-fadeIn" /></div></div><h5 style="margin-top: 30px;">Real Time Corporate Control</h5>
                    <p>WAKEUP CALL gives your corporate office real time control and overview of all your properties.  Contract Management, Claims Management, Certificate Tracking, Training and more, at your fingertips for each location.</p>
                    <h5>No More Reliance on a Carrier, Broker or other service provider</h5>
                    <p>You no longer need to be bound to a broker, carrier or other service with concerns of losing your data if you change service providers. Take back control of your information and risk management needs! With WAKEUP CALL as your personal resource, you can easily manage and locate all the tools you need and have access to the right resources, as well as your own archived data and detailed case information, 24/7!</p>
                    <h5>Staff Changes</h5>
                    <p>Unfortunately, GM and other key staff turnover are more common that we would like. With WAKEUP CALL, getting new members updated and running is as simple as logging in. Nothing gets lost, forgotten or falls through the cracks when WAKEUP CALL is helping manage your staff transitions.</p>
                    <h5>One Stop, Easy to Use Resource</h5>
                    <p>No more scrambling around, looking for information in multiple filing cabinets or logging into a different website for every tool you need. Tired of needing information you left it at the office? WAKEUP CALL is a one stop, online resource for risk management and administrative needs. Our interface was designed to be one of the easiest to learn and use tools available to hoteliers…..ever.</p>
                    <h5>Peace of Mind</h5>
                    <p>Sleep better knowing your entire team has a resource that provides them with the tools they need to prepare and handle the many crisis that can arise from day to day. WAKEUP CALL gives you the peace of mind that your hotel, employees, guests and money are all safe.</p>
                    <div class="modern_img_frame modern_four_col_large tt-img-right"><div class="img-preload"><img src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/framework/extended/timthumb/timthumb.php?src=https://www.wakeupcall.net/wp/wp-content/uploads/2015/05/bene-2.jpg&amp;h=111&amp;w=190' alt='' class="attachment-fadeIn" /></div></div><h5 style="margin-top: 30px;">Co-Branding</h5>
                    <p>If you’re a management or ownership group, showing partners and owners the value you bring is always important. WAKEUP CALL can co-brand the interface with your logo, so everyone can see the systems and value that sets your team apart from the others.</p>
                    <h5>Intuitive Navigation &amp; Interface</h5>
                    <p>WAKEUP CALL is NOT a program that you will spend 30 minutes relearning or trying to figure out every time you log in. Using WAKEUP CALL requires minimal computer skills to access all of our valuable tools and resources. Intuitive, user-friendly features are provided throughout the entire WAKEUP CALL site.</p>
                    <h5>Access to National Vendors and Service Providers</h5>
                    <p>WAKEUP CALL has arranged for free or discounted rates exclusively for WAKEUP CALL members on commonly used products and services. A few of the services include background searches, drug testing, MVR&#8217;s, site inspections and direct access to attorneys at our members-only discounted rates.</p>
                    <h5>We want to hear from you!</h5>
                    <p>WAKEUP CALL is a dynamic site, changing as the hospitality industry changes. In addition to our own development team, WAKEUP CALL is constantly seeking input from our members in order to stay current on the industry’s most critical issues. Check out our blog and let us know if there’s an issue that is affecting you that we can help address.</p>
                </main><!-- END main #content -->
            </div><!-- END main-area -->


            <div id="footer-top">&nbsp;</div><!-- END footer-top -->
        </div><!-- END main -->

        <footer role="contentinfo" id="footer">
            <div id="footer-callout" >
                <div id="footer-callout-content">
                    <script type="text/javascript" src="https://webbies.dk/assets/files/SudoSlider/package/js/jquery.sudoSlider.min.js"></script>
                    <script type="text/javascript" >
                        jQuery(document).ready(function($){
                            var sudoSlider = $("#wuc-slider").sudoSlider({
                                effect: "slide",
                                pause: 5000,
                                speed:500,
                                auto:true,
                                autoheight:false,
                                continuous: true,
                                prevNext:false
                            });
                        });
                    </script>

                    <h2 class="wuc"><span>OUR CLIENTS SAY IT BEST</span></h2>
                    <div id="wuc-slider" stye="height:120px;" >
                        <div><p class="footer-callout-text" style="text-align:center;">Implementation of WAKEUP CALL's platform has proven to be a tremendous asset in coordinating day-to-day procedures, such as interactions with guests and other members of staff.</p>
                            <p style="margin: 0 20px; text-align:center;">Michael Bullis, President, Destination Properties, LLC</p></div>

                        <div><p class="footer-callout-text" style="text-align:center;">WAKEUP CALL's defining asset is its ability to serve as a one-stop-shop for all functions that are necessary for ensuring a safely operated, legally certified and liability secured hospitality business.</p>
                            <p style="margin: 0 20px; text-align:center;">Rick Skinner, VP of Operations, HMG Hospitality</p></div>

                        <div><p class="footer-callout-text" style="text-align:center;">WAKEUP CALL offers us the ability to provide consistent information to all of our hotels, and gives us a great overview of their activity.  The benefits are numerous, and the cost is reasonable compared to the potential exposure we could face without a tool like WAKEUP CALL.</p>
                            <p style="margin: 0 20px; text-align:center;">Chuck Valentino, VP of Operations, Vista Investments, LLC</p></div>
                    </div>                                     </div><!-- END footer-callout-content -->
            </div><!-- END footer-callout -->

            <div class="footer-overlay">

                <div class="footer-content">
                    <div class="one_third tt-column"><h3><a class='rsswidget' href='http://hospitalityriskupdate.com/feed/'><img style='border:0' width='14' height='14' src='https://www.wakeupcall.net/wp/wp-includes/images/rss.png' alt='RSS' /></a> <a class='rsswidget' href='http://hospitalityriskupdate.com/'>Visit Our Blog</a></h3><ul><li><a class='rsswidget' href='http://hospitalityriskupdate.com/2015/12/14/overtime-changes-are-coming-will-you-be-ready/'>Overtime Changes are Coming: Will You be Ready?</a> <span class="rss-date">December 15, 2015</span> <cite>WAKEUP CALL</cite></li><li><a class='rsswidget' href='http://hospitalityriskupdate.com/2015/12/11/wa-hotel-fined-nearly-100000-for-worker-safety-and-health-violations/'>WA Hotel Fined Nearly $100,000 for Worker Safety and Health Violations</a> <span class="rss-date">December 11, 2015</span> <cite>WAKEUP CALL</cite></li><li><a class='rsswidget' href='http://hospitalityriskupdate.com/2015/12/09/eeocs-2015-guidance-on-pregnancy-accommodation-and-leave/'>EEOC’s 2015 guidance on pregnancy accommodation and leave</a> <span class="rss-date">December 10, 2015</span> <cite>WAKEUP CALL</cite></li></ul></div><div class="one_third tt-column"><h3>CONNECT WITH US</h3>

                        <ul class="social_icons tt_vector_social_icons tt_no_social_title tt_image_social_icons">
                            <li><a href="https://www.wakeupcall.net/wp/feed/" class="rss" title="RSS Feed">RSS</a></li>



                            <li><a href="https://twitter.com/TomRiskman" class="twitter" title="Twitter" target="_blank">Twitter</a></li>
                            <li><a href="https://www.linkedin.com/company/wakeup-call---hospitality-risk-management-online" class="linkedin" title="LinkedIn" target="_blank">LinkedIn</a></li>

                        </ul>

                        <h3>TALK TO US</h3>			<div class="textwidget"><ul class="tt-business-contact">
                                <li><a href="tel://866-675-3909" class="tt-biz-phone">866-675-3909</a></li>
                            </ul>
                        </div>
                        <h3>EMAIL US</h3>			<div class="textwidget"><ul class="tt-business-contact">
                                <li><a href="mailto:info@wakeupcall.net" class="tt-biz-email">info@wakeupcall.net</a></li>
                            </ul>
                        </div>
                    </div><div class="one_third_last tt-column"><h3>Sign Up for Weekly Updates</h3>			<div class="textwidget"><p><iframe src="https://www.wakeupcall.net/wp/subscribe.html" /></iframe></p>
                        </div>
                    </div>                </div><!-- END footer-content -->

            </div><!-- END footer-overlay -->

            <div id="footer_bottom">
                <div class="info">
                    <div id="foot_left">&nbsp;                    			<div class="textwidget"><p>Copyright &copy; WakeUp Call. All rights reserved.</p>
                            <script src="/js/login-widget.js"></script>
                            <style>
                                @import url('/css/login-widget.css');
                            </style>
                        </div>

                    </div><!-- END foot_left -->

                    <div id="foot_right">
                        <ul>
                            <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a href="https://www.wakeupcall.net/wp/about/">About</a></li>
                            <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38"><a href="https://www.wakeupcall.net/wp/contact-us/">Contact Us</a></li>
                            <li id="menu-item-429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429"><a href="https://www.wakeupcall.net/wp/privacy/">Privacy</a></li>
                            <li id="menu-item-435" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-435"><a href="https://www.wakeupcall.net/wp/terms/">Terms</a></li>
                        </ul>

                    </div><!-- END foot_right -->
                </div><!-- END info -->
            </div><!-- END footer_bottom -->
        </footer><!-- END footer -->

    </div><!-- END wrapper -->
</div><!-- END tt-layout -->
<script type='text/javascript'>
    /* <![CDATA[ */
    var php_data = {"mobile_menu_text":"Main Menu","mobile_sub_menu_text":"More in this section...","karma_jquery_slideshowSpeed":"8000","karma_jquery_pause_hover":"false","karma_jquery_randomize":"false","karma_jquery_directionNav":"true","karma_jquery_animation_effect":"fade","karma_jquery_animationSpeed":"600","testimonial_slideshowSpeed":"8000","testimonial_pause_hover":"false","testimonial_randomize":"false","testimonial_directionNav":"true","testimonial_animation_effect":"fade","testimonial_animationSpeed":"600","ubermenu_active":"false","sticky_sidebar":"true"};
    /* ]]> */
</script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/custom-main.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/superfish.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/retina.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/jquery.flexslider.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/jquery.fitvids.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/jquery.isotope.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/ui/core.min.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/ui/widget.min.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/ui/tabs.min.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/jquery/ui/accordion.min.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/themes/Karma/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-includes/js/comment-reply.js'></script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl":"https:\/\/www.wakeupcall.net\/wp\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
    /* ]]> */
</script>
<script type='text/javascript' src='https://www.wakeupcall.net/wp/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>

<a href="#0" class="karma-scroll-top"><i class="fa fa-chevron-up"></i></a>
</body>
</html>