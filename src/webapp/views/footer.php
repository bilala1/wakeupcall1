<div id="footer">
    <ul class="footerNav">
        <li><a href="/terms.php">Terms</a></li>
        <li><a href="/privacy.php">Privacy</a></li>
        <li><a href="/about.php">About Us</a></li>
        <li><a href="signup.php">Our Advantages</a></li>
		<!--        <li><a href="testimonials.php">Testimonials</a></li>-->
        <li><a href="/contact.php">Contact Us</a></li>
    </ul>
    <p class="right" style="margin-right:10px" id="copyright">Copyright &copy;2010-<?= date('Y');?> WAKEUP CALL, Inc.</p>
    <br class="clear" />
    <div class="seal"><img src="/images/RapidSSL_SEAL-90x50.gif" alt="Rapid SSL Seal" /></div>
    <!-- (c) 2005, 2011. Authorize.Net is a registered trademark of CyberSource Corporation --> 
    <div class="AuthorizeNetSeal seal" style="margin:10px"><script type="text/javascript" language="javascript">var ANS_customer_id="7005fde2-5c45-4ea1-b04f-1f089f24ee32";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">E-Commerce Solutions</a></div>
    <div class="seal" style="margin:15px 5px"><script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?code=fc1b15a958054c4cae8aae481eaaf647"></script></div>
</div>
<br class="clear" />
</div>
</div> <!--wrapper-->
<script type="text/javascript">
<? if (DEPLOYMENT == 'production'): ?>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-23979203-1']);
		_gaq.push(['_trackPageview']);

		(function(){
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
<? endif ?>
</script>
</body>
</html>