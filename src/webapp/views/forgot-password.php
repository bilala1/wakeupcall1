<? ini_set('display_errors','1');

include VIEWS . '/header.php'?>
<div id="content">
    <div id="leftcol" class="w230 left">
    	<img src="/images/img-terms.jpg" />
    </div>
    
    <!--  big-rightcol -->
    <div  class="w720 left" style="margin-left:30px;">
        <? include VIEWS . '/notice.php' ?>
    	<!-- middlecol -->
    	<div class="w470">
            <? if(!$notice): ?>
            <h1 class="headline">Recover Password</h1>	
            
            <form  action="forgot-password.php" method="post" class="form" style="width:350px">
              <label style="float:left">Email:</label>
              <input type="text" class="text" name="members_email" id="members_email" />
                <? if($errors['members_email']): ?>
                <span style="color:red"><?= $errors['members_email'] ?></span>
                <? endif ?>
                <br class="clear" />
                <input type="submit" value="Recover" class="btn right" style="margin-right:20px" /><br class="clear"/>
            </form>
            <? endif; ?>
        </div>
        <br clear="all" />   
</div>
    <br clear="all" />     

<? include VIEWS . '/footer.php' ?>
