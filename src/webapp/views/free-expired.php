<?php  include VIEWS. '/header.php' ?>

<div id="content" style="position: relative">
    <img src="/images/register-header.jpg" alt="" id="top-image" />

    <? include 'notice.php' ?>
    <div class="w540 left" id="leftcol">
        <p>Your free trial has ended.  To continue using this service, please register for a full membership <a href="<?= HTTPS_FULLURL ?>/full-membership.php">here</a>.</p>
    </div>
</div>

<? include  VIEWS. '/footer.php'; ?>
