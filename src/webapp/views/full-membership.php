<?
$page_title = 'Sign up for WAKEUP CALL Risk Management Tools';
$page_description = 'Sign up for our online risk management tools - includes: hospitality and insurance resources and training, HR services, claim management, employment law hotline, SDS library, hospitality industry news, special offers and more....';

include VIEWS . '/header.php';
?>
<style type="text/css">
    .heading {
        background: #999999;
        color: #fff;
        font-size: 17px;
        padding: 5px 15px;
        margin: 0;
    }

    .tab {
        width: 600px;
        position: absolute;
        top: 240px;
        background: #fff;
        display: none;
    }

    .tabContent {
        padding: 15px;
        border: 1px solid #999999;
        height: 330px;
        overflow-y: auto;
    }

    #tab-links li {
        border: 1px solid #fff;
        border-left: none;
    }

    #tab-links li.tab-active {
        border: 1px solid #999999;
        border-left: none;
    }

    .tab .close {
        background: url(/images/dialog-close.png) no-repeat;
        width: 24px;
        height: 24px;
        display: block;
        cursor: pointer;
        top: -5px;
        right: -4px;
        position: absolute;
    }

    .green {
        color: #4e8e4d
    }

    .checkmark {
        padding: 0;
        margin: 0 0 10px 0;
    }

    .checkmark li {
        list-style: none;
        background: url(/images/checkmark-little.jpg) no-repeat 0 3px;
        padding: 3px 18px;
    }

    .big-btn {
        width: 80%;
        margin: 0 auto;
    }

    .big {
        font-size: 25px;
        color: #D31145;
        display: block;
        margin-top: 10px;
    }

    strong {
        color: #333
    }

    .long-txt {
        width: 55%
    }

    h3.headline {
        font-size: 18px;
        line-height: 20px;
        font-weight: bold;
        margin: 0;
    }

    h4 {
        color: #333;
        font-size: 14px
    }

    #form .form-field label {
        margin-bottom: 0;
        width: 35%;
        float: none;
        display: inline-block;
    }

    #form .form-field * {
        vertical-align: top;
    }

    #form input {
        float: none;
        display: inline;
    }

    .form-field {
        margin-bottom: 10px;
    }

    #form .form-error {
        margin: 0;
        margin-left: 39%;
    }
</style>

<div id="content" style="position: relative">

<div id="register-form">
    <div style=" padding: 10px">
        <h3 class="headline">Full Membership Registration</h3>

        <p>To begin using WAKEUP CALL,<br/>
            please enter your payment information below.</p>
        <? if ($errors): ?>
        <p class="errors">Please correct the errors below (in red)</p>
        <? endif ?>
    </div>
    <form name="fullmembership" method="post" id="form" style="margin-right:0; border:1px solid #ccc">
        <input type="hidden" name="members_id" value="<?= $_REQUEST['members_id'] ?>"/>
        <? if ($errors['payment_error']): ?>
        <p class="error"><?= $errors['payment_error'] ?></p>
        <? endif ?>
        <div class="form-field">
            <label>Company Name:</label><?= $member['corporations_name'] ? : $member['licensed_locations_name'] ?>
        </div>
        <div class="form-field">
            <label>Annual
                Membership:</label><?= money_format('%n', YEARLY_COST) . ' USD' . ($member['corporations_id'] ? ' (per location)' : '') ?>
        </div>
        <div class="form-field">
            <label>Annual Billing Date:</label><?= date('F jS') ?>
        </div>

        <p style="text-align:center"><strong>Billing Information</strong></p>

        <div class="form-field">
            <div style="float:left; width:35%; margin-right:10px;">
                <?= html::checkbox(1, 'same_address', 0, array('style' => 'float:right; margin-top:5px')) ?>
            </div>
            <label for="same_address" style="width:55%; clear:right; text-align:left">Same as registered
                address?</label>
        </div>

        <?= html::textfield('<span class="imp">*</span> Address:', $_REQUEST, 'members_billing_addr1', array('class' => 'text', 'maxlength' => 150), $errors) ?>
        <?= html::textfield('Suite, PO Box:', $_REQUEST, 'members_billing_addr2', array('class' => 'text', 'maxlength' => 150), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> City:', $_REQUEST, 'members_billing_city', array('class' => 'text', 'maxlength' => 100), $errors) ?>
        <?= html::selectfield('<span class="imp">*</span> State:', html::$us_states, 'members_billing_state', $_REQUEST) ?>
        <?= html::textfield('<span class="imp">*</span> Zipcode:', $_REQUEST, 'members_billing_zip', array('size' => '5', 'class' => 'text', 'maxlength' => 25), $errors) ?>
        <br/>

        <p style="text-align:center"><strong>Account Information</strong></p>
        <?= html::selectfield('<span class="imp">*</span> Card Type:', array('mastercard' => 'Mastercard', 'visa' => 'Visa', 'amex' => 'American Express'), 'members_card_type', $_REQUEST['members_card_type']) ?>
        <?= html::textfield('<span class="imp">*</span> Name on Card:', $_REQUEST, 'members_card_name', array('class' => 'text', 'maxlength' => 255), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> Card Number:', $_REQUEST, 'members_card_num', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 16), $errors); ?>

        <div class="form-field">
            <label><span class="imp">*</span> Card Expire:</label>
            <?= html::select($card_months, 'members_card_expire_month', $_REQUEST) ?>
            <?= html::select($card_years, 'members_card_expire_year', $_REQUEST) ?>
        </div>
        <?= html::textfield('<span class="imp">*</span> Security Code:', '', 'security_code', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 4), $errors); ?>

        <?= html::textfield('Discount Code:', $_REQUEST, 'discount_code', array('class' => 'text'), $errors) ?>

        <div class="form-field" id="discount-wrapper" <?= empty($discount) ? 'style="display:none;"' : '' ?>>
            <label>Discount:</label><strong id="discount-field"><?= !empty($discount) ? $discount : '' ?></strong>
        </div>

        <?= html::textfield('Group Code:', $_REQUEST, 'franchises_code', array('class' => 'text'), $errors) ?>

        <div class="form-field" id="franchise-discount-wrapper" <?= empty($franchise_discount_string) ? 'style="display:none;"' : '' ?>>
            <label>Group Discount:</label><strong
                id="franchise-discount-field"><?= !empty($franchise_discount_string) ? $franchise_discount_string : '' ?></strong>
        </div>

        <div class="form-field">
            <label></label><button class="btn" id="discount_code_btn">Update Total</button>
        </div>

        <div class="form-field">
            <label>Total Cost:</label><strong id="total-cost"><?= !empty($franchise_cost_string) ? $franchise_cost_string : $total_cost_string ?></strong>
        </div>
        <p style="text-align:center">You will be billed when you click Join.</p>
        <input class="btn" style="display:block; margin:0 auto" type="submit" value="Join"/>
    </form>
</div>

<br class="clear"/>
</div>


<script type="text/javascript">

var member = <?= json_encode($member) ?>;

window.addEvent('domready', function (evt) {
    $('same_address').addEvent('click', function () {
        if (this.checked) {
            $('members_billing_addr1').set('value', member.address);
            $('members_billing_city').set('value', member.city);
            $('members_billing_state').set('value', member.state);
            $('members_billing_zip').set('value', member.zip);
            $('members_card_name').set('value', member.members_firstname + ' ' + member.members_lastname);
        }
    });

    //$('discount_code').addEvent('blur', discount);
    $('discount_code_btn').addEvent('click', discount).fireEvent('click');
});

function discount(e) {
    if (typeof e != 'undefined') {
        e.stop();
    }

    var franchiseCode = $('franchises_code').get('value'),
        discountCode = $('discount_code').get('value');

    if (!franchiseCode && !discountCode) {
        $('total-cost').set('html', "<?= $total_cost_string ?>");
        $('discount-wrapper').setStyle('display', 'none');
        $('discount_code').getSiblings('.form-error').setStyle('display', 'none');
        $('franchise-discount-wrapper').setStyle('display', 'none');
        $('franchises_code').getSiblings('.form-error').setStyle('display', 'none');
        return;
    }

    var request = new Request.JSON({
        url:'full-membership.php',
        method:'get',
        onSuccess:function (json, text) {
            var coupon_discount = json.discount_code_discount;
            var coupon_total = json.discount_code_total;

            var franchise_discount = json.franchise_discount;
            var franchise_total = json.franchise_total;

            var error_container;

            // Update coupon fields
            $('discount_code').getSiblings('.form-error').destroy();
            if(discountCode) {
                if(coupon_total < 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('discount_code').getParent());
                    error_container.set('html', 'This discount code is invalid');
                    error_container.setStyle('display', 'block');
                    $('discount-wrapper').setStyle('display', 'none');
                } else {
                    $('discount-wrapper').setStyle('display', 'block');
                    $('discount-field').set('html', coupon_discount);
                }
            } else {
                $('discount-wrapper').setStyle('display', 'none');
            }

            // Update franchise fields
            $('franchises_code').getSiblings('.form-error').destroy();
            if(franchiseCode) {
                if(franchise_total < 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('franchises_code').getParent());
                    error_container.set('html', 'This group code is invalid');
                    error_container.setStyle('display', 'block');
                    $('franchise-discount-wrapper').setStyle('display', 'none');
                } else {
                    $('franchise-discount-wrapper').setStyle('display', 'block');
                    $('franchise-discount-field').set('html', franchise_discount);
                }
            } else {
                $('franchise-discount-wrapper').setStyle('display', 'none');
            }

            $('total-cost').set('html', json.total);

            if( franchise_total >= 0 && coupon_total >= 0)
            {
                if(franchise_total === coupon_total) {
                    alert('The savings from the discount code and group code are the same.');
                } else if(franchise_total < coupon_total) {
                    alert('The group code offers greater savings than the discount code.  That discount will be applied instead of the discount code.');
                } else {
                    alert('The discount code offers greater savings than the group code.  That discount will be applied instead of the group code discount.');
                }
            }
        },
        onError:function (text) {
            var error_container;

            // Update coupon fields
            if(discountCode) {
                error_container = $('discount_code').getSiblings('.form-error');
                if (error_container.length == 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('discount_code').getParent());
                }
                error_container.set('html', 'An error occurred checking discount code');
                error_container.setStyle('display', 'block');
                $('discount-wrapper').setStyle('display', 'none');
            }

            // Update franchise fields
            if(franchiseCode) {
                error_container = $('franchises_code').getSiblings('.form-error');
                if (error_container.length == 0) {
                    error_container = new Element('p', {'class':'error form-error'});
                    error_container.inject($('discount_code').getParent());
                }
                error_container.set('html', 'An error occurred checking group code');
                error_container.setStyle('display', 'block');
                $('discount-wrapper').setStyle('display', 'none');
            }
        }
    }).get({ajax:'franchise-and-coupon-pricing', discount_code:$('discount_code').get('value'), franchise_code:$('franchises_code').get('value'), total:'<?=$total_cost?>'});
}


function updateTotalWithDiscount(discount, total) {
    $('discount-field').set('html', discount);
    $('total-cost').set('html', total);
    $('discount-wrapper').setStyle('display', 'block');
    var error_container = $('discount_code').getSiblings('.form-error');
    if (error_container.length != 0) {
        error_container.setStyle('display', 'none');
    }
}


window.addEvent('domready', function () {
    $$('#tab-links a').addEvent('click', function (e) {
        e.stop();

        hide_popups();

        $$(this).getParent().addClass('tab-active');
        $$('#' + $(this).get('data-popid')).setStyle('display', 'block');
    });

    document.addEvent('click', hide_popups);
    $$('.tab .close').addEvent('click', hide_popups);
    $$('.tab').addEvent('click', function (e) {
        e.stop();
    });
});

function hide_popups() {
    $$('.tab').setStyle('display', 'none');
    $$('#tab-links li').removeClass('tab-active');
}
</script>
<? include  VIEWS . '/footer.php'; ?>