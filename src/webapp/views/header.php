<?php
$browser = $_SERVER['HTTP_USER_AGENT'];
//echo $browser;exit;
if(stristr($browser,'MSIE 6.0')) {
    echo '<img src="'. FULLURL . '/images/logo.jpg" alt="'.SITE_NAME.'" /><br />';
    echo "We're sorry but your browser is not supported. Please upgrade your browser to a newer version to use ".SITE_NAME;
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?= $page_title ?: SITE_NAME ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="<?= $page_description ?: 'Online risk management tools, training and resources for hospitality and insurance industry - includes: HR services, claim management, employment law hotline' ?>"/>
<link rel="stylesheet" type="text/css" href="/css/public-pages.css?v=20180618"/>
<link type="text/css" rel="stylesheet" href="/css/multiBox.css" />
<link href="/css/MooDialog.css" rel="stylesheet" type="text/css" />
<link href='//fonts.googleapis.com/css?family=Kristi' rel='stylesheet' type='text/css' />
<!--[if IE 7]>
<style type="text/css">
#mainNav{
    border-radius:7px;
    behavior: url(/js/pie-css/PIE.php);
}
</style>
<![endif]-->
<script type="text/javascript" src="/js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="/js/mootools-more-1.3.2.1.js"></script>
<script type="text/javascript" src="/js/multiBox.js"></script>
<script type="text/javascript" src="/js/overlay.js"></script>
<script type="text/javascript" src="/js/MooDialog.js"></script>
<script type="text/javascript" src="/js/MooDialog.Confirm.js"></script>
<script type="text/javascript" src="/js/MooDialog.Request.js"></script>
<script type="text/javascript" src="/js/swfobject/swfobject.js?v=1"></script>
<script src="/AC_RunActiveContent.js" type="text/javascript"></script>

<script type="text/javascript">
//<![CDATA[
var BASEURL = '<?= BASEURL ?>';
var FULLURL = '<?= FULLURL ?>';
var initMultiBox;
window.addEvent('domready', function(){
    //call multiBox
    initMultiBox = new multiBox({
        mbClass: '.mb',//class you need to add links that you want to trigger multiBox with (remember and update CSS files)
        container: $(document.body),//where to inject multiBox
        descClassName: 'multiBoxDesc',//the class name of the description divs
        path: '/Files/',//path to mp3 and flv players
        //useOverlay: true,//use a semi-transparent background. default: false;
        maxSize: {w:600, h:400},//max dimensions (width,height) - set to null to disable resizing
        addDownload: false,//do you want the files to be downloadable?
        //addRollover: true,//add rollover fade to each multibox link
        //addOverlayIcon: true,//adds overlay icons to images within multibox links
        addChain: true,//cycle through all images fading them out then in
        recalcTop: true,//subtract the height of controls panel from top position
        addTips: true,//adds MooTools built in 'Tips' class to each element (see: http://mootools.net/docs/Plugins/Tips)
        autoOpen: 0//to auto open a multiBox element on page load change to (1, 2, or 3 etc)
    });
});
//]]>
</script>
<link rel="icon" type="image/ico"  href="/images/favicon.ico"  />
</head>

<body>

<div id="wrapper" class="shadow">
	<div id="header">
    	<a id="header_logo" href="/"><img src="<?=empty($_SESSION['custom_logo_directory']) || empty($_SESSION['custom_logo_file_name']) ? '/images/logo.jpg' : $_SESSION['custom_logo_directory'] . "/" . html::filter($_SESSION['custom_logo_file_name']) .".org.jpg" ?>" alt="WakeUP Call" id="logo"/></a>

<? if(ActiveMemberInfo::GetMemberId()): //if member is logged in ?>
    <?php include 'members/_navigation.php' ?>
<? else: //if not a member, menu should not link to actual pages ?>
            <nav>
                <ul>
                    <li><a href="">Dashboard</a></li>
                    <li><a href="">Files</a></li>
                    <li><a href="">Tracking</a></li>
                    <li><a href="">Employee Mgmt</a></li>
                    <li><a href="">Training</a></li>
                    <li><a href="">Forums</a></li>
                    <li><a href="">Resources</a></li>
                    <li><a href="">News</a></li>
                    <li><a href="">Support</a></li>
                    <li><a href="">My Account</a></li>
                </ul>
            </nav>
            <script type="text/javascript">
			//<![CDATA[
                $$('nav').getElements('a')[0].addEvent('click',function(){
                    return false;
                });

                window.addEvent('domready', function(){
                    $$('nav').getElements('a')[0].addEvent('click', function(){
                        new Element('<div>',{html: '<br /><h3 style="text-align:center">Please register to have full access to this feature.<br /><a href="signup.php" class="imp"><b>Register or Join Now!</b></a></h3>'}).MooDialog({size:{width:330}});
                    });
                });
			//]]>
            </script>
<? endif; ?>

    </div>
