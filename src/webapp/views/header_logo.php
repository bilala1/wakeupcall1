<?php
    if(!isset($franchise) || empty($franchise)){
        return "/images/logo.jpg";
    }
    else{
        $db = mysqli_db::init();


        $photo = $db->fetch_one('
            SELECT p.albums_photos_id, p.albums_photo_filename, a.albums_id, a.albums, a.albums_root_folder
            FROM albums_photos AS a
            WHERE a.albums_photos_id = ?
            LEFT JOIN albums as a ON a.albums_id = p.join_albums_id', array($franchise['join_albums_photos_id']));

        return $photo['albums_root_folder'].'/'. $photo['albums_photo_filename'] ;
    }
?>

