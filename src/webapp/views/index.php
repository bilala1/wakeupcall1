
<?php

//require_once('under_maintenance.php');

$browser = $_SERVER['HTTP_USER_AGENT']; 

if(stristr($browser,'MSIE 6.0')) {
    echo '<img src="'. FULLURL . '/images/logo.jpg" alt="WAKEUP CALL" /><br /><br />';
    echo "We're sorry but your browser is not supported.  Please upgrade your browser to use WAKEUP CALL.";
    exit;
}

if(MAINTENANCE_MODE == 'ON')
{
    header('Location: '.HTTP_FULLURL.'/under_maintenance.php', TRUE, 302);
    exit;
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Online risk management tools, training and resources for hospitality and insurance industry - includes: HR services, claim management, employment law hotline"/>
<link rel="icon" type="image/ico"  href="/images/favicon.ico"  />
<link rel="stylesheet" type="text/css" href="/css/public-pages.css?v=20180618"/>
<link type="text/css" rel="stylesheet" href="/css/multiBox.css" />
<!--[if lte IE 6]>
    <link type="text/css" rel="stylesheet" href="/css/multiBoxIE6.css" />
<![endif]-->
<?/*<script type="text/javascript" src="/js/mootools-1.2.4-core-yc.js"></script>*/?>
<script type="text/javascript" src="/js/mootools-core-1.3.1-full-compat-yc.js"></script>
<?/*<script type="text/javascript" src="/js/mootools-1.2.4.4-more.js"></script>*/?>
<script type="text/javascript" src="/js/mootools-more-1.3.2.1.js"></script>
<script type="text/javascript" src="/js/multiBox.js"></script>
<script type="text/javascript" src="/js/overlay.js"></script>
<script type="text/javascript" src="/js/login.js?v=2"></script>
<script type="text/javascript" src="/js/swfobject/src/swfobject.js?v=1"></script>
<script type="text/javascript" src="AC_RunActiveContent.js"></script>
<script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>

<script type="text/javascript">
var BASEURL = '<?= BASEURL ?>';
var FULLURL = '<?= FULLURL ?>';

</script>
<title>Hospitality and Insurance Risk Management Tools, Training and Resources - MT</title>
</head>

<body>
<div id="wrapper" class="shadow">
    <?php if(stristr('MSIE 6.', $_SERVER['HTTP_USER_AGENT'])): ?>
        <p>This browser is not supported.  Please upgrade your browser to use WAKEUP CALL.</p>
    <?php endif; ?>


	<div id="banner">



            <div style="float: right;">
                <div  style="margin-right:10px; height:100%">
                    <? if(strpos($_REQUEST['notice'], 'You have been logged') !== false): ?>
                    <div class="round-box">
                        <span class="error xsmall" id="mysignup_error" style="margin-bottom:5px"><?= $_REQUEST['notice'] ?: '' ?></span>
                    </div>
                    <? else: ?>
                    <span class="error xsmall" id="mysignup_error" style="margin-bottom:5px"><?= $_REQUEST['notice'] ?: '' ?></span>
                    <? endif ?>
                </div>

            </div>


    	<!--<img src="/images/banner.jpg" alt=""/>-->
       <!-- <a href="" id="mb1" class="mb blue-btn play" rel="[images]">PLAY VIDEO</a>-->
        <a href="/signup.php" class="mb blue-btn play">LEARN MORE</a>
        
        <div id="video">
            <?/*<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="480" height="270" id="FLVPlayer" align="middle">
                <param name="movie" value="FLVPlayer_Progressive.swf"/>
                    <param name="salign" value="lt" />
                    <param name="quality" value="high" />
                    <param name="scale" value="noscale" />
                    <param name="FlashVars" value="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" />
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="FLVPlayer_Progressive.swf" width="480" height="270">
                    <param name="movie" value="FLVPlayer_Progressive.swf"/>
                    <param name="salign" value="lt" />
                    <param name="quality" value="high" />
                    <param name="scale" value="noscale" />
                    <param name="FlashVars" value="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" />
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>*/?>
			<?/*<script type="text/javascript">
				AC_FL_RunContent( 
					'width','480',
					'height','270',
					'id','FLVPlayer',
					'src','FLVPlayer_Progressive',
					'flashvars','&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true',
					'quality','high',
					'scale','noscale',
                    'name','FLVPlayer',
					'salign','lt',
					'pluginspage','http://get.adobe.com/flashplayer/',
					'movie','FLVPlayer_Progressive'
					 ); //end AC code
            </script>
            <noscript>
                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" 
                codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="480" height="270" id="FLVPlayer">
                  <param name="movie" value="FLVPlayer_Progressive.swf" />
                  <param name="salign" value="lt" />
                  <param name="quality" value="high" />
                  <param name="scale" value="noscale" />
                  <param name="FlashVars" value="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" />
                  <embed src="FLVPlayer_Progressive.swf" flashvars="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" quality="high" scale="noscale" width="480" height="270" name="FLVPlayer" salign="LT" type="application/x-shockwave-flash" pluginspage="http://get.adobe.com/flashplayer/" />
                </object>
            </noscript>*/?>
			<? /*
            <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="480" height="270" id="FLVPlayer" align="middle">
                <param name="movie" value="FLVPlayer_Progressive.swf"/>
                    <param name="salign" value="lt" />
                    <param name="quality" value="high" />
                    <param name="scale" value="noscale" />
                    <param name="FlashVars" value="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" />
                <!--[if !IE]>-->
                <object id="fallback" type="application/x-shockwave-flash" data="FLVPlayer_Progressive.swf" width="480" height="270">
                    <param name="movie" value="FLVPlayer_Progressive.swf"/>
                    <param name="salign" value="lt" />
                    <param name="quality" value="high" />
                    <param name="scale" value="noscale" />
                    <param name="FlashVars" value="&MM_ComponentVersion=1&skinName=Halo_Skin_3&streamName=wcc&autoPlay=true&autoRewind=true" />
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash" style="position:absolute; left:350px; top:260px; color:red; text-decoration:none; text-align:center; font-weight:bold" target="_blank">
                        Please install Flash Player<br />to play the video:<br />
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
			*/ ?>
        </div>
        
    </div>
    
    <div id="content" style="padding-bottom:0; min-height:0">
        
    	<div class="w260 left" style="margin-right:10px; height:100%">
            <? if(strpos($_REQUEST['notice'], 'You have been logged') !== false): ?>
            <div class="round-box">
            	<span class="error xsmall" id="login_error" style="margin-bottom:5px"><?= $_REQUEST['notice'] ?: '' ?></span>
            </div>
            <? else: ?>
            <span class="error xsmall" id="login_error" style="margin-bottom:5px"><?= $_REQUEST['notice'] ?: '' ?></span>
            <? endif ?>
            <div class="border-box" style="background-position:0 -20px;height:100%">
                <form action="/members/login.php" method="post" class="left" id="memberlogin">
                   
                    <label for="username" style="text-align:left" class="normal">Email:</label><br />
                    <input type="text" name="login_email" value="<?= ($_COOKIE['wakeupcall']) ? $_COOKIE['wakeupcall']: '' ; ?>" id="login_email" class="text" style="width:120px; margin:0"/>
                    <label for="password" class="normal">Password:</label>
                    <input type="password" name="login_password" id="login_password" class="text" style="width:120px; margin:0"/>
                    <br class="clear" clear="all" />
                    <a href="/forgot-password.php" class="small block">Forgot Password</a>
                    <br class="clear" clear="all" />
                    <input type="checkbox" name="remember_me" id="remember_me" value="1" <?= ($_COOKIE['wakeupcall']) ? 'checked="checked"':'' ; ?> /> <label for="remember_me" style="float:none; display: inline; font-weight: normal">Remember Me</label>
                    <br class="clear" />
                    <input type="submit" value="Login" class="btn block" style="margin:10px 0 0 25px" />
                </form>
                <a href="#" id="login_keycard" style="cursor:pointer; margin-top:10px;"></a>
                <div id="keyCard" class="right" style="margin-top:20px;"><? /*
					<script language="JavaScript" type="text/javascript">
                        AC_FL_RunContent( 
                            'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0',
                            'width','140',
                            'height','80',
                            'align','middle',
                            'src','login',
                            'loop','false',
                            'quality','high',
                            'bgcolor','#ffffff',
                            'name','login',
                            'allowscriptaccess','sameDomain',
                            'allowfullscreen','false',
                            'pluginspage','http://www.adobe.com/go/getflashplayer',
                            'movie','login', 
                            'wmode', 'transparent'); //end AC code
                    </script>
                    <noscript>
                        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" 
                        codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" 
                        width="140" height="80"  align="top">
                        <param name="allowScriptAccess" value="sameDomain" />
                        <param name="allowFullScreen" value="false" />
                        <param name="movie" value="login.swf" />
                        <param name="quality" value="high" />
                        <param name="bgcolor" value="#ffffff" />
                        <param name="LOOP" value="false" />	
                        <param name="wmode" value="transparent" />
                        <embed src="login.swf" width="140" height="80" loop="false" align="middle" quality="high" bgcolor="#ffffff" name="main" 
                        allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" 
                        pluginspage="http://www.adobe.com/go/getflashplayer" wmode="transparent" />
                        </object>
                    </noscript> */ ?>
                </div>
                <br class="clear"/>
             </div>
        </div>
        
        <div class="w400 left">
        	<!--<img src="/images/home-logo.jpg" alt="WakeUp Call" />-->
            <p class="big big-letter" align="center">
            Hospitality <br/>
            Risk Management<br />
            Made Easier - <br />
            Online 24/7</p>
        </div>
        
        <div class="w260 right">
        	<div class="border-box" style="text-align:center; height:170px">

                <img src="/images/free-trial.png" class="block" style="margin-bottom:10px" />
                <a href="/signup-direct.php" class="green-btn center">LET'S GO</a>
            </div
            <div class="border-box" style="background-position:0 -20px;height:100%">
                <form action="/mysignup.php" method="get" class="left" id="mysignup">
                    <input type="text"  name="franchise_code" value="" id="franchise_code" class="text" style="width:100px; margin:0"/> &nbsp;&nbsp;
                    <input type="submit"  value="Group Code"  id="franchise_btn" style="width:120px;margin:0 0 0 0" />
                </form>

                <div class="error xsmall" align="left" id="code_error" style="padding: 2px;"><?= $_REQUEST['code_error'] ?: '' ?></div>
                <br class="clear"/>
            </div>
        </div>
        <br class="clear" style="height:1px" />
    <? include VIEWS . '/footer.php'; ?>