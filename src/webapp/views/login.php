<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login to <?= SITE_NAME ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description"
          content="Online risk management tools, training and resources for hospitality and insurance industry - includes: HR services, claim management, employment law hotline"/>
    <link rel="stylesheet" type="text/css" href="/css/login.css?v=20180618"/>
    <link type="text/css" rel="stylesheet" href="/css/font-awesome.min.css"/>

    <script>
        var BASEURL = '<?= BASEURL ?>';
        var FULLURL = '<?= FULLURL ?>';
    </script>
    <link rel="icon" type="image/ico" href="/images/favicon.ico"/>
</head>

<body>
<div class="login-form">
    <img src="images/logo.png" />
    <form action="/members/login.php" method="post" class="loginForm">

        <?php if (isset($_REQUEST['notice'])) { ?>
            <div class="notice" style="margin-bottom: 1em;"><?= $_REQUEST['notice'] ?: '' ?></div>
        <?php } ?>

        <input type="text" name="login_email" id="login_email" placeholder="USERNAME"
               value="<?= array_key_exists('wakeupcall', $_COOKIE) ? $_COOKIE['wakeupcall'] : '' ?>"/>
        <input type="password" name="login_password" id="login_password" class="text" placeholder="PASSWORD"/>

        <input type="submit" value="Login" class="btn-primary btn-ok" style="float:right;"/>

        <label for="remember_me" style="float:none; display: inline; font-weight: normal">
            <input type="checkbox"
                 name="remember_me"
                 id="remember_me"
                 value="1" <?= array_key_exists('wakeupcall', $_COOKIE) ? 'checked="checked"' : '' ?> />
                Remember Me</label>

        <a href="/forgot-password.php">Forgot Your Password?</a>
    </form>
</div>
</body>
</html>