<?  include VIEWS. '/header.php'?>
<style type="text/css">
strong{ color: #333}
.long-txt{ width: 55%}
h3.headline{ font-size:18px; line-height:20px; font-weight: bold; margin:0;}
h4{ color: #333; font-size: 14px}

#form .form-field label{
	margin-bottom:0;
	width:35%;
    float:none;
    display:inline-block;
}
#form .form-field *{
    vertical-align:top;
}
#form input{
    float:none;
    display:inline;
}


.form-field{
    margin-bottom:10px;
}

#form .form-error{
    margin:0;
    margin-left:39%;
}
</style>

<div id="content" style="position: relative">
	<img src="/images/register-header.jpg" alt="" id="top-image" />

    <? include 'notice.php' ?>
    <div class="w540 left" id="leftcol">
        <? include '_signup-text.php' ?>
    </div>
    <div class="right" style="width:310px; height:auto;">
        <h3 class="headline">Full Membership Payment</h3>
        <p>To continue using WAKEUP CALL,<br />
        please enter your payment information below.</p>
        <? if($errors): ?>
        <p class="errors">Please correct the errors below (in red)</p>
        <? endif ?>
        <form name="fullmembership" method="post" id="form" style="margin-right:0; border:1px solid #ccc">
            
        <p style="text-align:center"><strong>Billing Information</strong></p>  
        <div class="form-field">
            <label>Company Name:</label><?= $member['corporations_name'] ?: $member['hotels_name'] ?>
        </div>
        <div class="form-field">
            <label>Annual Billing Date:</label><?= date('F jS', strtotime($billing_date)) ?>
        </div>        
        
        <?= html::textfield('<span class="imp">*</span> Address:', $_REQUEST, 'members_billing_addr1', array('class' => 'text', 'maxlength' => 150), $errors) ?>
        <?= html::textfield('Suite, PO Box:', $_REQUEST, 'members_billing_addr2', array('class' => 'text', 'maxlength' => 150), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> City:', $_REQUEST, 'members_billing_city', array('class' => 'text', 'maxlength' => 100), $errors) ?>
        <?= html::selectfield('<span class="imp">*</span> State:', html::$us_states, 'members_billing_state', $_REQUEST) ?>
        <?= html::textfield('<span class="imp">*</span> Zipcode:', $_REQUEST, 'members_billing_zip', array('size' => '5', 'class' => 'text', 'maxlength' => 25), $errors) ?>
        <br />
        <p style="text-align:center"><strong>Account Information</strong></p>
        <?= html::selectfield('<span class="imp">*</span> Card Type:', array('mastercard' => 'Mastercard', 'visa' => 'Visa', 'amex' => 'American Express'), 'members_card_type', $_REQUEST['members_card_type']) ?>
        <?= html::textfield('<span class="imp">*</span> Name on Card:', $_REQUEST, 'members_card_name', array('class' => 'text', 'maxlength' => 255), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> Card Number:', $_REQUEST, 'members_card_num', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 16) ,$errors); ?>

        <div class="form-field">
            <label><span class="imp">*</span> Card Expire:</label>
            <?= html::select($card_months, 'members_card_expire_month', $_REQUEST) ?>
            <?= html::select($card_years, 'members_card_expire_year', $_REQUEST) ?>
        </div>
        <?= html::textfield('<span class="imp">*</span> Security Code:', '', 'security_code', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 4), $errors); ?>

            
            
        <div class="form-field">
            <? if(is_array($bills)): ?>
                <? foreach($bills as $bill): ?>
                    <label>Amount Due:</label><?= money_format('%n',$bill['bills_amount']) ?><br />
                <? endforeach; ?>
            <? endif; ?>
        </div>    
        <div class="form-field">
            <label>Total Due:</label><strong id="total-cost"><?= money_format('%n', $total_cost); ?></strong><br />
        </div>           
        
        <input class="btn" style="display:block; margin:0 auto" type="submit" value="Pay" />    
        </form>
    </div>
    <br class="clear" />
</div>            
<br class="clear" />
<? include  VIEWS. '/footer.php'; ?>            