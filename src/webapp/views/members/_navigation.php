<?php
// Determine the nav tree

$nav = array();

//Dashboard
if (UserPermissions::IsSiteFeatureEnabled('dashboard') &&
    UserPermissions::UserCanAccessScreen('dashboard')) {
    $nav[] = array(
        'pageName' => 'Dashboard',
        'url' => '/members/index.php'
    );
}

if (UserPermissions::IsSiteFeatureEnabled('files')) {
//Files
    $nav[] = array(
        'pageName' => 'Files',
        'subNav' => array(
            array(
                'pageName' => 'My Files',
                'url' => '/members/my-documents/index.php'
            ),
            array(
                'pageName' => 'Shared Files',
                'url' => '/members/my-documents/shareddocs.php'
            )
        )
    );
}

//Tracking
$hasCerts = UserPermissions::UserCanAccessScreen('certificates');
$hasContracts = UserPermissions::UserCanAccessScreen('contracts');
$hasClaims = UserPermissions::UserCanAccessScreen('claims');
$hasBusinessTracker = UserPermissions::UserCanAccessScreen('business_entities');
if ($hasCerts || $hasContracts || $hasClaims || $hasBusinessTracker) {
    $trackingNav = array(
        'pageName' => 'Tracking'
    );
    $subNav = array();

    if ($hasCerts) {
        $subNav[] = array(
            'pageName' => 'Certificates',
            'url' => '/members/my-documents/certificates/index.php'
        );
    }

    if ($hasClaims) {
        $subNav[] = array(
            'pageName' => 'Claims',
            'url' => '/members/claims/index.php'
        );
    }

    if ($hasContracts) {
        $subNav[] = array(
            'pageName' => 'Contracts',
            'url' => '/members/contracts/index.php'
        );
    }

    if ($hasBusinessTracker) {
        $subNav[] = array(
            'pageName' => 'Businesses',
            'text' => 'Businesses/Events',
            'url' => '/members/entities/business/index.php'
        );
    }
    $trackingNav['subNav'] = $subNav;
    $nav[] = $trackingNav;
}

//Hr/Elaw
$hasHr = UserPermissions::UserCanAccessScreen('hr');
$hasElaw = UserPermissions::UserCanAccessScreen('elaw');
if ($hasHr || $hasElaw) {
    $hrEeNav = array(
        'pageName' => 'Employee Mgmt'
    );
    $subNav = array();

    if (!$hasHr) {
        $hrEeNav['text'] = 'Employment Law';
    } else {
        $subNav[] = array(
            'pageName' => 'HR',
            'url' => '/members/hr/index.php'
        );
    }

    if (!$hasElaw) {
        $hrEeNav['text'] = 'HR';
    } else {
        $subNav[] = array(
            'pageName' => 'Employment Law',
            'url' => '/members/employmentlaw/terms.php'
        );
    }
    $hrEeNav['subNav'] = $subNav;
    $nav[] = $hrEeNav;
}

//Training/Webinars
$hasTraining = UserPermissions::UserCanAccessScreen('training');
$hasWebinars = UserPermissions::UserCanAccessScreen('webinars');
if ($hasTraining || $hasWebinars) {
    $trainingNav = array(
        'pageName' => 'Training');
    $subNav = array();

    if ($hasTraining) {
        $subNav[] = array(
            'pageName' => 'Training',
            'url' => '/members/webinars/training.php'
        );
    }

    if ($hasWebinars) {
        $subNav[] = array(
            'pageName' => 'Webinars',
            'url' => '/members/webinars/index.php'
        );
    }
    $trainingNav['subNav'] = $subNav;
    $nav[] = $trainingNav;
}

//Forums
$hasForum = UserPermissions::UserCanAccessScreen('forum');
$hasBulletin = ActiveMemberInfo::IsAccountMultiLocation();
if ($hasForum || $hasBulletin) {
    $forumNav = array(
        'pageName' => 'Forums'
    );
    $subNav = array();

    if ($hasForum) {
        $subNav[] = array(
            'pageName' => 'Message Forum',
            'url' => '/members/forums/view-forum.php?forums_id=1'
        );
    }

    if ($hasBulletin) {
        $subNav[] = array(
            'pageName' => 'Corporate Bulletin',
            'url' => '/members/forums/view-forum.php?forums_id=' . members::get_corporation_forum_id()
        );
    }
    $forumNav['subNav'] = $subNav;
    $nav[] = $forumNav;
}

//Resources
$hasLibrary = UserPermissions::IsSiteFeatureEnabled('doclibrary');
$hasSds = UserPermissions::IsSiteFeatureEnabled('sds');
$hasDefinitions = UserPermissions::IsSiteFeatureEnabled('definitions');
$hasServices = UserPermissions::IsSiteFeatureEnabled('services');
if ($hasLibrary || $hasSds || $hasDefinitions || $hasServices) {
    $resourceNav = array(
        'pageName' => 'Resources');
    $subNav = array();

    if ($hasLibrary) {
        $subNav[] = array(
            'pageName' => 'Document Library',
            'url' => '/members/documents/index.php'
        );
    }
    if ($hasSds) {
        $subNav[] = array(
            'pageName' => 'SDS Library',
            'url' => '/members/msds/index.php'
        );
    }
    if ($hasDefinitions) {
        $subNav[] = array(
            'pageName' => 'Definitions',
            'url' => '/members/definitions/index.php'
        );
    }
    if ($hasServices) {
        $subNav[] = array(
            'pageName' => 'Additional Services',
            'url' => '/members/services/index.php'
        );
    }
    $resourceNav['subNav'] = $subNav;
    $nav[] = $resourceNav;
}

//News
$hasSiteNews = UserPermissions::IsSiteFeatureEnabled('sitenews');
$hasHospitalityNews = UserPermissions::IsSiteFeatureEnabled('hospitalitynews');
$hasHrNews = UserPermissions::IsSiteFeatureEnabled('hrnews');
if ($hasSiteNews || $hasHospitalityNews || $hasHrNews) {
    $newsNav = array(
        'pageName' => 'News');
    $subNav = array();

    if ($hasSiteNews) {
        $subNav[] = array(
            'pageName' => 'WAKEUP CALL News',
            'url' => '/members/news/wakeupcall-news.php'
        );
    }
    if ($hasHospitalityNews) {
        $subNav[] = array(
            'pageName' => 'Hospitality News',
            'url' => '/members/news/rss-news.php'
        );
    }
    if ($hasHrNews) {
        $subNav[] = array(
            'pageName' => 'HR News',
            'url' => '/members/news/index.php'
        );
    }
    $newsNav['subNav'] = $subNav;
    $nav[] = $newsNav;
}

//Support
$hasContact = UserPermissions::IsSiteFeatureEnabled('contact');
$hasTutorials = UserPermissions::IsSiteFeatureEnabled('tutorials');
$hasFaqs = UserPermissions::IsSiteFeatureEnabled('faqs');
if ($hasContact || $hasTutorials || $hasFaqs) {
    $newsNav = array(
        'pageName' => 'Support');
    $subNav = array();

    if ($hasContact) {
        $subNav[] = array(
            'pageName' => 'Contact Us',
            'url' => '/members/support/contact.php'
        );
    }
    if ($hasTutorials) {
        $subNav[] = array(
            'pageName' => 'Tutorials',
            'url' => '/members/support/tutorials.php'
        );
    }
    if ($hasFaqs) {
        $subNav[] = array(
            'pageName' => 'FAQs',
            'url' => '/members/support/faqs.php'
        );
    }
    $newsNav['subNav'] = $subNav;
    $nav[] = $newsNav;
}

//My Account
$nav[] = array(
    'pageName' => 'My Account',
    'url' => '/members/account/account-details.php'
);

/*
 * My Account
Contact Us
Tutorials
FAQ's
 */
foreach ($nav as &$navPiece) {
    if ($page_name == $navPiece['pageName']) {
        $activeNav = $navPiece;
    }
    if (!array_key_exists('subNav', $navPiece)) {
        continue;
    }
    $subNav = $navPiece['subNav'];
    $navPiece['url'] = $subNav[0]['url'];
    if (count($subNav) == 1) {
        $navPiece['subNav'] = null;
        $navPiece['text'] = $subNav[0]['pageName'];
    }
}
?>
<nav>
    <ul>
        <?php foreach ($nav as $mainNav) {
        if ($page_name == $mainNav['pageName']) {
            $activeNav = $mainNav;
        }
        $itemText = array_key_exists('text', $mainNav) ? $mainNav['text'] : $mainNav['pageName'];
        ?>
        <li><a href="<?= $mainNav['url'] ?>"
               class="<?= $activeNav == $mainNav ? 'mainNav-active' : '' ?>
               <?= $mainNav['subNav'] ? 'has-subnav' : '' ?>"><?= $itemText ?></a>
            <?php if ($mainNav['subNav']) { ?>
                <ul class="allnav <?= $mainNav['pageName'] ?>-subNav <?= $page_name == $mainNav['pageName'] ? 'active' : 'popup' ?>">
                    <?php foreach ($mainNav['subNav'] as $subNav) {
                        ?>
                        <li><a href="<?= $subNav['url'] ?>"
                               class="<?= $sub_name == $subNav['pageName'] ? 'activesubpage' : '' ?>"><?= $subNav['pageName'] ?></a>
                        </li>
                    <?php } ?>
                </ul>
            <?php }
            } ?>
    </ul>
</nav>
