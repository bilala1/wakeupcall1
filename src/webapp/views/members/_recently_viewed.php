<style>
    .rightcol-box a {
        padding-bottom: 2px; /* breathing room for the background image */
    }
</style>
<div class="rightcol-box" style="border-bottom: none">
    <h3 class="red" id="recently_viewed">Recently Viewed <img src="/images/sort-down.png" /></h3>
    <ul id="recently_viewed_list">
        <? foreach(members::get_recently_viewed(10) as $document): ?>
        <li>
            <? if($document['downloads_type'] == 'file'): ?>
            <a target="_blank" href="/members/documents/download.php?documents_id=<?= $document['join_id'] ?>#<?= urlencode($document['filename']) ?>" title="<?= $document['filename'] ?>"><?= $document['name'] ?></a>
            <? elseif($document['downloads_type'] == 'certificate'): ?>
            <a target="_blank" href="/members/my-documents/certificates/edit.php?certificates_id=<?= $document['join_id'] ?>#<?= urlencode($document['filename']) ?>" title="<?= $document['filename'] ?>"><?= $document['name'] ?></a>
            <? elseif($document['downloads_type'] == 'msds'): ?>
            <a target="_blank" href="/members/msds/download.php?chemicals_id=<?= $document['join_id'] ?>#<?= urlencode($document['file']) ?>" title="<?= $document['name'] ?>"><?= $document['name'] ?></a>
            <? elseif($document['downloads_type'] == 'forum file'): ?>
            <a target="_blank" href="/members/forums/download.php?forums_posts_files_id=<?= $document['join_id'] ?>#<?= urlencode($document['file']) ?>" title="<?= $document['name'] ?>"><?= $document['name'] ?></a>
            <? elseif($document['downloads_type'] == 'contract'): ?>
            <a target="_blank" href="/members/contracts/download.php?contracts_files_id=<?= $document['join_id'] ?>#<?= urlencode($document['file']) ?>" title="<?= $document['name'] ?>"><?= $document['name'] ?></a>
            <? endif ?>
        </li>
        <? endforeach ?>
    </ul>
</div>
<script type="text/javascript">
 var myVerticalSlide = new Fx.Slide('recently_viewed_list');
window.addEvent('domready', function(){
   
    
    $('recently_viewed').addEvent('click', function(){
        var img = this.getElement('img');
        
        if(img.get('src').search('sort-up') != -1){
            img.set('src', img.get('src').replace('sort-up', 'sort-down'));
            myVerticalSlide.slideIn();
            //$('recently_viewed_list').setStyle('display','none');
        }else{
            img.set('src', img.get('src').replace('sort-down', 'sort-up'));
            myVerticalSlide.slideOut();
            //$('recently_viewed_list').setStyle('display','block')
        }
    });
});
</script>
