<?
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
	$tab_name		   = 'Account Details';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
   <!-- This is where you will find everything that has to do with your <?= SITE_NAME ?> account:  Locations, members, billing information, notification preferences, etc-->
    This is where you will find information that has to do with your <?= SITE_NAME ?>  account:  Locations, members, 
    billing information, notification preferences, etc. 
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <form action="/members/account/account-details.php" method="post" style="width:420px">
        <?= html::textfield('Email:', $member, 'members_email', array('maxlength' => 100), $errors) ?>
        <?= html::textfield('First Name:', $member, 'members_firstname', array('maxlength' => 100), $errors) ?>
        <?= html::textfield('Last Name:', $member, 'members_lastname', array('maxlength' => 100), $errors) ?>
        <?= html::textfield('Position/Title:', $member, 'members_title', array( 'maxlength' => 40), $errors) ?>        
        <?//= html::textfield('Email:', $member, 'members_email', array('maxlength' => 255), $errors) ?>
        <?= html::passwordfield('Change Password:', '', 'members_password', null, $errors) ?>
        <?= html::passwordfield('Confirm Password:', '', 'members_password2', null, $errors) ?>
        <?= html::textfield('Phone:', $member, 'members_phone', array('maxlength' => 20, 'class' => 'phone'), $errors) ?>
        <?= html::textfield('Fax:', $member, 'members_fax', array('maxlength' => 20, 'class' => 'phone'), $errors) ?>
        <? if(ActiveMemberInfo::IsAccountMultiLocation() && ActiveMemberInfo::_IsMainAccountAdmin()): ?>
        <br />
        <?= html::textfield('Corporation Name', $corporation, 'forums_name', array('maxlength' => 255), $errors) ?>
        <? endif ?>
        <div class="form-actions">
            <?= html::input('Save', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>
<script type="text/javascript">

if($('delete_account')){
    $('delete_account').addEvent('click', function(){
        if(!confirm('Are you sure you want to delete your account?')) { return false; }
    });
}

    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');       
    <? endif; ?>
window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_account');
    });
})    
</script>

<? include VIEWS. '/members/footer.php' ?>
