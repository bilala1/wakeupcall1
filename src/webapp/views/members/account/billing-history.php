<?php
    $page_title        = SITE_NAME;
    $page_name         = 'Billing History';
    $page_keywords     = '';
    $page_description  = '';
?>
<?php include VIEWS. '/members/header.php' ?>
<? include VIEWS. '/members/account/header.php' ?>

<div id="content">
	<h1>My Account</h1>
      <h3>Billing History</h3>
      <table width="50%">
      	<tr>
        	<th>Date</th>
            <th>Amount</th>
        </tr>
        <tr align="center">
        	<td>09/23/10</td>
            <td>$52.00</td>
        </tr>
        <tr align="center">
        	<td>10/23/10</td>
            <td>$52.00</td>
        </tr>
      </table>
</div>

<?php include VIEWS. '/members/footer.php' ?>
