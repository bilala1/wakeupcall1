<?php
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
    $tab_name          = 'Billing';
    $page_keywords     = '';
    $page_description  = '';
?>
<?php include VIEWS. '/members/header.php' ?>
<? if($errors): ?>
<br class="clear" />
<div class="notice">
    Please correct errors.
</div>
<? endif; ?>


<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">This is where you will find everything that has to do with your <?= SITE_NAME ?> account:  Locations, members, billing information, notification preferences, etc. </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <form action="/members/account/billing.php" method="post">

    <fieldset>
        <legend>Credit Card Information</legend>
        <?= html::textfield('Card Number',$member, 'members_card_num', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 16) ,$errors); ?>
        <?//=html::textfield('Card CVC','', 'members_card_cvc', array('class' => 'text', 'autocomplete' => 'off', 'maxlength' => 5) ,$errors); ?>

        <label>Card Expire</label>
        <?= html::select($card_months, 'members_card_expire_month', $member['members_card_expire_month']) ?>
        <?= html::select($card_years, 'members_card_expire_year', $member['members_card_expire_year']) ?><br /><br />

        <label>Card Type</label>
        <?= html::select(array(''=>'Please Select', 'MasterCard' => 'MasterCard', 'Visa' => 'Visa', 'American Express' => 'American Express',
                                'Discover' => 'Discover'), 'members_card_type', $member['members_card_type']) ?>
    </fieldset>

    <fieldset>
        <legend>Billing Information</legend>

        <?= html::textfield('Address 1',$member, 'members_billing_addr1',array('class' => 'text', 'maxlength' => 150),$errors) ?>

        <?= html::textfield('Address 2',$member, 'members_billing_addr2', array('class' => 'text', 'maxlength' => 150),$errors) ?>

        <?= html::textfield('City',$member, 'members_billing_city', array('class' => 'text', 'maxlength' => 100),$errors) ?>

        <label>State</label>
        <?= html::select(html::$us_states, 'members_billing_state', $member['members_billing_state']) ?><br />

        <?= html::textfield('Zip',$member, 'members_billing_zip', array('class' => 'text', 'maxlength' => 25),$errors) ?>
    </fieldset>

        <div class="form-actions">
            <?= html::input('Save Billing Information', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>

    </form>
    <br /><br />
    <h1>Billing History</h1>
    <table style="width:100%">
        <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Type</th>
            <th>Description</th>
            <th>Paid</th>
        </tr>
        <? foreach($bills as $bill): ?>
        <tr align="center">
            <td><?= date(DATE_FORMAT, strtotime($bill['bills_datetime'])) ?></td>
            <td><?= money_format('%n', $bill['bills_amount']) ?></td>
            <td><?= $bill['bills_type'] ?></td>
            <td><?= $bill['bills_description'] ?></td>
            <td><?= $bill['bills_paid'] ? 'Yes' : 'No' ?></td>
        </tr>
        <? endforeach ?>
    </table>
</div>
    <script type="text/javascript">
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
           $$('.page-desc').setStyle('display', 'none');
           $$('.info_text').set('html','Show Text Box');
        <? endif; ?>
        window.addEvent('load', function(){
            //infobox settings
            var webinarsRequest = new Request({
            url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
            method: 'get',
                onSuccess: function(responseText){
                    if(responseText == 'on'){
                        $$('.page-desc').reveal();
                        $$('.info_text').set('html','Hide Text Box');
                    }
                    else {
                        $$('.page-desc').dissolve();
                        $$('.info_text').set('html','Show Text Box');
                    }
                }
            });
            $$('.info_icon, .info_text').addEvent('click', function(event){
                if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                    var show = 'true';
                }
                else {
                    var show = 'false';
                }
                webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_account');
            });
        })
    </script>

<?php include VIEWS. '/members/footer.php' ?>
