<?php
	$page_title = SITE_NAME;
$page_name         = 'My Account';
$sub_name          = 'My Account';
$tab_name = 'Forum Signature';
$page_keywords = '';
$page_description = '';
?>
<?php include VIEWS . '/members/header.php' ?>

<div id="content">
		<span class="info_icon"></span>

		<p class="page-desc">
			This is where you edit your forum signature to use in the Message Forum section.
		</p>
		<span class="info_text">Hide Text Box</span>
		<br class="clear"/>
		<? include VIEWS . '/members/account/header.php' ?>

		<form action="" method="post">

			<?= html::textareafield('Signature', $member, 'members_forum_signature', array('style' => 'width: 70%; height: 100px'), $errors) ?><br />

			<div class="form-actions">
				<?= html::input('Save', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
			</div>

		</form>
</div>

<script type="text/javascript">
	<? /* open or collapse infobox? */ ?>
	<? if (!$infobox): ?>
	$$('.page-desc').setStyle('display', 'none');
	$$('.info_text').set('html', 'Show Text Box');
	<? endif; ?>

	window.addEvent('load', function() {
		//infobox settings
		var sigRequest = new Request({
			url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
			method: 'get',
			onSuccess: function(responseText) {
				if (responseText == 'on') {
					$$('.page-desc').reveal();
					$$('.info_text').set('html', 'Hide Text Box');
				}
				else {
					$$('.page-desc').dissolve();
					$$('.info_text').set('html', 'Show Text Box');
				}
			}
		});
		$$('.info_icon, .info_text').addEvent('click', function(event) {
			if ($$('.page-desc')[0].get('style').match(/display: none/gi)) {
				var show = 'true';
			}
			else {
				var show = 'false';
			}
			sigRequest.send('yn=' + show + '&type=members_settings_infobox_my_account');
		});
	})
</script>

<?php include VIEWS . '/members/footer.php' ?>
