<?
$page_title = SITE_NAME;
$page_name = 'My Wakeup Call';
$sub_name = 'account';
$tab_name = 'Account Details';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">

    <form id="form" method="post" action="/members/account/franchises/logoedit.php" enctype="multipart/form-data">
        <? if (isset($_REQUEST['albums_photos_id'])): ?>
        <input type="hidden" name="albums_photos_id" value="<?= $_REQUEST['albums_photos_id'] ?>"/>
        <? else: ?>
        <input type="hidden" name="join_albums_id" value="<?= $_REQUEST['albums_id'] ?>"/>
        <? endif ?>

        <input type="hidden" name="franchises_id" value="<?= $_REQUEST['franchises_id'] ?>"/>

        <? include(VIEWS . '/notice.php') ?>

        <fieldset class="alt1">
            <legend>Photo Information</legend>
            <table class="form" border="0">
                <? /*<tr>
                <td><label for="albums_photos_name">Album:</label></td>
                <td><?= html::select($album_list, 'join_albums_id', $join_albums_id) ?> </td>
            </tr>*/ ?>
                <tr>
                    <td><label for="forums_name">Photo Name:</label></td>
                    <td><input type="text" id="albums_photos_name" name="albums_photos_name"
                               value="<?= html::filter($photo['albums_photos_name']) ?>" size="30" maxlength="100"/>
                    </td>
                </tr>
                <tr>
                    <td><label for="forums_name">Photo:</label></td>
                    <td>
                        <img src="<?=empty($album['albums_root_folder']) ? '/data/albums/' : $album['albums_root_folder'] . "/"?>/<?= html::filter($photo['albums_photos_filename']) ?>.org.jpg"
                             alt="photo"/><br/>
                        <input type="file" name="albums_photos_filename">
                        (recommended size: 285px x 74px)
                    </td>
                </tr>
            </table>

            <?= html::submit('Save Photo', array('class' => 'submit center')) ?>
        </fieldset>
    </form>

</div>
</div>



<br class="clear"/>

<? include VIEWS . '/members/footer.php' ?>
