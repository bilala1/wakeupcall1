<style>
    #account-tab a, #forum-tab a {
        padding-top: 0;
        height: 36px;
    }
    .tab-title {
        width: 14%;
    }
</style>

<div style="height: 36px; border-bottom: 1px solid #aaa;margin-right: 12px;">
    <h3 id="account-tab" class="tab-title<?= $tab_name == 'Account Details' ? ' activetab' : '' ?>"><a href="/members/account/account-details.php">Account<br>Details</a></h3>
    <? if(UserPermissions::UserCanAccessScreen('editLocation') && members::get_members_status(ActiveMemberInfo::GetMemberId()) != 'free'): ?>
        <? if(ActiveMemberInfo::IsUserMultiLocation()): ?>
            <h3 class="tab-title<?= $tab_name == 'Sub-Accounts' ? ' activetab' : '' ?>"><a href="/members/account/hotels/list.php">Locations</a></h3>
        <? else: ?>
            <h3 class="tab-title<?= $tab_name == 'Sub-Accounts' ? ' activetab' : '' ?>"><a href="/members/account/hotels/edit.php?licensed_locations_id=<?= ActiveMemberInfo::SingleLocationUserLocationId() ?>">Location</a></h3>
        <? endif; ?>
    <? endif ?>
    <? if(UserPermissions::UserCanAccessScreen('manageUsers')): ?>
        <h3 class="tab-title<?= $tab_name == 'Users' ? ' activetab' : '' ?>"><a href="/members/account/hotels/members/list.php">Users</a></h3>
    <? endif ?>
    <? if(ActiveMemberInfo::_IsMainAccountAdmin() && !ActiveMemberInfo::IsChildAccount()): //task #3469  ?>
        <h3 class="tab-title<?= $tab_name == 'Billing' ? ' activetab' : '' ?>"><a href="<?= HTTPS_FULLURL ?>/members/account/billing.php">Billing</a></h3>
    <? endif; ?>
    <h3 class="tab-title<?= $tab_name == 'Notification Settings' ? ' activetab' : '' ?>"><a href="/members/account/notifications.php">Notifications</a></h3>
	<h3 id="forum-tab" class="tab-title<?= $tab_name == 'Forum Signature' ? ' activetab' : '' ?>"><a href="/members/account/forum-signature.php">Forum<br>Signature</a></h3>
</div>
<br />
