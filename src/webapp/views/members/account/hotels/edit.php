<?
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
    $tab_name		   = 'Sub-Accounts';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <h1 class="left"><?= $_REQUEST['licensed_locations_id'] ? 'Edit' : 'Add' ?> Location</h1>
    <? if(ActiveMemberInfo::IsUserMultiLocation()): ?>
        <div class="page-options"><a href="/members/account/hotels/list.php" class="right">Return to Locations List</a></div>
    <? endif; ?>
    <br clear="all" />
    
    <form action="/members/account/hotels/edit.php" method="post" enctype="multipart/form-data" id="add-hotel-form">
        <input type="hidden" name="licensed_locations_id" value="<?= $licensed_location['licensed_locations_id'] ?>" />

        <? if(!$licensed_location['licensed_locations_id']) {
           $locationTypeLabel = '<span class="imp">*</span> Location Type';
           $locationTypeList = arrays::values_as_keys(array_merge(array('Please Select'),array_keys(licensed_locations::get_location_types())));
           echo html::selectfield($locationTypeLabel, $locationTypeList, 'licensed_locations_full_type', $model->location_type, array('style' => 'width:255px'), $errors);
        } else { ?>
            <div class="form-field">
                <label>Location Type</label>
                <span><?= $licensed_location['licensed_locations_full_type'] ?></span>
                <input type="hidden" name="licensed_locations_full_type" id="licensed_locations_full_type" value="<?= $licensed_location['licensed_locations_full_type'] ?>" />
            </div>
        <? } ?>

            <?//= html::multiradio('Franchise?', 'hotels_franchise', array('1' => 'Yes', '0' => 'No'), $hotel, $errors) ?>
            <?= html::textfield('<span class="imp">*</span> Location Name', $licensed_location, 'licensed_locations_name', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
            <?= html::textfield('<span class="imp">*</span> Number of Rooms', $model->number_of_rooms, 'hotels_num_rooms', array('class' => 'long-txt', 'maxlength' => 11), $errors) ?>
            <?= html::textfield('<span class="imp">*</span> Address', $licensed_location, 'licensed_locations_address', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
            <?= html::textfield('<span class="imp">*</span> City', $licensed_location, 'licensed_locations_city', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
            <?= html::selectfield('<span class="imp">*</span> State', html::$us_states, 'licensed_locations_state', $licensed_location, array('style' => 'width:255px'), $errors) ?>
            <?= html::textfield(' <span class="imp">*</span> Zip', $licensed_location, 'licensed_locations_zip', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
            <?= html::textfield('<span class="imp">*</span> Phone', $licensed_location, 'licensed_locations_phone', array('class' => 'long-txt phone', 'maxlength' => 20), $errors) ?>
            <?= html::textfield('Fax', $licensed_location, 'licensed_locations_fax', array('class' => 'long-txt phone', 'maxlength' => 20), $errors) ?>

            <? if(!$licensed_location['licensed_locations_id']): ?>
                <p/>
                <p/>
                <div class="form-field">
                    <label for="discount_code">Discount Code:</label>
                    <?= html::input($_REQUEST, 'discount_code', array('class' => 'text', 'style' => 'width:100px')); ?>
    <!--                <button class="btn right" id="discount_code_btn">Update Total</button>-->
                    <? if ($errors['discount_code']): ?>
                    <p class="form-error"><?= $errors['discount_code'] ?></p>
                    <? endif ?>
                </div>


                <div class="form-field">
                    <label for="franchise_code">Group Code:</label>
                    <?= html::input($_REQUEST, 'franchise_code', array('class' => 'text', 'style' => 'width:100px')); ?>
                    <button class="btn" type="button" id="discount_code_btn">Update Total</button>
                    <? if ($errors['franchise_code']): ?>
                    <p class="form-error"><?= $errors['franchise_code'] ?></p>
                    <? endif ?>
                </div>


                <div class="form-field" id="discount-wrapper" <?= empty($discount) ? 'style="display:none;"' : '' ?>>
                    <label>Discount:</label><strong id="discount-field"><?= !empty($discount) ? $discount : '' ?></strong>
                </div>

                <div class="form-field" id="franchise-discount-wrapper" <?= empty($franchise_discount_string) ? 'style="display:none;"' : '' ?>>
                    <label>Group Discount:</label><strong
                        id="franchise-discount-field"><?= !empty($franchise_discount_string) ? $franchise_discount_string : '' ?></strong>
                </div>


                <div class="form-field">
                    <label for="total-cost">Total Cost:</label>
                    <?= html::input($_REQUEST, 'total-cost', array('class' => 'text', 'style' => 'width:100px', 'readonly'=>'readonly','value'=>(!empty($franchise_cost_string) ? $franchise_cost_string : $total_cost_string))); ?>
                </div>
            <? endif; ?>

        <?php if(ActiveMemberInfo::_IsAccountAdmin()) { ?>
            <div class="form-field">
                <label for=""><span><label for="edit_claims_emails_permission">Edit Claims Emails</label></span></label>
                <span><input value='1' name="edit_claims_emails_permission" id="edit_claims_emails_permission" type="checkbox" <?=  $location_email_permissions['location_can_override_claims_email'] == 1 ? 'checked = "checked"' :''; ?> /></span>
            </div>
            <div class="form-field">
                <label for=""><span><label for="edit_claims_emails_permission">Edit Certificates Emails</label></span></label>
                <span><input value='1' name="edit_certs_emails_permission" id="edit_certs_emails_permission" type="checkbox" <?=  $location_email_permissions['location_can_override_certificates_email'] == 1 ? 'checked = "checked"' :''; ?> /></span>
            </div>
                          
            <input type="hidden" id="discount_codes_id" name="discount_codes_id" value="" />
            <input type="hidden" id="franchises_id" name="franchises_id" value="" />

        <?php } ?>

        <?= html::hidden($licensed_location['licensed_locations_type'], 'licensed_locations_type'); ?>

        <div class="form-actions">
            <?= html::input(($licensed_location['licensed_locations_id'] ? 'Save' : 'Add') . ' Location', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>

<script>
    var totalCost = "<?= $total_cost ?>";
    var totalCostString = "<?= $total_cost_string ?>";
</script>
<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script src="/js/location.js?v=20180618"></script>

<? include VIEWS. '/members/footer.php' ?>
