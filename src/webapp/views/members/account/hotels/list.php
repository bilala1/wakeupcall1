<?
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
    $tab_name		   = 'Sub-Accounts';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>


<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">This is where you will find everything that has to do with your <?= SITE_NAME ?> account:  Locations, members, billing information, notification preferences, etc. </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <h1 class="left">Locations</h1>
    <div class="page-options">
        <?php if(!ActiveMemberInfo::IsChildAccount()) { ?>
            <a href="/members/account/hotels/edit.php" class="add-item">Add Location</a>
        <?php } ?>
    </div>
    <br clear="all" />

    <? foreach($licensed_location_types as $location_type => $location_type_title) {
        if($locations_by_type[$location_type]) {
    ?>
    <h3 class="left"><?= $location_type_title ?></h3>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <th><?= strings::sort('licensed_locations_name', 'Location Name') ?></th>
            <th><?= strings::sort('members_names', 'Users') ?></th>
            <th>Actions</th>
        </tr>
        <? foreach($locations_by_type[$location_type] as $licensed_location): ?>
            <tr <?= $licensed_location['licensed_locations_delete_datetime'] ? 'style="color: #bbb" ': ''; ?>>
                <? if($location_type == 'Corporate Office'): ?>
                    <td><?= $licensed_location['licensed_locations_name'] . ' (Corporate Office)'?></td>
                <? else: ?>
                    <td><?= $licensed_location['licensed_locations_name'] ?></td>
                <? endif; ?>
                <td>
                    <? foreach($licensed_location['members_ids'] as $i => $members_id) {?>
                    <? $member = $members[$members_id]?>
                        <a href="/members/account/hotels/members/edit.php?members_id=<?=$members_id?>">
                            <?= $member['members_firstname'] ?> <?= $member['members_lastname'] ?>
                        </a><? if($i < count($licensed_location['members_ids']) -1) { ?>, <? } ?>
                    <? } ?>
                </td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <? if ($licensed_location['licensed_locations_delete_datetime']): ?>
                                <li>
                                    <a href="/members/account/hotels/renew.php?licensed_locations_id=<?= $licensed_location['licensed_locations_id'] ?>"<?= $licensed_location['needs_charge'] ? ' class="renew"' : '' ?>>Renew
                                        <br/>
                                        <i>May renew before <?= date('m/d/Y',
                                                strtotime($licensed_location['licensed_locations_delete_datetime'] . ' + ' . DELETE_ENTITY_DAYS . ' days')); ?></i></a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/account/hotels/edit.php?licensed_locations_id=<?= $licensed_location['licensed_locations_id'] ?>">Edit</a>
                                </li>
                                <li>
                                    <a href="/members/account/hotels/delete.php?licensed_locations_id=<?= $licensed_location['licensed_locations_id'] ?>"
                                       class="deletecheck">Delete</a></li>
                            <? endif; ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="6"><?= $paging->get_html(); ?></td>
        </tr>
    </table>

    <br/>
    <?  }
         }
        ?>
</div>
<script style="text/javascript">
window.addEvent('domready', function(){
    $$('.renew').addEvent('click', function(){
        if(!confirm('Are you sure you want to renew this location. You will be charged an additional <?= money_format('%n', YEARLY_COST) ?>.')){
            return false;
        }
    });
});
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box'); 
    <? endif; ?>
window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_account');
    });
})    
</script>
<? include VIEWS. '/members/footer.php' ?>
