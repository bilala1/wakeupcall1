<?= MooDialogClass::StartFixedMooDialog("Account Admin"); ?>
    <ul>	
        <li>Account Admin users have access to all data in all corporate locations. </li>
        <li>Account Admins can be given priveleges to add/delete/and edit other Account Admins by checking the "Manage Account Admins" box.</li>
    </ul>
<?= MooDialogClass::EndFixedMooDialog(); ?>