<?
$page_title = SITE_NAME;
$page_name = 'My Wakeup Call';
$sub_name = 'account';
$tab_name = 'Users';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
        <br class="clear"/>
        <? include VIEWS . '/members/account/header.php' ?>
        <h1 class="left">Activity Log for <?= $member['members_firstname'] ?> <?= $member['members_lastname'] ?></h1>
        <div class="page-options"><a href="/members/account/hotels/members/list.php" class="right">Return to Users List</a></div>
        <br clear="all" />
        <ul>
            <? foreach($activities as $activity): ?>
            <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($activity['actions_datetime'])) ?>)</b> - <?= $activity['actions_name'] ?></li>
            <? endforeach ?>
        </ul>
        <?= $paging->get_html(); ?>
    </div>

<? include VIEWS. '/members/footer.php' ?>
