<?
$page_title = SITE_NAME;
$page_name = 'User';
$sub_name = 'account';
$page_keywords = '';
$page_description = '';
$tab_name = 'Users';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
        <br class="clear"/>
        <? include VIEWS . '/members/account/header.php' ?>
        <a href="/members/account/hotels/members/list.php" class="right">Return to Users List</a>

        <h1>Delete User</h1>

        <form action="/members/account/hotels/members/delete.php" method="post">
            <input type="hidden" name="members_id" value="<?= $member['members_id'] ?>"/>

            <p>
                Name: <?= $member['members_firstname'] ?> <?= $member['members_lastname'] ?><br>
                Email: <?= $member['members_email'] ?>
            </p>

            <? if ($member['total_docs'] > 0): ?>
                This user has <?= $member['total_docs'] ?> document<?= $member['total_docs'] == 1 ? '' : 's' ?>,
                <?= $member['has_shared_docs'] ?> of which <?= $member['has_shared_docs'] == 1 ? 'is' : 'are' ?> shared.

                <? if ($member['has_unshared_docs'] > 0): ?>
                    <?= html::multiradio('Unshared Documents:', 'unshared_docs', array('transfer' => 'Transfer to My Files', 'delete' => 'Delete them'), $_REQUEST, $errors) ?>
                <? endif; ?>
                <? if ($member['has_shared_docs'] > 0): ?>
                    <?= html::multiradio('Shared Documents:', 'shared_docs', array('transfer' => 'Transfer to My Files', 'delete' => 'Delete them'), $_REQUEST, $errors) ?>
                <? endif; ?>
            <? endif; ?>

            <input type="submit" value="Confirm Delete" class="btn"/>
        </form>
        <br class="clear"/>
    </div>

<?php include VIEWS . '/members/footer.php' ?>