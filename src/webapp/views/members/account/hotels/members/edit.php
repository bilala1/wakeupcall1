<?
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
    $page_keywords     = '';
    $page_description  = '';
    $tab_name		   = 'Users';
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <a href="/members/account/hotels/members/list.php" class="right" style="padding-right: 20px;">Return to Users List</a>
    <h1><?= $_REQUEST['members_id'] ? 'Edit' : 'Add' ?> User</h1>

    <form action="/members/account/hotels/members/edit.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="members_id" value="<?= $member['members_id'] ?>" />

        <?= html::textfield('First Name:', $member, 'members_firstname', array('maxlength' => 100), $errors) ?>

        <?= html::textfield('Last Name:', $member, 'members_lastname', array('maxlength' => 100), $errors) ?>

        <?= html::textfield('Position/Title:', $member, 'members_title', array( 'maxlength' => 40), $errors) ?>

        <?= html::textfield('Email:', $member, 'members_email', array('maxlength' => 255), $errors) ?>

        <?= html::textfield('Repeat Email:', $member, 'members_email2', array('maxlength' => 255), $errors) ?>

        <?= html::passwordfield('Password:', '', 'members_password', array('class' => 'input-text'), $errors) ?>

        <?= html::passwordfield('Repeat Password:', '', 'members_password2', array('class' => 'input-text'), $errors) ?>

        <h2>Permissions</h2>

        <? if (UserPermissions::UserCanAccessScreen('manageAdmins')): ?>
            <div class="form-field"><label for="">
                <span>User Type</span></label>
                    <span class="multiradio-container">
                        <span>
                            <label for="members_type_0">
                                <input type="radio" value="admin" id="members_type_0" name="members_type" <?= $member['members_type'] == 'admin' ? 'checked="checked"' : '' ?>/>
                                Account Admin
                                <?= html::help_button('account_admin_help') ?></label>
                        </span>
                        <br>
                        <span>
                            <label for="members_type_1">
                                <input type="radio" value="user" id="members_type_1" name="members_type" <?= !$member['members_id'] || ($member['members_type'] == 'user') ? 'checked="checked"' : '' ?>/>
                                Regular User
                                <?= html::help_button('regular_user_help') ?></label>
                        </span>
                    </span>
                </div>
        <? endif ?>

        <? if ($current_member_access['admin']): ?>
            <div id="adminPerms">
                <h3>Admin</h3>
                <? foreach ($access_levels_by_scope['admin'] as $admin_perm): ?>
                <? if($current_member_access['admin'][$admin_perm['members_access_levels_type']]): ?>
                    <?=
                    html::checkboxfield(
                        $admin_perm['members_access_levels_name'],
                        true,
                        'access[' . $admin_perm['members_access_levels_type'] . ']',
                        $edit_member_access['admin'][$admin_perm['members_access_levels_type']],
                        array(),
                        $errors) ?>
                <? endif; ?>
                <? endforeach ?>
                <br/>
            </div>
        <? endif ?>
        <? if ($current_member_access['sensitive']): ?>
            <div>
                <h3>Sensitive</h3>
                <? foreach ($access_levels_by_scope['sensitive'] as $sensitive_perm): ?>
                    <? if($current_member_access['sensitive'][$sensitive_perm['members_access_levels_type']] ):
                        $help_button = '';
                        if($sensitive_perm['members_access_levels_help_text']){
                            $help_button = html::help_button(null, null, $sensitive_perm['members_access_levels_help_text']);
                        }    
                    ?>
                    <?=
                    html::checkboxfield(
                        $sensitive_perm['members_access_levels_name'].$help_button,
                        true,
                        'sensitive[' . $sensitive_perm['members_access_levels_type'] . ']',
                        $edit_member_access['sensitive'][$sensitive_perm['members_access_levels_type']],
                        array(),
                        $errors) ?>
                    <? endif; ?>
                <? endforeach ?>
                <br/>
            </div>
        <? endif ?>
        <? if ($current_member_access['account']): ?>
            <div id="accountPerms">
                <h3>Account</h3>
                <? foreach ($access_levels_by_scope['account'] as $account_perm): 
                    $help_button = '';
                    if($account_perm['members_access_levels_help_text']){
                        $help_button = html::help_button(null, null, $sensitive_perm['members_access_levels_help_text']);
                    } 
                    ?>
                    <? if($current_member_access['account'][$account_perm['members_access_levels_type']]): ?>
                    <?=
                    html::checkboxfield(
                        $account_perm['members_access_levels_name'].$help_button,
                        true,
                        'access[' . $account_perm['members_access_levels_type'] . ']',
                        $edit_member_access['account'][$account_perm['members_access_levels_type']],
                        array(),
                        $errors) ?>
                    <? endif; ?>
                <? endforeach ?>
                <br/>
            </div>
        <? endif ?>

        <div id="locationPerms">
            <? if (count($locations_for_access) > 2): ?>
                <p class="callout">Please remember to save changes below, after setting permissions.</p>
            <? endif; ?>

            <table style="text-align:center;">
                <tr>
                    <th></th>
                    <? foreach ($access_levels_by_scope['location'] as $access) { ?>
                        <th><?= $access['members_access_levels_name'] ?></th>
                    <? } ?>
                </tr>
                <? foreach ($locations_for_access as $location): ?>
                    <? $prefix = 'loc_access[' . $location['licensed_locations_id'] . ']';
                    $user_location_access = $current_member_access['location'][$location['licensed_locations_id']];
                    $edit_location_access = $edit_member_access['location'][$location['licensed_locations_id']];
                    $attrs = array(
                        'class' => 'locationPerm',
                        'data-location' => $location['licensed_locations_id'],
                        'data-sensitive' => 'false'
                    );
                    $sensitive_attrs = $attrs;
                    $regular_attrs = $attrs;
                    $admin_attrs = $attrs;
                    if ($edit_location_access['locationAdmin']) {
                        $regular_attrs['disabled'] = 'disabled';
                    }
                    $sensitive_attrs['data-sensitive'] = 'true';
                    $admin_attrs['class'] = 'locationAdmin';
                    ?>
                    <tr>
                        <td><?= $location['licensed_locations_name'] ?></td>
                        <? foreach ($access_levels_by_scope['location'] as $loc_perm): ?>
                            <td style="<?= 1 == $loc_perm['members_access_levels_sensitive'] ? 'background:#ffe':''?>">
                                <? if ($user_location_access[$loc_perm['members_access_levels_type']]): ?>
                                    <?
                                    $useAttrs = $regular_attrs;
                                    if(1 == $loc_perm['members_access_levels_sensitive']) {
                                        $useAttrs = $sensitive_attrs;
                                    } else if ('locationAdmin' == $loc_perm['members_access_levels_type']) {
                                        $useAttrs = $admin_attrs;
                                    }
                                    echo html::checkbox(
                                        true,
                                        $prefix . '[' . $loc_perm['members_access_levels_type'] . ']',
                                        $edit_location_access[$loc_perm['members_access_levels_type']] || ($edit_location_access['locationAdmin'] && (0 == $loc_perm['members_access_levels_sensitive'])),
                                        $useAttrs);
                                    ?>
                                <? endif ?>
                            </td>
                        <? endforeach ?>
                    </tr>
                <? endforeach ?>
            </table>
        </div>
        <br clear="all" />
        <br /><br />

        <div class="form-actions">
            <?= html::input(($member['members_id'] ? 'Save' : 'Add') . ' User', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>
<script type="text/javascript" src="/js/users.js?v=20180618"></script>

<?php include VIEWS. '/members/footer.php' ?>
