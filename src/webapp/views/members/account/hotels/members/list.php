<?
$page_title = SITE_NAME;
$page_name         = 'My Account';
$sub_name          = 'My Account';
$tab_name = 'Users';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
        <span class="info_icon"></span>

        <p class="page-desc">This is where you will find everything that has to do with your <?= SITE_NAME ?> account
            information and where you set your preferences.</p>
        <span class="info_text">Hide Text Box</span>
        <br class="clear"/>
        <? include VIEWS . '/members/account/header.php' ?>
        <h1 class="left">Users</h1>

        <div class="page-options">
            <a href="/members/account/hotels/members/edit.php" class="add-item">Add New User</a>
        </div>
        <br clear="all"/>

        <? if ($admins): ?>
            <h3>Admins</h3>
            <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="userList">
                <tr>
                    <th><?= strings::sort('members_lastname', 'User Name') ?></th>
                    <th><?= strings::sort('members_email', 'Member\'s Email') ?></th>
                    <th>Actions</th>
                </tr>
                <? foreach ($admins as $member): ?>
                    <tr class="<?= $member['main_account_admin'] ? 'mainAdmin' : ''?>">
                        <td><?= $member['members_firstname'] . ' ' . $member['members_lastname'] ?></td>
                        <td><?= $member['members_email'] ?></td>
                        <td>
                            <? if (ActiveMemberInfo::_IsMainAccountAdmin() || !$member['main_account_admin']): ?>
                                <div class="actionMenu">
                                    <ul>
                                        <li>
                                            <a href="/members/account/hotels/members/activity-log.php?members_id=<?= $member['members_id'] ?>">Activity
                                                Log</a></li>
                                        <li>
                                            <a href="/members/account/hotels/members/edit.php?members_id=<?= $member['members_id'] ?>">Edit</a>
                                        </li>
                                        <? if (!$member['main_account_admin']): ?>
                                            <li>
                                                <a href="/members/account/hotels/members/delete.php?members_id=<?= $member['members_id'] ?>">Delete</a>
                                            </li>
                                        <? endif; ?>
                                    </ul>
                                </div>
                            <? else: ?>
                                &nbsp;
                            <? endif; ?>
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>

            <br clear="all"/>
            <h3>Users</h3>
        <? endif; ?>


        <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
            <tr>
                <th><?= strings::sort('members_lastname', 'User Name') ?></th>
                <th><?= strings::sort('members_email', 'Member\'s Email') ?></th>
                <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                    <th>Location(s)</th><? endif; ?>
                <th>Actions</th>
            </tr>
            <?php foreach ($members as $member): ?>
                <tr>
                    <td><?= $member['members_firstname'] . ' ' . $member['members_lastname'] ?></td>
                    <td><?= $member['members_email'] ?></td>
                    <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                        <td><?= $member['licensed_locations_name'] ?></td>
                    <? endif; ?>
                    <td>
                        <div class="actionMenu">
                            <ul>
                                <li>
                                    <a href="/members/account/hotels/members/activity-log.php?members_id=<?= $member['members_id'] ?>">Activity
                                        Log</a></li>
                                <li>
                                    <a href="/members/account/hotels/members/edit.php?members_id=<?= $member['members_id'] ?>">Edit</a>
                                </li>
                                <? if (!$member['non_managed']): ?>
                                    <li>
                                        <a href="/members/account/hotels/members/delete.php?members_id=<?= $member['members_id'] ?>">Delete</a>
                                    </li>
                                <? endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <script type="text/javascript">
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
        $$('.page-desc').setStyle('display', 'none');
        $$('.info_text').set('html', 'Show Text Box');
        <? endif; ?>
        window.addEvent('load', function () {
            //infobox settings
            var webinarsRequest = new Request({
                url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
                method: 'get',
                onSuccess: function (responseText) {
                    if (responseText == 'on') {
                        $$('.page-desc').reveal();
                        $$('.info_text').set('html', 'Hide Text Box');
                    }
                    else {
                        $$('.page-desc').dissolve();
                        $$('.info_text').set('html', 'Show Text Box');
                    }
                }
            });
            $$('.info_icon, .info_text').addEvent('click', function (event) {
                if ($$('.page-desc')[0].get('style').match(/display: none/gi)) {
                    var show = 'true';
                }
                else {
                    var show = 'false';
                }
                webinarsRequest.send('yn=' + show + '&type=members_settings_infobox_my_account');
            });
        });
    </script>

    <? include VIEWS . '/members/footer.php' ?>
