<?= MooDialogClass::StartFixedMooDialog("Regular User"); ?>
            <ul>
                <li>Regular Users have specifically designed access to certain areas of one or more specified locations </li>
                <li>Regular Users are the best choice for administrators or users of only one or two locations</li>
            </ul>
<?= MooDialogClass::EndFixedMooDialog(); ?>
