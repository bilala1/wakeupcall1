<div id="content">
<?
include(ACTIONS . '/members/protect.php');

$db = mysqli_db::init();

$licensed_locations_table = new mysqli_db_table('licensed_locations');
$members_table = new mysqli_db_table('members');

$licensed_location = licensed_locations::get_by_id_with_details($_REQUEST['licensed_locations_id']);

$member = $members_table->get($licensed_location['join_members_id']);
?>
    <form action="/members/account/hotels/password_popup_process.php?licensed_locations_id=<?= $_REQUEST['licensed_locations_id']?>" method="post" enctype="multipart/form-data" id="add-hotel-form">
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Note Description</th>
        </tr>
       
        <tr><td>
                <?= html::textfield('<span class="imp">*</span> EMail<br />(User Name)', $member, 'members_email', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> Re-enter EMail', $member, 'members_email2', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
                <?= html::passwordfield('<span class="imp">*</span> Password', $member, 'licensed_locations_pass', array('class' => 'long-txt input-text'), $errors) ?>
                <em  style="margin:-5px 30px 5px 150px;font-size:11px">(Password must have a minimum of 6 characters)</em>
                <?= html::passwordfield('<span class="imp">*</span> Re-enter Password', $member, 'licensed_locations_pass2', array('class' => 'long-txt input-text'), $errors) ?>
        </td></tr>
    </table>
        <input type="submit" value="<?='Save'?>" />
    </form>

</div>