<?
    $page_title        = SITE_NAME;
    $page_name         = 'My Account';
    $sub_name          = 'My Account';
	$tab_name		   = 'Notification Settings';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<style type="text/css">
label{
    width:380px;
}
.block{ display:block}
.normal{ font-weight:normal}
.input-checkbox{
	border: 1px solid #ccc;
	margin-top:20px
}
</style>
<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">This is where you will find everything that has to do with your <?= SITE_NAME ?> account:  Locations, members, billing information, notification preferences, etc. </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <? include VIEWS. '/members/account/header.php' ?>
    <form action="/members/account/notifications.php" method="post">
        
        <?= html::checkboxfield('1.	Message Forum Updates: <span class="normal block">(Will notify you of new topics posted in the Message Forum)</span> ', 1, 'notifications_forum_update', $notifications) ?>
        <?= html::checkboxfield('2.	Weekly News Updates: <span class="normal block">(You will receive our weekly email newsletter)</span>', 1, 'notifications_news_update', $notifications) ?>
        <br class="clear" />
        <div class="form-actions">
            <?= html::input('Save', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_account');
        });
    })
</script>
</div>
<? include VIEWS. '/members/footer.php' ?>
