<html>
<body>
<p style="font-size:70px; margin:0"><b>FAX</b></p>
<div style="border:8px solid black; margin:40px 80px; padding:20px;">
    <div style="border:1px solid #999; padding:4px">
        <b>To: <?= $carrier['carriers_name'] ?></b>
        <hr />
        Fax: <?= $carrier['carriers_fax'] ?>
        <hr />
        Date: <?= date('m/d/Y') ?>
        <hr />
        Pages: <?= $pagesSent ?> <small> (including this cover sheet)</small>
        <hr />
        <b>Re: Claim for <?= $hotel['hotels_name'] ?> Policy Number <?= $carrier['carriers_policy_number'] ?></b>
        <hr />
        <br /><br />
        <hr />
        <b>From: <?= $member['members_firstname'].' '.$member['members_lastname'] ?></b>
        <hr />
        Email: <?= $member['members_email'] ?>
        <hr />
        Phone: <?= $member['members_phone'] ?>
        <hr />
        CC:
        <hr />
        <b>Comments:</b>
    </div>
    Please process this claim report as soon as possible.<br />
    <br />
</div>
</body>
</html>