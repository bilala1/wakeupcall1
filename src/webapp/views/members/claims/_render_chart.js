var chartRenderer = (function() {
    var pub = {};

    pub.renderChart = function () {
        var reportForm = $('report-form');
        var queryString = reportForm.toQueryString();
        var url = reportForm.get('data-url');
        var reportType = $$('input[name=chartType]:checked')[0].get("value");
        var reportDataRequest = new Request.JSON({
                url: url + "?" + queryString,
                onSuccess: function (report) {
                    var data = new google.visualization.DataTable(report.data);
                    // this will make sure the y axis is whole numbers.
                    var maxValue = Math.floor(report.maxH / 4) * 4 + 4;
                    var options = {
                        title: report.title,
                        vAxis: {
                            maxValue: maxValue,
                            viewWindow:{
                                min:0
                            }
                        },
                        hAxis: {showTextEvery:1}
                    };
                    var chart = getChartByType(reportType);
                    google.visualization.events.addListener(chart, 'ready', myReadyHandler);

                    chart.draw(data, options);
                    $('chartDownload').setProperty('data-chart',chart.getImageURI());
                    $('chartDownload').setProperty('download',report.title.replace(/ /g,'')+'.png');
                }
            }
        ).get();
    }

    function myReadyHandler() {
        var windowType = $$('body').get('data-window-type');
        if (windowType == 'print') {
            window.print();
        }
    }

    function getChartByType(type) {
        var chartDiv = document.getElementById('chart_div');
        switch(type) {
            case "column":
                return new google.visualization.ColumnChart(chartDiv);
                break;
            case "pie":
                return new google.visualization.PieChart(chartDiv);
                break;
        }
    }

    return pub;
})();
