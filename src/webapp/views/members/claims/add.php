<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = 'Claims';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
    <h1 class="left">Add Claim</h1>

    <div class="page-options"><a href="/members/claims/carriers/index.php">Manage Carriers/Recipients</a> | <a
            href="/members/claims/index.php">Return to Claims List</a></div>
    <br clear="all"/>

    <form action="/members/claims/edit.php" name="editClaim" id="editClaim"  method="get" >

        <fieldset>
            <legend>Claim Details</legend>
            <?= html::selectfield_from_query('Location:', $licensed_locations, 'licensed_locations_id', $claim,
                'licensed_locations_name', 'Please Select', array(), $errors); ?>
        </fieldset>

        <input type="hidden" name="action" value="setLocation"/>

        <div class="form-actions">
            <input type="submit" id="save" value="Continue" class="btn-primary"/>
        </div>
    </form>
</div>

<script type="text/javascript">
window.addEvent('domready', function () {
    function chkClaimForm(form) {
        var claimIsValid = true;
        $$('p.form-error').destroy();

        var $location = $('licensed_locations_id');
        if ($location) {
            claimIsValid &= window.WUC.validateRequired($location, 'Please select a location');
        }
        if (!claimIsValid) {
            window.WUC.scrollToFirstError()
        }
        return claimIsValid;
    }

    $('save').addEvent('click', function (e) {
        var form = $('editClaim');
        if (chkClaimForm(form)) {
            form.submit();
        } else {
            e.preventDefault();
        }
    });
 });
</script>

<? include VIEWS . '/members/footer.php' ?>

