<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 8/15/15
 * Time: 9:20 AM
 */

//if (!isset($all_location_checked)) {
//    throw new Exception("$all_location_checked not set");
//}
//
//if (!isset($all_email_locations_checked)) {
//    throw new Exception("$all_email_locations_checked not set");
//}

// All the locations the user has access to
if (!isset($licensed_locations_for_member)) {
    throw new Exception("$licensed_locations_for_member not set");
}

// the locations this carrier is assigned to
if (!isset($email_recipient_locations)) {
    throw new Exception("$email_recipient_locations not set");
}

// the locations for which this carrier will always get a copy of the claim
if (!isset($always_email_locations)) {
    throw new Exception("$always_email_locations not set");
}

if (!isset($recipient_edit_locations)) {
    throw new Exception("$email_recipient_locations not set");
}
$show_location_can_edit = ActiveMemberInfo::_IsAccountAdmin();
?>
<? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
    <table id="recipient_location_table" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Choose Locations to have access to this recipient:</th>
        </tr>
        <tr>
            <td>
                <div>
                    <table align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
                        <tr>
                            <td><b>Location</b></td>
                            <td><b>Notify on all updates</b><?=html::help_button(null, null, 'An email notification will be sent any time a claim is created, updated or submitted to a carrier')?></td>
                            <td><b>Notify on initial save</b><?=html::help_button(null, null, 'An email notification will be sent any time a new claim is entered into the system')?></td>
                            <td><b>Notify on all submissions</b><?=html::help_button(null, null, 'An email notification will be sent any time a claim is submitted to a carrier')?></td>
                            
                            <? if ($show_location_can_edit) { ?>
                                <td><b>Location Can Edit</b></td>
                            <? } ?>
                        </tr>
                        <? if(ActiveMemberInfo::_IsAccountAdmin()): ?>
                        <tr>
                            <td style="text-align: left;">
                                <label class="checkbox-label">
                                <input type="checkbox" id="checkbox_all_locations" name="checkbox_all_locations" <?= $carriers_all_locations_checked ? 'checked' : ''; ?>/>
                                <b>All Locations</b>
                                </label>
                            </td>
                            <td style="text-align: left;">
                                <label class="checkbox-label">
                                <input type="checkbox" id="checkbox_receive_all" name="checkbox_receive_all" <?= $carriers_all_receive_checked ? 'checked' : ''; ?>/>
                                <b>All Locations</b>
                                </label>
                            </td>
                            <td style="text-align: left;">
                                <label class="checkbox-label">
                                <input type="checkbox" id="checkbox_receive_all_save" name="checkbox_receive_all_save" <?= $carriers_all_receive_onsave_checked ? 'checked' : ''; ?>/>
                                <b>All Locations</b>
                                </label>
                            </td>
                            <td style="text-align: left;">
                                <label class="checkbox-label">
                                <input type="checkbox" id="checkbox_receive_all_submission" name="checkbox_receive_all_submission" <?= $carriers_all_receive_onsubmission_checked ? 'checked' : ''; ?>/>
                                <b>All Locations</b>
                                </label>
                            </td>
                            
                            <? if(ActiveMemberInfo::IsAccountMultiLocation() && $show_location_can_edit) { ?>
                            <td></td>
                            <? } ?>
                        </tr>
                        <? endif; ?>
                        <? foreach($licensed_locations_for_member as $location):  ?>
                            <tr>
                                <? $location_checked = in_array($location['licensed_locations_id'], $email_recipient_locations); ?>
                                <? $receive_all_claims_onupdate_checked = in_array($location['licensed_locations_id'], $always_email_locations);
                                   $receive_all_claims_onsave_checked = in_array($location['licensed_locations_id'], $always_email_on_save_locations);
                                   $receive_all_claims_onsubmit_checked = in_array($location['licensed_locations_id'], $always_email_onsubmit_locations);                               
                                ?>
                                <? $locations_can_edit_checked = in_array($location['licensed_locations_id'], $recipient_edit_locations); ?>
                                <td style="text-align: left;"><label class="checkbox-label">
                                    <input type="checkbox" id="recipient_location_check" class="carrier_location" name="recipient_<?=$location['licensed_locations_id'];?>" <?= $location_checked ? "checked" : "" ?> >
                                     <?= $location['licensed_locations_name'] ?></label>
                                </td>
                                   <td style="text-align: left;">
                                        <input type="checkbox" id="recipient_send_all_check3" name="recipient_send_all_<?=$location['licensed_locations_id'];?>" <?= $receive_all_claims_onupdate_checked ? "checked" : "" ?> onclick='toggleCheckboxSelection(this);'>
                                    </td> 
                                    <td style="text-align: left;">
                                        <input type="checkbox" id="recipient_send_all_check1" name="recipient_send_all_on_save_<?=$location['licensed_locations_id'];?>" <?= $receive_all_claims_onsave_checked ? "checked" : "" ?> onclick='toggleCheckboxSelection(this);'>
                                    </td>
                                    <td style="text-align: left;">
                                        <input type="checkbox" id="recipient_send_all_check2" name="recipient_send_all_on_submission_<?=$location['licensed_locations_id'];?>" <?= $receive_all_claims_onsubmit_checked ? "checked" : "" ?> onclick='toggleCheckboxSelection(this);'>
                                    </td>
                                    
                                    <? if ($show_location_can_edit) { ?>
                                        <td style="text-align: left;">
                                            <input type="checkbox" id="recipient_location_can_edit_" name="recipient_location_can_edit_<?=$location['licensed_locations_id'];?>" <?= $locations_can_edit_checked ? "checked" : "" ?> >
                                        </td>
                                    <? } ?>
                            </tr>
                        <? endforeach; ?>
                    </table></div>
            </td></tr>
    </table>
<? } else{ ?>
    <div  id="recipient_location_table">
        <input type="hidden" id="recipient_location_check" name="recipient_<?=$locations[0]['licensed_locations_id']?>" value = 'on'>
        <div class="form-field">
            <label>
                <span>Notify on all updates<?=html::help_button(null, null, 'An email notification will be send any time a claim is created, updated or submitted to a carrier')?></span>
            </label>
             <? $receive_all_claims_onupdate_checked = in_array($locations[0]['licensed_locations_id'], $always_email_locations); ?>
            <input type="checkbox" id="recipient_send_all_check3" name="recipient_send_all_<?=$locations[0]['licensed_locations_id'];?>" <?= $receive_all_claims_onupdate_checked ? "checked" : "" ?> >
        </div>
        <div class="form-field">
            <label>
                <span>Notify on initial save<?=html::help_button(null, null, 'An email notification will be send any time a new claim is entered into the system')?></span>
            </label>
             <? $receive_all_claims_onsave_checked = in_array($locations[0]['licensed_locations_id'], $always_email_on_save_locations); ?>
            
            <input type="checkbox" id="recipient_send_all_check1" name="recipient_send_all_on_save_<?=$locations[0]['licensed_locations_id'];?>" <?= $receive_all_claims_onsave_checked ? "checked" : "" ?> >
        </div>
        <div class="form-field">
            <label>
                <span>Notify on all submission<?=html::help_button(null, null, 'An email notification will be send any time a claim is submitted to a carrier')?></span>
            </label>
             <? $receive_all_claims_onsubmit_checked = in_array($locations[0]['licensed_locations_id'], $always_email_onsubmit_locations); ?>
            
            <input type="checkbox" id="recipient_send_all_check2" name="recipient_send_all_on_submission_<?=$locations[0]['licensed_locations_id'];?>" <?= $receive_all_claims_onsubmit_checked ? "checked" : "" ?> >
        </div>
        <? if (ActiveMemberInfo::_IsAccountAdmin()): ?>
            <div class="form-field">
                    <label>
                        <span>Permissions for non-Account Admins:
                            <?= html::help_button(null, null, 'These settings configure access to this recipient for users with the Location Admin or claims permission. Account Admins can always view, edit, or delete a recipient.') ?>
                        </span>
                    </label>
                    <label class="checkbox-label">
                        <? $locations_can_edit_checked = in_array($locations[0]['licensed_locations_id'], $recipient_edit_locations);?>  
                            <input type="checkbox" id="recipient_location_can_edit_" name="recipient_location_can_edit_<?=$locations[0]['licensed_locations_id'];?>" <?= $locations_can_edit_checked ? "checked" : "" ?> >Non-Account Admins can edit this recipient
                </label>
            </div>
        <? endif ?>
    </div>

<? } ?>
