<?
    $page_title        = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Claims';
    $page_keywords     = '';
    $page_description  = '';
$show_location_can_edit = ActiveMemberInfo::_IsAccountAdmin();
if ($_REQUEST['carriers_id']) {
    if ($carrier['carriers_recipient'] == 0) {
       $carrierRecipientLabel = 'Carrier';
     } else {
        $carrierRecipientLabel = 'Recipient';
    }
} else {
    $carrierRecipientLabel = 'Carrier/Recipient';
}
if (!isset($locations)) { throw new Exception('model variable $locations not set.'); }
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
    <h1 class="left"><?php if ($_REQUEST['carriers_id']) {
            if ($carrier['carriers_recipient'] == 0) {
                ?><?= $isRenewal ? 'Renew' : 'Edit' ?> Carrier Policy
            <?php } else { ?>
                Edit Recipient
            <?php }
        } else { ?>
            Add Carrier Policy/Recipient
        <?php } ?></h1>
    <div class="page-options"><a href="/members/claims/carriers/index.php">Return to Carrier Policy/Recipient List</a>
    </div><br clear="all" />

    <form action="/members/claims/carriers/edit.php" method="post" enctype="multipart/form-data" id="editcarrier">

        <?php
        if (!$carrier) {
        ?>
        <div class="form-field">
            <label>Carrier Policy or Recipient:</label><select id="carriers_recipient" name="carriers_recipient" value="2"
                        onchange="RecipientHideHandler()">
                    <option value='2' selected>-Select-</option>
                    <option value='0'>Carrier Policy</option>
                    <option value='1'>Recipient</option>
                </select>
        </div>
        <?php } else { ?>
            <?= html::hidden($carrier, 'carriers_recipient') ?>
        <?php } ?>

        <div id="Recipient_Elements">
            <?= html::textfield('<span class="imp">*</span> '.$carrierRecipientLabel.' Name:', $carrier, 'carriers_name', array('maxlength' => 100), $errors) ?>
            
            <?= html::textfield('&nbsp;&nbsp;Contact Name:', $carrier, 'carriers_contact', array('maxlength' => 150), $errors) ?>
                        
            <?= html::textfield('<span class="imp">*</span> Email Address:', $carrier, 'carriers_email', array('maxlength' => 255), $errors) ?>
        </div>
        
        
        <div id="Recipient_Only_Elements">
            <?//     if($user_corporate): ?>
                <?//= html::checkbox(1, 'carriers_recipient_all', $carrier['carriers_recipient_all'])  Receives All Corporate Claims?>
            <?// endif; ?>
        </div>
        
        <div id="Carrier_Elements" >
            <div class="form-field">
                <div style="width:240px;margin-left:160px">
                    <input type="checkbox" name="carriers_no_email" value="1" style="float:left;margin-left:-3px"/>
                    <div style="width:210px;float:left;margin-top:-2px;margin-left:3px">Check the box if the carrier does not have an email address.</div>
                    <br clear="all" />
                </div>
            </div>
            <?= html::textfield('<span class="imp">&nbsp;</span> Phone Number:', $carrier, 'carriers_phone', array('maxlength' => 50), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Fax Number<br />&nbsp;&nbsp;to Submit Claim<br />&nbsp;&nbsp;<i>(1-800-444-1111)</i>:', $carrier, 'carriers_fax', array('maxlength' => 50), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Policy Number:', $carrier, 'carriers_policy_number', array('maxlength' => 255), $errors) ?>

            <?= html::textfield('<span class="imp">&nbsp;</span> Address:', $carrier, 'carriers_address', array('maxlength' => 255), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Effective Date:', $carrier['carriers_effective_start_date'] ? date('n/j/Y', strtotime($carrier['carriers_effective_start_date'])) : '', 'carriers_effective_start_date', array(), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Expiration Date:', $carrier['carriers_effective_end_date'] ? date('n/j/Y', strtotime($carrier['carriers_effective_end_date'])) : '', 'carriers_effective_end_date', array(), $errors) ?>

            <?php if($carrier && $isRenewal) { ?>
                <p class="callout">
                    Please set the new expiration date, and verify that the effective date and Policy number are correct.
                </p>
            <?php } ?>

            <?php if($carrier && !$isRenewal) { ?>
            <p class="callout" style="display:none;" id="renewWarning">
                If you are updating the effective/expiration dates for this policy because it has recently renewed, please use the policy renewal tool instead. Using the renewal tool helps keep claims sorted per policy period within the system. There is a renewal link in the Action menu for each policy or <a href="edit.php?renew&carriers_id=<?=$carrier['carriers_id']?>">click here</a>.
            </p>
            <?php } ?>

            <?php if($isRenewal) { ?>
                <?= html::checkboxfield("Hide old policy?", '1', 'carriers_hide_policy', '1') ?>
            <?php } ?>
            <div class="form-field">
                <label for=""><span class="imp">*</span> Coverage:</label>
                <span class="multiinput-container">
                    <? foreach(array_chunk($coverages + array('other' => 'Other'), ceil((count($coverages)+1)/3)) as $coverage_chunk): ?>
                        <span>
                        <? foreach($coverage_chunk as $coverage): ?>
                            <label for="coverage_<?= http::encode_url($coverage) ?>">
                                <input type="checkbox" name="coverages[]" id="coverage_<?= http::encode_url($coverage) ?>" value="<?= $coverage ?>" <?= in_array($coverage, $existing_coverages) ? 'checked="checked"' : '' ?>>
                                <?= $coverage ?>
                                <? if($coverage == 'Other') { ?>
                                    <input <?= in_array('Other', $existing_coverages) ? '' : 'disabled' ?> type="text" name="carriers_coverages_other" id="carriers_coverages_other" value="<?= $carrier['carriers_coverages_other'] ?>" style="margin-left: 30px">
                                <? } ?>
                            </label><br />
                        <? endforeach ?>
                        </span>
                    <? endforeach ?>
                </span>
            </div>

            <p><span class="imp">*</span> - required</p>
        </div>

        <? if($can_assign_locations): ?>
                <? include "_recipient_locations.php"; ?>
            <? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
                <table id="carrier_location_table" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
            <tr>
                <th>Choose Locations to have access to this carrier:</th>
            </tr>
                <tr><td >
                    <div>
                    <table align="center" cellpadding="3" cellspacing="1" border="0" class="clear" style="width: 100%;">
                        <tr>
                            <td><b>Location</b></td>
                            <? if($show_location_can_edit): ?>
                                <td><b>Location Can Edit</b></td>
                            <? endif; ?>
                        </tr>
                        <? if(ActiveMemberInfo::_IsAccountAdmin()): ?>
                            <tr>
                                <td style="text-align: left;">
                                    <input type="checkbox" id="check_all_box_carrier" name="carrier_allloc" <?= $carriers_all_locations_checked ? 'checked' : ''; ?>  />
                                    <label for="check_all_box_carrier" class="checkbox-label"><b>All Locations</b></label>
                                </td>
                                <? if(ActiveMemberInfo::IsAccountMultiLocation() && $show_location_can_edit): ?>
                                <td></td>
                                <? endif; ?>
                            </tr>
                        <? endif; ?>
                        <? foreach((array)$locations as $location): ?>
                        <tr>
                            <? $checked = ($carrier_locations && in_array($location['licensed_locations_id'], $carrier_locations)) ? "checked" : ""; ?>
                                <td>
                                    <label class="checkbox-label">
                                        <input type="checkbox" class="carrier_location"
                                               id="carrier_location_check_<?=$location['licensed_locations_id']?>"
                                               name="carrier_<?=$location['licensed_locations_id']?>"
                                               <?= $checked ?>>
                                        <?=$location['licensed_locations_name'] ?>
                                    </label>
                                </td>

                            <? if($show_location_can_edit): ?>
                                <? $checked = ($carrier_edit_locations && in_array($location['licensed_locations_id'], $carrier_edit_locations)) ? "checked" : ""; ?>
                                <td>
                                    <input type="checkbox" class="carrier_location"
                                           id="carrier_location_can_edit_<?=$location['licensed_locations_id']?>"
                                           name="carrier_location_can_edit_<?=$location['licensed_locations_id']?>"
                                        <?= $checked ?>>
                                </td>
                            <? endif; ?>

                        </tr>
                        <? endforeach; ?>
                    </table>
                    
                    </div>
            </td></tr>
        </table>
        <? } else{?>
         <div id="carrier_location_table">
                <input type="hidden" id="carrier_location_check" name="carrier_<?=$locations[0]['licensed_locations_id']?>" value = 'on'>
            <div class="form-field">
                <? if (ActiveMemberInfo::_IsAccountAdmin()): ?>
                <label>
                    <span>Permissions for non-Account Admins:
                        <?= html::help_button(null, null, 'These settings configure access to this carrier for users with the Location Admin or claims permission. Account Admins can always view, edit, or delete a carrier.') ?>
                    </span>
                </label>
                <label class="checkbox-label">
                    <?
                    $checked = '';
                    if($carrier_edit_locations && in_array($locations[0]['licensed_locations_id'], $carrier_edit_locations)){
                        $checked = 'checked=checked';
                    }
                    ?>
                    <input type="checkbox" id="carrier_location_can_edit_" <?=$checked?> name="carrier_location_can_edit_<?=$locations[0]['licensed_locations_id'] ?>"> Non-Account Admins can edit this carrier
                </label>
                <? endif ?>
            </div>
        </div>
            <? }?>
        <? endif; ?>
        <input type="hidden" name="carriers_id" value="<?= $carrier['carriers_id'] ?>" />
        <?php if($isRenewal) { ?>
            <input type="hidden" name="renew" />
        <?php } ?>

        <? if($can_insert_update_all || carriers::can_user_edit_carrier_recipient($carrier['carriers_id'])) { ?>
            <div class="form-actions">
                <?= html::input($isRenewal ? 'Renew' : $carrier ? 'Save' : 'Add', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit', 'id' => 'save_button')) ?>
            </div>
        <? } ?>
    </form>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/carriers_recipients.js?v=20180618"></script>

<? include VIEWS. '/members/footer.php' ?>
