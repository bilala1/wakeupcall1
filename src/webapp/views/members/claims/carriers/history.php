<div>
    <? $actions = carriers_history::get_carriers_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['carriers_id']); ?>
    <ul>
        <? if($actions): ?>
        <? foreach($actions as $action): ?>
        <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['carriers_history_datetime'])) ?>)</b> - <?= $action['carriers_history_description'] ?></li>
        <? endforeach ?>
        <? else: ?>
        <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
