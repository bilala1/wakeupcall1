<?php
    $page_title        = SITE_NAME;
    $page_name         = 'Tracking';
    $sub_name		   = 'Claims';
    $page_keywords     = '';
    $page_description  = '';
?>
<?php include VIEWS. '/members/header.php' ?>

<div id="content">
    <div class="page-header">
        <h1 class="left"><?= $show_hidden == '1' ? 'Hidden':'' ?> Carrier Policies/Recipients</h1>
        <div class="page-options">
            <div class="page-dialog-background"></div>
            <a href="/members/claims/carriers/edit.php" class="add-item">Add Carrier Policy/Recipient</a> |
            <a href="/members/claims/index.php">Back to Claims</a> |
            <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a>
        </div>


        <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '3' : '2' ?> filter-carriers" >
        <form action="index.php" id="filter" method="get">
        <div class="dialog-sections">

                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                    <div class="dialog-section">
                        <h3>Locations</h3>
                        <?= html::location_filter('carriers', $model['join_licensed_locations_id']) ?>
                    </div>
                <? } ?>
                <div class="dialog-section dates">
                    <h3>Effective Date</h3>
                    
                    <label>From:</label><?= html::date($model, 'carriers_start_time', array()) ?><br>
                    <label>To:</label><?= html::date($model, 'carriers_end_time', array()) ?>
                </div>
                <div class="dialog-section">
                    <h3>Display Options</h3>
                    <label><?= html::radio(0, 'carriers_show_hidden', $model) ?> Show Active Policies/Recipients</label><br>
                    <label><?= html::radio(1, 'carriers_show_hidden', $model) ?> Show Hidden Policies/Recipients</label>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::submit("Clear Filter", array('name' => 'clear')) ?>
            </div>
    </form>
    </div>
    </div>

    <style>
        .carriers-tabs {
            height: 36px;
            border-bottom: 1px solid #aaa;
        }
        .carriers-tabs .tab-title {
            width: 15%;
            cursor: pointer;
        }
        .tab-title .title {
            display: inline-block;
            width: 100%;
            height: 26px;
            color: #333;
            text-decoration: none;
            font-weight: bold;
            padding-top: 10px;
            text-shadow: none;
        }
        .carriers-tabs .activetab .title {
            color:#3386cb;
        }
    </style>
    <div class="carriers-tabs">
        <div class="tab-title" data-report-type="Carriers"><span class="title">Carrier Policies</span></div>
        <div class="tab-title" data-report-type="Recipients"><span class="title">Recipients</span></div>
    </div>
    
    <div id="Carriers" class="tab-content" style="display:none">
<h2 class="left">Carrier Policies</h2>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <th><?= strings::sort('carriers_name', 'Carrier Name') ?></th>
            <th style="max-width: 150px"><?= strings::sort('carriers_contact', 'Contact') ?></th>
            <? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
                <th style="max-width: 150px">Location</th>
            <? } ?>
            <th>Coverage</th>
            <th><?= strings::sort('carriers_policy_number', 'Policy #') ?></th>
            <th><?= strings::sort('carriers_effective_start_date', 'Effective Date') ?></th>
            <th><?= strings::sort('carriers_fax', 'Fax #') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <?php if(!$carriers) { 
            if (sizeof($params) < 2) {
        ?>
        <? if( $show_hidden != '1'): ?><tr><td colspan="7" align="center">You have not entered a policy yet</td></tr>
        <? else: ?><tr><td colspan="7" align="center">No Hidden Policies.</td></tr><? endif; ?>
        <?  } 
            else {
        ?>
        <tr><td colspan="7" align="center">Search did not turn up anything</td></tr>
        <?  }
        }
        ?>
        <?php foreach($carriers as $carrier): ?>
        <tr>
            <td><?= $carrier['carriers_name'] ?></td>
            <td><?= $carrier['carriers_contact'] ?></td>
            <? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
             <td><? if($carrier['carriers_all_locations'] == 1){
                        $location = 'All Locations';
                    }else{
                        $location_ids = carriers::get_carrier_locations_ids($carrier['carriers_id']);
                        $location= licensed_locations::getLocationNames($location_ids);
                    }
             
                    echo $location;
             ?></td>
            <? } ?>
            <td>
                <? if($carrier['carriers_coverages_list']): ?>
                    <ul>
                        <? foreach(explode(',',$carrier['carriers_coverages_list']) as $coverage): ?>
                            <? if($coverage == 'Other'){?>
                            <li><?= $coverage .' ( '.$carrier['carriers_coverages_other']. ' )' ?> </li>
                            <?}else{?>
                                <li><?= $coverage ?> </li>
                            <?}
                        endforeach; ?>
                    </ul>
                <? endif; ?>
            </td>

            <td><?= $carrier['carriers_policy_number'] ?></td>
            <td>
                <? if(!isset($carrier['carriers_effective_start_date']) ) { ?>
                <b>Please enter date</b>
                <? } else { ?>
                <?= date('n/j/Y', strtotime($carrier['carriers_effective_start_date'])); ?>-<br>
                <?= date('n/j/Y', strtotime($carrier['carriers_effective_end_date'])); ?>
                <? } ?>
            </td>
            <td align="center"><?= $carrier['carriers_fax'] ?></td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if (carriers::can_user_edit_carrier_recipient($carrier['carriers_id'])) : ?>
                            <li><a href="/members/claims/carriers/edit.php?renew&carriers_id=<?= $carrier['carriers_id'] ?>">Renew</a>
                            </li>
                            <li><a href="/members/claims/carriers/edit.php?carriers_id=<?= $carrier['carriers_id'] ?>">Edit</a>
                            </li>
                            <? if ($carrier['carriers_hidden']): ?>
                                <li>
                                    <a href="/members/claims/carriers/hide.php?carriers_id=<?= $carrier['carriers_id'] ?>&hide_carrier=0">Un-Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/carriers/hide.php?carriers_id=<?= $carrier['carriers_id'] ?>&hide_carrier=1">Hide</a>
                                </li>
                            <? endif; ?>
                             <li><a href="#" class="history" id="history_<?= $carrier['carriers_id'] ?>">History</a></li>
                            <li><a href="/members/claims/carriers/delete.php?carriers_id=<?= $carrier['carriers_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                        <? else: ?>
                            <li><a href="/members/claims/carriers/edit.php?carriers_id=<?= $carrier['carriers_id'] ?>">View</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>
    </table><br class='clear'/>
    </div>

    <div id="Recipients" class="tab-content" style="display:none">
   <h2 class="left">Recipients</h2> 
        <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <th><?= strings::sort('carriers_name', 'Recipient') ?></th>
            <th style="width: 150px"><?= strings::sort('carriers_contact', 'Contact') ?></th>
            <? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
            <th style="width: 150px">Location</th>
            <? } ?>
            <th style="width:55px">Actions</th>
        </tr>
        <?php if(!$recipients) { 
            if (sizeof($params) < 2) {
        ?>
        <? if( $show_hidden != '1'): ?><tr><td colspan="4" align="center">You have not entered a recipient yet</td></tr>
        <? else: ?><tr><td colspan="4" align="center">No Hidden Recipients.</td></tr><? endif; ?>
        <?  } 
            else {
        ?>
        <tr><td colspan="4" align="center">Search did not turn up anything</td></tr>
        <?  }
        }
        ?>
        <?php foreach($recipients as $carrier): ?>
        <tr>
            <td><?= $carrier['carriers_name'] ?></td>
            <td><?= $carrier['carriers_contact'] ?></td>
            <? if(ActiveMemberInfo::IsUserMultiLocation()) { ?>
            <td><?
                    $location = '';
                    if($carrier['carriers_all_locations'] == 1){
                        $location = 'All Locations';
                    }else{
                        $location_ids = carriers::get_linked_email_recipient_locations_ids($carrier['carriers_id']);
                        $location = licensed_locations::getLocationNames($location_ids);
                    }
             
                    echo $location;
             ?></td>
            <? } ?>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if (carriers::can_user_edit_carrier_recipient($carrier['carriers_id'])) : ?>
                            <li><a href="/members/claims/carriers/edit.php?carriers_id=<?= $carrier['carriers_id'] ?>">Edit</a>
                            </li>
                            <? if ($carrier['carriers_hidden']): ?>
                                <li>
                                    <a href="/members/claims/carriers/hide.php?carriers_id=<?= $carrier['carriers_id'] ?>&hide_carrier=0">Un-Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/carriers/hide.php?carriers_id=<?= $carrier['carriers_id'] ?>&hide_carrier=1">Hide</a>
                                </li>
                            <? endif; ?>
                            <li><a href="#" class="history" id="history_<?= $carrier['carriers_id'] ?>">History</a></li>
                            <li><a href="/members/claims/carriers/delete.php?carriers_id=<?= $carrier['carriers_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                        <? else: ?>
                            <li><a href="/members/claims/carriers/edit.php?carriers_id=<?= $carrier['carriers_id'] ?>">View</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>
    </table>
    </div>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/carriers_recipients_list.js?v=20180618"></script>

<?php include VIEWS. '/members/footer.php' ?>
