<style>
    .tab-content {
        display: none;
    }
    .templates {
        padding-top: 5px;
        padding-bottom: 5px;
    }
</style>
<?
function display_description($description){
    $word_count = str_word_count($description, 1);
    $des =  implode(' ', array_slice(str_word_count($description, 2), 0, 15));
    if(count($word_count)>15){
        $des = $des .'...';
    }
    return $des;
}

function carrier_details($claim_carriers){
    $carrier_data = array();
    $submitted_dates = array();
    $incurred = array();
    foreach($claim_carriers as $carriers){
        $carrier_names[] = $carriers['carriers_name'];
        $incurred[] = $carriers['claims_x_carriers_amount_paid']+$carriers['claims_x_carriers_amount_reserved'];
        if($carriers['claims_x_carriers_datetime_submitted']) {
            $submitted_dates[] = $carriers['claims_x_carriers_datetime_submitted'];
        }
    }
    sort($submitted_dates);
    $carrier_data['carriers_name'] = (count($carrier_names) >0)? implode(", ",$carrier_names):'';
    $carrier_data['incurred_amount'] = (count($incurred) >0)? array_sum($incurred):'';
    $carrier_data['submitted_dates'] =  (count($submitted_dates) >0)?$submitted_dates[0]:'';
    return $carrier_data;
}
?>
<!-- the ids of the tab-content divs must match the values in models/members/claims/claims_reports.php  -->
<? $show_location = ActiveMemberInfo::IsUserMultiLocation(); 

if(UserPermissions::UserCanAccessScreen('workers_comp')) {
?>
 
<div class="tab-content" id="WorkersComp">
    <div class="templates">
        <strong>Incident Templates:</strong><br/>
        <a href="/downloads/claims_forms/work_comp/WC Injury Report.pdf" target="_blank">Worker's Comp Injury Report</a>
    </div>
    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claim_workers_comp_employee_name', 'Claimant') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>           
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <? 
        if(!empty($workcomp_claims)){
        foreach ((array)$workcomp_claims as $claim):
            $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
            $carrier_data = carrier_details($claim_carriers);
            ?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= strings::wordwrap($claim['claim_workers_comp_employee_name'], 9, '&shy;', true) ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                </td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; 
        }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
    </table>
</div>
<? }?>
<div class="tab-content" id="General">
    <div class="templates">
        <strong>Incident Templates:</strong><br />
        <a href="/downloads/claims_forms/property_3rd_party/GL-Guest-Third Party - Incident Report.pdf" target="_blank">GL-Guest-Third-Party Property-Injury or Illness Incident Report</a>
        <? /*<a href="/downloads/claims_forms/property_3rd_party/INCIDENT REPORTS Revised 2011.doc" target="_blank">INCIDENT REPORTS Revised 2011</a>*/ ?>
    </div>
    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claim_general_liab_claimant', 'Claimant') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <? 
        if(!empty($liability_claims)){
            foreach((array)$liability_claims as $claim):
                $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
                $carrier_data = carrier_details($claim_carriers);
                ?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= strings::wordwrap($claim['claim_general_liab_claimant'], 9, '&shy;', true) ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach;  
         }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>

<div class="tab-content" id="Auto">
    <div class="templates">
        <strong>Incident Templates:</strong><br />
        <a href="/downloads/claims_forms/auto/AUTO Incident Report.pdf" target="_blank">AUTO incident reports</a>
    </div>
    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claim_auto_claimant', 'Claimant') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <? 
        if(!empty($auto_claims)){
        foreach((array)$auto_claims as $claim):
            $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
            $carrier_data = carrier_details($claim_carriers);
            ?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= $claim['claim_auto_claimant'] ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach;  }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>

<div class="tab-content" id="Property">
    <div class="templates">
        <strong>Incident Templates:</strong><br />
        <a href="/downloads/claims_forms/property_3rd_party/Company Property Incident Report.pdf" target="_blank">Company Property Incident Report</a>
    </div>
    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <? 
        if(!empty($property_claims)){
            foreach((array)$property_claims as $claim):
                $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
                $carrier_data = carrier_details($claim_carriers);
            ?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach;  
         }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>
<? if(UserPermissions::UserCanAccessScreen('labor_claims')) {?>
<div class="tab-content" id="Labor">

    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <? 
        if(!empty($labor_claims)){
            foreach((array)$labor_claims as $claim): 
                $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
                $carrier_data = carrier_details($claim_carriers);
                ?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach;  
         }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>
<? }
if(UserPermissions::UserCanAccessScreen('other_claims')) {?>

<div class="tab-content" id="Other">
    <br class="clear" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <? if($show_location): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('claims_datetime', 'DOI', null, null, null, 'DESC') ?></th>
            <th><?= strings::sort('claim_general_liab_claimant', 'Claimant') ?></th>
            <th><?= strings::sort('claims_status_code_description', 'Status') ?></th>
            <th>Description</th>
            <th class="sidebar_optional" >Total<br/>Incurred</th>
            <th><?= strings::sort('claims_datetime_submitted', 'Date<br />Submitted') ?></th>
            <th><?= strings::sort('carriers_name', 'Carrier') ?></th>
            <th style="width:55px">Actions</th>
        </tr>
        <?
        if(!empty($other_claims)){
        foreach((array)$other_claims as $claim): 
            $claim_carriers = Claims::get_claim_x_carriers($claim['claims_id']);
                $carrier_data = carrier_details($claim_carriers);?>
            <tr>
                <? if($show_location): ?>
                    <td><?= get_corporation_display_name($claim); ?></td>
                <? endif; ?>
                <td><?= date('m/d/Y', strtotime($claim['claims_datetime'])) ?></td>
                <td><?= strings::wordwrap($claim['claim_general_liab_claimant'], 9, '&shy;', true) ?></td>
                <td><?= $claim['claims_status_code_description'] ?></td>
                <td><? echo $description = display_description($claim['claims_description'])?>
                <td class="sidebar_optional" >$<?= !empty($carrier_data['incurred_amount'])? number_format(($carrier_data['incurred_amount']), 2, '.', ''):'' ?></td>
                <td><?= !empty($carrier_data['submitted_dates']) ? date('m/d/Y', strtotime($carrier_data['submitted_dates'])) : '' ?></td>
                <td><?= $carrier_data['carriers_name'] ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/claims/edit.php?claims_id=<?= $claim['claims_id'] ?>">Edit</a></li>
                            <li><a href="#" class="notes" id="notes_<?= $claim['claims_id'] ?>">Notes</a></li>
                            <li><a href="#" class="history" id="history_<?= $claim['claims_id'] ?>">History</a></li>
                            <?php if($claim['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $claim['claims_id'] ?>">Email / Fax History</a></li>
                            <?php } ?>
                            <li><a href="#" class="files" id="files_<?= $claim['claims_id'] ?>">Files</a></li>
                            <li><a href="/members/claims/delete.php?claims_id=<?= $claim['claims_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($claim['claims_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/claims/hide.php?claims_id=<?= $claim['claims_id'] ?>&hide_claim=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; 
        }else{?>
            <tr>
                <td colspan="11" style="padding-left:35%;">
                    <b>Please adjust the filters to see more results</b> 
                </td>
            </tr> 
       <? }
        ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>
<? }?>
