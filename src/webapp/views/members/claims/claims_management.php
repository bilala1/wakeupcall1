<fieldset class="j-toggle-parent j-toggle-collapse-initial j-toggle-collapsed">
<legend>Claim Management <a class="j-toggle-trigger"></a></legend>
<div class="j-toggle-content">
    <div class='col-50'>
        <?= html::textfield('Adjusters Name: ', $carriers['claims_x_carriers_adjusters_name'], 'claims_x_carriers_adjusters_name[]', array('maxlength' => '50'), $errors) ?>
        <?= html::textfield('Adjusters Email: ', $carriers['claims_x_carriers_adjusters_email'], 'claims_x_carriers_adjusters_email[]', array('maxlength' => '50'), $errors) ?>
        <?= html::textfield('Adjusters Phone: ', $carriers['claims_x_carriers_adjusters_phone'], 'claims_x_carriers_adjusters_phone[]', array('maxlength' => '50'), $errors) ?>
        <?= html::textfield('Claim Number: ', $carriers['claims_x_carriers_claim_number'], 'claims_claim_number[]', array('maxlength' => '50'), $errors) ?>
        <? 
        if($carriers['claims_x_carriers_datetime_submitted']) {
        $claims_carrier_status_values = array(
            Claims::$SUBMIT_OPEN => 'Submitted - Open',
            Claims::$SUBMIT_CLOSE => 'Submitted - Closed');
        }else{
            $claims_carrier_status_values = array(
            '' => 'Please Select...',
            claims::$SUBMIT_OPEN => 'Submitted - Open',
            claims::$SUBMIT_CLOSE => 'Submitted - Closed',
            claims::$RECORD_ONLY => 'Record Only - Not submitted',
            claims::$FIRST_AID =>'First Aid Only - Not submitted');
        }
        ?>
        <? if ($claim['join_claims_status_codes_id'] == Claims::$PENDING) { ?>
        <?= html::selectfield('Status:', $claims_carrier_status_values, 'join_status_codes_id[]', $carriers['join_claims_status_codes_id'],
            array('disabled' => 'disabled'), $errors); ?>
        <? } else { ?>
            <?= html::selectfield('Status:', $claims_carrier_status_values, 'join_status_codes_id[]', $carriers['join_claims_status_codes_id'], array(),
                $errors); ?>
        <? } ?>

        <?= html::textfield('Date Submitted:', !empty($carriers['claims_x_carriers_datetime_submitted']) ? date('n/j/Y',
            strtotime($carriers['claims_x_carriers_datetime_submitted'])) : '', 'claims_x_carriers_datetime_submitted[]', array('id'=>'claims_x_carriers_datetime_submitted'), $errors) ?>

        <?= html::textfield('Date Closed:', !empty($carriers['claims_x_carriers_datetime_closed']) ? date('n/j/Y',
            strtotime($carriers['claims_x_carriers_datetime_closed'])) : '', 'claims_x_carriers_datetime_closed[]', array('id'=>'claims_x_carriers_datetime_closed'), $errors) ?>
    </div>
    <div class='col-50'>
        <div class="claim_type claim_workers_comp">
        <?= html::textfield('Medical Costs: ',  money_format('%n', $carriers['claims_x_carriers_workers_comp_medical_costs']), 'claim_workers_comp_medical_costs[]',
            array('maxlength' => '20'), $errors) ?>
        </div>

        <?= html::textfield('Amount Paid:', money_format('%n', $carriers['claims_x_carriers_amount_paid']), 'claims_amount_paid[]',
            array(), $errors) ?>

        <?= html::textfield('Amount Reserved:', money_format('%n', $carriers['claims_x_carriers_amount_reserved']),
            'claims_amount_reserved[]', array(), $errors) ?>

        <?= html::textfield('Total Incurred:', money_format('%n', $claims_amount_incurred),
            'claims_amount_incurred[]', array('readonly' => 'readonly'), $errors) ?>
    </div>
</div>
</fieldset>