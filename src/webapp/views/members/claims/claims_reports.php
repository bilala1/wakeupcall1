<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 12/22/14
 * Time: 6:05 PM
 */

include MODELS . '/members/claims/claims_reports.php';


$report = ClaimReports::$list[$_GET["reportid"]];

$includeHidden = false;
if (isset($_GET["includeHidden"]) ) {
    $includeHidden = true;
}


// We can pass the chart type as a query param (for the single page)
// if it's not there, then use the default chart configured
// on the report object.
$reportType = $report['chartType'];

if (isset($_GET["chartType"])) {
    $reportType = $_GET["chartType"];
}

// For the dialog we get the claim start/end time from the page form
// with the id claims_start_time/claims_end_time
// for the single page, the values come from the dialog form
// with ids claims_start_time_dialog/claims_end_time_dialog
if ( isset($_GET["claims_start_time"])) {
    $claim_start_time = $_GET['claims_start_time'];
}
if ( isset($_GET["claims_start_time_dialog"])) {
    $claim_start_time = $_GET["claims_start_time_dialog"];
}
if(!isset($claim_start_time)) {
    $claim_start_time = date('m/d/Y', strtotime("-1 years"));
}

if ( isset($_GET["claims_end_time"])) {
    $claim_end_time = $_GET['claims_end_time'];
}
if ( isset($_GET["claims_end_time_dialog"])) {
    $claim_end_time = $_GET["claims_end_time_dialog"];
}
if(!isset($claim_end_time)) {
    $claim_end_time = date('m/d/Y');
}
if ( isset($_GET["join_claims_status_codes_id"])) {
    $join_claims_status_codes_id = $_GET['join_claims_status_codes_id'];
}
?>
<div id="content">
    <style scoped="">
        .report-button {
            height: 30px;
            padding: 0 5px 0 5px;
            margin-left: 2px;
        }
        form {
            display: table;
            width: 100%;
        }
        form .report-param {
            padding: 5px 5px 5px 5px;
        }
        <? if( !ActiveMemberInfo::IsUserMultiLocation() ) : ?>
        #location-drop-down {
            display:none;
        }
        <? endif ?>
    </style>
    <h1><?= $report['title']; ?></h1>
    <form id="report-form" class="report-form" data-url="<?=$report['url']; ?>" data-chart-type="<?=$reportType; ?>">
        <input type="hidden" name="reportid" value="<?=$_GET["reportid"]; ?>" />
        <div style="display:table-cell;">
        <? 
        foreach($report['params'] as $param) { ?>
            <div class="report-param">
            <? switch($param) {
                case 'locations':
                    echo '<div id="location-drop-down"><label>Locations:</label>';
                    echo reportHelpers::locations();
                    echo '</div>';
                    break;
                case 'fromDate':
                    echo '<label>From:</label>';
                    echo reportHelpers::fromDate($claim_start_time,"claims_start_time_dialog");
                    break;
                case 'toDate':
                    echo '<label>To:</label>';
                    echo reportHelpers::toDate($claim_end_time, "claims_end_time_dialog");
                    break;
                case 'claimType':
                    echo '<label>Claim Type:</label>';
                    echo reportHelpers::claimTypes();
                    break;
                case 'filterHidden':
                    echo '<label>Include Hidden Claims:</label>';
                    echo reportHelpers::includeHidden($includeHidden);
                    break;
                case 'Status':
                    echo '<label>Status:</label>';
                    echo reportHelpers::status($join_claims_status_codes_id);
                    break;
            }
            echo '</div>';
        }
        if($report['staticParams']){
            foreach($report['staticParams'] as $key => $value) {
                echo '<input type="hidden" name ="'.$key.'" value="'.$value.'" />';
            }
        }
        echo '<input id="reportTitleString" type="hidden" name ="title" value="'.urlencode($report['title']).'" />';
        ?>
        </div>
        <div style="display:table-cell">
            <div style="<?=in_array("chartType",$report['params']) ? "" : "display:none;"?>" >
                <div class="radio">
                    <label>
                        <input type="radio" name="chartType" id="chartTypeBar" value="column" <?= $reportType == 'column' ? "checked" : "";?> >
                        Bar
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="chartType" id="chartTypePie" value="pie" <?= $reportType == 'pie' ? "checked" : "";?>>
                        Pie
                    </label>
                </div>
            </div>
            <div id="button-area" style="padding-top: 10px;">
                <button class="report-button" id="reportRefresh">Refresh</button>
                <button class="report-button" id="reportPrint">Print</button>
                <button id="chartDownload" class="report-button">Download</button>
                <button id="saveToFile" class="report-button">Save To My Files</button>
            </div>
        </div>
    </form>
    <div id="chart_div" style="height: 500px;"></div>
</div>


