<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = 'Claims';
$page_keywords = '';
$page_description = '';
function limitWrods($string){
    $wc = str_word_count($string);
    if($wc > 30){
        $array = explode(" ", $string);
        array_splice($array, 30);
        $string = implode(" ", $array)." ...";
    }
    return($string);
}
?>
<? include VIEWS . '/members/header.php' ?>
<style>
    .col-50{ float: left; width: 50%;}
</style>

<div id="content">
    <? if ($errors['claims_file']): ?>
        <p class="errors"><?= $errors['claims_file'] ?></p>
    <? endif ?>
    <h1 class="left"><?= $_REQUEST['claims_id'] ? 'Edit' : 'Add' ?> Claim</h1>

    <div class="page-options"><a href="/members/claims/carriers/index.php">Manage Carrier Policies/Recipients</a> | <a
            href="/members/claims/index.php">Return to Claims List</a></div>
    <br clear="all"/>

    <form action="/members/claims/edit.php" name="editClaim" id="editClaim" method="post"
          onSubmit="return chkClaimForm(this)" enctype="multipart/form-data">
        <?= html::hidden($_REQUEST, 'claims_id') ?>

        <fieldset class="j-toggle-parent">
            <div class="j-toggle-summary">
               <?= html::plainTextField("Location", $location['licensed_locations_name']?$location['licensed_locations_name']:'',"summary_locations_name") ?>
               <?= html::plainTextField("Type", $claim['claims_type']?$claim['claims_type']:'',"summary_claims_type") ?>
               <?
               $employee_name = '';
               if($claim['claims_type'] == 'Workers Comp'){
                   $employee_name = $claim['claim_workers_comp_employee_name'];
               }elseif($claim['claims_type'] == 'General Liability / Guest Property'){
                   $employee_name = $claim['claim_general_liab_claimant'];
               }elseif($claim['claims_type'] == 'Auto'){
                   $employee_name = $claim['claim_auto_claimant'];
               }elseif($claim['claims_type'] == 'Your Property'){
                   $employee_name = '';
               }elseif($claim['claims_type'] == 'Labor Law'){
                   $employee_name = $claim['claim_employment_claimant'];
               }elseif($claim['claims_type'] == 'Other'){
                   $employee_name = $claim['claim_general_liab_claimant'];
               }
               ?>
               <?= html::plainTextField("Employee Name", $employee_name,"summary_claims_employee") ?>
               <?= html::plainTextField("Date of incident", !empty($claim['claims_datetime']) ? date('n/j/Y', strtotime($claim['claims_datetime'])) : '',"summary_claims_date") ?>
            </div>
            <legend>Incident Details <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
               <div>
                    <label>Location: </label>
                    <?= $location['licensed_locations_name'] ?>
                    <?= html::hidden($claim['join_licensed_locations_id'], 'licensed_locations_id') ?>
                    <?= html::hidden($claim['join_licensed_locations_id'], 'join_licensed_locations_id') ?>
                    <? //since we check for location id licensed_locations_id in js and join_licensed_locations_id in php
                    // this is stored in two hidden fields?>
                </div>
                <div class="form-field" <?= $_REQUEST['claims_id'] ? 'style="display:none"' : '' ?>>
                     <?= html::multiradio('Type:', 'claims_type',
                         arrays::values_as_keys($claim_types), $claim, $errors); ?>
                </div>
                <? if ($_REQUEST['claims_id']): ?>
                    <div class="form-field">
                        <label>Type:</label>
                        <?= $claim['claims_type'] ?>
                    </div>
                <? endif ?>
                <div id="type_error" class="claims_error" style="display:none; margin-left:150px">Please select a type</div>

                <? if ($claim['join_claims_status_codes_id'] == Claims::$PENDING) { ?>
                    <?= html::selectfield('Status:', $claims_status_values, 'join_claims_status_codes_id', $claim,
                        array('disabled' => 'disabled'), $errors); ?>
                <? } else { ?>
                    <?= html::selectfield('Status:', $claims_status_values, 'join_claims_status_codes_id', $claim, array(),
                        $errors); ?>
                <? } ?>

                <?= html::textfield('Date of Incident:',
                    !empty($claim['claims_datetime']) ? date('n/j/Y', strtotime($claim['claims_datetime'])) : '',
                    'claims_datetime', array(), $errors) ?>

                <?= html::textfield('Date Notified:', !empty($claim['claims_datetime_notified']) ? date('n/j/Y',
                    strtotime($claim['claims_datetime_notified'])) : '', 'claims_datetime_notified', array(), $errors) ?>

                <?= html::textareafield('Brief Description of Event:<br>(Max 255 characters)', $claim, 'claims_description', array('maxlength' => '255', 'cols' => 30, 'rows' => 4), $errors) ?>

                <div class="claim_type claim_workers_comp" style="display:none">
                    <?= html::textfield('Employee Name:', $claim, 'claim_workers_comp_employee_name',
                        array('maxlength' => 255), $errors) ?>
                    <?= html::textfield('Job Title:', $claim, 'claim_workers_comp_job_title', array('maxlength' => 255),
                        $errors) ?>
                    <?= html::selectfield('Department Where Employee Works:',
                        array('' => 'Please Select...') + $departments, 'claim_workers_comp_employee_department', $claim,
                        array(), $errors); ?>
                    <?= html::selectfield('Department Where Event Occurred:',
                        array('' => 'Please Select...') + $departments, 'claim_workers_comp_event_department', $claim,
                        array(), $errors); ?>
                    <?= html::selectfield('Event Type:',
                        array('' => 'Please Select...', 'Injury' => 'Injury', 'Illness' => 'Illness'),
                        'claim_workers_comp_event_type', $claim, array(), $errors); ?>
                    <div class="inj_ill inj_ill_Injury" style="display:none">
                        <div class="form-field">
                            <label for="">Injury Type:</label>
                            <span class="multiinput-container" id="inj_type">
                                <? foreach (array_chunk(arrays::sort(claims::$injury_types) + array('other' => 'Other'),
                                    ceil((count(claims::$injury_types) + 1) / 2)) as $injury_type_chunk): ?>
                                    <span>
                                    <? foreach ($injury_type_chunk as $injury_type): ?>
                                        <?= html::radio($injury_type, 'claim_workers_comp_injury_type', $claim,
                                            array('id' => 'claim_workers_comp_injury_type-' . http::encode_url($injury_type))) ?>
                                        <label
                                            for="claim_workers_comp_injury_type-<?= http::encode_url($injury_type) ?>"> <?= $injury_type ?></label>
                                            <? if ($injury_type == 'Other') { ?>
                                            <?= html::input($claim, 'claim_workers_comp_injury_type_other',
                                                array('maxlength' => 25, 'style' => 'display: none')) ?>
                                        <? } ?>
                                            <br/>
                                    <? endforeach ?>
                                    </span>
                                <? endforeach ?>
                            </span>
                            <? if ($errors['claim_workers_comp_injury_type']): ?>
                                <p class="form-error"><?= $errors['claim_workers_comp_injury_type'] ?></p>
                            <? endif ?>
                        </div>

                        <div class="form-field">
                            <label>Body Part:</label>
                            <div style="display: inline-block">
                                <span id="selected_body_parts"></span>
                                <a id="toggle_body_parts" href="#" style="margin:0 3em;"></a><br>
                                <span class="multiinput-container" id="body_part">
                                    <? foreach (array_chunk(arrays::sort(claims::$body_parts) + array('other' => 'Other'),
                                        ceil((count(claims::$body_parts) + 1) / 3)) as $body_part_chunk): ?>
                                        <span>
                                        <? foreach ($body_part_chunk as $body_part): ?>
                                            <input type="checkbox" class="body_part" name="claim_workers_comp_body_parts[]"
                                                   id="claim_workers_comp_body_parts-<?= http::encode_url($body_part) ?>"
                                                   value="<?= $body_part ?>" <?= in_array($body_part, explode(',',
                                                $claim['claim_workers_comp_body_parts'])) ? 'checked="checked"' : '' ?>><label
                                                    for="claim_workers_comp_body_parts-<?= http::encode_url($body_part) ?>"> <?= $body_part ?></label>
                                            <?php if($body_part == 'Other') {?>
                                                <?=html::input($claim,
                                                    'claim_workers_comp_body_parts_other',
                                                    array('maxlength' => 25, 'style' => 'display: none')) ?>
                                            <?php }?><br/>
                                        <? endforeach ?>
                                        </span>
                                    <? endforeach ?>
                                </span>
                                <? if ($errors['claim_workers_comp_body_parts']): ?>
                                    <p class="form-error"><?= $errors['claim_workers_comp_body_parts'] ?></p>
                                <? endif ?>
                            </div>
                        </div>

                    </div>
                    <div class="inj_ill inj_ill_Illness" style="display:none">
                        <div class="form-field">
                            <label for="">Illness Type:</label>
                            <span class="multiinput-container" id="ill_type">
                                <? foreach (array_chunk(arrays::sort(claims::$illness_types) + array('other' => 'Other'),
                                    ceil((count(claims::$illness_types) + 1) / 2)) as $illness_type_chunk): ?>
                                    <span>
                                    <? foreach ($illness_type_chunk as $illness_type): ?>
                                        <?= html::radio($illness_type, 'claim_workers_comp_illness_type', $claim,
                                            array('id' => 'claim_workers_comp_illness_type-' . http::encode_url($illness_type))) ?>
                                        <label
                                            for="claim_workers_comp_illness_type-<?= http::encode_url($illness_type) ?>"> <?= $illness_type ?></label>
                                            <? if ($illness_type == 'Other') { ?>
                                            <?= html::input($claim, 'claim_workers_comp_illness_type_other',
                                                array('maxlength' => 25, 'style' => 'display: none')) ?>
                                        <? } ?>
                                            <br/>
                                    <? endforeach ?>
                                </span>
                                <? endforeach ?>
                            </span>
                            <? if ($errors['claim_workers_comp_illness_type']): ?>
                                <p class="form-error"><?= $errors['claim_workers_comp_illness_type'] ?></p>
                            <? endif ?>
                        </div>
                    </div>

                    <fieldset class="j-toggle-parent">
                        <legend>OSHA Reporting <a class="j-toggle-trigger"></a></legend>
                        <div class="j-toggle-content">
                            <p class="callout">
                                <img src="/images/dialog-warning.png" class="icon">
                                Some incidents must be reported to OSHA within 8-24 hours. <a href="#" id="osha_popup">Check Chart Here</a>
                            </p>
                            <?= html::selectfield('Add To Osha Report:<a id="osha_include_help"><img border="0" alt="help?" src="../../images/dialog-question.png" width="12" height="12"></a>',
                                array('1' => 'Yes', '0' => 'No'), 'claim_workers_comp_osha_include', $claim, array(), $errors); ?>
                            <?= html::selectfield('Privacy Case:<a id="osha_privacy_help"><img border="0" alt="help?" src="../../images/dialog-question.png" width="12" height="12"></a>',
                                array('0' => 'No', '1' => 'Yes'), 'claim_workers_comp_osha_privacy', $claim, array(), $errors); ?>

                            <?= html::textfield('Case #:', $claim, 'claim_workers_comp_case_number', array('maxlength' => 255),
                                $errors) ?>

                            <div class="form-field" id="claim_workers_comp_classification">
                                <label style="width: auto">Classify the Case:</label>
                                (Please select the most serious outcome)<br>
                                <span class="multiinput-container" id="claim_workers_comp_classification" style="width:340px;">
                                    <div><?= html::checkboxfield('G - Death', 'work_comp_class_G',
                                            'claim_workers_comp_classification[]', $claim['claim_workers_comp_classification_G'] == 1,
                                            array('id' => 'work_comp_class_G')); ?></div>
                                    <div><?= html::checkboxfield('H - Days away from work', 'work_comp_class_H',
                                            'claim_workers_comp_classification[]', $claim['claim_workers_comp_classification_H'] == 1,
                                            array('id' => 'work_comp_class_H')); ?></div>
                                    <div><?= html::checkboxfield('I - Job transfer or restriction (remained at work)',
                                            'work_comp_class_I', 'claim_workers_comp_classification[]',
                                            $claim['claim_workers_comp_classification_I'] == 1,
                                            array('id' => 'work_comp_class_I')); ?></div>
                                    <div><?= html::checkboxfield('J - Other reportable cases (remained at work)', 'work_comp_class_J',
                                            'claim_workers_comp_classification[]', $claim['claim_workers_comp_classification_J'] == 1,
                                            array('id' => 'work_comp_class_J')); ?></div>
                                </span>
                            </div>
                        </div>
                    </fieldset>

                    <div
                        class="wc_classification_details_container_H wc_classification_details wc_classification_details_H">
                        <div class="form-field">
                            <label>
                                <span class="wc_classification_details_H">Date Away from Work:</span>
                            </label><?= html::date($claim, 'claim_workers_comp_datetime_away_start', array(), $errors) ?>
                            <?= html::errors('claim_workers_comp_datetime_away_start', $errors) ?>
                        </div>
                        <?= html::datefield('Date Return to Work Without Restrictions:', $claim, 'claim_workers_comp_datetime_away_end', array(),
                            $errors) ?>
                        <label><span>Total days away:</span></label><input id="claim_workers_comp_total_away" readonly/>
                    </div>
                    <div
                        class="wc_classification_details_container_I wc_classification_details wc_classification_details_I">
                        <div class="form-field">
                            <label>
                                <span class="wc_classification_details_I">Transferred/Restricted:</span>
                            </label><?= html::date($claim, 'claim_workers_comp_datetime_restricted_start', array(),
                                $errors) ?>
                            <?= html::errors('claim_workers_comp_datetime_restricted_start', $errors) ?>
                        </div>
                        <?= html::datefield('Date Return to Work Without Restrictions:', $claim, 'claim_workers_comp_datetime_restricted_end',
                            array(), $errors) ?>
                        <label><span>Total days away:</span></label><input id="claim_workers_comp_total_restricted"
                                                                           readonly/>
                    </div>

                </div>

                <div class="claim_type claim_general_liab" style="display:none">
                    <?= html::textfield('Claimant:', $claim, 'claim_general_liab_claimant', array('maxlength' => 255),
                        $errors) ?>

                    <div class="form-field">
                        <label for="">Area of Loss:</label>
                    <span class="multiinput-container" id="area_loss">
                        <? foreach (array_chunk(arrays::sort(claims::$loss_areas) + array('other' => 'Other'),
                            ceil((count(claims::$loss_areas) + 1) / 3)) as $loss_area_chunk): ?>
                            <span>
                            <? foreach ($loss_area_chunk as $loss_area): ?>
                                <label
                                    for="claim_general_liab_loss_area-<?= http::encode_url($loss_area) ?>">
                                    <input type="checkbox" name="claim_general_liab_loss_area[]"
                                       id="claim_general_liab_loss_area-<?= http::encode_url($loss_area) ?>"
                                       value="<?= $loss_area ?>" <?= in_array($loss_area,
                                    explode(',', $claim['claim_general_liab_loss_area'])) ? 'checked="checked"' : '' ?>>
                                 <?= $loss_area ?></label>
                                            <? if ($loss_area == 'Other') { ?>
                                    <?= html::input($claim, 'claim_general_liab_loss_area_other',
                                        array('maxlength' => 25, 'style' => 'display: none')) ?>
                                <? } ?>
                                <br/>
                            <? endforeach ?>
                            </span>
                        <? endforeach ?>
                    </span>
                        <? if ($errors['claim_general_liab_loss_area']): ?>
                            <p class="form-error"><?= $errors['claim_general_liab_loss_area'] ?></p>
                        <? endif ?>
                    </div>

                    <div class="form-field gen-liab-only">
                        <label for="">Type of Loss:</label>
                        <span class="multiinput-container">
                            <?php $loss_types = arrays::sort(claims::$loss_types_liab) + array('other' => 'Other');
                            foreach ($loss_types as $loss_type): ?>
                                <?= html::radio($loss_type, 'claim_general_liab_loss_type', $claim,
                                    array('id' => 'claim_general_liab_loss_type-' . http::encode_url($loss_type))) ?>
                                <label
                                        for="claim_general_liab_loss_type-<?= http::encode_url($loss_type) ?>"> <?= $loss_type ?></label>
                                    <? if ($loss_type == 'Other') { ?>
                                    <?= html::input($claim, 'claim_general_liab_loss_type_other',
                                        array('maxlength' => 25, 'style' => 'display: none')) ?>
                                <? } ?>
                                    <br/>
                            <? endforeach ?>
                            </span>
                        <? if ($errors['claim_general_liab_loss_type']): ?>
                            <p class="form-error"><?= $errors['claim_general_liab_loss_type'] ?></p>
                        <? endif ?>
                    </div>
                </div>

                <div class="claim_type claim_auto">
                    <?= html::textfield('Claimant:<div style="font-weight:normal">(Use your location if no third party)</div>',
                        $claim, 'claim_auto_claimant', array('maxlength' => 255), $errors) ?>
                    <div class="form-field">
                        <label for=""><span>Nature of Claim:</span></label><fieldset>
                            <? $autoNatures = arrays::values_as_keys(arrays::sort(claims::$natures)) + array("other" => "Other"); ?>
                            <? foreach ($autoNatures as $autoNature): ?>
                                <span><input type="radio" name="claim_auto_nature"
                                             id="claim_auto_nature-<?= http::encode_url($autoNature) ?>"
                                             value="<?= $autoNature ?>" <?= $autoNature == $claim['claim_auto_nature'] ? 'checked="checked"' : '' ?>><label
                                            for="claim_auto_nature-<?= http::encode_url($autoNature) ?>"
                                            style="width:auto"> <?= $autoNature ?></label>
                                <?php if($autoNature == 'Other') {?>
                                    <?=html::input($claim, 'claim_auto_nature_other') ?>
                                <?php }?>
                                </span><br clear="all"/>
                            <? endforeach ?>
                            <? if ($errors['claim_auto_nature']): ?>
                                <p class="form-error"><?= $errors['claim_auto_nature'] ?></p>
                            <? endif ?>
                        </fieldset>
                    </div>
                </div>

                <div class="claim_type claim_property">
                    <div class="form-field">
                        <label for=""><span>Type Of Loss:</span></label>
                    <fieldset id="claim_property_loss_types">
                        <? $loss_type_array = arrays::values_as_keys(arrays::sort(claims::$loss_types_property)) + array("other" => "Other"); ?>
                        <? foreach ($loss_type_array as $loss_type): ?>
                            <span><input type="checkbox" name="claim_property_loss_types[]"
                                         id="claim_property_loss_type-<?= http::encode_url($loss_type) ?>"
                                         value="<?= $loss_type ?>" <?= in_array($loss_type,
                                    explode(',', $claim['claim_property_loss_type'])) ? 'checked="checked"' : '' ?>><label
                                    for="claim_property_loss_type-<?= http::encode_url($loss_type) ?>"
                                    style="width:auto"> <?= $loss_type ?></label>
                            <? if ($loss_type == 'Other') { ?>
                                <?= html::input($claim, 'claim_property_loss_type_other',
                                    array('maxlength' => 25, 'style' => 'display: none')) ?>
                            <? } ?><br clear="all"/></span>
                        <? endforeach ?>
                        <? if ($errors['claim_property_loss_type']): ?>
                            <p class="form-error"><?= $errors['claim_property_loss_type'] ?></p>
                        <? endif ?>
                    </fieldset>
                    </div>
                </div>
                <div class="claim_type claim_labor_law">
                    <?= html::textfield('Name of employee filing complaint:', $claim, 'claim_employment_claimant',
                        array('maxlength' => 255), $errors) ?>
                    <?= html::textfield('Job Title:', $claim, 'claim_employment_job_title', array('maxlength' => 255),
                        $errors) ?>
                    <?= html::selectfield('Department Where Employee Works:',
                        array('' => 'Please Select...') + $departments, 'claim_employment_department', $claim,
                        array(), $errors); ?>
                    <?= html::textareafield('Where incident occured:', $claim, 'claim_employment_incident_location',
                            array('rows' => 5, 'cols' => 50)) ?>

                    <div id="other_parties" class="form-field">
                        <label><span>Other parties involved:</span></label>
                        <fieldset>
                         <? if(count($claim_employment_involved_parties)>0){
                                for($i = 0; $i < count($claim_employment_involved_parties);$i++){?>
                                    <input value="<?=$claim_employment_involved_parties[$i]['claim_employment_involved_parties_name']?>" name="claim_employment_involved_parties[]" id="claim_employment_involved_parties" maxlength="200" class="input-text j-otherText">
                                    <?
                                    if($i < count($claim_employment_involved_parties)-1) echo '<br>';
                                 }
                            }else{ ?>
                                <input value="" name="claim_employment_involved_parties[]" id="claim_employment_involved_parties" maxlength="200" class="input-text j-otherText">
                            <?}?>
                            <a href="#" id="add-another" class="add-item">Add another involved party</a>
                        </fieldset>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset class='j-toggle-parent'>
            <legend>Carriers <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
                <? if(is_array($selected_carriers)) foreach ($selected_carriers as $index => $carriers) { ?>
                    <div class='j-carrier j-toggle-parent carrier-container' id='carrierDetails<?= $index ?>'>
                        <h3><?= $carriers['carriers_name'] ?>
                            <a class="j-toggle-trigger"></a>
                            <a class="j-confirmDelete" href="#"
                               data-submittype='<?= $carriers['join_claims_status_codes_id'] ?>'
                               data-carrier_name='<?= $carriers['carriers_name'] ?>'
                               title="Are you sure you want to delete this Carrier?">
                                <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png"/>
                            </a></h3>

                        <div class="j-toggle-content">
                            <?
                            $claims_amount_incurred = $carriers['claims_x_carriers_amount_paid'] + $carriers['claims_x_carriers_amount_reserved'];
                            ?>
                            <div>
                                <div class='col-50'>
                                    <?= html::hidden($carriers['join_carriers_id'], 'join_carriers_id[]') ?>
                                    <?= html::hidden($carriers['claims_x_carriers_id'], 'claims_x_carriers_id[]') ?>
                                    <?= html::plainTextField("Carrier", $carriers['carriers_name']); ?>
                                    <?
                                    $coverage_list_array = explode(',', $carriers['carriers_coverages_list']);
                                    foreach ($coverage_list_array as $key => $coverage):
                                        if ($coverage == 'Other') {
                                            $coverage_list_array[$key] = $coverage . ' ( ' . $carriers['carriers_coverages_other'] . ' )';
                                        }
                                    endforeach; ?>
                                    <?= html::plainTextField("Coverage", implode(" , ", $coverage_list_array)) ?>

                                </div>
                                <div class="col-50">
                                    <?= html::plainTextField("Policy Effective Date",
                                        Formatting::date($carriers['carriers_effective_start_date'], 'Not Set')) ?>
                                    <?= html::plainTextField("Expiration Date",
                                        Formatting::date($carriers['carriers_effective_end_date'], 'Not Set')) ?>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <? include VIEWS . '/members/claims/claims_management.php' ?>
                        </div>
                    </div>
                <? } ?>
                <?
                //this section is to display one carrier always
                if (count($selected_carriers) == 0) {
                    ?>
                    <div class='j-carrier j-toggle-parent carrier-container' id='carrierDetails1'>
                        <h3>New Carrier
                            <a class="j-toggle-trigger"></a>
                            <a class="j-confirmDelete" data-submit_type=''
                               data-claims_x_carriers_id="0" title="Are you sure you want to delete this carrier?"
                               href="#">
                                <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png"/>
                            </a></h3>

                        <div class="j-toggle-content">
                            <?= html::selectfield('Select Carrier/Policy:', $carriers_for_location,
                                'join_carriers_id[]', $claim, array(
                                    'id' => 'join_carriers_id',
                                    'style' => 'width:680px; max-width:680px;',
                                    'class' => 'join_carriers_id'
                                ), $errors); ?>
                            <? include VIEWS . '/members/claims/claims_management.php' ?>
                        </div>
                    </div>
                <? } ?>
                <!--The below section is added so that even if we delete all carriers, we can add a new one-->
                <div id="carrier_info_template" style="display:none;">
                    <div class='j-carrier j-toggle-parent carrier-container' style="display:none;" id='carrierDetails'>
                        <h3>New Carrier
                            <a class="j-toggle-trigger"></a>
                            <a class="j-confirmDelete" data-submit_type=''
                               data-claims_x_carriers_id="0" title="Are you sure you want to delete this carrier?"
                               href="#">
                                <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png"/>
                            </a></h3>

                        <div class="j-toggle-content">
                            <div>
                                <?= html::selectfield('Select Carrier/Policy:', $carriers_for_location,
                                    'join_carriers_id[]', $claim, array(
                                        'id' => 'join_carriers_id[]',
                                        'style' => 'width:680px; max-width:680px;',
                                        'class' => 'join_carriers_id'
                                    ), $errors); ?>
                            </div>
                            <?
                            $carriers = array();
                            include VIEWS . '/members/claims/claims_management.php' ?>
                        </div>
                    </div>
                </div>
                <p class="add_another_carrier">
                    <a href="#" id='add_carrier' class="add-item">Add Another Carrier</a>
                </p>
            </div>
        </fieldset>

        <fieldset class="j-toggle-parent">
            <legend>Files and Documents <a class="j-toggle-trigger"></a></legend>
            <div class="file-container j-toggle-content">
                <table id="uploaded-files" class="file-table">
                    <tr>
                        <th><input id="submitAll" type="checkbox"/> Submit</th>
                        <th>File Name</th>
                        <th>Last Submitted to Carrier</th>
                        <th>Actions</th>
                    </tr>
                    <? if ($claim_files) {
                        foreach ($claim_files as $file): ?>
                            <? $filename = $file['files_name'];
                            if (strlen($filename) > 50) {
                                $filename = substr($filename, 0, 40) . '...' . substr($filename, -7);
                            }
                            ?>
                            <tr>
                                <td><input type="checkbox" name="submit-existing-doc[]"
                                           value="<?= $file['claims_files_id'] ?>"/></td>
                                <td>
                                    <a href="/members/claims/download.php?claims_id=<?= $_REQUEST['claims_id'] ?>&claims_files_id=<?= $file['claims_files_id'] ?>"
                                       title="<?= $file['files_name'] ?>"
                                       target="_blank"><?= $filename ?></a></td>
                                <td><?= !empty($file['claims_files_submitted']) ? date('n/j/Y',
                                        strtotime($file['claims_files_submitted'])) : '' ?></td>
                                <td>
                                    <a href="/members/claims/del-file.php?claims_id=<?= $_REQUEST['claims_id'] ?>&claims_files_id=<?= $file['claims_files_id'] ?>">Delete</a>
                                </td>
                            </tr>
                        <? endforeach;
                    } else { ?>
                        <td><input type="checkbox" name="submit-new-doc[0]"/></td>
                        <td><input type="file" name="claims_file[0]" class="j-file"></td>
                        <td></td>
                        <td><a href="#" onclick="deleteRow(event)">Delete</a></td>
                    <? } ?>
                </table>
                <a href="#" id="add_file" class="add-item add-file-link">Add another file</a>
            </div>
        </fieldset>
        <fieldset class='j-toggle-parent'>
            <legend>Claim Submission Notice <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
            <?
             $notify = 0;
            
            ?>
            <div class="form-field">
                <label>
                  Recipients automatically notified of this claim:
                </label>
                <fieldset>
                <?
                if(count($corporate_email_recipients)>0 ){
                $notify++;
                    foreach ((array)$corporate_email_recipients as $recipient) { 
                        
                        ?>
                        <div>
                    <input type='checkbox' checked=checked  disabled class='automatic'><?= $recipient['carriers_name'].' ('. $recipient['carriers_email'] .')';?><br>
                        </div>
                <?
                    }
                }
                if(count($recipients_tobe_notified_onsave)>0 && !isset($_REQUEST['claims_id']) ){
                    $notify++;
                    foreach ((array)$recipients_tobe_notified_onsave as $recipient) {?>
                    <div>
                    <input type='checkbox' checked=checked  disabled ><?= $recipient['carriers_name'].' ('. $recipient['carriers_email'].')' ;?><br>
                    </div>
                <?}
                }
                if(count($recipients_tobe_notified_onsubmit)>0 ){ 
                $notify++; ?>
                    <div id="notify_on_submission">
                       <?foreach ((array)$recipients_tobe_notified_onsubmit as $recipient) {?>
                        <div>
                            <input type='checkbox' checked=checked  disabled class='automatic'><?= $recipient['carriers_name'].' ('. $recipient['carriers_email'].')' ;?><br>
                        </div>
                       <?}?> 
                    </div>
                
               <? }
                ?>
                    <?php if($notify == 0) { ?>No automatic recipients set up for this location<?php } ?>
                </fieldset>
            </div>
            <?if($notify > 0 ){?>
                <div class="form-field">
                <label>
                  Send submitted files to automatic recipients:
                </label>
                <fieldset> <label>
                    <div>
                    <input type='checkbox' name="claim_send_files_to_recipients"><br>
                    </div>
                </fieldset>
                </div>
            <? } ?>
                
            <div id='submission_info'>
                <? if ($_REQUEST['claims_id'] && $claim['join_carriers_id'] && $claim['join_carriers_id'] != 0){ 
                    $i=1;
                    foreach($selected_carriers as $carriers ){?>
                    <div class="form-field j-carrierSubmissionInfo" id='submission_info_<?=$carriers['carriers_id']?>'>
                        <label>Submit to carrier <i><?=$carriers['carriers_name']?></i>:</label><fieldset>
                            <? if(!empty($carriers['carriers_fax'])) {?>
                                <label><input class = "showOnSelect" id="carriers_fax_input-<?=$carriers['carriers_id']?>" type="checkbox" name="claims_submit_type_fax-<?=$carriers['carriers_id']?>" value="fax" onChange="unSelectOther(this,'<?=$carriers['carriers_name']?>','fax');">
                                    Fax (<span id="carriers_fax_display"><?= empty($carriers['carriers_fax']) ? "unknown" : $carriers['carriers_fax'] ?></span>)
                                </label>
                                <br>
                            <? } ?>
                            <? if(!empty($carriers['carriers_email'])) {?>
                            <label>
                                <input class = "showOnSelect" id="carriers_email_input-<?=$carriers['carriers_id']?>" type="checkbox" name="claims_submit_type_email-<?=$carriers['carriers_id']?>" value="email" onChange="unSelectOther(this,'<?=$carriers['carriers_name']?>','email');" >
                                Email (<span id="carriers_email_display"><?= empty($carriers['carriers_email']) ? "unknown" : $carriers['carriers_email'] ?></span>)
                            </label>
                            <? } ?>
                             <div id = "adjuster_info-<?=$carriers['carriers_id']?>">
                            <? if(!empty($carriers['claims_x_carriers_adjusters_email'])){?>
                                <label>
                                    <input class="showOnSelect" id="carriers_adjuster_input-<?=$carriers['carriers_id']?>" type="checkbox" name="claims_submit_type_adjuster-<?=$carriers['carriers_id']?>" value="email" onChange="unSelectOther(this,'<?=$carriers['claims_x_carriers_adjusters_name']?>','email');" >
                                    <span id="carriers_adjuster_name-<?=$carriers['carriers_id']?>"><?=$carriers['claims_x_carriers_adjusters_name'] ?> </span>(<span id="carriers_adjuster_display-<?=$carriers['carriers_id']?>"><?=$carriers['claims_x_carriers_adjusters_email'] ?></span>)
                                </label>
                            <? } ?>
                            </div>
                            <div id="claims_submit_type_err"></div>
                        </fieldset>
                    </div>
                <? 
                $i++;
                }               
                    }?>
              <div id="claims_submit_type_err"></div>
            </div>

            
          
            <div class="form-field">
                <label for="send_copy_carriers_ids"><span>Additional Email Recipients:</label>
                <fieldset>
                    <select name="send_copy_carriers_ids[]" id="send_copy_carriers_ids" class='automatic'>
                        <option value=''>-none-</option>
                        <? foreach ($recipients_strings as $key => $carrier_string) {
                            echo "<option value='" . $key . "'>" . $carrier_string . "</option>";
                        }
                        ?>
                    </select>
                    <p style="margin-left:6px">
                    <a href="#" id="add_copy_carrier" class="add-item">Add e-mail recipient</a><br>
                    <b style="color:red">*</b>If you select a recipient(s), they will be e-mailed<br>
                    when you Save at the bottom of this form.</p>
                </fieldset>
            </div>
            <?= html::textfield('Email subject line', $claim, 'claims_email_subject', array(), $errors) ?>
            <?= html::selectfield("Email Template:", $templates, 'join_claims_email_templates_id', $claim['join_claims_email_templates_id'],  $customTemplate ? array('disabled' => 'disabled') : array(), $errors); ?>
            <div <?= $customTemplate ? "" : "style='display:none'" ?> id="email_template">
                <p class="callout">
                    <img src="/images/dialog-warning.png" class="icon">
                    When customizing this form, please do not alter the text within the brackets { }. These fields are automatically populated with information from your data. Altering or deleting these fields will omit this important data from the claim submission notice.</p>
                <textarea id="email_html" name="email_html" style="width:100%;height:280px;"><?= $templateHtml ?></textarea>
                <div class="form-actions">
                    <input type="button" class="btn-ok" value="Done Editing" id="collapseCustomEmail">
                    <input type="button" class="btn-delete" value="Delete Customization" id="deleteCustomEmail">
                </div>
            </div>

            <div class="form-actions">
                <input type="button" value="Preview Email" id="previewEmail" >
                <input <?= $customTemplate ? "style='display:none'" : "" ?> type="button" value="Customize Email" id="customizeEmail">
            </div>
            </div>
        </fieldset>
        <fieldset class="j-toggle-parent j-toggle-collapse-initial j-toggle-collapsed">
            <legend>Email / Fax History <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
              <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>To</th>
            <th>Subject</th>
            <th>Date</th>
            <th>Status</th>
            <th>Attachments</th>
            <th>Actions</th>
        </tr>
        <? foreach((array)$emails_history as $email): ?>
            <tr>
                <td><?= $email['sent_emails_email']?></td>
                <td><?= $email['sent_emails_desc'] ?></td>
                <td><?= date(DATE_FORMAT_FULL, strtotime($email['sent_emails_date'])) ?></td>
                <td><?= Formatting::emailStatus($email) ?></td>
                <td><?= $email['sent_emails_filename'] ?></td>
                <td>
                    <a id="preview_template" name="preview_template" onClick="PreviewTemplate(<?= $email['sent_emails_history_id']?>);"><?= $email['sent_emails_communication_type']=='fax'?'View Fax':'View Email'?></a>
                </td>
            </tr>
        <? endforeach; 
        if(empty($emails_history)){?>
        <tr>
            <td colspan="5">No emails sent</td>
        </tr>
        <?} ?>
    </table>  
    </div>
    </fieldset>

        <fieldset class="j-toggle-parent">
            <legend>Notes <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
                <p class="callout">Notes are for internal use only, and will not be submitted to carriers or recipients</p>
                    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear" id="notebox_container">
                        <tr>
                            <th>Subject</th>
                            <th>Note</th>
                            <th style="width:200px">Last Updated</th>
                            <th style="width:85px">Actions</th>
                        </tr>
                       <? if (!$claim_notes){ ?>  
                         <tr class="notebox">
                            <td><input type="text" id="claims_notes_subject_0" class="show_on_edit" name="claims_notes_subject[0]" value="<?= $claim_notes['claims_notes_subject'] ?>" >
                                <input type="hidden" id="claims_hidden_subject_0">
                                <span id="claims_notes_subject_text_0" class="hide_on_edit"></span>
                            </td> 
                            <td><textarea id="claims_notes_note_0" cols="50" rows="5" name="claims_notes_note[0]" class="show_on_edit"></textarea>
                                <input type="hidden" id="claims_hidden_notes_0">
                                <span id="claims_notes_note_text_0" class="hide_on_edit"></span>
                            </td> 
                            <td>&nbsp;</td>
                            <td>
                                <div class="actionMenu">
                                <ul>
                                    <li><a href="#"  id="donenotes_0_new" class="show_on_edit donenotes">Done</a></li>
                                    <li><a href="#"  id="cancelnotes_0_new" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>
                                    <li><a href="#"  id="editnotes_0" class="hide_on_edit edit_notes" style="display:none;">Edit</a></li>
                                    <li><a href="#"  id="deletenotes_0" class="show_on_edit delete" >Cancel</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>  
                       <? } ?>
                        <? if ($claim_notes): ?>
                        <?  $i=0;
                            foreach ((array)$claim_notes as $claim_note):  ?>
                            <tr class="notebox">
                                <td><input type="hidden" name="claims_notes_id[<?=$i?>]" value="<?= $claim_note['claims_notes_id'] ?>">
                                    <input type="hidden" id="claims_hidden_subject_<?=$i?>" value="<?= $claim_note['claims_notes_subject'] ?>">
                                    <input type="text" id="claims_notes_subject_<?=$i?>" name="claims_notes_subject[<?=$i?>]" value="<?= $claim_note['claims_notes_subject'] ?>" style="display:none;" class='show_on_edit'>
                                    <span id="claims_notes_subject_text_<?=$i?>" class="hide_on_edit"><?= $claim_note['claims_notes_subject'] ?></span></td>
                                 <td><input type="hidden" id="claims_hidden_notes_<?=$i?>" value="<?= $claim_note['claims_notes_note'] ?>"><span id="claims_notes_note_text_<?=$i?>" class="hide_on_edit"><?= limitWrods($claim_note['claims_notes_note']) ?></span>
                                     <textarea id="claims_notes_note_<?=$i?>" name="claims_notes_note[<?=$i?>]" style="display:none;" class='show_on_edit'><?= $claim_note['claims_notes_note'] ?></textarea>
                                 </td>
                                <td><?= date('F j, Y, g:i a', strtotime($claim_note['claims_notes_date'])) ?></td>
                                <td>
                                    <div class="actionMenu">
                                        <ul>
                                            <li><a href="#" class="notes hide_on_edit" id="notes_<?= $claim_note['claims_notes_id'] ?>">View full note</a></li>
                                            <li><a href="#"class="edit_notes hide_on_edit" id="edit_notes">Edit</a></li>
                                            <li><a href="#" onclick="delete_notes(this);">Delete</a></li>
                                            <li><a href="#"  id="donenotes_<?=$i?>" class="show_on_edit donenotes" style="display:none;">Done</a></li>
                                            <li><a href="#"  id="cancelnotes_<?=$i?>" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <? $i++;
                        endforeach; ?>
                           <? endif; ?>
                            <a href="#" id="another_note" class="add-item">Add another Note</a>
                    </table>
                    
            </div>
        </fieldset>
        
        <div class="form-actions">
            <?= html::hidden('', 'submit_to_carrier'); ?>
            <? if ($claim['join_claims_status_codes_id'] == Claims::$PENDING) { ?>
                <input type="button" name="saveAndSubmit" id="saveAndSubmit" value="Save and Submit to Carrier"
                       class='btn-primary' disabled />
                You cannot save the claim until the current pending submission finishes.
            <? } else { ?>
                <div class="callout" id='submit_message'>
                    <?
                    if(count($corporate_email_recipients)>0 ) {
                          echo "<span>A notice will be sent to <span id='additional_email'>";
                          foreach($corporate_email_recipients as $recipients){
                              $emai_list[] =  $recipients['carriers_email'] ;
                          }
                          echo implode(', ',$emai_list);
                          echo "</span></span>";
                      }
                      
                    ?>
                   
                </div>
                <input type="button" name="saveOnly" id="saveOnly" value="Save"
                       class='btn-primary btn-ok'/>
            <? }?>
        </div>
        <input type="hidden" id="fax_tmp_id" name="fax_tmp_id" value="<?= $faxTmpFileID ?>"/>
        <input type="hidden" name="carriers_id" id="carriers_id" value="<?= $claim['join_carriers_id']?$claim['join_carriers_id']:$selected_carrier['carriers_id']?>" />
        <input type="hidden" id="default_template_id" value="<?= $default_template_id ?>" />
        <input type="hidden" id="customize_template" name="customize_template" value="<?= $customTemplate ? 1 : 0 ?>"/>
        <input type="hidden" id="notify" name="notify" value="<?= $notify ?>"/>

    </form>
</div>
<div class="template hidden" id="delete-with-certs">
        <div>
            <p>This claim has already been submitted to this carrier. Are you sure you wanted to remove the carrier from this claim?</p>
            <div class="options">
                <button class="j-cancel btn-primary btn-ok">No, keep this carrier</button>
                <button class="j-delete btn-primary btn-delete">Yes, remove the carrier</button>
            </div>
        </div>
</div>
<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/claims.js?v=20180618"></script>

<? include VIEWS . '/members/footer.php' ?>

<script>
    function removeOptions(selectbox) {
        if(!selectbox) {
            return;
        }
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }
</script>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#email_html",
        menubar: "edit insert dyndata view format table tools",
        toolbar1: "dyndata undo redo | bold italic underline strikethrough | styleselect formatselect | forecolor backcolor removeformat | code",
        toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image | table | html",
        toolbar_items_size: 'small',
        plugins: "DynamicDBData table link image textcolor code paste",
        paste_data_images: true,
        convert_urls: false,
        images_upload_url: '/library/tiny_mce/ImageUpload.php',
        images_upload_base_path: '/',
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        DynDBD_MenuItems: <?= $dynemail->serialize_jscript(); ?>
    });
</script>
