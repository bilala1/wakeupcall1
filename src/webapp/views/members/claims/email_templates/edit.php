<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Claims';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>
<div id="content">
        <h1 class="left"><?= $template ? 'Edit' : 'Add' ?> Email Template</h1>
        <div class="page-options"><a href="/members/claims/email_templates/index.php">Return to Email Template List</a></div>
        <br clear="all"/>

        <form action="/members/claims/email_templates/edit.php?claims_email_templates_id=<?= $claims_email_templates_id?>" name="editEmailTemplates" id="editEmailTemplates" method="post" enctype="multipart/form-data">
            
            <div style="padding-bottom: 10px;">
            <? if(sizeof($licensed_locations) >= 1 ): ?>
                <b>Location:</b>           
                <?= html::select_from_query($licensed_locations, 'licensed_locations_id', $_REQUEST, 'licensed_locations_name', 'All Locations') ?>
            <? else: ?>
                <? if($edit_location['licensed_locations_name']): ?>
                    <b>Location:</b><?= $edit_location['licensed_locations_name'] ?>
                <? endif ?>
                <input type='hidden' name='licensed_locations_id' location='licensed_locations_id' value ="<?= $licensed_locations_id?>"/>
            <? endif ?>
            </div>
            <p class="callout">
                <img src="/images/dialog-warning.png" class="icon">
                When customizing this form, please do not alter the text within the brackets { }. These fields are automatically populated with information from your data. Altering or deleting these fields will omit this important data from the claim submission notice.</p>
            <? if(!$template || !$template['join_claims_id']) { ?>
                <?= html::textfield('Template name:', $claims_email_templates_name, 'claims_email_templates_name', array('maxlength' => 255), $errors) ?>
            <? } else { ?>
                <?=html::hidden('(Custom Template)', 'claims_email_templates_name') ?>
            <? } ?>
            <textarea id="email_html" name="email_html" style="width:100%;height:600px;"><?= $template_html ?></textarea>
            <br/>
            <div class="form-actions">
            <input type="submit" value="Save" class='btn-primary btn-ok' ></input>
            </div>
            
        </form>
    </div>

    <? include VIEWS . '/members/footer.php' ?>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooDialog.Request.js"></script>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#email_html",
        menubar: "edit insert dyndata view format table tools",
        toolbar1: "dyndata undo redo | bold italic underline strikethrough | styleselect formatselect | forecolor backcolor removeformat | code",
        toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image | table | html",
        toolbar_items_size: 'small',
        plugins: "DynamicDBData table link image textcolor code paste",
        paste_data_images: true,
        convert_urls: false,
        images_upload_url: '/library/tiny_mce/ImageUpload.php',
        images_upload_base_path: '/',
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        DynDBD_MenuItems: <?= $dynemail->serialize_jscript(); ?>
    });
</script>
