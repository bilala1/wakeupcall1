<div id="content" style="display:inline-block;min-width: 95%; min-height: 80%">
<?
    $db = mysqli_db::init();
    $licensed_location_id = $_REQUEST['licensed_locations_id'];
    $location_details = licensed_locations::get_by_id($licensed_location_id);
    include(SITE_PATH . '/' . MODELS . '/DynamicEmail/ClaimsEmail.php'); 
    $dynemail = new ClaimsRequestDynamicEmail();
    
    $claims_id = $_REQUEST['claims_id']?$_REQUEST['claims_id']:0;
    if($claims_id){
        $template = claims_email_templates::get_template_for_claim($claims_id);
    }
   
   $join_claims_email_templates_id = $_REQUEST['join_claims_email_templates_id'];
    
    if($_REQUEST['customize_template'] == 1 && $_REQUEST['email_html']){
        $template_html =  $_REQUEST['email_html'];
    }elseif($join_claims_email_templates_id){
       $template = claims_email_templates::get_by_id($join_claims_email_templates_id);
       $template_html = $template['email_html'];
    }else{ 
        $template_html = emails_default_templates::get_claims_email_template();
    }
    $join_carriers_id = $_REQUEST['join_carriers_id']?$_REQUEST['join_carriers_id']:$_REQUEST['carriers_id'];
    $claim = $_REQUEST;/*$db->fetch_one("SELECT * from claims c
                            left join claim_auto as ca on ca.join_claims_id = claims_id
                            left join claim_general_liab as cgl on cgl.join_claims_id = claims_id
                            left join claim_property as cp on cp.join_claims_id = claims_id
                            left join claim_workers_comp as cwc on cwc.join_claims_id = claims_id
                            left join claim_employment as ce on ce.join_claims_id = claims_id
                            left join claims_faxes cf on c.claims_id = cf.join_claims_id and cf.claims_faxes_status < 0 
                            where claims_id = ?", array($claims_id));*/
    $date_of_incident = $claim['claims_datetime'] ? date(DATE_FORMAT, strtotime($claim['claims_datetime'])) : "";
    $date_notified = $claim['claims_datetime_notified'] ? date(DATE_FORMAT, strtotime($claim['claims_datetime_notified'])) : "";
    $brief_description = $claim['claims_description'];
    $claimant = '';
    if($claim['claim_employment_claimant'])
        $claimant = $claim['claim_employment_claimant'];
    elseif($claim['claim_workers_comp_employee_name'])
        $claimant = $claim['claim_workers_comp_employee_name'];
    elseif($claim['claim_general_liab_claimant'])
        $claimant = $claim['claim_general_liab_claimant'];
    elseif($claim['claim_auto_claimant'])
        $claimant = $claim['claim_auto_claimant'];
    
    // get the license location name for the claim
    $claim_location = $db->fetch_singlet('SELECT licensed_locations_name FROM licensed_locations where licensed_locations_id = ?', array($claim['join_licensed_locations_id']));
    $member = $db->fetch_one('SELECT members_firstname,members_lastname,members_email FROM members WHERE members_id = ?', array(ActiveMemberInfo::GetMemberId()));
    $carrier = $db->fetch_one('SELECT * FROM carriers WHERE carriers_id = ?', array($join_carriers_id));
    $claim2carrierModel = array(
        "todays_date" => date('m-d-Y'),
        "carriers_name" => "[Recipient's Name]",
        "claim_location" => $claim_location,
        "carriers_policy_number" => "[Policy Number]",
        "members_firstname" => $member['members_firstname'],
        "members_lastname" => $member['members_lastname'],
        "claimant" => $claimant,
        "date_of_incident" => $date_of_incident,
        "date_notified" => $date_notified,
        "brief_description" => $brief_description
    ); 
    
    echo '<span class="none">';
    echo $dynemail->generate_html ($template_html, $claim2carrierModel);
    echo '</span>';
    
    
?>
</div>