<div id="content" style="display:inline-block;min-width: 95%; min-height: 80%">
<?
    include(SITE_PATH . '/' . MODELS . '/DynamicEmail/ClaimsEmail.php'); 
    $dynemail = new ClaimsRequestDynamicEmail();
    
    $claims_email_templates_id = $_REQUEST['claims_email_templates_id']?$_REQUEST['claims_email_templates_id']:0;
    if($claims_email_templates_id){
        $template = claims_email_templates::get_by_id($claims_email_templates_id);
    }
    $template_html = $_REQUEST['email_html'] ? $_REQUEST['email_html'] : 
                     ($template ? stripslashes($template['email_html']) : '');
    $claim2carrierModel = array(
        "carriers_name" => "Sample Carrier",
        "claim_location" => "Sample Location",
        "carriers_policy_number" => "1234567",
        "members_firstname" => "First",
        "members_lastname" => "Name",
        "isRecipient" => true
    );
    
    echo '<span class="none">';
    echo $dynemail->generate_html ($template['email_html'], $claim2carrierModel);
    echo '</span>';
    
?>
</div>