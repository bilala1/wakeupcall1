<table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
    <tr>
        <th>To</th>
        <th>Subject</th>
        <th>Date</th>
        <th>Status</th>
        <th>Attachments</th>
        <th>Actions</th>
    </tr>
    <? foreach((array)$emails as $email): ?>
        <tr>
            <td><?= $email['sent_emails_email']?></td>
            <td><?= $email['sent_emails_desc'] ?></td>
            <td><?= date(DATE_FORMAT_FULL, strtotime($email['sent_emails_date'])) ?></td>
            <td><?= Formatting::emailStatus($email) ?></td>
            <td><?= $email['sent_emails_filename'] ?></td>
            <td> <a id="preview_template" name="preview_template" onClick="PreviewTemplate(<?= $email['sent_emails_history_id']?>);"><?= $email['sent_emails_communication_type']=='fax'?'View Fax':'View Email'?></a>
            </td>
        </tr>
    <? endforeach;
    if(empty($emails)){?>
        <tr>
            <td colspan="5">No emails sent</td>
        </tr>
    <? } ?>
</table>