<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Claims';
$page_keywords = '';
$page_description = '';

include VIEWS . '/members/header.php';
?>

<div id="content">
    <div class="page-header">
        <h1 class="left">Claims Email History</h1>

        <div class="page-options">
            <div class="page-dialog-background"></div>
            <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a> | 
            <a href="/members/claims/index.php">Back To Claims</a>
        </div>

        <div class="clear"></div>
        <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '2' : '1' ?> filter-Email-History">
        <form action="index.php" id="filter" method="get" style="margin-bottom:15px">
            <div class="dialog-sections">
                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                        <div class="dialog-section">
                            <h3>Locations</h3>
                            <?= html::location_filter('claims', $filter_location_id) ?>
                            <?//= html::select_from_query($licensed_locations, 'licensed_locations_id', $filter_location_id, 'licensed_locations_name', '-all-') ?>
                        </div>
                    <? } ?>
                <div class="dialog-section dates">

                        <label>From:</label><?= '<input style="width:100px" type="text" id="email_history_start_time" name="email_history_start_time" value=' . $startTime.'>' ?><br>
                        <label>To:</label><?= '<input style="width:100px" type="text" id="email_history_end_time" name="email_history_end_time" value=' . $endTime . '>' ?>
                </div>
                </div>
                <div class="form-actions">
                    <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                    <?= html::hidden($model, 'clear_filter') ?>
                    <?= html::submit("Clear Filter", array('id' => 'clear_button')) ?>
                </div>
        </form>
        </div>
    </div>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
    <tr>
        <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
            <th><?= strings::sort('licensed_locations_name', 'Location'); ?></th>
        <? endif ?>
        <th><?= strings::sort('claims_type', 'Claim Type'); ?></th>
        <th><?= strings::sort('sent_emails_email', 'To'); ?></th>
        <th><?= strings::sort('sent_emails_desc', 'Subject'); ?></th>
        <th><?= strings::sort('sent_emails_date', 'Date', null, null, null, 'DESC'); ?></th>
        <th><?= strings::sort('sent_emails_history_status', 'Status'); ?></th>
        <th><?= strings::sort('sent_emails_filename', 'Attachments'); ?></th>
        <th style="min-width:55px;">Actions</th>
    </tr>
    
        <? foreach($emails as $email) : ?>
        <tr>
            <? $location = licensed_locations::get_by_id($email['join_licensed_locations_id']); ?>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= $location['licensed_locations_name']; ?></td>
            <? endif; ?>
            <td><?= $email['claims_type'] ?: 'Claim deleted' ?></td>
            <td><?= $email['sent_emails_email']?></td>
            <td><?= $email['sent_emails_desc'] ?></td>
            <td><?= $email['sent_emails_date'] ?></td>
            <td><?= Formatting::emailStatus($email) ?></td>
            <td><?= $email['sent_emails_filename'] ?></td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <li><a id="preview_template" name="preview_template" onClick="PreviewTemplate(<?= $email['sent_emails_history_id']?>);"><?= $email['sent_emails_communication_type']=='fax'?'View Fax':'View Email'?></a></li>
                        <?php if($email['claims_id']) { ?>
                            <li><a href="../edit.php?claims_id=<?= $email['claims_id'] ?>">View Claim</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>    
    <tr>
        <td colspan="99"><?= $paging->get_html(); ?></td>
    </tr>
</table>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
function PreviewTemplate($email_id)
{
    var reqDialog = new MooDialog.Request('preview_popup.php?id=' + $email_id, null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: 800,
                height: 500
            }
    });

    reqDialog.setRequestOptions({
        onRequest: function () {
            reqDialog.setContent('loading...');
        }
    }).open();

    return false;
}

window.addEvent('domready', function(){
    <? if($infobox): ?>
       $$('.page-desc').setStyle('display', 'block');
       $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
    jQuery('#clear_button').bind('click', clear_filter);
    
});
function clear_filter(){ 
        jQuery('#clear_filter').val("clear_filter");
    }
window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_email_history');
    });
    
    new MooCal($('email_history_start_time'), {
        leaveempty: false,
        clickout: true
    });

    new MooCal($('email_history_end_time'), {
        leaveempty: false,
        clickout: true
    });
});

</script>
<? include VIEWS . '/members/footer.php' ?>
