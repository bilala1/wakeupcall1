<div>
    <?
    $db = mysqli_db::init();

    $claim_files = $db->fetch_all('SELECT claims_files_id, files_name FROM claims_files INNER JOIN files on join_files_id = files_id WHERE join_claims_id = ? ', array($_REQUEST['claims_id']));
    ?>
    <ul>
        <? if(!$claim_files): ?>
            <li><b>No Attachments</b></li>
        <? else: ?>
            <? foreach($claim_files as $k => $file): ?>
                <li><a href="/members/claims/download.php?claims_files_id=<?= $file['claims_files_id'] ?>&type=.<?= files::get_extension($file['files_name']) ?>" target="_blank" title="<?= $file['files_name'] ?>"><?= $file['files_name'] ?></a><br /></li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>
