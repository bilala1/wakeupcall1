<div>
    <? $actions = claims_history::get_claims_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['claims_id']); ?>
    <ul>
        <? if($actions): ?>
        <? foreach($actions as $action): ?>
        <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['claims_history_datetime'])) ?>)</b> - <?= $action['claims_history_name'] ?></li>
        <? endforeach ?>
        <? else: ?>
        <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
