<?
$page_title        = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Claims';
$page_keywords     = '';
$page_description  = '';

if (!isset($locations)) { throw new Exception('model variable $locations not set.'); }


function get_corporation_display_name($claim)
{
    $echo_str = $claim['licensed_locations_name'];

    if(licensed_locations::is_corporate_location($claim['licensed_locations_id']))
        $echo_str = $echo_str . ' (Corporate Office)';

    return strings::wordwrap($echo_str , 9, '&shy;', true);
}
?>

<? include VIEWS. '/members/header.php' ?>

<div id="content">
<div class="page-header">
    <h1 class="left">Claims Management</h1>

    <div class="page-options">
        <div class="page-dialog-background"></div>
        <a href="/members/claims/edit.php" class="add-item">Add Claim</a> |
        <a href="/members/claims/carriers/index.php?carriers_show_hidden=0">Manage Carrier Policies/Recipients</a> |
        <a href="/members/claims/email_templates/index.php">Email Templates</a> |
        <a href="emails_history/index.php">Email / Fax History</a> |
        <a class="page-dialog-trigger" data-target=".report-body"><i class="fa fa-caret-down"></i> Reports</a> |
        <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a>
    </div>

    <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '3' : '2' ?> filter-claims">
        <form action="index.php" id="filter" method="get">
            <div class="dialog-sections">
                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                    <div class="dialog-section">
                        <h3>Locations</h3>
                        <?= html::location_filter('claims', $model['join_licensed_locations_id']) ?>
                    </div>
                <? } ?>
                <div class="dialog-section dates">
                    <h3>Date of Incident</h3>
                    <label>Quick Filter:</label><?= html::select(array('' => '') + array(
                            7=>'Last 7 days', 14=>'Last 14 days', 30=>'Last 30 days', 90=> 'Last 90 days'
                        ), 'quick_filter', '', array(), $errors); ?><br>
                    <label>From:</label><?= html::date($model, 'claims_start_time', array()) ?><br>
                    <label>To:</label><?= html::date($model, 'claims_end_time', array()) ?>

                    <h3>Status</h3>
                    <?= html::select($claims_status_values, 'join_claims_status_codes_id', $join_claims_status_codes_id,array(),$errors ) ?>
                </div>
                
                <div class="dialog-section">
                    <h3>Display Options</h3>
                    <label><?= html::radio(0, 'claims_show_hidden', $model) ?> Show Active Claims</label><br>
                    <label><?= html::radio(1, 'claims_show_hidden', $model) ?> Show Hidden Claims</label>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::hidden($model, 'clear_filter') ?>
                <?= html::submit("Clear Filter", array('id' => 'clear_button')) ?>
            </div>
        </form>
    </div>

    <div class="page-dialog report-body page-dialog-3">
        <div class="dialog-sections">
                <div class="dialog-section">
                    <h3>Excel Export</h3>
                    <form id="excel" action="/members/claims/report.php" method="post">
                        <?= '<input type="hidden" name="page" value="'.$_REQUEST['page'] .'">' ?>
                        <?= '<input type="hidden" name="claims_start_time" value="'.$_REQUEST['claims_start_time'] .'">' ?>
                        <?= '<input type="hidden" name="claims_end_time" value="'.$_REQUEST['claims_end_time'] .'">' ?>
                        <?= '<input type="hidden" name="order" value="'.$_REQUEST['order'] .'">' ?>
                        <?= '<input type="hidden" name="claims_show_hidden" value="'.$_REQUEST['claims_show_hidden'] .'">' ?>
                        <?= '<input type="hidden" name="licensed_locations_id" value="'.$_REQUEST['licensed_locations_id'] .'">' ?>
                        <ul id="report_type">
                            <? if(UserPermissions::UserCanAccessScreen('workers_comp')) {?>
                            <li><a href ="/members/claims/report.php?report_type=Workers Comp">Worker's Comp Claims</a></li>
                            <? }?>
                            <li><a href ="/members/claims/report.php?report_type=General Liability / Guest Property" >General Liability / Guest Property</a></li>
                            <li><a href ="/members/claims/report.php?report_type=Auto">Auto Claims</a></li>
                            <li><a href ="/members/claims/report.php?report_type=Your Property">Property Claims</a></li>
                            <? if(UserPermissions::UserCanAccessScreen('labor_claims')) {?>
                                <li><a href ="/members/claims/report.php?report_type=Labor">Labor Claims</a></li>
                            <? }
                             if(UserPermissions::UserCanAccessScreen('other_claims')) {?>
                                <li><a href ="/members/claims/report.php?report_type=Other">Other</a></li>
                             <? } ?>
                        </ul>
                    </form>
                </div>

            <? if(UserPermissions::UserCanAccessScreen('workers_comp')) {?>
                <div class="dialog-section">
                    <h3>OSHA 300 Reporting</h3>
                    <form id="osha" action="/members/claims/osha_report.php" method="post">
                        <?= '<input type="hidden" name="licensed_locations_id" value="'.$_REQUEST['licensed_locations_id'] .'">' ?>
                        <table>
                            <tr>
                                <td width="50px"><b>From: </b></td>
                                <td width="20px">
                                    <select name='from_month' id='from_month'>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </td>
                                <td width="40px">
                                    <select name='from_year' id='from_year'>
                                        <?
                                        $now = date('Y');
                                        $then = $now - 10;
                                        $current_month = date('m');
                                        if($current_month <= 4){
                                            $year_selected = $now -1;
                                        }else{
                                            $year_selected = $now;
                                        }
                                        for($i = $now; $i >= $then; $i--){
                                            $selected = '';
                                            if($i == $year_selected){
                                                $selected = "selected=selected";
                                            }
                                            echo '<option value="'. $i .'"'. $selected.'>'. $i .'</option>' ;
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50px"><b>To: </b></td>
                                <td width="20px">
                                    <select name='to_month' id='to_month'>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12" selected>12</option>
                                    </select>
                                </td>
                                <td width="40px">
                                    <select name='to_year' id='to_year'>

                                        <?
                                        $now = date('Y');
                                        $then = $now - 10;

                                        for($i = $now; $i >= $then; $i--){
                                            $selected = '';
                                            if($i == $year_selected){
                                                $selected = "selected=selected";
                                            }
                                            echo '<option value="'. $i .'"'. $selected.'>'. $i .'</option>' ;
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                                <tr>
                                    <td colspan="4">
                                        <?= html::location_filter('claims') ?>
                                    </td>
                                </tr>
                            <? } ?>
                            <tr>
                                <td colspan="4">
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-ok btn-primary" value="Generate OSHA 300 Report"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            <? } ?>

            <div class="dialog-section">
                <h3>Other reports</h3>
                <ul id="claim-reports">
                    
                </ul>
            </div>
        </div>
    </div>
</div>
<? if($_SESSION['claims_show_hidden'] == '1'): ?>
    <div>
        <h2>Hidden Claims</h2>
    </div>
<? endif ?>
    <style>
        .claims-tabs {
            height: 36px;
            border-bottom: 1px solid #aaa;
        }
        .claims-tabs .tab-title {
            width: 15%;
            cursor: pointer;
        }
        .tab-title .title {
            display: inline-block;
            width: 100%;
            height: 26px;
            color: #333;
            text-decoration: none;
            font-weight: bold;
            padding-top: 10px;
            text-shadow: none;
        }
        .claims-tabs .double-line span {
            padding-top: 0;
            height: 36px;
        }
        .claims-tabs .activetab .title {
            color:#3386cb;
        }
    </style>
    <div class="claims-tabs">
        <!-- the data-report-type attr must match the values in models/members/claims/claims_reports.php  -->
        <? if(UserPermissions::UserCanAccessScreen('workers_comp')) {?>
        <div class="tab-title double-line" data-report-type="WorkersComp"><span class="title">Worker's Comp<br> Claims</span></div>
        <? }?>
        <div class="tab-title double-line" data-report-type="General"><span class="title">General Liability/<br>Property of Others</span></div>
        <div class="tab-title" data-report-type="Auto"><span class="title">Auto Claims</span></div>
        <div class="tab-title" data-report-type="Property"><span class="title">Property Claims</span></div>
        <? if(UserPermissions::UserCanAccessScreen('labor_claims')) {?>
        <div class="tab-title" data-report-type="Labor"><span class="title">Labor Claims</span></div>
        <? } if(UserPermissions::UserCanAccessScreen('other_claims')) { ?>
        <div class="tab-title" data-report-type="Other"><span class="title">Other</span></div>
        <? } ?>
    </div>
    <div class="claims-content">
        <? include VIEWS. '/members/claims/claims_data.php' ?>
    </div>
</div>
<!--    Report Dialog-->
<div id="report-dialog-workers-comp">
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/claims_list.js?v=20180618"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
window.addEvent('domready', function(){
    <? /* open or collapse infobox? */ ?>
    <? if($infobox): ?>
    $$('.page-desc').reveal();
    $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
    google.charts.load('current', {packages: ['corechart']});
    //google.load("visualization", "1", {packages:["corechart"]});
 });
<? include "_render_chart.js"; ?>   

</script>
<? include VIEWS. '/members/footer.php' ?>
