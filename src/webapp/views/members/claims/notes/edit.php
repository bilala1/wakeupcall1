<html>
<head>
    <? include VIEWS . '/members/scripts.php' ?>
</head>
<body class="dialog">
    <div class="page-options">
      <?
      $claims_id = $_REQUEST['claims_id'];
      if(!$claims_id) $claims_id = $claims_note['join_claims_id'];
      ?>
    <a href="notes_popup.php?claims_id=<?= $claims_id ?>">Back to Notes</a>
    </div>
    <h1 class="left"><?= $_REQUEST['claims_notes_id'] ? 'Edit' : 'Add' ?> Note</h1>
    <br class="clear" />
    <form action="/members/claims/notes/edit.php" name="editNote" id="editNote" method="post" class="full-width" enctype="multipart/form-data">
        <?if ($_REQUEST['claims_notes_id']) :?>
            <?= html::hidden($_REQUEST, 'claims_notes_id') ?>
        <? endif; ?>
        <?= html::hidden($_REQUEST, 'claims_id') ?>
        <?= html::hidden($_REQUEST, 'nav_id') ?>

        <?= html::textfield('Note Subject:', $claims_note['claims_notes_subject'], 'claims_notes_subject', null, $errors) ?>

        <?= html::textareafield('Note:', $claims_note['claims_notes_note'], 'claims_notes_note', array('rows'=>5, 'cols'=>50)) ?>

        <div class="form-actions">
        <?= html::submit('Save', array('class' => 'btn-primary btn-ok')) ?>
        </div>
    </form>
</body>
</html>