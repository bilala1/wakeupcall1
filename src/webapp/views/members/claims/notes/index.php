<?
    $page_title        = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Claims';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <br class="clear" />

    <h1 class="left">Notes</h1>
    <div class="page-options">
		<a href="/members/claims/notes/edit.php?claims_id=?<?= $_REQUEST['claims_id'] ?>">Add Note</a>
	</div>

    <br clear="all" />
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Note Description</th>
            <th>Date</th>
            <th style="width:55px">Actions</th>
        </tr>
        <? foreach((array)$claim_notes as $claim_note): ?>
            <tr>
                <td><?= $claim_note['claims_notes_subject']?></td>
                <td><?= date('F j, Y, g:i a', strtotime($claim_note['claims_notes_date'])) ?></td>
                <td>
                    <a href="/members/claims/notes/edit.php?claims_notes_id=<?= $claim_note['claims_notes_id'] ?>&claims_id=<?= $claim_note['claims_notes_id'] ?>">Edit</a>
                    <a href="/members/claims/notes/view.php?claims_notes_id=<?= $claim_note['claims_notes_id'] ?>&claims_id=<?= $claim_note['claims_notes_id'] ?>">View</a>
                </td>
                 </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>

    <br />
</div>

<? include VIEWS. '/members/footer.php' ?>