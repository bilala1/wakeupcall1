<html>
<head>
    <? include VIEWS . '/members/scripts.php' ?>
</head>
<body class="dialog">
    <? include(ACTIONS . '/members/claims/notes/index.php'); ?>
    <? if($claim_notes): ?>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Subject</th>
            <th>Date</th>
            <th style="width:150px">Actions</th>
        </tr>
        <? foreach((array)$claim_notes as $claim_note): ?>
            <tr>
                <td><?= $claim_note['claims_notes_subject']?></td>
                <td><?= date('F j, Y, g:i a', strtotime($claim_note['claims_notes_date'])) ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="view.php?claims_notes_id=<?= $claim_note['claims_notes_id'] ?>" >View full note</a></li>
                            <li><a href="edit.php?claims_notes_id=<?= $claim_note['claims_notes_id'] ?>&claims_id=<?= $claim_note['join_claims_id'] ?>">Edit</a></li>
                            <li><a class="deletecheck" href="delete.php?claims_notes_id=<?= $claim_note['claims_notes_id'] ?>&claims_id=<?= $_REQUEST['claims_id'] ?>&redirect=popup">Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
    <? else: ?>
       <p>
           <b>No Existing Notes</b>
       </p>
    <? endif; ?>
    <a href="/members/claims/notes/edit.php?claims_id=<?= $_REQUEST['claims_id'] ?>&nav_id=claims" class="add-item">Add Note</a>
</body>
</html>
