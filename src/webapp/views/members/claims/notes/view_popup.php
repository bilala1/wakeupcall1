<div id="content">
    <? include(ACTIONS . '/members/claims/notes/view.php'); ?>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th><?= $claims_note['claims_notes_subject']?></th>
            <th width='200px'><?= date('F j, Y, g:i a', strtotime($claims_note['claims_notes_date'])) ?></th>
        </tr>
        <tr>
            <td colspan="2"><?= $claims_note['claims_notes_note']?></td>
        </tr>
    </table>
</div>