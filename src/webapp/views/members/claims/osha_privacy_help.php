<div id="content">
    <h3 class="clear">If the answer to any of these questions is yes then the case is considered a privacy case.</h3>
    <ul>
        <li>Is the injury or illness related to an intimate body part or the reproductive system?</li>
        <li>Is the injury or illness resulting from a sexual assault?</li>
        <li>Is the injury or illness related to a mental illness?</li>
        <li>Is the injury or illness related to HIV infection, hepatitis or tuberculosis?</li>
        <li>Is the injury or illness the result of a needlestick injury or cut from a sharp object contaminated with
            another person's blood or other potentially infections material?
        </li>
        <li>Did the employee voluntarily request that his or her name not be entered on the OSHA 300 log?</li>
    </ul>
</div>