<?php
/**
 * Created by PhpStorm.
 * User: johncampbell
 * Date: 1/22/15
 * Time: 10:21 PM
 */

// Get all the categories (for the user).
$db = mysqli_db::init();
$categories = $db->fetch_all('
	SELECT *
	FROM members_library_categories
	WHERE join_members_id = ' . $db->filter(ActiveMemberInfo::GetMemberId())
    . ' ORDER BY members_library_categories_name ASC'
);
?>
<div id="saveToMyFileDialog">
    <style scoped>
        #reportSaveError, #reportSaveErrorFolder {
            color:red;
        }
    </style>
    <form>
        <div>
            <label style="width:100px;">Report Name:</label>
            <input id="reportName" type="text" style="width: 280px;" />
            <div id="reportSaveError">&nbsp;</div>
        </div>
        <div>
            <label style="width:100px;">Folder:</label>
            <?= html::select_from_query($categories, 'members_library_categories_id', $_REQUEST, 'members_library_categories_name', '-select folder-'); ?>
            <div id="reportSaveErrorFolder">&nbsp;</div>
        </div>
        <div class="form-actions">
            <button id="cancelReportButton" class="btn-primary">Cancel</button>
            <button id="saveReportButton" class="btn-ok btn-primary">Save</button>
        </div>
    </form>
</div>