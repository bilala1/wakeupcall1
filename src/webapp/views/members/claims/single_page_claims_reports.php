<?
ob_start();
include 'claims_reports.php';
$body = ob_get_clean();
?>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript" src="/js/mootools-core-1.4.0-full-compat-yc.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <style>
            #report-form {
                display: none;
            }
            #button-area {
                display: none;
            }
        </style>
    </head>
    <body data-window-type="print">
    <? echo $body; ?>
        <script>
            google.load("visualization", "1", {packages:["corechart"]});
            <? include "_render_chart.js"; ?>

            window.addEvent('domready', function() {
                chartRenderer.renderChart();
            });
        </script>
    </body>
</html>