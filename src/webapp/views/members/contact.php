<?
    $page_title        = SITE_NAME;
    /*$page_name         = 'Forums';
    $sub_name          = 'forum';
    $page_keywords     = '';
    $page_description  = '';*/
    
	include VIEWS .'/members/header.php';
?>

<div id="content">
	<div id="leftcol" class="w700"> 
        <form action="/members/contact.php" method="post">
            
            <?= html::textfield('*First Name', $_REQUEST, 'firstname', array('class' => 'long-txt'), $errors) ?>
            
            <?= html::textfield('*Last Name', $_REQUEST, 'lastname', array('class' => 'long-txt'), $errors) ?>
            
            <?= html::textfield('*Email', $_REQUEST, 'email', array('class' => 'long-txt'), $errors) ?>
            
            <?= html::textareafield('Message', $_REQUEST, 'message', array('class' => 'text', 'rows' => 10), $errors) ?>
            <input type="submit"  value="Contact" class="btn center" />
        </form>
	</div> <!-- End Leftcol -->
</div>

<? include VIEWS.'/members/footer.php'; ?>
