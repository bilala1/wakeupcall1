<div>
    <?
    $db = mysqli_db::init();

    $contract = $db->fetch_one('SELECT contracts_additional_details FROM contracts WHERE contracts_id = ? ', array($_REQUEST['contracts_id']));
    ?>
    <p>
        <?= !empty($contract['contracts_additional_details']) ?
            htmlentities($contract['contracts_additional_details'], ENT_COMPAT, 'UTF-8') :
            'No additional details'  ?>
    </p>
</div>
