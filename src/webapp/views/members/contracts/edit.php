<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Contracts';
$page_keywords = '';
$page_description = '';
?>
<script>
    //Formatting::dbKeysToJavascript
    var remindExpiredMembers = <?=json_encode(($remind_expired_members))?>;
    var remindMilestoneMembers = <?=json_encode($contracts_milestones_remind_members)?>;
    var licensed_locations_array = <?=json_encode($licensed_locations_array)?>
 </script>
<? include VIEWS . '/members/header.php' ?>
<style>
    .send-rem-check { width: 660px;}
    .send-rem-check .Dynlabel{ width: 50% !important; float: left; font-weight: normal !important;}
    #configureEmails label {
        width: 180px;
    }
    .milestone_check {width: 120Px;font-weight: normal}
</style>
<div id="content">
    <? if ($errors['contracts_file']): ?>
        <p class="errors"><?= $errors['contracts_file'] ?></p>
    <? endif ?>
        <h1 class="left"><?= $_REQUEST['contracts_id'] ? 'Edit' : 'Add' ?> Contract</h1>

        <div class="page-options"><a href="/members/contracts/index.php">Return to Contracts List</a></div>
        <br clear="all"/>

        <form action="/members/contracts/edit.php" name="editContract" id="editContract" method="post"
              onSubmit="return chkContractForm(this)" enctype="multipart/form-data">
            <?= html::hidden($_REQUEST, 'contracts_id') ?>

            <fieldset class="j-toggle-parent">
                <div class="j-toggle-summary">
                    <?
                        $location = array();
                        if ($contract['contracts_all_locations']) {
                            $location[] = "All Locations";
                        }else{
                            if($contract['join_licensed_locations']){
                                foreach($contract['join_licensed_locations'] as $locations){
                                    $location[]= $licensed_locations_array[$locations];
                                }
                            }
                        }
                    ?>
                    <?= html::plainTextField("Location", implode(',',$location),"summary_locations_name") ?>
                    <?= html::plainTextField("Contracted Party", $contract['contracts_contracted_party_name']?$contract['contracts_contracted_party_name']:'',"summary_contracts_contracted_party_name") ?>
                    <?= html::plainTextField("Description", $contract['contracts_description']?$contract['contracts_description']:'',"summary_contracts_description") ?>
                    <? $expiry_date =  $contract['contracts_expiration_date']!='0000-00-00'?Formatting::date($contract['contracts_expiration_date'], 'Not Set'):''; ?>
                    <?= html::plainTextField("Expiration Date", $expiry_date,"summary_expiry") ?>
                    <?= html::plainTextField("Status", $contract['contracts_status']?$contract['contracts_status']:'',"summary_contracts_status") ?>
                </div>
                <legend>Contract Details <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                <?= html::location_picker($contract, $errors, 'contracts'); ?>
                <? if(!ActiveMemberInfo::IsUserMultiLocation()) {
                    $locationid = ActiveMemberInfo::SingleLocationUserLocationId();
                   echo "<input type='hidden' name='single_location_id' id='single_location_id' value=$locationid >"; 
                }?>
                <?= html::textfield('Contracted Party:', $contract, 'contracts_contracted_party_name', array('maxlength' => '200'), $errors) ?>
                <?= html::textfield('Contact Name:', $contract, 'contracts_contact_name', array('maxlength' => '200'), $errors) ?>
                <?= html::textfield('Contact Phone:', $contract, 'contracts_contact_phone', array('maxlength' => '20', 'class' => 'phone'), $errors) ?>
                <?= html::textfield('Contact Email:', $contract, 'contracts_contact_email', array('maxlength' => '200'), $errors) ?>

                <?= html::textfield('Description:', $contract, 'contracts_description', array('maxlength' => '50'), $errors) ?>
                <?= html::textareafield('Additional Contract Details:', $contract, 'contracts_additional_details', array('maxlength' => '500', 'cols' => 30, 'rows' => 10), $errors) ?>

                <?= html::selectfield('Status:', $contracts_status_values, 'contracts_status', $contract, array(), $errors); ?>
                <?= html::datefield('Effective Date:', $contract, 'contracts_effective_date', array(), $errors) ?>
                <?= html::datefield('Expiration Date:', $contract, 'contracts_expiration_date', array(), $errors) ?>
                <?= html::selectfield('Transition on Expiration:', $contracts_transition_status_values, 'contracts_transition_status_to', $contract, array(), $errors); ?>
                </div>
            </fieldset>

            <fieldset id="configureEmails" class="j-toggle-parent">
                <legend>Configure Expiration Emails <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                    <?= html::selectfield('Remind before expiration: ', $contracts_reminder_times, 'contracts_expiration_notify_lead_days', $contract, array(), $errors) ?>
                    <?= html::checkboxfield('Remind after expiration: ' . html::help_button(null, null, 'Select \'Yes\' to be sent a weekly email reminder until the expired contract is updated.'),
                        1, 'contracts_expired_notification', $contract['contracts_expired_notification'], array(), $errors) ?>
                    <div class="form-field">
                        <label>Send Emails To:</label><fieldset class="send-rem-check">
                            <div id="remind_expired"></div>
                    </div><br />
                    <label>Additional Email Recipients:</label>
                    <a href="#" id="add-recipient" class="add-item">Add a recipient</a><br/>
                    <? if ($additional_recipients)
                        foreach ($additional_recipients as $additional_recipient): ?>
                            <div class="form-field">
                                <label><span>Email Address:</span></label>
                                <input value="<?= $additional_recipient['contracts_additional_recipients_email'] ?>"
                                       name="contracts_additional_recipients_email[]" maxlength="200" class="input-text">
                                <a href="#" class="delete-recipient">Delete</a>
                            </div>
                        <? endforeach; ?>
                    <div id="additional-recipients"></div>
                </div>
            </fieldset>

            <div id="j-additional-milestone">
            <? if(count($contracts_milestones_existing)<= 0){ ?>
                <fieldset class="j-toggle-parent j-additional-milestone" id="milestoneDetails_0">
                <legend>New Milestone <a class="j-toggle-trigger"></a>
                    <a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">
                    <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" />
		    </a>
                </legend>
                <div class="j-toggle-content">
                    <div class="form-field">
                            <label><span>Milestone Description:</span></label>
                            <input type="text" name="contracts_milestones_description[0]">
                    </div>
                    <div class="form-field">
                            <label><span>Milestone Date:</span></label>
                            <input type="text" name="contracts_milestones_date[0]" class="milestone_date">
                    </div>
                    <div class="form-field" id="remind_days_0">
                        <label><span>Remind before:</span></label>
                         <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_0[]' value= 0 ><label class='milestone_check'><span>0 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_0[]' value= 30><label class='milestone_check'><span>30 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_0[]' value= 60><label class='milestone_check'><span>60 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_0[]' value= 90><label class='milestone_check'><span>90 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_0[]' value= 120><label class='milestone_check'><span>120 Days</span></label>
                    </div>
                    <div class="form-field">
                            <label>Send Emails To:</label><fieldset class="send-rem-check">
                            <div class="remindMembersContainer" id="remind_milestone_0"></div>
                    </div>
                  <label>Additional Email Recipients:</label>
                <a href="#" class="add-item add-milestone-recipient" id="add-milestone-recipient_0">Add a recipient</a><br/>
                <div id="additional-milestone-recipients_0"></div>
                </div>  
                </fieldset>
            <? } else{
                    $i = 0;
                    foreach($contracts_milestones_existing as $contracts_milestone){
                        $existing_milestone_remind_days = contracts::get_contracts_milestones_remind_days($contracts_milestone['contracts_milestones_id']);
                        $milestones_additional_recipients = contracts::get_contracts_milestones_additional_recipients($contracts_milestone['contracts_milestones_id']);
                        ?>
                        <fieldset class="j-toggle-parent j-additional-milestone" id="milestoneDetails_<?=$i?>">
                            <legend>Milestone <a class="j-toggle-trigger"></a>
                            <a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">
                            <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" />
                            </a>
                            </legend>
                            <div class="j-toggle-content">
                            <div class="form-field">
                                    <label><span>Milestone Description:</span></label>
                                    <input type="hidden" name="contracts_milestones_id[<?=$i?>]" value="<?=$contracts_milestone['contracts_milestones_id']?>">
                                    <input type="text" name="contracts_milestones_description[<?=$i?>]" value="<?=$contracts_milestone['contracts_milestones_description']?>">
                            </div>
                            <div class="form-field">
                                    <label><span>Milestone Date:</span></label>
                                    <input type="text" name="contracts_milestones_date[<?=$i?>]" value="<?=date('n/j/Y',strtotime($contracts_milestone['contracts_milestones_date']))?>" class="milestone_date">
                            </div>
                            <div class="form-field" id="remind_days_<?=$i?>">
                                <label><span>Remind before:</span></label>
                                <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_<?=$i?>[]' value= 0 <?= in_array(0,$existing_milestone_remind_days)?'checked=checked':''?>><label class='milestone_check'><span>0 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_<?=$i?>[]' value= 30 <?= in_array(30,$existing_milestone_remind_days)?'checked=checked':''?>><label class='milestone_check'><span>30 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_<?=$i?>[]' value= 60 <?= in_array(60,$existing_milestone_remind_days)?'checked=checked':''?>><label class='milestone_check'><span>60 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_<?=$i?>[]' value= 90 <?= in_array(90,$existing_milestone_remind_days)?'checked=checked':''?>><label class='milestone_check'><span>90 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='contracts_milestones_remind_days_<?=$i?>[]' value= 120 <?= in_array(120,$existing_milestone_remind_days)?'checked=checked':''?>><label class='milestone_check'><span>120 Days</span></label>
                            </div>
                            <div class="form-field">
                                    <label>Send Emails To:</label><fieldset class="send-rem-check">
                                    <div class="remindMembersContainer" id="remind_milestone_<?=$i?>" class="remind_milestone"></div>
                            </div>
                            <label>Additional Email Recipients:</label>
                            <a href="#" class="add-item add-milestone-recipient" id="add-milestone-recipient_<?=$i?>">Add a recipient</a><br/>
                            <?foreach($milestones_additional_recipients as $milestones_additional_recipient){?>
                                <label><span>Email Address:</span></label> 
                                    <input name="contracts_milestones_additional_recipients_email_<?=$i?>[]" value="<?=$milestones_additional_recipient['contracts_milestones_additional_recipients_email']?>" maxlength="200" class="input-text"> 
                                    <a href="#" class="delete-recipient">Delete</a><br>
                           <? } ?>
                            <div id="additional-milestone-recipients_<?=$i?>"></div>
                            </div>  
                        </fieldset>
                    <?$i++; 
                    }
                }?>
            </div> 
            <a href="javascript:void(0);" id="add_milestone" class="add-item add-file-link">Add Another Milestone</a><br/>

            <fieldset class="j-toggle-parent">
                <legend>Files <a class="j-toggle-trigger"></a></legend>
                <div class="file-container j-toggle-content">
                    <table id="uploaded-files" class="file-table">
                        <tr>
                            <th>Active</th>
                            <th>File Name</th>
                            <th>Date Uploaded</th>
                            <th>Actions</th>
                        </tr>
                        <? if ($contract_files)
                            foreach ($contract_files as $file): ?>
                                <?  $filename = $file['files_name'];
                                if (strlen($filename) > 50) {
                                    $filename = substr($filename, 0, 40) . '...' . substr($filename, -7);
                                }
                                ?>
                                <tr>
                                    <td>
                                        <? if($file['contracts_files_active'] == 1) { ?>
                                            &check; Yes
                                        <? } ?>
                                    </td>
                                    <td>
                                        <a href="/members/contracts/download.php?contracts_id=<?= $_REQUEST['contracts_id'] ?>&contracts_files_id=<?= $file['contracts_files_id'] ?>"
                                           title="<?= $file['files_name'] ?>" target="_blank"><?= $filename ?></a>
                                    </td>
                                    <td>
                                        <? if($file['files_datetime']) { ?>
                                            <?= date('m/d/Y', strtotime($file['files_datetime'])) ?>
                                        <? } else { ?>
                                            Unknown
                                        <? } ?>
                                    </td>
                                    <td>
                                        <div class="actionMenu">
                                            <ul>
                                                <? if($file['contracts_files_active'] != 1) { ?>
                                                    <li>
                                                        <a href="set_active.php?contracts_files_id=<?= $file['contracts_files_id'] ?>">Set Active </a>
                                                    </li>
                                                <? } ?>
                                                <li>
                                                    <a href="/members/contracts/del-file.php?contracts_id=<?= $_REQUEST['contracts_id'] ?>&contracts_files_id=<?= $file['contracts_files_id'] ?>">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                    </table>
                    <a href="#" id="add_file" class="add-item add-file-link">Add a file</a><br/>
                </div>
            </fieldset>

            <br clear="all"/>

            <div class="form-actions">
                <?= html::input(($contract ? 'Save' : 'Add') . ' Contract', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'button')) ?>
            </div>

        </form>
        <input type="hidden" id="users_with_access">
        <div style="display:none">
            <input id="contract_file" type="file" name="contracts_file[]">
        </div>
    </div>

    <script type="text/javascript" src="/js/clickout.js"></script>
    <script type="text/javascript" src="/js/MooCal.js"></script>
    <script type="text/javascript" src="/js/moment.min.js"></script>
    <script type="text/javascript" src="/js/contracts.js?v=20180618"></script>

    <? include VIEWS . '/members/footer.php' ?>
