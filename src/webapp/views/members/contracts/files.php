<div>
    <?
    $db = mysqli_db::init();

    $contract_files = $db->fetch_all('SELECT contracts_files_id, files_name FROM contracts_files INNER JOIN files ON join_files_id = files_id WHERE join_contracts_id = ? ', array($_REQUEST['contracts_id']));
    ?>
    <ul>
        <? if (!$contract_files): ?>
            <li><b>No Attachments</b></li>
        <? else: ?>
            <? foreach ($contract_files as $k => $file): ?>
                <li>
                    <a href="/members/contracts/download.php?contracts_files_id=<?= $file['contracts_files_id'] ?>&type=.<?= files::get_extension($file['files_name']) ?>"
                       target="_blank"
                       title="<?= $file['files_name'] ?>"><?= $file['files_name'] ?></a><br/>
                </li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>
