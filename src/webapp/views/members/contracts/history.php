<div>
    <? $actions = contracts_history::get_contracts_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['contracts_id']); ?>
    <ul>
        <? if ($actions): ?>
            <? foreach ($actions as $action): ?>
                <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['contracts_history_datetime'])) ?>)</b>
                    - <?= $action['contracts_history_description'] ?></li>
            <? endforeach ?>
        <? else: ?>
            <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
