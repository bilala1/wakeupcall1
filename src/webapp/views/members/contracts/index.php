<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Contracts';
$page_keywords = '';
$page_description = '';

function get_corporation_display_name($contract)
{
    $echo_str = $contract['licensed_locations_name'];

    if (licensed_locations::is_corporate_location($contract['licensed_locations_id']))
        $echo_str = $echo_str . ' (Corporate Office)';

    return strings::wordwrap($echo_str, 9, '&shy;', true);
}

?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
<div class="page-header">
    <h1>Contract Management</h1>

    <div class="page-options">
        <div class="page-dialog-background"></div>
        <? if (ActiveMemberInfo::IsAccountMultiLocation() && !licensed_locations::get_licensed_locations_ids_for_member(ActiveMemberInfo::GetMemberId())): ?>
            <span title="Please add a location first">Add Contract</span>
        <? else: ?>
            <a href="/members/contracts/edit.php" class="add-item">Add Contract</a>
        <? endif ?>
        |
        <a href="/members/contracts/report.php">Excel Report</a> |
        <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a>
    </div>

    <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '3' : '2' ?> filter-contracts">

        <form action="index.php" id="filter" method="get">
            <div class="dialog-sections">

                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                    <div class="dialog-section">
                        <h3>Locations</h3>
                        <?= html::location_filter('contracts', $model['join_licensed_locations_id']) ?>
                    </div>
                <? } ?>
                <div class="dialog-section dates">
                    <h3>Expiration Date</h3>
                    <label>Quick Filter:</label><?= html::select(array('' => '') + array(
                            30 => 'Next 30 days',
                            60 => 'Next 60 days',
                            90 => 'Next 90 days'
                        ), 'quick_filter', '', array(), $errors); ?><br>
                    <label>From:</label><?= html::date($model, 'contracts_start_time', array()) ?><br>
                    <label>To:</label><?= html::date($model, 'contracts_end_time', array()) ?>
                </div>
                <div class="dialog-section">
                    <h3>Display Options</h3>
                    <label><?= html::radio(0, 'contracts_show_hidden', $model) ?> Show Active Contracts</label><br>
                    <label><?= html::radio(1, 'contracts_show_hidden', $model) ?> Show Hidden Contracts</label><br>
                    <h3>Threshold for pending Expiration</h3>
                    <?= html::select(array(
                            30 => '30 days',
                            60 => '60 days',
                            90 => '90 days'
                        ), 'pref', $userPrefs['user_prefs_contract_expire_lead_days'], array(), $errors); ?><br>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::submit("Clear Filter", array('name' => 'clear')) ?>
            </div>
        </form>
    </div>
</div>

<? if ($contracts_show_hidden == '1'): ?>
    <h2>Hidden Contracts</h2>
<? endif ?>

    <? if(empty($contracts)) { ?>
        <h3>No contracts to display</h3>
        <p>Add a contract, or check/reset to filters to see more contracts.</p>
    <? } ?>

<? foreach($contract_types as $contract_type => $contracts) { ?>
    <? if ($contracts) { ?>
<h3><?= $contracts_status_values[$contract_type] ?> Contracts</h3>
<table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear contracts">
    <tr>
        <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
            <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
        <? endif ?>
        <th><?= strings::sort('contracts_contracted_party_name', 'Contracted<br>Party', null, null, null, 'DESC') ?></th>
        <th><?= strings::sort('contracts_contact_name', 'Contact<br>Name') ?></th>
        <th><?= strings::sort('contracts_effective_date', 'Effective<br>Date') ?></th>
        <th><?= strings::sort('contracts_expiration_date', 'Expiration<br>Date') ?></th>
        <th>Active Contract File</th>
        <th><?= strings::sort('contracts_description', 'Description') ?></th>
        <th style="width:55px">Actions</th>
    </tr>
    <? foreach ($contracts as $contract):
            $doHighlight = false;
            if($contract['contracts_expiration_date'] && $contract['contracts_expiration_date'] != '0000-00-00'){
                $days_diff = round((strtotime($contract['contracts_expiration_date'])- time())/(60 * 60 * 24));
                if($days_diff < $userPrefs['user_prefs_contract_expire_lead_days']) $doHighlight = true;
            }
            $expired='';
            if($contract['contracts_expiration_date'] != '0000-00-00'){
                $expired = strtotime($contract['contracts_expiration_date']) < time(); 
            } 
            if($contract['files_name'] == '' || $expired)$doHighlight = true;
        
         $perms = UserPermissions::UserObjectPermissions('contracts', $contract); ?>
        <tr <?= ($doHighlight == true) ? 'class="notices"' : '' ?>>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= strings::wordwrap($contract['licensed_locations_name'], 9, '&shy;', true); ?></td>
            <? endif ?>
            <td><?= strings::wordwrap($contract['contracts_contracted_party_name'], 9, '&shy;', true) ?></td>
            <td><?= strings::wordwrap($contract['contracts_contact_name'], 9, '&shy;', true) ?></td>
            <td><?= date('m/d/Y', strtotime($contract['contracts_effective_date'])) ?></td>
            <td <?= $expired?'class="error"' : ''?>><?= empty($contract['contracts_expiration_date']) ? 'Expiration Date Missing' : date('m/d/Y', strtotime($contract['contracts_expiration_date'])) ?></td>
            <td class="hyphenate <?= $contract['files_name']?'':' error'?>" >
            <?php if($contract['files_name'] != ''): ?>
                <a target="_blank" href="/members/contracts/download.php?contracts_files_id=<?= $contract['contracts_files_id'] ?>&type=.<?= files::get_extension($contract['files_name']) ?>"><?= $contract['files_name'] ?></a>
            <? else: ?>
                <span>Need to Upload or Set File to 'Active'!</span>
            <? endif; ?>
            </td>
            <td><?= strings::wordwrap($contract['contracts_description'], 9, '&shy;', true) ?></td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if ($perms['edit']) { ?>
                            <li>
                                <a href="/members/contracts/edit.php?contracts_id=<?= $contract['contracts_id'] ?>">Edit</a>
                            </li>
                        <? } ?>
                        <li><a href="#" class="history" id="history_<?= $contract['contracts_id'] ?>">History</a></li>
                        <li><a href="#" class="files" id="files_<?= $contract['contracts_id'] ?>">Files</a></li>
                        <li><a href="#" class="details" id="details_<?= $contract['contracts_id'] ?>">Details</a></li>
                        <? if ($perms['delete']) { ?>
                            <li><a href="/members/contracts/delete.php?contracts_id=<?= $contract['contracts_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                        <? } ?>
                        <? if ($perms['edit']) { ?>
                            <? if ($contract['contracts_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/contracts/hide.php?contracts_id=<?= $contract['contracts_id'] ?>&hide_contract=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/contracts/hide.php?contracts_id=<?= $contract['contracts_id'] ?>&hide_contract=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
        </tr>
    <? endforeach; ?>
    <tr>
        <td colspan="99"><?= $paging->get_html(); ?></td>
    </tr>
</table>
    <? } ?>
<? } ?>

</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/contracts_list.js?v=20180618"></script>
<? if(!$infobox): ?>
    <script type="text/javascript">
    window.addEvent('domready', function () {
        <? /* open or collapse infobox? */ ?>
        $$('.page-desc').setStyle('display', 'none');
        $$('.info_text').set('html', 'Show Text Box');
    });
    </script>
<? endif; ?>
<? include VIEWS . '/members/footer.php' ?>
