<?
$page_title = SITE_NAME;
$page_name = 'Resources';
$sub_name = 'Definitions';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

    <div id="content">
            <h1>Definitions</h1>

            <form>
                <?= html::input($_REQUEST, 'term', array()) ?>
                <?= html::submit('Search') ?>
                <br clear="all"/>
            </form>

            <?php if (isset($_REQUEST['term'])) { ?>
                <h2>Search Results for: <?= $_REQUEST['term']; ?></h2>
                <? if ($terms): ?>
                    <table>
                        <tr>
                            <th>Term</th>
                            <th>Definition</th>
                        </tr>
                        <? foreach ($terms as $term): ?>
                            <tr>
                                <td>
                                    <?= str_replace($_REQUEST['term'],
                                        '<span class="search_highlight">' . $_REQUEST['term'] . '</span>',
                                        $term['definitions_word']); ?>
                                </td>
                                <td style="text-align: left;">
                                    <?= $term['definitions_description']; ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                <? else: ?>
                    <p style="text-align: center">
                        Nothing was found.<br/>
                        Would you like to request a definition?
                    </p>
                    <div class="form-actions">
                        <input type="hidden" name="term" value="<?= $_REQUEST['term'] ?>"/>
                        <input type="submit" id="reqDef" class="btn-primary btn-ok" value="Request Definition"/>
                        <script>
                            $("reqDef").addEvent("click", function () {
                                var term = <?= '\'' . $_REQUEST['term'] . '\'' ?>;
                                var reqURL = '<?= FULLURL ?>/members/req-def.php';
                                var jsonRequest = new Request.JSON({
                                    url: reqURL,
                                    onSuccess: function (response) {
                                        var p = new Element('p');
                                        p.set('html', 'Your request has been submitted. We will be in touch shortly.');
                                        p.inject('reqDef', 'after');
                                        $('reqDef').destroy();
                                    }
                                }).get({'term': term});
                            })
                        </script>
                    </div>
                <? endif; ?>
            <?php } ?>
    </div>

<? include VIEWS . '/members/footer.php' ?>