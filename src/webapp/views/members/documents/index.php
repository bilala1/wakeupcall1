<?
    $page_title        = SITE_NAME;
    $page_name         = 'Resources';
    $sub_name          = 'Document Library';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <span class="info_icon"></span>

    <p class="page-desc">
        <?= SITE_NAME ?> has created a library of documents and links to assist you with your HR and other liability
        needs. We have made a concentrated effort to streamline the library to make it efficient and easy to use. We
        have partnered with HR360 to bring you additional HR material pertinent to federal, state and local everyday
        matters along with new and ongoing legislation that may affect your business. The HR360 links are highlighted in
        green text so they can be easily identified. You can save documents and HR360 links to your My Files area
        on <?= SITE_NAME ?></p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear"/>

    <div style="float:right;padding-right: 15px;">
			<form action="/members/search.php" method="get">
				<strong>Search</strong>
				<input type="text" class="text"	name="q" style="width:150px;" />
				<input type="hidden" name="type" value="docs" />
				<input class="btn" type="submit" value="Search" />
			</form>
		</div>
        <h1 style="padding-bottom: 10px;"><?= ($_REQUEST['join_library_categories_id']) ? library_categories::get_category_name($_REQUEST['join_library_categories_id']) : 'Document Library' ?></h1>
        
        <div style="width:32%; float:left;">
            <table style="width: 100%; border-top: 1px dotted #ccc">
            <? foreach(library_categories::get_library_categories_levels() as $category): ?>
                <tr <?= $category['sub_level'] != 0 ? 'class="subcat' . $category['join_library_categories_id'] . '"' : '' ?> 
                    <? if(isset($_REQUEST['join_library_categories_id'])) {
                        if($category['sub_level'] != 0) {
                            if($category['join_library_categories_id'] != $parent_id) {
                                print 'style="display:none"';
                            }
                        }
                    }
                    elseif ($category['sub_level'] != 0) {
                        print 'style="display:none"';
                    }
                    ?>>
                    <td class="tbody" style="padding: 5px 2px 5px <?= 10+($category['sub_level']*10) ?>px">
						<? if($category['has_children']): ?>
						<? if($parent_id != 0 && $category['library_categories_id'] == $parent_id): ?>
						<img src="/images/arrow-down.png" <?= $category['has_children'] ? 'onClick="changeArrow(this);showChildren(' . $category['library_categories_id'] . ')"' : '' ?> style="cursor:pointer" />
						<? else: ?>
						<img src="/images/arrow-right.png" <?= $category['has_children'] ? 'onClick="changeArrow(this);showChildren(' . $category['library_categories_id'] . ')"' : '' ?> style="cursor:pointer" />
						<? endif; ?>
						<? endif; ?>
						
						<a name="<?= str_replace(' ','_',$category['library_categories_name']) ?>" <?= !$category['has_children'] ? 'style="margin-left:8px"' : '' ?> href="<?= FULLURL; ?>/members/documents/index.php?join_library_categories_id=<?= $category['library_categories_id'] ?>">
							<?= $category['library_categories_name'] ?>
						</a><? /* (<?= $category['num_documents']; ?>) */ ?>
					</td>
                </tr>
            <? endforeach; ?>
            <tr>
                <td class="tbody"><a href="<?= FULLURL; ?>/members/documents/index.php?viewall=true">View All</a></td>
            </tr>
            </table>
        </div>

        <div style="width:68%; float:left; clear: right;">

            <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
                <tr>
                    <th><?= strings::sort('documents_title', 'Name') ?></th>
                    <th>Actions</th>
                </tr>
                <? foreach ($documents as $document): ?>
                    <tr>
                        <td><span
                                class="icon-<?= strtolower(files::get_extension($document['files_name'])) ?> docLibrary-format-<?= $document['documents_format'] ?>">
                                <?= $document['documents_title'] ?>
                            </span></td>
                        <td style="white-space:nowrap;">
                            <? if ((members::get_members_status(ActiveMemberInfo::GetMemberId()) == 'free') && ((int)$document['documents_freetrial'] == 0)) { ?>
                                <a class="download-view" title="Restricted for trial members">Download/View</a>
                            <? } else {
                                $text = 'Download/View';
                                $target = '_blank';
                                switch ($document['documents_format']) {
                                    case 'url':
                                        $url = $document['documents_filename'];
                                        $text = 'Open Link';
                                        break;
                                    case 'hr360':
                                        $info = Services\Services::$hrProvider->getLinkInfo($document['documents_file']);
                                        if(!$info['newWindow']) {
                                            $target = '_self';
                                        }
                                        $url = $info['url'];
                                        $text = 'View on HR360';
                                        break;
                                    case 'file':
                                    default:
                                        $url = '/members/documents/download.php?documents_id=' . $document['documents_id'] .
                                            '&type=.' . files::get_extension($document['files_name']);
                                        break;
                                }
                                ?>
                                <a target="<?= $target ?>"
                                   class="imageLink docLibrary-format-<?= $document['documents_format'] ?>"
                                   href="<?= $url ?>"><?= $text ?></a>
                                <?
                            } ?>
                            |
                            <? if ((int)$document['in_library'] < 1): ?>
                                <a class="add-to-my-wuc"
                                   href="/members/documents/add-document.php?documents_id=<?= $document['documents_id'] ?><?= $_REQUEST['join_library_categories_id'] ? '&join_library_categories_id=' . $_REQUEST['join_library_categories_id'] : ''; ?>">Add
                                    to My WAKEUP CALL </a>
                            <? else: ?>
                                Already in library
                            <? endif; ?>
                        </td>
                    </tr>
                <? endforeach; ?>
            </table>
            <? if ($total > 25): ?>&nbsp;Pages: <?= $paging->get_html(array('style' => 'display: inline; margin-left: 5px')); ?><? endif ?>
        </div>
    </div>
    <script type="text/javascript">
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
           $$('.page-desc').setStyle('display', 'none');
           $$('.info_text').set('html','Show Text Box');
        <? endif; ?>
        
        function changeArrow(img) {
            if(img.src.indexOf('arrow-right.png') > -1) {
                img.src = '/images/arrow-down.png';
            }
            else {
                img.src = '/images/arrow-right.png';
            }
        }
        function showChildren(parentcat) {
            $$('[class=subcat' + parentcat + ']').each(function(e){
                if(e.getStyle('display') == 'none') {
                    if(Browser.name == 'ie' && parseInt(Browser.version) < 8) {
                        e.setStyle('display','block');    
                    }
                    else {
                        e.setStyle('display','table-row');
                    }
                }
                else {
                    e.hide();
                }
            });
        }
        
        window.addEvent('load', function(){
            //$$('[class^=subcat]').setStyle('display','none');
			$$('.download-view').addEvent('click', function() {
				new Element('<div>', {html: '<br /><h3 style="text-align:center">Sorry, this document is not available during your Trial membership.</h3>'}).MooDialog({size:{width:330}});
			});

            //infobox settings
            var webinarsRequest = new Request({
            url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
            method: 'get',
                onSuccess: function(responseText){
                    if(responseText == 'on'){
                        $$('.page-desc').reveal();
                        $$('.info_text').set('html','Hide Text Box');
                    }
                    else {
                        $$('.page-desc').dissolve();
                        $$('.info_text').set('html','Show Text Box');
                    }
                }
            });
            $$('.info_icon, .info_text').addEvent('click', function(event){
                if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                    var show = 'true';
                }
                else {
                    var show = 'false';
                }
                webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_doc_library');
            });
        });

        window.addEvent('domready',function(){
            $$('.add-to-my-wuc').addEvent('click', function(){
                var documents_id = this.get('href').split("=")[1];

				var html = '<option value="-1">- Select Folder -</option><option value="0">General</option>';
                <? if(is_array(documents::get_my_folders())): ?>
                    <? foreach (documents::get_my_folders() as $k => $folder): ?>
                        html += '<option value="<?= $k ?>" ><?= htmlentities(addslashes(strlen($folder)>18 ? substr($folder,0,18).' ...' : $folder)) ?></option>';
                    <? endforeach; ?>
                <? endif; ?>
                var dropdown = new Element('select', {
                                            html: html,
                                            'class': 'myClass',
                                            styles: {
                                                display: 'none'
                                            },
                                            events: {
                                                change: function(){
                                                    if(this.get('value')=='-1'){
                                                        alert('Please select a folder for this file');
                                                    }
                                                    else {
                                                        var Req = new Request({
                                                                               method: 'get',
                                                                               url: '<?= FULLURL; ?>/members/documents/add-document.php',
                                                                               onSuccess: function(responseText){
                                                                                   var success = new Element('span', { html: 'Added to library'});
                                                                                   success.inject(dropdown,'before');
                                                                                   dropdown.destroy();
                                                                                  // $$('.notice')[0].set('text', responseText);
                                                                                  // $$('.notice')[0].setStyle('display', 'inline');
                                                                               }
                                                                             });
                                                            Req.send('documents_id='+documents_id+'&join_library_categories_id='+this.get('value'));
                                                       }
                                                },
                                                mouseover: function(){

                                                }
                                            }
                });
                dropdown.inject(this,'before');
                dropdown.set('value',-1);
                dropdown.set('reveal', { display:'inline'});
                dropdown.reveal();
                this.destroy();

                return false;
            })
        })
    </script>

<? include VIEWS. '/members/footer.php' ?>
