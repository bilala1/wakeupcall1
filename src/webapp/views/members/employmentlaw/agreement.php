<html>
<header>
<link rel="stylesheet" type="text/css" href="/css/style.css"/>
<style type="text/css">
    ul li {
        line-height: 15px;
        
    }
</style>
</header>    

<body style="text-align: left; padding: 10px;">
    
<span style="text-align: center"><h2>DISCLAIMER AND AGREEMENT AS TO WAKEUP CALL MEMBER EMPLOYMENT ASSISTANCE HOTLINE </h2></span>
    
<p>
    WAKEUP CALL has partnered with the nationally recognized law firm Elser, Moskowitz, Edelman & Dicker LLP (WILSON ELSER), to offer an e-mail Loss Assistance Hotline. 
    This hotline provides you with a free hour of legal consultation with a knowledgeable attorney on any employment matter that you feel is pertinent to your business. 
</p>

<p>
    In order to utilize this service and prior to the rendering of any legal advice, as the WAKEUP CALL member for this account, you must agree to and acknowledge the terms posted on this site as follows:
</p>

<p>
    <ol>
        <li>You will be contacting WILSON ELSER for purposes of obtaining legal advice with respect to questions concerning labor and employment law. </li>
        <li>For up to the first hour of time WILSON ELSER will provide its response at no charge. All services after one hour will be at the discounted rate of $275 Partners and $225 Associates.</li>
        <li>Your e-mail form must identify your name, company and the position in the company on whose behalf you are asking the online question, the identification of any potential adverse parties or at least other relevant parties, so a conflict check can be done by WILSON ELSER. </li>
        <li>
            Your electronic acknowledgment, understanding and agreement that the relationship with WILSON ELSER is limited in scope to questions you, the member, pose and the answers you, the member, are given. 
            Any broader relationship must be agreed to by WILSON ELSER in writing; and an electronic acknowledgment that absent an agreement with WILSON ELSER in writing, its relationship with WILSON ELSER terminates when WILSON ELSER answers the questions you, the member, poses.
        </li>
        <li>
            Your electronic acknowledgment, understanding and agreement that this is NOT a claim reporting service. Any communications with WILSON ELSER, and any and all future communications and/or correspondence with WILSON ELSER do not constitute notice of a claim to your insurer in that it is your, the member's, responsibility to provide written notice of a potential claim to your appropriate carrier. 
            To report a Claim, follow the Claim reporting instructions in your insurance policy and also notify your insurance agent. 
        </li>
        <li>
            Your electronic acknowledgment, understanding and agreement that the initial input provided by WILSON ELSER is for guidance only and is not intended to provide calculated legal advice to resolve any particular risk management problem; therefore you <u>should not base their course of action solely upon WILSON ELSER's communications.</u> 
            There are no promises or guarantees made as to any outcome. 
        </li>
        <li>WILSON ELSER reserves the right not to answer any e-mail question immediately upon written response to you if it feels that there are any conflicts or ethical issues. </li>
        <li>You have been given the reasonable opportunity to seek the advice of independent legal counsel of your choice with respect to all matters contained in this electronic agreement.  </li>
        <li>
            WILSON ELSER will track and record questions and answers and may use these questions and answers in a generic manner for Wakeup Call to use for their FAQ database. 
            Questions and answers will be rephrased in such a way as not to identify any members, entities or people specifically.
        </li>
    </ol>
</p>

<p>
    Please note, that for purposes of the above Disclaimer and Agreement you, anyone representing you, or anyone you have or have not given permission to use this Employment Assistance Email Hotline or WAKEUP CALL services in association with your membership, are considered the WAKEUP CALL member.
</p>

<p>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I, individually on behalf of my company have reviewed and agree to the above conditions and by electronically acknowledging my consent.
</p>


</body>
</html>          
