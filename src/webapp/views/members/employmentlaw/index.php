<?
    $page_title        = SITE_NAME;
    $page_name         = 'Employee Mgmt';
    $sub_name		   = 'Employment Law';
    $page_keywords     = 'employment law';
    $page_description  = 'Employment Law';
?>
<? include VIEWS. '/members/header.php' ?>

<style type='text/css'>
    #elaw .form-field label {
        width: 190px;
    }
	#elaw input[type=text].readonly {
		background-color: #CCC;
		color: #444;
	}
</style>

<script type="text/javascript">
	window.addEvent('domready', function() {
       var myAccordion = new Fx.Accordion($('accordion'), 'h3.toggler', '#accordion .content', {
		   opacity: false,
		   onActive: function(toggler, element) {
			   toggler.setStyle('color', '#005DAA');
               toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle_down.jpg") no-repeat scroll 0 5px transparent');
		   },
		   onBackground: function(toggler, element) {
			   //toggler.setStyle('color', '#D31145');
               toggler.setStyle('color', '#005DAA');
               toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle.jpg") no-repeat scroll 0 5px transparent');
		   }
	   });
       
       	$$('h3[class^=toggler]').each(function(e) {
            e.addEvent('mouseover',function(){
                this.setStyle('background-color','#f4efa4');
            });
            e.addEvent('mouseout',function(){
                this.setStyle('background-color','#FCFBFB');
            });

            e.addEvent('click', function(){
                var number = parseInt(e.get('class').match(/[0-9]/gi));
                $$('.element').each(function(e) {
                    if(e.get('class').match(number)){
                        if(e.get('style').match(/height: auto/gi)){
                            myAccordion.display(-1);
                        }
                    }
                });     
            })
        }); 
    })
    
    window.addEvent('load', function(){
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
           $$('.page-desc').setStyle('display', 'none');
           $$('.info_text').set('html','Show Text Box'); 
        <? endif; ?>
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_empl_law');
        });
    })        
</script>

<div id="content">
        <span class="info_icon"></span>
        <div class="page-desc">
           <?= SITE_NAME ?> has partnered with WilsonElser to provide you with answers to your employment law questions via 
           email. You may see more information on WilsonElser in the PDF link below or in the Additional Services section. The 
           form below, including your question, will be emailed directly to the WilsonElser office, where it will be directed to 
           the appropriate attorney. Their response will be sent to the email address associated with your WAKEUP CALL account. 
           <br />
           <br />
           Please keep all questions related to employment law issues only. It may be helpful and faster for you to review our 
           employment law frequently asked questions (FAQ's), below the Question Form, before emailing WilsonElser. 
        </div>
        <span class="info_text">Hide Text Box</span>
        <br class="clear" />

        <h1>Employment Law</h1>

        <div id="accordion">
        	<h3 class="toggler">Question Form</h3>
        	<div class="content">
        		<form action="" method="post" style="padding: 5px;" id="elaw">

        			<?/*
        			Please do not set these to disabled. There will be a blank email sent if you do,
        			and the Children of the Corn will hunt you down and disable *YOU*!
        			*/?>

        			<?= html::textfield('Location/Business Name:', $member, 'entity_name', array('readonly' => 'readonly', 'type' => 'text','class'=>'readonly'), $errors); ?>
        			<?= html::textfield('Your Name:', $member, 'members_fullname', array('readonly' => 'readonly', 'type' => 'text','class'=>'readonly'), $errors); ?>
                    <?= html::textfield('Position/Title:', $member, 'members_title', array('readonly' => 'readonly', 'type' => 'text','class'=>'readonly'), $errors); ?>
        			<?= html::textfield('Address:', $member, 'address', array('readonly' => 'readonly', 'type' => 'text','class'=>'readonly'), $errors); ?>
        			<div class="form-field">
        				<label><span>&nbsp;</span></label><input type="text" style="width: 120px; margin-right: 3px;" class="input-text readonly" name="city" id="city" value="<?= $member['city'] ?>" readonly="readonly" />
        				<input type="text" style="width: 36px; margin-right: 3px" class="input-text readonly" name="state" id="state" value="<?= $member['state'] ?>" readonly="readonly" />
        				<input type="text" style="width: 68px" class="input-text readonly" name="zip" id="zip" value="<?= $member['zip'] ?>" readonly="readonly" />
        			</div>
        			<?= html::textfield('E-mail:', $member, 'members_email', array('readonly' => 'readonly', 'type' => 'text','class'=>'readonly'), $errors); ?>
                    <?= html::textfield('Names of Parties Involved:', $member, 'parties_involved', array('type' => 'text'), $errors); ?>
                    
					<?= html::textareafield('Question:', '', 'question_content', array('rows' => 10, 'style' => 'width: 450px; resize: none;'), $errors) ?>
					<div class="form-field">
						<label>&nbsp;</label>
						<?= html::submit('Submit', array('class' => 'btn')) ?>
					</div>
        		</form>
        		<a href="/data/WilsonElser_Capabilities.pdf" style="display:block; margin-left: 150px; margin-bottom: 15px;" target="_blank">Click here to view Attorney PDF.</a>
        	</div>

        	<h3 class="toggler">FAQ's</h3>
        	<div class="content">
        		<div style="padding: 5px; overflow: auto; height: 400px">
		    		<? foreach ($elaw_faqs as $faq): ?>
		    		<div class="divider">
		    			<h3>Q: <?= $faq['faqs_question'] ?></h3>
		    			A: <?= $faq['faqs_answer'] ?>
		    		</div>
		    		<? endforeach; ?>
        		</div>
        	</div>
        </div>
    </div>

<? include VIEWS . '/members/footer.php' ?>
