<?
    $page_title        = SITE_NAME;
	$page_name         = 'Employee Mgmt';
	$sub_name		   = 'EMployment Law';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<script type="text/javascript">
	window.addEvent('load', function() {
	<? /* open or collapse infobox? */ ?>
	<? if (!$infobox): ?>
		$$('.page-desc').setStyle('display', 'none');
		$$('.info_text').set('html', 'Show Text Box');
		<? endif; ?>
		//infobox settings
		var webinarsRequest = new Request({
			url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
			method: 'get',
			onSuccess: function(responseText) {
				if (responseText == 'on') {
					$$('.page-desc').reveal();
					$$('.info_text').set('html', 'Hide Text Box');
				}
				else {
					$$('.page-desc').dissolve();
					$$('.info_text').set('html', 'Show Text Box');
				}
			}
		});
		$$('.info_icon, .info_text').addEvent('click', function(event) {
			if ($$('.page-desc')[0].get('style').match(/display: none/gi)) {
				var show = 'true';
			}
			else {
				var show = 'false';
			}
			webinarsRequest.send('yn=' + show + '&type=members_settings_infobox_empl_law');
		});
	});
</script>

<div id="content">
		<span class="info_icon"></span>
		<p class="page-desc">
			<?= SITE_NAME ?> has partnered with WilsonElser to provide you with answers to your employment law questions 
            via email. You may see more information on WilsonElser in the PDF link below or in the Additional Services section. 
            The form below, including your question, will be emailed directly to the WilsonElser office, where it will be 
            directed to the appropriate attorney. Their response will be sent to the email address associated with 
            your <?= SITE_NAME ?> account. 
            <br /><br />
            Please keep all questions related to employment law issues only. It may be helpful and faster for you to 
            review our employment law frequently asked questions (FAQ's), below the Question Form, before emailing WilsonElser. 
		</p>
		<span class="info_text">Hide Text Box</span>
		<br class="clear"/>


		<h1>Employment Law</h1>

        <div style="padding-top: 10px">
        	<fieldset style="width:auto">
        		<label><h3>Agreement:</h3></label>

        		<iframe width="100%" height="300" src='agreement.php'></iframe>

        		<form method="post" action="">
        			<?= html::checkboxfield('<b>I agree:</b>', 1, 'members_elaw_agreement_yn', array(), $errors) ?>

        			<input class="input-submit btn" type="submit" name="submit" value="Submit" />
        		</form>
        	</fieldset>
        </div>
    </div>

<? include VIEWS . '/members/footer.php' ?>
