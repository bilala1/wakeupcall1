<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = 'Businesses';
$page_keywords = '';
$page_description = '';
function limitWrods($string){
    $wc = str_word_count($string);
    if($wc > 30){
        $array = explode(" ", $string);
        array_splice($array, 30);
        $string = implode(" ", $array)." ...";
    }
    return($string);
}
?>
<script>
    var form_mode = '';
    <?if($business){?>
    form_mode = 'edit';
    <?}?>
</script>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
    <h1 class="left"><?= $business['entities_id'] ? 'Edit' : 'Add' ?> Business</h1>

    <div class="page-options"><a href="/members/entities/business/index.php">Return to Businesses List</a></div>
    <br clear="all"/>

    <form action="/members/entities/business/edit.php" name="editbusiness" id="editbusiness" method="post"
          onSubmit="return chkBusinessForm(this)" enctype="multipart/form-data">
        <fieldset class="j-toggle-parent">
            <legend>Business Details <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
            <?= html::hidden($business, 'business_entities_id') ?>

            <?= html::hidden($_REQUEST, 'business_entities_id') ?>
            <?= html::hidden($_REQUEST, 'entities_id') ?>

                <?= html::textfield('Business Name:', $business, 'entities_name', array('maxlength' => '255'),
                    $errors) ?>
            
            <? if(!ActiveMemberInfo::IsUserMultiLocation()) {
                echo html::plainTextField("Associated Location", $licensed_location_name);
            }else{
                echo html::location_picker($business, $errors, 'business_entities',false, "Associated Location(s)");
            }?>
            <?if($business){?>
                <div class="callout" id='location_change_message' style="display: none">
                When changing locations, please verify that the recipients for any tracked item milestones are still appropriate.   
                </div>
            <?}?>
            </div>
        </fieldset>

        <? if($business) { ?>
            <fieldset class="j-toggle-parent">
                <legend>Tracked Items <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                 <a href="/members/entities/items/edit.php?entities_id=<?=$business['entities_id'] ?>" class="add-item">Add a tracked item</a>
                 <? if(count($entity_item_data)>0){ ?>
                  <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
                        <tr>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Next Milestone</th>
                            <th style="width:85px">Actions</th>
                        </tr>
                        <? foreach ($entity_item_data as $entity_item){
                            $milestone = entities_items::get_next_milestone_for_entity_item($entity_item['entities_items_id']);
                            if(empty($milestone)){
                                $next_milestone = "No Milestones";
                            }else{
                                $next_milestone = $milestone['entities_items_milestones_description'] . ' on ' . $milestone['entities_items_milestones_date']; 
                            }
                            ?>
                        <tr>
                            <td><?= $entity_item['entities_items_name'] ?></td>
                            <td><?= $entity_item['entities_items_details'] ?></td>
                            <td><?= $next_milestone ?></td>
                            <td>
                                <div class="actionMenu">
                                    <ul>
                                        <li><a href="/members/entities/items/edit.php?entities_id=<?=$business['entities_id'] ?>&entities_items_id=<?= $entity_item['entities_items_id'] ?>">Edit</a></li>
                                        <li><a href="#" class="history" id="history_<?= $entity_item['entities_items_id'] ?>">History</a></li>
                                        <li><a href="#" class="notes_popup" id="notes_<?=$business['entities_id'] ?>_<?= $entity_item['entities_items_id'] ?>">Notes</a></li>
                                        <li><a href="#" class="files" id="files_<?=$business['entities_id'] ?>_<?= $entity_item['entities_items_id'] ?>">Files</a></li>
                                        <li><a class="deletecheck" href="/members/entities/items/delete.php?entities_items_id=<?= $entity_item['entities_items_id'] ?>&business_entities_id=<?=$_REQUEST['business_entities_id']?>">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <? } ?>
                  </table>
                     
                <? }
                 
                 ?>
                </div>
            </fieldset>
        <? } ?>
        <div class="form-actions">
            <?= html::input($business ? 'Save Business' : 'Save to Start Tracking', 'save',
                array('class' => 'btn-primary btn-ok', 'type' => 'button')) ?>
        </div>

    </form>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/businesses.js?v=20180618"></script>

<? include VIEWS . '/members/footer.php' ?>
