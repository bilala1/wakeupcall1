<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = 'Businesses';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>
<div id="content">
    <div class="page-header">
        <h1><? if( $businesses_show_hidden == '1') { ?>Hidden <? } ?>Business/Event Tracking</h1>

        <div class="page-options">
            <div class="page-dialog-background"></div>
            <? if (ActiveMemberInfo::IsAccountMultiLocation() && !licensed_locations::get_licensed_locations_ids_for_member(ActiveMemberInfo::GetMemberId())): ?>
                <span title="Please add a location first">Add Business Entity</span>
            <? else: ?>
                <a href="/members/entities/business/edit.php" class="add-item">Add Business Entity</a>
            <? endif ?> |
            <a class="page-dialog-trigger" data-target=".filter-body" tabindex><i class="fa fa-caret-down"></i> Filter</a>
        </div>
        
        <div class="page-dialog filter-body page-dialog-1 filter-business">

            <form action="index.php" id="filter" method="get">
                <div class="dialog-sections">
                    <div class="dialog-section">
                        <h3>Display Options</h3>
                        <label><?= html::radio(0, 'businesses_show_hidden', $model) ?> Show Active Business</label><br>
                        <label><?= html::radio(1, 'businesses_show_hidden', $model) ?> Show Hidden Business</label><br>

                    </div>
                </div>
                <div class="form-actions">
                    <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                    <?= html::submit("Clear Filter", array('name' => 'clear')) ?>
                </div>
            </form>
        </div>
    </div>

    <? if (empty($businesses)) { ?>
        <h3>No businesses to display</h3>
    <? } ?>

    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th><?= strings::sort('entities_name', 'Name') ?></th>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <th><?= strings::sort('licensed_locations_name', 'Associated Location(s)') ?></th>
            <? endif ?>
            <th>Next Milestone</th>
            <th style="width:55px">Actions</th>
        </tr>
        <? foreach ($businesses as $entity): 
            $milestone = entities_items::get_next_milestone_for_entity($entity['entities_id']);
            if(empty($milestone)){
                $next_milestone = "No Milestones";
            }else{
                $next_milestone = $milestone['entities_items_milestones_description'] . ' for ' .$milestone['entities_items_name'] .' on ' . $milestone['entities_items_milestones_date'];
            }
            ?>
            <tr>
                <td><?= strings::wordwrap($entity['entities_name'], 9, '&shy;', true) ?></td>
                <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                    <td><?= strings::wordwrap($entity['licensed_locations_name'], 9, '&shy;', true); ?></td>
                <? endif ?>
                <td><?=$next_milestone?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li>
                                <a href="/members/entities/business/edit.php?business_entities_id=<?= $entity['business_entities_id'] ?>">Edit</a>
                            </li>
                            <li><a href="#" class="history" id="history_<?= $entity['entities_id'] ?>">History</a></li>
                            <li><a href="/members/entities/business/delete.php?business_entities_id=<?= $entity['business_entities_id'] ?>"
                                   class="deletecheck">Delete</a></li>
                            <? if ($entity['entities_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/entities/business/hide.php?business_entities_id=<?= $entity['business_entities_id'] ?>&hide_business=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/entities/business/hide.php?business_entities_id=<?= $entity['business_entities_id'] ?>&hide_business=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>

</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
    window.addEvent('domready', function () {
        $$('.history').addEvent('click', function () {
            var parts = this.get('id').split('_');

            var id = parts[1];

            var reqDialog = new MooDialog.Request(BASEURL + '/members/entities/history.php?entities_id=' + id, null, {
                'class': 'MooDialog myDialog',
                autoOpen: false,
                size: {
                    width: 450,
                    height: 300
                }
            });

            reqDialog.setRequestOptions({
                onRequest: function () {
                    reqDialog.setContent('loading...');
                }
            }).open();

            return false;
        });

        $$('.files').addEvent('click', function () {
            var parts = this.get('id').split('_');

            var id = parts[1];

            var reqDialog = new MooDialog.Request(BASEURL + '/members/entities/files.php?entities_id=' + id, null, {
                'class': 'MooDialog myDialog',
                autoOpen: false,
                size: {
                    width: 450,
                    height: 300
                }
            });

            reqDialog.setRequestOptions({
                onRequest: function () {
                    reqDialog.setContent('loading...');
                }
            }).open();

            return false;
        });

        $$('.notes').addEvent('click', function () {
            var parts = this.get('id').split('_');

            var id = parts[1];

            var reqDialog = new MooDialog.Request(BASEURL + '/members/entities/notes/notes_popup.php?type=business&entities_id=' + id, null, {
                'class': 'MooDialog myDialog',
                autoOpen: false,
                size: {
                    width: 450,
                    height: 300
                }
            });

            reqDialog.setRequestOptions({
                onRequest: function () {
                    reqDialog.setContent('loading...');
                }
            }).open();

            return false;
        });

        $$('body').addEvent('click:relay(a.viewNote)', function (e) {
            var parts = this.get('id').split('_');
            var id = parts[1];

            var reqDialog = new MooDialog.Request('../notes/view_popup.php?type=business&entities_notes_id=' + id, null, {
                'class': 'MooDialog myDialog',
                autoOpen: false,
                size: {
                    width: 450,
                    height: 300
                }
            });

            reqDialog.setRequestOptions({
                onRequest: function () {
                    reqDialog.setContent('loading...');
                }
            }).open();

            e.stop();
            return false;
        });
    });
</script>
<? include VIEWS . '/members/footer.php' ?>
