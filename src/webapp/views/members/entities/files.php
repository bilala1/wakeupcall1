<div>
    <?
    $db = mysqli_db::init();

    $entity_files = $db->fetch_all('
      SELECT entities_files_id, files_name, business_entities_id FROM entities_files ef
      INNER JOIN files ON join_files_id = files_id
      LEFT JOIN business_entities be on be.join_entities_id = ef.join_entities_id
      WHERE ef.join_entities_id = ? ', array($_REQUEST['entities_id']));
    ?>
    <ul>
        <? if (!$entity_files): ?>
            <li><b>No Attachments</b></li>
        <? else: ?>
            <? foreach ($entity_files as $k => $file): ?>
                <li>
                    <a href="/members/entities/<?= $file['business_entities_id'] ? 'business' : 'personnel' ?>/download.php?entities_files_id=<?= $file['entities_files_id'] ?>&type=.<?= files::get_extension($file['files_name']) ?>"
                       target="_blank"
                       title="<?= $file['files_name'] ?>"><?= $file['files_name'] ?></a><br/>
                </li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>
