<div>
    <? $actions = entities_history::get_entities_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['entities_id']); ?>
    <ul>
        <? if ($actions): ?>
            <? foreach ($actions as $action): ?>
                <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['entities_history_datetime'])) ?>)</b>
                    - <?= $action['entities_history_description'] ?></li>
            <? endforeach ?>
        <? else: ?>
            <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
