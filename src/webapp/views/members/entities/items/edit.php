<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = $entityType == 'personnel' ? 'Personnel' : 'Businesses';
$page_keywords = '';
$page_description = '';
function limitWrods($string){
    $wc = str_word_count($string);
    if($wc > 30){
        $array = explode(" ", $string);
        array_splice($array, 30);
        $string = implode(" ", $array)." ...";
    }
    return($string);
}
?>
<script>
    var remindMilestoneMembers = <?=json_encode($users)?>;
</script>
<style>
    .send-rem-check { width: 660px;}
    .send-rem-check .Dynlabel{ width: 50% !important; float: left; font-weight: normal !important;}
    #configureEmails label {
        width: 180px;
    }
    .milestone_check {width: 120Px;font-weight: normal}
</style>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
    <h1 class="left"><?= $_REQUEST['entities_items_id'] ? 'Edit' : 'Add' ?> Tracked Item</h1>

    <div class="page-options">
        <a href="/members/entities/<?= $entityType ?>/edit.php?entities_id=<?= $entityId ?>">Return to <?= $entity['entities_name'] ?></a>

    </div>
    <br clear="all"/>

    <form action="/members/entities/items/edit.php?entities_id=<?=$_REQUEST['entities_id']?>" name="edititems" id="edititems" method="post"
          onSubmit="return chkEntityItemsForm(this)" enctype="multipart/form-data">
        <fieldset class="j-toggle-parent">
            <legend>Tracked Item Details <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
            <?= html::hidden($_REQUEST, 'entities_items_id') ?>

            <?= html::textfield('Name:', $entities_item, 'entities_items_name', array('maxlength' => '255'),
                $errors) ?>
            <?= html::textareafield('Details:', $entities_item, 'entities_items_details', array('rows' => 5),
                $errors) ?>
            <?= html::textfield('Effective Date:', !empty($entities_item['entities_items_effective_date']) ? date('n/j/Y', strtotime($entities_item['entities_items_effective_date'])) : '', 'entities_items_effective_date', array('maxlength' => '255','class' => 'datepicker'),
                $errors) ?>
                </div>
            </fieldset>
  
        <div id="j-additional-milestone">
            <? if(count($entities_items_milestones_existing)<= 0){ ?>
                <fieldset class="j-toggle-parent j-additional-milestone" id="milestoneDetails_0">
                <legend>New Milestone <a class="j-toggle-trigger"></a>
                    <a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">
                    <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" />
		    </a>
                </legend>
                <div class="j-toggle-content">
                    <div class="form-field">
                            <label><span>Milestone Description:</span></label>
                            <input type="text" name="entities_items_milestones_description[0]">
                    </div>
                    <div class="form-field">
                            <label><span>Milestone Date:</span></label>
                            <input type="text" name="entities_items_milestones_date[0]" class="milestone_date">
                    </div>
                    <div class="form-field" id="remind_days_0">
                        <label><span>Remind before:</span></label>
                         <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_0[]' value= 0 ><label class='milestone_check'><span>0 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_0[]' value= 30><label class='milestone_check'><span>30 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_0[]' value= 60><label class='milestone_check'><span>60 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_0[]' value= 90><label class='milestone_check'><span>90 Days</span></label>
                         <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_0[]' value= 120><label class='milestone_check'><span>120 Days</span></label>
                    </div>
                    <div class="form-field">
                            <label>Send Emails To:</label><fieldset class="send-rem-check">
                            <div class="remindMembersContainer" id="remind_milestone_0">
                                <fieldset class="send-rem-check">
                                    <? foreach($users as $key => $value){ ?>
                                        <label class="Dynlabel"><span><input id="send_mails_<?=$value['members_id']?>" class="input-checkbox" type="checkbox" name="entities_items_milestones_remind_members_0[]" value="<?=$value['members_id']?>" ></span>&nbsp;<?=$value['members_lastname'].', '.$value['members_firstname'].' ('.$value['permission'].')'?></label>
                                    <? }?>
                                </fieldset>
                            </div>
                    </div>
                  <label>Additional Email Recipients:</label>
                <a href="#" class="add-item add-milestone-recipient" id="add-milestone-recipient_0">Add a recipient</a><br/>
                <div id="additional-milestone-recipients_0"></div>
                </div>  
                </fieldset>
            <? } else{
                    $i = 0;
                    foreach($entities_items_milestones_existing as $entities_items_milestone){
                        $existing_milestone_remind_days=array();
                        ?>
                        <fieldset class="j-toggle-parent j-additional-milestone" id="milestoneDetails_<?=$i?>">
                            <legend>Milestone <a class="j-toggle-trigger"></a>
                            <a class="j-confirmDelete"  title="Are you sure you want to delete this milestone?" href="#">
                            <img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" />
                            </a>
                            </legend>
                            <div class="j-toggle-content">
                            <div class="form-field">
                                    <label><span>Milestone Description:</span></label>
                                    <input type="hidden" name="entities_items_milestones_id[<?=$i?>]" value="<?=$entities_items_milestone['entities_items_milestones_id']?>">
                                    <input type="text" name="entities_items_milestones_description[<?=$i?>]" value="<?=$entities_items_milestone['entities_items_milestones_description']?>">
                            </div>
                            <div class="form-field">
                                    <label><span>Milestone Date:</span></label>
                                    <input type="text" name="entities_items_milestones_date[<?=$i?>]" value="<?=$entities_items_milestone['entities_items_milestones_date']? date('n/j/Y',strtotime($entities_items_milestone['entities_items_milestones_date'])) :''?>" class="milestone_date">
                            </div>
                            <div class="form-field" id="remind_days_<?=$i?>">
                                <label><span>Remind before:</span></label>
                                <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_<?=$i?>[]' value= 0 <?= in_array(0,$existing_entities_items_milestones_remind_days[$entities_items_milestone['entities_items_milestones_id']])?'checked=checked':''?>><label class='milestone_check'><span>0 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_<?=$i?>[]' value= 30 <?= in_array(30,$existing_entities_items_milestones_remind_days[$entities_items_milestone['entities_items_milestones_id']])?'checked=checked':''?>><label class='milestone_check'><span>30 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_<?=$i?>[]' value= 60 <?= in_array(60,$existing_entities_items_milestones_remind_days[$entities_items_milestone['entities_items_milestones_id']])?'checked=checked':''?>><label class='milestone_check'><span>60 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_<?=$i?>[]' value= 90 <?= in_array(90,$existing_entities_items_milestones_remind_days[$entities_items_milestone['entities_items_milestones_id']])?'checked=checked':''?>><label class='milestone_check'><span>90 Days</span></label>
                                 <input type='checkbox' class='input-checkbox'  name='entities_items_milestones_remind_days_<?=$i?>[]' value= 120 <?= in_array(120,$existing_entities_items_milestones_remind_days[$entities_items_milestone['entities_items_milestones_id']])?'checked=checked':''?>><label class='milestone_check'><span>120 Days</span></label>
                            </div>
                            <div class="form-field">
                                    <label>Send Emails To:</label><fieldset class="send-rem-check">
                                    <div class="remindMembersContainer" id="remind_milestone_<?=$i?>" class="remind_milestone">
                                    <fieldset class="send-rem-check">
                                        <? 
                                        foreach($users as $key => $value){ 
                                            $checked = '';
                                            if(in_array($value['members_id'], $entities_items_milestones_remind_members[$entities_items_milestone['entities_items_milestones_id']])){
                                                $checked = "checked=checked";
                                            }
                                            ?>
                                            <label class="Dynlabel"><span><input id="send_mails_<?=$value['members_id']?>" class="input-checkbox" type="checkbox" name="entities_items_milestones_remind_members_<?=$i?>[]" value="<?=$value['members_id']?>" <?=$checked?> ></span>&nbsp;<?=$value['members_lastname'].', '.$value['members_firstname'].' ('.$value['permission'].')'?></label>
                                        <? }?>
                                    </fieldset>
                                    </div>
                            </div>
                            <label>Additional Email Recipients:</label>
                            <a href="#" class="add-item add-milestone-recipient" id="add-milestone-recipient_<?=$i?>">Add a recipient</a><br/>
                            <? 
                            if(count($existing_entities_items_milestones_additional_recipients[$entities_items_milestone['entities_items_milestones_id']]) > 0){
                                foreach($existing_entities_items_milestones_additional_recipients[$entities_items_milestone['entities_items_milestones_id']] as $milestones_additional_recipient){?>
                            <div class="form-field">
                                <label><span>Email Address:</span></label> 
                                <input name="entities_items_milestones_additional_recipients_email_<?=$i?>[]" value="<?=$milestones_additional_recipient['entities_items_milestones_additional_recipients_email']?>" maxlength="200" class="input-text"> 
                                <a href="#" class="delete-recipient">Delete</a><br>
                            </div>
                               <? 
                                }
                            }
                            ?>
                            <div id="additional-milestone-recipients_<?=$i?>"></div>
                            </div>  
                        </fieldset>
                    <?$i++; 
                    }
                }?>
            </div> 
            <a href="javascript:void(0);" id="add_milestone" class="add-item add-file-link">Add Another Milestone</a><br/>

        <fieldset class="j-toggle-parent">
            <legend>Files <a class="j-toggle-trigger"></a></legend>
            <div class="file-container j-toggle-content">
                <table id="uploaded-files" class="file-table">
                    <tr>
                        <th>File Name</th>
                        <th>Date Uploaded</th>
                        <th>Actions</th>
                    </tr>
                    <? if ($entity_items_files) {
                        foreach ($entity_items_files as $file): ?>
                            <? $filename = $file['files_name'];
                            if (strlen($filename) > 50) {
                                $filename = substr($filename, 0, 40) . '...' . substr($filename, -7);
                            }
                            ?>
                            <tr>
                                <td>
                                    <a href="/members/entities/items/download.php?entities_items_id=<?= $_REQUEST['entities_items_id'] ?>&entities_items_files_id=<?= $file['entities_items_files_id'] ?>"
                                       title="<?= $file['files_name'] ?>" target="_blank"><?= $filename ?></a>
                                </td>
                                <td>
                                    <? if ($file['files_datetime']) { ?>
                                        <?= date('m/d/Y', strtotime($file['files_datetime'])) ?>
                                    <? } else { ?>
                                        Unknown
                                    <? } ?>
                                </td>
                                <td>
                                    <a href="/members/entities/items/del-file.php?entity_items_id=<?= $_REQUEST['entities_items_id'] ?>&entities_items_files_id=<?= $file['entities_items_files_id'] ?>">Delete</a>
                                </td>
                            </tr>
                        <? endforeach;
                    } else { ?>
                        <tr>
                            <td><input type="file" name="entities_items_files[]"></td>
                            <td></td>
                            <td><a href="#" onclick="deleteRow(event)">Delete</a></td>
                        </tr>
                    <? } ?>
                </table>
                <a href="#" id="add_file" class="add-item add-file-link">Add a file</a><br/>

                <? if ($errors['entities_items_files']): ?>
                    <p class="errors"><?= $errors['entities_items_files'] ?></p>
                <? endif ?>
            </div>
        </fieldset>

        <fieldset class="j-toggle-parent">
            <legend>Notes <a class="j-toggle-trigger"></a></legend>
            <div class="file-container j-toggle-content">
                    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear" id="notebox_container">
                        <tr>
                            <th style="width:200px">Subject</th>
                            <th style="width:470px">Note</th>
                            <th style="width:180px">Last Updated</th>
                            <th style="width:85px">Actions</th>
                        </tr>
                        <?
                        if (!$entities_items_notes){?>
                        <tr class="notebox">
                            <td><input type="text" id="entities_items_notes_subject_0" class="input-text show_on_edit" name="entities_items_notes_subject[0]" >
                                <input type="hidden" id="entities_items_hidden_subject_0">
                                <span id="entities_items_notes_subject_text_0" class="hide_on_edit"></span></td>
                            <td><textarea id="entities_items_notes_note_0" name="entities_items_notes_note[0]" class="show_on_edit" ></textarea>
                                <input type="hidden" id="entities_items_hidden_note_0">
                                <span id="entities_items_notes_note_text_0" class="hide_on_edit"></span>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <div class="actionMenu">
                                <ul>
                                    <li><a href="#"  id="donenotes_0_new" class="show_on_edit donenotes">Done</a></li>
                                    <li><a href="#"  id="cancelnotes_0_new" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>
                                    <li><a href="#"  id="editnotes_0" class="hide_on_edit edit_notes" style="display:none;">Edit</a></li>
                                    <li><a href="#"  id="deletenotes_0" class="show_on_edit delete" >Cancel</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>  
                       <? }
                        $i = 0; 
                        foreach ((array)$entities_items_notes as $entity_note): ?>
                            <tr class="notebox">
                                <td><input type="hidden" name="entities_items_notes_id[]" value="<?= $entity_note['entities_items_notes_id'] ?>" >
                                    <input type="hidden" id="entities_items_hidden_subject_<?=$i?>" value="<?= $entity_note['entities_items_notes_subject'] ?>">
                                    <input type="text" id="entities_items_notes_subject_<?=$i?>" name="entities_items_notes_subject[<?=$i?>]" value="<?= $entity_note['entities_items_notes_subject'] ?>" style='display:none;' class='show_on_edit'>
                                    <span class="hide_on_edit" id="entities_items_notes_subject_text_<?=$i?>"><?= $entity_note['entities_items_notes_subject'] ?> </span>
                                
                                </td>
                                <td><span class="hide_on_edit" id="entities_items_notes_note_text_<?=$i?>"><?= limitWrods($entity_note['entities_items_notes_note']) ?></span>
                                    <input type="hidden" id="entities_items_hidden_note_<?=$i?>" value="<?= $entity_note['entities_items_notes_note'] ?>">
                                    <textarea id="entities_items_notes_note_<?=$i?>" name="entities_items_notes_note[<?=$i?>]" style='display:none;' class='show_on_edit'><?= $entity_note['entities_items_notes_note'] ?></textarea></td>
                                <td><?= empty($entity_note['entities_items_notes_datetime'])?'':date('F j, Y, g:i a', strtotime($entity_note['entities_items_notes_datetime'])) ?></td>
                                <td>
                                    <div class="actionMenu">
                                        <ul>
                                            <li><a href="#" class="notes hide_on_edit" id="notes_<?= $entity_note['entities_items_notes_id'] ?>">View full note</a></li>
                                            <li><a href="#" class="edit_notes hide_on_edit" id="edit_notes">Edit</a></li>
                                            <li><a href="#" onclick="delete_notes(this);">Delete</a></li>
                                            <li><a href="#"  id="donenotes_<?=$i?>" class="show_on_edit donenotes" style="display:none;">Done</a></li>
                                            <li><a href="#"  id="cancelnotes_<?=$i?>" class="show_on_edit cancelnotes" style="display:none;">Cancel</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <? 
                        $i++;
                        endforeach; ?>
                    </table>
                <a href="#" id="another_note" class="add-item">Add another note</a>
            </div>
        </fieldset>

        <div class="form-actions">
          <?= html::input(($entities_item ? 'Save' : 'Add') . ' Tracked Item', 'save',
                array('class' => 'btn-primary btn-ok', 'type' => 'button')) ?>
        </div>

    </form>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/entities_items.js"></script>

<? include VIEWS . '/members/footer.php' ?>
