<div>
    <?
    $db = mysqli_db::init();

    $entity_files = $db->fetch_all('
        SELECT *
        FROM entities_items_files ef
        INNER JOIN files ON join_files_id = files_id
        INNER JOIN entities_items ON entities_items_id = ef.join_entities_items_id
        INNER JOIN entities ON entities_id = join_entities_id
        WHERE
            entities_items_id = ?',
        array($_REQUEST['entities_items_id']
        ));
    ?>
    <ul>
        <? if (!$entity_files): ?>
            <li><b>No Attachments</b></li>
        <? else: ?>
            <? foreach ($entity_files as $k => $file): ?>
                <li>
                    <a href="/members/entities/items/download.php?entities_items_files_id=<?= $file['entities_items_files_id'] ?>"
                       target="_blank"
                       title="<?= $file['files_name'] ?>"><?= $file['files_name'] ?></a><br/>
                </li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>
