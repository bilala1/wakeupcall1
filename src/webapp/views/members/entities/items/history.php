<div>
    <? $actions = entities_items_history::get_entities_items_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['entities_items_id']); ?>
    <ul>
        <? if ($actions): ?>
            <? foreach ($actions as $action): ?>
                <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['entities_items_history_datetime'])) ?>)</b>
                    - <?= $action['entities_items_history_description'] ?></li>
            <? endforeach ?>
        <? else: ?>
            <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
