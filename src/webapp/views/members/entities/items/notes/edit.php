<html>
<head>
    <? include VIEWS . '/members/scripts.php' ?>
</head>
<body class="dialog">
<h1 class="left"><?= $_REQUEST['entities_items_notes_id'] ? 'Edit' : 'Add' ?> Note</h1>

<div class="page-options">
    <a href="notes_popup.php?entities_items_id=<?= $_REQUEST['entities_items_id'] ?>">Back to Notes</a>
</div>
<br class="clear">

<form action="/members/entities/items/notes/edit.php" name="editNote" id="editNote" class="full-width" method="post">
    <? if ($_REQUEST['entities_items_notes_id']) : ?>
        <?= html::hidden($_REQUEST, 'entities_items_notes_id') ?>
    <? endif; ?>
    <?= html::hidden($_REQUEST, 'entities_id') ?>
    <?= html::hidden($_REQUEST, 'entities_items_id') ?>

    <?= html::textfield('Note Subject:', $entities_items_note['entities_items_notes_subject'],
        'entities_items_notes_subject', null, $errors) ?>

    <?= html::textareafield('Note:', $entities_items_note['entities_items_notes_note'], 'entities_items_notes_note',
        array('rows' => 5, 'cols' => 50)) ?>

    <div class="form-actions">
        <?= html::submit('Save', array('class' => 'btn-ok btn-primary')) ?>
    </div>
</form>
</body>
</html>