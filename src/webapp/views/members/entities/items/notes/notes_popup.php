<html>
<head>
    <? include VIEWS . '/members/scripts.php' ?>
</head>
<body class="dialog">
<? if ($entity_notes): ?>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Subject</th>
            <th>Date</th>
            <th style="width:150px">Actions</th>
        </tr>
        <? foreach ((array)$entity_notes as $entity_note): ?>
            <tr>
                <td><?= $entity_note['entities_items_notes_subject'] ?></td>
                <td><?= date('F j, Y, g:i a', strtotime($entity_note['entities_items_notes_datetime'])) ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li>
                                <a href="view.php?entities_items_id=<?= $_REQUEST['entities_items_id'] ?>&entities_items_notes_id=<?= $entity_note['entities_items_notes_id'] ?>">View full note</a>
                            </li>
                            <li>
                                <a href="edit.php?entities_items_id=<?= $_REQUEST['entities_items_id'] ?>&entities_items_notes_id=<?= $entity_note['entities_items_notes_id'] ?>">Edit</a>
                            </li>
                            <li>
                                <a class="deletecheck"
                                   href="delete.php?entities_items_notes_id=<?= $entity_note['entities_items_notes_id'] ?>&redirect=popup">Delete</a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
<? else: ?>
    <p>
        <b>No Existing Notes</b>
    </p>
<? endif; ?>
<a href="edit.php?entities_items_id=<?= $_REQUEST['entities_items_id'] ?>" class="add-item">Add Note</a>
</body>
</html>