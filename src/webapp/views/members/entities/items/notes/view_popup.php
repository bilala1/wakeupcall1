<div id="content">
    <? include(ACTIONS . '/members/entities/items/notes/view.php'); ?>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th><?= $entities_items_note['entities_items_notes_subject']?></th>
            <th width='200px'><?= date('F j, Y, g:i a', strtotime($entities_items_note['entities_items_notes_datetime'])) ?></th>
        </tr>
        <tr>
            <td colspan="2"><?= $entities_items_note['entities_items_notes_note']?></td>
        </tr>
    </table>
</div>