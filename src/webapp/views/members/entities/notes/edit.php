<?
$page_title        = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Entities';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <h1 class="left"><?= $_REQUEST['entities_notes_id'] ? 'Edit' : 'Add' ?> Note</h1>
    <div class="page-options">
    <?
        if('entities' == $nav_id)
           echo '<a href="/members/entities/index.php">Return To Entities</a>';
        else if($entities_id)
            echo '<a href="/members/entities/edit.php?entities_id=' . $entities_id .'">Return To Entity</a>';

    ?>
    </div>

    <br class="clear" />
    <form action="/members/entities/notes/edit.php" name="editNote" id="editNote" method="post" onSubmit="return chkEntityForm(this)" enctype="multipart/form-data">
        <?if ($_REQUEST['entities_notes_id']) :?>
            <?= html::hidden($_REQUEST, 'entities_notes_id') ?>
        <? endif; ?>
        <?= html::hidden($_REQUEST, 'entities_id') ?>
        <?= html::hidden($_REQUEST, 'nav_id') ?>
        <?= html::hidden($_REQUEST, 'type') ?>

        <?= html::textfield('Note Subject:', $entities_note['entities_notes_subject'], 'entities_notes_subject', null, $errors) ?>

        <?= html::textareafield('Note:', $entities_note['entities_notes_note'], 'entities_notes_note', array('rows'=>5, 'cols'=>50)) ?>

        <br clear="all" />

        <?= html::submit('Save', array('class' => 'btn')) ?>
    </form>
</div>
<? include VIEWS. '/members/footer.php' ?>
