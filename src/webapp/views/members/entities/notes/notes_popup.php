<div id="content">
    <? include(ACTIONS . '/members/entities/notes/index.php'); ?>
    <? if($entity_notes): ?>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>Note Description</th>
            <th>Date</th>
            <th style="width:55px">Actions</th>
        </tr>
        <? foreach((array)$entity_notes as $entity_note): ?>
            <tr>
                <td><?= $entity_note['entities_notes_subject']?></td>
                <td><?= date('F j, Y, g:i a', strtotime($entity_note['entities_notes_datetime'])) ?></td>
                <td>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="#" class="viewNote" id="notes_<?= $entity_note['entities_notes_id'] ?>">View</a></li>
                            <li><a href="../notes/edit.php?type=<?= $_REQUEST['type'] ?>&entities_notes_id=<?= $entity_note['entities_notes_id'] ?>">Edit</a></li>
                            <li><a class="deletecheck" href="../notes/delete.php?type=<?= $_REQUEST['type'] ?>&entities_notes_id=<?= $entity_note['entities_notes_id'] ?>">Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="99"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
        <a href="/members/entities/notes/edit.php?type=<?= $_REQUEST['type'] ?>&entities_id=<?= $_REQUEST['entities_id'] ?>&nav_id=entities">Add Note</a>
    <? else: ?>
       <b>No Existing Notes: </b><a href="/members/entities/notes/edit.php?type=<?= $_REQUEST['type'] ?>&entities_id=<?= $_REQUEST['entities_id'] ?>&nav_id=entities">Add Note</a>
    <? endif; ?>
</div>
