<?
$page_title        = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Entities';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
        <h1 class="left">Note</h1>
        <br clear="all" />
        <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
            <tr>
                <th><?= $entities_note['entities_notes_subject']?></th>
                <th width='200px'><?= date('F j, Y, g:i a', strtotime($entities_note['entities_notes_datetime'])) ?></th>
            </tr>
            <tr>
                <td colspan="2"><?= $entities_note['entities_notes_note']?></td>
            </tr>
        </table>

        <br />
    </div>
<? include VIEWS. '/members/footer.php' ?>