<div class="rightcol-box">
    <? #pre($_SESSION); ?>
    <h3 class="red">Recent <?= $forums_id != 1 ? 'Bulletin' : 'Forum' ?> Posts</h3>
    <ul>
    <? foreach($rightcol_forums::get_recent_posts(4, $forums_id) as $post): ?>
        <li>
            <a href="/members/forums/view-topic.php?topics_id=<?= $post['topics_id'] ?>"><strong><?= $post['topics_title'] ?></strong></a>
            <br />By <?= $post['members_username'] ?>
            <br />
            <?= times::tz_convert($post['posts_postdate'], DATE_FORMAT_FULL); ?>
        </li>
   <? endforeach; ?>
   </ul>
</div>
