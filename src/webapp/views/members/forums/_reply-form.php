<div class="clear"></div>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
<? /* // post tags considered useless. thus removed

window.addEvent('load', function () {

    var taglist = new TextboxList('posts_tags', {
        unique: true,
        plugins: {
            autocomplete: {
                minLength: 3,
                queryRemote: true,
                remote: {
                    url: BASEURL + '/ajax-tags.php'
                }
            }
        }
    });
});
*/ ?>
</script>
<h2>New Reply</h2>

<form id="reply-form" action="/members/forums/new-reply.php" method="post">

	<strong>Post Body</strong><br />
	<textarea id="posts_body" name="posts_body" style="width: 100%; resize: none; height: 200px;"><?= html::filter($_POST['posts_body'] ? $_POST['posts_body'] : $quoted); ?><?//= "\n\n\n"?></textarea><br />
	<br />
<? /*
	<strong>Post Tags</strong> Type your keyword, and press "enter" to add it.<br />
	<input type="text" id="posts_tags" class="topics_title" name="posts_tags" value="<?= html::filter($_POST['posts_tags']); ?>" /><br />
	<br />
*/ ?>
	<div class="posts_enabled" style="float: left;">
		<h2>Post Settings</h2>
		<table>
			<tbody>
				<? /*<tr>
					<td><input checked="checked" type="checkbox" name="enable_bbcode" id="enable_bbcode" value="1"></td>
					<td><label style="width: 200px;" for="enable_bbcode">Enable BBCode</label></td>
				</tr> */ ?>
				<tr>
					<td><input checked="checked" type="checkbox" name="enable_signature" id="enable_signature" value="1"></td>
					<td><label style="width: 200px;" for="enable_signature">Enable My Forum Signature</label></td>
				</tr>
				<tr>
					<td><input checked="checked" type="checkbox" name="add_watchdog" id="add_watchdog" value="1"></td>
					<td><label style="width: 200px;" for="add_watchdog">Notify me of replies</label></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="left" style="width: 400px; margin-left: 20px;" id="file_uploads">
		<!--
		<h3>Upload files</h3>
		<br />
		<a href="#" onClick="addFile(); return false;">Add File</a>
		-->
	</div>

	<br clear="left" />
	<br />

	<div class="form-actions">
		<input type="hidden" name="topics_id" value="<?= html::filter($_REQUEST['topics_id']); ?>" />
		<input type="submit" value="Reply" class="btn-ok btn-primary" />
	</div>
</form>

<div class="clear"></div>

<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#posts_body",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
	    target_list: false,
		default_link_target: "_blank",
		link_title: false,
        plugins: "bbcode link image textcolor",
    });
});
</script>