<div class="topicpost">
    <div class="<?= $post['posts_id'] == $topic['first_post'] ? 'topicinfo' : 'postinfo' ?>" align="right">Posted: <?= times::tz_convert($post['posts_postdate'], DATE_FORMAT_FULL) ?></div>

    <div class="authorinfo">
        <ul>
            <li><?= $post['members_username'] ?></li>
            <? if($post['avatar_small']): ?>
            <li><?= $post['avatar_small'] ?></li>
            <? endif ?>
            <? if($post['countries_name']): ?>
            <li><?= $post['countries_name'] ?></li>
            <? endif ?>
            <? if($post['join_members_id'] != FORUM_FORMER_MEMBER): ?>
                <li><strong class="blue">Posts:</strong> <?= $post['forum_posts'] ?></li>
            <? endif; ?>  
            <?/*<li><strong class="blue">Points:</strong> <?= $post['points'] ?></li>
            <li><?= $post['points']['bracket'] ?></li>*/?>
        </ul>
    </div>

    <div class="postbody">
        <? if(!$_REQUEST['topics_id']): ?>
        <h2>Topic: <a href="/members/forums/view-topic.php?topics_id=<?= $post['topics_id'] ?>"><?= $post['topics_title'] ?></a></h2>
        <? endif ?>
        <p class="postbodytext"><?= $post['enabled']['bbcode'] ? html::bbcode_format(html::bbcode_prettify($post['posts_body'])) : nl2br($post['posts_body']); ?></p>
        <? if($post['posts_modified_reason']): ?>
            <div class="postedit">
                <strong>Edit By:</strong><?//= $post['editor'] ?> Forum Monitor
                <? if($post['posts_modified_reason']): ?>
                - <strong>Reason:</strong> <?= $post['posts_modified_reason'] ?>
                <? endif; ?>
            </div>
        <? endif; ?>
        <? if(sizeof($post['tags'])): ?>
            <div class="posttags">
                <strong class="blue">Tags:</strong>
                <? foreach($post['tags'] as $index => $tag): ?>
                <? extract($tag) ?>
                <a href="/members/forums/tag/<?= http::encode_url($global_tags_name) . '/' . $global_tags_id . '/' . forums::get_forum_id_by_topic($post['join_topics_id']) ?>"><?= html::filter($global_tags_name) ?></a>
                <?= count($post['tags'])-1 > $index ? ',' : '' ?>
                <? endforeach; ?>
            </div>
        <? endif; ?>
        <? if(count($post['files'])): ?>
            <div class="postfiles">
                <strong class="blue">Files:</strong>
                <? foreach($post['files'] as $index => $file): ?>
                <a target="_blank" href="/members/forums/download.php?forums_posts_files_id=<?= $file['forums_posts_files_id'] ?>#<?= urlencode($file['forums_posts_files_filename']) ?>" title="<?= $file['forums_posts_files_original_filename'] ?>"><?= html::xss($file['forums_posts_files_original_filename']) ?></a>
                <?= count($post['files'])-1 > $index ? ',' : '' ?>
                <? endforeach; ?>
            </div>
        <? endif; ?>
        <? if($post['enabled']['signature'] && $post['members_forum_signature']): ?>
            <div class="postsig">
            <?= html::bbcode_format(html::bbcode_prettify($post['members_forum_signature'])); ?>
            </div>
        <? endif; ?>
    </div>
    <br clear="all" />
    <? if(ActiveMemberInfo::GetMemberId()): ?>
    <div class="postactions right">
        <? if(ActiveMemberInfo::GetMemberId() == $post['join_members_id'] && $post['join_topics_id'] == $_REQUEST['topics_id']): ?>
            <? if($post['posts_id'] == $topic_post['posts_id']): ?>
            <a href="/members/forums/new-topic.php?topics_id=<?= $_REQUEST['topics_id'] ?>">Edit</a>
            <? else: ?>
            <a href="/members/forums/edit-post.php?topics_id=<?= $_REQUEST['topics_id'] ?>&posts_id=<?= $post['posts_id'] ?>">Edit</a>
            <? endif ?>
        <? endif ?>
        <? if($forum_permissions['reply']): ?>
        <span class="multiquote <?= $_SESSION['multiquote'][$_REQUEST['topics_id']][$post['posts_id']] ? 'multiquote_active' : null ?>" posts_id="<?= $post['posts_id'] ?>" topics_id="<?= $_REQUEST['topics_id'] ?>">Quote</span>
        <? endif ?>
        <a href="/members/forums/report.php?posts_id=<?= $post['posts_id'] ?>" class="flag">Flag</a>
        <br />
    </div>
    <? endif; ?>
    <br clear="right">
</div>