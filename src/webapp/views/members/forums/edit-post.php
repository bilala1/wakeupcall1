<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
	$forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
	$forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<div id="forum-container">
	<div id="content" class="forum-bkg">
			<div id="breadcrumb" class="left">
				<?= $breadcrumb; ?>
			</div>
			<br clear="left"/>
			<br/>

			<form action="" method="post">
				<strong>Post Body</strong><br/>
                
				<textarea id="posts_body" name="posts_body" style="width: 100%; resize: none; height: 200px;"><?= $post['posts_body'] ?></textarea>
				<br />
				<br />

				<strong>Reason for Editing</strong><br />
				<input type="text" name="posts_modified_reason" value="<?= $post['posts_modified_reason'] ?>" style="width: 100%;"/>

				<br /><br />

				<div class="posts_enabled" style="float: left;">

					<input type="hidden" name="enable_bbcode" value="1" />

					<h2>Post Settings</h2>
					<table>
						<tbody>
						<tr>
							<td>
								<input <?= $post['posts_enabled']['signature'] == 1 ? 'checked="checked"' : '' ?> type="checkbox" name="enable_signature" id="enable_signature" value="1">
							</td>
							<td><label style="width: 200px;" for="enable_signature">Enable My Forum Signature</label>
							</td>
						</tr>
						</tbody>
					</table>
				</div>

				<br clear="left"/>
				<br/>

				<div class="form-actions">
					<input type="hidden" name="topics_id" value="<?= html::filter($_REQUEST['topics_id']); ?>"/>
					<input type="submit" value="Edit Post" class="btn-ok btn-primary" />
				</div>
			</form>
	</div>
</div>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#posts_body",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
		target_list: false,
		default_link_target: "_blank",
		link_title: false,
        plugins: "bbcode link image textcolor",
    });
});
</script>

<? include VIEWS . '/members/footer.php'; ?>
