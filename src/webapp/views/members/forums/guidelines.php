<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<script type="text/javascript">
    
window.addEvent('load', function(){
    var accordion = new Fx.Accordion('div.forumcat h2', 'div.forumcat div.forumcat_forums', {
        show: 0
    }); 
});
</script>
<div id="content">
    	<h1>Forum Guidlines</h1>
		<ul class="left" style="padding-left:15px;" >
            <li>Please be sure posts are category approprate.</li>
            <li>No off-topic or off-color postings.</li>
            <li>Postings may be deleted at the discretion of our Moderators.</li>
            <li>No advertising is allowed.</li>
            <li>Be Courtious. No name calling, personal attacks or flaming.</li>
            <li>Certain words will trigger moderation of the post. These words mostly cover political and religious topics.</li>	        
        </ul>
          
        <br class="clear"/>
</div>
<?php include VIEWS .'/members/footer.php'; ?>
