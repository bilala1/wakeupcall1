<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<script type="text/javascript">
window.addEvent('load', function(){
    var accordion = new Fx.Accordion('div.forumcat h2', 'div.forumcat div.forumcat_forums', {
        show: 0
    }); 
});
</script>
<div id="content">
        <h1>Select a Forum</h1>
        <ul id="forums">
        <? foreach($forums as $forum): ?>
            <li class="forum">
                <h2><?= $forum['forums_name'] ?></h2>
                <ul class="forum-topics">
                    <? foreach($forum['topics'] as $topic): ?>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endforeach; ?>
        </ul>
        <a href="/contact.php" class="link right" style="margin-right:15px;">Suggest a New Forum &raquo;</a><br class="clear"/><br />
		
		<? if(!ActiveMemberInfo::GetMemberId()): ?>
     	<div  id="join" class="left" style="width:300px">
       		<strong class="red">Join Here</strong><br />
         	<a href="/register.php" class="small">Member Benefits</a>
 		</div>
 		<? endif ?>
        
</div>

<? include VIEWS.'/members/footer.php'; ?>
