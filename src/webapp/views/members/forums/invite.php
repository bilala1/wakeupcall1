<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<script type="text/javascript">

window.addEvent('load', function(){

    $('addinput').addEvent('click', function(){
        var newinput = $$('.friend_input')[0].clone();
        newinput.inject($('inputs'));
        
        newinput.getElement('.remove').addEvent('click', function(){
            var tparent = this.getParent('div');
            var total = $$('.friend_input').length;
            
            if(total > 1){
                tparent.destroy();
            }
        });
        
        return false;
    });
    
    $$('.remove').addEvent('click', function(){
        var tparent = this.getParent('.friend_input');
        var total = $$('.friend_input').length;
        
        if(total > 1){
            tparent.destroy();
        }
    });

});

</script>

<div id="forum-container">
    
<div id="content">
        <form action="/members/forums/invite.php" method="post" class="invite-form">
       		<h1 class="blue">Invite Friends</h1>	
            <a href="#" id="addinput" class="right non-underline"><img src="/images/icon-plus.png" alt="add field"/>Add Field</a><br />
            <br />
            <div id="inputs">
                <div class="friend_input">
                    <label class="blue">Email</label><input type="text" name="emails[]"  class="text"/> 
                    <a href="#" class="remove red non-underline"><img src="/images/icon-remove.png" alt="remove"/></a>
                </div>
            </div>
            <br />
            <label class="blue">Message: <br/> (Optional)</label>
            <textarea rows="5" name="message" style="vertical-align:top; resize: none"></textarea><br /><br/>
            <input type="submit" value="Invite"  class="blue-btn right"/><br class="clear"/>
        </form>

    </div>
</div> <!--  Fourm-Container-->
        
<?php include VIEWS .'/members/footer.php'; ?>
