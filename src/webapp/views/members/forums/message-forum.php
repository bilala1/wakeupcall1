<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<div id="content">
        <h1>Select a Forum</h1>
        <ul id="forums">
        <? foreach($forums as $forum): ?>
            <li class="forum">
                <h2><a href="/members/forums/view-forum.php?forums_id=<?= $forum['forums_id'] ?>"><?= $forum['forums_name'] ?></a></h2>
                <ul class="forum-topics">
                    <? foreach($forum['topics'] as $topic): ?>
                    <li><a href="/members/forums/view-topic.php?topics_id=<?= $topic['topics_id'] ?>"><?= $topic['topics_title'] ?></a> (<?= $topic['topics_replies'] ?>)</li>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endforeach; ?>
        </ul>
        <a href="/members/contact.php" class="link right" style="margin-right:15px;">Suggest a New Forum &raquo;</a><br class="clear"/><br />
		
		<? if(!ActiveMemberInfo::GetMemberId()): ?>
     	<div  id="join" class="left" style="width:300px">
       		<strong class="red">Join Here</strong><br />
         	<a href="/register.php" class="small">Member Benefits</a>
 		</div>
 		<? endif ?>
        
	</div>

<? include VIEWS.'/members/footer.php'; ?>
