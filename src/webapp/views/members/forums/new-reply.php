<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>
<script type="text/javascript">
/*
function addFile(){
    var upload_name = Math.ceil((Math.random() * 99999)) + '_file_upload';

    var frameupload = new Element('iframe', {
        'name': upload_name,
        'id': upload_name,
        'styles': {
            'display': 'none',
            'visibility': 'none'
        }
    }).inject($(document.body));

    var form = new Element('form', {
        'method': 'post',
        'action': BASEURL + '/members/forums/ajax-upload.php',
        'enctype': 'multipart/form-data'
    }).inject($('file_uploads'));

    var file = new Element('input', {
        'type': 'file',
        'name': 'filename'
    }).inject(form);

    file.addEvent('change', function(){
        this.getParent('form').set('target', upload_name);

        frameupload.addEvent('load', function(){
            var json = JSON.decode(frameupload.contentDocument.body.innerHTML);

            if(json.uploaded){
                new Element('input', {
                    'type': 'hidden',
                    'name': 'files[]',
                    'value': json.real_filename+'~~'+json.filename
                }).inject($('reply-form'));

                new Element('div', {
                    'text': json.real_filename
                }).inject(file, 'after');

                file.dispose();
            }
            else {
                file.dispose();
                alert(json.error);
            }
        });

        this.getParent('form').submit();
    });
}
*/
</script>
<div id="forum-container">

<div id="content" class="forum-bkg">
	    <div id="breadcrumb" class="left">
	    	<?= $breadcrumb; ?>
    	</div>
    	<br clear="left" />
        <br />

		<? include(VIEWS . '/members/forums/_reply-form.php'); ?>

    </div>
</div>

<? include VIEWS .'/members/footer.php'; ?>
