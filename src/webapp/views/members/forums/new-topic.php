<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<link href="/css/TextboxList.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/js/GrowingInput.js"></script>
<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script> 
<script type="text/javascript"> 
window.addEvent('load', function () {
    tinyMCE.init({
        selector:"#posts_body",
        menubar: "",
        toolbar: "bold italic underline | undo redo | link unlink image | forecolor | styleselect | removeformat",
        toolbar_items_size: 'small',
        bbcode_dialect: "punbb",
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        plugins: "bbcode link image textcolor",
    });

    /*
    var taglist = new TextboxList('posts_tags', {
        unique: true,
        plugins: {
            autocomplete: {
                minLength: 3,
                queryRemote: true,
                remote: {
                    url: BASEURL + '/ajax-tags.php'
                }
            }
        }
    }); */

});

function addFile(){
    var upload_name = Math.ceil((Math.random() * 99999)) + '_file_upload';

    var frameupload = new Element('iframe', {
        'name': upload_name,
        'id': upload_name,
        'styles': {
            'display': 'none',
            'visibility': 'none'
        }
    }).inject($(document.body));

    var form = new Element('form', {
        'method': 'post',
        'action': BASEURL + '/members/forums/ajax-upload.php',
        'enctype': 'multipart/form-data'
    }).inject($('file_uploads'));

    var file = new Element('input', {
        'type': 'file',
        'name': 'filename'
    }).inject(form);

    file.addEvent('change', function(){
        this.getParent('form').set('target', upload_name);

        frameupload.addEvent('load', function(){
            var json = JSON.decode(frameupload.contentDocument.body.innerHTML);
            if(json.uploaded){
                new Element('input', {
                    'type': 'hidden',
                    'name': 'files[]',
                    'value': json.real_filename+'~~'+json.filename
                }).inject($('reply-form'));

                new Element('div', {
                    'text': json.real_filename
                }).inject(file, 'after');

                file.dispose();
            }
            else {
                file.dispose();
                alert(json.error);
            }
        });

        this.getParent('form').submit();
    });
}
</script>

<div id="forum-container">

<div id="content" class="forum-bkg">
	    <div id="breadcrumb" class="left">
	    	<?= $breadcrumb; ?> &raquo; <?= $_REQUEST['topics_id'] ? 'Edit' : 'New' ?> Topic
    	</div>
    	<br clear="left" />

    	<h2><?= $_REQUEST['topics_id'] ? 'Edit' : 'New' ?> Topic</h2>
    	<br />
    	<? if($errors): ?>
		<p class="errors"><?= implode('<br />', $errors); ?>
		<? endif; ?>

    	<form id="reply-form" action="/members/forums/new-topic.php" onsubmit="form_submitted()" method="post">
            <?= html::hidden($topic, 'topics_id') ?>
    		<strong>Topic Title</strong><br />
            <?= html::input($topic, 'topics_title', array('maxlength' => 255, 'class' => 'topics_title')) ?>
    		<br />
    		<br />
    		<strong>Topic Body</strong><br />
            <?= html::textarea($topic, 'posts_body', array('style' => 'width:100%; resize:none; height:200px')) ?>
    		<br />
			<? /*
			<strong>Post Tags</strong> Type your keyword, and press "enter" to add it.<br />
            <?= html::input($topic, 'posts_tags', array('class' => 'topics_title')) ?>
    		<br />
            */ ?>
    		<div class="posts_enabled" style="float:left">
                <h2>Post Settings</h2>
                <table>
                    <tbody>
                        <? /*
                        <tr>
                            <td><?= html::checkbox(1, 'enable_bbcode', $_REQUEST['topics_id'] ? $topic['settings']['bbcode'] : 1) ?></td>
                            <td><label for="enable_bbcode">Enable BBCode</label></td>
                        </tr>
                        <tr>
                            <td><?= html::checkbox(1, 'enable_signature', $_REQUEST['topics_id'] ? $topic['settings']['signature'] : 1) ?></td>
                            <td><label for="enable_signature">Enable My Forum Signature</label></td>
                        </tr>
                        */ ?>
                        <? if(!$_REQUEST['topics_id']): ?>
                        <tr>
                            <td><?= html::checkbox(1, 'add_watchdog', $_REQUEST['topics_id'] ? $topic['settings']['bbcode'] : 1) ?></td>
                            <td><label for="add_watchdog">Notify me of replies</label></td>
                        </tr>
                        <? endif ?>
                        <? if(ActiveMemberInfo::_IsAccountAdmin()): ?>
                        <!--<tr>
                            <td><?= html::checkbox(1, 'sticky_topic', $topic['topics_stickied']) ?></td>
                            <td><label for="sticky_topic">Sticky this topic to the top</label></td>
                        </tr>-->
                        <? endif ?>
                    </tbody>
                </table>
    		</div>
			<? if (!forums::is_in_message_forum($_REQUEST['forums_id'])): ?>
    		<div class="left" style="width: 400px; margin-left: 20px; margin-top:20px" id="file_uploads">
    		    <h3>Add Attachment to Post</h3>
    		    <br />
    		    <a href="#" onClick="addFile(); return false;">Add File</a>
                <? foreach((array)$files as $file): ?>
                <div>
					<?= $file['forums_posts_files_original_filename'] ?>&nbsp;
					<a class="delete_file" data-id="<?= $file['forums_posts_files_id'] ?>">
						<img alt="Delete" style="vertical-align: middle" src="/images/dialog-close2.png" />
					</a>
				</div>
                <? endforeach ?>
    		</div>
			<? endif ?>

    		<br clear="left" />
    		<br />

        <div class="form-actions">
			<input type="hidden" name="forums_id" value="<?= html::filter($_REQUEST['forums_id']) ?>" />
			<input type="submit" id="submit-btn" class="btn-ok btn-primary" value="<?= $_REQUEST['topics_id'] ? 'Edit' : 'Post' ?>" /> <br class="clear"/>
        </div>
    </form>

    </div>
	</div>

<script type="text/javascript">
	window.addEvent('domready', function() {
		$$('.delete_file').addEvent('click', function() {
			var files_id = this.get('data-id');
			var parent = this.getParent('div');
			var delRequest = new Request({
				url: FULLURL + '/members/forums/delete-forum-post-file.php',
				method: 'post',
				data: 'forum_posts_files_id='+files_id,
				onSuccess: function() {
					parent.destroy();
				}
			}).send();
		});
	});
	
	function form_submitted() {
		document.body.style.cursor = 'wait';
		$('submit-btn').set('value', 'Please Wait...').disabled = true;
	}
</script>

<? include VIEWS .'/members/footer.php'; ?>
