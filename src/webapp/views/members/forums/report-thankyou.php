<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<div id="forum-container">
    
<div id="content">
		<div id="leftcol-left">
            <h1 class="blue">Thanks!</h1>
        
            <p>Thank you for helping make our forum a better place. We've received your report and will look into it soon.<br />
            <br />
            <a href="/members/forums/view-topic.php?topics_id=<?= html::filter($_REQUEST['topics_id']) ?>" class="red">Go back to the topic</a>
            </p>
        </div><br class="clear"/>
	</div> <!--Content-->

</div> <!--  Fourm-Container-->      

              
<?php include VIEWS .'/members/footer.php'; ?>
