<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>



<div id="content">
        <?php include(VIEWS.'/notice.php') ?>
        <form action="/members/forums/report.php" method="post" id="report">
            <h2>Report a post</h2>
            <strong>Reason</strong><br />
            <select name="reasons_id" style="width:300px;">
                <?php foreach($reasons as $reason): ?>
                <option value="<?= $reason['report_reasons_id'] ?>"><?= $reason['report_reasons_name'] ?></option>
                <?php endforeach; ?>
            </select><br />
            <br />
            <strong>Please provide a description for this report</strong><br />
            <textarea name="report_desc" style="width: 300px; height: 120px; resize: none;"></textarea>
            <br />
            <br />
            <input type="hidden" name="posts_id" value="<?= html::filter($_REQUEST['posts_id']); ?>" />
            <input type="submit" value="Report" class="btn right" /><br class="clear"/>
        </form>
    </div>
<?php include VIEWS .'/members/footer.php'; ?>
