<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<div id="content">
        <h2>Search results for tag: <?= $tag['global_tags_name'] ?></h2>
        <div class="left paging">
            <? if($pages): ?>Pages: <?= $pages; ?><? endif; ?>
        </div>
        <br clear="all" />
       
        <div id="topicposts">
            <? foreach($posts as $post): ?>
            <? include '_topicpost.php' ?>
            <? endforeach; ?>
        </div>
        <div class="paging right">
            <? if($pages): ?>Pages: <?= $pages; ?><? endif; ?>
        </div>
    </div>
<? include VIEWS .'/members/footer.php'; ?>
