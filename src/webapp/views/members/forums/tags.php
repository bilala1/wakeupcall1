<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

	<div id="content">
            <h1>Forum Tags</h1>
    
            <ul style="padding-left:30px;">
                <? foreach($tags as $tag): ?>
                <? extract($tag) ?>
                <li>
                    <a href="/members/forums/tags/<?= $global_tags_name ?>/<?= $global_tags_id ?>"><?= $global_tags_name ?></a> 
                </li>
                <? endforeach ?>
            </ul>
                
       </div>
   
<?php include VIEWS .'/members/footer.php'; ?>
