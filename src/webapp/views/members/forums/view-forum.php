<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<div id="content">
        <? if($_REQUEST['forums_id'] == 1 || forums::is_in_message_forum($_REQUEST['forums_id'])): ?>
        <span class="info_icon"></span>
            <p class="page-desc">
<!--                Communicate with other members, risk managers and attorneys!
                Chances are that if you add a discussion topic or participate in an existing topic, you will discover answers from your peers and from industry experts.
                Please be respectful of all participating parties and keep in mind that inappropriate discussion points will be taken off line.
                The <?= SITE_NAME ?> moderators will do their best to keep discussion topics moving in an appropriate manner.-->
                
<!--                This is where your corporate office will leave messages and documents for your use. It's a great way to ensure that
                everyone is using the same tools and getting the same information. (See tutorial for help)-->
                
                Communicate with other members, risk managers and attorneys! Chances are, if you add a discussion topic, or participate 
                in an existing topic, you will discover answers from your peers and from industry experts. Please be respectful of all 
                participating parties and keep in mind that inappropriate discussion points will be taken off line. 
                The <?= SITE_NAME ?> moderators will do their best to keep discussion topics moving in an appropriate manner.
                <br /><br />
                <?= html::checkbox(1,'notifications_forum_update',$notifications['notifications_forum_update']); ?> Check box to be notified of any new topics 

            </p>
            <span class="info_text">Hide Text Box</span>
            <br class="clear" />
            <script type="text/javascript">
                window.addEvent('load', function(){
                    //webinars notification
                    var webinarsRequest = new Request({
                    url: '<?= FULLURL; ?>/members/ajax-change-notification-setting.php',
                    method: 'get',
                        onSuccess: function(responseText){
                            $('notifications_forum_update').set('value', responseText);
                        }
                    });
                    $('notifications_forum_update').addEvent('click', function(event){
                        webinarsRequest.send('yn=' + this.get('checked')+'&type=notifications_forum_update');
                    });
                });                
                <? /* open or collapse infobox? */ ?>
                <? if(!$infobox_message_forum): ?>
                   $$('.page-desc').setStyle('display', 'none');
                   $$('.info_text').set('html','Show Text Box');
                <? endif; ?>
                window.addEvent('load', function(){
                        //infobox settings
                        var webinarsRequest = new Request({
                        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
                        method: 'get',
                            onSuccess: function(responseText){
                                if(responseText == 'on'){
                                    $$('.page-desc').reveal();
                                    $$('.info_text').set('html','Hide Text Box');
                                }
                                else {
                                    $$('.page-desc').dissolve();
                                    $$('.info_text').set('html','Show Text Box');
                                }
                            }
                        });
                        $$('.info_icon, .info_text').addEvent('click', function(event){
                            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                                var show = 'true';
                            }
                            else {
                                var show = 'false';
                            }
                            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_message_forum');
                        });
                })
            </script>
        <? else: ?>
            <span class="info_icon"></span>
            <p class="page-desc">
                This is where your corporate office will leave messages and documents for your use. It's a great way to ensure that everyone 
                is using the same tools and getting the same information. (See tutorial for help)
            </p>
            <span class="info_text">Hide Text Box</span>
            <br class="clear" />
            <script type="text/javascript">
                <? /* open or collapse infobox? */ ?>
                <? if(!$infobox_corporate_forum): ?>
                   $$('.page-desc').setStyle('display', 'none');
                   $$('.info_text').set('html','Show Text Box');
                <? endif; ?>
                window.addEvent('load', function(){
                        //infobox settings
                        var webinarsRequest = new Request({
                        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
                        method: 'get',
                            onSuccess: function(responseText){
                                if(responseText == 'on'){
                                    $$('.page-desc').reveal();
                                    $$('.info_text').set('html','Hide Text Box');
                                }
                                else {
                                    $$('.page-desc').dissolve();
                                    $$('.info_text').set('html','Show Text Box');
                                }
                            }
                        });
                        $$('.info_icon, .info_text').addEvent('click', function(event){
                            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                                var show = 'true';
                            }
                            else {
                                var show = 'false';
                            }
                            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_corporate_forum');
                        });
                })
            </script>
        <? endif; ?>
        <h1 class="left"><?= $forum['forums_name'] ?></h1>
        <div class="page-options">
            <? if($forum_permissions['new_topic']): ?>
            <a href="/members/forums/new-topic.php?forums_id=<?= html::filter($_REQUEST['forums_id']) ?>">New Discussion</a>
            <? endif ?>
        </div><br clear="all" />

        <?/*<div id="breadcrumb" class="left">
            <?= $breadcrumb; ?>
        </div><br class="clear"/>*/?>

        <? if($pages): ?>
        <div class="left paging">
            Pages: <?= $pages; ?>
        </div>
        <br clear="all" />
        <? endif; ?>

        <table width="100%" align="center" cellpadding="1" cellspacing="1" border="0" id="forumlist">
            <tbody>
                <tr>
                    <th colspan="2">Topic</th>
                    <th width="60">Views</th>
                    <th width="70">Replies</th>
                    <th  width="115">Last Post</th>
                </tr>
                <? foreach($topics as $topic): ?>
                <tr class="item" <?= $topic['topics_stickied'] ? 'style="background-color:#F0F0EE"' : '' ?>>
                    <td>
						<?= $topic['has_attachments'] ? '<img src="/images/paperclip.png" alt="attachments" />' : '' ?>
                        <?= $topic['not_read'] ? 'New Post!<br />' : '' ?>
                        <?= $topic['topics_stickied'] ? '<img src="/images/sticky.png" alt="stickied topic" />' : '' ?>
                    </td>
                    <td>
                        <h3><a class="topictitle" href="/members/forums/view-topic.php?topics_id=<?= $topic['topics_id'] ?>"><?= $topic['topics_title'] ?></a></h3>
                        By <?= $topic['members_username'] ?>: <?= times::tz_convert($topic['topics_createdate']); ?>
                    </td>
                    <td align="center"><?= number_format($topic['topics_views']); ?></td>
                    <td align="center"><?= number_format($topic['topics_replies']); ?></td>
                    <td align="right">
                        By <?= $topic['last_post']['members_username'] ?><br /><?= times::tz_convert($topic['last_post']['posts_postdate'], DATE_FORMAT_FULL) ?>
                    </td>
                </tr>
                <? endforeach; ?>
            </tbody>
        </table>

        <div class="paging">
            <? if($pages): ?>Pages: <?= $pages; ?><? endif; ?>
        </div>

    </div>
<? include VIEWS .'/members/footer.php'; ?>
