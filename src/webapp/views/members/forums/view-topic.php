<?
$forumsId = $_REQUEST['forums_id'];
if(!$forumsId) {
    $forumsId = forums::get_forum_id_by_topic($_REQUEST['topics_id']);
}
if(!$forumsId) {
    $forumsId = 1;
}
$isMainForum = $forumsId == 1 || forums::is_in_message_forum($forumsId);

$page_title        = SITE_NAME;
$page_name         = 'Forums';
$sub_name          = $isMainForum ? 'Message Forum' : 'Corporate Bulletin';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS .'/members/header.php'; ?>

<script type="text/javascript">

window.addEvent('load', function(){
    $$('.multiquote').addEvent('click', function(){
        var posts_id = this.get('posts_id');
        var topics_id = this.get('topics_id');
        var parent = this;

        var quote = new Request.JSON({
            url: BASEURL + '/members/forums/ajax-multiquote.php',
            method: 'get',
            onSuccess: function(json){
                if(json.action == 'added'){
                    parent.set('html', 'Quoting...');
                    parent.addClass('multiquote_active');
                }
                else {
                    parent.set('html', 'Quote');
                    parent.removeClass('multiquote_active');
                }
				window.location.reload();
            }
        }).get({
            topics_id: topics_id,
            posts_id: posts_id
        });
    });


});

</script>
<div id="content">
    	    <div id="breadcrumb">
    	    	<?= $breadcrumb; ?>
	    	</div>

			<? if(ActiveMemberInfo::GetMemberId()): ?>
        		<? if($watching): ?>
		        <br>You are watching this topic | <a href="/members/forums/unwatch.php?topics_id=<?= $_REQUEST['topics_id'] ?>">Unwatch</a><br />
		        <? else: ?>
		        <a href="/members/forums/watch.php?topics_id=<?= $_REQUEST['topics_id'] ?>">Watch Topic</a><br />
		        <? endif; ?>
		    <? endif; ?>
		    <br />

            <h2><?= $topic['topics_title'] ?></h2>

	    	<div class="paging right">
	    		<? if($pages): ?>Pages: <?= $pages; ?><? endif; ?>
    		</div>
            <br clear="all" />
    		<div id="topicposts">
    			<? foreach($posts as $post): ?>
                    <? include '_topicpost.php' ?>
    			<? endforeach; ?>
    		</div>

    		<div class="paging right">
    			<? if($pages): ?>Pages: <?= $pages; ?><? endif; ?>
    		</div>
    		<br clear="right" />

			<? if (ActiveMemberInfo::GetMemberId() && $forum_permissions['reply']): ?>
				<!--<a id="reply-form"></a>-->
				<? include(VIEWS . '/members/forums/_reply-form.php'); ?>
			<? endif; ?>
        </div>
<? include VIEWS .'/members/footer.php'; ?>
