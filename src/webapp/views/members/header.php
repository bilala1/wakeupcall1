<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?= isset($page_title) ? $page_title : SITE_NAME?><?= isset($page_name) ? ' | ' . $page_name : '' ?></title>
    <meta name="description" content="<?php echo isset($page_description) ? $page_description : SITE_NAME ?>" />
    <meta name="keywords" content="<?php echo isset($page_keywords) ? $page_keywords : SITE_NAME ?>" />
    <?php include('scripts.php'); ?>
    <link rel="icon" type="image/ico"  href="/images/favicon.ico"  />
    </head>
<body>
    <? if(surveys::get_unanswered(ActiveMemberInfo::GetMemberId())): ?>
	<div class="survey-list">
		<a href="/members/surveys/list.php">New Survey</a>
  	</div>
    <? endif ?>

    <div id="wrapper">
    	<div id="header">
            <a name="news_top"></a>
        	<div class="w470 left" style="margin:0;padding:0">
                <img src="<?=empty($_SESSION['custom_logo_directory']) || empty($_SESSION['custom_logo_file_name']) ? '/images/logo.jpg' : $_SESSION['custom_logo_directory'] . "/" . html::filter($_SESSION['custom_logo_file_name']) .".org.jpg" ?>" alt="<?= SITE_NAME ?>" id="logo" style="margin:5px 0 0;"/>
        	</div>

            <div class="w470 right" id="search" align="right">
                <form action="/members/search.php" method="get">
                	<a href="/members/logout.php" id="logout">Logout</a>
                    <strong>Search</strong>
                    <input type="text" name="q" class="text" value="<?= $_GET['q'] ?>" style="width:150px"/>
                    <?= ''//html::select(search::get_types(), 'type', $_REQUEST) ?>
                    <input type="submit" value="Search"  class="btn"/>
                </form>
            </div><br clear="all" />

            <?php include '_navigation.php' ?>

        </div>

        <? include VIEWS . '/members/notice.php' ?>

