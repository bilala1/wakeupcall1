<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/style.css"/>
<style type="text/css">
    ul li {
        line-height: 15px;
        
    }
</style>
</head>

<body style="text-align: left; padding: 10px; background: white;">
<p><?= SITE_NAME ?> has partnered with HR360 to bring our members a resource to assist with everyday Human Resources needs and concerns. About HR 360:</p>
<p>HR 360 is a unique resource that explains and simplifies complicated information about a host of key employment areas. HR 360 covers much more than just benefits-including labor and employment topics, such as recruiting and hiring, employee testing, EEO and discrimination, vacation & leave policies, and employee termination issues, as well as selected tax and retirement planning issues.</p>
<p>HR 360 discusses, explains and analyzes complex federal laws and confusing acronyms-such as COBRA, ERISA, ADA, FLSA, FMLA, HIPAA and others-in plain English, so that they can be understood by the professional and lay person alike.</p>
<p>HR 360 also includes a voluminous library of proprietary forms, checklists, model language, company policies, sample letters and employee communications covering a wide range of benefits and related employment areas. These forms can be downloaded and customized for use in Employee Handbooks and other company communications.</p>
<p>Read over the following agreement so you are familiar with the terms of HR 360 services, in addition to what HR 360 can and cannot provide you. This agreement is direct between you, the <?= SITE_NAME ?> member, and HR 360. The following Terms and Conditions refer to the use of the HR360 website and content. There are links to the HR360 website throughout the <?= SITE_NAME ?> site for your use.</p>
<h2>Agreement:</h2>
<p>This License Agreement (the "Agreement") is entered into as of this date ("Effective Date"), between Licensor, HR 360, Inc ("HR 360") and Licensee, <?= SITE_NAME ?> Member (&quot;Member&quot; or &quot;You&quot;). Member and HR360 are referred to herein individually and collectively as "Party" or "Parties."</p>
<p>HR 360 provides web-based content from an HR and Benefits Library covering United States laws and topics for private sector businesses (&quot;Licensed Content&quot;) such as:</p>
<ul>
<li>Employee benefits</li>
<li>Human resources</li>
<li>Retirement planning</li>
<li>Federal and state laws</li>
<li>Other information, including but not limited to forms, tools, glossaries, FAQs, checklists, policies, model documents, and a human resources and employee benefits newsletter,</li>
<li>All collectively referred to as "Information"</li>
</ul>
<p>Member will have access to the Licensed Content in accordance with the terms & conditions of this agreement.</p>
<h3>Additional Definitions:</h3>
<p>HR 360, licensor, refers to the owner and operator of HR 360, hr360.com and hrlibrary360.com, its principals, officers, employees, affiliates, heirs, assigns, authors, editors, researchers and associates.</p>
<p>The term &quot;Publisher,&quot; as used herein, refers to HR 360, the owner and operator of HR 360, its principals, officers, employees, affiliates, heirs, assigns, authors, editors, researchers and associates.</p>
<p>The term &quot;Distributors,&quot; as used herein, refers to <?= SITE_NAME ?> and its principals, officers, employees and consultants.</p>
<h3>Conditions of Use:</h3>
<p>By using the HR 360 website, you acknowledge your agreement to the following conditions of use without limitation or qualification. Please read these conditions carefully before using this website. These terms and conditions may be revised at any time by updating this posting. You are bound by any such revisions and should therefore periodically visit the Terms page on the HR360 website to review the then current terms and conditions to which you are bound.</p>
<p>All articles and information, including, but not limited to, interactive tools such as the Benefits Notices Generator, and any other materials provided on this site (collectively, "Information") are provided for general information purposes only and are not intended to constitute legal, accounting or tax advice or opinions on any specific matters. Transmission of the materials and information contained herein is not intended to create, and receipt thereof does not constitute formation of, an attorney-client relationship.  Laws and regulations change frequently and their application can vary widely based upon the specific facts and circumstances involved. Your situation may involve federal, state or local laws, and/or application of company plan documents, employee handbook or other company policies. Accordingly, information on this website are not promised or guaranteed to be accurate, correct or complete. If legal advice or other expert assistance is required, the services of a competent professional should be sought. The Information provided relates to U.S. federal and state laws only and does not cover issues of international law or regulations.</p>
<p>The information provided and the opinions expressed are not necessarily those of HR 360, or its Distributors.</p>
<h3>Disclaimer:</h3>
<p>HR 360 and Distributors assume no liability for the use or interpretation of information contained herein. HR 360 and Distributors expressly disclaim all liability in respect to actions taken or not taken based on any or all the contents of this website. HR 360 and Distributors will not be liable, under the terms of this disclaimer or otherwise, for any loss of profit, loss of revenue, loss of contract or any other indirect or consequential loss you may suffer as a result of the use of any of the information contained in the web site.  The materials presented on this site are provided "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS.</p>
<p>You, and not HR 360, are responsible for the applicability and accuracy of Information as it relates to your company, employees or clients. Your use of Information does not relieve you of responsibility, including those to any third party, for matters involving your company, employees or clients (e.g., preparation of tax returns, etc.) and you may not rely upon HR 360 for advice regarding same. The provision of Information by HR 360 is not intended to create, and your use does not constitute, any form of relationship between HR 360 or the author(s) and you.</p>
<h3>Limitation of Liability:</h3>
<p>Under no circumstances, including, but not limited to, negligence, shall HR 360 or any party involved in creating, producing, delivering or distributing the website be liable to you for any direct, incidental, consequential, indirect, or punitive damages that result from the use of, or the inability to use, the materials on this website. In no event shall HR 360's total liability to you for all damages, losses, and causes of action exceed the amount paid by you, if any, for accessing this website. HR 360 also assumes no responsibility, and shall not be liable for, any exposure to or damages from worms, viruses, trojans and the like, that may infect your computer equipment or other property as a result of your access to, use of, or browsing in the website or your downloading of any materials, data, text, images, video, or audio from this website, including as a result of any violation or failure, for any reason, of any encryption technology employed by HR 360.</p>
<h3>Restrictions on Use of Materials:</h3>
<p>This website is owned and operated by HR 360. Except as otherwise expressly permitted by HR 360, no materials from this website or any website owned, operated, licensed or controlled by HR 360 may be copied, reproduced, republished, uploaded, posted, transmitted, or distributed in any way. You may download material displayed on this website for your use only, provided that you also retain all copyright and other proprietary notices contained on the materials. You may not distribute, modify, transmit, reuse, repost, or use the content of this website for public or commercial purposes, including the text, images, audio, and video without HR 360' written permission. HR 360 neither warrants nor represents that your use of materials displayed on this website will not infringe rights of third parties.</p>
<h3>Copyright Notice and Reproduction:</h3>
<p>This site as a whole is copyrighted as a collective work, and individual works appearing on or accessible through this site are likewise subject to copyright protection. You agree to honor the copyrights in this site (including the selection, coordination and arrangement of the contents of this site) and in the works available on or through this site. Copyright is not claimed for materials originating from U.S. government sources.</p>
<p>In addition, trademarks and service marks belonging to us or to others appear on or are accessible through this site. The fact that we have permitted you access to this site does not constitute authorization to reproduce our trademarks or service marks for any other purpose.</p>
<h3>Changes to the Website:</h3>
<p>HR 360 may correct any errors or omissions or change the format or content of the site at any time.</p>
<h3>Illegal Activity:</h3>
<p>If you access or use any information on this web site and by doing so, you break any law, HR 360 will not be responsible.</p>
<h3>Changes to Terms and Conditions:</h3>
<p style="font-weight: bold">These terms and conditions are subject to change without notice and at any time.  While accessing or using the website, you agree to be bound by any and all current terms and conditions. You should always check such terms and conditions each time you access or use the HR360 website.</p>
<h3>Communicating with Us:</h3>
<p>Member may send us email. However, if Member communicates with us, Member should not send us confidential or sensitive information via email because Member communication will not be treated as privileged or confidential. If Member communicates with HR 360 by email, Member should note that the security of internet email is uncertain. By sending sensitive or confidential email messages Member accepts the risks of such uncertainty and possible lack of confidentiality over the internet.</p>
<p><a href="mailto:support@hr360.com">support@hr360.com</a></p>
<p>In accordance with the US Treasury Department Circular 230, materials on the HR 360 website and communications with the Publisher or the authors of these materials are not to be considered a "covered opinion" or other written tax advice, and should not be relied upon for any IRS audit, tax dispute or other purposes.</p>
<h3>Links and Internet Resources:</h3>
<p>We provide references and links to websites not affiliated with HR 360 as a service to our subscribers. While we believe these sites to contain valuable information, we cannot attest to the accuracy of information provided by these links or any other linked site. Providing these links does not constitute an endorsement of the sponsors of the site or the information or products presented therein.</p>
<p>Also, be aware that the privacy protection provided on HR 360 may not be available at the external link. Kindly refer to the Terms of Use, Privacy and other information on all sites visited.</p>
<h3>Personal Use and Site Licenses:</h3>
<p>By accessing this site, you agree to use it for your personal research and reference purposes only (either for the benefit of yourself or for the benefit of your employer). You agree that you will not disclose your password to anyone else, nor otherwise permit anyone to gain unauthorized access to the non-public areas of our site.</p>
<p>Use of the HR 360 website constitutes your agreement to abide by this Agreement and respect the Copyrights of HR 360.</p>
<p>Please note, that for purposes of the above Agreement, you, the <?= SITE_NAME ?> member, anyone representing you, or anyone you have or have not given permission to use <?= SITE_NAME ?> services in association with your membership, are considered the Member and agree to all the terms of this Agreement.</p>
<p style="font-style: italic">Member shall hold harmless <?= SITE_NAME ?>, its Owners, officers, directors, employees, agents, representatives, from and against all losses, claims, damages, injuries, demands, payments, suits, actions, recoveries, judgments and expenses, including but not limited to attorneys' fees, arising from any services provided HR 360.

</body>
</html>          
