<?
    $page_title        = SITE_NAME;
    $page_name         = 'Employee Mgmt';
    $sub_name		   = 'HR';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <div class="right">
        <form action="documents-search.php" method="post">
            <label style="text-align:right">Search Document Library: </label>
            <?= html::input($_REQUEST, 'documents_title') ?>
            <input type="submit" value="Search" class="btn"/>
        </form>
    </div>  
    <h1>Search HR Docs.</h1>  
        <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
            <tr>
                <th><?= strings::sort('documents_title', 'Name') ?></th>
                <th>Download</th>
                <th width="200">Actions</th>
            </tr>
            <? foreach($documents as $document): ?>
            <? if($document['documents_file']): ?>
                    <tr>
                        <td><?= $document['documents_title'] ?></td>
                        <td>
                            <? if((members::get_members_status(ActiveMemberInfo::GetMemberId())== 'free') && ((int)$document['documents_freetrial']==0)): ?>
                                <i>Restricted to full members.</i>
                            <? else: ?>
                                <a target="_blank" href="/members/documents/download.php?documents_id=<?= $document['documents_id'] ?>"><?= $document['documents_filename'] ?></a>
                            <? endif; ?>
                        </td>
                        <td>
                            <a href="/members/documents/add-document.php?documents_id=<?= $document['documents_id'] ?>">add to my documents</a>
                        </td>
                    </tr>
            <? endif ?>
            <? endforeach; ?>
        </table>
    <?= $paging->get_html() ?>    

</div>

<? include VIEWS. '/members/footer.php' ?>
