<?
$page_title = SITE_NAME;
$page_name = 'Employee Mgmt';
$sub_name = 'HR';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
    <h1>Human Resources</h1>
    <? if (!$agreement_yn): ?>
        <div>
            <label><h3>Agreement:</h3></label>
            <iframe src="agreement.php" width="100%" height="300" style="width:100%">
                <p>Your browser does not support iframes.</p>
            </iframe>
            <form method="post" url="index.php" id="agreement-form">
                <?= html::checkboxfield('<b>I Agree:</b>', 1, 'agree_with_agreement', $_REQUEST, array(),
                    $errors) ?>

                <div class="form-actions">
                    <?= html::hidden($forwardUrl, 'url') ?>
                    <?= html::submit('Submit',
                        array('class' => 'button btn-primary btn-ok', 'name' => 'submit', 'id' => 'submit-button')); ?>
                </div>
            </form>
        </div>
    <? else: ?>
        <p>WAKEUP CALL has partnered with HR360 to provide our members up to date information and resources for your HR needs. You will find links to specific sections of HR360 in our Document Library as well as links below. Once you are on the HR360 website, feel free to look around beyond the links we provided to familiarize yourself with all the tools available.</p>
        <? if($forwardUrl) { ?>
            <div class="form-actions">
                <?=html::createHrLink('View on HR360', $forwardUrl, array('class'=>'button btn-ok btn-primary')) ?>
            </div>
        <? } ?>
    <img src="<?= HTTPS_FULLURL ?>/images/hr.jpg" style="float:right; margin-right:2em;">
            <ul style="margin-left:66px;">
            <li><?= html::createHrLink('HR360 Home Page') ?></li>
            <li><?= html::createHrLink('COBRA', 'https://www.hr360.com/Employee-Benefits/COBRA/Introduction-to-COBRA-Continuation-Coverage.aspx') ?></li>
            <li><?= html::createHrLink('Discrimination', 'https://www.hr360.com/Human-Resources/Discrimination/Introduction-to-Discrimination.aspx') ?></li>
            <li><?= html::createHrLink('Employee Records & Files', 'https://www.hr360.com/Human-Resources/Employee-Records-and-Files/Employer-Records-and-Files.aspx') ?></li>
            <li><?= html::createHrLink('FMLA', 'https://www.hr360.com/Employee-Benefits/Family-and-Medical-Leave-Act-(FMLA)/Introduction-to-FMLA.aspx') ?></li>
            <li><?= html::createHrLink('Forms & Policies', 'https://www.hr360.com/form-search/') ?></li>
            <li><?= html::createHrLink('Handbook Builder', 'https://www.hr360.com/tools/Employee-Handbook-Builder/') ?></li>
            <li><?= html::createHrLink('Healthcare Reform', 'https://www.hr360.com/Health-Care-Reform') ?></li>
            <li><?= html::createHrLink('Hiring Process', 'https://www.hr360.com/Recruitment-and-Hiring/Hiring-Process/Hiring-Process.aspx') ?></li>
            <li><?= html::createHrLink('Job Description Builder', 'https://www.hr360.com/tools/Job-Description-Builder/') ?></li>
            <li><?= html::createHrLink('Labor Laws & Guidelines', 'https://www.hr360.com/Human-Resources/Labor-Laws-and-Guidelines/Labor-Laws-and-Guidelines.aspx') ?></li>
            <li><?= html::createHrLink('State Laws', 'https://www.hr360.com/state-laws') ?></li>
            <li><?= html::createHrLink('Termination Steps', 'https://www.hr360.com/Termination/Termination-Steps/Termination-Steps.aspx') ?></li>
            <li><?= html::createHrLink('Unemployment', 'https://www.hr360.com/Employee-Benefits/Unemployment-Insurance/Introduction-to-Unemployment-Insurance.aspx') ?></li>
        </ul>
    <? endif; ?>
</div>

<? include VIEWS . '/members/footer.php' ?>
