<div>
<!-- START - IRMI Glossary -->
<!-- IRMI Glossary of Insurance and Risk Management Terms -->
<!-- http://www.IRMI.com/online/insurance-glossary/ -->
<!-- Copyright � 2000-2011 International Risk Management Institute, Inc. (IRMI). All rights reserved. -->

<h1>Insurance and Risk Management Terms</h1>
<hr />
<? foreach($terms as $term): ?>
<p><a href="<?= $term['irmi_terms_url'] ?>" target="_blank"><?= $term['irmi_terms_name'] ?></a></p>
<? endforeach ?>
<hr />
<p class="Copyright">Copyright &copy International Risk Management Institute, Inc. (IRMI)<br /><a href="../../../online/insurance-glossary/default.aspx">www.IRMI.com/Online/Insurance-Glossary/</a></p>
<!-- END - IRMI Glossary -->
</div>