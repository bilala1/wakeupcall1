<?
    $page_title        = SITE_NAME;
    $page_name         = 'Resources';
    $sub_name          = 'SDS Library';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<style type="text/css">
fieldset{
    position:relative;
}
.form-field{
    float:left;
    margin:0 20px 0 0;
}
.form-field label{
    display:block;
    width:auto;
}
.form-field .input-text{
    width:170px;
}
fieldset .input-submit{
    position:absolute;
    right:10px;
    bottom:10px;
}
</style>
<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
        Use one or more of the fields below to search for Safety Data Sheets stored on Actio's database of
        SDS information. You can download them to your computer and/or print them for your SDS book. <br />
        (See tutorial for help)
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />       
    <h1>SDS Library</h1>
    <form action="/members/msds/index.php" method="get">
    <fieldset>
        <legend>Search</legend>
        <?= html::textfield('Product', $_GET, 'product') ?>
        <?= html::textfield('Manufacturer', $_GET, 'manufacturer') ?>
        <?= html::textfield('SDS Number', $_GET, 'msdsnumber') ?>
        <?= html::submit('Search') ?>
    </fieldset>
    </form>
    <br /><br />
    <table style="width:100%;">
        <tr>
            <th>Product</th>
            <th>Synonyms</th>
            <th>Manufacturer</th>
            <th style="width:100px">SDS Number</th>
        </tr>
        <? if(!$results['DATA']): ?>
        <tr><td colspan="4" style="text-align:center">No Results</td></tr>
        <? else: ?>
        <? foreach((array)$results['DATA'] as $result): ?>
        <?/* Before they changed the format
        <tr>
            <td><a href="/members/msds/external-download.php?msds_name=<?= urlencode(html::xss($result[6])) ?>&url=<?= urlencode(html::xss($result[8])) ?>" target="_blank"><?= html::filter($result[6]) ?></a></td>
            <td><?= html::filter($result[7]) ?></td>
            <td><?= html::filter($result[3]) ?></td>
            <td><?= $result[4] ? html::filter($result[4]) : '&nbsp;' ?></td>
        </tr>*/?>
        <tr>
            <td><a href="/members/msds/external-download.php?msds_name=<?= urlencode(html::xss($result[5])) ?>&url=<?= urlencode(html::xss($result[7])) ?>" target="_blank"><?= html::filter($result[5]) ?></a></td>
            <td><?= html::filter($result[6]) ?></td>
            <td><?= html::filter($result[2]) ?></td>
            <td><?= $result[3] ? html::filter($result[3]) : '&nbsp;' ?></td>
        </tr>
        <? endforeach ?>
        <? endif ?>
    </table>
</div>
<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box'); 
    <? endif; ?>    
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_msds');
        });
    })          
</script>

<? include VIEWS. '/members/footer.php' ?>
