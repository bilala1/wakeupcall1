<?
$page_title = SITE_NAME;
$page_name = 'Tracking';
$sub_name = 'Certificates';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>
<div id="content">
    <h1 class="left">Add Certificate</h1>

    <div class="page-options"><a href="/members/vendors/index.php">Manage Vendors</a> | <a
            href="/members/my-documents/certificates/index.php">Return to Certificates List</a></div>
    <br class="clear"/>

    <form action="/members/my-documents/certificates/edit.php" method="get" enctype="multipart/form-data"
          id="editCertificate">

        <fieldset>
            <legend>Certificate Details</legend>
            <?= html::selectfield_from_query('Location:', $licensed_locations, 'licensed_locations_id', $certificate,
                'licensed_locations_name', 'Please Select', array(), $errors); ?>
        </fieldset>

        <input type="hidden" name="action" value="setLocation"/>
        <input type="hidden" name="vendors_id" value="<?=$_REQUEST['vendors_id']?>"/>

        <div class="form-actions">
            <input type="submit" id="save" value="Continue" class="btn-primary"/>
        </div>
    </form>
</div>

<script type="text/javascript">
    function chkCertificateForm(form) {
        var certificateIsValid = true;

        $$('p.form-error').destroy();

        var $location = $('licensed_locations_id');
        if ($location) {
            certificateIsValid &= window.WUC.validateRequired($location, 'Please select a location');
        }

        if (!certificateIsValid) {
            window.WUC.scrollToFirstError()
        }
        return certificateIsValid;
    }

    $('save').addEvent('click', function (e) {
        var form = $('editCertificate');
        if (chkCertificateForm(form)) {
            form.submit();
        } else {
            e.preventDefault();
        }
    });

</script>

<? include VIEWS . '/members/footer.php' ?>

