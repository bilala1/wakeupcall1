<script>
    var remind_expiring_members = <?=json_encode($remind_expiring_members)?>;
    var remind_expired_members = <?=json_encode($remind_expired_members)?>;
 </script>   
<?
    $page_title        = SITE_NAME;
    $page_name         = 'Tracking';
    $sub_name		   = 'Certificates';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<style type="text/css">
label{
    width:200px;
}
.multiinput-container{
    width:500px;
}
.multiinput-container label{
    width:200px;
}
.send-rem-check{ width: 720px;}
.send-rem-check .Dynlabel{ width: 50% !important; float: left;}
.clearfix{ clear: both;}
</style>
<div id="content">
    <h1 class="left"><?= $_REQUEST['certificates_id'] ? ($_REQUEST['action'] != 'update' ? 'Edit' : 'Update') : 'Add' ?> Certificate</h1>
    <div class="page-options"><a href="/members/vendors/index.php">Manage Vendors</a> | <a href="/members/my-documents/certificates/index.php">Return to Certificates List</a></div>
    <br class="clear"/>

    <form action="/members/my-documents/certificates/edit.php" method="post" enctype="multipart/form-data" id="editCertificate">

        <fieldset class="j-toggle-parent">
            <div class="j-toggle-summary">
               <?= html::plainTextField("Location", $location['licensed_locations_name']?$location['licensed_locations_name']:'',"summary_locations_name") ?>
               <?= html::plainTextField("Vendor", $vendors[$certificate['join_vendors_id']]?$vendors[$certificate['join_vendors_id']]:'',"summary_vendor") ?>
               <? $expiry_date =  $certificate['certificates_expire']!='0000-00-00'?Formatting::date($certificate['certificates_expire'], 'Not Set'):''; ?>
               <?= html::plainTextField("Expiry Date", $expiry_date,"summary_expiry") ?>
            </div>
            <legend>Certificate Details <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
            <?= html::textfield('Location:', $location, 'licensed_locations_name', array('readonly' => 'readonly', 'disabled' => 'disabled')) ?>
            <?= html::hidden($certificate, 'join_licensed_locations_id') ?>

            <? if($_REQUEST['action'] != 'update') { ?>
                <? if (ActiveMemberInfo::_IsAccountAdmin()): ?>
                    <div class="form-field">
                        <label>
                            <span>Permissions for non-Account Admins:
                                <?= html::help_button(null, null, 'These settings configure access to this certificate for users with the Location Admin or Certificates permission. Account Admins can always view, edit, or delete a certificate.') ?>
                            </span>
                        </label><fieldset>
                            <label class="checkbox-label">
                                <input value="0" name="certificates_location_can_view" type="hidden">
                                <input value="1"
                                       name="certificates_location_can_view"
                                       id="certificates_location_can_view"
                                       type="checkbox"
                                       <?php if($certificate['certificates_location_can_view'] == "1") { echo "checked"; } ?>
                                       class="input-checkbox">
                                Non-Account Admins can <b>view</b> this certificate
                            </label>
                            <br>
                            <label class="checkbox-label">
                                <input value="0" name="certificates_location_can_edit" type="hidden">
                                <input value="1"
                                       name="certificates_location_can_edit"
                                       id="certificates_location_can_edit"
                                       type="checkbox"
                                    <?php if($certificate['certificates_location_can_edit'] == "1") { echo "checked"; } ?>
                                       class="input-checkbox">
                                Non-Account Admins can <b>edit/delete</b> this certificate
                            </label>
                        </fieldset>
                    </div>
                <? endif ?>

                <div class="form-field j-vendors-row">
                    <input type="hidden" id="certificates_join_vendor_id" value="<?=$certificate['join_vendors_id'] ?>" />
                    <?
                    $vendor_selected = $certificate['join_vendors_id'];
                    if(!empty($_REQUEST['vendors_id'])){
                        $vendor_selected = $_REQUEST['vendors_id'];
                    }
                    ?>
                    <label for="join_vendors_id"><span>Vendor:</span></label><?= html::select(array('' => 'Please Select') + $vendors, 'join_vendors_id', $vendor_selected, array(), $errors); ?>
                    <a href="/members/vendors/edit.php?return_to_cert=<?= $_REQUEST['certificates_id'] ?: 'new' ?>" class="add-item">Add New Vendor</a>
                </div>
            <? } ?>
            <?= html::selectfield('Type:', array('regular' => 'Regular Operations', 'special' => 'Special Projects'), 'certificates_type', $certificate, array(), $errors); ?>

            <div id="special_project_name" style="display:none">
            <?= html::textfield('Project Name:', $certificate, 'certificates_project_name', array('maxlength' => 255), $errors) ?>
            </div>

            <?= html::datefield('Expiration Date:', $certificate, 'certificates_expire', array(), $errors) ?>

            

            <? if($_REQUEST['action'] != 'update'){ ?>
                <fieldset class="j-toggle-parent">
                    <legend>Types of Coverage <a class="j-toggle-trigger"></a></legend>
                    <div class="j-toggle-content">
                    <p class="callout">
                        Choose the coverage type(s) you want to send a request for. For your convenience, the minimum limits
                        you require can be included in the request notice, but it is not required for the <?= SITE_NAME ?>
                        system. Please review your request notice below before clicking <?= $certificate['certificates_id'] ? 'Save Certificate' : 'Add Certificate' ?>
                        to confirm the limit wording meets your needs.
                    </p>
                        <?
                        function displayCoverageRow($coverages, $coverageName, $coverageId)
                        {
                            $checked = $coverages[$coverageName] ? 'checked="checked"' : '';
                            $disabled = $coverages[$coverageName] ? '' : 'disabled';
                            $value = $coverages[$coverageName]['certificate_coverages_amount_1'] == '0.00' ? '' : $coverages[$coverageName]['certificate_coverages_amount_1'];
                            $otherValues = '';
                            echo "<tr>
                                <td>
                                    <label for='$coverageId'>
                                    <input type='checkbox' class='input-checkbox coverage_checkbox' id='$coverageId' name='coverages[]' value='$coverageName' $checked>
                                    $coverageName</label>
                                </td>
                                <td colspan='3'>
                                    <input maxlength='255'  class='coverage_amt' id='{$coverageId}_amount' $disabled name='coverages_amount[$coverageName]' value='$value'>
                                    Per Occurrence
                                </td>
                            </tr>";
                        }
                        ?>
                        <table class="cert-coverages">
                            <tr>
                                <th>Coverage</th>
                                <th colspan="3">Requested Limit</th>
                            </tr>
                            <? displayCoverageRow($coverages, 'General Liability (GL)', 'coverage_gl'); ?>
                            <tr>
                                <td>
                                    <?
                                    $checked = '';
                                    $disabled = 'disabled';
                                    if ($coverages["Worker's Compensation (W/C)"]) {
                                        $checked = 'checked="checked"';
                                        $disabled = '';
                                    }
                                    ?>
                                    <label for="coverage_comp">
                                        <input type="checkbox" class="input-checkbox coverage_checkbox"
                                               id="coverage_comp" name="coverages[]"
                                               value="Worker's Compensation (W/C)" <?= $checked ?>>
                                        Worker's Compensation (W/C)
                                        <?= html::help_button(null, null,
                                            "Limits for Worker's Comp are in reference to only the Employer's Liability part of the policy. ") ?>
                                    </label>
                                </td>
                                <td>

                                    <input maxlength='255' class="coverage_amt"
                                           id="coverage_comp_amount" <?= $disabled ?>
                                           name="coverages_amount[Worker's Compensation (W/C)][amount1]"
                                           value='<?= $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_1'] == '0.00' ? "" : $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_1'] ?>'>
                                    Injury each Accident
                                </td>
                                <td>

                                    <input maxlength='255' class="coverage_amt" id="coverages_amount2" <?= $disabled ?>
                                           name="coverages_amount[Worker's Compensation (W/C)][amount2]"
                                           value='<?= $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_2'] == '0.00' ? "" : $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_2'] ?>'>
                                    Injury by Disease
                                </td>
                                <td>
                                    <input maxlength='255' class="coverage_amt" id="coverages_amount3" <?= $disabled ?>
                                           name="coverages_amount[Worker's Compensation (W/C)][amount3]"
                                           value='<?= $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_3'] == '0.00' ? "" : $coverages["Worker's Compensation (W/C)"]['certificate_coverages_amount_3'] ?>'>
                                    Disease Policy Limit
                                </td>
                            </tr>
                            <? displayCoverageRow($coverages, 'Automotive (Auto)', 'coverage_auto'); ?>
                            <? displayCoverageRow($coverages, 'Umbrella/Excess Liability (UMB)',
                                'coverage_umbrella'); ?>
                            <? displayCoverageRow($coverages, 'Professional / E&O (P/E&O)', 'coverage_el'); ?>
                            <? displayCoverageRow($coverages, 'Crime (CR)', 'coverage_crime'); ?>
                            <? displayCoverageRow($coverages, 'Property (Prop)', 'coverage_prop'); ?>
                            <? displayCoverageRow($coverages, 'Builder\'s Risk (BR)', 'coverage_prof'); ?>
                            <tr>
                                <td>
                                    <?
                                    $checked = '';
                                    $disabled = 'disabled';
                                    if ($coverages[$certificate['certificates_coverage_other']]) {
                                        $checked = 'checked="checked"';
                                        $disabled = '';
                                    }
                                    ?>
                                    <label for="coverage_other">
                                        <input type="checkbox" class="input-checkbox coverage_checkbox"
                                               id="coverage_other" value="Other" <?= $checked ?>>
                                        Other
                                </td>
                                <td>
                                    <input type="text" name="certificates_coverage_other"
                                           id="certificates_coverage_other" <?= $disabled ?>
                                           value="<?= $certificate['certificates_coverage_other'] ?>"/>
                                </td>
                                <td colspan="2">
                                    <input maxlength='255' class="coverage_amt"
                                           id="coverage_other_amount" <?= $disabled ?>
                                           name='certificates_coverages_other_amount'
                                           value='<?= $coverages[$certificate['certificates_coverage_other']]['certificate_coverages_amount_1'] == '0.00' ? "" : $coverages[$certificate['certificates_coverage_other']]['certificate_coverages_amount_1'] ?>'>
                                    Per Occurrence
                                </td>
                            </tr>
                        </table>
                        <? if ($errors['coverages']) { ?>
                            <p class="error form-error"><?= $errors['coverages'] ?></p>
                        <? } ?>
                    </div>
                </fieldset>
               
            <? } ?>
            </div>
        </fieldset>

        <? if($_REQUEST['action'] != 'update'){ ?>

            <fieldset class="j-toggle-parent">
                <legend>Additional Insured <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                <?= html::checkboxfield('Include request for Additional Insured:', 1,
                    'certificates_include_additional_insured_request',
                    $certificate['certificates_include_additional_insured_request'], array(), $errors) ?>

                <div class="form-field" id="legal_entity">
                    <label for="join_legal_entity_names_id">Legal Entity Names:</label>
                    <fieldset>
                        <div id="addlInsuredTemplate" class="j-addlInsuredLine" style="display:none">
                            <?= html::select($entity_name_by_location, 'join_legal_entity_names_id[]', '', array('class' => 'j-entitySelect')); ?>
                            <div class="j-newAddlEntity" style="display:none">
                                <input class='j-entityName disabledHideText' maxlength='255' name='legal_entity_names_name[]'>
                                <? if (ActiveMemberInfo::_IsAccountAdmin()) { ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;Associate with:
                                    <span style="display: inline-block">
                                        <label>
                                            <input type="radio" value="Location" name="associate_with[]"/>
                                            Location Only
                                        </label>
                                            <br>
                                        <label>
                                            <input type="radio" value="Account" name="associate_with[]"/>
                                            Entire Account
                                        </label>
                                    </span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <? } ?>
                            </div>
                            <a class="j-removeAddlEntity">Remove</a>
                        </div>
                        <? foreach ($all_legal_entity_names as $index=>$join_legal_entity_names_id) { ?>
                            <div class="j-addlInsuredLine">
                                <?= html::select($entity_name_by_location, 'join_legal_entity_names_id[]', $join_legal_entity_names_id, array('class' => 'j-entitySelect', 'style' => $join_legal_entity_names_id == 'add' ? 'display:none' : '')); ?>
                                <span class="j-newAddlEntity" style="display: <?= $join_legal_entity_names_id == 'add' ? '' : 'none' ?>">
                                    <input class='j-entityName disabledHideText' maxlength='255' name='legal_entity_names_name[]' value="<?= $join_legal_entity_names_id == 'add' ? $locationName : '' ?>" />
                                    <? if (ActiveMemberInfo::_IsAccountAdmin()) { ?>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Associate with:
                                        <span style="display: inline-block">
                                        <label for="location">
                                            <input type="radio" value="Location" name="associate_with[<?= $index ?>]"/>
                                            Location Only
                                        </label>
                                            <br>
                                        <label for="account">
                                            <input type="radio" value="Account" name="associate_with[<?= $index ?>]"/>
                                            Entire Account
                                        </label>
                                    </span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    <? } ?>
                                </span>
                                <a class="j-removeAddlEntity">Remove</a>
                            </div>
                        <? } ?>
                    </fieldset>
                    <? if ($errors['join_legal_entity_names_id']) { ?>
                        <p class="error form-error"><?= $errors['join_legal_entity_names_id'] ?></p>
                    <? } ?>
                </div>
                <div class="form-actions">
                    <input type="button" class="btn-add" value="Add Another Additional Insured" id="AddLegalEntityName">
                </div>
                </div>
            </fieldset>

            <div class="callout">
                <b><?= SITE_NAME ?></b> can send two types of emails on your behalf:
                <ul style="margin-top:.5em;">
                    <li><b>Vendor Certificate of Insurance Requests</b> are sent to the vendor’s email address. The content of the email sent can be customized and previewed in the Vendor Certificate Requests section below or you can use the default content. Whichever content you use, the same notice will be sent for any Right Now, Before Expiration or After Expiration Requests.</li>
                    <li><b>Certificate Expiration Reminders</b> are sent to a member, or to an email address you specify. They cannot be customized.</li>
                </ul>
            </div>

            <fieldset class="j-toggle-parent">
                <div class="j-toggle-summary">
                <div class="form-field"><label><span>Days before expiration to send email</span></label><span id="summary_expiration_days"><?=$certificate['certificates_email_days']?></span></div>
                </div>
                <legend>Configure Email Timing <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                <h3>Right Now</h3>
                <div class="form-field">
                    <label>Vendor Certificate Request</label><label for="request_cert" class="checkbox-label">
                        <input
                            type="checkbox" class="input-checkbox" id="request_cert" name="request_cert"
                            value="request_cert" <?= ($_REQUEST['request_cert']) ? 'checked' : ''; ?>></input>
                        Send a Vendor Certificate Request right now
                                            </label>
                </div>
                <hr>

                <h3>Before Expiration</h3>
                <?= html::selectfield('Days before expiration to send email', arrays::values_as_keys(array(0, 15, 30, 45)), 'certificates_email_days', $certificate, array(), $errors) ?>

                <div class="form-field">
                    <label>Vendor Certificate Request</label><label for="certificates_remind_vendor" class="checkbox-label">
                        <input
                            type="checkbox" class="input-checkbox" id="certificates_remind_vendor" name="certificates_remind_vendor"
                            value="1" <?= ($certificate['certificates_remind_vendor']) ? 'checked' : ''; ?>></input>
                        Send a Vendor Certificate Request before expiration
                    </label>
                </div>
                <div class="form-field">
                    <label>Certificate Expiration Reminder</label><fieldset class="send-rem-check">
                        <div id="remind_before_expiry"></div>

                        <label for="certificates_remind_other_chk">
                            <?= html::checkbox('1', 'certificates_remind_other_chk', $certificate, array('id' => 'certificates_remind_other_chk')) ?>
                            Someone else:
                            <input type="text" name="certificates_remind_other" id="certificates_remind_other" value="<?= $certificate['certificates_remind_other'] ?>"/>
                        </label>
                    </fieldset>
                    <? if($errors['certificates_remind_other']) { ?>
                        <p class="error form-error"><?= $errors['certificates_remind_other'] ?></p>
                    <? } ?>
                    <? if($errors['certificates_reminder_email']) { ?>
                        <p class="error form-error"><?= $errors['certificates_reminder_email'] ?></p>
                    <? } ?>
                </div>
                <hr>
                <h3>After Expiration</h3>
                <?= html::selectfield('Send email weekly until<br>updated' . html::help_button(null, null, 'Select \'Yes\' to be sent a weekly email reminder until the expired certificate is updated.'),
                    array('1' => 'Yes', '0' => 'No'), 'certificates_remind_member', $certificate, array(), $errors) ?>

                <div class="form-field">
                    <label>Vendor Certificate Request</label><label for="certificates_remind_vendor_expired" class="checkbox-label">
                        <input
                            type="checkbox" class="input-checkbox" id="certificates_remind_vendor_expired" name="certificates_remind_vendor_expired"
                            value="1" <?= ($certificate['certificates_remind_vendor_expired']) ? 'checked' : ''; ?>></input>
                        Send a weekly Vendor Certificate Request after expiration
                    </label>
                </div>

                <div class="form-field">
                    <label>Certificate Expiration Reminder</label><fieldset class="send-rem-check">
                        <div id="remind_after_expiry"></div>
                       
                        <label for="certificates_remind_other_expired_chk">
                            <?= html::checkbox('1', 'certificates_remind_other_expired_chk', $certificate, array('id' => 'certificates_remind_other_expired_chk')) ?>
                            Someone else:
                            <input type="text" name="certificates_remind_other_expired" id="certificates_remind_other_expired" value="<?= $certificate['certificates_remind_other_expired'] ?>"/>
                        </label>
                    </fieldset>
                    <? if($errors['certificates_remind_other_expired']) { ?>
                        <p class="error form-error"><?= $errors['certificates_remind_other_expired'] ?></p>
                    <? } ?>
                    <? if($errors['certificates_reminder_expired_email']) { ?>
                        <p class="error form-error"><?= $errors['certificates_reminder_expired_email'] ?></p>
                    <? } ?>
            </fieldset>
            <? } ?>
             <fieldset class="j-toggle-parent">
                <legend>Certificate Files <a class="j-toggle-trigger"></a></legend>
                <div class="file-container j-toggle-content">
                <table id="uploaded-files" class="file-table">
                    <tr>
                        <th>Active</th>
                        <th>File Name</th>
                        <th>Date Uploaded</th>
                        <th>Actions</th>
                    </tr>
                    <? if ($certFiles) {
                        foreach ($certFiles as $file) { ?>
                            <?  $filename = $file['files_name'];
                            if (strlen($filename) > 50) {
                                $filename = substr($filename, 0, 40) . '...' . substr($filename, -7);
                            }
                            ?>
                            <tr>
                                <td>
                                    <? if($file['certificates_files_active'] == 1) { ?>
                                        &check; Yes
                                    <? } ?>
                                </td>
                                <td>
                                    <a href="download.php?certificates_files_id=<?= $file['certificates_files_id'] ?>&type=.<?= files::get_extension($file['files_name']) ?>"
                                       title="<?= $file['files_name'] ?>" target="_blank"><?= $filename ?></a>
                                </td>
                                <td>
                                    <? if($file['files_datetime']) { ?>
                                        <?= date('m/d/Y', strtotime($file['files_datetime'])) ?>
                                    <? } else { ?>
                                        Unknown
                                    <? } ?>
                                </td>
                                <td>
                                    <? if($file['certificates_files_active'] != 1) { ?>
                                        <a href="set-active.php?certificates_files_id=<?= $file['certificates_files_id'] ?>">Set Active</a> |
                                    <? } ?>
                                    <a href="del-file.php?certificates_files_id=<?= $file['certificates_files_id'] ?>&action=<?=$_REQUEST['action']?$_REQUEST['action']:''?>">Delete</a>
                                </td>
                            </tr>
                        <? } ?>
                    <? } else { ?>
                        <td></td>
                        <td><input type="file" name="certificates_file[]" class="j-file"></td>
                        <td></td>
                        <td><a href="#" onclick="deleteRow(event)">Delete</a></td>
                    <? } ?>
                </table>
                <a href="#" id="add_file" class="add-item add-file-link">Attach another certificate file</a>
                <? if ($errors['certificates_file']): ?>
                    <p class="errors"><?= $errors['certificates_file'] ?></p>
                <? endif ?>
            </div>
               
             </fieldset>
         <? if($_REQUEST['action'] != 'update'){ ?>
            <fieldset id="configureCertRequests" class="j-toggle-parent">
                <legend>Configure Vendor Certificate Requests <a class="j-toggle-trigger"></a></legend>
                <div class="j-toggle-content">
                <?= html::textfield('Email subject line:', $certificate, 'certificates_email_subject', array(), $errors) ?>

                <div class="form-field">
                    <label>Request certificates be sent to:</label>
                    <fieldset>
                        <?= html::checkbox('1', 'certificates_request_cert_to_email_chk', $certificate) ?>
                        <label for="certificates_request_cert_to_email_chk">
                            An email address
                        </label><br/>
                        <?= html::input($certificate, 'certificates_request_cert_to_email', array('style' => 'margin-left: 30px')) ?>
                        <?= html::errors('certificates_request_cert_to_email', $errors) ?>
                        <br>

                        <?= html::checkbox('1', 'certificates_request_cert_to_fax', $certificate) ?>
                        <label for="certificates_request_cert_to_fax">
                            The location, via fax
                        </label><br/>

                        <?= html::checkbox('1', 'certificates_request_cert_to_address', $certificate) ?>
                        <label for="certificates_request_cert_to_address">
                            The location, via postal mail
                        </label><br/>
                    </fieldset>
                </div>

                <div class="form-field">
                    <input type="hidden" name="template_id" id='template_id' value="<?=$certificate['join_certificates_email_templates_id']?>">
                    <label for="join_certificates_email_templates_id">Email Template:</label>
                    <?= html::select($templates, 'join_certificates_email_templates_id', $certificate,  $customTemplate ? array('disabled' => 'disabled') : array(), $errors); ?>
                </div>

                <div <?= $customTemplate ? "" : "style='display:none'" ?> id="email_template">
                    <p class="callout">
                        <img src="/images/dialog-warning.png" class="icon">
                        When customizing this form, please do not alter the text within the brackets { }. These fields are automatically populated with information from your data. Altering or deleting these fields will omit this important data from the vendor certificate request email.</p>
                    <textarea id="email_html" name="email_html" style="width:100%;height:280px;"><?= $templateHtml ?></textarea>
                    <div class="form-actions">
                        <input type="button" class="btn-ok" value="Done Editing" id="collapseCustomEmail">
                        <input type="button" class="btn-delete" value="Delete Customization" id="deleteCustomEmail">
                    </div>
                </div>

                <div class="form-actions">
                    <input type="button" value="Preview Email" id="previewEmail" >
                    <input <?= $customTemplate ? "style='display:none'" : "" ?> type="button" value="Customize Email" id="customizeEmail">
                </div>
                </div>
            </fieldset>
            <fieldset class="j-toggle-parent j-toggle-collapse-initial j-toggle-collapsed">
            <legend>Email History <a class="j-toggle-trigger"></a></legend>
            <div class="j-toggle-content">
              <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
        <tr>
            <th>To</th>
            <th>Subject</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
        <? foreach((array)$emails_history as $email): ?>
            <tr>
                <td><?= $email['sent_emails_email']?></td>
                <td><?= $email['sent_emails_desc'] ?></td>
                <td><?= date(DATE_FORMAT_FULL, strtotime($email['sent_emails_date'])) ?></td>
                <td>
                   <a id="preview_template" name="preview_template" onClick="PreviewTemplate(<?= $email['sent_emails_history_id']?>);">View Email</a>
                </td>
            </tr>
        <? endforeach; 
        if(empty($emails_history)){?>
        <tr>
            <td colspan="5">No emails sent</td>
        </tr>
        <?} ?>
    </table>  
    </div>
    </fieldset>
        <? } ?>

        <input type="hidden" name="action" value="<?= $_REQUEST['action'] == 'update' ? 'update' : 'edit' ?>" />
        <input type="hidden" name="certificates_id" id="certificates_id" value="<?= $certificate['certificates_id'] ?>" />
        <input type="hidden" id="default_template_id" value="<?= $default_template_id ?>" />
        <input type="hidden" id="customize_template" name="customize_template" value="<?= $customTemplate ? 1 : 0 ?>"/>
        <div class="form-actions">
            <input type="submit" id="save" value="<?= $certificate['certificates_id'] ? 'Save Certificate' : 'Add Certificate' ?>" class='btn-primary btn-ok'/>
        </div>
    </form>
</div>
    <script type="text/javascript" src="/js/clickout.js"></script>
    <script type="text/javascript" src="/js/MooCal.js"></script>
    <script type="text/javascript" src="/js/certificates.js?v=20180618"></script>

<? include VIEWS. '/members/footer.php' ?>
<script type="text/javascript" src="/library/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#email_html",
        menubar: "edit insert dyndata view format table tools",
        toolbar1: "dyndata undo redo | bold italic underline strikethrough | styleselect formatselect | forecolor backcolor removeformat | code",
        toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image | table | html",
        toolbar_items_size: 'small',
        plugins: "DynamicDBData table link image textcolor code paste",
        paste_data_images: true,
        convert_urls: false,
        images_upload_url: '/library/tiny_mce/ImageUpload.php',
        images_upload_base_path: '/',
        target_list: false,
        default_link_target: "_blank",
        link_title: false,
        DynDBD_MenuItems: <?= $dynemail->serialize_jscript(); ?>
    });
</script>