<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Certificates';
$page_keywords = '';
$page_description = '';

include VIEWS . '/members/header.php';
?>

<div id="content">
<h1 class="left">Certificate Request Email Template Management</h1>

<div class="page-options">
    <? if (ActiveMemberInfo::IsAccountMultiLocation() && !licensed_locations::get_licensed_locations_ids_for_member(ActiveMemberInfo::GetMemberId())): ?>
        <span style="text-decoration:underline" title="Please add a location first">Please add a location first</span> |
    <? else: ?>
        <a href="/members/my-documents/certificates/email_templates/edit.php" class="add-item">Add Email Template</a> |
    <? endif ?>
    <a href="/members/my-documents/certificates/index.php">Back To Certificates</a>
</div>

<div class="clear"></div>

<br/><br/>

    <form action="index.php" id="filter" method="get" style="margin-bottom:15px">
    <? if(sizeof($licensed_locations) > 1 ): ?>
        <b>Location:</b>
        <?= html::select_from_query($licensed_locations, 'licensed_locations_id', $filter_location_id, 'licensed_locations_name', 'All Locations') ?>
    <? endif ?>
    </form>

    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
    <tr>
        <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
          <th style="min-width:250px;"><?= strings::sort('licensed_locations_name', 'Location') ?></th>
        <? endif ?>
        <th style="width:100%;">Template Name</th>
        <th style="min-width:55px;">Actions</th>
    </tr>
    
        <? foreach($email_templates as $template) : ?>
        <? $perms = UserPermissions::UserObjectPermissions('certificates', $template); ?>
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <? if($template['join_licensed_locations_id'] == 0){
                    $location_name = 'All Locations';
                }else{
                    $location_name = $template['licensed_locations_name'];
                }
                ?>
                <td><?= strings::wordwrap($location_name, 9, '&shy;', true); ?></td>
            <? endif; ?>
            <td>
                <?= $template['join_certificates_id'] ? "(Custom Template)" : $template['certificates_email_templates_name']; ?>
                <?php if($template['certificates_email_templates_default']) { ?>(default)<?php } ?>
            </td>
            <? $permissions = emails_templates_permissions::get_for_location($template['join_licensed_locations_id']); ?>
            
            <td>
                <div class="actionMenu">
                    <ul>
                        <li><a name="preview_template"
                               onClick="PreviewTemplate(<?= $template['certificates_email_templates_id'] ?>, 'email');">Preview</a>
                        </li>
                        <? if (($permissions != null && $permissions['location_can_override_certificates_email'] == 1) || ActiveMemberInfo::_IsAccountAdmin()): ?>
                            <li><a name="edit_template"
                                   href="edit.php?email_edit_type=edit&certificates_email_templates_id=<?= $template['certificates_email_templates_id'] ?>">Edit</a>
                            </li>
                            <? if($template['join_licensed_locations_id'] == 0 && ActiveMemberInfo::_IsAccountAdmin() && !$template['certificates_email_templates_default']) { ?>
                                <li><a name="set_default" onclick = "setDefault(<?= $template['certificates_email_templates_id'] ?>)">Set as Default</a>
                                </li>
                            <? } ?>
                            <li><a name="delete_template" class="deletecheck" title="Any certificate currently using this template will update to default template. Are you sure you want to delete this?"
                                   href="delete.php?certificates_email_templates_id=<?= $template['certificates_email_templates_id'] ?>">Delete</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>    
    <tr>
        <td colspan="99"><?= $paging->get_html(); ?></td>
    </tr>
</table>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript">
function PreviewTemplate(certificates_email_templates_id)
{
    var reqDialog = new MooDialog.Request('/members/my-documents/certificates/email_templates/preview_popup.php?certificates_email_templates_id=' + certificates_email_templates_id, null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: 800,
                height: 500
            }
            
    });

    reqDialog.setRequestOptions({
        onRequest: function () {
            reqDialog.setContent('loading...');
        }
    }).open();

    return false;
}

window.addEvent('domready', function(){
    <? if($infobox): ?>
       $$('.page-desc').setStyle('display', 'block');
       $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
});

window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_certificates');
    });
});

window.addEvent('domready', function(){
        if($('licensed_locations_id')){
            $('licensed_locations_id').addEvent('change', function(){
                $('filter').submit();
            });
        }
    });
function setDefault(certificates_email_templates_id){
    var url = '/API/certificates/set_default_template.php?certificates_email_templates_id=' + certificates_email_templates_id;
    jQuery.ajax({url: url, success: function(result){
            if(result == 'Updated'){
               window.location.reload();
            }
        }
    })
}
</script>
<? include VIEWS . '/members/footer.php' ?>
