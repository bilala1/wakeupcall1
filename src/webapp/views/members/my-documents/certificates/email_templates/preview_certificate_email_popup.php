<div id="content" style="display:inline-block;min-width: 95%; min-height: 80%">
<?
    $vendor_id = $_REQUEST['join_vendors_id'];
    $vender_details = vendors::get_vendor($vendor_id);
    $licensed_location_id = $_REQUEST['join_licensed_locations_id'];
    $location_details = licensed_locations::get_by_id($licensed_location_id);
    include(SITE_PATH . '/' . MODELS . '/DynamicEmail/CertRequestEmail.php'); 
    $dynemail = new CertRequestDynamicEmail();
    $certificates_id = $_REQUEST['certificates_id']?$_REQUEST['certificates_id']:0;
    if($certificates_id){
        
        $template = certificates_email_templates::get_template_for_certificate($certificates_id);
    }
    $join_certificates_email_templates_id = $_REQUEST['join_certificates_email_templates_id'];
    
    if($_REQUEST['customize_template'] == 1 && $_REQUEST['email_html']){
        $template_html =  $_REQUEST['email_html'];
    }elseif($join_certificates_email_templates_id){
       $template = certificates_email_templates::get_by_id($join_certificates_email_templates_id);
       $template_html = $template['email_html'];
    }else{
        $template_html = emails_default_templates::get_certificate_request_template();
    }
             
   if($_REQUEST['certificates_include_additional_insured_request'] == 1){
      $certificate_legal_entity = array();
        $legal_entity_names_details = array();
        $j = 0; $k=0;
        for($i=0;$i< count($_REQUEST['join_legal_entity_names_id']);$i++){
            if($_REQUEST['join_legal_entity_names_id'][$i]== 'add'){
                $additional_insured[]=$_REQUEST['legal_entity_names_name'][$i];
            }else{
                $certificate_legal_entities = legal_entities::getById($_REQUEST['join_legal_entity_names_id'][$i]);
                $additional_insured[] = $certificate_legal_entities['legal_entity_names_name'];
            }

        } 
   }
    $certRequestModel = array(
        'vendors_name' => $vender_details['vendors_name'],
        'vendors_street' => $vender_details['vendors_street'],
        'loc_name' => $location_details['licensed_locations_name'],
        'certificates_coverages' => $_REQUEST['coverages_amount'],
        'certificates_coverage_other' => $_REQUEST['certificates_coverage_other'],
        'certificates_coverage_other_amount' => $_REQUEST['certificates_coverages_other_amount'],
        'members_fax' => $location_details['licensed_locations_fax'],
        'members_billing_addr1' => $location_details['licensed_locations_address'],
        'members_billing_city' => $location_details['licensed_locations_city'],
        'members_billing_state' => $location_details['licensed_locations_state'],
        'members_billing_zip' => $location_details['licensed_locations_zip'],
        'certificates_request_cert_to_email' => $_REQUEST['certificates_request_cert_to_email'],
        'certificates_request_cert_to_fax' => $_REQUEST['certificates_request_cert_to_fax'],
        'certificates_request_cert_to_address' => $_REQUEST['certificates_request_cert_to_address'],
        'certificates_include_additional_insured_request'=> $_REQUEST['certificates_include_additional_insured_request'],
        'certificates_default_location_name' => $_REQUEST['certificates_default_location_name'],
        'certificate_additional_insured' => $additional_insured
    );
    echo '<span class="none">';
    echo $dynemail->generate_html ($template_html, $certRequestModel);
    echo '</span>';
    
?>
</div>