<div id="content" style="display:inline-block;min-width: 95%; min-height: 80%">
<?
    include(SITE_PATH . '/' . MODELS . '/DynamicEmail/CertRequestEmail.php'); 
    $dynemail = new CertRequestDynamicEmail();
    
//    $licensed_locations_id = $_REQUEST['licensed_locations_id'] ? $_REQUEST['licensed_locations_id'] : 0;
//    $template = certificates_email_templates::get_best_fallback_for_location(ActiveMemberInfo::GetAccountId(), $licensed_locations_id);
    $certificates_email_templates_id = $_REQUEST['certificates_email_templates_id']?$_REQUEST['certificates_email_templates_id']:0;
    if($certificates_email_templates_id){
        $template = certificates_email_templates::get_template($certificates_email_templates_id);
    }

    $template_html = $_REQUEST['email_html'] ? $_REQUEST['email_html'] : 
                     ($template ? stripslashes($template['email_html']) : '');

    $certRequestModel = array(
        'vendors_name' => "Sample Vendor",
        'vendors_street' => "123 Vendor Street",
        'loc_name' => "Sample Location",
        'certificates_coverages' => "Sample Coverage 1, Sample Coverage 2",
        'certificates_coverage_other' => "",
        'members_fax' => "(123) 457-7890",
        'members_billing_addr1' => "500 Sample Avenue",
        'members_billing_addr2' => "P.O. Box 1234",
        'members_billing_city' => "Sample City",
        'members_billing_state' => "ST",
        'members_billing_zip' => "12345",
        'certificates_include_additional_insured_request' => "1",
        'certificates_default_location_name' => "Sample Management Group",
        'certificates_request_cert_to_email' => 'user@email.com',
        'certificates_request_cert_to_fax' => '1',
        'certificates_request_cert_to_address'=> '1'
    );

    echo '<span class="none">';
    echo $dynemail->generate_html (stripslashes($template['email_html']), $certRequestModel);
    echo '</span>';
    
?>
</div>