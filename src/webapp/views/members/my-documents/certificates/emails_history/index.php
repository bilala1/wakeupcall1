<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Certificates';
$page_keywords = '';
$page_description = '';

include VIEWS . '/members/header.php';
?>

<div id="content">
<div class="page-header">
    <h1 class="left">Certificate Email History</h1>

    <div class="page-options">
        <div class="page-dialog-background"></div>
        <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a> | 
        <a href="/members/my-documents/certificates/index.php">Back To Certificates</a>
    </div>

    <div class="clear"></div>
    <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '2' : '1' ?> filter-Email-History">
        <form action="index.php" id="filter" method="get" style="margin-bottom:15px">
            <div class="dialog-sections">
                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                        <div class="dialog-section">
                            <h3>Locations</h3>
                            <?= html::location_filter('certificates', $filter_location_id) ?>
                        </div>
                    <? } ?>
                <div class="dialog-section dates">

                        <label>From:</label><?= '<input style="width:100px" type="text" id="email_history_start_time" name="email_history_start_time" value=' . $startTime.'>' ?><br>
                        <label>To:</label><?= '<input style="width:100px" type="text" id="email_history_end_time" name="email_history_end_time" value=' . $endTime . '>' ?>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::hidden($model, 'clear_filter') ?>
                <?= html::submit("Clear Filter", array('id' => 'clear_button')) ?>
            </div>

            
        </form>
    </div>
</div>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
    <tr>
        <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
            <th style="min-width:250px;">Location</th>
        <? endif ?>
        <th><?= strings::sort('vendors_name', 'Vendor'); ?></th>
        <th>To</th>
        <th>Subject</th>
        <th style="min-width:100px;"><?= strings::sort('sent_emails_date', 'Date', null, null, null, 'DESC'); ?></th>
        <th>Status</th>
        <th style="min-width:55px;">Actions</th>
    </tr>
    
        <? foreach($emails as $email) : ?>
        <tr>
            <? $location = licensed_locations::get_by_id($email['join_licensed_locations_id']); ?>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <? if($email['join_licensed_locations_id'] == 0): ?>
                    <td>Corporate</td>
                <? else: ?>
                    <td><?= $location['licensed_locations_name']; ?></td>
                <? endif; ?>
            <? endif; ?>
            <td><?php
                if($email['vendors_name']) {
                    echo($email['vendors_name']);
                } else if($email['certificates_id']) {
                    echo('Vendor deleted');
                } else {
                    echo('Certificate deleted');
                } ?></td>
            <td><?= $email['sent_emails_email'] ?></td>
            <td><?= $email['sent_emails_desc'] ?></td>
            <td><?= $email['sent_emails_date'] ?></td>
            <td><?= Formatting::emailStatus($email) ?></td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <li><a id="preview_template" name="preview_template" onClick="PreviewTemplate(<?= $email['sent_emails_history_id']?>);">View Email</a></li>
                        <?php if($email['certificates_id']) { ?>
                            <li><a href="../edit.php?certificates_id=<?= $email['certificates_id'] ?>">View Certificate</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>    
    <tr>
        <td colspan="99"><?= $paging->get_html(); ?></td>
    </tr>
</table>
</div>

<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>
<script type="text/javascript">
function PreviewTemplate($email_id)
{
    var reqDialog = new MooDialog.Request('preview_popup.php?id=' + $email_id, null, {
            'class': 'MooDialog myDialog',
            autoOpen: false,
            size: {
                width: 800,
                height: 500
            }
    });

    reqDialog.setRequestOptions({
        onRequest: function () {
            reqDialog.setContent('loading...');
        }
    }).open();

    return false;
}

window.addEvent('domready', function(){
    <? if($infobox): ?>
       $$('.page-desc').setStyle('display', 'block');
       $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
    jQuery('#clear_button').bind('click', clear_filter);
    
});
function clear_filter(){ 
        jQuery('#clear_filter').val("clear_filter");
    }

window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_email_history');
    });
    
    new MooCal($('email_history_start_time'), {
        leaveempty: false,
        clickout: true
    });

    new MooCal($('email_history_end_time'), {
        leaveempty: false,
        clickout: true
    });
});

</script>
<? include VIEWS . '/members/footer.php' ?>
