<div>
    <?
    $db = mysqli_db::init();

    $certificates_files = $db->fetch_all('SELECT certificates_files_id, files_name FROM certificates_files INNER JOIN files on join_files_id = files_id WHERE join_certificates_id = ? ', array($_REQUEST['certificates_id']));
    ?>
    <ul>
        <? if(!$certificates_files): ?>
            <li><b>No Attachments</b></li>
        <? else: ?>
            <? foreach($certificates_files as $k => $file): ?>
                <li><a href="/members/my-documents/certificates/download.php?certificates_files_id=<?= $file['certificates_files_id'] ?>&type=.<?= files::get_extension($file['files_name']) ?>" target="_blank" title="<?= $file['files_name'] ?>"><?= $file['files_name'] ?></a><br /></li>
            <? endforeach; ?>
        <? endif; ?>
    </ul>
</div>
