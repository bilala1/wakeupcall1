<div>
    <h1>Activity Log for <?= $actions[0]['certificates_filename'] ?></h1>
    <ul>
        <? foreach($actions as $action): ?>
        <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['actions_datetime'])) ?>)</b> - <?= $action['actions_name'] ?></li>
        <? endforeach ?>
    </ul>
</div>