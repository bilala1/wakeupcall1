<?
    $page_title        = SITE_NAME;
    $page_name         = 'Tracking';
    $sub_name		   = 'Certificates';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<script type="text/javascript" src="/js/clickout.js"></script>
<script type="text/javascript" src="/js/MooCal.js"></script>

<script type="text/javascript">
window.addEvent('domready', function(){
    <? /* open or collapse infobox? */ ?>
    <? if($infobox): ?>
       $$('.page-desc').setStyle('display', 'block');
       $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
    });
</script>

<div id="content">
    <div class="page-header">
    <h1 class="left"><? if( $certificates_show_hidden == '1') { ?>Hidden <? } ?>Certificate Tracking</h1>
   <div class="page-options">
       <div class="page-dialog-background"></div>
        <a href="/members/my-documents/certificates/edit.php" class="add-item">Add Certificate</a> |
        <a href="/members/vendors/index.php">Manage Vendors</a> |
        <a href="/members/my-documents/certificates/legal_names/index.php">Manage Legal Entity Names</a> |
        <a href="/members/my-documents/certificates/email_templates/index.php">Email Templates</a> |
        <a href="emails_history/index.php">Email History</a> |
        <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a>
    </div>
    <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '3' : '2' ?>">
        <form action="index.php" id="filter" method="get">
            <div class="dialog-sections">

                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                    <div class="dialog-section">
                        <h3>Locations</h3>
                        <?= html::location_filter('certificates', $model['join_licensed_locations_id']) ?>
                    </div>
                <? } ?>
                <div class="dialog-section dates">
                    <h3>Expiration Date</h3>
                    <label>Quick Filter:</label><?= html::select(array('' => '') + array(
                            7 => 'Next 7 days',
                            14 => 'Next 14 days',
                            30 => 'Next 30 days'
                        ), 'quick_filter', '', array(), $errors); ?><br>
                    <label>From:</label><?= html::date($model, 'certificates_start_time', array()) ?><br>
                    <label>To:</label><?= html::date($model, 'certificates_end_time', array()) ?>
                </div>
                <div class="dialog-section">
                    <h3>Display Options</h3>
                    <label><?= html::radio(0, 'certificates_show_hidden', $model) ?> Show Active Certificates</label><br>
                    <label><?= html::radio(1, 'certificates_show_hidden', $model) ?> Show Hidden Certificates</label><br>
                    <h3>Threshold for pending Expiration</h3>
                    <?= html::select(array(
                            7 => '7 days',
                            14 => '14 days',
                            30 => '30 days'
                        ), 'pref', $userPrefs['user_prefs_cert_expire_lead_days'], array(), $errors); ?><br>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::hidden($model, 'clear_filter') ?>
                <?= html::submit("Clear Filter", array('id' => 'clear_button')) ?>
            </div>
        </form>
    </div>
    </div>
    <h3>Regular Operations</h3>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('vendors_name', 'Vendor Name') ?></th>
            <th style="width:135px;">Types of Coverage</th>
            <th><?= strings::sort('certificates_expire', 'Expiration') ?></th>
            <th>Active Certificate File</th>
            <th style="width:70px">Actions</th>
        </tr>
        <? foreach($regular_certs as $certificate): ?>
            <?
            $doHighlight = false;
            if($certificate['certificates_expire'] && $certificate['certificates_expire'] != '0000-00-00'){
                $days_diff = round((strtotime($certificate['certificates_expire'])- time())/(60 * 60 * 24));
                if($days_diff < $userPrefs['user_prefs_cert_expire_lead_days']) $doHighlight = true;
            } 
            if($certificate['certificates_expire'] != '0000-00-00'){
                $expired = strtotime($certificate['certificates_expire']) < time();
            }
            if($certificate['files_name'] == '' || $expired)$doHighlight = true;
            $perms = UserPermissions::UserObjectPermissions('certificates', $certificate); ?>
            <tr <?= ($doHighlight == true) ? 'class="notices"' : '' ?>>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= strings::wordwrap($certificate['licensed_locations_name'], 9, '&shy;', true); ?></td>
            <? endif ?>
            <td><a href="/members/vendors/edit.php?vendors_id=<?= $certificate['vendors_id'] ?>"><?= $certificate['vendors_name'] ?></a></td>
            <td><?= $certificate['certificates_coverages'] ?></td>
            <td <?= $expired?'class="error"' : ''?>><? if($certificate['certificates_expire'] && $certificate['certificates_expire'] != '0000-00-00') echo date('m/d/Y', strtotime($certificate['certificates_expire'])) ?></td>
            <td class="hyphenate <?= $certificate['files_name']?'':' error'?>">
            <?php if($certificate['files_name'] != ''): ?>
                <a target="_blank" href="/members/my-documents/certificates/download.php?certificates_files_id=<?= $certificate['certificates_files_id'] ?>&type=.<?= files::get_extension($certificate['files_name']) ?>"><?= $certificate['files_name'] ?></a>
            <? else: ?>
                <span>Need to Upload or Set File to 'Active'!</span>
            <? endif; ?>
            </td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if ($perms['edit']) { ?>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>">Edit</a>
                            </li>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>&action=update">Update</a>
                            </li>
                        <? } ?>
                        <li><a href="#" class="files" id="files_<?= $certificate['certificates_id'] ?>">Files</a></li>
                        <li><a href="#" class="history" id="history_<?= $certificate['certificates_id'] ?>">History</a>
                        </li>
                        <?php if($certificate['has_emails']) { ?>
                            <li><a href="#" class="emailHistory" id="emailHistory_<?= $certificate['certificates_id'] ?>">Email History</a></li>
                        <?php } ?>
                        <? if ($perms['delete']): ?>
                            <li>
                                <a href="/members/my-documents/certificates/delete.php?certificates_id=<?= $certificate['certificates_id'] ?>"
                                   class="deletecheck">Delete</a>
                            </li>
                        <? endif ?>
                        <? if ($perms['edit']) { ?>
                            <? if ($certificate['certificates_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="6"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
    <br />
    <h3>Special Projects</h3>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('vendors_name', 'Vendor Name') ?></th>
            <th><?= strings::sort('certificates_project_name', 'Project Name') ?></th>
            <th>Types of Coverage</th>
            <th><?= strings::sort('certificates_expire', 'Expiration') ?></th>
            <th>Active Certificate File</th>
            <th style="width:70px">Actions</th>
        </tr>
        <? foreach($special_certs as $certificate): ?>
        <?  $doHighlight = false;
            if($certificate['certificates_expire'] && $certificate['certificates_expire'] != '0000-00-00'){
                $days_diff = round((strtotime($certificate['certificates_expire'])- time())/(60 * 60 * 24));
                if($days_diff < $userPrefs['user_prefs_cert_expire_lead_days']) $doHighlight = true;
            }
            if($certificate['certificates_expire'] != '0000-00-00'){
                $expired = strtotime($certificate['certificates_expire']) < time();
            }
            if($certificate['files_name'] == '' || $expired) $doHighlight = true;
            $perms = UserPermissions::UserObjectPermissions('certificates', $certificate); ?>
        <tr <?= ($doHighlight == true) ? 'class="notices"' : '' ?>>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= strings::wordwrap($certificate['licensed_locations_name'], 9, '&shy;', true); ?></td>
            <? endif ?>
            <td><a href="/members/vendors/edit.php?vendors_id=<?= $certificate['vendors_id'] ?>"><?= $certificate['vendors_name'] ?></a></td>
            <td><?= $certificate['certificates_project_name'] ?></td>
            <td><?= implode(', ', explode(',', $certificate['certificates_coverages'])) ?></td>
            <td <?= $expired?'class="error"' : ''?>><? if($certificate['certificates_expire'] && $certificate['certificates_expire'] != '0000-00-00') echo date('m/d/Y', strtotime($certificate['certificates_expire'])) ?></td>
            <td class="hyphenate <?= $certificate['files_name']?'':' error'?>">
                <? if($certificate['files_name'] != ''): ?>
                    <a target="_blank" href="/members/my-documents/certificates/download.php?certificates_files_id=<?= $certificate['certificates_files_id'] ?>&type=.<?= files::get_extension($certificate['files_name']) ?>"><?= $certificate['files_name'] ?></a>
                <? else: ?>
                    <span>Need to Upload or Set File to 'Active'!</span>
                <? endif; ?>
            </td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if ($perms['edit']) { ?>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>">Edit</a>
                            </li>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>&action=update">Update</a>
                            </li>
                        <? } ?>
                        <li><a href="#" class="files" id="files_<?= $certificate['certificates_id'] ?>">Files</a></li>
                        <li><a href="#" class="history" id="history_<?= $certificate['certificates_id'] ?>">History</a>
                        </li>
                        <?php if($cert['has_emails']) { ?>
                            <li><a href="#" class="emailHistory" id="emailHistory_<?= $cert['certificates_id'] ?>">Email History</a></li>
                        <?php } ?>
                        <? if ($perms['delete']): ?>
                            <li>
                                <a href="/members/my-documents/certificates/delete.php?certificates_id=<?= $certificate['certificates_id'] ?>"
                                   class="deletecheck">Delete</a>
                            </li>
                        <? endif ?>
                        <? if ($perms['edit']) { ?>
                            <? if ($certificate['certificates_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
        </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="6"><?= $paging->get_html(); ?></td>
        </tr>
    </table>
</div>
<script type="text/javascript" src="/js/certificates_list.js?v=20180618"></script>
<? include VIEWS. '/members/footer.php' ?>
