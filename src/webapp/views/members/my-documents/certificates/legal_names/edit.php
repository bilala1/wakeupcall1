<?php
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Certificates';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>
<div id="content">
    <h1 class="left"><?= $_REQUEST['legal_entity_names_id'] ? 'Edit' : 'Add' ?> Legal Entity Name</h1>   
    <div class="page-options"><a href="/members/my-documents/certificates/legal_names/index.php">Return to Legal Entity List</a></div>
    <br clear="all"/>  
    <form action="/members/my-documents/certificates/legal_names/edit.php" method="POST">
        <? if(!$legal_entity_names_id) { ?>
            <?= html::selectfield_from_query('Location:', $licensed_locations, 'join_locations_id', $legal_entities,
                'licensed_locations_name', 'Please Select', array(), $errors);
            }else{?>
                <div class="form-field">
                    <label><span>Location:</span></label>
                    <? if($legal_entities['join_locations_id'] == 0){ ?>
                    All Locations
                    <? }else{ ?>
                    <?= $legal_entities['licensed_locations_name']; } ?>
                    <?= html::hidden($legal_entities, 'join_licensed_locations_id') ?>
                </div>
            <? } ?>

        <?= html::textfield('Legal Entity name:'. html::help_button(null, null, "Changing this legal entity name will update certificates in ". SITE_NAME ." to use the new entity name. If you need a new certificate of insurance containing the new legal entity name, you will need to request it from your vendor"),
            $legal_entities, 'legal_entity_names_name', array('maxlength' => 255), $errors) ?>
        <input type='hidden' name='legal_entity_names_id' location='legal_entity_names_id' value='<?= $legal_entity_names_id?>'/>
        <div class="form-actions">
            <input type="submit" id="save" value="Save" class="btn-primary btn-ok"/>
        </div>
    </form>
</div>

<? include VIEWS . '/members/footer.php' ?>