<?
$page_title = SITE_NAME;
$page_name         = 'Tracking';
$sub_name		   = 'Certificates';
$page_keywords = '';
$page_description = '';

include VIEWS . '/members/header.php';
?>

<div id="content">
<h1 class="left"><?= $_SESSION['legal_entity_show_hidden'] ? "Hidden " : "" ?> Legal Entity Names</h1>

<div class="page-options">
    <? if (ActiveMemberInfo::IsAccountMultiLocation() && !licensed_locations::get_licensed_locations_ids_for_member(ActiveMemberInfo::GetMemberId())): ?>
        <span style="text-decoration:underline" title="Please add a location first">Please add a location first</span> |
    <? endif ?>
        <a href="/members/my-documents/certificates/legal_names/edit.php">Add Legal Entity Name</a> |
        <? if($_SESSION['legal_entity_show_hidden'] != 1) { ?>
            <a href="/members/my-documents/certificates/legal_names/index.php?legal_entity_show_hidden=1">View Hidden Legal Entity Names</a> |
        <? } else { ?>
            <a href="/members/my-documents/certificates/legal_names/index.php?legal_entity_show_hidden=0">View Active Legal Entity Names</a> |
        <? } ?>
    <a href="/members/my-documents/certificates/index.php">Back To Certificates</a>
</div>

<div class="clear"></div>

  <table width="90%" align="center" cellpadding="3" cellspacing="1" border="0" class="clear">
    <tr>
        <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
            <th style="min-width:250px;">Location</th>
        <? endif ?>
        <th style="width:100%;">Legal Entity Name</th>
        <th style="min-width:55px;">Actions</th>
    </tr>
    <?php foreach($legal_entities as $legal_entity){?>
    <? $perms = UserPermissions::UserObjectPermissions('certificates', $legal_entity);?>
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <? if($legal_entity['join_locations_id'] == 0): ?>
                    <td>All Locations</td>
                <? else: ?>
                    <td><?= $legal_entity['licensed_locations_name']; ?></td>
                <? endif; ?>
            <? endif; ?>
            <td>
                <?= $legal_entity['legal_entity_names_name'] ? $legal_entity['legal_entity_names_name'] : ""; ?>
                
            </td>
            <td>
                <div class="actionMenu">
                    <ul>
                        
                        <? if ( ($perms != null && $perms['edit'] == 1) || ActiveMemberInfo::_IsAccountAdmin()): ?>
                            <li><a name="edit_entity"
                                   href="edit.php?legal_entity_names_id=<?= $legal_entity['legal_entity_names_id'] ?>">Edit</a>
                            </li>
                        <? endif; ?> 
                        <? if ( ($perms != null && $perms['delete'] == 1) || ActiveMemberInfo::_IsAccountAdmin()): ?>
                            <li><a 
                                   href="delete.php?legal_entity_names_id=<?= $legal_entity['legal_entity_names_id'] ?>" 
                                   class="j-delete"
                                   data-numcerts="<?= $legal_entity['certificates_count'] ?>"
                                   data-legal_entity_names_id="<?= $legal_entity['legal_entity_names_id'] ?>"
                                   title="Are you sure you want to delete this Legal entity name?">Delete</a>
                            </li>
                        <? endif; ?>
                        <? if ($legal_entity['legal_entity_names_hidden'] <> 1): ?>
                            <li>
                                <a href="hide.php?legal_entity_names_id=<?= $legal_entity['legal_entity_names_id'] ?>&hide_entity_name=1">Hide</a>
                            </li>
                        <? else: ?>
                            <li>
                                <a href="hide.php?legal_entity_names_id=<?= $legal_entity['legal_entity_names_id'] ?>&hide_entity_name=0">Un-Hide</a>
                            </li>
                        <? endif ?>
                    </ul>
                </div>
            </td>
        </tr>
    <?php }?>
  </table>

</div>
<div class="template hidden" id="delete-with-certs">
    <div>
        <p>This legal entity name is associated with one or more certificates. Deleting it will remove it as an additional insured from
            all certificates.</p>
        <p>If you don't want the legal entity name to appear in this list any more, consider hiding it.</p>
        <div class="options">
            <button class="j-cancel btn-primary">Cancel</button>
            <button class="j-hide btn-primary btn-ok">Hide Legal Entity Name</button>
            <button class="j-delete btn-primary  btn-primary btn-delete">Delete Legal Entity Name</button>
        </div>
    </div>
</div>
<script src="/js/legal_entity_names.js?v=20180618"></script>
<script type="text/javascript">
 window.addEvent('domready', function(){
    <? if($infobox): ?>
       $$('.page-desc').setStyle('display', 'block');
       $$('.info_text').set('html','Hide Text Box');
    <? endif; ?>
});
window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_certificates');
    });
});

</script>

<? include VIEWS . '/members/footer.php' ?>