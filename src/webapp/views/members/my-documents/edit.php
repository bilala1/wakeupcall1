<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
    $sub_name		   = 'My Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
    
<div id="content">
    <h1 class="left"><?= $_REQUEST['documents_id'] ? 'Edit' : 'Add' ?> File</h1>
    <div class="page-options"><a href="/members/my-documents/index.php">Return to Files List</a></div><br clear="all" />
    <br />
    <form id="form" action="edit.php" method="post" enctype="multipart/form-data">
        
        <?= html::selectfield_from_query('Folder', $member_categories, 'join_members_library_categories_id', $document, 'members_library_categories_name', array(0 => '-none-'), array(), $errors) ?>
        <? if(UserPermissions::UserCanAccessScreen('shareDocuments')): ?>
            <?= html::checkboxfield('Shared Document:', 1, 'documents_is_corporate_shared', $document) ?>
           <div id="show_location" style="display: none;">
            <?= html::location_picker($document, $errors, 'members_x_documents',false); ?>
                <? if(!ActiveMemberInfo::IsUserMultiLocation()) {
                    $locationid = ActiveMemberInfo::SingleLocationUserLocationId();
                   echo "<input type='hidden' name='single_location_id' id='single_location_id' value=$locationid >"; 
            }?>
            </div>
            <?if(ActiveMemberInfo::IsParentAccount()) { ?>
                <?= html::checkboxfield('Shared to Child Accounts:' . html::help_button(null, null,
                        'Child accounts are accounts that have a \'Parent\' account who pays for their WAKEUP CALL Membership, '.
                         'and who can manage aspects of the membership on behalf of the Child account.'), 1, 'documents_share_to_child', $document) ?>
            <? } ?>
        <? endif ?>
        
        <div>
            <? if($document['files_name']): ?>
            <a target="_blank" href="/members/documents/download.php?documents_id=<?= $document['documents_id'] ?>#.<?= urlencode($document['files_name']) ?>"><?= $document['files_name'] ?></a><br />
            <? endif ?>
            
            <?= html::textfield('File:', null, 'documents_file', array('type' => 'file','onChange' => 'setTitle()'), $errors) ?>
            <?= html::textfield('Title:', $document['documents_title'], 'documents_title', array('maxlength' => 255), $errors) ?>
        
        </div>

        <input type="hidden" value="<?= urlencode($_REQUEST['redirect']);?>" name="redirect" />
        <input type="hidden" name="documents_id" value="<?= $_REQUEST['documents_id'] ?>" />
        <div class="form-actions">
            <?= html::input(($document ? 'Save' : 'Add') . ' File', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>
<script type="text/javascript" src="/js/my_documents.js"></script>
<script type="text/javascript">
    function setTitle() {
        if($('documents_title').get('value') == '') {
            var str = $('documents_file').get('value');
            var str_array = str.split("\\");
            var title = str_array.pop();
            var extensionIndex = title.lastIndexOf(".");
            if(extensionIndex > 0) {
                title = title.substring(0, extensionIndex);
            }
            $('documents_title').set('value',title);
        } 
    }
</script>

<? include(VIEWS . '/admin/footer.php'); ?>
