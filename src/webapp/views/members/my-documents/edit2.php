<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
    $sub_name		   = 'My Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
    
<div id="content">
    <a href="/members/my-documents/index.php" class="right">Return to Files List</a>
    <h1><?= $_REQUEST['documents_id'] ? 'Edit' : 'Add' ?> File</h1>
    <br />
    <form id="form" action="edit2.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="documents_id" value="<?= $_REQUEST['documents_id'] ?>" />
        <label>Title:</label><?= $document['documents_title'] ?>
        
        <?= html::selectfield_from_query('Folder', $member_categories, 'join_members_library_categories_id', $document, 'members_library_categories_name', array(0 => '-none-'), array(), $errors) ?>
        <? if(UserPermissions::UserCanAccessScreen('shareDocuments')): ?>
            <?= html::checkboxfield('Shared Document:', 1, 'documents_is_corporate_shared', $document) ?>
        <div id="show_location" style="display: none;">
            <?= html::location_picker($document, $errors, 'members_x_documents',false); ?>
                <? if(!ActiveMemberInfo::IsUserMultiLocation()) {
                    $locationid = ActiveMemberInfo::SingleLocationUserLocationId();
                   echo "<input type='hidden' name='single_location_id' id='single_location_id' value=$locationid >"; 
            }?>
        </div>
        
            <?if(ActiveMemberInfo::IsParentAccount()) { ?>
                <?= html::checkboxfield('Shared to Child Accounts:' . html::help_button(null, null,
                        'Child accounts are accounts that have a \'Parent\' account who pays for their WAKEUP CALL Membership, '.
                        'and who can manage aspects of the membership on behalf of the Child account.'), 1, 'documents_share_to_child', $document) ?>
            <? } ?>
        <? endif ?>
        <div>
            <? if((members::get_members_status(ActiveMemberInfo::GetMemberId())== 'free') && ((int)$document['documents_freetrial']==1)): ?>
                <? if($document['documents_file']): ?>
                <a target="_blank" href="/members/documents/download.php?documents_id=<?= $document['documents_id'] ?>#.<?= urlencode($document['documents_filename']) ?>"><?= $document['documents_filename'] ?></a><br />
                <? endif ?>
            <? endif; ?>
        </div>
        <input type="hidden" value="<?= urlencode($_REQUEST['redirect']);?>" name="redirect" />
        <div class="form-actions">
            <?= html::input(($document ? 'Save' : 'Add') . ' File', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>
<script type="text/javascript" src="/js/my_documents.js"></script>
<? include(VIEWS . '/admin/footer.php'); ?>
