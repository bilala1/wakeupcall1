<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
    $sub_name		   = 'My Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <h1 style="float: left;"><?= $_REQUEST['members_library_categories_id'] ? 'Edit' : 'Add' ?> Folder</h1>
    <div class="page-options"><a href="/members/my-documents/index.php">Return to Files List</a></div><br clear="all" />
    <br />
    <form action="/members/my-documents/folders/edit.php" method="post">
        <input type="hidden" name="members_library_categories_id" value="<?= $folder['members_library_categories_id'] ?>" />
        
        <?= html::textfield('Folder Name:', $folder['members_library_categories_name'], 'members_library_categories_name', array('maxlength' => 255), $errors) ?>

        <div class="form-actions">
            <?= html::input(($folder ? 'Save' : 'Add') . ' Folder', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>
</div>

<? include VIEWS. '/members/footer.php' ?>
