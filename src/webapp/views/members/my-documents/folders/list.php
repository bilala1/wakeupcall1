<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
    $sub_name		   = 'My Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<link href="/css/iconize.css" rel="stylesheet" type="text/css" />

<div id="content">
    <a href="edit.php" class="right">Create Folder</a>
    <h1>My Folders</h1>
    
    <table cellspacing="0" cellpadding="3" style="width:100%">
        <tr>
            <th><?= strings::sort('members_library_categories_name', 'Name') ?></th>
            <th style="width:100px">Actions</th>
        </tr>
        <? foreach($folders as $folder): ?>
        <tr>
            <td><?= $folder['members_library_categories_name'] ?></td>
            <td>
                <a href="edit.php?members_library_categories_id=<?= $folder['members_library_categories_id'] ?>">Edit</a> |
                <a href="delete.php?members_library_categories_id=<?= $folder['members_library_categories_id'] ?>" class="deletecheck">Delete</a>
            </td>
        </tr>
        <? endforeach ?>
    </table>
</div>

<? include VIEWS. '/members/footer.php' ?>
