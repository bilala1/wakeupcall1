<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
	$sub_name		   = 'My Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <h1 class="left">My Files</h1>
    <div class="page-options">
        <a href="edit.php" class="add-item">Add File</a> |
        <a href="folders/edit.php" class="add-item">Add Folder</a> |
        <form action="/members/search.php" method="get" style="display:inline">
                <input type="text" class="text"	name="q" style="width:150px;" placeholder="Search"/>
                <input type="hidden" name="type" value="mydocs" />
                <input class="btn" type="submit" value="Search" />
        </form>
    </div>
    <br clear="all" />
    <hr>
    <? if(!$folders): ?>
        <p style="color:gray">You currently have no files listed.</p>
    <? else: ?>
        <? foreach($folders as $id =>$folder): ?>
		<? if ($id != 0): ?>
		<span class="right">
			<a href="folders/edit.php?members_library_categories_id=<?=$id?>">Edit Folder</a> |
			<a class="deletecheck" href="folders/delete.php?members_library_categories_id=<?=$id?>">Delete Folder</a>
		</span>
		<? endif ?>
        <div  class="j-toggle-parent">
        <h2 style="margin-bottom:5px"><?= htmlentities($folder['members_library_categories_name']) ?: 'General' ?>:<a class="j-toggle-trigger"></a></h2>
        <div class="j-toggle-content">
        <table cellspacing="0" cellpadding="3" style="width:100%; margin-bottom:25px">
            <tr>
                <th width="40%">Name</th>
                <th width="45%">Download/View</th>
                <? if(UserPermissions::UserCanAccessScreen('shareDocuments')): ?>
                    <th width="20%">Shared Document?</th>
                <? endif ?>
                <th  width="15%">Actions</th>
            </tr>
            <? foreach($folder['files'] as $document): ?>
            <tr>
                <td><?= $document['documents_title'] ?></td>
                <td>
                    <? if (members::get_members_status(ActiveMemberInfo::GetMemberId()) == 'free' && ((int)$document['documents_freetrial'] == 0)): ?>
                        <i>Restricted to full members.</i>
                    <? else:
                        $text = 'Download/View';
                        $target = '_blank';
                        switch ($document['documents_format']) {
                            case 'url':
                                ?>
                                <a target="_blank"
                                   class="docLibrary-format-<?= $document['documents_format'] ?>"
                                   href="<?= $document['documents_filename'] ?>">Open Link</a>
                                <?
                                break;
                            case 'hr360':
                                ?>
                                <?= html::createHrLink('View on HR360', $document['documents_file']) ?>
                                <?
                                break;
                            case 'file':
                            default:
                                $url = '/members/documents/download.php?documents_id=' . $document['documents_id'] .
                                    '&type=.' . files::get_extension($document['files_name']); ?>
                                <a target="_blank"
                                   class="docLibrary-format-<?= $document['documents_format'] ?>"
                                   href="<?= $url ?>">Download/View</a>
                                <?
                                break;
                        }
                    endif; ?>
                </td>
                <? if(UserPermissions::UserCanAccessScreen('shareDocuments') && !ActiveMemberInfo::IsUserMultiLocation()){ ?>
                    <td>
                        <?= html::checkbox(1, 'edit_documents_is_corporate_shared[' . $document['documents_id'] . ']', $document['documents_is_corporate_shared'], array("data-docId" => $document['documents_id'])) ?>
                    </td>
                <? }
                else if(UserPermissions::UserCanAccessScreen('shareDocuments') && ActiveMemberInfo::IsUserMultiLocation()){ ?>
                    <td><? if($document['documents_is_corporate_shared'] == 1) {
                            if ($document['members_x_documents_all_locations'] == 1) {
                                $location = 'All Locations';
                            } else {
                                $location_ids = myDocuments::get_locations($document['members_x_documents_id']);
                                $location = licensed_locations::getLocationNames($location_ids);
                            }

                            echo $location;
                        }
                        ?></td>
                <? }?>
                    
                <td>
                    <div class="actionMenu">
                        <ul>
                            <? if (in_array($document['documents_type'], array('personal', 'submitted'))): ?>
                                <li><a href="edit.php?documents_id=<?= $document['documents_id'] ?>">Edit</a>
                            <? else: ?>
                                <li><a href="edit2.php?documents_id=<?= $document['documents_id'] ?>">Edit</a></li>
                            <? endif ?>
                            <li><a <?= in_array($document['documents_type'],
                                    array('personal', 'submitted')) ? 'class="deletecheck"' : '' ?>
                                    href="/members/documents/remove-document.php?documents_id=<?= $document['documents_id'] ?>"><?= in_array($document['documents_type'],
                                        array('personal', 'submitted')) ? 'Delete' : 'Remove' ?></a></li>

                        </ul>
                    </div>
                </td>
            </tr>
            <? endforeach ?>
            
        </table>
         </div>
       </div>
        <? endforeach ?>
    <? endif ?>
</div>

<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>

    var corpDocTimeouts = {};

    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_files');
        });

        $$('input[type="checkbox"]').addEvent('change', function(){
            var input = $(this),
                id = input.get('data-docId');

            var newEl = new Element('span', {html: 'Saving...', style: 'padding-left:10px;'});
            newEl.inject(input, 'after');

            if(corpDocTimeouts[id]) {
                clearTimeout(corpDocTimeouts[id]);
            }

            corpDocTimeouts[id] = setTimeout(function() {
                saveCorpDoc(input, id, newEl);
            }, 1000);
        });

        function saveCorpDoc(input, id, elToDestroy) {
            var value = input.get('checked') ? 1 : 0,
                args = {ajax: 'shared_doc'},
                flashMessage = function(config) {
                    elToDestroy.destroy();
                    var newEl = new Element('span', config);
                    newEl.inject(input, 'after');
                    setTimeout(function() {
                        newEl.fade('out');
                    }, 2000);
                    setTimeout(function() {
                        newEl.destroy();
                    }, 2500);
                };

            args['edit_documents_is_corporate_shared[' + id + ']'] = value;

            var request = new Request.JSON({
                url:'index.php',
                method:'post',
                onSuccess:function (json) {
                    flashMessage({html: 'Saved', style: 'padding-left:10px;'});
                },
                onError:function (text) {
                    flashMessage({html: 'An error occurred', style: 'color: red; padding-left:10px;'});
                }
            });

            request.post(args);
        };
    })
</script>

<? include VIEWS. '/members/footer.php' ?>
