<?
    $page_title        = SITE_NAME;
    $page_name         = 'Files';
	$sub_name		   = 'Shared Files';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">    <h1 class="left">Shared Files</h1>
    <div class="page-options"></div>
    <br clear="all" />
    <? if(!$folders): ?>
        <p style="color:gray">You currently have no files listed.</p>
    <? else: ?>
        <? foreach($folders as $id =>$folder): ?>
        <div  class="j-toggle-parent">
        <h2 style="margin-bottom:5px"><?= $id ?: 'General' ?>: <a class="j-toggle-trigger"></a></h2>
        <div class="j-toggle-content">
        <table cellspacing="0" cellpadding="3" style="width:100%; margin-bottom:25px">
            <tr>
                <th width="40%">Name</th>
                <? if(ActiveMemberInfo::_IsAccountAdmin()): ?>
                    <th>Shared By</th>
                <? endif ?>
                <th width="45%">Download/View</th>
            </tr>
            <? foreach($folder as $document): ?>
            <tr>
                <td><?= $document['documents_title'] ?></td>
                <? if(ActiveMemberInfo::_IsAccountAdmin()): ?>
                    <td><?= $document['members_firstname'] . ' ' . $document['members_lastname'] ?></td>
                <? endif ?>
                <td>
                    <? if (members::get_members_status(ActiveMemberInfo::GetMemberId()) == 'free' && ((int)$document['documents_freetrial'] == 0)): ?>
                        <i>Restricted to full members.</i>
                    <? else:
                        $text = 'Download/View';
                        $target = '_blank';
                        switch ($document['documents_format']) {
                            case 'url':
                                ?>
                                <a target="_blank"
                                   class="docLibrary-format-<?= $document['documents_format'] ?>"
                                   href="<?= $document['documents_filename'] ?>">Open Link</a>
                                <?
                                break;
                            case 'hr360':
                                ?>
                                <?= html::createHrLink('View on HR360', $document['documents_file']) ?>
                                <?
                                break;
                            case 'file':
                            default:
                                $url = '/members/documents/download.php?documents_id=' . $document['documents_id'] .
                                    '&type=.' . files::get_extension($document['files_name']); ?>
                                <a target="_blank"
                                   class="docLibrary-format-<?= $document['documents_format'] ?>"
                                   href="<?= $url ?>">Download/View</a>
                                <?
                                break;
                        }
                    endif; ?>
                </td>
            </tr>
            <? endforeach ?>
        </table>
        </div>
        </div>
        <? endforeach ?>
    <? endif ?>
</div>

<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>    
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_my_files');
        });
    })
</script>

<? include VIEWS. '/members/footer.php' ?>
