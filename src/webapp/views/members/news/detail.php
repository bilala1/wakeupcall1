<?
/*    $page_title        = html::filter($article['page_title']) . ' < ' . html::filter($article_group['name']) . ' NEWS | ' . SITE_NAME;*/
    $page_title        = SITE_NAME;
    $page_name         = 'News';
    $sub_name          = 'WAKEUP CALL News';
    $page_keywords     = $article['page_keywords'];
    $page_description  = $article['page_description'];
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <div id="blog-body">
        <h1 class="blog-title"><?= $article['articles_title'] ?></h1>
        <strong><?= $article['articles_subtitle'] ?></strong>

        <? if($article['articles_byline']): ?>
            <p class="byline"><?= $article['articles_byline'] ?></p>
        <? endif ?>
        <? if($article['articles_newsdate']): ?>
            <p class="feature-date"><?= $article['articles_newsdate'] ?></p>
        <? endif ?>
        <?= $article['articles_body'] ?><br />
    </div>
</div>

<? include VIEWS. '/members/footer.php' ?>
