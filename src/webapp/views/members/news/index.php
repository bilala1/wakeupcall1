<?
    $page_title        = SITE_NAME;
    $page_name         = 'News';
    $sub_name          = 'HR News';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
        <?= SITE_NAME ?> partnered with multiple sources of news specifically aimed at the hospitality industry as well as HR and safety news.
        Please keep in mind some of these news links will open another browser off of the <?= SITE_NAME ?> site.
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <h1>HR News</h1>

	<? foreach ($items as $item): ?>
		<div class="divider">
			<?
			$source = explode('/', $item['link']);
			$source = $source[2];
			?>
			<h3 class="title"><a href="<?= html::xss($item['link']) ?>" target="_blank"><?= html::xss($item['title']) ?></a>
			</h3>

			<p>
				<? if ($item['type'] !== 'smartbrief'): ?>
				<?= $item['description'] ?>
				<? endif ?>
			</p>
			&nbsp;&nbsp; <span style="color: #999999;">Source: <?= html::xss($source) ?>
			&nbsp;&nbsp;&nbsp;published <?= date(DATE_FORMAT, strtotime($item['pubDate'])) ?></span>
		</div>
	<? endforeach ?>
</div>
<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_news');
        });
    })
</script>

<? include VIEWS. '/members/footer.php' ?>
