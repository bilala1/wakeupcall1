<?
    $page_title        = SITE_NAME;
    $page_name         = 'News';
    $sub_name          = 'Hospitality News';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
<!--        <?= SITE_NAME ?> partnered with multiple sources of news specifically aimed at the hospitality industry as well as HR and safety news.
        Please keep in mind some of these news links will open another browser window off of the <?= SITE_NAME ?> site. -->
        
        <?= SITE_NAME ?> partnered with multiple sources of news specifically aimed at the hospitality industry along with HR 
        and safety news sources. <em>Please keep in mind that some of these news links will open another browser off of the <?= SITE_NAME ?> site.</em>
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
	<form name="date_form" id="date_form" action="rss-news.php" method="get">
		<select name="date" class="right" onchange="document.getElementById('date_form').submit()">
			<? foreach($date_list as $date => $count): ?>
				<option value="<?=$date?>" <?= $date == $show_date ? 'selected="selected"' : '' ?>>
					<?= ($date == $today ? 'Today' : date('m/d/Y', strtotime($date))) . " ({$count})"?>
				</option>
			<? endforeach ?>
		</select>
	</form>
    <h1>Hospitality News</h1>
    <? $counter = 1; ?>
	<? foreach($items as $item): ?>
   		<div class="divider">
   		    <?
   		        $source = explode('/',$item['link']);
   		        $source = $source[2];
   		    ?>
            <h3 class="title"><a href="<?= html::xss($item['link']) ?>" target="_blank"><?= html::xss($item['title']) ?></a></h3>
            <p>
			<? if ($item['type'] !== 'smartbrief'): ?>
				<?= $item['description'] ?>
			<? endif ?>
			</p>
            &nbsp;&nbsp; <span style="color: #999999;">Source: <?= html::xss($source) ?> &nbsp;&nbsp;&nbsp;published <?= date(DATE_FORMAT, strtotime($item['pubDate'])) ?></span>
    	</div>
    	<? if($counter > 1 && $counter % 10 == 0): ?>
    	<a style="font-size:12pt;color:#56833e;float:right" href="#news_top"><img src="<?= FULLURL . '/images/arrow.jpg'?>" border="0" />Back to Top</a><br /><br /><br />
    	<? endif ?>
    <? $counter++; endforeach ?>
    <? if($count % 10 != 1): ?>
    <a style="font-size:12pt;color:#56833e;float:right" href="#news_top"><img src="<?= FULLURL . '/images/arrow.jpg'?>" border="0" />Back to Top</a><br /><br /><br />
    <? endif; ?>
</div>
<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_news');
        });
    })
</script>

<? include VIEWS. '/members/footer.php' ?>
