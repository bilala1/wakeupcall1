<?
    $page_title        = SITE_NAME;
    $page_name         = 'News';
    $sub_name          = 'WAKEUP CALL News';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
        <?= SITE_NAME ?> partnered with multiple sources of news specifically aimed at the hospitality industry as well as HR and safety news.
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <h1>WAKEUP CALL News</h1>

	<? foreach(articles::get_recent(100) as $article): ?>
   		<div class="divider">
            <h3 class="title"><a href="/members/news/detail.php?articles_id=<?= $article['articles_id'] ?>"><?= $article['articles_title'] ?></a></h3>
            <? if($article['articles_subtitle']): ?>
            <h4>(<?= $article['articles_subtitle'] ?>)</h4>
            <? endif ?>
            <?= $article['articles_byline'] ? 'Written by:  ' . html::filter($article['articles_byline']) . '<br />' : '' ?>
            <?= html::filter($article['articles_teaser']) ?>
    	</div>
    <? endforeach ?>
</div>
<script type="text/javascript">
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_news');
        });
    })
</script>

<? include VIEWS. '/members/footer.php' ?>
