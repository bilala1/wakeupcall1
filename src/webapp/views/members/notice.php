<?
if(!$notice && isset($_REQUEST['notice'])){
    $notice = urldecode(stripcslashes($_REQUEST['notice']));
}
if($_REQUEST['error']){
    $errors[] = urldecode(stripcslashes($_REQUEST['error']));
}
if($errors){
    $error_string = '';
    foreach($errors as $key => $error){
        $error_string .= is_int($key) ? $error : '';
    }
}
?>
<? if($notice || $error_string): ?>
    <br clear="all" />
<? endif ?>
<? if($notice): ?>
    <p class="notice"><?= html::xss($notice) ?></p>
<? endif ?>
<? if($errors && !$error_string): ?>
    <p class="errors">There are errors below</p>
<? elseif($error_string): ?>
    <p class="errors">
        <?= html::xss($error_string) ?>
        <? if($error_string == 'Credit card is missing.'): ?>
            Please provide <a target="_blank" href="<?= FULLURL; ?>/members/account/billing.php">credit card information</a>.
        <? endif; ?>
    </p>
<? endif ?>
