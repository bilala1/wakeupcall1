<link rel="stylesheet" href="/css/style.css?v=20180618" type="text/css"/>
<link href="/css/MooDialog.css" rel="stylesheet" type="text/css" />
<link href="/css/iconize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/font-awesome.min.css">
<link rel="stylesheet" href="/js/jquery-ui-1.12.1/jquery-ui.min.css">

<?php if(!isset($page_no_mootools) || !$page_no_mootools) { ?>
    <script src="/js/mootools-core-1.4.0-full-compat-yc.js"></script>
    <script src="/js/mootools-more-1.4.0.1.js"></script>
    <script src="/js/overlay.js"></script>
    <script src="/js/MooDialog.js"></script>
    <script src="/js/MooDialog.Confirm.js"></script>
    <script src="/js/MooDialog.Request.js"></script>
<?php } ?>
<script src="https://www.google.com/jsapi"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
    jQuery.noConflict();
    // jQuery is now an alias to the jQuery functions; creating the new alias is optional.
</script>
<script src="/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
    var BASEURL = '<?= BASEURL ?>';
    var FULLURL = '<?= FULLURL ?>';
</script>
<script src="/js/global.js?v=20180618"></script>