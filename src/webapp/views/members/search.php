<?php
    $page_title        = SITE_NAME;
    $page_name         = $_REQUEST['type']=='mydocs'?'Files': 'Resources';
    $sub_name          = $_REQUEST['type']=='mydocs'?'My Files':'Document Library';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<style>
    .notice {
        margin-right: 15px;
    }
</style>
<div id="content">
        <p class="notice">
            Search Results for "<?= urldecode($_REQUEST['q']) ?>" are listed below.
        </p>
        <? if($_REQUEST['type'] == 'docs' || $_REQUEST['type'] == 'faqs'): ?>
        <div style="float:right;vertical-align:middle; margin-top:5px; margin-right: 15px;">
			<form action="/members/search.php" method="get" style="float:left">
				<strong>Search <?= $_REQUEST['type'] == 'docs' ? 'Documents' : 'FAQ\'s' ?></strong>
                <?= html::input($_REQUEST, 'q', array('style' => 'width:150px')) ?>
                <?= html::hidden($_REQUEST, 'type') ?>
				<input class="btn" type="submit" value="Search" />
			</form>
            <?
                $documentUrl = FULLURL . "/members/";
                if($_REQUEST['type'] == 'docs')
                    $documentUrl = $documentUrl . 'documents/index.php';
                else if($_REQUEST['back'] == 'faq-wuc')
                    $documentUrl = $documentUrl . 'tutorials/index.php';
                else
                    $documentUrl = $documentUrl . 'faqs/index.php';
            ?>
			&nbsp;&nbsp;<a style="display: inline-block; margin-top: 5px;" href="<?= $documentUrl; ?>">Back to <?= $_REQUEST['type'] == 'docs' ? 'Library' : 'FAQ\'s' ?></a>
		</div>
		<?= $_REQUEST['type'] == 'faqs' ? '<br clear="left" />' : '' ?>
		<? endif; ?>
		<? foreach($results as $result): ?>
            <div style="margin-bottom: 20px">
                <h2><?= $result['name'] ?></h2>
                <? if(empty($result['results'])): ?>
                    <i>There are no results</i>
                <? else: ?>
                    <? include(VIEWS .'/members/search/'.$result['file']) ?>
                <? endif ?>
            </div>
		<? endforeach; ?>
</div>
<? include VIEWS. '/members/footer.php' ?>
