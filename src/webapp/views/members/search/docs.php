<table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
    <thead>
        <tr>
            <th>Document Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($result['results'] as $document): ?>
        <tr>
            <td><span class="icon-<?=strtolower(files::get_extension($document['documents_filename']))?>"><?= $document['documents_title'] ?></span></td>
            <td style="white-space:nowrap;">
                <? if((members::get_members_status(ActiveMemberInfo::GetMemberId())== 'free') && ((int)$document['documents_freetrial']==0)): ?>
                    <i title="Restricted for trial members">Download/View</i>
                <? else: ?>
                    <a target="_blank" class="imageLink" href="<?= FULLURL;?>/members/documents/download.php?documents_id=<?= $document['documents_id'] ?>&type=.<?= files::get_extension($document['documents_filename']) ?>">Download/View</a>
                <? endif; ?>
                |
                <? if((int)$document['in_library']<1): ?>
                    <a class="add-to-my-wuc" href="<?= FULLURL;?>/members/documents/add-document.php?documents_id=<?= $document['documents_id'] ?>&redirect=<?= urlencode('/members/search.php?q='.$_REQUEST['q']); ?>">Add to My WAKEUP CALL </a>
                <? else: ?>
                    Already in library
                <? endif; ?>
            </td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
window.addEvent('domready',function(){
        $$('.add-to-my-wuc').addEvent('click', function(){
            var documents_id = this.get('href').split("=")[1];

            var dropdown = new Element('select', {
                                        html:  '<option value="-1">- Select Folder -</option><option value="0">General</option><? foreach(documents::get_my_folders() as $k => $folder): ?><option value="<?= $k ?>"><?= $folder ?></option><? endforeach; ?>',
                                        'class': 'myClass',
                                        styles: {
                                            display: 'none'
                                        },
                                        events: {
                                            change: function(){
                                                if(this.get('value')=='-1'){
                                                    alert('Please select a folder for this file');
                                                }
                                                else {
                                                    var Req = new Request({
                                                                           method: 'get', 
                                                                           url: '<?= FULLURL; ?>/members/documents/add-document.php',
                                                                           onSuccess: function(responseText){
                                                                               var success = new Element('span', { html: 'Added to library'});
                                                                               success.inject(dropdown,'before');
                                                                               dropdown.destroy();   
                                                                              // $$('.notice')[0].set('text', responseText);
                                                                              // $$('.notice')[0].setStyle('display', 'inline');
                                                                           }
                                                                         });
                                                        Req.send('documents_id='+documents_id+'&join_library_categories_id='+this.get('value'));
                                                   }
                                            },
                                            mouseover: function(){

                                            }
                                        }
            });
            dropdown.inject(this,'before');
            dropdown.set('value',-1);
            dropdown.set('reveal', { display:'inline'});
            dropdown.reveal();
            this.destroy();

            return false;
        })
    })        
</script>    
