<?php $counter = 1; ?>
<?php foreach($result['results'] as $faq): ?>
<div class="divider">
	<h3><a href="" onClick="showAns('Ans<?= $counter ?>');return false;">Q: <?= $faq['faqs_question'] ?></a></h3>
	<div class="faq_ans" id="Ans<?= $counter ?>">A: <?= $faq['faqs_answer'] ?></div>
</div>
<?php $counter++;endforeach; ?>

<script type="text/javascript">
function showAns(AnsBox) {
    //$(AnsBox).set('reveal', {duration:500, mode: 'vertical'});
    if($(AnsBox).getStyle('display') == 'block') {
        $(AnsBox).dissolve();
    }
    else {
        $(AnsBox).reveal({ duration:500, mode: 'vertical' });
    }
}
</script>
