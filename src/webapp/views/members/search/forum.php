<table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
    <thead>
        <tr>
            <th>Topic</th>
            <th>Post</th>
        </tr>
    </thead>
    <tbody>
        <? 
        function stripBBCode($text_to_search) {
            $pattern = '|[[\/\!]*?[^\[\]]*?]|si';
            $replace = '';
            return preg_replace($pattern, $replace, $text_to_search);
        }
        foreach($result['results'] as $forum): 
           $posts = stripBBCode($forum['posts_body']);
           if(strlen($posts)>200){
               $posts = substr($posts, 0, 200).'...';
           }
            ?>
        <tr>
            <td><a class="topictitle" href="/members/forums/view-topic.php?topics_id=<?= $forum['topics_id'] ?>"><?= $forum['topics_title'] ?></a></td>
            <td><?= $posts ?></td>
            
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
