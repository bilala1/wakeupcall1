<table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
    <thead>
        <tr>
            <th>Document Name</th>
            <th>Download</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($result['results'] as $document): ?>
        <tr>
            <td><span class="icon-<?=strtolower(files::get_extension($document['documents_filename']))?>"><?= $document['documents_title'] ?></span></td>
            <td><a target="_blank" class="imageLink" href="/members/documents/download.php?documents_id=<?= $document['documents_id'] ?>&type=.<?= files::get_extension($document['documents_filename']) ?>">Download</a></td>
            <td>
                <? if($document['documents_file']): ?>
                    <a href="<?= FULLURL;?>/members/documents/remove-document.php?documents_id=<?= $document['documents_id'] ?>&redirect=<?= urlencode('/members/search.php?q='.$_REQUEST['q']); ?>"><?= in_array($document['documents_type'], array('personal', 'submitted')) ? 'Delete' : 'Remove' ?></a>
                    <? if(in_array($document['documents_type'], array('personal', 'submitted'))): ?>
                        | <a href="<?= FULLURL;?>/members/my-documents/edit.php?documents_id=<?= $document['documents_id'] ?>&redirect=<?= urlencode('/members/search.php?q='.$_REQUEST['q']); ?>">Edit</a>
                    <? else: ?>
                        | <a href="<?= FULLURL;?>/members/my-documents/edit2.php?documents_id=<?= $document['documents_id'] ?>&redirect=<?= urlencode('/members/search.php?q='.$_REQUEST['q']); ?>">Edit</a>
                    <? endif ?>
                <? endif ?>
            </td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
