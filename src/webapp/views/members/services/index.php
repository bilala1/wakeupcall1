<?
    $page_title        = SITE_NAME;
    $page_name         = 'Resources';
    $sub_name          = 'Additional Services';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<?/* accordion styles moved to style.css */?>
<script type="text/javascript">
window.addEvent('domready', function() {
   var myAccordion = new Fx.Accordion($('accordion'), '#accordion h3', '#accordion .content', {
		opacity: false,
	   display: -1,
		onActive: function(toggler, element) {
			toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle_down.jpg") no-repeat scroll 0 5px transparent');
		},
		onBackground: function(toggler, element) {
			toggler.setStyle('background', 'url("' + FULLURL + '/images/triangle.jpg") no-repeat scroll 0 5px transparent');
		}
	});

    $$('h3[class^=toggler]').each(function(e) {
        e.addEvent('mouseover',function(){
            this.setStyle('background-color','#ffe9c9');
        });
        e.addEvent('mouseout',function(){
            this.setStyle('background-color','white');
        });

        e.addEvent('click', function(){
            var number = parseInt(e.get('class').match(/[0-9]/gi));
            $$('.content').each(function(e){
                if(e.get('class').match(number)){
                    if(e.get('style').match(/height: auto/gi)){
                        myAccordion.display(-1);
                    }
                }
            });
        })
    });
})

window.addEvent('load', function(){

    var myRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-notification-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            $('notifications_additional_services').set('value', responseText);
        }
    });

    $('notifications_additional_services').addEvent('click', function(event){
        myRequest.send('yn=' + this.get('checked')+'&type=notifications_additional_services');
    });

});
window.addEvent('domready', function(){
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>
})
window.addEvent('load', function(){
    //infobox settings
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            if(responseText == 'on'){
                $$('.page-desc').reveal();
                $$('.info_text').set('html','Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html','Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event){
        if($$('.page-desc')[0].get('style').match(/display: none/gi)){
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_addit_services');
    });
})
</script>

<div id="content">

    <span class="info_icon"></span>
    <p class="page-desc">
        <?= SITE_NAME ?> continues to provide our members with additional benefits and even greater value. The 
        links below connect you to service providers and products with special discounts for our members. The 
        hospitality industry uses these services and products on a regular basis and we hope that you will save 
        money from the special rates we have negotiated for our members. <?= SITE_NAME ?> does not charge a fee for 
        advertising and we do not receive any benefit other than knowing that we have helped our members. If you have 
        additional ideas for services to include, please let us know through the Concierge's Desk in the sidebar.
        <br /><br />
        <?= html::checkbox(1,'notifications_additional_services',$additional_services_notification); ?> Check box to be notified of any updates to Additional Services
    </p>
    
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />       
    <h1>Additional Services</h1>

<div id="accordion">
    <h3 class="toggler 1" style="color:#005DAA">Hospitality Non-Slip Shoes</h3>
    <div class="content 1">
        <p>Don't slip, Get Gripped... and do it in style! SlipGrips.com carries the widest selection of slip resistant and non-slip 
        shoes that cater to the hospitality industry, housekeepers and maintenance workers as well as other various industries and 
        occupations requiring non-slip shoes. For over 80 years, SlipGrips has developed and crafted quality industrial footwear for 
        workplaces worldwide. SlipGrips successfully combines its technical excellence with the necessary and essential emphasis on 
        comfort, protection and fashion demanded by today's workers. Our designers and technical specialists continually research new 
        compounds and materials in order to provide you with the best possible product at the right price. With pay roll deductions 
        available it's easy to get shoes that meet a variety of your demands and needs! Click through some of our hottest no-slip shoes 
        today from SlipGrips, Timberland, Rockport, Dickies, Converse, and more...</p>
        
        <ul>
            <li>Designed with the hospitality industry in mind</li>
            <li>$5000 Guarantee Pledge towards WC Claims</li>
            <li>Payroll deductions available for you and your employees</li>
            <li>Corporate programs available</li>
            <li>Free shipping/Free returns</li>
            <li>Variety of styles and manufacturers to choose from</li>
            <li>Use our discount code <span style="color: #ff0000;">WAKEUP</span> for 20% savings</li>
        </ul>
        
        <p>Slip Grips - <a target="_blank" href="http://www.slipgrips.com/products/search/hotel_footwear/">http://www.slipgrips.com/products/search/hotel_footwear/</a></p>

    </div>

    <h3 class="toggler 2" style="color:#005DAA">All-in-One Federal and State Posters</h3>
    <div class="content 2">
        <p>The All In One Poster Company, Inc. is one of the nation's top providers of state and federal employment posters, as well as safety and specialty posters.  Since our inception in 2002, the primary purpose of our company has been to provide our customers with the easiest, most accurate yet inexpensive solution to posting requirements.  We are a certified minority owned small business that prides itself in excellent customer service with a high esteem for integrity and honesty.  This, coupled with our top quality products at the best prices, has earned us years of loyalty from a diverse selection of clients ranging from small to large businesses, government agencies (local, state, federal), public establishments, learning institutions, and non-profit organizations.  Some of our highlights are:</p>
        <ul>
            <li>Hospitality is one of our top focus industries</li>
            <li>All state and federal poster requirements, for all 50 states, plus D.C. and Puerto Rico</li>
            <li>Better Business Bureau highest rating A+</li>
            <li>Voted number one in terms of accuracy, quality, customer service, and pricing</li>
            <li>Best Price Guarantee</li>
            <li>Go Green with our paper poster.  Save money and help the planet</li>
            <li>Corporate Management Programs that can save you up to 50%</li>
            <li>5% of annual profit donated to The Smile Train non-profit to help children</li>
            <li>Use our discount code <span style="color: #ff0000;">WAKEUP</span> for additional savings
        </ul>
        <p>Visit us at:<br />
        <a target="_blank" href="http://www.allinoneposters.com">http://www.allinoneposters.com</a><br /><br />
        Additional Hospitality specific poster at:<br />
        <a target="_blank" href="http://www.allinoneposters.com/s.nl/sc.8/category.3077/.f">http://www.allinoneposters.com/s.nl/sc.8/category.3077/.f</a>
        </p>
    </div>

    <h3 class="toggler 3" style="color:#005DAA">Pre-Employment Drug, Health, and Background Screening</h3>
    <div class="content 3">

        <p>As one of the world's largest screening providers focused 100% on employment screening, HireRight 
        specializes in helping organizations of all sizes efficiently implement and manage their employment screening 
        programs. HireRight prides itself on understanding the complex screening needs of today's employers, and strives 
        to provide easy-to-use, streamlined solutions that help organizations work smartly.</p>
        
        <p>There's a reason why so many employers trust HireRight to handle their screening programs:</p>
        
        <ul>
            <li>Flexible, tailored pre-employment background checking solutions</li>
            <li>Drug &amp; Health Screening</li>
            <li>Employment, Education, MVR, Criminal Checks and More</li>
            <li>Electronic I-9 Management &amp; E-Verify with DHS</li>
            <li>More than 150 different services in more than 200 countries and territories worldwide</li>
            <li>Continuous technological innovation</li>
            <li>Constant customer feedback and a drive to offer the industry's best solutions at the most competitive prices.</li>
            <li>Waived $19.95 sign-up fee for WAKEUP CALL members, plus additional discounts depending on the package chosen.</li>
        </ul>

        
        <p><a href="http://www.hireright.com/wakeupcall" target="_blank">http://www.hireright.com/wakeupcall</a></p>
        <p><a href="http://www.hireright.com/wakeupcall" target="_blank"><img src="<?= FULLURL; ?>/images/hireright.jpg" border="0" /></a></p>
    </div>

    <h3 class="toggler 4" style="color:#005DAA">Employment Law Attorney</h3>
    <div class="content 4">

        <p>Wilson Elser, a full-service and leading defense litigation law firm serves its clients with more 
        than 800 attorneys in 23 offices in the United States and through a network of affiliates in key regions 
        globally. Founded in 1979, it ranks among the top law firms identified by The American Lawyer and is included 
        in the top 50 of The National Law Journal's survey of the nation's largest law firms. We recognize that employers 
        face a growing set of legal challenges. There are new, and often complex, statutory regulations, increased potential 
        exposure from the ever-expanding challenges of employment decisions, and increased oversight by the U.S. Department of 
        Labor, to name just a few. Wilson Elser helps companies meet these challenges, and more, with a broad range of 
        capabilities in employment law.</p>


        <p>Ask for Bruno Katz<br />
        <a href="mailto:bruno.katz@wilsonelser.com">bruno.katz@wilsonelser.com</a><br />
        619-881-3317 Office<br />
        619-321-6201 Fax
        <!--<a target="_blank" href="http://wilsonelser.com">www.wilsonelser.com</a>-->
        </p>
    </div>

    <h3 class="toggler 5" style="color:#005DAA">More Coming Soon!</h3>
    <div class="content 5">
        <p><?= SITE_NAME ?> is constantly looking for additional services and deals for our members based on member input and industry needs. 
            Check back often as we add and update the Additional Services. 
        </p>
    </div>
</div>

</div>

<? include VIEWS. '/members/footer.php' ?>
