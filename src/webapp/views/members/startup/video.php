<?php
$page_title 		= "WAKEUP CALL | Welcome Video";
$page_name 			= '';
$page_keywords		= '';
$page_description 	= '';
include VIEWS . '/members/header.php';
?>
<script type="text/javascript" src="/js/swfobject/src/swfobject.js?v=1"></script>
<script type="text/javascript" src="/AC_RunActiveContent.js"></script>
<script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>

<style type="text/css">
	#video {
		width:100%;
		height: 467px;
		background-color: #111;
	}
</style>

<div id="content">
	<div style="width: 800px; margin: auto">
		<h1>Welcome to <?= SITE_NAME ?>!</h1>

		<div id="video"></div>

		<br />

		<form action="" method="post">
            <?= html::checkbox(1, 'members_show_startup_video', members::get_members_show_startup_video(ActiveMemberInfo::GetMemberId())) ?>
			<label for="members_show_startup_video">Show at Startup</label>

			<input type="submit" class="btn right" value="Continue" />
		</form>
	</div>
</div>

<script type="text/javascript">
	var FULLURL = '<?= FULLURL ?>';

	window.addEvent('domready', function() {
		$f("video", {
			src: FULLURL + "/flowplayer-3.2.7.swf",
			wmode: "transparent"
		}, {
			wmode: 'transparent',
			key: '#$4902077e83495bce003',
			clip: {
				//url: FULLURL + '/WUC_Open_VIdeo_Final_2.mp4',
				url: FULLURL + '/wuc_open_2012-10-03-12-40.mp4',
				onFinish: function() {
					//$('video').setStyle('display', 'none');
				}
			}
		});
	});
</script>

<?php include VIEWS . '/members/footer.php' ?>