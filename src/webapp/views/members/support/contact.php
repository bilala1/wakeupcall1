<?
$page_title = SITE_NAME;
$page_name = 'Support';
$sub_name = 'Contact Us';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

    <div id="content">
        <h1>Concierge Desk</h1>

        <form method="post" onSubmit="return chkForm(this)" class="center" style="width:250px">
            <?= html::select(arrays::values_as_keys(array(
                'General Question/Request',
                'Request SDS (Not Listed)',
                'Request Document (Not Listed)',
                'Website Bug',
                'Website Recommendation',
                'Other'
            )), 'suggestion_type', '', array('style' => 'width:250px; margin-bottom:10px')) ?><br/>

            <p id="suggestion-error">Please type a suggestion.</p>
            <?= html::textarea('', 'suggestion', array('style' => 'width:250px; height:50px')) ?><br/>
            <input type="hidden" name="referer" value="<?= $_SERVER['REQUEST_URI'] ?>"/>

            <div class="form-actions">
                <?= html::submit('Send Request / Suggestion', array('class' => 'btn-primary btn-ok')) ?>
            </div>
        </form>

        <script type="text/javascript">
            function chkForm(form) {
                document.getElementById('suggestion-error').style.display = 'none';
                if (form.suggestion.value == '') {
                    document.getElementById('suggestion-error').style.display = 'block';
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>
    </div>

<? include VIEWS . '/members/footer.php' ?>