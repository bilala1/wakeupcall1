<?
$page_title        = SITE_NAME;
$page_name         = 'Support';
$sub_name          = 'FAQs';
$page_keywords     = '';
$page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
<!--		<?= SITE_NAME ?> has made every effort to design and implement a user friendly and intuitive website. 
        We realize there will still be times when a little help to guide you through a process, would be nice. 
        With that in mind, we have posted the following tutorials for you to refer to as needed. 
        Please note that many of the tutorials will also appear in the sidebar when appropriate.-->
        <?= SITE_NAME ?> has made every effort to design and implement a user friendly and intuitive website. We 
        realize that there will still be times when a little help to guide you through a process would be nice. With 
        that in mind, we have posted the following FAQs for you to refer to as needed.
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />  
    
        <div style="float:right; margin-right: 15px;">
			<form method="get" action="/members/search.php">
				<strong>Search</strong>
				<input type="text" style="width:150px;" name="q" class="text">
				<input type="hidden" value="faqs" name="type">
                <input type="hidden" value="faq-wuc" name="back">
				<input type="submit" value="Search" class="btn">
			</form>
		</div>
        <br clear="all" />      
    
    <h1>WAKEUP CALL FAQs</h1>
    
    <? $counter = 1; foreach($faqs as $faq): ?>
		<div class="divider">
			<h3><a href="" onClick="showAns('Ans<?= $counter ?>');return false;">Question: <?= $faq['faqs_question'] ?></h3></a>
			<div class="faq_ans" id="Ans<?= $counter ?>">Answer: <?= $faq['faqs_answer'] ?></div>
		</div>
		<? if($counter > 1 && $counter % 5 == 0): ?>
		<a href="#news_top" style="font-size:12pt;color:#56833e;float:right"><img src="<?= FULLURL . '/images/arrow.jpg'?>" border="0" />Back to Top</a><br /><br />
	    <? endif ?>
	<? $counter++; endforeach ?>
	<? if($counter % 10 != 1): ?>
	<a href="#news_top" style="font-size:12pt;color:#56833e;float:right"><img src="<?= FULLURL . '/images/arrow.jpg'?>" border="0" />Back to Top</a><br /><br />
    <? endif; ?>
</div>
<script type="text/javascript">
    function showAns(AnsBox) {
        //$(AnsBox).set('reveal', {duration:500, mode: 'vertical'});
        if($(AnsBox).getStyle('display') == 'block') {
            $(AnsBox).dissolve();
        }
        else {
            $(AnsBox).reveal({ duration:500, mode: 'vertical' });
        }
    }
    
    <? /* open or collapse infobox? */ ?>
    <? if(!$infobox): ?>
       $$('.page-desc').setStyle('display', 'none');
       $$('.info_text').set('html','Show Text Box');
    <? endif; ?>    
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_tutorials');
        });
    })          
</script>

<? include VIEWS. '/members/footer.php' ?>
