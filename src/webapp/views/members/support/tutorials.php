<?
$page_title = SITE_NAME;
$page_name = 'Support';
$sub_name = 'Tutorials';
$page_keywords = '';
$page_description = '';
?>
<? include VIEWS . '/members/header.php' ?>

<div id="content">
    <h1>Tutorials</h1>

    <p>Due to recent changes in Google technology we are updating the tutorials so they are playable on multiple browsers. Updated tutorials coming soon.</[>

    <p>If you need assistance, please contact us through the concierge's desk. Sorry for any inconvenience.</p>
    <? /*
    <?php if (UserPermissions::UserCanAccessScreen('claims') || UserPermissions::UserCanAccessScreen('certificates')) { ?>
        <h2>Tracking</h2>
        <ul>
            <?php if (UserPermissions::UserCanAccessScreen('claims')) { ?>
                <li><a class="j-tutorial" data-video="adding-a-claim-2012-10-03-12-40.mp4">Adding a Claim</a></li>
            <?php } ?>
            <?php if (UserPermissions::UserCanAccessScreen('certificates')) { ?>
                <li><a class="j-tutorial" data-video="Certificate-Tracking.mp4">Certificate Tracking</a></li>
                <li><a class="j-tutorial" data-video="carriers_recipients-2012-10-03-12-40.mp4">Carriers and
                        Recipients</a></li>
            <?php } ?>
        </ul>
    <?php } ?>

    <?php if (ActiveMemberInfo::IsAccountMultiLocation() || UserPermissions::UserCanAccessScreen('forum')) { ?>
        <h2>Forums</h2>
        <ul>
            <?php if (ActiveMemberInfo::IsAccountMultiLocation()) { ?>
                <li><a class="j-tutorial" data-video="Corporate-Bulletin.mp4">Corporate Bulletin</a></li>
            <?php } ?>
            <?php if (UserPermissions::UserCanAccessScreen('forum')) { ?>
                <li><a class="j-tutorial" data-video="Message-Forum.mp4">Message Forum</a></li>
            <?php } ?>
        </ul>
    <?php } ?>

    <h2>Files</h2>
    <ul>
        <li><a class="j-tutorial" data-video="My-Files.mp4">My Files</a></li>
    </ul>

    <?php if (UserPermissions::UserCanAccessScreen('elaw') || UserPermissions::UserCanAccessScreen('hr')) { ?>
        <h2>Employee Mgmt</h2>
        <ul>
            <?php if (UserPermissions::UserCanAccessScreen('hr')) { ?>
                <li><a class="j-tutorial" data-video="HR.mp4">HR</a></li>
            <?php } ?>
            <?php if (UserPermissions::UserCanAccessScreen('elaw')) { ?>
                <li><a class="j-tutorial" data-video="Employment-Law.mp4">Employment Law</a></li>
            <?php } ?>
        </ul>
    <?php } ?>

    <h2>Resources</h2>
    <ul>
        <li><a class="j-tutorial" data-video="Document-Library.mp4">Document Library</a></li>
        <li><a class="j-tutorial" data-video="MSDS-Library.mp4">MSDS Library</a></li>
    </ul>
 */ ?>
</div>

<style type="text/css">
    #video {
        width: 100%;
        height: 100%;
    }
    #overlay {
        visibility: visible !important;
    }
</style>

<script type="text/javascript">
    jQuery('.j-tutorial').click(function (e) {
        var video = jQuery(e.target).attr('data-video');
        var dialog = new MooDialog( {
            size: {
                width: 720,
                height: 412
            },
            closeOnOverlayClick: true,

        });

        dialog.addEvent('open', function () {
            $f("video", {
                src: FULLURL + "/flowplayer-3.2.7.swf",
                wmode: "transparent"
            }, {
                wmode: 'transparent',
                key: '#$4902077e83495bce003',
                clip: {
                    url: '<?= FULLURL ?>/videos/' + video,
                    onFinish: function () {
                        //$('video').setStyle('display', 'none');
                    }
                }
            });
        });

        dialog.setContent(new Element('div#video'), {width:"200", height:"200"});
        dialog.open();
    });
</script>
<script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>
<? include VIEWS . '/members/footer.php' ?>
