<?
    $page_title        = SITE_NAME;
    $page_name         = 'Surveys';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
<div id="leftcol" class="w260">
	<br />
</div>

<div id="middlecol" class="w700">

<h1>Survey</h1>
<h2 class="title"><?= $survey['surveys_title'] ?></h2>

Please choose an option below:<br />
<form action="/members/surveys/vote.php" method="post">

<input type="hidden" name="surveys_id" value="<?= $survey['surveys_id'] ?>" />
<table style="border-top: 1px dotted #ccc">
    <? foreach($survey['options'] as $option): ?>
    <tr>
        <td><input id="option_<?= $option['survey_options_id'] ?>" type="radio" name="survey_options_id" value="<?= $option['survey_options_id'] ?>" /></td>
        <td><label for="option_<?= $option['survey_options_id'] ?>"><?= $option['survey_options_name'] ?></label></td>
    </tr>
    <? endforeach; ?>
    <tr>
    	<td colspan="2"><input type="submit" value="Vote" class="btn center" /</td>
    </tr>
</table>


</form>

</div>
<br class="clear"/>
</div>

<? include VIEWS. '/members/footer.php' ?>
