<?
    $page_title        = SITE_NAME;
    $page_name         = 'Surveys';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>
<div id="content">
<div id="leftcol" class="w260">

<h1>Unanswered Surveys:</h1>

<ul>
<? foreach($surveys as $_survey): ?>
<li><a href="/members/surveys/index.php?surveys_id=<?= $_survey['surveys_id'] ?>"><?= $_survey['surveys_title'] ?></a></li>
<? endforeach ?>
</ul>

</div>

<div id="middlecol" class="w700">
</div>
<br class="clear"/>
</div>

<? include VIEWS. '/members/footer.php' ?>
