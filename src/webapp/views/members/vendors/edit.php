<?
    $page_title        = SITE_NAME;
    $page_name         = 'Tracking';
	$sub_name		   = 'Certificates';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <h1 class="left"><?= $_REQUEST['vendors_id'] ? 'Edit' : 'Add' ?> Vendor</h1>
    <div class="page-options"><a href="/members/vendors/index.php">Back to Vendor List</a></div><br clear="all" />

    <form action="/members/vendors/edit.php" method="post" enctype="multipart/form-data" id="editvendor">
        <?= html::hidden($vendor, 'vendors_id') ?>
        <?= html::hidden($_REQUEST, 'return_to_cert') ?>

        <?= html::location_picker($vendor, $errors, 'vendors'); ?>

        <?= html::textfield('<span class="imp">*</span> Vendor Name:', $vendor, 'vendors_name', array('maxlength' => 100), $errors) ?>
        
        <?= html::textfield('<span class="imp">*</span> Contact Name:', $vendor, 'vendors_contact_name', array('maxlength' => 255), $errors) ?>
        
        <?= html::textfield('<span class="imp">*</span> Email:', $vendor, 'vendors_email', array('maxlength' => 255), $errors) ?>
        
        <?= html::textfield('Phone:', $vendor, 'vendors_phone', array('maxlength' => 100, 'class' => 'phone'), $errors) ?>
        
        <?= html::textfield('Street Address:', $vendor, 'vendors_street', array('maxlength' => 255), $errors) ?>
        
        <?= html::textareafield('Services Provided:', $vendor, 'vendors_services', array('maxlength' => 255, 'class' => 'text'), $errors) ?>
        <fieldset class="j-toggle-parent">
        <legend>Certificates <a class="j-toggle-trigger"></a></legend>
        <div class="j-toggle-content">
        <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <th>Location</th>
            <? endif ?>
            <th style="width:135px;">Types of Coverage</th>
            <th>Expiration</th>
            <th>Active Certificate File</th>
            <th style="width:70px">Actions</th>
        </tr>
        <? foreach($certificates as $certificate): ?>
            <?
            if($certificate['certificates_expire'] != '0000-00-00'){
            $expired = strtotime($certificate['certificates_expire']) < time(); }?>
            <? $doHighlight = $certificate['files_name'] == '' || $expired; ?>
            <? $perms = UserPermissions::UserObjectPermissions('certificates', $certificate); ?>
            <tr <?= ($doHighlight == true) ? 'class="notices"' : '' ?>>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= strings::wordwrap($certificate['licensed_locations_name'], 9, '&shy;', true); ?></td>
            <? endif ?>
            <td><?= $certificate['certificates_coverages'] ?></td>
            <td <?= $expired?'class="error"' : ''?> ><?=  ($expired == true) ? date('m/d/Y', strtotime($certificate['certificates_expire'])).'</span>' : ( ($certificate['certificates_expire'] == '0000-00-00')? "":date('m/d/Y', strtotime($certificate['certificates_expire'] ))) ?></td>
            <td class="hyphenate <?= $certificate['files_name']?'':' error'?>">
            <?php if($certificate['files_name'] != ''): ?>
                <a target="_blank" href="/members/my-documents/certificates/download.php?certificates_files_id=<?= $certificate['certificates_files_id'] ?>&type=.<?= files::get_extension($certificate['files_name']) ?>"><?= $certificate['files_name'] ?></a>
            <? else: ?>
                <span>Need to Upload or Set File to 'Active'!</span>
            <? endif; ?>
            </td>
            <td>
                <div class="actionMenu">
                    <ul>
                        <? if ($perms['edit']) { ?>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>">Edit</a>
                            </li>
                            <li>
                                <a href="/members/my-documents/certificates/edit.php?certificates_id=<?= $certificate['certificates_id'] ?>&action=update">Update</a>
                            </li>
                            <li><a href="#" class="history" id="history_<?= $certificate['certificates_id'] ?>">History</a>
                            </li>
                            <?php if($certificate['has_emails']) { ?>
                                <li><a href="#" class="emailHistory" id="emailHistory_<?= $certificate['certificates_id'] ?>">Email History</a></li>
                            <?php } ?>
                            <? if ($perms['delete']): ?>
                                <li>
                                    <a href="/members/my-documents/certificates/delete.php?certificates_id=<?= $certificate['certificates_id'] ?>"
                                       class="deletecheck">Delete</a></li>
                            <? endif ?>
                            <? if ($certificate['certificates_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/my-documents/certificates/hide.php?certificates_id=<?= $certificate['certificates_id'] ?>&hide_certificate=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        <? } ?>
                    </ul>
                </div>
            </td>
            </tr>
        <? endforeach; ?>
        </table>
        <div>
         <?if($bypass_location == 1){?>
            <a href="/members/my-documents/certificates/edit.php?licensed_locations_id=<?=$vendor_location?>&action=setLocation&vendors_id=<?= $_REQUEST['vendors_id']?>" class="add-item">Add Certificate</a>
         <? }else{?>
        <a href="/members/my-documents/certificates/edit.php?vendors_id=<?= $_REQUEST['vendors_id']?>" class="add-item">Add Certificate</a>
         <? }?>
        </div>
        </div>
    </fieldset>
        <p><span class="imp">*</span> - required</p>
        <div class="form-actions">
            <?= html::input(($vendor ? 'Save' : 'Add') . ' Vendor', 'save', array('class' => 'btn-primary btn-ok', 'type' => 'submit')) ?>
        </div>
    </form>  
</div>
<script src="/js/vendors.js?v=20180618"></script>
<? include VIEWS. '/members/footer.php' ?>
