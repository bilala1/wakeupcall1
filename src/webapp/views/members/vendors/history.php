<div>
    <? $actions = vendors_history::get_vendors_history_with_data(ActiveMemberInfo::GetMemberId(), $_REQUEST['vendors_id']); ?>
    <ul>
        <? if ($actions): ?>
            <? foreach ($actions as $action): ?>
                <li><b>(<?= date(DATE_FORMAT_FULL, strtotime($action['vendors_history_datetime'])) ?>)</b>
                    - <?= $action['vendors_history_description'] ?></li>
            <? endforeach ?>
        <? else: ?>
            <b>No History For Current Record.</b>
        <? endif; ?>
    </ul>
</div>
