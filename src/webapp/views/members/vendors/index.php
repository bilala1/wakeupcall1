<?
    $page_title        = SITE_NAME;
    $page_name         = 'Tracking';
	$sub_name		   = 'Certificates';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<div id="content">
    <div class="page-header">
    <h1 class="left"><? if( $_SESSION['vendors_show_hidden'] == 1) { ?>Hidden <? } ?>Vendors</h1>
    <div class="page-options">
        <div class="page-dialog-background"></div>
        <a href="/members/vendors/edit.php" class="add-item">Add Vendor</a> |
        <a href="/members/vendors/report.php">Export Vendor List</a> |
        <a class="page-dialog-trigger" data-target=".filter-body"><i class="fa fa-caret-down"></i> Filter</a> |
        <a href="/members/my-documents/certificates/index.php">Back to Certificates</a>
    </div><br clear="all" />
    <div class="page-dialog filter-body page-dialog-<?= ActiveMemberInfo::IsUserMultiLocation() ? '2' : '1' ?> filter-contracts">

        <form action="index.php" id="filter" method="get">
            <div class="dialog-sections">

                <? if (ActiveMemberInfo::IsUserMultiLocation()) { ?>
                    <div class="dialog-section">
                        <h3>Locations</h3>
                        <?= html::location_filter('vendors', $model['join_licensed_locations_id']) ?>
                    </div>
                <? } ?>
                <div class="dialog-section">
                    <h3>Display Options</h3>
                    <label><?= html::radio(0, 'vendors_show_hidden', $model) ?> Show Active Vendors</label><br>
                    <label><?= html::radio(1, 'vendors_show_hidden', $model) ?> Show Hidden Vendors</label><br>
                </div>
            </div>
            <div class="form-actions">
                <?= html::submit("Set Filter", array('name' => 'filter')) ?>
                <?= html::submit("Clear Filter", array('name' => 'clear')) ?>
            </div>
        </form>
    </div>
</div>

    
    <table style="margin-top: 50px;table-layout: fixed; width: 100%;">
        <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <th><?= strings::sort('licensed_locations_name', 'Location') ?></th>
            <? endif ?>
            <th><?= strings::sort('vendors_name', 'Vendors Name') ?></th>
            <th><?= strings::sort('vendors_contact_name', 'Contact Name') ?></th>
            <th>Phone</th>
            <th>Number of Certificates</th>
            <th>Actions</th>
        </tr>
        <? if(!$vendors) { 
            if (sizeof($params) < 2) {
        ?>
        <tr><td colspan="4" align="center">You have not entered a vendor yet</td></tr>
        <?  } else { ?>
        <tr><td colspan="4" align="center">Search did not turn up anything</td></tr>
        <?  }
        }
        ?>
        <? foreach($vendors as $vendor): ?>
        <? $perms = UserPermissions::UserObjectPermissions('vendors', $vendor); ?>
            <tr>
            <? if (ActiveMemberInfo::IsUserMultiLocation()): ?>
                <td><?= strings::wordwrap($vendor['licensed_locations_name'], 9, '&shy;', true); ?></td>
            <? endif ?>
            <td><?= $vendor['vendors_name'] ?></td>
            <td><a href="mailto:<?= $vendor['vendors_email'] ?>"><?= $vendor['vendors_contact_name'] ?></a></td>
            <td align="center"><?= $vendor['vendors_phone'] ?></td>
            <td align="center"><?= $vendor['certificates_count'] ?></td>
            <td>
                <? if ($perms['edit']) { ?>
                    <div class="actionMenu">
                        <ul>
                            <li><a href="/members/vendors/edit.php?vendors_id=<?= $vendor['vendors_id'] ?>">Edit</a>
                                <li><a href="#" class="history" id="history_<?= $vendor['vendors_id'] ?>">History</a></li>
                            </li>
                            <? if ($perms['delete']) { ?>
                                <li><a href="/members/vendors/delete.php?vendors_id=<?= $vendor['vendors_id'] ?>"
                                       class="j-delete"
                                       data-numcerts="<?= $vendor['certificates_count'] ?>"
                                       data-vendorid="<?= $vendor['vendors_id'] ?>"
                                       title="Are you sure you want to delete this vendor?">Delete</a></li>
                            <? } ?>
                            <? if ($vendor['vendors_hidden'] <> 1): ?>
                                <li>
                                    <a href="/members/vendors/hide.php?vendors_id=<?= $vendor['vendors_id'] ?>&hide_vendor=1">Hide</a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a href="/members/vendors/hide.php?vendors_id=<?= $vendor['vendors_id'] ?>&hide_vendor=0">Un-Hide</a>
                                </li>
                            <? endif ?>
                        </ul>
                    </div>
                <? } ?>
            </td>
            </tr>
        <? endforeach; ?>
    </table>
    <?= $paging->get_html(); ?>
</div>
    <div class="template hidden" id="delete-with-certs">
        <div>
            <p>This vendor has associated certificates. If the vendor is deleted the certificates will be deleted as well.</p>
            <p>If you don't want the vendor to appear in this list any more, consider hiding the vendor and associated certificates.</p>
            <div class="options">
                <button class="j-cancel btn-primary">Cancel</button>
                <button class="j-hide btn-primary btn-ok">Hide Vendor and Certificates</button>
                <button class="j-delete btn-primary btn-delete">Delete Vendor and Certificates</button>
            </div>
        </div>
    </div>
    <script src="/js/vendors_list.js?v=20180618"></script>

<? include VIEWS. '/members/footer.php' ?>