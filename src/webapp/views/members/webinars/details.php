<?
    $page_title        = SITE_NAME;
    $page_name         = 'Training';
    $sub_name		   = 'Webinars';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>
<script type="text/javascript">
window.addEvent('load', function() {
    <? /* open or collapse infobox? */ ?>
    <? if (!$infobox): ?>
        $$('.page-desc').setStyle('display', 'none');
        $$('.info_text').set('html', 'Show Text Box');
    <? endif; ?>
    
    //webinars notification
    var webinarsRequest = new Request({
    url: '<?= FULLURL; ?>/members/ajax-change-notification-setting.php',
    method: 'get',
        onSuccess: function(responseText){
            $('notifications_webinars_update').set('value', responseText);
        }
    });
    $('notifications_webinars_update').addEvent('click', function(event){
        webinarsRequest.send('yn=' + this.get('checked')+'&type=notifications_webinars_update');
    });
    
    //infobox settings
    var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
        onSuccess: function(responseText) {
            if (responseText == 'on') {
                $$('.page-desc').reveal();
                $$('.info_text').set('html', 'Hide Text Box');
            }
            else {
                $$('.page-desc').dissolve();
                $$('.info_text').set('html', 'Show Text Box');
            }
        }
    });
    $$('.info_icon, .info_text').addEvent('click', function(event) {
        if ($$('.page-desc')[0].get('style').match(/display: none/gi)) {
            var show = 'true';
        }
        else {
            var show = 'false';
        }
        webinarsRequest.send('yn=' + show + '&type=members_settings_infobox_webinars');
    });
    
    <? if($webinar_passed && $webinar['webinars_file_recording']): ?>
    $f("webinar-recording", {
            src: "<?= FULLURL ?>/flowplayer-3.2.7.swf", 
            wmode: "transparent"
        }, {
        wmode: 'transparent',
        key: '#$4902077e83495bce003',
        clip: {
            url: '<?= FULLURL.WEBINAR_DIR . $webinar['webinars_file_recording'] ?>',
            autoPlay: false,
            scaling: 'fit',
        }
    });
    <? endif ?>
});
</script>

<div id="content">
	<span class="info_icon"></span>
	<p class="page-desc">
		<span style="font-size: 150%">Webinars</span>
		<br/><br/>
		<?= SITE_NAME ?> along with along with our partner-industry experts, conduct educational webinars, on topics
		crucial to the hospitality industry.
		We strive to provide webinars based upon membership requests whenever possible. Please visit our webinar section
		regularly to view our scheduled webinars and to access our webinar archives.
		<br/><br/>
		<?= html::checkbox(1, 'notifications_webinars_update', $notifications['notifications_webinars_update']); ?>
		Check box to be notified of webinar events
		<br/><br/>
	</p>
	<span class="info_text">Hide Text Box</span>
	<br class="clear"/>


    <h1 class="left"><?= $webinar['webinars_name'] ?></h1>
    <div class="page-options"><a href="index.php">Return to Webinars List</a></div>
    <br clear="all"/>

    <strong><?= times::from_mysql_utc($webinar['webinars_datetime']) ?></strong>

    <? if($webinar_upcoming): ?>
        <? if(in_array($webinar['webinars_id'], $webinars_attending)): ?>
        <a class="unreg_btn" href="/members/webinars/unregister.php?webinars_id=<?= $webinar['webinars_id'] ?>">unregister</a>
        <? else: ?>
        <a class="reg_btn" href="/members/webinars/register.php?webinars_id=<?= $webinar['webinars_id'] ?>">register</a>
        <? endif ?>
    <? endif ?>
    <br />

    <br /><hr /><br />

    <h3>Description</h3>
    <p><?= $webinar['webinars_description'] ?></p>
    <br />

    <? if($webinar_upcoming && ($webinar['webinars_file1'] || $webinar['webinars_file2'])): ?>
        <h3>File<?= ($webinar['webinars_file1'] && $webinar['webinars_file2']) ? 's' : '' ?></h3>
        <? if($webinar['webinars_file1']): ?>
        <p><a href="<?= WEBINAR_DIR . $webinar['webinars_file1'] ?>" target="_blank"><?= $webinar['webinars_file1_name'] ?></a></p>
        <? endif ?>
        <? if($webinar['webinars_file2']): ?>
        <p><a href="<?= WEBINAR_DIR . $webinar['webinars_file2'] ?>" target="_blank"><?= $webinar['webinars_file2_name'] ?></a></p>
        <? endif ?>
        <br />
    <? endif ?>

    <? if(!$webinar_passed && $webinar_upcoming): ?>
        <h3>Connection Information</h3>
        <p><?= $webinar['webinars_connection_info'] ?></p>
        <p><strong>Webinar Link:</strong> <?= strings::linkify($webinar['webinars_link']) ?></p>
        <br />
    <? elseif($webinar_passed && $webinar['webinars_file_recording']): ?>
        <p><strong>Webinar Recording:</strong> <a href="<?= WEBINAR_DIR . $webinar['webinars_file_recording'] ?>" target="_blank">recording</a></p>
        <div id="webinar-recording" style="width:<?= $dimensions['width'] ?>px;height:<?= $dimensions['height'] ?>px"></div>
    <? endif ?>
</div>

<? include VIEWS. '/members/footer.php' ?>
