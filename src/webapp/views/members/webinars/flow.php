<?
function flvdim($name) {
    $file = @fopen($name, 'rb');
    if($file === false)
        return false;

    $header = fread($file, 2048);
    fclose($file);
    if($header === false)
        return false;

    return array(
        'width' => flvdim_get($header, 'width'),
        'height' => flvdim_get($header, 'height')
    );
}

function flvdim_get($header, $field) {
    $pos = strpos($header, $field);
    if($pos === false)
        return false;

    $pos += strlen($field) + 2;
    return flvdim_decode(ord($header[$pos]), ord($header[$pos + 1]));
}

function flvdim_decode($byte1, $byte2) {
    $high1 = $byte1 >> 4;
    $high2 = $byte2 >> 4;
    $low1 = $byte1 & 0x0f;

    $mantissa = ($low1 << 4) | $high2;

    // (1 + m�2^(-8))�2^(h1 + 1) = (2^8 + m)�2^(h1 - 7)
    return ((256 + $mantissa) << $high1) >> 7;
}

var_dump(flvdim(SITE_PATH . '/wcc.flv'));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <script type="text/javascript" src="/js/flowplayer-3.2.6.min.js"></script>
    <script type="text/javascript">
    function init(){
        $f("video", {
                src: BASEURL + "/flowplayer-3.2.7.swf", 
                wmode: "transparent"
            }, {
            wmode: 'transparent',
            key: '#$4902077e83495bce003',
            clip: {
                url: BASEURL + '/wcc.flv',
                autoPlay: false,
                scaling: 'fit',
                onBegin: function() {
                   //alert(clip.metaData);
                   alert('hi');
                } 
            }
        });
    }
    </script>
  </head>
  <body onload="init()">
    <div id="video" style="width:600px; height:500px"></div>
  </body>
</html>
