<?
    $page_title        = SITE_NAME;
    $page_name         = 'Training';
	$sub_name		   = 'Webinars';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<script type="text/javascript">
    window.addEvent('load', function(){
        //webinars notification
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-notification-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                $('notifications_webinars_update').set('value', responseText);
            }
        });
        $('notifications_webinars_update').addEvent('click', function(event){
            webinarsRequest.send('yn=' + this.get('checked')+'&type=notifications_webinars_update');
        });
    });
    window.addEvent('domready', function(){
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
           $$('.page-desc').setStyle('display', 'none');
           $$('.info_text').set('html','Show Text Box');
        <? endif; ?>
    });
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_webinars');
        });
    });


</script>

<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
        <span style="font-size: 150%">Webinars</span>
        <br /><br />
        <?= SITE_NAME ?>, along with other industry experts and guest specialists, will 
        conduct educational webinars on topics crucial to the hospitality industry. We strive to provide 
        webinars based upon membership requests whenever possible. Please visit the webinar section regularly 
        to view the scheduled webinars and to access the webinar archives. If you have a webinar topic you would 
        like us to explore, please send us a message through the Concierge's Desk in the sidebar.
        
        <br /><br />
        <?= html::checkbox(1,'notifications_webinars_update',$notifications['notifications_webinars_update']); ?> Notify me of future webinars
        <br /><br />
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />
    <h1>Webinars</h1>
    <h2>Upcoming</h2>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <th><?= strings::sort('webinars_name_upcoming', 'Name') ?></th>
            <th><?= strings::sort('webinars_datetime_upcoming', 'Date') ?></th>
            <th width="150">Actions</th>
        </tr>
        <? foreach($upcoming as $webinar): ?>
            <tr>
                <td><a href="/members/webinars/details.php?webinars_id=<?= $webinar['webinars_id'] ?>"><?= $webinar['webinars_name'] ?></a></td>
                <td><?= times::from_mysql_utc($webinar['webinars_datetime'],'m/d/Y h:i a T') ?></td>
                <td>
                    <? if(in_array($webinar['webinars_id'], $webinars_attending)): ?>
                    <a href="/members/webinars/unregister.php?webinars_id=<?= $webinar['webinars_id'] ?>" class="unreg_btn">Unregister</a>
                    <? else: ?>
                    <a href="/members/webinars/register.php?webinars_id=<?= $webinar['webinars_id'] ?>" class="reg_btn">Register</a>
                    <? endif ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>
    <?= $paging_upcoming->get_html() ?>
    <br /><hr /><br />
    <h2>Prior Webinars</h2>
    <table width="100%" align="center" cellpadding="3" cellspacing="1" border="0">
        <tr>
            <th><?= strings::sort('webinars_name_past', 'Name') ?></th>
            <th><?= strings::sort('webinars_datetime_past', 'Date') ?></th>
        </tr>
        <? foreach($past as $webinar): ?>
            <tr>
                <td><a href="/members/webinars/details.php?webinars_id=<?= $webinar['webinars_id'] ?>"><?= $webinar['webinars_name'] ?></a></td>
                <td><?= times::from_mysql_utc($webinar['webinars_datetime'],'m/d/Y  h:i a T') ?></td>
            </tr>
        <? endforeach; ?>
    </table>
    <?= $paging_past->get_html() ?>
</div>

<? include VIEWS. '/members/footer.php' ?>
