<?
    $page_title        = SITE_NAME;
    $page_name         = 'Training';
    $sub_name		   = 'Training';
    $page_keywords     = '';
    $page_description  = '';
?>
<? include VIEWS. '/members/header.php' ?>

<script type="text/javascript">
    window.addEvent('load', function(){
        //training notification
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-notification-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                $('notifications_training_update').set('value', responseText);
            }
        });
        $('notifications_training_update').addEvent('click', function(event){
            webinarsRequest.send('yn=' + this.get('checked')+'&type=notifications_training_update');
        });
    })
    window.addEvent('domready', function(){
        <? /* open or collapse infobox? */ ?>
        <? if(!$infobox): ?>
           $$('.page-desc').setStyle('display', 'none');
           $$('.info_text').set('html','Show Text Box'); 
        <? endif; ?>
    })
    window.addEvent('load', function(){
        //infobox settings
        var webinarsRequest = new Request({
        url: '<?= FULLURL; ?>/members/ajax-change-infobox-setting.php',
        method: 'get',
            onSuccess: function(responseText){
                if(responseText == 'on'){
                    $$('.page-desc').reveal();
                    $$('.info_text').set('html','Hide Text Box');
                }
                else {
                    $$('.page-desc').dissolve();
                    $$('.info_text').set('html','Show Text Box');
                }
            }
        });
        $$('.info_icon, .info_text').addEvent('click', function(event){
            if($$('.page-desc')[0].get('style').match(/display: none/gi)){
                var show = 'true';
            }
            else {
                var show = 'false';
            }
            webinarsRequest.send('yn=' + show+'&type=members_settings_infobox_training');
        });

    })          
</script>

<div id="content">
    <span class="info_icon"></span>
    <p class="page-desc">
        <span style="font-size: 150%">Training</span>
        <br /><br />
        Self-paced training is provided for you and your staff to fulfill OSHA requirements, with additional 
        topics for improved safety and security. The link below will <em style="text-decoration:underline"><b>open an additional browser</b></em> 
        window to your training module.  <em style="text-decoration:underline">A separate user name and password will be emailed to you from COGGNO within 
        12 hours of your registration.</em> <br />
       
        <br /><br />
        If you would like to add a shortcut directly to the training module on your desktop, see the link below. This can 
        be helpful for employees to have direct access to the training, rather than having them log into the main <?= SITE_NAME ?> site.
        <br /><br />
         (See tutorial for help)
        <br /><br />
        <?= html::checkbox(1,'notifications_training_update',$notifications['notifications_training_update']); ?> Check box to 
        be notified of any updates to training courses available
    </p>
    <span class="info_text">Hide Text Box</span>
    <br class="clear" />       
    <h1>Training</h1>
    <p>To set up employees for the training all at once, rather than one employee at a time, use the provided spreadsheet.  <a href="/data/Multiple User Sign-Up Sheet.xls">Multiple User Sign-Up Sheet</a>.</p>
    <p>Don't forget the four required OSHA trainings you need to provide:</p>
    <ol style="margin-left:20px">
        <li>Bloodborne Pathogens</li>
        <li>Emergency Response</li>
        <li>Hazard Communication</li>
        <li>Lockout/Tagout</li>
    </ol>
    
    <a href="http://wakeupcall.coggno.com/" target="_blank" class="center" style="text-align:center; text-decoration:none">
        <div style="width:200px" class="green_btn">Launch Training Module</div>
    </a>
    <br />
    <p>To add a shortcut directly to the Training Module on your desktop, click here: <a href="<?= FULLURL; ?>/members/webinars/shortcut-download.php" target="_blank">WAKEUP CALL Training Module</a></p>
</div>

<? include VIEWS. '/members/footer.php' ?>
