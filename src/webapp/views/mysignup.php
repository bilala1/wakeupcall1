<?
$page_title = 'Sign up for WAKEUP CALL Risk Management Tools';
$page_description = 'Sign up for our online risk management tools - includes: hospitality and insurance resources and training, HR services, claim management, employment law hotline, SDS library, hospitality industry news, special offers and more....';

include VIEWS . '/header.php';
?>
<style type="text/css">
.heading{ 
	background:#999999;
	color:#fff;
	font-size:17px;
	padding: 5px 15px;
	margin:0;
	}
.tab{
	width:600px;
	position: absolute;
	top:240px;
	background:#fff;
    display:none;
}

.tabContent{
	padding:15px;
	border: 1px solid #999999;
    height:330px;
    overflow-y:auto;
}
#tab-links li{
    border:1px solid #fff;
	border-left: none;
}
#tab-links li.tab-active{
	border:1px solid #999999;
	border-left: none;
}

.tab .close {
	background: url(/images/dialog-close.png) no-repeat;
	width: 24px;
	height: 24px;
	display: block;
	cursor: pointer;
	top: -5px;
	right: -4px;
	position: absolute;
}

.green{color:#4e8e4d}
.checkmark{ padding:0; margin:0 0 10px 0;}
.checkmark li{
	list-style: none;
	background: url(/images/checkmark-little.jpg) no-repeat 0 3px; 
	padding:3px 18px;
}
.big-btn{
	width:80%;
	margin:0 auto;
}

	.big{
		font-size:25px;
		color: #D31145;
		display:block;
		margin-top:10px;
	}

</style>

<div id="content">
	<h1 class="bigTitle">Feeling Like a Target for Regulations, Penalties &amp; Lawsuits?</h1>

    <div style="width:620px;" class="left">
    	<div class="testimonial left" style="padding-left: 24px;">
            <div class="quotes left">
                <p>...the WAKEUP CALL HR services are extremely helpful. They respond very quickly and actually 
                being able to talk to someone on the phone is refreshing.<img src="/images/quote-right.jpg" style="vertical-align:bottom" /> </p>
            </div>
        </div>
        
        <div class="testimonial right">
            <div class="quotes left">
                <p>The Employment Law Hotline has saved us attorney costs and provided us with professional, 
                    timely advice. We find this resource on WAKEUP CALL to be invaluable.
                    <img src="/images/quote-right.jpg" style="vertical-align:bottom" />
                </p>
            </div>
        </div>
        
        <br class="clear" />
        
        <div class="testimonial left" style="padding-left: 24px;">
            <div class="quotes left">
        	<p>The OSHA Kit available on WAKEUP CALL made it easy to address the black hole that 
            is OSHA compliance. Everything we needed to be in compliance, in one place. It was great.
             <img src="/images/quote-right.jpg" style="vertical-align:bottom" />
            </p>
            </div>
        </div>
        
        <div class="testimonial right">
            <div class="quotes left">
                <p>Having an easy to use 
                 resource, like WAKEUP 
                 CALL, available to my staff
                 24 hrs a day, has made it
                 easier to implement a safety
                 program, which we couldn't
                 manage before.
                <img src="/images/quote-right.jpg" style="vertical-align:bottom" />
                </p>
            </div>
        </div>        
    
    </div> <!--leftcol-->
    
    <div class="right" style="width:300px" align="center">
        <div id="signup-box-mask">
            <a href="" id="signup-box">
                <h2>Don't Get Caught With</h2>
                <ul class="no-disc" style="margin-left:0">
                    <li>&bull; Wrongful Termination Lawsuits</li>
                    <li>&bull; Out-Of-Control Claims</li>
                    <li>&bull; OSHA Compliance Fines</li>
                </ul>
                <div class="big-btn-wrapper"><span class="big-btn">Sign Up Now!</span></div>
            </a>
        </div>
        <strong class="big">FREE 15-DAY TRIAL!</strong>
    </div>
    
    <br class="clear" />
    
    <h2 style="padding-left:30px; margin-top:25px;">Some of What's Inside Includes:</h2>
    
    <div class="center" style="position: relative; min-height:100px;">
        <ul class="list" style="position: absolute; left:95px; top:0">
            <li>Live HR Services</li>
            <li>Online Training- 24/7</li>
            <li>Employment Law Hotline</li>
        </ul>
        <ul class="list" style="position: absolute; left:410px; top:0;">
            <li>OSHA Kit</li>
            <li>Claims Management</li>
            <li>Certificate Tracking</li>
        </ul>
        <ul class="triangle blue list" style="position: absolute; left:600px; top:0;" id="tab-links">
            <li><a href="" data-popid="pop-overview">Overview Of Services</a></li>
            <li><a href="" data-popid="pop-additional">Additional Benefits</a></li>
            <li><a href="" data-popid="pop-membership">Membership Pricing</a></li>
        </ul>
     </div>
        
        
        <div class="tab" id="pop-overview">
            <a class="close"></a>
            <h2 class="heading">Overview Of Services</h2>
            <div class="tabContent">
                <p><strong>Human Resources Services</strong>  Unlimited access to speak with live HR specialists 
                providing state-specific information and forms.</p>
                
                <p><strong>Employment Law Hotline  Access</strong> to a national law firm, with offices coast to coast, 
                to address your Employment Law questions and concerns.</p>
                
                <p><strong>24/7 Online Training</strong>  Designed for you and your team's convenience, users can login 
                and take the training courses you select for them, at their own pace. You are provided with reports on the 
                progress and completion of everyone's training, including Certificates of Completion. California required 
                AB1825 Sexual Harassment is included.</p>
                
                <p><strong>Extensive Library of Industry-Specific, Downloadable Documents and Forms; Includes All Required 
                OSHA Documents</strong>;  Includes required OSHA documents, reusable and savable incident reports, HR forms, 
                EE handouts, sample waivers and more.</p>
                
                <p><strong>Claims Management</strong>  Submit claims directly to your carriers, while WAKEUP CALL provides a log for your records.
                This helps you to manage losses, monitor trends, keep claims from getting out of hand, and update your entire team.</p>
                
                <p><strong>Certificate of Insurance Monitoring</strong>  Keep your risk transfer program up-to-date and easy to manage. Track 
                certificates provided by all of your vendors and consultants. <em>(This is becoming a requirement with some insurance carriers.)</em></p>
                
                <p><strong>Webinars</strong>  On a regular basis, WAKEUP CALL has timely webinars to provide valuable insight for your 
                team on the hottest topics and areas of concern in the hospitality industry.</p>
                
                <p><strong>Intranet Option Included for Owners of Multiple Locations</strong>  Private corporate postings and 
                data storage for Quick &amp; Easy communications between you and all your properties.</p>
                
                <p><strong>Live Message Boards/Forums</strong>  Discuss general and specific topics with all WAKEUP CALL members. Attorneys and Risk 
                Managers also participate to help provide answers to your questions.</p>
                
                <p><strong>Up-to-Date News</strong>  The hospitality industry is continually changing. So that you can stay up-to-date on the most 
                important issues in the industry, WAKEUP CALL brings current industry news to you daily from multiple sources.</p>
                
                <p><strong>Searchable SDS Library</strong>  Gives you access to over 2.7 million products and chemicals, and provides the tools
                you need to remain in compliance with SDS OSHA's SDS requirements.</p>
                
                <p><strong>FAQ Section</strong>, a continuously populated, up-to-date feature, provides you access to the answers to the 
                most commonly asked risk management questions.</p>
                
                <p>And more to come . . .</p>
            </div>
        </div>
        
        <div class="tab" id="pop-additional">
            <a class="close"></a>
            <h2 class="heading">Additional Benefits</h2>
            
            <div class="tabContent">
                <p><strong>Incredible Value</strong>  WAKEUP CALL is full of valuable tools and resources &mdash; all available to you at a <em>fraction</em> of 
                what it would cost if you paid for these services individually elsewhere. (See pricing information for more details.)</p>
                
                <p><strong>Intuitive Navigation &amp; Interface</strong>  Using WAKEUP CALL requires minimal computer skills to access all 
                of the tools and resources. Intuitive ease of use is a priority throughout the entire WAKEUP CALL site.</p>
                
                <p><strong>No More Reliance on a Carrier or Broker</strong>  You no longer need to be bound to a broker or carrier to provide 
                you with risk management resources. This often requires you to contact their risk management phone representative, 
                who then accesses their limited, generic resources. With WAKEUP CALL as your resource, you are able to manage 
                and locate all tools and resources, 24/7.</p>
                
                <p><strong>Access to National Vendors and Service Providers</strong>  WAKEUP CALL has arranged for free or discounted rates 
                exclusively for WAKEUP CALL members on commonly used products and services. A few of the services include 
                background searches, drug testing, MVR's, site inspections and direct access to attorneys at our members-only discounted rates.</p>
                
                <p><strong>We want to hear from you!</strong>  WAKEUP CALL is a dynamic site, changing as the hospitality industry changes. In 
                addition to its own development team, WAKEUP CALL is constantly seeking input from members to keep current 
                on your most critical issues.</p>
            </div>
        </div>
        
        <div class="tab" id="pop-membership">
            <a class="close"></a>
            <h2 class="heading">Membership Pricing</h2>
            <div class="tabContent">
                <h3>Value or Low Price? </h3>
                <p>In today's economy, we all want value, but we <em>need</em> a low price.  WAKEUP CALL accommodates both of your concerns:</p>
                
                <h4 class="green">VALUE</h4> 
                <p>What other single site provides you:</p>
                <ul class="checkmark">
                    <li>An HR specialist to speak with</li>
                    <li>An Employment Law Attorney to answer your questions</li>
                    <li>A constantly updated library containing current documents for your OSHA, Safety and HR needs</li>
                    <li>35 online training courses, including sexual harassment</li>
                    <li>Message Boards/Forums with many participating hospitality risk managers and attorneys</li>
                    <li>A complete and up-to-date SDS library</li>
                    <li>The latest breaking industry news</li>
                    <li>An Intranet option to serve multiple locations</li>
                    <li>An in-site concierge service to help you find what you are looking for</li>
                </ul>
                
                <h4 class="green">LOW PRICE  </h4> 
                <p>If priced separately, these resources would cost $3000-$5000 per year.  </p>
                
                <p>WAKEUP CALL is offering these services to you, online, all in one place, for the low 
                introductory price of <strong class="green">$<?= YEARLY_COST ?> per year</strong>.  <span style="font-size:10px">(Options available for multiple locations.)</span></p>
            </div>
        </div>
        
    <div id="register-form"<?= $errors ? ' style="display:block"' : '' ?>>
        <a class="close"></a>
        <? include VIEWS . '/register-form.php'; ?>
    </div>
</div>
<script type="text/javascript">
window.addEvent('domready', function(){
    $$('#tab-links a').addEvent('click', function(e){
        e.stop();
        
        hide_popups();
        
        $$(this).getParent().addClass('tab-active');
        $$('#'+$(this).get('data-popid')).setStyle('display', 'block');
    });
    
    document.addEvent('click', hide_popups);
    $$('.tab .close').addEvent('click', hide_popups);
    $$('.tab').addEvent('click', function(e){
        e.stop();
    });
});

function hide_popups(){
    $$('.tab').setStyle('display', 'none');
    $$('#tab-links li').removeClass('tab-active');
}
</script>
<? include VIEWS . '/footer.php'; ?>