<?
if(!$notice && isset($_REQUEST['notice'])){
    $notice = urldecode($_REQUEST['notice']);
}
if($_REQUEST['error']){
    $errors[] = html::filter(urldecode($_REQUEST['error']));
}
if($errors){
    $error_string = '';
    foreach($errors as $key => $error){
        $error_string .= is_int($key) ? $error.'<br />' : '';
    }
}
?>
<? if($notice): ?>
<p class="notice"><?= html::filter($notice) ?></p>
<? endif ?>
<? if($errors && !$error_string): ?>
<p class="errors">There are errors below</p>
<? elseif($error_string): ?>
<p class="errors"><?= $error_string ?></p>
<? endif ?>