<?
$page_title = SITE_NAME;
$page_description = 'Notification Settings';
?>
<? include VIEWS . '/header.php' ?>
<style type="text/css">
    label {
        font-weight: bold;
        width: 150px;
        display: inline-block;
        text-align: left;
    }

    .normal {
        font-weight: normal;
    }

    .block {
        display: block;
    }
</style>
<? include VIEWS . '/members/notice.php' ?>
<div id="content">
    <div id="middlecol">
        <h2>Unsubscribe from our Weekly Newsletter</h2>

        <form action="notifications.php" method="post">
            <?= html::textfield('Email address:', $data, 'members_email_address', array(), $errors) ?>
            <br class="clear"/>
            <input type="submit" value="Unsubscribe" class="btn center"/>
        </form>
    </div>
</div>
<script type="text/javascript">
    window.addEvent('domready', function(){
        $$('#tab-links a').addEvent('click', function(e){
            e.stop();

            hide_popups();

            $$(this).getParent().addClass('tab-active');
            $$('#'+$(this).get('data-popid')).setStyle('display', 'block');
        });

        document.addEvent('click', hide_popups);
        $$('.tab .close').addEvent('click', hide_popups);
        $$('.tab').addEvent('click', function(e){
            e.stop();
        });
    });

    function hide_popups(){
        $$('.tab').setStyle('display', 'none');
        $$('#tab-links li').removeClass('tab-active');
    }
</script>
<? include VIEWS . '/footer.php'; ?>
