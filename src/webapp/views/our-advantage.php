<?  include VIEWS. '/header.php'?>
	
    
    <div id="content">
<!--    	<img src="/images/header-ouradvantage.jpg" alt="Our Advantage" id="top-image" />-->

        <div class="w540 left" style="min-height:350px" id="leftcol">
        	<img src="/images/img-ouradvantage.jpg" class="left"/>
        	
            <div class="w400 left" style="margin-left:20px">
                <h2 class="headline">Our Advantages</h2>
                <h3>The One-Stop Site for Online Hospitality Risk Management</h3>
        
                <p><strong>Intuitive Navigation &amp; Interface</strong> requires minimal computer skills to access all areas. Ease of 
                use is a priority throughout the design process of the entire WAKEUP CALL site.</p>
                
                <p><strong>Human Resources Services</strong> including unlimited access to speak with live specialists 
                providing state-specific information and forms.</p>
                
                <p><strong>Employment Law Email Hotline</strong>, at no additional cost you can contact our law firm, 
                with offices coast to coast, about employment law issues and concerns. </p>
            
                <p><strong>Live Message Boards/Forums</strong> allow you to discuss general and specific topics with all WAKEUP CALL 
                members. Attorneys and Risk Managers provide answers to your questions, specifically for you and your location. </p>
                
                <p><strong>Intranet Option Included for Owners of Multiple Locations</strong> It facilitates private corporate postings 
                and data storage for Quick &amp; Easy communications between you and all your properties.</p>
                
                <p><strong>The Claims Management</strong> function allows claim submissions directly to your carriers, while WAKEUP CALL
                provides a log for your records. Helps to manage losses and trending and keeps claims from getting out 
                of hand while updating your entire team. </p>
                
                <p><strong>Certificate of Insurance Monitoring</strong> keeps your risk transfer program up-to-date and easy to manage.</p>
                
                <p>WAKEUP CALL is your single source for <strong>Up-to- Date News</strong> on the hospitality industry, 
                from multiple sources. </p>
        
                <h3>Finally, the Solutions You Need are All in One Place</h3>
                
                <p><strong>24/7 Online Training</strong>, including the California required AB1825 Sexual Harassment, designed for you and your team's 
                convenience. Users can login and take the training courses you select for them, at their own pace. You are provided with 
                reports on the progress and completion of everyone's training, including Certificates of Completion.</p>
                
                <p>The extensive <strong>Library of Industry-Specific, Downloadable Documents and Forms; Includes All Required OSHA Documents, </strong>
                incident reports, HR forms, EE handouts, sample waivers and more.</p>
                
                <p><strong>No More Reliance on a Carrier or Broker</strong> requiring you to contact their risk management phone representative to 
                locate and access their limited, generic resources. With WAKEUP CALL as your sole resource, you are able to manage 
                and locate all resources, on all topics, 24/7.</p>
                
                <p>The WAKEUP Call <strong>Searchable SDS Library</strong> provides the tools you need to remain in compliance with SDS OSHA requirements.</p>
                
                <p><strong>The FAQ Section</strong>, a continuously populated, up-to-date feature provides you access to the answers to the most commonly 
                asked risk management questions.</p>
                
                <p><strong>Maintain an Open Line of Communication</strong> with WAKEUP CALL to learn of recommended current actions, or to 
                learn about future services. WAKEUP CALL is a dynamic site, changing as the hospitality industry changes. 
                In addition to its own development team, WAKEUP CALL is constantly seeking input from members to keep current on 
                your most critical issues.</p>
            </div>     
        </div>
         <? include  VIEWS. '/register.php'; ?>
        <br class="clear" />
    	
    </div>
    
<? include  VIEWS. '/footer.php'; ?>