<style type="text/css">
	#form .form-field label{
		margin-bottom:0;
		float:none;
		display:inline-block;
	}
	#form .form-field *{
		vertical-align:top;
	}
	#form input{
		float:none;
		display:inline;
	}

	.long-txt{ width: 60%}
	h3.headline{ font-size:18px; line-height:20px; font-weight: bold; margin:0;}
	h4{ color: #333; font-size: 14px}

	.free-trial{ 
		margin:0 0 25px;
		text-align: center;
	}

	.form-field{
		margin:3px 0;
		padding:0;
	}

	#form .form-error{
		margin:0;
		margin-left:33%;
	}
	.form-field .multiradio-container label {
		padding: 0 0 10px 4px;
	}
	#trial-form {
		display:none;
		background:#fff;
		position:absolute;
		top:-100px;
		right:0;
		width:415px;
		-moz-box-shadow:    0 0 1px 1px #cccccc;
		-webkit-box-shadow: 0 0 5px 1px #cccccc;
		box-shadow:         0 0 5px 1px #cccccc;
	}
	#trial-form a.close{
		background: url(/images/dialog-close.png) no-repeat scroll 0 0 transparent;
		cursor: pointer;
		display: block;
		height: 24px;
		left: -9px;
		position: absolute;
		top: -9px;
		width: 24px;
	}

	#trial-form-form .form-field label{
		margin-bottom:0;
		float:none;
		display:inline-block;
	}
	#trial-form-form .form-field *{
		vertical-align:top;
	}
	#trial-form-form input{
		float:none;
		display:inline;
	}
</style>
<form action="<?= http::current_page() ?>" method="post" id="form" name="form" style="margin-right:0; border:1px solid #ccc">
<? if ($errors): ?>
	<p class="errors">Please correct the errors below (in red)</p>
<? endif ?>
    <p id="form-msg"><strong style="font-size:15px">Please choose a business type to start your registration.</strong><br />
     (<span class="imp">*</span> Required Information)</p>
    <input type="hidden" name="music" id="music" />
	<?//= html::hidden($_REQUEST, 'type') ?>
	<?= html::hidden('register', 'type') ?>

	<? /* = html::multiradio('<span class="imp">*</span> Business Type', 'btype', array(
	  'hotel' => 'Individual (single location)',
	  'corporation' => 'Corporation/Management (multiple locations)'
	  ), $_REQUEST, $errors) */ ?>
    <div class="form-field">
        <span class="multiradio-container">
            <span style="display:inline-block">
				<?= html::radio('hotel', 'btype', $_REQUEST, array('id' => 'btype_0')) ?>
                <label for="btype_0" style="width:auto"> Individual
                    <span style="font-size: 80%; margin-left: 5px">(single location)</span>
                </label>
                <img class="tip" rel="An individual account is for single locations looking for answers to all their risk management questions and needs, employee safety training, extensive document library's,  plus much, much more. " src="<?= FULLURL; ?>/images/tip.jpg" />
                <br clear="all" />
            </span>
            <span style="display:inline-block">
				<?= html::radio('corporation', 'btype', $_REQUEST, array('id' => 'btype_1')) ?>
                <label for="btype_1" style="width:auto"> Corporation/Management
                    <span style="font-size: 80%; margin-left: 5px">(multiple locations)</span>
                </label>
                <img class="tip" rel="A Corporate/Management account is for organizations that have multiple locations they want to be able to communicate with.
					 It offers all the tools and services of an individual account plus a private intranet type area that can be used to post notices and documents for all your locations to use.
					 It also allows the review of all your location's usage; such as claims, training records, document libraries and more. It's a great tool to help you stay in touch and on top all your locations risk management concerns. " src="<?= FULLURL; ?>/images/tip.jpg" />
					 <? if (isset($errors['btype'])): ?>
	                <p class="error"><?= $errors['btype'] ?>
					<? endif ?>
					<br clear="all" />
            
    </span>
        </span></div>

	<div class="form-field">
		<?= html::textfield('Trial Code', $_REQUEST, 'trial_code', array('class' => 'text', 'maxlength' => 255), $errors) ?></a>
		<label>&nbsp;</label><span style="color:blue;">Need a Trial Code? <a id="requestTrailCode" style="color: blue;text-decoration: underline;cursor: pointer;">Click Here.</a></span>
	</div>

    <div id="ind-form">
		<? //= html::multiradio('<span class="imp">*</span> Industry Type', 'ind_type', array('hotel' => 'Hotel', 'resort' => 'Resort', 'spa' => 'Spa', 'other' => 'Other'), $_REQUEST, $errors) ?>
		<?= html::selectfield('<span class="imp">*</span> Industry Type', arrays::values_as_keys(array('Please Select', 'Hotel - Full Service/Resort', 'Hotel - Limited Service', 'Spa - Destination Spa', 'Other')), 'ind_type', $_REQUEST, array('style' => 'width:55%'), $errors) ?>

		<?= html::textfield('<span class="imp">*</span> Location Name', $_REQUEST, 'hotels_name', array('class' => 'long-txt', 'onChange' => 'hideLocErr();', 'maxlength' => 255), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Number of Rooms', $_REQUEST, 'hotels_num_rooms', array('class' => 'long-txt', 'maxlength' => 11), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Address', $_REQUEST, 'hotels_address', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> City', $_REQUEST, 'hotels_city', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
		<?= html::selectfield('<span class="imp">*</span> State', html::$us_states, 'hotels_state', $_REQUEST, array('style' => 'width:55%'), $errors) ?>
		<?= html::textfield(' <span class="imp">*</span> Zip', $_REQUEST, 'hotels_zip', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
        <br clear="all" />
        <strong>Account Administrator:</strong>
		<?= html::textfield('<span class="imp">*</span> First Name', $_REQUEST, 'hotels_members_firstname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>

		<?= html::textfield('<span class="imp">*</span> Last Name', $_REQUEST, 'hotels_members_lastname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Position/Title', $_REQUEST, 'hotels_members_title', array('class' => 'long-txt', 'maxlength' => 40), $errors) ?>        
		<?= html::textfield(' <span class="imp">*</span> Phone', $_REQUEST, 'hotels_members_phone', array('class' => 'long-txt', 'maxlength' => 20), $errors) ?>
		<?= html::textfield('Fax', $_REQUEST, 'hotels_members_fax', array('class' => 'long-txt', 'maxlength' => 20), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> Email<br />(User Name)', $_REQUEST, 'hotels_members_email', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> Re-enter Email', $_REQUEST, 'hotels_members_email2', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
		<?= html::passwordfield('<span class="imp">*</span> Password', $_REQUEST, 'hotels_pass', array('class' => 'long-txt'), $errors) ?>
        <div class="form-field">
            <label id="label-placeholder"></label>
            <em id="password-note" style="margin:-5px 30px 5px 0;font-size:11px">(Password must have a minimum of 6 characters)</em>
        </div>
		<?= html::passwordfield('<span class="imp">*</span> Re-enter Password', $_REQUEST, 'hotels_pass2', array('class' => 'long-txt'), $errors) ?>
        <br class="clear" />
    </div>
    <div id="corp-form">
		<?= html::textfield('<span class="imp">*</span> Company Name', $_REQUEST, 'corporations_name', array('class' => 'long-txt', 'maxlength' => 255, 'onChange' => 'hideLocErr();'), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Address', $_REQUEST, 'corporations_address', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> City', $_REQUEST, 'corporations_city', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
		<?= html::selectfield('<span class="imp">*</span> State', html::$us_states, 'corporations_state', $_REQUEST, array('style' => 'width:55%'), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Zip', $_REQUEST, 'corporations_zip', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
        <br clear="all" />
        <p style="text-align:center"><strong>Account Administrator:</strong></p>
		<?= html::textfield('<span class="imp">*</span> First Name', $_REQUEST, 'members_firstname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>

		<?= html::textfield('<span class="imp">*</span> Last Name', $_REQUEST, 'members_lastname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Position/Title', $_REQUEST, 'members_title', array('class' => 'long-txt', 'maxlength' => 40), $errors) ?>         
		<?= html::textfield('<span class="imp">*</span> Phone', $_REQUEST, 'members_phone', array('class' => 'long-txt', 'maxlength' => 20), $errors) ?>
		<?= html::textfield('Fax', $_REQUEST, 'members_fax', array('class' => 'long-txt', 'maxlength' => 20), $errors) ?>
		<?= html::textfield('<span class="imp">*</span> Email<br />(User Name)', $_REQUEST, 'members_email', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
        <?= html::textfield('<span class="imp">*</span> Re-enter Email', $_REQUEST, 'members_email2', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>

		<?= html::passwordfield('<span class="imp">*</span> Password', $_REQUEST, 'members_password', array('class' => 'long-txt'), $errors) ?>
		<div class="form-field">
			<label id="label-placeholder"></label>
			<em id="password-note" style="margin:-5px 30px 5px 0;font-size:11px">(Password must have a minimum of 6 characters)</em>
		</div>

		<?= html::passwordfield('<span class="imp">*</span> Re-enter Password', $_REQUEST, 'members_password2', array('class' => 'long-txt'), $errors) ?>

		<?= html::multiradio('<span class="imp">*</span> Corporate Office Separate from Hotels?', 'separate', array('Yes' => 'Yes', 'No' => 'No'), $_REQUEST, $errors) ?>

		<?= html::textfield('Number of Locations', $_REQUEST, 'corporate_locations', array('class' => 'long-txt', 'maxlength' => 11), $errors) ?>

        <div id="corporate_base">
			<?= html::textfield('Corporate Address', $_REQUEST, 'corporate_address', array('class' => 'long-txt', 'maxlength' => corporate_locations), $errors) ?>
			<?= html::textfield('City', $_REQUEST, 'corporate_city', array('class' => 'long-txt', 'maxlength' => corporate_locations), $errors) ?>
			<?= html::selectfield('State', html::$us_states, 'corporate_state', $_REQUEST, array('style' => 'width:55%'), $errors) ?>
			<?= html::textfield('Zip', $_REQUEST, 'corporate_zip', array('class' => 'long-txt', 'maxlength' => corporate_locations), $errors) ?>
        </div>
        <br clear="all" />
		<?= html::multiradio('Add location now?', 'other_loc', array('Yes' => 'Yes', 'No' => 'No'), $_REQUEST, $errors) ?>
		<? /* <div class="form-field">
		  <label for="">
		  <span>Add location now?</span>
		  </label>
		  <span class="multiradio-container">
		  <span>
		  <input id="other_loc_0" type="radio" name="other_loc" value="Yes">
		  <label for="other_loc_0" style="width:auto"> Yes</label>
		  <br clear="all">
		  </span>
		  <span>
		  <input id="other_loc_1" type="radio" name="other_loc" value="No">
		  <label for="other_loc_1" style="width:auto"> No</label>
		  <br clear="all">
		  </span>
		  </span>
		  </div> */ ?>
        <div id="other_locations" style="background-color:#E5E5E5; padding-top:5px">
			<? //= html::multiradio('<span class="imp">*</span> Industry Type', 'first_ind_type', array('hotel' => 'Hotel', 'resort' => 'Resort', 'spa' => 'Spa', 'other' => 'Other'), $_REQUEST, $errors) ?>
			<?= html::selectfield('<span class="imp">*</span> Industry Type', arrays::values_as_keys(array('Please Select', 'Hotel - Full Service/Resort', 'Hotel - Limited Service', 'Spa - Destination Spa', 'Other')), 'first_ind_type', $_REQUEST, array('style' => 'width:55%'), $errors) ?>
            <?= html::textfield('<span class="imp">*</span> Location Name', $_REQUEST, 'first_hotels_name', array('class' => 'long-txt', 'maxlength' => 255, 'onChange' => 'hideLocErr();'), $errors) ?>
			<?= html::textfield('<span class="imp">*</span> Number of Rooms', $_REQUEST, 'first_hotels_num_rooms', array('class' => 'long-txt', 'maxlength' => 11), $errors) ?>
			<?= html::textfield('<span class="imp">*</span> Address', $_REQUEST, 'first_hotels_address', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
			<?= html::textfield('<span class="imp">*</span> City', $_REQUEST, 'first_hotels_city', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
			<?= html::selectfield('<span class="imp">*</span> State', html::$us_states, 'first_hotels_state', $_REQUEST, array('style' => 'width:55%'), $errors) ?>
			<?= html::textfield(' <span class="imp">*</span> Zip', $_REQUEST, 'first_hotels_zip', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>
            <p style="padding: 5px; font-weight: bold;">More locations can be added after registration.</p>
        </div>
        <br clear="all" />
    </div>

    <div class="form-field">
        <div style="float:left; margin-right:10px;">
			<?= html::checkbox(1, 'terms', 0, array('style' => 'float:right; margin-top:5px')) ?>
        </div>
        <label for="terms" style="clear:right; text-align:left;width:auto;">I have read and agree to the
			<noscript>
			<style type="text/css">
				a#termsofuse {
					display:none;
				}
			</style>
			<a href="/terms.php" target="_blank">terms of use</a>
			</noscript>
			<a href="#termsofuse" id="termsofuse" onclick="return showTerms()">terms of use</a>
		</label>
    </div>

    <?/*<input type="submit" name="register_btn" value="Register" class="btn center" id="reg-btn" style="display: block" />*/?>
    
    <a href="#" id="type_register" class="blue-reg"><span class="bigTitle">Register</span> Try Your Free 15 Day Trial</a>
    <a href="#" id="type_join" class="red-reg"><span class="bigTitle">Join</span> Skip the Free Trial for Full Access</a>
</form>

<div id="locErr">
    This location is already using the WAKEUP CALL service.  Please change your location or, if you are the owner of this location, contact us.
</div>

<script type="text/javascript" src="/js/AutoComplete.js"></script>
<script type="text/javascript">
	//<![CDATA[
	function showTerms(e) {
	    var termsPopup = new Element('div', {
	        html: '<div id="inner_terms" style="width: 100%; height: 100%; overflow-y: auto;"></div>'
	    }).MooDialog({
	        size: {
	            width: 500,
	            height: 400
	        }
	    });

	    var termsText = new Request({
	        url: FULLURL + '/terms.php',
	        onSuccess: function (response) {
	            response = response.replace('class="title"', '');
	            $('inner_terms').set('html', response);
	        }
	    }).send();
        
        return false;
	}



	document.getElementById('music').value = 314;
    
    /*
    $('type_join').addEvent('click',function(){
        $('reg-btn').set('value','Join');
    });
     */

	function formatPhone(input) {
	    if(input.value.length == 1 && input.value != '(') {
	        input.value = '(' + input.value;
	    }
	    if(input.value.length == 4) {
	        input.value = input.value + ') ';
	    }
	    if(input.value.length == 5) {
	        var spare = input.value.substr(input.value.length - 1);
	        if(spare != ')') {
	            input.value = input.value.substr(0, input.value.length - 1) + ') ' + spare;
	        } else {
	            input.value = input.value + ' ';
	        }
	    }
	    if(input.value.length == 6) {
	        var spare = input.value.substr(input.value.length - 1);
	        if(spare != ' ') {
	            input.value = input.value.substr(0, input.value.length - 1) + ' ' + spare;
	        }
	    }
	    if(input.value.length == 10) {
	        var spare = input.value.substr(input.value.length - 1);
	        if(spare != '-') {
	            input.value = input.value.substr(0, input.value.length - 1) + '-' + spare;
	        }
	    }
	    if(input.value.length > 14) {
	        input.value = input.value.substr(0, input.value.length - 1);
	    }
	}

	function hideLocErr() {
	    $('locErr').hide();
	}

	function hideCorpBase() {
	    $('corporate_base').setStyle('display', 'none');
	}

	function showCorpBase() {
	    $('corporate_base').setStyle('display', 'block');
	}

	function showOtherLoc() {
	    $('other_locations').setStyle('display', 'block');
	}

	function hideOtherLoc() {
	    $('other_locations').setStyle('display', 'none');
	}

	function showCorpForm() {
	    $('corp-form').setStyle('display', 'block');
	    //$('reg-btn').setStyle('display', 'block');
	}

	function hideCorpForm() {
	    var len = document.form.other_loc.length;
	    for(i = 0; i < len; i++) {
	        document.form.other_loc[i].checked = false;
	    }
	    $$('#corp-form input[type=text]').each(function (e) {
	        e.set('value', '');
	    });
	    $('corp-form').setStyle('display', 'none');
	}

	function showIndForm() {
	    $('ind-form').setStyle('display', 'block');
	    //$('reg-btn').setStyle('display', 'block');
	}

	function hideIndForm() {
	    var len = document.form.ind_type.length;
	    for(i = 0; i < len; i++) {
	        document.form.ind_type[i].checked = false;
	    }
	    $$('#ind-form input[type=text]').each(function (e) {
	        e.set('value', '');
	    });
	    $('ind-form').setStyle('display', 'none');
	}

	function type_clicked() {
	    var myVerticalSlide = new Fx.Slide('slide_up');
	    myVerticalSlide.slideOut();

	    myVerticalSlide2.slideIn();
	}

	function show_error() {
	    //$('locErr').position({y:this.elem.getNext().getPosition($('content')).y}).show();
	}

	var myVerticalSlide2;
	window.addEvent('domready', function (evt) {
	    $('hotels_members_phone').addEvent('keyup', function (event) {
	        if(event.event.keyCode != '8' && event.keyCode != '46') {
	            formatPhone(this);
	        }
	    });
	    $('hotels_members_fax').addEvent('keyup', function (event) {
	        if(event.event.keyCode != '8' && event.keyCode != '46') {
	            formatPhone(this);
	        }
	    });
	    $('members_phone').addEvent('keyup', function (event) {
	        if(event.event.keyCode != '8' && event.keyCode != '46') {
	            formatPhone(this);
	        }
	    });
	    $('members_fax').addEvent('keyup', function (event) {
	        if(event.event.keyCode != '8' && event.keyCode != '46') {
	            formatPhone(this);
	        }
	    });

	    /*$$('input[name=type]').each(function(e) {
				if(e.get('value') == 'corporation') {
					if(e.checked == true) {
						showCorpForm();
					}
				}
				else if(e.get('value') == 'hotel') {
					if(e.checked == true) {
						showIndForm();
					}
				}
			});*/

	    $('type_register').addEvent('click', function () {
	        //$('reg-btn').set('value','Register');
	        $('type').set('value', 'register');
	        this.getParents('form')[0].fireEvent('submit')
	        //$('included-free-tab').fireEvent('click');
	        //type_clicked();
	        // $('type_join').dissolve();
	        return false;
	    });
	    $('type_join').addEvent('click', function () {
	        //$('reg-btn').set('value','Join');
	        $('type').set('value', 'join');
	        this.getParents('form')[0].fireEvent('submit')
	        //$('pricing-tab').fireEvent('click');
	        //type_clicked();
	        //  $('type_register').dissolve();
	        return false;
	    });

	    $('btype_0').addEvent('click', function () {
	        hideCorpForm();
	        showIndForm();
	    });
	    $('btype_1').addEvent('click', function () {
	        hideIndForm();
	        showCorpForm();
	    });
	    $$('[name=btype]:checked').fireEvent('click');

	    new AutoComplete($('hotels_name'), 'ajax-search.php', show_error);
	    new AutoComplete($('corporations_name'), 'ajax-search.php', show_error);
	    new AutoComplete($('first_hotels_name'), 'ajax-search.php', show_error);

	    $$('#ind-form input[type=radio], #ind-form select').addEvent('click', field_changed);
	    $$('#ind-form input[type=text], #ind-form .input-text').addEvent('keydown', field_changed);

	    function field_changed(e) {
	        if($$('[name=btype]:checked').length == 0) {
	            e.stop();
	            alert('You must select either Individual or Corporation/Management first');
	        }
	    }

	    $('other_loc_0').addEvent('click', showOtherLoc);
	    $('other_loc_1').addEvent('click', hideOtherLoc);
	    $$('[name=other_loc]:checked').fireEvent('click');

	    $$('[name=other_loc], [name=btype]').addEvent('click', function () {
	        $('locErr').hide();
	    });

	    $$('#ind_type, #first_ind_type').addEvent('change', function () {
	        if(this.get('value') == 'Other') {
	            $('hotels_num_rooms').setStyle('display', 'none').getParent().grab(new Element('span', {
	                text: 'Does Not Apply'
	            }).addClass('rooms_msg'));
	            $('first_hotels_num_rooms').setStyle('display', 'none').getParent().grab(new Element('span', {
	                text: 'Does Not Apply'
	            }).addClass('rooms_msg'));
	        } else {
	            $$('#hotels_num_rooms, #first_hotels_num_rooms').setStyle('display', 'inline');
	            $$('.rooms_msg').destroy();
	        }
	    });

	    $('form').addEvent('submit', function () {
	        if(!$('terms').checked) {
	            alert('You must agree to the terms of use');
	            return false;
	        }
	        $('form').submit();
	    });

	    new Tips($$('.tip'), {
	        fixed: true,
	        hideDelay: 1000,
	        offset: {
	            x: 0
	        }
	    });

	    $$('body').addEvent('click:relay(.tip-wrap)', function (e) {
	        e.stopPropagation();
	        e.stop();
	    });
	    document.addEvent('click', function () {
	        $$('.tip-wrap').hide();
	    });
	});

	/*window.addEvent('beforeunload', function(){
			$('form')
				.set('send', {async:false})
				.send();
				});*/
	//]]>
	function clearTrialForm()
	{
		$('trial_code_firstname').set('value','');
		$('trial_code_lastname').set('value','');
		$('trial_code_email').set('value','');
		$('trial_code_phone').set('value','');
	}
	window.addEvent('load', function () {
        var signupBox = $('signup-box');
        if ( !(signupBox === null) ) {
            $('signup-box').addEvent('click', function (e) {
                e.stop();
                window.location = "signup-direct.php";
            });
        }

		$('requestTrailCode').addEvent('click', function(e) {
			e.stop();
			clearTrialForm();
            // only hide if display the popup form
            var signupBox = $('signup-box');
            if ( !(signupBox === null) ) {
                $('register-form').hide();
            }
			$('trial-form').show();
		});

		$$('#trial-form a.close').addEvent('click', function () {
			$('trial-form').hide();
			$$('body').setStyle('height', 'auto');
		});

		$('type-trial-code').addEvent('click', function (e) {
			e.stop();
			console.log(this.getParents('form')[0].toQueryString());
			$('trial-code-error').set('html','');
			if (!$('trial_code_firstname').get('value')) {
				$('trial-code-error').set('html','Please enter your first name.');
				return false;
			}
			if (!$('trial_code_lastname').get('value')) {
				$('trial-code-error').set('html','Please enter your last name.');
				return false;
			}
			if (!$('trial_code_email').get('value')) {
				$('trial-code-error').set('html','Please enter your email.');
				return false;
			}
			if (!$('trial_code_phone').get('value')) {
				$('trial-code-error').set('html','Please enter your phone.');
				return false;
			}
			new Request({
				url: '/request-trial-code.php',
				onSuccess: function () {
					alert('Thank you! Your Free Trial code request has been received. A Free Trial code will be emailed to you within one business day. Please contact us with any questions at info@wakeupcall.net.');
					window.location.href = "/";
				},
				onFailure: function() {
					alert('Sorry--cannot register for a free trial at this time.');
				}}).post($('trial-form-form'));
			return false;
		});
	});
</script>