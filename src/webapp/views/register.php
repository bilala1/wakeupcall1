<style type="text/css">
	#form .form-field label{
		margin-bottom:0;
		float:none;
		display:inline-block;
	}
	#form .form-field *{
		vertical-align:top;
	}
	#form input{
		float:none;
		display:inline;
	}

	.long-txt{ width: 60%}
	h3.headline{ font-size:18px; line-height:20px; font-weight: bold; margin:0;}
	h4{ color: #333; font-size: 14px}

	.free-trial{ 
		margin:0 0 25px;
		text-align: center;
	}

	.form-field{
		margin:3px 0;
		padding:0;
	}

	#form .form-error{
		margin:0;
		margin-left:35%;
	}
	.form-field .multiradio-container label {
		padding: 0 0 10px 4px;
	}
</style>

<script type="text/javascript" src="/js/AutoComplete.js"></script>
<div class="right" style="width:310px; height:auto;">
    <div class="free-trial orange-box">
        <h3 class="headline">Register below for your<br />
			FREE 15-DAY  <br />TRIAL MEMBERSHIP</h3>
    </div>

	<? // if(!$_REQUEST['type']): ?>
    <div id="slide_up">
        <ul id="register_points">
            <li>Live Human Resource Services</li>
            <li>Employment Law Hotline</li>
            <li>24/7 Online Training</li>
            <li>100's of Industry Specific Downloadable Documents</li>
            <li>Claims Management</li>
            <li>Certificate of Insurance Tracking</li>
            <li>Intranet Options for Multiple Locations</li>
            <li>SDS Library</li>
            <li>Up to Date Industry News</li>
            <li>Special Offers/Discounts for Additional Services</li>
            <li>More and More Being Added</li>
        </ul>
    </div>    
	<? if (http::current_page() == 'signup.php'): ?>
		<a href="#" id="type_register" class="blue-reg"><span class="bigTitle">Register</span> Try Your Free 15 Day Trial</a>
		<a href="#" id="type_join" class="red-reg"><span class="bigTitle">Join</span> Skip the Free Trial for Full Access</a>
		<br />
	<? else: ?>
		<a href="signup.php?type=register" class="blue-reg"><span class="bigTitle">Register</span>Try Your Free 15 Day Trial</a> 
		<a href="signup.php?type=join" class="red-reg"><span class="bigTitle">Join</span> Skip the Free Trial for Full Access</a>
		<br />
	<? endif ?>

	<? // endif ?>

	<? if (http::current_page() == 'signup.php'): ?>
		<? include 'register-form.php' ?>
	<? endif ?>
</div>
<div id="locErr">
    This location is already using the WAKEUP CALL service.  Please change your location or, if you are the owner of this location, contact us.
</div>