
<style type="text/css">
label{
	margin-bottom: 10px;
	width:35%;
}
.long-txt{ width: 55%}
h3.headline{ font-size:18px; line-height:20px; font-weight: bold; margin:0;}
h4{ color: #333; font-size: 14px}

.free-trial{ 
	margin:0 0 25px;
	text-align: center;}
</style>

<script type="text/javascript" src="/js/TextboxList.js"></script>
<script type="text/javascript" src="/js/TextboxList.Autocomplete.js"></script>

    
    <div class="right" style="width:300px; height:auto;">
        <div class="free-trial">
            <h3 class="headline">Register here for the<br />
            FREE TRIAL MEMBERSHIP</h3>
            <h4>Receive Free and Discounted <br />valuable Services from <br />our Strategic Alliance Providers</h4>
        </div>

        <h2 class="headline">Register <small class="imp" style="font-size:11px">(* required field)</small></h2>
        
        <form action="" method="post" id="form" name="form" style="margin-right:0; border:1px solid #ccc">
            <p id="form-msg">Please choose a business type to start your registration.</p>
            <input type="hidden" name="music" id="music" />
            <input type="hidden" name="btype" id="btype" value="" />
            <label style="margin-bottom:25px"><span class="imp">*</span> Business Type</label>
            <input type="hidden" name="type" value="" checked="checked" />
            <input type="radio" name="type" id="type-corporation" value="corporation" <?= $_REQUEST['type'] == 'corporation' ? 'checked="checked"' : ''?> 
            onClick="hideIndForm();showCorpForm();"/> 
            <label for="type-corporation" class="normal">Corporation</label><br />
            <input type="radio" name="type" id="type-hotel" value="hotel" <?= $_REQUEST['type'] == 'hotel' ? 'checked="checked"' : ''?>  
            onClick="hideCorpForm();showIndForm();"/> 
            <label for="type-hotel" class="normal">Individual</label>
            <br class="clear" />
            
            <?= $errors['type'] ? '<p class="error">Please select one</p>' : '' ?>

            <div id="corp-form">
                <?= html::textfield('<span class="imp">*</span> Company Name', $_REQUEST, 'company_name', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> Address', $_REQUEST, 'company_address', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> City', $_REQUEST, 'company_city', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> State', $_REQUEST, 'company_state', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> Zip', $_REQUEST, 'company_zip', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> First Name', $_REQUEST, 'members_firstname', array('class' => 'long-txt'), $errors) ?>
                
                <?= html::textfield('<span class="imp">*</span> Last Name', $_REQUEST, 'members_lastname', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> Phone', $_REQUEST, 'members_phone', array('class' => 'long-txt'), $errors) ?>
                <?= html::textfield('<span class="imp">*</span> Email', $_REQUEST, 'members_email', array('class' => 'long-txt'), $errors) ?>
                
                <?= html::passwordfield('<span class="imp">*</span> Password', $_REQUEST, 'members_password', array('class' => 'long-txt'), $errors) ?>
               
                <?= html::passwordfield('<span class="imp">*</span> Re-enter Password', $_REQUEST, 'members_password2', array('class' => 'long-txt'), $errors) ?>
                <em class="right" style="margin:-5px 30px 5px 0;font-size:11px">(Password must have a minimum of 6 characters)</em>
                <?= html::textfield('Number of Locations', $_REQUEST, 'corporate_locations', array('class' => 'long-txt')) ?>
                <br clear="all" />
                <label for="base" style="vertical-align:middle"><span class="imp">*</span> Based at corporate address?</label>
                <div style="vertical-align:middle">
                <input type="radio" name="base" value="Yes" <?= $_REQUEST['base'] == 'Yes' ? 'checked="checked"' : ''?> style="vertical-align:middle;" onClick="hideCorpBase();" />&nbsp;Yes <br />
                <input type="radio" name="base" value="No" <?= $_REQUEST['base'] == 'No' ? 'checked="checked"' : ''?> style="vertical-align:middle;" onClick="showCorpBase();" />&nbsp;No <br />
                <div class="error" style="display:block"><?= $errors['base']; ?></div>
                
                </div>
                
                <div id="corporate_base">
                    <?= html::textfield('Corporate Address', $_REQUEST, 'corporate_address', array('class' => 'long-txt')) ?>
                    <?= html::textfield('City', $_REQUEST, 'corporate_city', array('class' => 'long-txt')) ?>
                    <?= html::textfield('State', $_REQUEST, 'corporate_state', array('class' => 'long-txt')) ?>
                    <?= html::textfield('Zip', $_REQUEST, 'corporate_zip', array('class' => 'long-txt')) ?>
                </div>
                <br clear="all" />
                <div style="vertical-align:middle">
                    <label for="other_loc" style="vertical-align:middle">Sign up other locations now?</label>
                    <input type="radio" name="other_loc" value="Yes" <?= $_REQUEST['other_loc'] == 'Yes' ? 'checked="checked"' : ''?> style="vertical-align:middle" onClick="showOtherLoc();" />&nbsp;Yes<br />
                    <input type="radio" name="other_loc" value="No" <?= $_REQUEST['other_loc'] == 'No' ? 'checked="checked"' : ''?> style="vertical-align:middle;" onClick="hideOtherLoc();" />&nbsp;No
                </div>
                
                <div id="other_locations">
                    <?= html::textfield('User name (e-mail)', $_REQUEST, 'other_email', array('class' => 'long-txt')) ?>
                    <?= html::passwordfield('Password', $_REQUEST, 'other_password', array('class' => 'long-txt')) ?>
                </div>
                <br clear="all" />
            </div>
           <div id="ind-form">
               <label for="bus_type" style="margin-bottom:60px"><span class="imp">*</span> Industry Type</label>
                   <input type="radio" name="bus_type" value="hotel" <?= $_REQUEST['bus_type'] == 'hotel' ? 'checked="checked"' : ''?>>&nbsp;Hotel <br />
                   <input type="radio" name="bus_type" value="resort" <?= $_REQUEST['bus_type'] == 'resort' ? 'checked="checked"' : ''?> >&nbsp;Resort <br />
                   <input type="radio" name="bus_type" value="spa" <?= $_REQUEST['bus_type'] == 'spa' ? 'checked="checked"' : ''?> >&nbsp;Spa <br />
                   <input type="radio" name="bus_type" value="other" <?= $_REQUEST['bus_type'] == 'other' ? 'checked="checked"' : ''?> >&nbsp;Other <br />
                   <span class="error"><?= $errors['bus_type']; ?></span>
               <br clear="all" />
               
               <div style="vertical-align:middle;margin-top:-7px">
                <label for="franchise">Franchise?</label>
                   <input type="radio" name="franchise" value="yes" <?= $_REQUEST['franchise'] == 'yes' ? 'checked="checked"' : ''?> >&nbsp;Yes <br />
                   <input type="radio" name="franchise" value="no" <?= $_REQUEST['franchise'] == 'no' ? 'checked="checked"' : ''?> >&nbsp;No      
               </div>
               
               <br clear="all" />
               
               <?= html::textfield('<span class="imp">*</span> Location Name', $_REQUEST, 'hotels_name', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield('<span class="imp">*</span> Number of Rooms', $_REQUEST, 'hotels_num_rooms', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield('<span class="imp">*</span> Address', $_REQUEST, 'hotels_address', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield('<span class="imp">*</span> City', $_REQUEST, 'hotels_city', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield('<span class="imp">*</span> State', $_REQUEST, 'hotels_state', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield(' <span class="imp">*</span> Zip', $_REQUEST, 'hotels_zip', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield(' <span class="imp">*</span> Location Administrator', $_REQUEST, 'hotels_admin', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield(' <span class="imp">*</span> Phone', $_REQUEST, 'hotels_phone', array('class' => 'long-txt'), $errors) ?>
               <?= html::textfield('<span class="imp">*</span> E-Mail', $_REQUEST, 'hotels_email', array('class' => 'long-txt'), $errors) ?>
               <?= html::passwordfield('<span class="imp">*</span> Password', $_REQUEST, 'hotels_pass', array('class' => 'long-txt'), $errors) ?>
               <em class="right" style="margin:-5px 30px 5px 0;font-size:11px">(Password must have a minimum of 6 characters)</em>
                <?= html::passwordfield('<span class="imp">*</span> Re-enter Password', $_REQUEST, 'hotels_pass2', array('class' => 'long-txt'), $errors) ?>
            
               <br class="clear" />
           </div>
           
           <input type="submit"  value="Register" class="btn center" id="reg-btn" style="display: block" />
        </form>
    </div>
<script type="text/javascript">
document.getElementById('music').value = 314;

function hideCorpBase() {
    $('corporate_address').set('value','');
    $('corporate_city').set('value','');
    $('corporate_state').set('value','');
    $('corporate_zip').set('value','');
    $('corporate_base').setStyle('display','none');
}

function showCorpBase() {
    $('corporate_base').setStyle('display','inline');
}

function showOtherLoc() {
    $('other_locations').setStyle('display','inline');
}

function hideOtherLoc() {
    $('other_email').set('value','');
    $('other_password').set('value','');
    $('other_locations').setStyle('display','none');
}

function showCorpForm() {
    $('btype').set('value','corp');
    $('corp-form').setStyle('display','inline');
    $('reg-btn').setStyle('display','block');
    $('type-corporation').fireEvent('click');
}
function hideCorpForm() {
    var len = document.form.base.length;
    for(i=0;i<len;i++) {
        document.form.base[i].checked = false;
    }
    var len = document.form.other_loc.length;
    for(i=0;i<len;i++) {
        document.form.other_loc[i].checked = false;
    }
    $$('#corp-form input[type=text]').each(function(e) {
        e.set('value','');
    });
    $('corp-form').setStyle('display','none');
}
function showIndForm() {
    $('btype').set('value','ind');
    $('ind-form').setStyle('display','inline');
    $('reg-btn').setStyle('display','block');
}
function hideIndForm() {
    var len = document.form.bus_type.length;
    for(i=0;i<len;i++) {
        document.form.bus_type[i].checked = false;
    }
    $$('#ind-form input[type=text]').each(function(e) {
        e.set('value','');
    });
    $('ind-form').setStyle('display','none');
}

window.addEvent('load', function(evt) {
    $$('input[name=type]').each(function(e) {
        if(e.get('value') == 'corporation') {
            if(e.checked == true) {
                showCorpForm();
            }
        }
        else {
            if(e.get('value') == 'hotel') {
                if(e.checked == true) {
                    showIndForm();
                }
            }
        }
    });
});
</script>

