<?
$page_title = 'Sign up for WAKEUP CALL Risk Management Tools';
$page_description = 'Sign up for our online risk management tools - includes: hospitality and insurance resources and training, HR services, claim management, employment law hotline, SDS library, hospitality industry news, special offers and more....';

include VIEWS . '/header.php';
?>
<style type="text/css">
.heading{ 
	background:#999999;
	color:#fff;
	font-size:17px;
	padding: 5px 15px;
	margin:0;
	}
.tab{
	width:600px;
	position: absolute;
	top:240px;
	background:#fff;
    display:none;
}

.tabContent{
	padding:15px;
	border: 1px solid #999999;
    height:330px;
    overflow-y:auto;
}
#tab-links li{
    border:1px solid #fff;
	border-left: none;
}
#tab-links li.tab-active{
	border:1px solid #999999;
	border-left: none;
}

.tab .close {
	background: url(/images/dialog-close.png) no-repeat;
	width: 24px;
	height: 24px;
	display: block;
	cursor: pointer;
	top: -5px;
	right: -4px;
	position: absolute;
}

.green{color:#4e8e4d}
.checkmark{ padding:0; margin:0 0 10px 0;}
.checkmark li{
	list-style: none;
	background: url(/images/checkmark-little.jpg) no-repeat 0 3px; 
	padding:3px 18px;
}
.big-btn{
	width:80%;
	margin:0 auto;
}

	.big{
		font-size:25px;
		color: #D31145;
		display:block;
		margin-top:10px;
	}

</style>

<div id="content">

    <div id="register-form" style="display:block; position: static;width:auto;">
        <? include VIEWS . '/register-form.php'; ?>
    </div>
    <div id="trial-form">
        <a class="close"></a>
        <p id="trial-form-msg" style="padding-top: 10px;padding-left: 5px;"><strong style="font-size:15px;">Please complete the form to register for a free trial code.</strong><br />
            (<span class="imp">*</span> Required Information)</p>

        <form id="trial-form-form" style="padding-bottom: 20px;"> <!-- I hate myself -->
            <?= html::textfield('<span class="imp">*</span> First Name', $_REQUEST, 'trial_code_firstname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Last Name', $_REQUEST, 'trial_code_lastname', array('class' => 'long-txt', 'maxlength' => 100), $errors) ?>

            <?= html::textfield('<span class="imp">*</span> Email', $_REQUEST, 'trial_code_email', array('class' => 'long-txt', 'maxlength' => 255), $errors) ?>

            <?= html::textfield(' <span class="imp">*</span> Phone', $_REQUEST, 'trial_code_phone', array('class' => 'long-txt', 'maxlength' => 20), $errors) ?>
            <div id="trial-code-error"></div>
            <div style="padding-top: 5px;">
                <a href="#" id="type-trial-code" class="blue-reg"><span class="bigTitle">Request Trial Code</a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
window.addEvent('domready', function(){
    $$('#tab-links a').addEvent('click', function(e){
        e.stop();
        
        hide_popups();
        
        $$(this).getParent().addClass('tab-active');
        $$('#'+$(this).get('data-popid')).setStyle('display', 'block');
    });
    
    document.addEvent('click', hide_popups);
    $$('.tab .close').addEvent('click', hide_popups);
    $$('.tab').addEvent('click', function(e){
        e.stop();
    });
});

function hide_popups(){
    $$('.tab').setStyle('display', 'none');
    $$('#tab-links li').removeClass('tab-active');
}
</script>
<? include VIEWS . '/footer.php'; ?>