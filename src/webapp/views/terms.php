<? if (!http::is_ajax()):
	$page_title          = 'WAKEUP CALL Risk Management Tools - Terms of Use';
	$page_description = 'WAKEUP CALL Risk Management Tools - Terms of Use, Privacy Policy, Account Password and Security, Permitted Use, Acceptance Policy, Proprietary Rights, Disclaimer,...';
	include VIEWS. '/header.php';
?>
    <div id="content">
        <div class="w540 left" id="leftcol">
            <img src="/images/img-terms.jpg" class="left"/>
            <div class="w400 left" style="margin-left:20px">
<? else: ?>
<style type="text/css">
	ol {
		margin-left: 25px;
	}
</style>
<? endif; ?>


				<h2 class="headline">TERMS OF USE</h2>
				<p>Effective Date: 1/1/2011</p>
				<p>Welcome to www.WakeUpCall.net (the “Web site”) of WAKEUP CALL RM, LLC and its affiliates (collectively, WAKEUP CALL,
				“WUC”, “we”, and “us”). WUC provides the services on this Web site (collectively, “Services”) to you subject to the
				following Terms of Use (“Terms”).</p>
				<h3 style="color: #005DAA">1) Acceptance of Terms of Use</h3>
				<p>Please carefully read the following Terms before using the Web site. By accessing and using the Web site or Services, or
				the content displayed on, posted to, transmitted, streamed, or distributed or otherwise made available on or through the
				Web site, including without limitation, User Materials and Submissions (collectively, “Content”), you acknowledge that
				you have read, understood and agree to be bound by these Terms which form an agreement that is effective as if you had
				signed it. If at any time you do not agree to these Terms, please do not access or use the Web site or any of its
				Content or Services.</p>
				<p>These Terms may be revised or updated from time to time. Accordingly, you should check the Terms regularly for updates.
				You can determine when the Terms were last revised by referring to the “Last Revised” legend that will be located at the
				top of this page. Any changes in these Terms take effect upon posting and will only apply to use of the Web site,
				Content, or Services after that date. Each time you access, use or browse the Web site, Content, or Services you signify
				your acceptance of the then-current Terms.</p>
				<h3 style="color: #005DAA">2) Permitted Users of Web site</h3>
				<p>The Web site, Content, or Services are solely directed to persons at least 18 years of age (or for jurisdictions in
				which 18 years old is not the age of majority, at least the age of majority for your jurisdiction).
				We are not responsible or liable for any content, communication, or other use or access of the Web site, Content, or
				Services by users of this Web site in violation of these Terms.</p>
				<h3 style="color: #005DAA">3) Permitted Use of Web site</h3>
				<p>The Content (other than Submissions which are governed by Section 8), Web site, and Services are the sole and exclusive
				property of WUC and/or its licensors. You agree not to reproduce, republish, upload, post, duplicate, modify, copy,
				alter, distribute, create derivative works from, sell, resell, transmit, transfer, display, perform, license, assign or
				publish, or exploit for any commercial purpose, any portion of the Web site, Content or Services other than as expressly
				authorized by WUC in writing, including without limitation, posting or transmitting any advertising, sponsorships, or
				promotions on, in or through the Service, Web site, or Content. You agree that each membership, and all services provided
                to that membership, are expressly for the use of the single location associated with that membership. You agree not to
                share membership usage or information with any non-members or non-member locations. Corporate (multiple location) memberships
                agree not to share any membership services with any locations that do not have a membership or have not been added to their
                corporate group within the WUC system,  whether owned, managed or non-owned by the corporate member. No changes in membership
                are permitted other than mentioned in section 6 ‘Transfer Policy’.  Use of the Web site or the Content or Services in any
				way not expressly permitted by these Terms is prohibited, and may be actionable under United States or international
				law. You agree not to access the Web site, Content or Services through any technology or means other than the video
				streaming pages of the Service itself or other explicitly authorized means WUC   may designate. You agree not to use or
				launch any automated system, including without limitation, "robots", "spiders", or "offline readers" that accesses the
				Web site, Content, or Service. WUC reserves the right to remove or suspend access to the Web site, Content, or Services
				without prior notice.</p>
				<p>You may not duplicate, publish, display, modify, alter, distribute, perform, reproduce, copy, sell, resell, exploit, or
				create derivative works from any part of the Web site or the Content or Services unless expressly authorized by WUC in
				writing or as expressly set forth herein. You agree that you will not remove, obscure, or modify any acknowledgements,
				credits or legal, intellectual property or proprietary notices, or marks, or logos contained on the Web site or in the
				Content or Services. You agree not to collect or harvest any personally identifiable information, from the Service, nor
				to use the communication systems provided by the Service (e.g., forums, comments) for any commercial solicitation
				purposes. You agree not to solicit, for commercial purposes, any users of the Service or Web site. In your use of the
				Web site, Content or Service, you will comply with all applicable laws, regulations, rules, decrees, and ordinances.
				You understand that when using the Service, you will be exposed to Content from a variety of sources, and that WUC is
				not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such Content.
				You further understand and acknowledge that you may be exposed to Content that is inaccurate, misleading, or incomplete,
				and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against WUC
				with respect thereto.</p>
				<p>Special terms may apply to some products or Services offered on the Web site, or to any sweepstakes, contests, or
				promotions that may be offered on the Web site. Such special terms (which may include official rules and expiration
				dates) may be posted in connection with the applicable product, service, sweepstakes, contest, promotion, feature or
				activity. By entering such sweepstakes or contests or participating in such promotions you will become subject to those
				terms or rules. We urge you to read the applicable terms or rules, which are linked from the particular activity, and to
				review our Privacy Policy which, in addition to these Terms, governs any information you submit in connection with such
				sweepstakes, contests and promotions. Any such special terms or rules are in addition to these Terms and, in the event
				of a conflict, any such terms shall prevail over these Terms.</p>
				<h3 style="color: #005DAA">4) Privacy Policy</h3>
				<p>Please review the <a href="/privacy.php" target="_blank">Privacy Policy</a> for the Web site. By using or visiting this Web site or the
				Content or Services, you signify your agreement to the Privacy Policy. If you do not agree with the Privacy Policy, you
				are not authorized to use the Web site. The terms of the Privacy Policy are incorporated herein by this reference.</p>
				<h3 style="color: #005DAA">5) Cancellation Policy</h3>
				<ol>
					<li>WAKEUP CALL provides services, not products or “goods.”  The customer may cancel their subscription to these services
						at any time.  WAKEUP CALL offers their services on a free 15 day trial before joining.  After that the customer must pay
						for the full yearly service, which will be automatically renewed annually.  The customer may cancel their subscription
						to WAKEUP CALL’s services at any time, but the customer will not receive any refund for the period of their subscription
						that they do not use.  All sales are final whether the free 15 day trial is utilized or not.</li>

					<li>What is the difference between a refund and a cancellation?<br />
						In a refund, the money for the requested transaction is refunded back to the customer.  In a cancellation, only future
						payment obligations are affected.  If a customer requests a cancellation, no future billings will be charged to their
						account. Keep in mind, a cancellation will not generate a refund - it will only stop any future billings.</li>
				</ol>
				<h3 style="color: #005DAA">6) Transfer Policy</h3>
				<p>WAKEUP CALL does not allow the transfer of memberships from one business (location) to another for Individual (single
				location) memberships. A Corporate (multiple locations) membership can transfer a current membership from a sold
				location to a newly purchased/managed location for the balance to the membership. The transfer must be within 30 days of
				the sale of the location wanting to transfer from.  Memberships cannot be transferred between currently owned locations.
                A Corporate (multiple locations) membership cannot transfer it’s corporate location membership to any other location,
                whether owned, managed or non-owned by the corporate member.  </p>
				<h3 style="color: #005DAA">7) Account Password and Security</h3>
				<p>The Web site contains features that require registration. You agree to provide accurate, current and complete
				information about yourself as prompted. If you provide any information that is inaccurate, not current or incomplete, or
				WUC has reasonable grounds to suspect that such information is inaccurate, not current or incomplete, WUC may remove or
				de-register you from this Web site or contest, at its sole discretion. WUC reserves the right to take appropriate steps
				against any person or entity that intentionally provides false or misleading information to gain access to portions of
				the Web site that would otherwise be denied.  You agree that you will not have any claim against WUC as a result of any
				action it takes based on its reasonable suspicions.</p>
				<p>At the time you register for online account access, you may be required to select a username and password to be used in
				conjunction with your account. You are responsible for maintaining the confidentiality of your password and are fully
				responsible for all uses of your password and transactions conducted in your account, whether by you or others. You
				agree to (a) log out of your account at the end of each session; (b) keep your password confidential and not share it
				with anyone else; and (c) immediately notify WUC of any unauthorized use of your password or account or any other breach
				of security. WUC is authorized to act on instructions received through use of your password, and is not liable for any
				loss or damage arising from your failure to comply with this Section. You agree not to circumvent, disable or otherwise
				interfere with security-related features of the Service or features that prevent or restrict use or copying of any
				Content or enforce limitations on use of the Service or the Content therein.</p>
				<h3 style="color: #005DAA">8) Proprietary Rights</h3>
				<p>You acknowledge and agree that, as between WUC and you, all right, title, and interest in and to the Web site, Content,
				and Services including without limitation any patents, copyrights, trademarks, trade secrets, inventions, know-how, and
				all other intellectual property rights are owned exclusively by WUC or its licensors, are valid and enforceable, and are
				protected by United States intellectual property laws and other applicable laws. Any attempt to use, redistribute,
				reverse engineer, or redesign the information, code, videos, textual or visual materials, graphics, or modules contained
				on the Web site for any other purpose is prohibited. WUC and its licensors reserve all rights not expressly granted in
				and to the Web site, Service and the Content.</p>
				<p></p>Copyright: As between you and WUC, you acknowledge and agree that all Content and Services included in the Web site,
				such as text, graphics, logos, icons, videos, images, media, data, audio, visual, animation, software and other
				information and materials, is the copyright and property of WUC or its content suppliers and protected by U.S. and
				international copyright laws. Permission is granted to electronically copy and print hard copy portions of the Web site
				or Content for the sole purpose of using the Web site as a personal internal resource or otherwise for its intended
				purposes, provided that all hard copies contain all copyrights and trademarks, and other applicable intellectual
				property and proprietary marks and notices. Any other use, including the reproduction, modification, distribution,
				transmission, republication, display or performance, of the Web site, Content, or Services is strictly prohibited.
				Trademarks: The trademarks, service marks, logos, slogans, trade names and trade dress used on the Web site are
				proprietary to WUC or its licensors. Without limiting the foregoing, “WUC” is the trademark of WUC. Unauthorized use of
				any trademark of WUC may be a violation of federal or state trademark laws. Any third party names or trademarks
				referenced in the Web site do not constitute or imply affiliation, endorsement or recommendation by WUC, or of WUC by
				the third parties.</p>
				<h3 style="color: #005DAA">9) Your Indemnity of WUC</h3>
				<p>YOU AGREE TO INDEMNIFY, DEFEND AND HOLD WUC, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, REPRESENTATIVES, SUBSIDIARIES,
				AFFILIATES, LICENSORS, PARTNERS, SERVICE PROVIDERS AND OTHERS ACTING IN CONCERT WITH IT, HARMLESS FROM ANY LOSSES,
				LIABILITIES, CLAIMS, DAMAGES, OBLIGATIONS, DEMANDS, COSTS OR DEBTS, AND EXPENSES INCLUDING WITHOUT LIMITATION REASONABLE
				ATTORNEYS' FEES, MADE BY YOU OR ON YOUR BEHALF OR BY ANY THIRD PARTY DUE TO OR ARISING OUT OF (A) YOUR CONNECTION,
				POSTING, OR SUBMISSION TO OR USE OF THE WEB SITE OR THE CONTENT OR SERVICES OR SUBMISSION; (B) YOUR VIOLATION OF THESE
				TERMS, ANY APPLICABLE LAWS, OR THE RIGHTS OF WUC OR ANY THIRD PARTY, INCLUDING WITHOUT LIMITATION ANY COPYRIGHT,
				PROPERTY, OR PRIVACY RIGHT; OR (C) ANY CLAIM THAT YOUR SUBMISSION CAUSED DAMAGE TO A THIRD PARTY.</p>
				<h3 style="color: #005DAA">10) User Generated Content</h3>
				<p>a) Communications Services: The Web site may contain areas for you to leave comments or feedback, chat areas, blogs,
				bulletin board services, focus groups, forums, contests, games, communities, calendars, and/or other message or
				communication facilities designed to enable you and others to communicate with WUC and other users of the Web site
				(collectively, "Communication Services"). The opinions expressed in the Communication Services reflect solely the
				opinion(s) of the participants and may not reflect the opinion(s) of WUC. You acknowledge that your submissions to the
				Web site may be or become available to others. You agree only to post, send and receive messages and materials that are
				in accordance with these Terms and related to the particular Communication Service.</p>
				<p>b) Prohibited Actions: You agree that the following actions are prohibited and constitute a material breach of these
				Terms. This list is not meant to be exhaustive, and WUC reserves the right to determine what types of conduct it
				considers to be inappropriate use of the Web site. In the case of inappropriate use, WUC or its designee may take such
				measures as it determines in its sole discretion.</p>
				<p>By way of example, and not as a limitation, you agree that when using the Web site, Content, Services or a Communication
				Service, you will not:
				<ol>
					<li>Use the Web site, Content or Services for any purpose or to take any actions in violation of local, state, national,
					or international laws, regulations, codes, or rules.</li>
					<li>Violate any code of conduct or other guidelines that may be applicable for any particular Communication Service.</li>
					<li>Take any action that imposes an unreasonable or disproportionately large load on the Web site's infrastructure or
					otherwise in a manner that may adversely affect performance of the Web site or restrict or inhibit any other user from
					using and enjoying the Communication Services or the Web site.</li>
					<li>Use the Web site for unauthorized framing of or linking to, or access via automated devices, bots, agents, scraping,
					scripts, intelligent search or any similar means of access to the Content or Services.</li>
					<li>Aggregate, copy, duplicate, publish, or make available any of the Content or Services or any other materials or
					information available from the Web site to third parties outside the Web site in any manner or any other materials or
					information available from the Web site.</li>
					<li>Defame, bully, abuse, harass, stalk, demean, threaten or discriminate against others or otherwise violate the rights
					(such as rights of privacy and publicity) of others or reveal another users' personal information, e.g. hate speech.</li>
					<li>Publish, post, upload, distribute or disseminate any inappropriate, disparaging, profane, defamatory, infringing,
					obscene, indecent, sexually explicit, or unlawful topic, name, material, content, video, image, audio, caption, or
					information. Be advised that we work closely with law enforcement and we report child exploitation.</li>
					<li>Upload or download files that contain software or other material protected by intellectual property laws or other
					laws, unless you own or control the rights, titles, or interests thereto or have received all necessary consents or
					rights.</li>
					<li>Upload or transmit files that contain viruses, corrupted files, or any other similar software or programs that may
					damage the operation of another's computer.</li>
					<li>Use the Web site to make available unsolicited advertising or promotional materials, spam, pyramid schemes, chain
					letters, or similar forms of unauthorized advertising or solicitation or conduct or forward surveys.</li>
					<li>Harvest or otherwise collect information about others, including email addresses, without their consent.</li>
					<li>Download any file or material posted by another user of a Communication Service that you know, or reasonably should
					know, cannot be legally distributed in such manner.</li>
					<li>Falsify or delete any author attributions, legal or other notices, or proprietary designations or labels of origin
					or source.</li>
					<li>Restrict or inhibit any other user from using and enjoying the Communication Services.</li>
					<li>Engage in any other action that, in the judgment of WUC, exposes it or any third party to potential liability or
					detriment of any type.</li>
				</ol>
				</p>
				<p>If you use, view, or access this Web site in contravention of these Terms, or if you have repeatedly violated these
				Terms or a third party's copyright, we reserve the right to terminate the permissions or rights granted to you by WUC
				and we reserve all of our rights under this Agreement, at law and in equity.</p>
				<h3 style="color: #005DAA">11) Notice and Procedures for Making Claims of Copyright or Intellectual Property Infringement</h3>
				<p>WUC may, in appropriate circumstances and at its sole discretion, disable and/or terminate use of the Web site, Content,
				or Services by users who infringe on the intellectual property of others. If you believe that your work has been copied
				in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated,
				please provide WUC’s Copyright Agent a Notice containing the following information:
				<ol>
					<li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other
					intellectual property interest;</li>
					<li>a description of the copyrighted work or other intellectual property that you claim has been infringed;</li>
					<li>a description of where the material that you claim is infringing is located on the Web site (providing URL(s) in the
					body of an email is the best way to help WUC locate content quickly);</li>
					<li>your name, address, telephone number, and email address;</li>
					<li>a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner,
					its agent, or the law;</li>
					<li>a statement by you, made under penalty of perjury, that the above information in your Notice is accurate and that you
					are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's
					behalf.</li>
				</ol>
				</p>

				<p>If you believe in good faith that a notice of copyright infringement has been wrongly filed by WUC against you, the
				Digital Millennium Copyright Act (“DMCA”) permits you to send WUC a counter-notice.</p>
				<p>Notices and counter-notices must meet the then-current statutory requirements imposed by the DMCA;
				see <a href="http://www.loc.gov/copyright/" target="_blank">http://www.loc.gov/copyright/</a> for details. Notices of claims of copyright or other intellectual property
				infringement and counter-notices should be sent to WUC’s Copyright Agent who can be reached in the following ways:<br /><br />
				Mailing Address:<br />6965 El Camino Real Ste. 105-155, Carlsbad, CA 92009</p>
				<p>Make sure you know whether the Content that you have seen on WUC infringes your copyright. If you are not certain what
				your rights are, or whether your copyright has been infringed, you should check with a legal adviser first. Be aware
				that there may be adverse legal consequences in your country if you make a false or bad faith allegation of copyright
				infringement by using this process.</p>
				<p>Please also note that the information provided in this legal notice may be forwarded to the person who provided the
				allegedly infringing Content.</p>
				<h3 style="color: #005DAA">12) Links</h3>
				<p>Links to Other Web sites and Search Results: The Web site may contain links to web sites operated by other parties. The
				Web site provides these links to other web sites as a convenience, and your use of these sites is at your own risk. The
				linked sites are not under the control of WUC   which is not responsible for the content available on third party sites.
				Such links do not imply endorsement of information or material on any other site and WUC disclaims all liability with
				regard to your access to, use of or transactions with such linked web sites. You acknowledge and agree that WUC shall
				not be responsible or liable, directly or indirectly, for any damage, loss or other claim caused or alleged to be caused
				by or in connection with, access to, use of or reliance on the Web site or any Content or Services available on or
				through any other site or resource.</p>
				<p>Links to the Web site: You may link another web site to the WUC Web site subject to the following linking policy: (i)
				the appearance, position and other aspects of any link may not be such as to damage or dilute the reputation of WUC or
				the Web site; (ii) the appearance, position and other attributes of the link may not create the false appearance that
				your site, business, organization or entity is sponsored by, endorsed by, affiliated with, or associated with WUC or the
				Web site; (iii) when selected by a user, the link must display the Web site on full-screen and not within a "frame" on
				the linking web site; (iv) the link may not be part of any scheme to avoid registration; and (iv) WUC reserves the right
				to revoke its consent to the link at any time and in its sole discretion.</p>
				<h3 style="color: #005DAA">13) Modifications to Web site</h3>
				<p>WUC reserves the right at any time and from time to time to modify, suspend, block, terminate, or discontinue,
				temporarily or permanently, the Web site, Content, or Services, or any portion thereof, with or without notice. You
				agree that WUC will not be liable to you or to any third party for any modification, suspension, blocking, termination,
				or discontinuance of the Web site, Content, or Services.</p>
				<h3 style="color: #005DAA">14) Suspension and Termination Rights</h3>
				<p>WUC  reserves the right, at its sole discretion, immediately and without notice, to suspend or terminate your access to
				the Web site, Content, or Services or any part thereof for any reason, including without limitation any breach by you of
				these Terms. You agree that WUC shall not be liable to you or any third party for any such suspension or termination.</p>
				<h3 style="color: #005DAA">15) Disclaimer</h3>
				<p>THE WEB SITE AND CONTENT AND THE INFORMATION, SERVICES, PRODUCTS, SWEEPSTAKES, CONTESTS, DRAWINGS, OR OTHER ACTIVITIES
				OFFERED, CONTAINED IN OR ADVERTISED ON THE WEB SITE, INCLUDING WITHOUT LIMITATION TEXT, VIDEO, GRAPHICS AND LINKS, ARE
				PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED. TO THE
				MAXIMUM EXTENT PERMITTED BY LAW, WUC  AND ITS LICENSORS, SUPPLIERS AND RELATED PARTIES DISCLAIM ALL REPRESENTATIONS AND
				WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO THE WEB SITE AND CONTENT, INFORMATION, SERVICES, PRODUCTS AND MATERIALS
				AVAILABLE ON OR THROUGH THE WEB SITE, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A
				PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, FREEDOM FROM COMPUTER VIRUS AND IMPLIED WARRANTIES ARISING FROM COURSE OF
				DEALING OR COURSE OF PERFORMANCE. YOUR USE OF THE WEB SITE OR ANY CONTENT OR SERVICES ARE ENTIRELY AT YOUR OWN RISK.
				TO THE FULLEST EXTENT PERMITTED BY LAW, WUC EXCLUDES ALL WARRANTIES, CONDITIONS, TERMS OR REPRESENTATIONS ABOUT THE
				ACCURACY OR COMPLETENESS OF THIS WEB SITE'S CONTENT OR THE CONTENT OF ANY WEB SITES LINKED TO THIS WEB SITE (INCLUDING,
				BUT NOT LIMITED TO, THINKHR, ACTIO, BLR, ANYTHING IN ADDITIONAL SERVICES) AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR
				ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER,
				RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS
				AND/OR ANY AND ALL PERSONAL, CONFIDENTIAL AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OR
				CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE
				TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY
				LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR SUBMISSION OR THE USE OF ANY CONTENT POSTED, EMAILED,
				TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES OR WEB SITE. WUC DOES NOT WARRANT, ENDORSE, GUARANTEE, OR
				ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICES OR ANY
				HYPERLINKED SERVICES OR FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND WUC WILL NOT BE A PARTY TO OR IN ANY WAY BE
				RESPONSIBLE FOR MONITORING ANY TRANSACTION OR COMMUNICATION BETWEEN YOU AND ANY OTHER USER OR THIRD-PARTY PROVIDERS OF
				PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD
				USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</p>
				<p>Without limiting the foregoing, you are responsible for taking all necessary precautions to insure that any Content,
				Services, or access to the Web site is free of viruses or other harmful code.</p>
				<h3 style="color: #005DAA">16) Limitation on Liability</h3>
				<p>TO THE MAXIMUM EXTENT PERMITTED BY LAW, WUC AND ITS RELATED PARTIES DISCLAIM ALL LIABILITY, WHETHER BASED IN CONTRACT,
				TORT (INCLUDING WITHOUT LIMITATION NEGLIGENCE), STRICT LIABILITY OR ANY OTHER THEORY ARISING OUT OF OR IN CONNECTION
				WITH THE WEB SITE, USE, INABILITY TO USE OR PERFORMANCE OF THE INFORMATION, CONTENT, SERVICES, PRODUCTS AND MATERIALS
				AVAILABLE FROM OR THROUGH THE WEB SITE. IN NO EVENT SHALL WUC, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS OR ANY OF ITS
				AFFILIATED ENTITIES OR SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL, EXEMPLARY OR
				CONSEQUENTIAL LOSSES, EXPENSES, OR DAMAGES, EVEN IF THESE PERSONS AND ENTITIES HAVE BEEN PREVIOUSLY ADVISED OF THE
				POSSIBILITY OF SUCH DAMAGES. THESE LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OR THE
				EXISTENCE OF ANY LIMITED REMEDY. WITHOUT LIMITING THE FOREGOING, THE MAXIMUM AGGREGATE LIABILITY OF WUC ARISING OUT OF
				OR IN CONNECTION WITH THESE TERMS OR THE WEB SITE OR THE CONTENT, INFORMATION, MATERIALS, PRODUCTS OR SERVICES ON OR
				THROUGH THE WEB SITES SHALL NOT EXCEED FIFTY DOLLARS (U.S.).</p>
				<p>YOU SPECIFICALLY ACKNOWLEDGE THAT WUC SHALL NOT BE LIABLE FOR CONTENT OR THE DISPARAGING, DEFAMATORY, INDECENT,
				OFFENSIVE, SEXUALLY EXPLICIT OR ILLEGAL CONDUCT OR CONTENT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM
				THE FOREGOING RESTS ENTIRELY WITH YOU.</p>
				<p>Exclusions and Limitations: Because some jurisdictions do not allow limitations on how long an implied warranty lasts,
				or the exclusion or limitation of liability for consequential or incidental damages, the above limitations may not apply
				to you. This Limitation of Liability shall be to the maximum extent permitted by applicable law.</p>
				<h3 style="color: #005DAA">17) Notice Required by California Law</h3>
				<p>Pursuant to California Civil Code Section 1789.3, users are entitled to the following specific consumer rights notice:
				The name, address and telephone number of the provider of this Web site is WAKEUP CALL RM, LLC, 6965 El Camino Real Ste. 105-155,
				Carlsbad, CA 92009, 866-675-3909. Complaints regarding this Web site, or the Content or Services or requests to
				receive further information regarding use of this Web site or the Content or Services may be sent to the above address
				or to <a href="/contact.php" target="_blank">our contact form</a>.</p>
				<p>The Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs may
				be contacted in writing at 1625 North Market Boulevard, Suite S202, Sacramento, CA 95834 or by telephone at (916)
				574-7950 or (800) 952-5210.</p>
				<h3 style="color: #005DAA">18) Governing Law and Disputes</h3>
				<p>These Terms shall be governed by, and will be construed under, the laws of the State of California, U.S.A., without
				regard to choice of law principles. You irrevocably agree to the exclusive jurisdiction by the federal and state courts
				located in San Diego County, California, U.S.A., to settle any dispute which may arise out of, under, or in connection
				with these Terms, as the most convenient and appropriate for the resolution of disputes concerning these Terms. ANY
				CAUSE OF ACTION OR CLAIM YOU MAY HAVE WITH RESPECT TO THESE TERMS, THE WEB SITE OR THE CONTENT OR SERVICES MUST BE
				COMMENCED WITHIN SIX (6) MONTHS AFTER THE CLAIM OR CAUSE OF ACTION ARISES OR SUCH CLAIM OR CAUSE OF ACTION SHALL BE
				BARRED.</p>
				<p>The Web site is controlled within the United States of America and directed to individuals residing in the United
				States. Those who choose to access the Web site from locations outside of the United States do so on their own
				initiative, and are responsible for compliance with local laws if and to the extent local laws are applicable. WUC does
				not represent that the Web site, Content, or Services are appropriate outside the United States of America. WUC reserves
				the right to limit the availability of the Web site to any person, geographic area or jurisdiction at any time in its
				sole discretion.</p>
				<h3 style="color: #005DAA">19) Force Majeure</h3>
				<p>WUC shall not be liable for any delay or failure to perform resulting from causes outside its reasonable control or
				unforeseen circumstances such as acts of nature or God, fire, flood, earthquake, accidents, strikes, war, terrorism,
				governmental act, failure of or interruption in common carriers (including without limitation Internet service providers
				and web hosting providers) or utilities, or shortages of transportation facilities, fuel, energy, labor or materials.</p>
				<h3 style="color: #005DAA">20) Ability To Accept Terms of Service</h3>
				<p>You represent and warrant that you are either at least 18 years of age (or for jurisdictions in which 18 years old is
				not the age of majority to legally enter into binding contracts, at least such age of majority for your jurisdiction),
				or an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into
				the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms, and to abide
				by and comply with these Terms. You acknowledge that we have given you a reasonable opportunity to review these Terms
				and that you have agreed to them.</p>
				<h3 style="color: #005DAA">21) Miscellaneous</h3>
				<p>These Terms and the <a href="/privacy.php" target="_blank">Privacy Policy</a> and any other legal notices published by WUC set forth the entire understanding and
				agreement between you and WUC with respect to the subject matter hereof. If any provision of the Terms or the Privacy
				Policy is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should
				endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the Terms
				or the Privacy Policy shall remain in full force and effect. Headings are for reference only and in no way define,
				limit, construe or describe the scope or extent of such section. WUC’s failure to act with respect to any failure by you
				or others to comply with these Terms or the Privacy Policy does not waive its right to act with respect to subsequent or
				similar failures. You may not assign, transfer, sublicense or delegate these Terms or the Privacy Policy or your rights
				or obligations under these Terms or the Privacy Policy without the prior written consent of WUC, but same may be
				assigned by WUC without restriction. Any assignment, transfer, sublicense, or delegation in violation of this provision
				shall be null and void. There are no third party beneficiaries to these Terms or the Privacy Policy.</p>
				<h3 style="color: #005DAA">22) Questions</h3>
				<p>Please direct any questions you may have about these Terms, technical questions or problems with the Web site, or report
				a violation of these Terms, or comments or suggestions to WUC at <a href="mailto:info@wakeupcall.net">info@wakeupcall.net</a>.
<? if (!http::is_ajax()): ?>
			</div>
		</div>
         <? include  VIEWS. '/register.php'; ?>
        <br class="clear" />
 		</div>

<? include  VIEWS. '/footer.php'; ?>
<? endif; ?>