<?php
if (DEPLOYMENT != 'production') {
    ?>
    <h1>Test Scheduled Jobs</h1>
    <?= $notice ? ('<p>' . implode($notice, '<p>')) : '' ?>
    <form method="post" action="test-crons.php">
        <label>Only send email to: </label>
        <input name="testing_email_address_filter" value="<?= $_SESSION['testing_email_address_filter'] ?>">
        <br>
        <input type="submit" value="Set Email Filter" name="set_testing_email_address_filter">
        <input type="submit" value="Clear Email Filter" name="clear_testing_email_address_filter">
    </form>

    <p>Email is currently
        <?= DISABLE_EMAIL ? 'disabled.' : 'enabled.' ?>
    </p>

    <h2>Often</h2>
    <ul>
        <li><a href="test-crons.php?script=check-faxes">check-faxes</a> Checks the status of fax submissions</li>
    </ul>

    <h2>Nightly</h2>
    <ul>
        <li><a href="test-crons.php?script=vendor-certificate-request">vendor-certificate-request</a> Sends
            certificate requests to vendors for expired or expiring certificates.
        </li>

        <li><a href="test-crons.php?script=contract-expiration-reminder">contract-expiration-reminder</a> Sends reminder
            emails for contract expiration
        </li>

        <li><a href="test-crons.php?script=free-membership-expired">free-membership-expired</a> After a member has been
            a 'free' member for 15 days, they get sent an email with a "benefits of full membership" ALSO: sets status
            from 'free' to 'locked'
        </li>

        <li><a href="test-crons.php?script=create-bills">create-bills</a> Creates bills - set up an account that hasn't
            paid in a year, run this, then view its bills
        </li>
        <li><a href="test-crons.php?script=auto-billing">auto-billing</a> Processes bills - get a bill created, then run
            this, and verify that it paid/failed and locked
        </li>

        <li><a href="test-crons.php?script=full-membership-expired-reminder">full-membership-expired-reminder</a> 1
            month and 3 months after full membership expired (auto bill didn't work?)
        </li>

        <li><a href="test-crons.php?script=free-membership-expired-reminder">free-membership-expired-reminder</a> 2
            weeks, 2 months, 4 months after free trial expired
        </li>

        <li><a href="test-crons.php?script=delete-members">delete-members</a> Delete members that have been 'deleted'
            for 60 days
        </li>

        <li><a href="test-crons.php?script=webinar-reminder-registered">webinar-reminder-registered</a> remind about
            registered webinar
        </li>

        <li><a href="test-crons.php?script=webinar-reminder-unregistered">webinar-reminder-unregistered</a> remind about
            unregistered webinar
        </li>

        <li><a href="test-crons.php?script=update-rss">update-rss</a> Updates RSS feeds
        </li>

        <li><a href="test-crons.php?script=membership-renewal-reminder">membership-renewal-reminder</a> Reminder of
            auto-renewal
        </li>
        <li><a href="test-crons.php?script=entities_items_milestone_reminder">entities_items_milestone_reminder</a>
            Reminder of
            entities items milestone
        </li>
        <li><a href="test-crons.php?script=contracts_milestone_reminder">contracts_milestone_reminder</a> Reminder of
            contract milestone
        </li>
        <li><a href="test-crons.php?script=process-access-logs">process-access-logs</a> Processes raw access
            logs for use in usage reporting
        </li>
    </ul>
    <h2>Weekly</h2>
    <ul>
        <li><a href="test-crons.php?script=certificate-expiration-reminder">certificate-expiration-reminder</a></li>

        <li><a href="test-crons.php?script=weekly-newsletter">weekly-newsletter</a> Sends the weekly newsletter</li>
    </ul>
    <?php
}
?>