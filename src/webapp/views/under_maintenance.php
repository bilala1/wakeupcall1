<?
$page_title = 'Login to WAKEUP CALL';
$page_description = 'Login to WAKEUP CALL';

include VIEWS . '/header.php';
?>
    <style type="text/css">
        .heading{
            background:#999999;
            color:#fff;
            font-size:17px;
            padding: 5px 15px;
            margin:0;
        }
        .tab{
            width:600px;
            position: absolute;
            top:240px;
            background:#fff;
            display:none;
        }

        .tabContent{
            padding:15px;
            border: 1px solid #999999;
            height:330px;
            overflow-y:auto;
        }
        #tab-links li{
            border:1px solid #fff;
            border-left: none;
        }
        #tab-links li.tab-active{
            border:1px solid #999999;
            border-left: none;
        }

        .tab .close {
            background: url(/images/dialog-close.png) no-repeat;
            width: 24px;
            height: 24px;
            display: block;
            cursor: pointer;
            top: -5px;
            right: -4px;
            position: absolute;
        }
    </style>

    <div id="content">
        <h1>Maintenance</h1>
        <p><?= SITE_NAME ?> is currently undergoing maintenance. Please check back shortly.</p>
    </div>
    <script type="text/javascript">
        window.addEvent('domready', function(){
            $$('#tab-links a').addEvent('click', function(e){
                e.stop();

                hide_popups();

                $$(this).getParent().addClass('tab-active');
                $$('#'+$(this).get('data-popid')).setStyle('display', 'block');
            });

            document.addEvent('click', hide_popups);
            $$('.tab .close').addEvent('click', hide_popups);
            $$('.tab').addEvent('click', function(e){
                e.stop();
            });
        });

        function hide_popups(){
            $$('.tab').setStyle('display', 'none');
            $$('#tab-links li').removeClass('tab-active');
        }
    </script>
<? include VIEWS . '/footer.php'; ?>