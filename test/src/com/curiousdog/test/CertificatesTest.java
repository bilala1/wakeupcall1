package com.curiousdog.test;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.curiousdog.test.bean.Certificate;
import com.curiousdog.test.bean.Location;
import com.curiousdog.test.tracking.certificates.Vendor;
import com.curiousdog.test.utils.TestCache;
import com.curiousdog.test.utils.TestUtils;

public class CertificatesTest {

	static Logger log = Logger.getLogger(CertificatesTest.class);
	
	public CertificatesTest() {
		// TODO Auto-generated constructor stub
	}

	public boolean testVendors(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;

		log.info("VENDORS: Check if there are locations in cache for vendors test");
		List<Location>newLocations = TestCache.getInstance().getMyLocations() ;
		if ( newLocations == null || newLocations.size() == 0 )
		{
			log.error("VENDORS: No locations found in cache for test. Need to run locations test first");
			//Run locations test. This should populate locations cache.
			MyAccountTest accountTest = new MyAccountTest() ;
			if ( !accountTest.TestMyAccountAddLocation(drv) ) 
			{	
				log.error("VENDORS: Unable to create new locations for testing vendors. Locations test failed");
				return false ;
			}
		}
		
		//Click on MyWUC to make sure we are at the right place (assuming we're logged in)
		drv.findElement(By.partialLinkText("My WAKEUP")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("VENDORS: Get to Certificates Tracking section of My WUC");
		//Go to Certificates Tracking section
		drv.findElement(By.linkText("Certificate Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("VENDORS: Testing Manage Vendors functionality");
		drv.findElement(By.linkText("Manage Vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		
		log.info("VENDORS: Testing Adding a vendor");
		if ( !addVendor(drv) )
		{
			log.error("Adding vendor test failed");
			return false ;
		}
		log.info("Testing editing vendors");
		if ( !editVendor(drv) )
		{
			log.error("Editing vendors test failed") ;
			return false ;
		}
		if ( !addCertificates(drv) )
		{
			log.error("Adding a new certificate failed");
			return false ;
		}
		else
		{
			log.info("Editing certificate");
			if ( !editCertificate(drv) )
			{
				log.error("Edit certificate test failed");
				return false ;
			}
			log.info("Update certificate") ;
			if ( !updateCertificate(drv) )
			{
				log.error("Update certificate test failed");
				return false ;
			}
			log.info("Delete certificate");
			if ( !deleteCertificate(drv) )
			{
				log.error("Delete certificate test failed");
				return false ;
			}
			log.info("Hide-UnHide certificates");
			if ( !hideUnhide(drv) )
			{
				log.error("Hide-Unhide certificates test failed");
				return false ;
			}
		}
		return true ;
	}
	
	public boolean addVendor(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		
		String currentUrl = drv.getCurrentUrl() ;
		log.info("ADD VENDOR: Starting test to add a new vendor");
		log.info("ADD VENDOR: current page: " + currentUrl);
		
		log.info("ADD VENDOR: Going to Add Vendor form. Click on 'Add Vendor' link");
		drv.findElement(By.linkText("Add Vendor")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Verify we're at the correct page
		currentUrl = drv.getCurrentUrl() ;
		if ( !(homePage + "/members/vendors/edit.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("ADD VENDOR: We got redirected to an unexpected URL: " + currentUrl);
			return false ;
		}

		log.info("ADD VENDOR: Submitting empty form and checking error messages");

		drv.findElement(By.id("licensed_locations_all")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("ADD VENDOR: Making sure all error messages were displayed");
		
		boolean allGood = true ;
		if (!TestUtils.findText(drv, "There are errors below"))     
		{
			log.error("ADD VENDOR: Message 'There are errors below' did not display. Test failed") ;
			allGood = false ;
		}
		/*
		 * for some reason this message disappeared from the errors
		 */
		//if (!TestUtils.findText(drv, "Please select at least one location"))
		//{
		//	log.error("ADD VENDOR: Message 'Please select at least one location' did not display. Test failed") ;
		//	allGood = false ;
		//}
		if (!TestUtils.findText(drv, "Please enter something"))
		{
			log.error("ADD VENDOR: Message 'Please enter something' did not display. Test failed") ;
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("ADD VENDOR: Message 'You must enter a properly formatted email' did not display. Test failed") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//First vendor applies to all locations:
		log.info("ADD VENDOR: Mark vendor for all locations");
		drv.findElement(By.id("licensed_locations_all")).click();
		log.info("ADD VENDOR: Check Locations can view checkbox") ;
		drv.findElement(By.id("vendors_location_can_view")).click();
		log.info("ADD VENDOR: Fill out the only Vendor name field");
		drv.findElement(By.id("vendors_name")).sendKeys("Test Vendor for all locations");
		drv.findElement(By.id("licensed_locations_all")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if (!TestUtils.findText(drv, "Please enter something"))
		{
			log.error("ADD VENDOR: Message 'Please enter something' did not display. Test failed") ;
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("ADD VENDOR: Message 'You must enter a properly formatted email' did not display. Test failed") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("ADD VENDOR: Fill out the rest of the form");
		drv.findElement(By.id("vendors_contact_name")).sendKeys("Jaffar Naggatt");
		drv.findElement(By.id("vendors_email")).sendKeys("dzaytsev@hotmail.com");
		drv.findElement(By.id("vendors_services")).sendKeys("This vendor provides a full range of services including exotic dancing for pool guests");
		drv.findElement(By.id("licensed_locations_all")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("ADD VENDOR: Submitted first vendor, adding a second vendor. Make sure it showed up");
		if (!TestUtils.findText(drv, "Jaffar Naggatt") ||
		    !TestUtils.findText(drv, "Test Vendor for all locations") ||
		    !TestUtils.findText(drv, "All Locations")
		)
		{
			log.error("ADD VENDOR: Names for the added vendor did not display on the screen");
			return false ;
		}
		drv.findElement(By.linkText("Add Vendor")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("ADD VENDOR: Check this vendor for only one location (whichever shows up last in the list");
		//Add one more vendor only for 1 location
		List<WebElement> accessList = drv.findElements(By.xpath("//*[starts-with(@id,'licensed_locations')]"))  ;
		if ( accessList.size() == 0 )
		{
			log.error("ADD VENDOR: No checkboxes for locations found") ;
			return false ;
		}
		WebElement oneLocation = (WebElement)accessList.get(accessList.size()-1) ;
		if ( oneLocation.isEnabled() ) 
		{	
			oneLocation.click();
		}
		log.info("ADD VENDOR: Fill out the rest of the fields");
		drv.findElement(By.id("vendors_location_can_view")).click();
		drv.findElement(By.id("vendors_location_can_edit")).click();
		drv.findElement(By.id("vendors_name")).sendKeys("Test Vendor for only 1 location");
		drv.findElement(By.id("vendors_contact_name")).sendKeys("Bilbo Baggins");
		drv.findElement(By.id("vendors_email")).sendKeys("dzaytsev@hotmail.com");
		drv.findElement(By.id("vendors_services")).sendKeys("This vendor only works on electric, heating and cooling");
		drv.findElement(By.id("vendors_name")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("ADD VENDOR: Submitted the second vendor. Now we should have 2 vendors");
		if (!TestUtils.findText(drv, "Jaffar Naggatt") ||
			!TestUtils.findText(drv, "Test Vendor for all locations") ||
			!TestUtils.findText(drv, "Test Vendor for only 1 location") ||
			!TestUtils.findText(drv, "Bilbo Baggins")
			)
		{
			log.error("ADD VENDOR: Names for the added vendor did not display on the screen");
			return false ;
		}
		//Click on Hidden to make sure there are no hidden vendors
		log.info("ADD VENDOR: Check hidden vendors");
		drv.findElement(By.linkText("View Hidden vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		log.info("ADD VENDOR: We should have no hidden vendors at this time");
		if ( !TestUtils.findText(drv, "You have not entered a vendor yet"))
		{
			log.error("ADD VENDOR: There are hidden vendors that are not supposed to be there, will not stop test yet.");
		}
		log.info("ADD VENDOR: Return to visible vendors and make them hidden");
		drv.findElement(By.linkText("View Visible vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		List<WebElement> list = drv.findElements(By.linkText("Hide")) ;
		for ( int i = 0 ; i < list.size() ; i++ )
		{
			WebElement check = TestUtils.getFirstElement(drv, "Hide") ;
			if ( check.isDisplayed() && check.isEnabled() ) check.click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		}
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		if ( !TestUtils.findText(drv, "You have not entered a vendor yet") ) 
		{
			log.error("ADD VENDOR: Message 'You have not entered a vendor yet' did not appear or not all files were hidden");
			return false ;
		}
		log.info("ADD VENDOR: Go to the Hidden Vendor section to make sure both vendors are now there");
		drv.findElement(By.linkText("View Hidden vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Jaffar Naggatt") ||
			 !TestUtils.findText(drv, "Bilbo Baggins")	)
		{
			log.error("ADD VENDOR: One or both vendors did not appear on the hidden files list" );
			return false ;
		}
		log.info("ADD VENDOR: Un-Hide all vendors");
		list = drv.findElements(By.linkText("Un-Hide")) ;
		for ( int i = 0 ; i < list.size() ; i++ )
		{
			WebElement check = TestUtils.getFirstElement(drv, "Un-Hide") ;
			if ( check.isDisplayed() && check.isEnabled() ) check.click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		}
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Go back to visible vendors
		drv.findElement(By.linkText("View Visible vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Now delete all vendors for test purposes
		log.info("ADD VENDOR: Delete all vendors now") ;
		list = drv.findElements(By.linkText("Delete")) ;
		for ( int i = 0 ; i < list.size() ; i++ )
		{
			WebElement check = TestUtils.getFirstElement(drv, "Delete") ;
			if ( check.isDisplayed() && check.isEnabled() ) check.click();
			drv.switchTo().activeElement() ;
			WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
			buttonOK.click();			
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.switchTo().parentFrame() ;
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		}
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Add 1 vendor for certificates testing
		log.info("ADD VENDOR: Add one vendor for all locations to continue testing");
		drv.findElement(By.linkText("Add Vendor")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		Vendor vendor1 = new Vendor() ;
		vendor1.setName("Test Vendor for ALL Locations");
		vendor1.setContactName("Bilbo Baggins");
		vendor1.setEmail("dzaytsev@hotmail.com");
		vendor1.setAddress("1000 Vendor1 St, Aliquippa, PA 15001");
		vendor1.setServices("This vendor does stuff at ALL locations");
		vendor1.setPhone("1111111111");
		vendor1.setLocationCanEdit(true);
		vendor1.setLocationCanView(true);
		//vendor1.getLocations().add("ALL");
		
		drv.findElement(By.id("licensed_locations_all")).click();
		drv.findElement(By.id("vendors_location_can_view")).click();
		drv.findElement(By.id("vendors_location_can_edit")).click();
		drv.findElement(By.id("vendors_name")).sendKeys(vendor1.getName());
		drv.findElement(By.id("vendors_contact_name")).sendKeys(vendor1.getContactName());
		drv.findElement(By.id("vendors_email")).sendKeys(vendor1.getEmail());
		drv.findElement(By.id("vendors_street")).sendKeys(vendor1.getAddress());
		drv.findElement(By.id("vendors_phone")).sendKeys(vendor1.getPhone());
		drv.findElement(By.id("vendors_services")).sendKeys(vendor1.getServices());
		drv.findElement(By.id("vendors_name")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;	

		//Verify we are on the correct page
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/vendors/index.php") < homePage.length() )
		{
			log.error("ADD VENDOR: We got redirected to an unexpected URL: " + currentUrl);
			return false ;
		}

		//Checking if new vendor showed up on the screen
		log.info("ADD VENDOR: Submitted first vendor, adding a second vendor. Make sure it showed up");
		if (!TestUtils.findText(drv, "Bilbo Baggins") ||
		    !TestUtils.findText(drv, "Test Vendor for ALL Locations") ||
		    !TestUtils.findText(drv, "All Locations")
		)
		{
			log.error("ADD VENDOR: Data for the added vendor did not display on the screen");
			return false ;
		}
		
		//Remember the Vendor for the future use
		log.info("ADD VENDOR: Get id of the freshly added vendor");
		List<WebElement> allVendors = drv.findElements(By.linkText("Delete")) ;
		List<String> allVendorsIds = TestUtils.getIdStrings(drv, allVendors, "vendors_id") ;
		String vendor1Id = allVendorsIds.get(0) ;
		log.info("ADD VENDOR: Vendor ID extracted and equals to: " + vendor1Id) ;
		vendor1.setId(vendor1Id) ;
		System.out.println("First vendor id = '" + vendor1Id + "'") ;

		TestCache.getInstance().getMyVendors().add(vendor1) ;
		
		//Now we can create 2 more vendors.
		//First vendor will have first location
		drv.findElement(By.linkText("Add Vendor")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		Location location1 = TestCache.getInstance().getMyLocations().get(0) ;
		Location location2 = TestCache.getInstance().getMyLocations().get(1) ;
		
		log.info("ADD VENDOR: Adding a vendor for location: " + location1.getName());
		String service2 = "This vendor works for " + location1.getName() + " only" ;
		Vendor vendor2 = new Vendor() ;
		vendor2.setName("Vendor for Location " + location1.getName());
		vendor2.setContactName("Frodo Baggins");
		vendor2.setEmail("dzaytsev+1@hotmail.com");
		vendor2.setAddress("2000 Vendor2 St, Aliquippa, PA 15002");
		vendor2.setPhone("2222222222") ;
		vendor2.setServices(service2);
		vendor2.setLocationCanEdit(true);
		vendor2.setLocationCanView(true);
		//vendor2.getLocations().add(location1.getId());
		
		//drv.findElement(By.id("licensed_locations_all")).click();
		drv.findElement(By.id("vendors_location_can_view")).click();
		drv.findElement(By.id("vendors_location_can_edit")).click();
		drv.findElement(By.id("vendors_name")).sendKeys(vendor2.getName());
		drv.findElement(By.id("vendors_contact_name")).sendKeys(vendor2.getContactName());
		drv.findElement(By.id("vendors_email")).sendKeys(vendor2.getEmail());
		drv.findElement(By.id("vendors_street")).sendKeys(vendor2.getAddress());
		drv.findElement(By.id("vendors_phone")).sendKeys(vendor2.getPhone());
		drv.findElement(By.id("vendors_services")).sendKeys(vendor2.getServices());
		String location_checkbox1 = "licensed_locations_" + location1.getId() ;
		drv.findElement(By.id(location_checkbox1)).click();
		drv.findElement(By.id("vendors_name")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;	

		List<WebElement> secondVendors = drv.findElements(By.linkText("Delete")) ;
		List<String> secondVendorsIds = TestUtils.getIdStrings(drv, secondVendors, "vendors_id") ;

		List<String> newIDs = TestUtils.getAddedIds(allVendorsIds, secondVendorsIds) ;
		if ( newIDs.size() != 1 ) 
		{
			log.error("ADD VENDOR: " + TestUtils.showStringList("Not just 1 vendor was added: ", newIDs)) ;
			return false ;
		}
		String secondNewVendorId = newIDs.get(0) ;
		vendor2.setId(secondNewVendorId);
		
		TestCache.getInstance().getMyVendors().add(vendor2) ;
		
		
		//Second vendor will have second location
		drv.findElement(By.linkText("Add Vendor")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		log.info("ADD VENDOR: Adding a vendor for location: " + location2.getName()) ;
		String service3 = "This vendor works for " + location2.getName() + " only" ;
		Vendor vendor3 = new Vendor() ;
		vendor3.setName("Vendor for Location " + location2.getName());
		vendor3.setContactName("Gandalf");
		vendor3.setEmail("dzaytsev+2@hotmail.com");
		vendor3.setAddress("3000 Vendor3 St, Aliquippa, PA 15003");
		vendor3.setPhone("3333333333");
		vendor3.setServices(service3);
		vendor3.setLocationCanEdit(true);
		vendor3.setLocationCanView(true);
		//vendor3.getLocations().add(location2.getId());
		
		//drv.findElement(By.id("licensed_locations_all")).click();
		drv.findElement(By.id("vendors_location_can_view")).click();
		drv.findElement(By.id("vendors_location_can_edit")).click();
		drv.findElement(By.id("vendors_name")).sendKeys(vendor3.getName());
		drv.findElement(By.id("vendors_contact_name")).sendKeys(vendor3.getContactName());
		drv.findElement(By.id("vendors_email")).sendKeys(vendor3.getEmail());
		drv.findElement(By.id("vendors_street")).sendKeys(vendor3.getAddress());
		drv.findElement(By.id("vendors_phone")).sendKeys(vendor3.getPhone());
		drv.findElement(By.id("vendors_services")).sendKeys(vendor3.getServices());
		String location_checkbox2 = "licensed_locations_" + location2.getId() ;
		drv.findElement(By.id(location_checkbox2)).click();
		drv.findElement(By.id("vendors_name")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;	
		
		
		List<WebElement> thirdVendors = drv.findElements(By.linkText("Delete")) ;
		List<String> thirdVendorsIds = TestUtils.getIdStrings(drv, thirdVendors, "vendors_id") ;

		newIDs = TestUtils.getAddedIds(secondVendorsIds, thirdVendorsIds) ;
		if ( newIDs.size() != 1 ) 
		{
			log.error("ADD VENDOR: " + TestUtils.showStringList("Not just 1 vendor was added: ", newIDs)) ;
			return false ;
		}
		String thirdNewVendorId = newIDs.get(0) ;
		vendor3.setId(thirdNewVendorId);
		
		TestCache.getInstance().getMyVendors().add(vendor3) ;
		
		log.info("ADD VENDOR: Testing add/hide/un-hide/delete vendors is completed");
		return true ;
	}
	
	public boolean editVendor(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		String temp ;
		log.info("EDIT VENDORS: Test starting. Editing descriptions, names, and location for vendors") ;
		log.info("EDIT VENDORS: Make sure we are at the right place") ;
		String currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/vendors/index.php") < homePage.length() )
		{
			log.error("EDIT VENDORS: Current page is not what we expected. Current page: " + currentUrl);
			log.info("EDIT VENDORS: Navigate to the list of vendors page.");
		}
		//Navigate to the correct page
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		drv.findElement(By.partialLinkText("Certificate")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		drv.findElement(By.linkText("Manage Vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("EDIT VENDORS: Make sure AGAIN we are at the right place") ;
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/vendors/index.php") < homePage.length() )
		{
			log.error("EDIT VENDORS: Second URL check failed. Current page: " + currentUrl + ". Test failed.");
			return false ;
		}
		
		List<Vendor> vendors = TestCache.getInstance().getMyVendors() ;
		Vendor vendor1 = vendors.get(0) ;
		String vendor1Id = vendor1.getId() ;
		WebElement vendor1Link = TestUtils.getLinkElementById(drv, "Edit", "vendors_id", vendor1Id) ;
		if ( vendor1Link == null )
		{
			log.error("EDIT VENDORS: Link to edit vendor " + vendor1Id + " not found. Failing the test");
			return false ;
		}
		
		vendor1Link.click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//We are supposed to land on vendor 1 edit page
		currentUrl = drv.getCurrentUrl() ;
		temp = homePage + "/members/vendors/edit.php?vendors_id=" + vendor1Id ;
		if ( !currentUrl.equalsIgnoreCase(temp) )
		{
			log.error("EDIT VENDORS: Second URL check failed. Current page: " + currentUrl + ". Test failed.");
			return false ;
		}

		boolean canView	= drv.findElement(By.id("vendors_location_can_view")).isSelected() ;
		boolean canEdit = drv.findElement(By.id("vendors_location_can_edit")).isSelected() ;
		String name		= drv.findElement(By.id("vendors_name")).getAttribute("value") ;
		String contact	= drv.findElement(By.id("vendors_contact_name")).getAttribute("value") ;
		String email	= drv.findElement(By.id("vendors_email")).getAttribute("value") ;
		String phone	= drv.findElement(By.id("vendors_phone")).getAttribute("value") ;
		String street	= drv.findElement(By.id("vendors_street")).getAttribute("value") ;
		String services	= drv.findElement(By.id("vendors_services")).getAttribute("value") ;
		
		boolean allGood = true ;
		if ( !canView ) 
		{
			log.error("EDIT VENDORS: Vendor shows that location cannot view. Not correct");
			allGood = false ;
		}
		if ( !canEdit )
		{
			log.error("EDIT VENDORS: Vendor shows that location cannot edit. Not correct");
			allGood = false ;
		}
		if ( !name.equalsIgnoreCase(vendor1.getName()) )
		{
			log.error("EDIT VENDORS: Displayed name does not match vendor's name");
			log.error("EDIT VENDORS: Displayed Name='" + name + "', should be: '" + vendor1.getName() + "'");
			allGood = false ;
		}
		if ( !contact.equalsIgnoreCase(vendor1.getContactName()) )
		{
			log.error("EDIT VENDORS: Displayed contact name does not match vendor's contact name");
			log.error("EDIT VENDORS: Displayed Contact Name='" + contact + "', should be: '" + vendor1.getContactName() + "'");
			allGood = false ;
		}
		if ( !email.equalsIgnoreCase(vendor1.getEmail()) )
		{
			log.error("EDIT VENDORS: Displayed email does not match vendor's email address");
			log.error("EDIT VENDORS: Displayed email='" + email + "', should be: '" + vendor1.getEmail() + "'");
			allGood = false ;
		}
		//if ( !phone.equalsIgnoreCase(vendor1.getPhone()) )
		//{
		//	log.error("EDIT VENDORS: Displayed phone number does not match vendor's phone number");
		//	log.error("EDIT VENDORS: Displayed phone number='" + phone + "', should be: '" + vendor1.getPhone() + "'");
		//	allGood = false ;
		//}
		if ( !street.equalsIgnoreCase(vendor1.getAddress()) )
		{
			log.error("EDIT VENDORS: Displayed street address does not match vendor's address");
			log.error("EDIT VENDORS: Displayed street address='" + street + "', should be: '" + vendor1.getAddress() + "'");
			allGood = false ;
		}
		if ( !services.equalsIgnoreCase(vendor1.getServices()) )
		{
			log.error("EDIT VENDORS: Displayed services do not match vendor's services");
			log.error("EDIT VENDORS: Displayed services='" + vendor1.getServices() + "', should be: '" + vendor1.getServices() + "'");
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("EDIT VENDORS: Not all fields were shown correctly. Test failed.");
			return false ;
		}
		log.info("EDIT VENDORS: Vendor's information is displayed correctly");
		
		log.info("EDIT VENDORS: Attempting to change options for vendor 1 - change locations, name and services");
		log.info("EDIT VENDORS: Clear input box for vendor's services");
		drv.findElement(By.id("vendors_services")).clear();
		log.info("EDIT VENDORS: Clear input box for vendor's name") ;
		drv.findElement(By.id("vendors_name")).clear();
		log.info("EDIT VENDORS: Clear check box for all locations");
		drv.findElement(By.id("licensed_locations_all")).click();
		if ( drv.findElement(By.id("licensed_locations_all")).isSelected() )
		{
			log.error("EDIT VENDORS: All locations check box did not get cleared");
			return false ;
		}
		
		Location location1 = TestCache.getInstance().getMyLocations().get(0) ;
		Location location2 = TestCache.getInstance().getMyLocations().get(1) ;
		
		String location1Id = location1.getId() ;
		String location2Id = location2.getId() ;
		
		vendor1.setName("GUM, Inc.");
		vendor1.setServices("Main Test Vendor") ;
		
		log.info("EDIT VENDORS: Changing vendor's services to: " + vendor1.getServices()) ;
		drv.findElement(By.id("vendors_services")).sendKeys(vendor1.getServices());
		log.info("EDIT VENDORS: Changing vendor's name to: " + vendor1.getName());
		drv.findElement(By.id("vendors_name")).sendKeys(vendor1.getName());
		log.info("EDIT VENDORS: Checking check box for location " + location1.getName()) ;
		drv.findElement(By.id("licensed_locations_" + location1Id)).click();
		log.info("EDIT VENDORS: Checking check box for location " + location2.getName());
		drv.findElement(By.id("licensed_locations_" + location2Id)).click();
		
		drv.findElement(By.id("vendors_name")).submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if ( !TestUtils.findText(drv, vendor1.getName()) )
		{
			log.error("EDIT VENDORS: The updated vendor's name was not shown on the screen. Rename failed. Test failed.");
			return false ;
		}
		log.info("EDIT VENDORS: Edit vendors test completed successfully");
		
		return true ;
		
	}
	
	public boolean addCertificates(WebDriver drv)
	{
		boolean result = true ;
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;

		//Making sure prerequisites are there:
		log.info("ADD: Making sure we have cached locations for the test");
		if ( TestCache.getInstance().getMyLocations() == null || 
			 TestCache.getInstance().getMyLocations().size() == 0 )
		{
			log.info("ADD: No cached locations found. Need to run locations test");
			MyAccountTest accts = new MyAccountTest() ;
			if ( !accts.TestMyAccountAddLocation(drv) )
			{
				log.error("ADD: Locations test failed. Quitting the test");
				return false ;
			}
		}
		log.info("ADD: Making sure we have cached vendors to for the test") ;
		if ( TestCache.getInstance().getMyVendors() == null || 
			 TestCache.getInstance().getMyVendors().size() == 0 )
		{
			log.info("ADD: No cached vendors found. Need to run vendors test");
			if ( !addVendor(drv) )
			{
				log.error("ADD: Vendors test failed. Quitting the test");
				return false ;
			}
		}
		
		log.info("ADD: Starting adding certificate test");
		//Click on MyWUC to make sure we are at the right place (assuming we're logged in)
		drv.findElement(By.partialLinkText("My WAKEUP")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("ADD: Get to Certificates Tracking section of My WUC");
		//Go to Certificates Tracking section
		drv.findElement(By.linkText("Certificate Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Get list of existing certificates:
		List<WebElement> existingCertificates = drv.findElements(By.linkText("Delete")) ;
		List<String> existingCertificateIds = TestUtils.getIdStrings(drv, existingCertificates, "certificates_id") ;
		
		log.info("ADD: Going to add certificate form");
		drv.findElement(By.linkText("Add Certificate")).click();
		String currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/my-documents/certificates/edit.php") < homePage.length() )
		{
			log.error("ADD: Got redirected to an unexpected URL: " + currentUrl);
			return false ;
		}
		Location loc1 = TestCache.getInstance().getMyLocations().get(0) ;
		Location loc2 = TestCache.getInstance().getMyLocations().get(1) ;
		List<Vendor> vendors = TestCache.getInstance().getMyVendors() ;
		
		WebElement location = drv.findElement(By.id("licensed_locations_id")) ;
		//Select chooseLocation = new Select(drv.findElement(By.id("licensed_locations_id"))) ;
		log.info("ADD: Choose location A and get a list of 2 possible vendors");
		TestUtils.selectItem(location, loc1.getName()) ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		drv.findElement(By.id("certificates_type")).submit();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("ADD: We expect URL not to change and error messages must be displayed");
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/my-documents/certificates/edit.php") < homePage.length() )
		{
			log.error("ADD: Got redirected to an unexpected URL: " + currentUrl);
			result = false ;
		}	
		
		//if ( !TestUtils.findText(drv, "Please select a location") )
		//{
		//	log.error("ADD: Error message 'Please select a location' was not displayed on the screen");
		//	result = false ;
		//}
		if ( !TestUtils.findText(drv, "Please select a vendor") )
		{
			log.error("ADD: Error message 'Please select a vendor' was not displayed on the screen");
			//result = false ;
		}
		if ( !TestUtils.findText(drv, "Please select at least one coverage type") )
		{
			log.error("ADD: Error message 'Please select at least one coverage type' was not displayed on the screen");
			//result = false ;
		}
		if ( !TestUtils.findText(drv, "Please enter the expiration date of the certificate") )
		{
			log.error("ADD: Error message 'Please enter the expiration date of the certificate' was not displayed on the screen");
			//result = false ;
		}
		//if ( !result )
		//{
		//	log.error("ADD: Not all expected error messages were displayed on the form");
		//	return result;
		//}
		//result = true ;
		
		log.info("ADD: Starting to fill up the form for location 1: should give choice of 2 vendors");
		
		
		List availableVendors = TestUtils.getDropDownItems(drv, "join_vendors_id") ;
		//Making sure we have correct vendors to choose from
		if ( availableVendors.size() != 3 )
		{
			log.error("ADD: Incorrect number of vendors was displayed = " + availableVendors.size());
			return false ;
		}
		
		//System.out.println(availableVendors.get(2) + " type: " + availableVendors.get(2).getClass().getName()) ;
		
		Vendor vendorCommon = vendors.get(0) ;
		Vendor vendorLoc1   = vendors.get(1) ;
		Vendor vendorLoc2   = vendors.get(2) ;
				
		if ( (TestUtils.findStringInList(availableVendors, vendorCommon.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorCommon.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc1.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorLoc1.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc2.getName(), false)) >= 0 )
		{
			log.error("ADD: Vendor '" + vendorLoc2.getName() + "' was displayed, wrong. Test failed.");
			return false ;
		}
		
		drv.findElement(By.id("certificates_location_can_view")).click();
		drv.findElement(By.id("certificates_location_can_edit")).click();
		
		log.info("ADD: Choosing vendor '" + vendorLoc1.getName() + "' in the dropdown");
		WebElement vendor = drv.findElement(By.id("join_vendors_id")) ;
		TestUtils.selectItem(vendor, vendorLoc1.getName()) ;
		log.info("ADD: Submitting incompletely filled out form and check for error messages");
		//drv.findElement(By.id("certificates_location_can_edit")).submit();
		drv.findElement(By.id("save")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Please select at least one coverage type") )
		{
			log.error("ADD: Error message 'Please select at least one coverage type' was not displayed on the screen");
			//result = false ;
		}
		if ( !TestUtils.findText(drv, "Please enter the expiration date of the certificate") )
		{
			log.error("ADD: Error message 'Please enter the expiration date of the certificate' was not displayed on the screen");
			//result = false ;
		}
		if ( !result )
		{
			log.error("ADD: Not all expected error messages were displayed on the form");
			//return result;
		}
		//result = true ;
		
		log.info("ADD: Choosing a certificate1.txt file for upload");
		drv.findElement(By.id("certificates_file")).sendKeys("C:\\WakeUpCallScripts\\CERTIFICATE1.txt");
		WebElement certificates_email_days = drv.findElement(By.id("certificates_email_days")) ;
		log.info("ADD: Choosing 15 days to warn before certificate expiration");
		TestUtils.selectItem(certificates_email_days, "15");
		log.info("ADD: Choosing General Liability coverage");
		drv.findElement(By.id("coverage_gl")).click() ;
		log.info("ADD: Trying to set incorrect date");
		drv.findElement(By.id("certificates_expire")).click(); 
		drv.switchTo().activeElement() ;
		TestUtils.calendarDayChoose(drv, "cal-container", "2017", "Jun", "26");
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.switchTo().parentFrame() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		Certificate certificate1 = new Certificate() ;
		certificate1.setCertFileName("CERTIFICATE1.txt");
		certificate1.setDaysBeforeExp("15");
		certificate1.setExpirationDate("06/26/2017");
		certificate1.setLocation(loc1);
		certificate1.setLocationCanEdit(true);
		certificate1.setLocationCanView(true);
		certificate1.setOperationsType("Regular Operations");
		certificate1.setRemindUntilUpd("Yes");
		certificate1.setSendToVendor(true);
		certificate1.setTypeGenLiability(true);
		certificate1.setVendor(vendorLoc1);
		
		log.info("ADD: Submitting form with first certificate");
		//certificates_email_days.submit(); 
		drv.findElement(By.id("save")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("ADD: Certificate submitted. We expect to be back on the certificates list page");
		currentUrl = drv.getCurrentUrl() ;
		if (currentUrl.indexOf("/members/my-documents/certificates/index.php") < homePage.length())
		{
			log.error("ADD: We got redirected to an unexpected URL: " + currentUrl);
			return false ;
		}

		log.info("ADD: Getting updated list of certificates to extract new id");
		List<WebElement> updatedCertificates = drv.findElements(By.linkText("Delete")) ;
		List<String> updatedCertificateIds = TestUtils.getIdStrings(drv, updatedCertificates, "certificates_id") ;

		List<String> newIDs = TestUtils.getAddedIds(existingCertificateIds, updatedCertificateIds) ;
		if ( newIDs.size() > 1 ) 
		{
			log.error("ADD: " + TestUtils.showStringList("More than 1 certificate was added: ", newIDs)) ;
			return false ;
		}
		log.info("ADD: Certificate id is " + newIDs.get(0));
		certificate1.setId(newIDs.get(0)) ;
		TestCache.getInstance().getMyCertificates().add(certificate1) ;
		log.info("ADD: Added first certificate to the cache. Now there are: " + TestCache.getInstance().getMyCertificates().size());

		//assign updated list of ids to the "existing" list of certificates
		//------------------------------------------------------------------
		existingCertificateIds = updatedCertificateIds ;
		
		Certificate certificate2 = new Certificate() ;
		certificate2.setCertFileName("CERTIFICATE2.txt");
		certificate2.setDaysBeforeExp("30");
		certificate2.setExpirationDate("08/20/2017");
		certificate2.setLocation(loc2);
		certificate2.setLocationCanEdit(false);
		certificate2.setLocationCanView(true);
		certificate2.setOperationsType("Special Projects");
		certificate2.setProjectName("Grand Theft Auto") ;
		certificate2.setRemindUntilUpd("Yes");
		certificate2.setSendToVendor(true);
		certificate2.setTypeCrime(true);
		certificate2.setVendor(vendorLoc2);
		
		log.info("ADD: Adding second certificate for the second location");
		drv.findElement(By.linkText("Add Certificate")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		location = drv.findElement(By.id("licensed_locations_id")) ;
		log.info("ADD: Choose location B and get a list of 2 possible vendors");
		TestUtils.selectItem(location, loc2.getName()) ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		availableVendors = TestUtils.getDropDownItems(drv, "join_vendors_id") ;
		if ( availableVendors.size() != 3 )
		{
			log.error("ADD: Incorrect number of vendors was displayed = " + availableVendors.size());
			return false ;
		}

		if ( (TestUtils.findStringInList(availableVendors, vendorCommon.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorCommon.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc2.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorLoc2.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc1.getName(), false)) >= 0 )
		{
			log.error("ADD: Vendor '" + vendorLoc1.getName() + "' was displayed, wrong. Test failed.");
			return false ;
		}
		vendor = drv.findElement(By.id("join_vendors_id")) ;
		TestUtils.selectItem(vendor, vendorLoc2.getName()) ;
		log.info("ADD: Choosing Special Projects certificate type");
		WebElement certificateType = drv.findElement(By.id("certificates_type")) ;
		TestUtils.selectItem(certificateType, "Special Projects") ;
		drv.findElement(By.id("certificates_project_name")).sendKeys("Grand Theft Auto");
		drv.findElement(By.id("certificates_location_can_view")).click();

		log.info("ADD: Choosing a certificate2.txt file for upload");
		drv.findElement(By.id("certificates_file")).sendKeys("C:\\WakeUpCallScripts\\CERTIFICATE2.txt");
		certificates_email_days = drv.findElement(By.id("certificates_email_days")) ;
		log.info("ADD: Choosing 30 days to warn before certificate expiration");
		TestUtils.selectItem(certificates_email_days, "30");
		log.info("ADD: Choosing Crime coverage");
		drv.findElement(By.id("coverage_crime")).click() ;
		log.info("ADD: Setting expiration date to August 20, 2017");
		drv.findElement(By.id("certificates_expire")).click(); 
		drv.switchTo().activeElement() ;
		TestUtils.calendarDayChoose(drv, "cal-container", "2017", "Aug", "20");
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.switchTo().parentFrame() ;
		log.info("ADD: Submitting 2nd certificate for the location B vendor");
		drv.findElement(By.id("save")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("ADD: Getting updated list of certificates to extract new id");
		updatedCertificates = drv.findElements(By.linkText("Delete")) ;
		updatedCertificateIds = TestUtils.getIdStrings(drv, updatedCertificates, "certificates_id") ;

		newIDs = TestUtils.getAddedIds(existingCertificateIds, updatedCertificateIds) ;
		if ( newIDs.size() > 1 ) 
		{
			log.error("ADD: " + TestUtils.showStringList("More than 1 certificate was added: ", newIDs)) ;
			return false ;
		}
		log.info("ADD: Certificate id is " + newIDs.get(0));
		certificate2.setId(newIDs.get(0)) ;
		TestCache.getInstance().getMyCertificates().add(certificate2) ;
		log.info("ADD: Added second certificate to the cache. Now there are: " + TestCache.getInstance().getMyCertificates().size());
		
		//=============================================================
		existingCertificates = updatedCertificates ;
		existingCertificateIds = updatedCertificateIds ;

		Certificate certificate3 = new Certificate() ;
		certificate3.setCertFileName("CERTIFICATE3.txt");
		certificate3.setDaysBeforeExp("0");
		certificate3.setExpirationDate("09/11/2018");
		certificate3.setLocation(loc2);
		certificate3.setLocationCanEdit(false);
		certificate3.setLocationCanView(true);
		certificate3.setOperationsType("Special Projects");
		certificate3.setProjectName("Crime Scene Cleanup") ;
		certificate3.setRemindUntilUpd("Yes");
		certificate3.setSendToVendor(true);
		certificate3.setTypeCrime(true);
		certificate3.setVendor(vendorCommon);
		
		
		log.info("ADD: Adding third certificate for the second location and common vendor");
		drv.findElement(By.linkText("Add Certificate")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		location = drv.findElement(By.id("licensed_locations_id")) ;
		log.info("ADD: Choose location B and get a list of 2 possible vendors");
		TestUtils.selectItem(location, loc2.getName()) ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		availableVendors = TestUtils.getDropDownItems(drv, "join_vendors_id") ;
		if ( availableVendors.size() != 3 )
		{
			log.error("ADD: Incorrect number of vendors was displayed = " + availableVendors.size());
			return false ;
		}

		if ( (TestUtils.findStringInList(availableVendors, vendorCommon.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorCommon.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc2.getName(), false)) == -1 )
		{
			log.error("ADD: Vendor '" + vendorLoc2.getName() + "' was not displayed");
			return false ;
		}
		if ( (TestUtils.findStringInList(availableVendors, vendorLoc1.getName(), false)) >= 0 )
		{
			log.error("ADD: Vendor '" + vendorLoc1.getName() + "' was displayed, wrong. Test failed.");
			return false ;
		}
		vendor = drv.findElement(By.id("join_vendors_id")) ;
		TestUtils.selectItem(vendor, vendorCommon.getName()) ;
		log.info("ADD: Choosing Special Projects certificate type");
		certificateType = drv.findElement(By.id("certificates_type")) ;
		TestUtils.selectItem(certificateType, "Special Projects") ;
		drv.findElement(By.id("certificates_project_name")).sendKeys("Crime Scene Cleanup");
		drv.findElement(By.id("certificates_location_can_view")).click();
		//certificates_remind_other
		log.info("ADD: Choosing a certificate3.txt file for upload");
		drv.findElement(By.id("certificates_file")).sendKeys("C:\\WakeUpCallScripts\\CERTIFICATE3.txt");
		certificates_email_days = drv.findElement(By.id("certificates_email_days")) ;
		log.info("ADD: Choosing 30 days to warn before certificate expiration");
		TestUtils.selectItem(certificates_email_days, "0");
		log.info("ADD: Choosing Crime coverage");
		drv.findElement(By.id("coverage_crime")).click() ;
		log.info("ADD: Setting expiration date to September 11, 2018") ;
		drv.findElement(By.id("certificates_expire")).click(); 
		drv.switchTo().activeElement() ;
		TestUtils.calendarDayChoose(drv, "cal-container", "2018", "Sep", "11");
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.switchTo().parentFrame() ;
		log.info("ADD: Submitting 3rd certificate for the common vendor") ;
		drv.findElement(By.id("save")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("ADD: Getting updated list of certificates to extract new id");
		updatedCertificates = drv.findElements(By.linkText("Delete")) ;
		updatedCertificateIds = TestUtils.getIdStrings(drv, updatedCertificates, "certificates_id") ;

		newIDs = TestUtils.getAddedIds(existingCertificateIds, updatedCertificateIds) ;
		if ( newIDs.size() > 1 ) 
		{
			log.error("ADD: " + TestUtils.showStringList("More than 1 certificate was added: ", newIDs)) ;
			return false ;
		}
		log.info("ADD: Certificate id is " + newIDs.get(0));
		certificate3.setId(newIDs.get(0)) ;
		TestCache.getInstance().getMyCertificates().add(certificate3) ;
		
		log.info("ADD: Added third certificate to the cache. Now there are: " + TestCache.getInstance().getMyCertificates().size());
		log.info("ADD: Form submitted. Test to add certificates is completed");
		
		return true ;
	}
	
	public boolean editCertificate(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;

		String currentUrl = drv.getCurrentUrl() ;
		log.info("EDIT: Starting test on: " + currentUrl);
		if ( currentUrl.indexOf("members/my-documents/certificates/index.php") > 0 )
		{
			log.error("EDIT: Not the right place to start the certificates edit test");
			log.info("Going to the correct place...");
			drv.findElement(By.partialLinkText("My WAKEUP")).click(); 
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.findElement(By.linkText("Certificate Tracking")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			currentUrl = drv.getCurrentUrl() ;
			log.info("EDIT: Now we are at: " + currentUrl);
		}
		
		List newCertificates = TestCache.getInstance().getMyCertificates() ;
		if ( newCertificates == null || newCertificates.size() == 0 )
		{
			log.error("EDIT: No new certificates for the test. Quitting.") ;
			return false ;
		}
		log.info("EDIT: We have " + newCertificates.size() + " to run the test");
		log.info("EDIT: Opening the first certificate for editing") ;
		Certificate cert1 = (Certificate)newCertificates.get(0) ;
		String cert1Id    = cert1.getId() ;
		String cert1EditUrl  = homePage + "/members/my-documents/certificates/edit.php?certificates_id=" + cert1Id ;
		log.info("EDIT: Clicking on edit certificate link: " + cert1EditUrl);
		WebElement cert1Link = TestUtils.getLinkElementById(drv, "Edit", "certificates_id", cert1Id) ;
		if ( cert1Link == null )
		{
			log.error("EDIT: Edit link for certificate " + cert1Id + " is not found");
			return false ;
		}
		cert1Link.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//http://staging.wakeupcall.net/members/my-documents/certificates/edit.php?certificates_id=501
		String expectedUrl = homePage + "/members/my-documents/certificates/edit.php?certificates_id=" + cert1Id ;
		log.info("EDIT: Making sure we got to the right page: " + expectedUrl);
		currentUrl = drv.getCurrentUrl() ;
		if (!expectedUrl.equalsIgnoreCase(currentUrl))
		{
			log.error("EDIT: We are redirected to an unexpected URL: " + currentUrl);
			return false ;
		}
		log.info("EDIT. LOCATION FOR CERT: " + drv.findElement(By.id("licensed_locations_id")).getAttribute("value") ) ;
		Select location = new Select(drv.findElement(By.id("licensed_locations_id"))) ;
		String locationNameForm = location.getFirstSelectedOption().getText() ;
		String locationNameCert = cert1.getLocation().getName() ;
		boolean allGood = true ;
		if ( !locationNameForm.equalsIgnoreCase(locationNameCert) )
		{
			log.error("EDIT: location shown on the form incorrectly") ;
			allGood = false ;
		}
		log.info("EDIT: Checking if vendor shows up correctly");
		Select vendor = new Select(drv.findElement(By.id("join_vendors_id"))) ;
		String vendorNameForm = vendor.getFirstSelectedOption().getText() ;
		String vendorNameCert = cert1.getVendor().getName() ;
		if ( !vendorNameForm.equalsIgnoreCase(vendorNameCert) )
		{
			log.error("EDIT: vendor is not shown on the form correctly");
			allGood = false ;
		}
		log.info("EDIT: Checking if certificate type shows up correctly");
		Select certType = new Select(drv.findElement(By.id("certificates_type"))) ;
		String certTypeForm = certType.getFirstSelectedOption().getText() ;
		String certTypeCert = cert1.getOperationsType() ;
		if ( !certTypeForm.equalsIgnoreCase(certTypeCert) )
		{
			log.error("EDIT: certificate type displayed is not correct");
			allGood = false ;
		}
		log.info("EDIT: Checking project name");
		WebElement projectNameTxt = null ;
		try
		{
			projectNameTxt = drv.findElement(By.id("certificates_project_name")) ;
		}
		catch(Exception ex)
		{
			log.error("EDIT: Exception finding element for project name: " + ex.getMessage());
			ex.printStackTrace() ;
		}
		if ( projectNameTxt != null )
		{
			String projectNameForm = projectNameTxt.getAttribute("value") ;
			String projectNameCert = cert1.getProjectName() ;
			if ( projectNameForm == null && projectNameCert != null )
			{
				log.error("EDIT: Project name is empty on the form but not empty in the certificate. This is incorrect");
				allGood = false ;
			}
			else if ( projectNameForm != null && projectNameCert == null )
			{
				log.error("EDIT: Project name is not empty on the form but empty in the certificate. This is incorrect");
				allGood = false ;
			}
			else if ( projectNameForm != null && !projectNameForm.equalsIgnoreCase(projectNameCert) )
			{
				log.error("EDIT: Project names are different between form and certificate");
				allGood = false ;
			}
		}
		else 
		{
			String projectNameCert = cert1.getProjectName() ;
			if ( projectNameCert != null && projectNameCert.trim().length() > 0 )
			{
				log.error("EDIT: Project name textbox was not displayed but certificate has project name that is not null or empty");
				allGood = false ;
			}
		}
		log.info("EDIT: Check location can view checkbox");
		boolean locCanViewForm = drv.findElement(By.id("certificates_location_can_view")).isSelected() ;
		boolean locCanViewCert = cert1.isLocationCanView() ;
		if ( locCanViewForm != locCanViewCert )
		{
			log.error("EDIT: Location can view does not match between form and certificate");
			allGood = false ;
		}
		log.info("EDIT: Check location can edit checkbox");
		boolean locCanEditForm = drv.findElement(By.id("certificates_location_can_edit")).isSelected() ;
		boolean locCanEditCert = cert1.isLocationCanEdit() ;
		if ( locCanEditForm != locCanEditCert )
		{
			log.error("EDIT: Location can edit does not match between form and certificate");
			allGood = false ;
		}
		log.info("EDIT: Checking certificate file name");
		String certFile = TestUtils.getFileNameFromPath(cert1.getCertFileName(), "//") ;
		if ( !TestUtils.findText(drv, certFile) )
		{
			log.error("EDIT: Certificate's file name is not found on the page");
			allGood = false ;
		}
		log.info("EDIT: Checking email before expiration number of days");
		Select emailDaysSelect = new Select(drv.findElement(By.id("certificates_email_days"))) ;
		String emailDaysForm = emailDaysSelect.getFirstSelectedOption().getText().trim() ;
		String emailDaysCert = cert1.getDaysBeforeExp().trim().trim();
		if ( !emailDaysForm.equalsIgnoreCase(emailDaysCert) )
		{
			log.error("EDIT: Days until expiration is different on the form '" + emailDaysForm + "' and certificate '" + emailDaysCert + "'.");
			allGood = false ;
		}
		
		//Reminder options
		log.info("EDIT: Make sure send reminder options are displayed correctly");
		log.info("EDIT: Remind vendor");
		boolean remindVendorForm = drv.findElement(By.id("certificates_remind_vendor")).isSelected() ;
		boolean remindVendorCert = cert1.isSendToVendor() ;
		if ( remindVendorForm != remindVendorCert )
		{
			log.error("EDIT: Remind to user option is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Remind location admin");
		boolean remindLocAdmForm = drv.findElement(By.id("certificates_send_copy")).isSelected() ;
		boolean remindLocAdmCert = cert1.isSendToLocAdmin() ;
		if ( remindLocAdmForm != remindLocAdmCert )
		{
			log.error("EDIT: Remind to location admin is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Remind account admin");
		boolean remindCrpAdmForm = drv.findElement(By.id("certificates_remind_corporate_admin")).isSelected() ;
		boolean remindCrpAdmCert = cert1.isSendToAccAdmin() ;
		if ( remindCrpAdmForm != remindCrpAdmCert )
		{
			log.error("EDIT: Remind to account admin is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Remind other") ;
		boolean remindOtherForm  = drv.findElement(By.id("certificates_remind_other_chk")).isSelected() ;
		boolean remindOtherCert  = cert1.isSendToAddress() ;
		if ( remindOtherForm != remindOtherCert )
		{
			log.error("EDIT: Remind other is not displayed correctly");
			allGood = false ;
		}
		WebElement other = drv.findElement(By.id("certificates_remind_other")) ;
		if ( other.isEnabled() )
		{
			log.info("EDIT: Remind other text");
			String otherForm = other.getAttribute("value") ;
			String otherCert = cert1.getTypeOtherText() ;
			if ( otherCert != null && !otherCert.equalsIgnoreCase(otherForm) )
			{
				log.error("EDIT: Send to other text is different.");
				allGood = false ;
			}
		}
		log.info("EDIT: Checking selected coverages");
		log.info("EDIT: Check General Liability checkbox");
		boolean coverageGLForm  = drv.findElement(By.id("coverage_gl")).isSelected() ;
		boolean coverageGLCert  = cert1.isTypeGenLiability() ;
		if ( coverageGLForm != coverageGLCert )
		{
			log.error("EDIT: General Liability coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Umbrella coverages");
		boolean coverageUmForm  = drv.findElement(By.id("coverage_umbrella")).isSelected() ;
		boolean coverageUmCert  = cert1.isTypeUmbrella() ;
		if ( coverageUmForm != coverageUmCert )
		{
			log.error("EDIT: Umbrella coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Workman's Comp checkbox");
		boolean coverageWCForm  = drv.findElement(By.id("coverage_comp")).isSelected() ;
		boolean coverageWCCert  = cert1.isTypeWorkComp() ;
		if ( coverageWCForm != coverageWCCert )
		{
			log.error("EDIT: Workman's comp coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Automotive checkbox");
		boolean coverageAuForm  = drv.findElement(By.id("coverage_auto")).isSelected() ;
		boolean coverageAuCert  = cert1.isTypeAutomotive() ;
		if ( coverageAuForm != coverageAuCert )
		{
			log.error("EDIT: Automotive coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Crime coverage");
		boolean coverageCrForm  = drv.findElement(By.id("coverage_crime")).isSelected() ;
		boolean coverageCrCert  = cert1.isTypeCrime() ;
		if ( coverageCrForm != coverageCrCert )
		{
			log.error("EDIT: Crime coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Property coverage checkbox");
		boolean coveragePpForm  = drv.findElement(By.id("coverage_prop")).isSelected() ;
		boolean coveragePpCert  = cert1.isTypeProperty() ;
		if ( coveragePpForm != coveragePpCert )
		{
			log.error("EDIT: Property coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Professional coverage checkbox");
		boolean coveragePfForm  = drv.findElement(By.id("coverage_prof")).isSelected() ;
		boolean coveragePfCert  = cert1.isTypeProfessional() ;
		if ( coveragePfForm != coveragePfCert )
		{
			log.error("EDIT: Professional coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Check Building risks coverage checkbox");
		boolean coverageBlForm  = drv.findElement(By.id("coverage_el")).isSelected() ;
		boolean coverageBlCert  = cert1.isTypeBuildRisk() ;
		if ( coverageBlForm != coverageBlCert )
		{
			log.error("EDIT: Building Risk coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Checking Other coverage checkbox");
		boolean coverageOtForm  = drv.findElement(By.id("coverage_other")).isSelected() ;
		boolean coverageOtCert  = cert1.isTypeOther() ;
		if ( coverageOtForm != coverageOtCert )
		{
			log.error("EDIT: Other coverage is not displayed correctly");
			allGood = false ;
		}
		log.info("EDIT: Checking other coverage description field");
		String coverageOther = "" ;
		WebElement coverageOtherTxt = null ;
		try
		{
			coverageOtherTxt = drv.findElement(By.id("certificates_coverage_other")) ;
			coverageOther = coverageOtherTxt.getAttribute("value") ;
		}
		catch(Exception ex)
		{
			log.error("EDIT: Exception trying to find Other coverage description text field: " + ex.getMessage());
			ex.printStackTrace();
		}
		if ( coverageOther != null && coverageOther.length() > 0 )
		{
			if ( !coverageOther.equalsIgnoreCase(cert1.getTypeOtherText())) 
			{
				log.error("EDIT: Other coverage description is not displayed correctly");
				allGood = false ;
			}
		}
		log.info("EDIT: Checking Expiration date field");
		String expirationDateForm = drv.findElement(By.id("certificates_expire")).getAttribute("value") ;
		String expirationDateCert = cert1.getExpirationDate() ;
		if ( !TestUtils.compareDateStrings(expirationDateForm, expirationDateCert) )
		{
			log.error("EDIT: Expiration date was displayed incorrectly: '" + expirationDateForm + "' on the screen vs '" + expirationDateCert + "' in the certificate");
			log.error("Comparison by characters = " + TestUtils.compareStringsByCharacter(expirationDateForm, expirationDateCert));
			allGood = false ;
		}
		Select certExpSel = new Select(drv.findElement(By.id("certificates_remind_member"))) ;
		String certExpForm = certExpSel.getFirstSelectedOption().getText() ;
		String certExpCert = cert1.getRemindUntilUpd() ;
		if ( !certExpForm.equalsIgnoreCase(certExpCert) )
		{
			log.error("EDIT: Remind Me Until Updated is displayed incorrectly. Screen shows '" + certExpForm + "' and certificate has '" + certExpCert + "'.");
			log.error("Comparison by characters = " + TestUtils.compareStringsByCharacter(certExpForm, certExpCert));
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("EDIT: Not all fields were filled out correctly. Failing the test.");
			return false ;
		}
		
		
		//If all fields are displayed correctly, we change data and save it.		
		
		
		drv.findElement(By.id("certificates_location_can_edit")).click();
		drv.findElement(By.id("certificates_remind_other_chk")).click();
		drv.findElement(By.id("certificates_remind_other")).clear();
		drv.findElement(By.id("certificates_remind_other")).sendKeys("ooparina@curiousdog.com") ;
		drv.findElement(By.id("coverage_umbrella")).click();
		drv.findElement(By.id("coverage_comp")).click();
		
		log.info("EDIT: Submitting edited certificate") ;
		drv.findElement(By.id("save")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//make sure we got redirected to the right place
		currentUrl = drv.getCurrentUrl() ;
		log.info("EDIT: We got redirected to: " + currentUrl);
		if ( currentUrl.indexOf("members/my-documents/certificates/index.php") <= 0 )
		{
			log.error("EDIT: We did not land on the expected page");
			return false ;
		}
		
		log.info("EDIT: Click on the certificate we edited to make sure the changes got recorded.") ;
		cert1Link = TestUtils.getLinkElementById(drv, "Edit", "certificates_id", cert1Id) ;
		if ( cert1Link == null )
		{
			log.error("EDIT: Edit link for certificate " + cert1Id + " is not found");
			return false ;
		}
		cert1Link.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Did we get to the right place?
		expectedUrl = homePage + "/members/my-documents/certificates/edit.php?certificates_id=" + cert1Id ;
		log.info("EDIT: Making sure we got to the right page: " + expectedUrl);
		currentUrl = drv.getCurrentUrl() ;
		if (!expectedUrl.equalsIgnoreCase(currentUrl))
		{
			log.error("EDIT: We are redirected to an unexpected URL: " + currentUrl);
			return false ;
		}

		allGood = true ;
		log.info("EDIT: Verifying the following fields:");
		log.info("EDIT: Checking location can edit checkbox - should be un-checked");
		if ( drv.findElement(By.id("certificates_location_can_edit")).isSelected() )
		{
			log.error("EDIT: Location can edit is not showing correctly.");
			allGood = false ;
		}
		log.info("EDIT: Checking Remind Other checkbox - should be checked");
		if ( !drv.findElement(By.id("certificates_remind_other_chk")).isSelected() )
		{
			log.error("EDIT: Remind other checkbox is not shown correctly.");
			allGood = false ;
		}
		log.info("EDIT: Checking Umbrella Coverage checkbox - should be checked");
		if ( !drv.findElement(By.id("coverage_umbrella")).isSelected() )
		{
			log.error("EDIT: Umbrella Coverage checkbox is not shown correctly.");
			allGood = false ;
		}
		log.info("EDIT: Checking Workmans Comp checkbox - should be checked");
		if ( !drv.findElement(By.id("coverage_comp")).isSelected() )
		{
			log.error("EDIT: Workmans Comp checkbox is not shown correctly.");
			allGood = false ;
		}
		log.info("EDIT: Checking Remind Other text box");
		String otherEmail = drv.findElement(By.id("certificates_remind_other")).getAttribute("value") ;
		if ( !("ooparina@curiousdog.com".equalsIgnoreCase(otherEmail)) ) 
		{
			log.error("EDIT: Text in 'Other Email' text box is not shown correctly.");
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("EDIT: Edited fields were not displayed correctly. Failing test.");
			return false ;
		}
		log.info("EDIT: Edit test is completed successfully");
		return true ;
	}
	
	public boolean updateCertificate(WebDriver drv)
	{
		boolean result = true ;
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		
		String currentUrl = drv.getCurrentUrl() ;
		log.info("UPDATE: We got redirected to: " + currentUrl);
		if ( currentUrl.indexOf("members/my-documents/certificates/index.php") <= 0 )
		{
			log.error("UPDATE: We did not land on the expected page. Will get to the right place");
			drv.findElement(By.partialLinkText("My WAKEUP")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			drv.findElement(By.linkText("Certificate Tracking")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			currentUrl = drv.getCurrentUrl() ;
			if ( currentUrl.indexOf("members/my-documents/certificates/index.php") <= 0 )
			{
				log.error("UPDATE: Still not getting to the correct url. Failing test.");
				return false ;
			}
		}
		
		List<Certificate> certs = TestCache.getInstance().getMyCertificates() ;
		if ( certs == null || certs.size() == 0 )
		{
			log.error("UPDATE: No certificates were created for testing. Quitting test.");
			return false ;
		}
		
		Certificate cert1 = certs.get(0) ;
		String cert1Id = cert1.getId() ;
		//http://staging.wakeupcall.net/members/my-documents/certificates/edit.php?certificates_id=501&action=update
		String cert1Link = homePage + "/members/my-documents/certificates/edit.php?certificates_id=" + cert1Id + "&action=update" ;
		WebElement update1 = TestUtils.getLinkElementById(drv, "Update", "certificates_id", cert1Id, "&action=update") ;
				//drv.findElement(By.linkText(cert1Link)) ;
		if ( update1 == null )
		{
			log.error("UPDATE: Edit link for certificate " + cert1Id + " is not found");
			return false ;
		}
		update1.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("UPDATE: Change type of the certificate to 'Special Projects'");
		Select cert_type = new Select(drv.findElement(By.id("certificates_type"))) ;
		TestUtils.selectItem(drv.findElement(By.id("certificates_type")), "Special Projects");
		log.info("UPDATE: This should make project name text box visible");
		WebElement projectName = drv.findElement(By.id("certificates_project_name")) ;
		if ( projectName == null )
		{
			log.error("UPDATE: Project name text box is null - this is error, failing test");
			return false ;
		}
		if ( !projectName.isDisplayed() || !projectName.isEnabled() )
		{
			log.error("UPDATE: Project name text box is not visible or not enabled. Failing test");
			return false ;
		}
		projectName.sendKeys("Ticks and fleas infestation");
		log.info("UPDATE: Updating certificate file to certificate4.txt file.");
		drv.findElement(By.id("certificates_file")).sendKeys("C:\\WakeUpCallScripts\\CERTIFICATE4.txt");
		log.info("UPDATE: Submitting form") ;
		//don't feel like messing with date picker
		drv.findElement(By.id("save")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		return true ;
	}
	
	public boolean deleteCertificate(WebDriver drv)
	{
		log.info("DELETE: Testing deleting a certificate");
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;

		List<Certificate> certs = TestCache.getInstance().getMyCertificates() ;
		if ( certs.size() <= 0 )
		{
			log.error("DELETE: There is no certificates to delete");
			return false ;
		}
		String currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("members/my-documents/certificates/index.php") <= 0 )
		{
			log.info("DELETE: We are not at the right place - going to the list of certificates");
			drv.findElement(By.partialLinkText("My WAKEUP")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			drv.findElement(By.linkText("Certificate Tracking")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			currentUrl = drv.getCurrentUrl() ;
			if ( currentUrl.indexOf("members/my-documents/certificates/index.php") <= 0 )
			{
				log.error("DELETE: After trying to get to the correct place we are still not where we want to be " + currentUrl);
				log.error("DELETE: Test failed");
				return false ;
			}
		}
		
		log.info("DELETE: Get element for deleting the last added certificate");
		Certificate doomedCert = certs.get(certs.size()-1) ;
		String doomedId = doomedCert.getId() ;
		WebElement deleteLink = TestUtils.getLinkElementById(drv, "Delete", "certificates_id", doomedId) ;
		String deleteUrl = deleteLink.getAttribute("href") ;
		log.info("DELETE: Delete link: " + deleteUrl) ;
		log.info("DELETE: Click on the Delete link");
		deleteLink.click();
		drv.switchTo().activeElement() ;
		WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
		buttonOK.click();			
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.switchTo().parentFrame() ;
		
		log.info("DELETE: Trying to find the delete link url on the page");
		if ( TestUtils.findText(drv, deleteUrl) )
		{
			log.error("DELETE: The deleted certificate's 'Delete' link is still shown on the page");
			return false ;
		}
		TestCache.getInstance().getMyCertificates().remove( certs.size()-1 ) ;
		log.info("DELETE: Delete test completed successfully");
		return true ;
	}
	
	public boolean hideUnhide(WebDriver drv)
	{
		log.info("HIDE-UNHIDE: Testing hiding/unhiding certificates");
		List<Certificate> certs = TestCache.getInstance().getMyCertificates() ;
		if ( certs.size() <= 0 )
		{
			log.error("HIDE-UNHIDE: There are no certificates to test with");
			return false ;
		}
		log.info("HIDE-UNHIDE: Hide all newly added certificates");
		for ( int i = 0 ; i < certs.size() ; i++ )
		{
			Certificate c1 = certs.get(i) ;
			String certId = c1.getId() ;
			log.info("HIDE-UNHIDE: Hiding certificate # " + certId);
			WebElement hideLink = TestUtils.getLinkElementById(drv, "Hide", "certificates_id", certId) ;
			hideLink.click(); 
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			log.info("HIDE-UNHIDE: Search for the certificate id, it should be gone from the page");
			if ( TestUtils.findText(drv, "certificates_id=" + certId) ) 
			{
				log.error("HIDE-UNHIDE: Found link to the certificate that is supposed to be hidden. Test failed");
				return false ;
			}
			log.info("HIDE-UNHIDE: All new certificates are hidden now");
		}

		log.info("HIDE-UNHIDE: Going to the hidden certificates page");
		drv.findElement(By.linkText("View Hidden certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("HIDE-UNHIDE: Un-Hide all newly added certificates");
		for ( int i = 0 ; i < certs.size() ; i++ )
		{
			Certificate c1 = certs.get(i) ;
			String certId = c1.getId() ;
			log.info("HIDE-UNHIDE: Un-Hiding certificate # " + certId);
			WebElement unhideLink = TestUtils.getLinkElementById(drv, "Un-Hide", "certificates_id", certId) ;
			unhideLink.click(); 
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			log.info("HIDE-UNHIDE: Search for the certificate id, it should be gone from the page");
			if ( TestUtils.findText(drv, "certificates_id=" + certId) ) 
			{
				log.error("HIDE-UNHIDE: Found link to the certificate that is supposed to be hidden. Test failed");
				return false ;
			}
			log.info("HIDE-UNHIDE: All new certificates are hidden now");
		}

		log.info("HIDE-UNHIDE: Going back to the visible certificates page");
		drv.findElement(By.linkText("View Visible certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		return true ;
	}
}
