package com.curiousdog.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.utils.TestUtils;

public class CorporateBulletinForm 
{
	static Logger log = Logger.getLogger(CorporateBulletinForm.class);

	public boolean CorporateBulletinTest(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homeUrl = bundle.getString("HOME.PAGE") ;
		
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		
		//Navigate to the Locations tab of My Account section
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("CORP BULLETIN: Navigating to 'Corporate Bulletin' tab of MyWUC...");
		drv.findElement(By.partialLinkText("Signature")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		//Verify that we came to the correct place:
		String currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/forum-signature.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("CORP BULLETIN: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}
		
		log.info("CORP BULLETIN: Adding initial value 'Jeffrey Merritt'") ;
		drv.findElement(By.id("members_forum_signature")).clear();
		drv.findElement(By.id("members_forum_signature")).sendKeys("Jeffrey Merritt") ;
		drv.findElement(By.id("members_forum_signature")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Your forum signature has been saved.") )
		{
			log.error("CORP BULLETIN: Status message 'Your forum signature has been saved' did NOT appear on the screen");
			return false ;
		}
		 
		log.info("CORP BULLETIN: Goging to Corporate Bulletin to verify the signature");
		drv.findElement(By.partialLinkText("Corporate Bulletin")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		currentUrl = drv.getCurrentUrl() ;
		if (currentUrl.indexOf("/members/forums/view-forum.php") < homeUrl.length()) 
		{
			log.error("CORP BULLETIN: Going to Corporate Bulletin we got redirected to unexpected page: " + currentUrl);
			return false ;
		}
		
		drv.findElement(By.linkText("New Discussion")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = sdf.format(new Date()) ;
		String testSubject = "Test discussion " + timeStamp ;
		log.info("CORP BULLETIN: Adding a new topic '" + testSubject + "' and check if the signature appears") ;
		drv.findElement(By.id("topics_title")).sendKeys(testSubject) ;
		
		//drv.findElement(By.id("topics_title")); //sendKeys("This is a very important test suggestion");
		
		drv.switchTo().frame("posts_body_ifr") ;
		drv.findElement(By.id("tinymce")).sendKeys("This is a very important test suggestion");
		drv.switchTo().parentFrame();
		//TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		String gotText = drv.findElement(By.id("topics_title")).getAttribute("value") ;
		log.info("CORP BULLETIN: If you see text: '" + gotText + "', we are back on the parent frame!" );
		drv.findElement(By.id("topics_title")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("CORP BULLETIN: It takes a long time to create a new topic");
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//WebElement addSignature = 
		//TestUtils.findDynamicElement(drv, By.id("enable_signature"), 100) ;
		
		boolean allGood = true ;
		log.info("CORP BULLETIN: Checking if we are on the correct screen");
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/forums/view-topic.php") < homeUrl.length() )
		{
			log.error("CORP BULLETIN: After adding a new topic, we got redirected to unexpected URL= " + currentUrl) ;
			allGood = false ;
		}
		log.info("CORP BULLETIN: Checking if Bulletin Message appeared on the screen");
		if ( !TestUtils.findText(drv, testSubject) )
		{
			log.error("CORP BULLETIN: Added discussion name did NOT show up on the screen");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "This is a very important test suggestion") )
		{
			log.error("CORP BULLETIN: Added message did NOT show up on the screen") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("CORP BULLETIN: BULLETIN ERROR FOUND. Test failed") ;
		}
		log.info("CORP BULLETIN: First message added to the forum successfully.");
		drv.switchTo().frame("posts_body_ifr") ;
		drv.findElement(By.id("tinymce")).sendKeys("This message is even more important");
		drv.switchTo().defaultContent(); ;
		WebElement addSignature = drv.findElement(By.id("enable_signature")) ;
		if ( !addSignature.isSelected() )
		{
			addSignature.click() ;
		}
		addSignature.submit(); 
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("CORP BULLETIN: It takes a long time to create a new topic");
		TestUtils.findDynamicElement(drv, By.id("enable_signature"), 100) ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "This message is even more important") )
		{
			log.error("CORP BULLETIN: Message where signature was supposed to be added did NOT appear on the screen");
			//return false ;
		}
		if ( !TestUtils.findText(drv, "Jeffrey Merritt") )
		{
			log.error("CORP BULLETIN: The Signature did NOT appear on the screen");
			//return false ;
		}
		//Now changing signature and making sure that the new signature shows up instead of the old one
		log.info("CORP BULLETIN: Changing Forum Signature");
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		
		//Navigate to the Locations tab of My Account section
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("CORP BULLETIN: Navigating to 'Forum Signature' tab of MyWUC...");
		drv.findElement(By.partialLinkText("Signature")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		log.info("CORP BULLETIN: Changing signature to 'Jeffrey Douglas Merritt'") ;
		drv.findElement(By.id("members_forum_signature")).clear();
		drv.findElement(By.id("members_forum_signature")).sendKeys("Jeffrey Douglas Merritt") ;
		drv.findElement(By.id("members_forum_signature")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("CORP BULLETIN: Changing Forum Signature");
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		
		//Navigate to the Locations tab of My Account section
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("CORP BULLETIN: Going back to the forum to see if signature is updated") ;
		drv.findElement(By.partialLinkText("Corporate Bulletin")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("CORP BULLETIN: Find the test discussion + '" + testSubject + "'...") ;
		drv.findElement(By.linkText(testSubject)).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Jeffrey Douglas Merritt") )
		{
			log.error("CORP BULLETIN: Signature was NOT updated in the forum. Signature test failed");
			return false ;
		}
		
		return true ;

	}
}
