package com.curiousdog.test;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.utils.TestUtils;

public class LoginForm {

	static Logger log = Logger.getLogger(LoginForm.class);
	
	public LoginForm() {
		// TODO Auto-generated constructor stub
	}

	public boolean login(WebDriver drv, String userId, String password, boolean rememberMe)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		log.info("Login home page: " + homePage);
		drv.get(homePage + "/index.php") ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		log.info("Setting user id = '" + userId + "'");
		drv.findElement(By.id("login_email")).clear() ;
		drv.findElement(By.id("login_email")).sendKeys(userId);
		log.info("Setting password = '" + password + "'");
		drv.findElement(By.id("login_password")).clear() ;
		drv.findElement(By.id("login_password")).sendKeys(password);
		if ( rememberMe ) 
		{
			log.info("Check Remember Me checkbox");
			drv.findElement(By.id("remember_me")).click();
		}
		//Submit login form
		log.info("Submitting credentials to log in");
		drv.findElement(By.id("remember_me")).submit() ;
		
		try
		{
			TestUtils.waitForUrl(drv, "/members/news/rss-news.php", 10);
		}
		catch ( TimeoutException tox)
		{
			log.info("Wait for login failed. Either log in failed or welcome video is displayed");
		}
		catch ( Exception ex )
		{
			String message = ex.getMessage() ;
			log.error("Unexpected exception thrown while waiting for successful login: " + message);
			return false ;
		}
		//check if the url has changed
		String currentUrl = drv.getCurrentUrl() ;
		log.info("After login redirected to page: " + currentUrl);
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("members/news/rss-news.php") > 0 )
		{
			log.info("Login successful");
			return true ;
		}
		else
		{
			if ( homePage.equalsIgnoreCase(currentUrl) || (homePage + "/index.php").equalsIgnoreCase(currentUrl))
			{
				log.info("Login failed - We did not moved off the login screen.");
				if ( TestUtils.findText(drv, "Incorrect Login") )
				{
					log.info("Error message for login " + userId + " has been displayed") ;
					return false ;
				}
				else
				{
					log.info("Error message for login " + userId + " has NOT been displayed");
					return false ;
				}
			}
			log.info("We left login screen. Assuming we got redirected to the 'welcome' video");
			WebElement waitingfor = null ;
			try
			{
				waitingfor = TestUtils.findDynamicElement(drv, By.id("members_show_startup_video"), 10) ;
				if ( waitingfor.isDisplayed() )
				{
					log.info("Login successful.");
					WebElement members_show_startup_video = drv.findElement(By.id("members_show_startup_video") ) ;
					log.info("Unchecked Show Welcome video checkbox");
					members_show_startup_video.click();
					members_show_startup_video.submit();
				}
			}
			catch(Exception ex)
			{
				log.info("Welcome video screen was not displayed. Continue test.") ;
			}
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		}
		log.info("Login successfully completed. Welcome video was displayed");
		return true ;
	}
	
	public boolean testLogin(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String userId   = bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;

		//Sending incorrect login:
		log.info("Logging in with incorrect values");
		if ( !login(drv, "ABRAKADABRA", "ABRAKADABRA", false) )
		{
			log.info("Login with incorrect values was prevented");
		}
		else
		{
			log.error("Login with incorrect values was NOT prevented. Test failed");
			return false ;
		}
		//Sending correct login values:
		if ( !login(drv, userId, password, true) )
		{
			log.error("Logging in with correct user name and password failed. Test failed.");
			return false ;
		}
		return true ;
	}
}
