package com.curiousdog.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.curiousdog.test.bean.Location;
import com.curiousdog.test.bean.User;
import com.curiousdog.test.utils.TestCache;
import com.curiousdog.test.utils.TestUtils;

public class MyAccountTest {

	TestCache cache; 
	static Logger log = Logger.getLogger(MyAccountTest.class);

	public MyAccountTest() 
	{
		// TODO Auto-generated constructor stub
		//cache.getInstance() ;
	}
	
	public boolean TestMyAccountAddLocation(WebDriver drv)
	{
		log.info("LOCATIONS: Start the locations test...");
		
		//Load config info
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homeUrl = bundle.getString("HOME.PAGE") ;
		
		//Go to MyWakeUp call section
		log.info("LOCATIONS: Go to My WAKEUP Call section") ;
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Navigate to the Locations tab of My Account section
		log.info("LOCATIONS: Go to My Account section") ;
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("LOCATIONS: Go to 'Locations' tab of MyWUC...");
		drv.findElement(By.linkText("Locations")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		//Verify that we came to the correct place:
		log.info("LOCATIONS: Verify the current URL is where we expected to be");
		String currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/hotels/list.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("LOCATIONS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}
		
		//Get list of currently existing locations:
		log.info("LOCATIONS: Get a list of the existing locations by counting links to 'Delete'");
		List<WebElement> existingLocations = drv.findElements(By.linkText("Delete")) ;
		List<String> existingLocationIds = TestUtils.getIdStrings(drv, existingLocations, "licensed_locations_id") ;
		//TestCache.getInstance().setStringCache1(existingLocationIds);
		
		//Add Location
		log.info("LOCATIONS: Clicking on 'Add Location' link...") ;
		drv.findElement(By.linkText("Add Location")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
	
		//Check the URL of the page we got to...
		log.info("LOCATIONS: Check current URL to make sure we got to the correct place");
		if ( !(homeUrl + "/members/account/hotels/edit.php").equalsIgnoreCase(drv.getCurrentUrl()))
		{
			log.error("LOCATIONS: We are at a wrong page: " + drv.getCurrentUrl());
			return false ;
		}

		//Click return to locations list
		log.info("LOCATIONS: Click on 'Return to Locations List' link");
		drv.findElement(By.linkText("Return to Locations List")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//We are expecting to return to locations list screen
		log.info("LOCATIONS: Expected to return to the Locations List screen");
		currentUrl = drv.getCurrentUrl() ;
		log.info("LOCATIONS: Verify the current URL is where we expected to be");
		if ( !(homeUrl + "/members/account/hotels/list.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("LOCATIONS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}

		//--------------------------------------------------------------------------------------------------------
		//Go back to the Add New Location screen
		log.info("LOCATIONS: Clicking on 'Add Location' link...") ;
		drv.findElement(By.linkText("Add Location")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		//Try submitting an empty form
		log.info("LOCATIONS: Submitting an empty form to verify error messages");
		WebElement locationNameElement = drv.findElement(By.id("licensed_locations_name")) ;
		locationNameElement.submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Checking if all required labels showed up on the screen
		boolean allGood = true ;
		if ( !TestUtils.findText(drv, "There are errors below") )
		{
			log.error("LOCATIONS: Error message 'There are errors below' was not displayed");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Please select a location type") )
		{
			log.error("LOCATIONS: Error message 'Please select a location type' was not displayed");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Location Name") )
		{
			log.error("LOCATIONS: Error message 'You must enter a value for Location Name' was not displayed");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Does Not Apply") )
		{
			log.error("LOCATIONS: Label 'Does Not Apply' was not displayed");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Please enter something") )
		{
			log.error("LOCATIONS: Error message 'Please enter something' was not displayed");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Phone") )
		{
			log.error("LOCATIONS: Error message 'You must enter a value for Phone' was not displayed");
			allGood = false ;
		}
		//Not terminating test because of missing error messages
		//if ( !allGood ) return false ;
		
		//----------------------------------------------------------------------------------------------------------
		log.info("LOCATIONS: Select 'Hotel - Full Service/Resort' type of location");
		Select dropdown_cardType = new Select(drv.findElement(By.id("licensed_locations_full_type")));
		dropdown_cardType.selectByVisibleText("Hotel - Full Service/Resort");

		WebElement numOfRooms = drv.findElement(By.id("hotels_num_rooms")) ;
		if ( !numOfRooms.isDisplayed() || !numOfRooms.isEnabled() )
		{
			log.error("LOCATIONS: Number of rooms should be enabled when Hotel Full Service type is selected");
			return false ;
		}

		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = dtf.format(new Date()) ;

		//Create first new location:
		Location firstAddedLocation = new Location() ;
		firstAddedLocation.setName("Location A " + timeStamp) ;
		firstAddedLocation.setAddress("1 Location A St.");
		firstAddedLocation.setCity("Coraopolis");
		firstAddedLocation.setState("Pennsylvania");
		firstAddedLocation.setZip("15026");
		firstAddedLocation.setPhone("4123948888");
		firstAddedLocation.setNoOfRooms("50");
		
		//ADD FIRST LOCATION
		log.info("LOCATIONS: Adding first location: " + firstAddedLocation.getName());
		drv.findElement(By.id("licensed_locations_name")).sendKeys(firstAddedLocation.getName());
		numOfRooms.sendKeys(firstAddedLocation.getNoOfRooms()) ;
		drv.findElement(By.id("licensed_locations_address")).sendKeys(firstAddedLocation.getAddress());
		drv.findElement(By.id("licensed_locations_city")).sendKeys(firstAddedLocation.getCity());
		Select licensed_locations_state = new Select(drv.findElement(By.id("licensed_locations_state")));
		licensed_locations_state.selectByVisibleText(firstAddedLocation.getState());
		drv.findElement(By.id("licensed_locations_zip")).sendKeys(firstAddedLocation.getZip());
		drv.findElement(By.id("licensed_locations_phone")).sendKeys(firstAddedLocation.getPhone());
		log.info("LOCATIONS: Submitting form with the first location " + firstAddedLocation.getName());
		numOfRooms.submit(); 
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Verify that the new location showed up in the list of locations
		//Make sure we are back to the locations list page:
		currentUrl = drv.getCurrentUrl() ;
		log.info("LOCATIONS: We got redirected to " + currentUrl + ". Verifying if that is correct");
		if ( (homeUrl + "/members/account/hotels/list.php?notice=Hotel+Added").equalsIgnoreCase(drv.getCurrentUrl()) )
		{
			log.info("LOCATIONS: Making sure the name of the new location is displayed on the screen");
			if ( !TestUtils.findText(drv, firstAddedLocation.getName()) )
			{
				log.error("LOCATIONS: New location did not show up in the list of locations") ;
				return false ;
			}
			else
			{
				log.info("LOCATIONS: Name of new added location is found on the page");
			}
		}
		else
		{
			log.error("LOCATIONS: We got redirected to unexpected location: " + drv.getCurrentUrl()) ;
			return false ;
		}
		
		log.info("LOCATIONS: Get list of all locations, including the newly added one");
		//Find all existing locations including the new location 
		List<WebElement> updatedLocations = drv.findElements(By.linkText("Delete")) ;
		//Find the id of the new location by comparing lists of ids
		List<String> updatedLocationIds = TestUtils.getIdStrings(drv, updatedLocations, "licensed_locations_id") ;
		//Compare lists of old locations and new locations to find the difference		
		List<String> newIDs = TestUtils.getAddedIds(existingLocationIds, updatedLocationIds) ;
		if ( newIDs.size() > 1 ) 
		{
			log.error("LOCATIONS: " + TestUtils.showStringList("More than 1 location was added: ", newIDs)) ;
			return false ;
		}
		log.info("LOCATIONS: Location id is " + newIDs.get(0));
		log.info("LOCATIONS: Assign the location id to the newly created location and save it to cache");
		firstAddedLocation.setId(newIDs.get(0));
		TestCache.getInstance().addLocation(firstAddedLocation) ;
		System.out.println("NEW LOCATION ADDED " + newIDs.get(0));
		
		//Get a list of current locations for comparison, including the newly added location A
		log.info("LOCATIONS: Re-read the list of existing locations by grabbing all 'Delete' links");
		existingLocations = drv.findElements(By.linkText("Delete")) ;
		existingLocationIds = TestUtils.getIdStrings(drv, existingLocations, "licensed_locations_id") ;
		
		//---------------------------------------------------------------------------------------------------
		//Add Location #2
		log.info("LOCATIONS: Clicking on 'Add Location' link...") ;
		drv.findElement(By.linkText("Add Location")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
		//Generate a new timestamp for another test file:
		timeStamp = dtf.format(new Date()) ;

		//Create a second new location for testing
		Location secondAddedLocation = new Location() ;
		secondAddedLocation.setName("Location B " + timeStamp) ;
		secondAddedLocation.setAddress("2 Location B Rd");
		secondAddedLocation.setCity("Pittsburgh");
		secondAddedLocation.setState("Pennsylvania");
		secondAddedLocation.setZip("15217");
		secondAddedLocation.setPhone("4124216127");
		//secondAddedLocation.setNoOfRooms("25");
		
		//Fill out the form again
		log.info("LOCATIONS: Adding second location: " + secondAddedLocation.getName());
		dropdown_cardType = new Select(drv.findElement(By.id("licensed_locations_full_type")));
		dropdown_cardType.selectByVisibleText("Other");
		TestUtils.Pause(50) ;
		
		drv.findElement(By.id("licensed_locations_name")).sendKeys(secondAddedLocation.getName());
		//numOfRooms.sendKeys(secondAddedLocation.getNoOfRooms()) ;
		drv.findElement(By.id("licensed_locations_address")).sendKeys(secondAddedLocation.getAddress());
		drv.findElement(By.id("licensed_locations_city")).sendKeys(secondAddedLocation.getCity());
		licensed_locations_state = new Select(drv.findElement(By.id("licensed_locations_state")));
		licensed_locations_state.selectByVisibleText(secondAddedLocation.getState());
		drv.findElement(By.id("licensed_locations_zip")).sendKeys(secondAddedLocation.getZip());
		drv.findElement(By.id("licensed_locations_phone")).sendKeys(secondAddedLocation.getPhone());
		drv.findElement(By.id("licensed_locations_phone")).submit(); 
		log.info("LOCATIONS: Submitting form for the second location " + secondAddedLocation.getName());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Verify that location name #2 showed up on the screen
		if ( !TestUtils.findText(drv, secondAddedLocation.getName()) )
		{
			log.error("LOCATIONS: New location did not show up in the list of locations") ;
			return false ;
		}
		log.info("LOCATIONS: Second location " + secondAddedLocation.getName() + " added successfully");
		//get list of locations that includes newly added location 
		updatedLocations = drv.findElements(By.linkText("Delete")) ;
		updatedLocationIds = TestUtils.getIdStrings(drv, updatedLocations, "licensed_locations_id") ;
				
		//cache.getInstance().setStringCache1(existingLocationIds);
		//get the new locations's id
		newIDs = TestUtils.getAddedIds(existingLocationIds, updatedLocationIds) ;
		if ( newIDs.size() > 1 ) 
		{
			log.error("LOCATIONS: " + TestUtils.showStringList("More than 1 location was added: ", newIDs)) ;
			return false ;
		}
		log.info("LOCATIONS: Second location id is " + newIDs.get(0));
		secondAddedLocation.setId(newIDs.get(0));
		//add second location to the cache
		TestCache.getInstance().addLocation(secondAddedLocation) ;
		System.out.println("NEW LOCATION ADDED " + newIDs.get(0));
		log.info("LOCATIONS: Two new locations were successfully added to the account. Locations test is completed.");
		return true ;
	}
	
	//test adding users and their permissions
	public boolean TestMyAccountUsersIndividual(WebDriver drv)
	{
		log.info("USERS: Start Users Test ...");
		
		//Going to the users section of my account
		log.info("USERS: Click on My WACKEUP link");
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("USERS: Arrived at the URL: " + drv.getCurrentUrl());
		
		//Get settings from properties file
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homeUrl = bundle.getString("HOME.PAGE") ;
		String mainUser= bundle.getString("LOGIN") ;
		String password= bundle.getString("PASSWORD") ;
		
		//checking if we have a location to use with adding a new user
		log.info("USERS: Check if there are locations in cache for users test");
		List<Location>newLocations = TestCache.getInstance().getMyLocations() ;
		if ( newLocations == null || newLocations.size() == 0 )
		{
			log.error("USERS: No locations found in cache for test. Need to run locations test first");
			//Run locations test. This should populate locations cache.
			if ( !TestMyAccountAddLocation(drv) ) 
			{	
				log.error("USERS: Unable to create new locations for testing users. Locations test failed");
				return false ;
			}
		}

		log.info("USERS: Passed locations test. Starting users test.") ;

		//Adding first user - regular user with permissions:
		// a. works here
		// b. certificates
		// c. claims
		// ONLY
		log.info("USERS: Adding first user. This is a regular user with permissions only for 'works here', 'certificates' and 'claims'") ;
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Make sure we got to the right place
		///members/account/account-details.php
		String currentUrl = drv.getCurrentUrl() ;
		log.info("USERS: Arrived at the following URL: " + currentUrl);
		if ( !(homeUrl + "/members/account/account-details.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}
		
		log.info("USERS: Clicking on Users tab of My Account section");
		drv.findElement(By.linkText("Users")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		//Make sure we got to the right place
		///members/account/hotels/members/list.php
		currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/hotels/members/list.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}

		log.info("USERS: Get a list of existing users for extracting a new user id later");
		List<WebElement> existingUsers = drv.findElements(By.linkText("Delete")) ;
		List<String> existingUsersIds = TestUtils.getIdStrings(drv, existingUsers, "members_id") ;
		
		log.info("USERS: Click on 'New User' link");
		drv.findElement(By.partialLinkText("New User")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Checking if we are in the right place:
		//members/account/hotels/members/edit.php
		currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/hotels/members/edit.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}

		//Go back to the list by clicking on 'Return to Users List' link
		log.info("USERS: Go back by clicking 'Return to Users List' link");
		drv.findElement(By.linkText("Return to Users List")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		log.info("USERS: Verify that we got back to the list of users");
		currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/hotels/members/list.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}

		//Back to Adding New User screen
		//-----------------------------------------------------------------------------------------------
		log.info("USERS: Click on 'New User' link");
		drv.findElement(By.partialLinkText("New User")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Submit an empty form and make sure we get correct error messages
		log.info("USERS: Submitting an empty form to see if all error messages are displayed");
		drv.findElement(By.id("members_firstname")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		boolean allGood = true ;
		if ( !TestUtils.findText(drv, "There are errors below") )
		{
			log.error("USERS. Empty form submission. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for First Name") )
		{
			log.error("USERS. Empty form submission. Message 'You must enter a value for First Name' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Last Name") )
		{
			log.error("USERS. Empty form submission. Message 'You must enter a value for Last Name' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Position/Title") )
		{
			log.error("USERS. Empty form submission. Message 'You must enter a value for Position/Title' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email") )
		{
			log.error("USERS. Empty form submission. Message 'You must enter a properly formatted email' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Repeat Email") )
		{
			log.error("USERS. Empty form submission. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Password") )
		{
			log.error("USERS. Empty form submission. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Repeat Password") )
		{
			log.error("USERS. Empty form submission. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("USERS. Empty form submission. Not all error messages were displayed");
			//return false ;
		}
		//------------------------------------------------------------------------------------------------------------
		//                                         ADD FIRST NEW USER
		//------------------------------------------------------------------------------------------------------------
		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = dtf.format(new Date()) ;
		String user1Email = "first" + timeStamp + "@fox.com" ;
		log.info("USERS: Generated email/account for new user is: " + user1Email);
		
		User firstUser = new User() ;
		firstUser.setFirstName("First");
		firstUser.setLastName("User");
		firstUser.setPosition("Administrator");
		firstUser.setEmail(user1Email) ;
		firstUser.setRepeatEmail(user1Email);
		firstUser.setPassword("password");
		firstUser.setRepeatPassword("password");

		//Fill out the form's common fields
		drv.findElement(By.id("members_firstname")).sendKeys(firstUser.getFirstName());
		drv.findElement(By.id("members_lastname")).sendKeys(firstUser.getLastName());
		drv.findElement(By.id("members_title")).sendKeys(firstUser.getPosition());

		//Submitting a malformed email address:
		log.info("Submitting form with malformed emails");
		drv.findElement(By.id("members_email")).sendKeys("whoopsy");
		drv.findElement(By.id("members_email2")).sendKeys("doopsy");
		drv.findElement(By.id("members_email2")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if ( !TestUtils.findText(drv, "There are errors below") )
		{
			log.error("USERS. Submitting malformed email. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email") )
		{
			log.error("USERS. Submitting malformed email. Message 'You must enter a properly formatted email' was not shown") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("USERS. Submitting malformed email. Not all error messages were displayed");
			return false ;
		}

		//submitting not-matching emails:
		log.info("Submitting form with not-matching emails");
		drv.findElement(By.id("members_email")).clear() ;
		drv.findElement(By.id("members_email2")).clear() ;
		drv.findElement(By.id("members_email")).sendKeys(user1Email);
		drv.findElement(By.id("members_email2")).sendKeys("mymail@mail.com");
		drv.findElement(By.id("members_email2")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if ( !TestUtils.findText(drv, "There are errors below") )
		{
			log.error("USERS. Submitting not-matching emails. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Must match other Email") )
		{
			log.error("USERS. Submitting not-matching emails. Message 'Must match other Email' was not shown") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("USERS. Submitting not-matching emails. Not all error messages were displayed");
			//return false ;
		}
		
		//submitting not matching passwords
		log.info("Submitting form with not matching passwords") ;
		drv.findElement(By.id("members_email")).clear() ;
		drv.findElement(By.id("members_email2")).clear() ;
		drv.findElement(By.id("members_email")).sendKeys(user1Email);
		drv.findElement(By.id("members_email2")).sendKeys(user1Email);
		drv.findElement(By.id("members_password")).sendKeys(firstUser.getPassword());
		drv.findElement(By.id("members_password2")).sendKeys("password2");
		drv.findElement(By.id("members_password2")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//submitting not-matching passwords:
		if ( !TestUtils.findText(drv, "There are errors below") )
		{
			log.error("USERS. Submitting not-matching emails. Message 'There are errors below' was not shown") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Must match other Password") )
		{
			log.error("USERS. Submitting not-matching emails. Message 'Must match other Password' was not shown") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("USERS. Submitting not-matching emails. Not all error messages were displayed");
			//return false ;
		}
		

		log.info("Filling out the form properly");
		drv.findElement(By.id("members_password")).clear() ;
		drv.findElement(By.id("members_password2")).clear() ;
		drv.findElement(By.id("members_password")).sendKeys(firstUser.getPassword());
		drv.findElement(By.id("members_password2")).sendKeys(firstUser.getPassword());
		
		//Choose not-admin, regular user
		log.info("USERS: Choose regular user");
		drv.findElement(By.id("members_type_1")).click();

		//Give user access to employment law and hr only
		log.info("USERS: Adding access to Employment Law");
		drv.findElement(By.id("access[elaw]")).click();
		log.info("USERS: Adding HR");
		drv.findElement(By.id("access[hr]")).click();
		//drv.findElement(By.id("access[forum]")).click();
		//drv.findElement(By.id("access[shareDocuments]")).click() ;
		
		//give user permissions:
		log.info("USERS: Adding permissions for user 1: " + firstUser.getEmail());
		
		Location location1 = newLocations.get(0) ;
		if ( location1 != null )
		{ 
			log.info("USERS: Choosing the first added location : " + location1.getName()) ;
			String location1_id = location1.getId() ;
			log.info("USERS: Element ids have prefix: loc_access[" + location1_id + "]");
			log.info("USERS: Adding 'Works here' permission") ;
			drv.findElement(By.id("loc_access[" + location1_id + "][associated]")).click(); 
			log.info("USERS: Adding certificates access");
			drv.findElement(By.id("loc_access[" + location1_id + "][certificates]")).click();
			log.info("USERS: Adding claims access") ;
			drv.findElement(By.id("loc_access[" + location1_id + "][claims]")).click();
			log.info("USERS: Submitting the form to create 1 test user");
			drv.findElement(By.id("members_title")).submit() ;
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		}
		else
		{
			log.error("USERS: Unable to get 1st added location to test adding users. Failing the test");
			return false ;
		}
		// We are supposed to end up on the list of users
		log.info("USERS: Currently we got redirected to : " + drv.getCurrentUrl());
		currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/hotels/members/list.php?notice=New+user+added").equalsIgnoreCase(currentUrl) )
		{
			log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}
		log.info("USERS: Get the id for the newly added user");
		List<WebElement> updatedUsers = drv.findElements(By.linkText("Delete")) ;
		List<String> updatedUsersIds = TestUtils.getIdStrings(drv, updatedUsers, "members_id") ;
		List<String> newIDs = TestUtils.getAddedIds(existingUsersIds, updatedUsersIds) ;
		if ( newIDs.size() != 1 ) 
		{
			log.error("USERS: " + TestUtils.showStringList("Not just 1 user was added: ", newIDs)) ;
			return false ;
		}
		String firstNewUserId = newIDs.get(0) ;
		firstUser.setId(firstNewUserId);
		log.info("USERS: First new user id = " + firstUser.getId());
		TestCache.getInstance().getMyUsers().add(firstUser) ;
		
		//Assign list of new ids to the existing ids list
		existingUsersIds = updatedUsersIds ;
		
		//Now add the first user to test cache
		log.info("USERS: Add the first new user to the users cache");
		TestCache.getInstance().getMyUsers().add(firstUser) ;
		User secondUser = new User() ;
		Location location2 = null ;
		if ( newLocations.size() > 1 )
		{
			log.info("USERS: Found second new location to add another new user") ;
			location2 = newLocations.get(1) ;
			log.info("USERS: Choosing the second added location: " + location2.getName());
			String location2_id = location2.getId() ;

			timeStamp = dtf.format(new Date()) ;
			String user2Email = "second" + timeStamp + "@fox.com" ;
			log.info("USERS: Generated email/account for new user is: " + user2Email);
			
			secondUser.setFirstName("Second");
			secondUser.setLastName("User");
			secondUser.setPosition("Bookkeeper");
			secondUser.setEmail(user2Email) ;
			secondUser.setRepeatEmail(user2Email);
			secondUser.setPassword("password");
			secondUser.setRepeatPassword("password");

			log.info("USERS: Clicking on 'New User' link");
			drv.findElement(By.partialLinkText("New User")).click();
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
						
			drv.findElement(By.id("members_firstname")).sendKeys(secondUser.getFirstName());
			drv.findElement(By.id("members_lastname")).sendKeys(secondUser.getLastName());
			drv.findElement(By.id("members_title")).sendKeys(secondUser.getPosition());
			drv.findElement(By.id("members_email")).sendKeys(user2Email);
			drv.findElement(By.id("members_email2")).sendKeys(user2Email);
			drv.findElement(By.id("members_password")).sendKeys(secondUser.getPassword());
			drv.findElement(By.id("members_password2")).sendKeys(secondUser.getPassword());
			
			log.info("USERS: Choose regular user");
			drv.findElement(By.id("members_type_1")).click();
 			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
 			
 			log.info("USERS: Adding access to forum");
 			drv.findElement(By.id("access[forum]")).click();
 			log.info("USERS: Adding access to Share Documents");
 			drv.findElement(By.id("access[shareDocuments]")).click() ;
			
 			log.info("USERS: Make this user Location Admin");
 			log.info("Check " + "loc_access[" + location2_id + "][locationAdmin]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][locationAdmin]")).click();
 			log.info("USERS: Adding certificates access");
 			log.info("Check " + "loc_access[" + location2_id + "][certificates]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][certificates]")).click();
 			log.info("USERS: Adding claims access");
 			log.info("Check " + "loc_access[" + location2_id + "][claims]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][claims]")).click();
 			log.info("USERS: Adding contracts access");
 			log.info("Check " + "loc_access[" + location2_id + "][contracts]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][contracts]")).click();
 			log.info("USERS: Allow this user to manage users");
 			log.info("Check " + "loc_access[" + location2_id + "][manageUsers]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][manageUsers]")).click();
 			log.info("USERS: Allow this user to edit locations");
 			log.info("Check " + "loc_access[" + location2_id + "][editLocation]");
 			drv.findElement(By.id("loc_access[" + location2_id + "][editLocation]")).click();
 			log.info("Finished assigning permissions. Submitting form");
 			drv.findElement(By.id("members_title")).submit() ;
 			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
 			
 			currentUrl = drv.getCurrentUrl() ;
 			log.info("After submitting the form, user redirected to URL: " + currentUrl);
 			if ( !(homeUrl + "/members/account/hotels/members/list.php?notice=New+user+added").equalsIgnoreCase(currentUrl) )
 			{
 				log.error("USERS: Got redirected to an unexpected page: " + currentUrl);
 				return false ;
 			}
 			
		}
		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		updatedUsers    = drv.findElements(By.linkText("Delete")) ;
		updatedUsersIds = TestUtils.getIdStrings(drv, updatedUsers, "members_id") ;

		newIDs = TestUtils.getAddedIds(existingUsersIds, updatedUsersIds) ;
		if ( newIDs.size() != 1 ) 
		{
			log.error("USERS: " + TestUtils.showStringList("Not just 1 user was added: ", newIDs)) ;
			return false ;
		}
		String secondNewUserId = newIDs.get(0) ;
		secondUser.setId(secondNewUserId);

		TestCache.getInstance().getMyUsers().add(secondUser) ;
		
		log.info("Created new users. Now will log in as each of the users to makes sure permissions are set correctly");
		if ( !logout(drv) )
		{
			log.error("USERS. Was not able to log out cleanly. Stopping the test.") ;
			return false ;
		}
		
		log.info("USERS: Logging in as the first new user.") ;
		if ( login(drv, firstUser.getEmail(), firstUser.getPassword()) )
		{
			log.info("USER1: Logged in as the first user who has only the following permissions: ");
			log.info("USER1: For the first new location: '" + location1.getName() + "'. Location id = '" + location1.getId() + "'");
			log.info("USER1:    - Employment Law") ;
			log.info("USER1:    - HR") ;
			log.info("USER1:        + Location Admin") ;
			log.info("USER1:        + certificates") ;
			log.info("USER1:        + claims") ;
			log.info("USER1:        + contracts") ;
			log.info("USER1:        + Manage Users") ;
			log.info("USER1:        + Edit Location") ;
			
			//See if the proper permissions worked for the new user #1
			log.info("USER1: Checking the upper functions menu");
			if ( TestUtils.findText(drv, "My WAKEUP") )
			{
				log.info("USER1: My WAKEUP CALL is on the screen");
				log.info("USER1: Going into My WAKEUP CALL section to verify permissions");
				drv.findElement(By.partialLinkText("My WAKEUP")).click();
				TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
				//Works here does not really do anything
				//User should have access to Certificates and Claims only.
				if ( TestUtils.findText(drv, "My Files") )
				{
					log.info("USER1: 'My Files' section is visible");
				}
				if ( TestUtils.findText(drv, "Certificate Tracking") ) 
					log.info("USER1: 'Certificate Tracking' section is visible");
				else
				{
					log.error("USER1: 'Certificate Tracking' section is not visible. This is an error. User should have this access");
					return false ;
				}
				if ( TestUtils.findText(drv, "Claims Management") )
					log.info("USER1: 'Claims Management' section is visible");
				else
				{
					log.error("USER1: 'Claims Management' section is not visible. This is an error. User should have this access");
					return false ;
				}
				if ( TestUtils.findText(drv, "Contract Management") )
				{
					log.error("USER1: 'Contract Management' section is visible. This is an error, user must not have this access");
					return false ;
				}
				else log.info("USER1: 'Contract Management' section is not visible.") ;				
				if ( TestUtils.findText(drv, "Corporate Bulletin") )
				{
					log.error("USER1: 'Corporate Bulletin' section is visible. This is an error. User must not have this access");
					log.error("USER1: However, it shows up anyways even with no access. Skipping.");
					//return false ;
				}
				else log.info("USER1: 'Corporate Bulletin' section is not visible.") ;
				if ( TestUtils.findText(drv, "Shared Documents") )
				{
					log.error("USER1: 'Shared Documents' section is visible, probably should be visible to everybody. Skipping.");
				}
				if ( TestUtils.findText(drv, "My Account") )
				{
					log.info("USER1: 'My Account' section is visible");
					log.info("USER1: Verifying if there are proper tabs in My Account section") ;
					drv.findElement(By.linkText("My Account")).click();
					TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

					if ( ( TestUtils.findText(drv, "Account") && TestUtils.findText(drv, "Details") ) )
					{
						log.info("USER1: 'Account Details' is on the screen") ;
						log.info("USER1: Verifying data on the screen");
						String readFirstName = drv.findElement(By.id("members_firstname")).getAttribute("value") ;
						String readLastName  = drv.findElement(By.id("members_lastname")).getAttribute("value") ;
						String readPhoneNo   = drv.findElement(By.id("members_phone")).getAttribute("value") ;
						//String readLocation  = drv.findElement(By.id("forums_name")).getText() ;
						
						log.info("USER1: Account info on the screen - ");
						log.info("USER1: First Name:    " + readFirstName) ;
						if ( readFirstName != null && readFirstName.equalsIgnoreCase(firstUser.getFirstName()) )
						{
							log.info("USER1: First Name saved and displayed correctly");
						}
						else
						{
							log.error("USER1: Was not saved or displayed correctly");
							return false ;
						}
						log.info("USER1: Last Name:     " + readLastName);
						if ( readLastName != null && readLastName.equalsIgnoreCase(firstUser.getLastName()) )
						{
							log.info("USER1: Last Name saved and displayed correctly");
						}
						else
						{
							log.error("USER1: Last Name was not saved or displayed correctly");
							return false ;
						}
						log.info("USER1: Phone Number:  " + readPhoneNo);
						//log.info("Location Name: " + readLocation);
					}
					else
					{
						log.error("USER1: 'Account Details' is NOT on the screen. This is an error. Test failed.") ;
						return false ;
					}
					if ( isTextOnPagePresent(drv, "Locations") ) 	log.info("Found function found Locations");
					else                      						log.info("Found function DID NOT find Locations") ;
					
					if ( ( TestUtils.findText(drv, "Locations") ) )
					{
						log.error("USER1: 'Locations' is on the screen. This is an error. The user must not have this access") ;
						//return false ;
					}
					else
					{
						log.info("USER1: 'Locations' is NOT on the screen. This is correct.") ;
					}
					if ( ( TestUtils.findText(drv, "Users") ) )
					{
						log.error("USER1: 'Users' is on the screen. This is an error. The user must not have this access") ;
						//return false ;
					}
					else
					{
						log.info("USER1: 'Users' is on NOT the screen. This is correct") ;
					}
					if ( ( TestUtils.findText(drv, "Billing") ) )
					{
						log.error("USER1: 'Billing' is on the screen. This is an error. The user must not have this access") ;
						//return false ;
					}
					else
					{
						log.info("USER1: 'Billing' is on NOT the screen. This is correct.") ;
					}
					if ( ( TestUtils.findText(drv, "Notifications") ) )
						log.info("USER1: 'Notifications' is on the screen") ;
					else
					{
						log.error("USER1: 'Notifications' is NOT on the screen. This tab must always be visible") ;
						//return false ;
					}
					if ( ( TestUtils.findText(drv, "Forum") && TestUtils.findText(drv, "Signature") ) )
						log.info("USER1: 'Forum Signature' is on the screen") ;
					else
					{
						log.error("USER1: 'Forum Signature' is NOT on the screen. This is an error. Every user must be able to see it") ;
						//return false ;
					}
				}
			}
			if ( TestUtils.findText(drv, "HR") )
			{
				log.info("USER1: HR is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Employment") && TestUtils.findText(drv, "Law") ) )
			{
				log.info("USER1: Employment Law is on the screen") ;
			}
			if ( TestUtils.findText(drv, "Webinars/") && TestUtils.findText(drv, "Training" ) )
			{
				log.info("USER1: Webinars/Training is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Message") &&  TestUtils.findText(drv, "Forum") ) )
			{
				log.info("USER1: Message Forum is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Document") &&  TestUtils.findText(drv, "Library") ) )
			{
				log.info("USER1: Document Library is on the screen");
			}
			if ( ( TestUtils.findText(drv, "SDS") &&  TestUtils.findText(drv, "Library") ) )
			{
				log.info("USER1: SDS Library is on the screen");
			}
			if ( TestUtils.findText(drv, "News") )
			{
				log.info("USER1: News is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Additional") &&  TestUtils.findText(drv, "Services") ) )
			{
				log.info("USER1: Additional Services is on the screen");
			}
			if ( TestUtils.findText(drv, "WAKEUP CALL FAQs") )
			{
				log.info("USER1: WAKEUP CALL FAQs is on the screen");
			}
			//Checking if user has files sharing rights:
			drv.findElement(By.linkText("My Files")).click();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			if ( TestUtils.findText(drv, "Shared Document?"))
			{
				log.error("USER1. Sharing documents functionality is not disabled. This is an error.") ;
				//return false ;
			}
			else log.info("USER1: Sharing documents functionality is disabled. This is correct.");
			
			log.info("USER1: Logging out of the first new user.");
			if ( !logout(drv) )
			{
				log.error("USER1. Was not able to log out cleanly. Stopping the test.") ;
				return false ;
			}
		}
		else
		{
			log.error("USER1: Unable to log in as first newly created user: " + firstUser.getEmail() + ". Test failed.");
			return false ;
		}
		//Now test the second newly created user:
		//-----------------------------------------------------------------------------------------------------------
		log.info("USERS: Logging in as the second new user.") ;
		if ( login(drv, secondUser.getEmail(), secondUser.getPassword()) )
		{
			log.info("USER2: Logged in as the second user who has only the following permissions: ");
			log.info("USER2: For the second new location: '" + location2.getName() + "'. Location id = '" + location2.getId() + "'");
			log.info("USER2:    - Employment Law") ;
			log.info("USER2:    - HR") ;
			log.info("USER2:        + works here") ;
			log.info("USER2:        + certificates") ;
			log.info("USER2:        + claims") ;
			
			//See if the proper permissions worked for the new user #1
			log.info("USER2: Checking the upper functions menu");
			if ( TestUtils.findText(drv, "My WAKEUP") )
			{
				log.info("USER2: My WAKEUP CALL is on the screen");
				log.info("USER2: Going into My WAKEUP CALL section to verify permissions");
				drv.findElement(By.partialLinkText("My WAKEUP")).click();
				TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
				if ( TestUtils.findText(drv, "My Files") )
				{
					log.info("USER2: 'My Files' section is visible");
				}
				if ( TestUtils.findText(drv, "Certificate Tracking") )
				{
					log.info("USER2: 'Certificate Tracking' section is visible");
				}
				if ( TestUtils.findText(drv, "Claims Management") )
				{
					log.info("USER2: 'Claims Management' section is visible");
				}
				if ( TestUtils.findText(drv, "Contract Management") )
				{
					log.info("USER2: 'Contract Management' section is visible");
				}
				if ( TestUtils.findText(drv, "Corporate Bulletin") )
				{
					log.info("USER2: 'Corporate Bulletin' section is visible");
				}
				if ( TestUtils.findText(drv, "Shared Documents") )
				{
					log.info("USER2: 'Shared Documents' section is visible");
				}
				if ( TestUtils.findText(drv, "My Account") )
				{
					log.info("USER2: 'My Account' section is visible");
					log.info("USER2: Verifying if there are proper tabs in My Account section") ;
					drv.findElement(By.linkText("My Account")).click();
					TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

					if ( ( TestUtils.findText(drv, "Account") && TestUtils.findText(drv, "Details") ) )
					{
						log.info("USER2: 'Account Details' is on the screen") ;
						log.info("USER2: Verifying data on the screen");
						String readFirstName = drv.findElement(By.id("members_firstname")).getAttribute("value") ;
						String readLastName  = drv.findElement(By.id("members_lastname")).getAttribute("value") ;
						String readPhoneNo   = drv.findElement(By.id("members_phone")).getAttribute("value") ;
						//String readLocation  = drv.findElement(By.id("forums_name")).getText() ;
						
						log.info("USER2: Account info on the screen - ");
						log.info("USER2: First Name:    " + readFirstName) ;
						if ( readFirstName != null && readFirstName.equalsIgnoreCase(secondUser.getFirstName()) )
						{
							log.info("USER2: First Name saved and displayed correctly");
						}
						else
						{
							log.error("USER2: Was not saved or displayed correctly");
							return false ;
						}
						log.info("USER2: Last Name:     " + readLastName);
						if ( readLastName != null && readLastName.equalsIgnoreCase(secondUser.getLastName()) )
						{
							log.info("USER2: Last Name saved and displayed correctly");
						}
						else
						{
							log.error("USER2: Last Name was not saved or displayed correctly");
							return false ;
						}
						log.info("USER2: Phone Number:  " + readPhoneNo);
						//log.info("Location Name: " + readLocation);
					}
					else
					{
						log.info("USER2: 'Account Details' is NOT on the screen") ;
					}
					if ( ( TestUtils.findText(drv, "Locations") ) )
					{
						log.info("USER2: 'Locations' is on the screen") ;
					}
					else
					{
						log.info("USER2: 'Locations' is NOT on the screen") ;
					}
					if ( ( TestUtils.findText(drv, "Users") ) )
					{
						log.info("USER2: 'Users' is on the screen") ;
					}
					else
					{
						log.info("USER2: 'Users' is on NOT the screen") ;
					}
					if ( ( TestUtils.findText(drv, "Billing") ) )
					{
						log.info("USER2: 'Billing' is on the screen") ;
					}
					else
					{
						log.info("USER2: 'Billing' is on NOT the screen") ;
					}
					if ( ( TestUtils.findText(drv, "Notifications") ) )
					{
						log.info("USER2: 'Notifications' is on the screen") ;
					}
					else
					{
						log.info("USER2: 'Notifications' is NOT on the screen") ;
					}
					if ( ( TestUtils.findText(drv, "Forum") && TestUtils.findText(drv, "Signature") ) )
					{
						log.info("USER2: 'Forum Signature' is on the screen") ;
					}
					else
					{
						log.info("USER2: 'Forum Signature' is NOT on the screen") ;
					}
				}
				
			}
			if ( TestUtils.findText(drv, "HR") )
			{
				log.info("USER2: HR is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Employment") && TestUtils.findText(drv, "Law") ) )
			{
				log.info("USER2: Employment Law is on the screen") ;
			}
			if ( TestUtils.findText(drv, "Webinars/") && TestUtils.findText(drv, "Training" ) )
			{
				log.info("USER2: Webinars/Training is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Message") &&  TestUtils.findText(drv, "Forum") ) )
			{
				log.info("USER2: Message Forum is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Document") &&  TestUtils.findText(drv, "Library") ) )
			{
				log.info("USER2: Document Library is on the screen");
			}
			if ( ( TestUtils.findText(drv, "SDS") &&  TestUtils.findText(drv, "Library") ) )
			{
				log.info("USER2: SDS Library is on the screen");
			}
			if ( TestUtils.findText(drv, "News") )
			{
				log.info("USER2: News is on the screen");
			}
			if ( ( TestUtils.findText(drv, "Additional") &&  TestUtils.findText(drv, "Services") ) )
			{
				log.info("USER2: Additional Services is on the screen");
			}
			if ( TestUtils.findText(drv, "WAKEUP CALL FAQs") )
			{
				log.info("USER2: WAKEUP CALL FAQs is on the screen");
			}

			log.info("USER2: Logging out of the first new user.");
			if ( !logout(drv) )
			{
				log.error("USER2. Was not able to log out cleanly. Stopping the test.") ;
				return false ;
			}
		}
		else
		{
			log.error("USER2: Unable to log in as second newly created user: " + secondUser.getEmail() + ". Test failed.");
			return false ;
		}
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//log in as the admin user and change some of the permissions for the both new users,
		//then verify the permissions again
		log.info("USERS: Logging in as the main admin user of the account to edit new users");
		if ( !login(drv, mainUser, password) )
		{
			log.error("USERS: Unable as log in as the main admin user of the account. Test failed");
			return false ;
		}
		
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.findElement(By.linkText("My Account")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		drv.findElement(By.linkText("Users")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		return true ;
	}
	
	public boolean TestForumSignature(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homeUrl = bundle.getString("HOME.PAGE") ;
		
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		
		//Navigate to the Locations tab of My Account section
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("FORUM SIGNATURE: Navigating to 'Forum Signature' tab of MyWUC...");
		drv.findElement(By.partialLinkText("Signature")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		//Verify that we came to the correct place:
		String currentUrl = drv.getCurrentUrl() ;
		if ( !(homeUrl + "/members/account/forum-signature.php").equalsIgnoreCase(currentUrl) )
		{
			log.error("FORUM SIGNATURE: Got redirected to an unexpected page: " + currentUrl);
			return false ;
		}
		
		log.info("FORUM SIGNATURE: Adding initial value 'Jeffrey Merritt'") ;
		drv.findElement(By.id("members_forum_signature")).clear();
		drv.findElement(By.id("members_forum_signature")).sendKeys("Jeffrey Merritt") ;
		drv.findElement(By.id("members_forum_signature")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Your forum signature has been saved.") )
		{
			log.error("Status message 'Your forum signature has been saved' did NOT appear on the screen");
			return false ;
		}
		 
		log.info("FORUM SIGNATURE: Goging to Message Forum to verify the signature");
		drv.findElement(By.partialLinkText("Forum")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		currentUrl = drv.getCurrentUrl() ;
		if (currentUrl.indexOf("/members/forums/view-forum.php") < homeUrl.length()) 
		{
			log.error("FORUM SIGNATURE. Going to Message Forum we got redirected to unexpected page: " + currentUrl);
			return false ;
		}
		log.info("FORUM SIGNATURE: Creating a new discussion...") ;
		drv.findElement(By.linkText("New Discussion")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = sdf.format(new Date()) ;
		String testSubject = "Test discussion " + timeStamp ;
		log.info("FORUM SIGNATURE: Adding a new topic '" + testSubject + "' and check if the signature appears") ;
		drv.findElement(By.id("topics_title")).sendKeys(testSubject) ;
		
		//drv.findElement(By.id("topics_title")); //sendKeys("This is a very important test suggestion");
		
		drv.switchTo().frame("posts_body_ifr") ;
		drv.findElement(By.id("tinymce")).sendKeys("This is a very important test suggestion");
		drv.switchTo().parentFrame();
		//TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		//String gotText = drv.findElement(By.id("topics_title")).getAttribute("value") ;
		//log.info("If you see text: '" + gotText + "', we are back on the parent frame!" );
		drv.findElement(By.id("topics_title")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("FORUM SIGNATURE: It takes a long time to create a new topic");
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//WebElement addSignature = 
		//TestUtils.findDynamicElement(drv, By.id("enable_signature"), 100) ;
		
		boolean allGood = true ;
		log.info("Checking if we are on the correct screen");
		currentUrl = drv.getCurrentUrl() ;
		if ( currentUrl.indexOf("/members/forums/view-topic.php") < homeUrl.length() )
		{
			log.error("FORUM SIGNATURE: After adding a new topic, we got redirected to unexpected URL= " + currentUrl) ;
			allGood = false ;
		}
		log.info("FORUM SIGNATURE: Checking if Forum Message appeared on the screen");
		if ( !TestUtils.findText(drv, testSubject) )
		{
			log.error("FORUM SIGNATURE: Added discussion name did NOT show up on the screen");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "This is a very important test suggestion") )
		{
			log.error("FORUM SIGNATURE: Added message did NOT show up on the screen") ;
			allGood = false ;
		}
		if ( !allGood )
		{
			log.error("FORUM SIGNATURE: FORUM ERROR FOUND. Test failed") ;
		}
		
		drv.switchTo().frame("posts_body_ifr") ;
		drv.findElement(By.id("tinymce")).sendKeys("And this is even more important message");
		drv.switchTo().defaultContent(); ;
		WebElement addSignature = drv.findElement(By.id("enable_signature")) ;
		if ( !addSignature.isSelected() )
		{
			addSignature.click() ;
		}
		addSignature.submit(); 
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if ( !TestUtils.findText(drv, "This is the reply with signature") )
		{
			log.error("FORUM SIGNATURE: Message where signature was supposed to be added did NOT appear on the screen");
			//return false ;
		}
		if ( !TestUtils.findText(drv, "Jeffrey Merritt") )
		{
			log.error("FORUM SIGNATURE: The Signature did NOT appear on the screen");
			//return false ;
		}
		//Now changing signature and making sure that the new signature shows up instead of the old one
		log.info("FORUM SIGNATURE: Changing Forum Signature");
		drv.findElement(By.partialLinkText("My WAKEUP")).click();
		
		//Navigate to the Locations tab of My Account section
		drv.findElement(By.linkText("My Account") ).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("FORUM SIGNATURE: Navigating to 'Forum Signature' tab of MyWUC...");
		drv.findElement(By.partialLinkText("Signature")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		log.info("FORUM SIGNATURE: Changing signature to 'Jeffrey Douglas Merritt'") ;
		drv.findElement(By.id("members_forum_signature")).clear();
		drv.findElement(By.id("members_forum_signature")).sendKeys("Jeffrey Douglas Merritt") ;
		drv.findElement(By.id("members_forum_signature")).submit() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("FORUM SIGNATURE: Going back to the forum to see if signature is updated") ;
		drv.findElement(By.partialLinkText("Forum")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("FORUM SIGNATURE: Find the test discussion + '" + testSubject + "'...") ;
		drv.findElement(By.linkText(testSubject)).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( !TestUtils.findText(drv, "Jeffrey Douglas Merritt") )
		{
			log.error("FORUM SIGNATURE: Signature was NOT updated in the forum. Signature test failed");
			return false ;
		}
		
		return true ;
	}
	
	public boolean isTextOnPagePresent(WebDriver drv, String text) {
	    WebElement body = drv.findElement(By.tagName("body"));
	    String bodyText = body.getText();
	    return bodyText.contains(text);
	}
	
	
	public boolean logout(WebDriver drv)
	{
		log.info("LOGOUT. Logging out of the current account") ;
		drv.findElement(By.linkText("Logout")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		String currentUrl = drv.getCurrentUrl() ;

		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homeUrl = bundle.getString("HOME.PAGE") ;
		if (    !( homeUrl.equalsIgnoreCase(currentUrl)) &&
				!( (homeUrl+"/").equalsIgnoreCase(currentUrl)) &&
				!( (homeUrl + "/index.php").equalsIgnoreCase(currentUrl)) )
		{
			log.error("LOGOUT. We're at an unexpected URL: " + currentUrl);
			return false ;
		}
		log.info("LOGOUT. Successfully logged out of the account");
		return true ;
	}
	
	public boolean login(WebDriver drv, String email, String password)
	{
		return new LoginForm().login(drv, email, password, false);
	}
}
