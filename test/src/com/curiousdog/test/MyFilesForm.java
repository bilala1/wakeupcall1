package com.curiousdog.test;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.utils.TestUtils;

public class MyFilesForm {

	static Logger log = Logger.getLogger(MyFilesForm.class);

	public MyFilesForm() 
	{

	}
	
	public boolean TestFilesForm(WebDriver drv)
	{
		cleanUpMyFiles(drv) ;

		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String baseDir = bundle.getString("FILES.BASEDIR") ;
		
		WebElement MyWUC = TestUtils.findDynamicElement(drv, By.partialLinkText("My WAKEUP"), 10) ;
		//WebElement MyWUC = drv.findElement(By.partialLinkText("My WAKEUP")) ;
		MyWUC.click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		String currentUrl = drv.getCurrentUrl() ;
		log.info("Page redirected to: " + currentUrl);
		
		//Go to add folder form:
		log.info("Adding a new folder");
		WebElement addFolder = drv.findElement(By.linkText("Add Folder")) ;
		addFolder.click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Make sure we came to the right place:
		currentUrl = drv.getCurrentUrl() ;
		if ( !( currentUrl.indexOf("/members/my-documents/folders/edit.php") > 0 ) )
		{
			log.error("Add folder: We got redirected to a wrong URL: '" + currentUrl + "'. Test failed.") ;
			return false ;
		}
		//Cancel out of this form
		log.info("Cancelling out of Add Folder screen");
		WebElement cancel = drv.findElement(By.linkText("Return to Files List")) ;
		cancel.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		currentUrl = drv.getCurrentUrl() ;
		log.info("Redirected to page '" + currentUrl);
		if ( !(currentUrl.indexOf("members/my-documents/index.php") > 0) )
		{
			log.error("Cancel out of Add Folder screen: We got redirected to a wrong page: '" + currentUrl + "'. Test failed.") ;
			return false ;
		}
		
		//Submit empty folder name:
		log.info("Add Folder: create new folder with an empty folder name");
		addFolder = drv.findElement(By.linkText("Add Folder")) ;
		addFolder.click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		//Verify that page returned to the Files list
		currentUrl = drv.getCurrentUrl() ;
		if ( !(currentUrl.indexOf("members/my-documents/folders/edit.php") > 0) )
		{
			log.error("Add folder: We got redirected to a wrong URL: '" + currentUrl + "'. Test failed.") ;
			return false ;
		}
		
		//Clear text box
		WebElement members_library_categories_name = drv.findElement(By.id("members_library_categories_name")) ;
		members_library_categories_name.clear();
		members_library_categories_name.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Make sure error message appears on the form:
		if (!TestUtils.findText(drv, "There are errors below"))
		{
			log.error("Add Folder: Error message 'There are errors below' is absent from the form") ;
			return false ;
		}
		if (!TestUtils.findText(drv, "You must enter a value for Folder Name")) 
		{
			log.error("Add Folder: Error message 'You must enter a value for Folder Name' is absent from the form") ;
			return false ;
		}
		
		//On the Add Folder form - Create new folder : Folder #1
		log.info("Add Folder: creating folder 'Folder #1'") ;
		members_library_categories_name = drv.findElement(By.id("members_library_categories_name")) ;
		members_library_categories_name.sendKeys("Folder #1");
		members_library_categories_name.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Verify new folder is added to the files/folders list:
		if (!TestUtils.findText(drv, "Folder #1"))
		{
			log.error("Add Folder: Added folder 'Folder #1' is not found on the page");
			return false ;
		}
		
		//Go to add file form
		log.info("Going to Add File form.");
		WebElement addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Make sure we came to the right place:
		currentUrl = drv.getCurrentUrl() ;
		if ( !(currentUrl.indexOf("members/my-documents/edit.php") > 0) )
		{
			log.error("Add Folder: Add file - We got redirected to a wrong URL: '" + currentUrl + "'. Test failed.") ;
			return false ;
		}
		
		//Cancel out of add file form
		log.info("Cancelling out of Add File form");
		WebElement cancelFile = drv.findElement(By.linkText("Return to Files List")) ;
		cancelFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		currentUrl = drv.getCurrentUrl() ;
		if (!(currentUrl.indexOf("members/my-documents/index.php") > 0) )
		{
			log.error("Add File: Cancel add file - We did not return to files list: '" + currentUrl + "'. Test failed.") ;
			return false ;
		}
		
		//Add file to the Uncategorized 
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Try to submit an empty form:
		log.info("Add File: Submitting empty Add File form");
		WebElement documents_title = drv.findElement(By.id("documents_title")) ;
		documents_title.clear() ;
		documents_title.submit() ;		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if (!TestUtils.verifyTextPresent(drv, "There are errors below")
	    || !TestUtils.verifyTextPresent(drv, "Please upload a file")
	    || !TestUtils.verifyTextPresent(drv, "You must enter a value for Title"))
		{
			log.error("Add file: Not found error messages for empty form submission") ;
			return false ;
		}

		List oldDeleteLinks = drv.findElements(By.linkText("Delete") ) ;
		List<String> oldFiles = TestUtils.getIdStrings(drv, oldDeleteLinks, "documents_id") ;
		
		//Choose file and submit:
		log.info("Add file 'FileTest1.txt' without a specified folder. File should create folder 'Uncategorized'");
		WebElement documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest1.txt"); 
		documents_title = drv.findElement(By.id("documents_title")) ;
		documents_title.clear() ;
		documents_title.sendKeys("This is the first test file");
		documents_title.submit() ;		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//See what IDs were added:
		List newDeleteLinks = drv.findElements(By.linkText("Delete") ) ;
		List<String> newFiles = TestUtils.getIdStrings(drv, newDeleteLinks, "documents_id") ;
		TestUtils.getAddedIds(oldFiles, newFiles) ;

		oldDeleteLinks = drv.findElements(By.linkText("Delete") ) ;
		oldFiles = TestUtils.getIdStrings(drv, oldDeleteLinks, "documents_id") ;
		
		//Verify the file was added to the Uncategorized folder:
		if (!TestUtils.verifyTextPresent(drv, "FileTest1") 
		|| !TestUtils.verifyTextPresent(drv, "This is the first test file") 
		|| !TestUtils.verifyTextPresent(drv, "Uncategorized"))
		{
			log.error("Add Files: The added file 'FileTest1' or 'Uncategorized' folder is not found on the page");
			return false ;
		}
				
		log.info("Edit file: Move file 'FileTest1.txt' to a folder");
		//Edit file 
		WebElement editFile = drv.findElement(By.linkText("Edit")) ;
		editFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		newDeleteLinks = drv.findElements(By.linkText("Delete") ) ;
		newFiles = TestUtils.getIdStrings(drv, newDeleteLinks, "documents_id") ;
		TestUtils.getAddedIds(oldFiles, newFiles) ;
		
		
		//Place file into Folder #1
		log.info("Edit file: Reassign file FileTest1.txt to folder 'Folder #1'. Also, make this file Shared.");
		WebElement join_members_library_categories_id = drv.findElement(By.id("join_members_library_categories_id") ) ;
		TestUtils.selectItem(join_members_library_categories_id, "Folder #1") ;
		WebElement edit_documents_is_corporate_shared = drv.findElement(By.id("documents_is_corporate_shared")) ;
		edit_documents_is_corporate_shared.click();
		edit_documents_is_corporate_shared.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Verify the file was moved from the Uncategorized folder:
		if ( !TestUtils.findText(drv,"FileTest1.txt") ||
		     !TestUtils.findText(drv,"This is the first test file") ||
		     TestUtils.findText(drv,"Uncategorized"))
		{
			log.error("Edit file: The added file is not found on the page or 'Uncategorized' folder was not removed");
			return false ;
		}
		
		//Edit folder
		log.info("Edit folder: Rename folder 'Folder #1' to 'The first test folder ever!'");
		WebElement editFolder = drv.findElement(By.linkText("Edit Folder")) ;
		editFolder.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Rename folder to: The first test folder ever!
		members_library_categories_name = drv.findElement(By.id("members_library_categories_name") ) ;
		members_library_categories_name.clear(); 
		members_library_categories_name.sendKeys("The first test folder ever!");
		members_library_categories_name.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		if ( !TestUtils.findText(drv, "The first test folder ever!") )
		{
			log.error("Edit Folder: The renamed folder is not found in the files/folders list");
			return false ;
		}
		//Add another folder
		log.info("Add another folder 'Folder #2'.");
		addFolder = drv.findElement(By.linkText("Add Folder")) ;
		addFolder.click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Create folder : Folder #2
		members_library_categories_name = drv.findElement(By.id("members_library_categories_name")) ;
		members_library_categories_name.sendKeys("Folder #2");
		members_library_categories_name.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Verify folder 2 is added to the file list:
		if (!TestUtils.findText(drv, "Folder #2") ) 
		{
			log.error("Create folder: The new folder 'Folder #2' is not found in the files/folders list");
			return false ;
		}
		
		//Add another file
		log.info("Add another file 'FileTest2.txt' and place it into folder 'Folder #2'.");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Choose and submit file to 
		WebElement folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Folder #2") ;
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest2.txt"); 
		documents_title = drv.findElement(By.id("documents_title")) ;
		documents_title.clear(); 
		documents_title.sendKeys("This is the second test file");
		documents_title.submit() ;		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Verify file 2 is added to the file list:
		if (!TestUtils.findText(drv, "FileTest2.txt") ||
			!TestUtils.findText(drv, "This is the second test file"))
		{
			System.out.println("The added file File Test 2 is not found on the page");
			return false ;
		}

		newDeleteLinks = drv.findElements(By.linkText("Delete") ) ;
		newFiles = TestUtils.getIdStrings(drv, newDeleteLinks, "documents_id") ;
		TestUtils.getAddedIds(oldFiles, newFiles) ;
		
		//Add third file
		log.info("Add third test file 'FileTest3.txt' and place it into 'Folder #2'.");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Choose file and place it into Folder #2
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest3.txt"); ;
		folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Folder #2") ;
		folderSelect.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Verify file 3 is added to the file list:
		if (!TestUtils.findText(drv, "FileTest3.txt")) 
		{
			log.error("Add File: The added file 'FileTest3.txt' is NOT found in the files/folders list.");
			return false ;
		}
		
		//Add another file
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Select file and place it into Folder #2 - we will have 2 copies of the same file in the same folder
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest2.txt"); ;
		folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Folder #2") ;
		folderSelect.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Add another file
		log.info("Add the same file 'FileTest2.txt' and place it into the same folder 'Folder #2'.");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Select file and place it into Folder #2 - we will have 2 copies of the same file in the same folder
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest2.txt"); ;
		folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Folder #2") ;
		folderSelect.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		//Delete all folders
		log.info("Delete all folders test.");
		List allDeleteFolderLinks = drv.findElements(By.linkText("Delete Folder") ) ;
		for ( int i = 0 ; i < allDeleteFolderLinks.size() ; i++ )
		{
	 		WebElement deleteLink = TestUtils.getFirstElement(drv, "Delete Folder") ;
			deleteLink.click();
			drv.switchTo().activeElement() ;
			WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
			buttonOK.click();			
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.switchTo().parentFrame() ;
		}
		//Verify that all folders are deleted:
		allDeleteFolderLinks = drv.findElements(By.linkText("Delete Folder") ) ;
		if ( allDeleteFolderLinks.size() > 0 )
		{
			log.error("Delete Folders: Not all folders were successfully deleted");
			return false ;
		}
		//All existing files were placed into 'Uncategorized' system folder
		if ( !TestUtils.findText(drv, "Uncategorized") )
		{
			log.error("Folder 'Uncategorized' was not found in the files/folders list");
			return false ;
		}
		
		//Delete all files
		log.info("Delete all files");
		List allDeleteFileLinks = drv.findElements(By.linkText("Delete") ) ;
		for ( int i = 0 ; i < allDeleteFileLinks.size() ; i++ )
		{
	 		WebElement deleteLink = TestUtils.getFirstElement(drv, "Delete") ;
	 		System.out.println("Delete link: " + deleteLink.getAttribute("href") ) ;
			deleteLink.click();
			drv.switchTo().activeElement() ;
			WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
			buttonOK.click();			
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.switchTo().parentFrame() ;
		}
		allDeleteFileLinks = drv.findElements(By.linkText("Delete") ) ; ;
		if ( allDeleteFileLinks.size() > 0 )
		{
			log.error("Delete Files: Not all files were successfully deleted");
			return false ;
		}
		//'Uncategorized' system folder should be removed from the screen
		if ( TestUtils.findText(drv, "Uncategorized") )
		{
			log.error("Folder 'Uncategorized' was still found in the files/folders list");
			return false ;
		}
		//Now we need some test of a shared document and a library submission
		//Add Keep folder
		log.info("Create 'Keep Folder' to test sharing files with other users");
		addFolder = drv.findElement(By.linkText("Add Folder")) ;
		addFolder.click() ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Create folder : Keep Folder for further testing
		members_library_categories_name = drv.findElement(By.id("members_library_categories_name")) ;
		members_library_categories_name.sendKeys("Keep Folder");
		members_library_categories_name.submit();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		if ( !TestUtils.findText(drv, "Keep Folder") )
		{
			log.error("New folder 'Keep Folder' is not found in the files/folders list");
			return false ;
		}
		//Add keep files
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("Add file 'FileTest1.txt', place it into 'Keep Folder' and make the file shared");
		WebElement documents_is_corporate_shared = drv.findElement(By.id("documents_is_corporate_shared") ) ;
		documents_is_corporate_shared.click();
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest1.txt"); ;
		folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Keep Folder") ;
		documents_title = drv.findElement(By.id("documents_title") ) ;
		documents_title.clear(); 
		documents_title.sendKeys("First test file");
		documents_title.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("Add file 'FileTest2.txt', place it into 'Keep Folder' and NOT make it shared");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest2.txt"); ;
		folderSelect = drv.findElement(By.id("join_members_library_categories_id")) ;
		TestUtils.selectItem(folderSelect, "Keep Folder") ;
		documents_title = drv.findElement(By.id("documents_title") ) ;
		documents_title.clear(); 
		documents_title.sendKeys("Second test file");
		documents_title.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("Add file 'FileTest3.txt', not place it into any folders and make it shared");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		documents_is_corporate_shared = drv.findElement(By.id("documents_is_corporate_shared") ) ;
		documents_is_corporate_shared.click();
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest3.txt"); 
		documents_title = drv.findElement(By.id("documents_title") ) ;
		documents_title.clear(); 
		documents_title.sendKeys("Third test file");
		documents_title.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		log.info("Add file 'FileTest4.txt', not place it into any folders and not make it shared");
		addFile = drv.findElement(By.linkText("Add File"));
		addFile.click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		WebElement submitted = drv.findElement(By.id("submitted")) ;
		submitted.click();
		documents_file = drv.findElement(By.id("documents_file")) ;
		documents_file.sendKeys(baseDir + "FileTest4.txt"); ;
		documents_title = drv.findElement(By.id("documents_title") ) ;
		documents_title.clear(); 
		documents_title.sendKeys("Fourth test file");
		documents_title.submit();		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Checkboxes to share all not-shared files.
		log.info("Make all files shared using checkboxes on the files/folders list");
		//Search by xpath for elements: edit_documents_is_corporate_shared
		List<WebElement> checks = drv.findElements(By.xpath("//*[starts-with(@id,'edit_documents_is_corporate_shared')]")) ;
		log.info("Found " + checks.size() + " checkboxes...") ;
		for ( int i = 0 ; i < checks.size() ; i++ )
		{
			WebElement cb = (WebElement)checks.get(i) ;
			if ( cb.isDisplayed() && !cb.isSelected() )
			{
				cb.click();
				//System.out.println(" - click it");
			}
			TestUtils.Pause(100) ;
		}
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		log.info("Files/Folders Test: My Files section test completed successfully");
		return true ;
	}

	public void cleanUpMyFiles(WebDriver drv)
	{
		WebElement MyWUC = TestUtils.findDynamicElement(drv, By.partialLinkText("My WAKEUP"), 5) ;
		MyWUC.click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		WebElement myFiles = TestUtils.findDynamicElement(drv, By.linkText("My Files"), 5) ;
		myFiles.click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		if ( TestUtils.findText(drv, "You currently have no files listed") ) 
		{
			log.info("Clean Up My Files: No files found");
			return ;
		}

		log.info("Clean Up My Files: Delete all folders.");
		List allDeleteFolderLinks = drv.findElements(By.linkText("Delete Folder") ) ;
		for ( int i = 0 ; i < allDeleteFolderLinks.size() ; i++ )
		{
	 		WebElement deleteLink = TestUtils.getFirstElement(drv, "Delete Folder") ;
			deleteLink.click();
			drv.switchTo().activeElement() ;
			WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
			buttonOK.click();			
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.switchTo().parentFrame() ;
		}
		
		log.info("Clean Up My Files: Delete all files.");
		List allDeleteFileLinks = drv.findElements(By.linkText("Delete") ) ;
		//Check if my id extraction thingy works
		TestUtils.getIdStrings(drv, allDeleteFileLinks, "documents_id") ;

		for ( int i = 0 ; i < allDeleteFileLinks.size() ; i++ )
		{
	 		WebElement deleteLink = TestUtils.getFirstElement(drv, "Delete") ;
	 		System.out.println("Delete link: " + deleteLink.getAttribute("href") ) ;
			deleteLink.click();
			drv.switchTo().activeElement() ;
			WebElement buttonOK = drv.findElement(By.cssSelector("input.btn.ok-btn")) ;
			buttonOK.click();			
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			drv.switchTo().parentFrame() ;
		}
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}
}
