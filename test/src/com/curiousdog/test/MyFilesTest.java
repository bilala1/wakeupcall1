package com.curiousdog.test;

import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.curiousdog.test.utils.TestUtils;

public class MyFilesTest {

	public MyFilesTest(WebDriver drv) {
		System.out.println("Going to the main login page") ;
		drv.get("http://staging.wakeupcall.net/index.php") ;
		
		WebElement login_email = drv.findElement(By.id("login_email") ) ;	
		login_email.sendKeys("olik.oparina@gmail.com");
		
		WebElement login_password = drv.findElement(By.id("login_password") ) ;	
		login_password.sendKeys("password");
		
		WebElement remember_me = drv.findElement(By.id("remember_me")) ;
		remember_me.click();
		remember_me.submit();
		
		//try { drv.wait(); } catch(InterruptedException ie) { } 
		//TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		String currentURL = drv.getCurrentUrl() ;
		System.out.println("Current page is: " + currentURL ) ;
		/*
		WebElement waitingfor = TestUtils.findDynamicElement(drv, By.id("members_show_startup_video"), 10) ;
		WebElement members_show_startup_video = drv.findElement(By.id("members_show_startup_video") ) ;
		members_show_startup_video.click();
		members_show_startup_video.submit();
		*/
		MyFilesForm filesForm = new MyFilesForm() ;
		filesForm.TestFilesForm(drv) ;
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//WebDriver drv = new FirefoxDriver() ;
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,UnexpectedAlertBehaviour.ACCEPT);
		WebDriver drv = new FirefoxDriver(dc);

		MyFilesTest test = new MyFilesTest(drv) ;
		//MyAccountTest acctTest = new MyAccountTest(drv) ;
		//acctTest.TestMyAccountUsersIndividual(drv) ;
		
		TestUtils.Pause(10000) ;
		//drv.quit();
		System.out.println("TEST IS COMPLETED") ;
	}

}
