package com.curiousdog.test;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;

public class RunTest {

	static Logger log = Logger.getLogger(RunTest.class);
	
	public RunTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) 
	{
		boolean success = true ;
		
		System.out.println("Starting WakeUpCall test...");
		log.info("Starting WakeUpCall test program");
		
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		log.info("Opening home page: " + homePage);
		WebDriver drv = new FirefoxDriver() ;
	    //drv.manage().window().maximize();
	    //drv.navigate().to("http://www.google.com");
	    //drv.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		drv.get(homePage + "/index.php");
		
		System.out.println("Reading the required tests...");
		String tests = bundle.getString("DO.TEST") ;
		List<String> testsList = TestUtils.splitString(tests, ',') ;
		for ( int i = 0 ; i < testsList.size() ; i++ )
		{
			String testKey = (String)testsList.get(i) ;
			System.out.println("Running test #" + i + " : " + testKey);
			if ( !dispatcher(drv, testKey) ) 
			{
				log.info("******************* TEST FAILED **********************");
				success = false ;
				break ;
			}
			else
			{
				log.info("******************* TEST SUCCEEDED ********************");
			}
			TestUtils.Pause(1000) ;
		}
		
		TestUtils.Pause(TestUtils.PAUSE_BORING) ;
		if ( success ) drv.quit();
		log.info("TEST COMPLETED") ;
		System.out.println("TEST COMPLETED");
	}

	private static boolean dispatcher(WebDriver drv, String testKey)
	{
		if ( testKey == null || testKey.trim().length() == 0 ) 
		{
			log.error("Empty list of tests. Cannot continue");
			return false ;
		}
		if ( "LOGIN".equalsIgnoreCase(testKey) )
		{
			log.info("LOGIN TEST");
			LoginForm loginForm = new LoginForm() ;
			return loginForm.testLogin(drv) ;
		}
		else if ( "SIGNUP.CORPORATE".equalsIgnoreCase(testKey) )
		{
			
			log.info("Executing SIGNUP.CORPORATE test");
			SignUpCorporate suCorp = new SignUpCorporate() ;
			return suCorp.testForm(drv) ;
		}
		else if	( "SIGNUP.INDIVIDUAL".equalsIgnoreCase(testKey) )
		{
			log.info("Executing SIGNUP.INDIVIDUAL test");
			SignUpIndividual suInd = new SignUpIndividual() ;
			return suInd.testIndividualFormHotelFullService(drv) ;
		}
		else if ( "MYFILES".equalsIgnoreCase(testKey) )
		{
			log.info("Executing MYWUC My Files test") ;
			MyFilesForm myFiles = new MyFilesForm() ;
			return myFiles.TestFilesForm(drv) ;
		}
		else if ( "ACCOUNT.USERS".equalsIgnoreCase(testKey) )
		{
			log.info("Executing My Account Users Management test");
			MyAccountTest accUsers = new MyAccountTest() ;
			return accUsers.TestMyAccountUsersIndividual(drv) ;
		}
		else if ( "ACCOUNT.LOCATION".equalsIgnoreCase(testKey) )
		{
			log.info("Executing My Account Locations Management test");
			MyAccountTest accLocations = new MyAccountTest() ;
			return accLocations.TestMyAccountAddLocation(drv) ;
		}
		else if ( "CERTIFICATES.VENDOR".equalsIgnoreCase(testKey) )
		{
			log.info("Executing Certificates Vendors Management test");
			CertificatesTest certs = new CertificatesTest() ;
			return certs.testVendors(drv) ;
		}
		else if ( "ACCOUNT.SIGNATURE".equalsIgnoreCase(testKey) )
		{
			MyAccountTest accUsers = new MyAccountTest() ;
			return accUsers.TestForumSignature(drv) ;
		}
		else if ( "CORPBULLETIN".equalsIgnoreCase(testKey) )
		{
			CorporateBulletinForm corpBulletin = new CorporateBulletinForm() ;
			return corpBulletin.CorporateBulletinTest(drv) ;
		}
		else 
		{
			log.info("Test " + testKey + " is not yet implemented.") ;
			return true ;
		}
	}
}
