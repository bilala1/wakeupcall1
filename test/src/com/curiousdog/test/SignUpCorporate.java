package com.curiousdog.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.curiousdog.test.utils.TestUtils;

public class SignUpCorporate {

	static Logger log = Logger.getLogger(SignUpCorporate.class);

	public SignUpCorporate() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public boolean testForm(WebDriver drv)
	{
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;
		log.info("Going to the home page:  " + homePage + " and clicking on LET'S GO button") ;
		
		WebElement el = drv.findElement(By.partialLinkText("LET'S GO") ) ;	
		el.click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);
		
		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = dtf.format(new Date()) ;

		try
		{
			if (!TestUtils.stupidModalWindowTest(drv) ) return false;
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
		drv.findElement(By.id("btype_1")).click() ;
		TestUtils.Pause(100) ;
		log.info("Loaded Registration page and chosen Corporate registration") ;
		
		log.info("Submitting empty form to check error messages") ;
		boolean allGood = true ;
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);

		if ( !TestUtils.findText(drv, "Please correct the errors below (in red)") )
		{
			log.error("Error message 'Please correct the errors below (in red)' was not displayed on the screen");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Address")) 			
		{
			log.error("Error message 'You must enter a value for Address'  was not displayed on the screen");
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for City"))
		{
			log.error("Error message 'You must enter a value for City'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Zip")	)
		{
			log.error("Error message 'You must enter a value for Zip'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for First Name"))
		{
			log.error("Error message 'You must enter a value for First Name'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Last Name"))
		{
			log.error("Error message 'You must enter a value for Last Name'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Title"))
		{
			log.error("Error message 'You must enter a value for Title'  was not displayed on the screen") ;
			allGood = false ;
		}
		//if ( !TestUtils.findText(drv, "Phone must end with a number"))
		//{
		//	log.error("Error message 'Phone must end with a number'  was not displayed on the screen") ;
		//	allGood = false ;
		//}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("Error message 'You must enter a properly formatted email'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Password must have a minimm of 6 characters"))
		{
			log.error("Error message 'Password must have a minimm of 6 characters'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.'  was not displayed on the screen") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;		
			
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		log.info("Start filling out form...");
		drv.findElement(By.name("corporations_name_autocomplete")).sendKeys("BoomBam Inns" + timeStamp);
		drv.findElement(By.id("corporations_address")).sendKeys("500 Cherrington Pkwy");
		drv.findElement(By.id("corporations_city")).sendKeys("Coraopolis");
		Select dropdown_state = new Select(drv.findElement(By.id("corporations_state")));
		dropdown_state.selectByVisibleText("Pennsylvania");
		drv.findElement(By.id("corporations_zip")).sendKeys("15026");
		drv.findElement(By.id("members_firstname")).sendKeys("Jeffrey");
		drv.findElement(By.id("members_lastname")).sendKeys("Merritt");
		drv.findElement(By.id("members_title")).sendKeys("General Manager");
		
		log.info("Submitting the form with partially filled in information");
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);

		log.info("Checking displayed error messages");
		//if (!TestUtils.findText(drv, "Phone must end with a number"))
		//{
		//	log.error("Error message 'Phone must end with a number' was not displayed on the screen");
		//	allGood = false ;
		//}
		if (!TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("Error message 'You must enter a properly formatted email' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Password must have a minimm of 6 characters"))
		{
			log.error("Error message 'Password must have a minimm of 6 characters' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.' was not displayed on the screen");
			allGood = false ;
		}
		if (!allGood) return false ;

		log.info("Testing sending incorrect phone number");
		drv.findElement(By.id("members_phone")).sendKeys("ABCDEFGHI7") ;
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);
				
		if (!TestUtils.findText(drv, "Phone cannot contain non-numeric characters except (, ), and -"))
		{
			log.error("Error message 'Phone cannot contain non-numeric characters except (, ), and -");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("Error message 'You must enter a properly formatted email' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Password must have a minimm of 6 characters"))
		{
			log.error("Error message 'Password must have a minimm of 6 characters' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.' was not displayed on the screen");
			allGood = false ;
		}
		if (!allGood) return false ;
		
		drv.findElement(By.id("members_phone")).clear() ;
		drv.findElement(By.id("members_phone")).sendKeys("4128934747") ;
		log.info("Submitting poorly-formed email address") ;
		drv.findElement(By.id("members_email")).sendKeys("abrakadabracom");
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);

		if (!TestUtils.findText(drv, "You must enter a properly formatted email"))
		{
			log.error("Error message 'You must enter a properly formatted email' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Password must have a minimm of 6 characters"))
		{
			log.error("Error message 'Password must have a minimm of 6 characters' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.' was not displayed on the screen");
			allGood = false ;
		}
		if (!allGood) return false ;
		
		log.info("Submitting not-matching email addresses") ;
		drv.findElement(By.id("members_phone")).clear() ;
		drv.findElement(By.id("members_phone")).sendKeys("8370)8383838))") ;
		drv.findElement(By.id("members_email")).sendKeys("abra@kadabra.com");
		drv.findElement(By.id("members_email2")).sendKeys("poops@yahoo.com");
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);

		if (!TestUtils.findText(drv, "Please enter your ten digit phone number"))
		{
			log.error("Error message 'Please enter your ten digit phone number' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Must match other value"))
		{
			log.error("Error message 'Must match other value' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Password must have a minimm of 6 characters"))
		{
			log.error("Error message 'Password must have a minimm of 6 characters' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.' was not displayed on the screen");
			allGood = false ;
		}
		if (!allGood) return false ;

		String userEmail = bundle.getString("LOGIN") ;
		String password  = bundle.getString("PASSWORD") ;
		
		log.info("Sending not matching passwords");
		drv.findElement(By.id("members_email")).clear() ;
		drv.findElement(By.id("members_email")).sendKeys(userEmail);
		drv.findElement(By.id("members_email2")).clear() ;
		drv.findElement(By.id("members_email2")).sendKeys(userEmail);
		drv.findElement(By.id("members_phone")).clear() ;
		drv.findElement(By.id("members_phone")).sendKeys("8888888888))") ;
		drv.findElement(By.id("members_password")).sendKeys(password + "1");
		drv.findElement(By.id("members_password2")).sendKeys(password + "2");
		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT);

		if (!TestUtils.findText(drv, "Must match other value"))
		{
			log.error("Error message 'Must match other value' was not displayed on the screen");
			allGood = false ;
		}
		if (!TestUtils.findText(drv, "Please select yes or no."))
		{
			log.error("Error message 'Please select yes or no.' was not displayed on the screen");
			allGood = false ;
		}
		if (!allGood) return false ;

		drv.findElement(By.id("members_password")).clear() ;
		drv.findElement(By.id("members_password")).sendKeys(password);
		drv.findElement(By.id("members_password2")).clear() ;
		drv.findElement(By.id("members_password2")).sendKeys(password);
		drv.findElement(By.id("separate_0")).click() ;
		drv.findElement(By.id("corporate_locations")).sendKeys("10");
		drv.findElement(By.id("other_loc_0")).click();
		TestUtils.Pause(100) ;
		
		//When we click the radio button above - new location div should pop up
		
		if ( !drv.findElement(By.name("first_hotels_name_autocomplete")).isDisplayed() ||
			 !drv.findElement(By.id("first_hotels_num_rooms")).isDisplayed() ||
			 !drv.findElement(By.id("first_hotels_address")).isDisplayed()   || 
			 !drv.findElement(By.id("first_hotels_city")).isDisplayed() ||
			 !drv.findElement(By.id("first_hotels_zip")).isDisplayed()  ||
			 !drv.findElement(By.id("first_ind_type")).isDisplayed()    ||
			 !drv.findElement(By.id("first_hotels_state")).isDisplayed() )
		{
			System.out.println("Locations form did not display") ;
			return false ;
		}
		
		drv.findElement(By.id("other_loc_1")).click();
		TestUtils.Pause(100) ;
		
		//When we click the radio button above - new location div should pop up
		
		if ( drv.findElement(By.name("first_hotels_name_autocomplete")).isDisplayed() ||
			 drv.findElement(By.id("first_hotels_num_rooms")).isDisplayed() ||
			 drv.findElement(By.id("first_hotels_address")).isDisplayed()   || 
			 drv.findElement(By.id("first_hotels_city")).isDisplayed() ||
			 drv.findElement(By.id("first_hotels_zip")).isDisplayed()  ||
			 drv.findElement(By.id("first_ind_type")).isDisplayed()    ||
			 drv.findElement(By.id("first_hotels_state")).isDisplayed() )
		{
			System.out.println("Locations form did not disappear") ;
			return false ;
		}

		drv.findElement(By.id("other_loc_0")).click();
		TestUtils.Pause(100) ;

		drv.findElement(By.name("first_hotels_name_autocomplete")).sendKeys("Pittsburgh Fleabag Supreme") ;
		drv.findElement(By.id("first_hotels_num_rooms")).sendKeys("50") ;
		drv.findElement(By.id("first_hotels_address")).sendKeys("502 Cherrington Pkwy") ;
		drv.findElement(By.id("first_hotels_city")).sendKeys("Coraopolis") ;
		drv.findElement(By.id("first_hotels_zip")).sendKeys("15108") ;

		Select first_hotels_state = new Select(drv.findElement(By.id("first_hotels_state")));
		first_hotels_state.selectByVisibleText("Pennsylvania");

		Select first_ind_type = new Select(drv.findElement(By.id("first_ind_type")));
		first_ind_type.selectByVisibleText("Hotel - Full Service/Resort");

		drv.findElement(By.id("terms")).click() ;
		drv.findElement(By.id("type_join")).click();
		
		//The payment form should be displayed
		SignUpPaymentForm getMoney = new SignUpPaymentForm(drv, bundle) ;
		if ( !getMoney.fillOutForm(drv, bundle) )
		{
			//drv.get("http://staging.wakeupcall.net/index.php") ;
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
			return false ;
		}
		
		System.out.println("CORPORATE SIGN IN TEST IS COMPLETED SUCCESSFULLY") ;
		return true ;
	}
}
