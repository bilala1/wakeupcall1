package com.curiousdog.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.curiousdog.test.utils.TestUtils;

public class SignUpIndividual {

	static Logger log = Logger.getLogger(SignUpIndividual.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public SignUpIndividual() 
	{
	
	}

	public boolean testIndividualFormHotelFullService (WebDriver drv)
	{
		//Go to the registration test
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String homePage = bundle.getString("HOME.PAGE") ;

		drv.findElement(By.linkText("LET'S GO")).click();
		TestUtils.Pause(1000) ;
		
		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMMddhhmmss") ;
		String timeStamp = dtf.format(new Date()) ;
				
		//Verify page's URL 
		String currentUrl = drv.getCurrentUrl() ;
		log.info("Redirected to URL = '" + currentUrl + "'") ;
		if ( currentUrl.indexOf("signup-direct.php") <= 0 )
		{
			log.error("Unexpected URL: '" + currentUrl + "'. Test failed.");
			return false ;
		}
		//Choose Individual registration:
		log.info("Choosing individual registration");
		WebElement radio1 = drv.findElement(By.id("btype_0")) ;
		radio1.click();
		if ( !TestUtils.findText(drv, "Account Administrator:" ) ) 
		{
			log.error("We loaded something wrong! 'Account Administrator' label is not found. Test failed.") ;
			return false ;
		}
		
		log.info("Fill out only one field and check error messages") ;

		String locationName = "Individual Member " + timeStamp ;
		
		drv.findElement(By.name("hotels_name_autocomplete")).sendKeys(locationName)  ;
		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register") ).click();
		TestUtils.Pause(500);
		
		//Must display a few error messages:
		boolean allGood = true ;
		if ( !TestUtils.findText(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Please select a location type") )
		{
			log.error("Missing error message: Please select a location type") ;
			allGood = false ;
		}		
		if ( !TestUtils.findText(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		
		TestUtils.Pause(500);
		
		log.info("Submit only 2 fields") ;
		Select dropdown_indtype = new Select(drv.findElement(By.id("ind_type")));
		dropdown_indtype.selectByVisibleText("Hotel - Full Service/Resort");
		
		log.info("Enter incorrect value into number of rooms:") ;
		drv.findElement(By.name("hotels_num_rooms")).sendKeys("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		
		//Submit for full (not-trial) membership
		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register")).click();
		TestUtils.Pause(500);
		allGood = true ;
		if ( !TestUtils.findText(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Submit form with filled out Address") ;
		drv.findElement(By.name("hotels_address")).sendKeys("123 Main St");
		drv.findElement(By.name("hotels_city")).sendKeys("Poopsville");
		Select dropdown_state = new Select(drv.findElement(By.id("hotels_state")));
		dropdown_state.selectByVisibleText("Georgia");
		log.info("Submit non-numeric zip code");
		drv.findElement(By.name("hotels_zip")).sendKeys("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

		//Submit for trial membership
		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register")).click();
		TestUtils.Pause(500);

		if ( !TestUtils.findText(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Enter Personal Information") ;
		drv.findElement(By.name("hotels_members_firstname")).sendKeys("Jennifer");
		drv.findElement(By.name("hotels_members_lastname")).sendKeys("Aniston");
		drv.findElement(By.name("hotels_members_title")).sendKeys("Owner/Manager");
		log.info("Send incorrect phone number characters");
		drv.findElement(By.name("hotels_members_phone")).sendKeys("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

		//Submit for trial membership
		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register")).click();
		TestUtils.Pause(500);
		
		allGood = true ;
		if ( !TestUtils.findText(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.findText(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if (!allGood) return false ;

		log.info("Changing last characters of phone number to numbers");
		drv.findElement(By.name("hotels_members_phone")).clear();
		drv.findElement(By.name("hotels_members_phone")).sendKeys("ABCDEF1122");
		TestUtils.Pause(500);

		log.info("Submit for trial membership with fixed phone number") ;
		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register")).click();
		TestUtils.Pause(500);
		
		//Fix phone number - replace the last character with a number:
		if ( !TestUtils.findText(drv, "Phone cannot contain non-numeric characters except (, ), and -") )
		{
			log.error("Missing error message: Phone cannot contain non-numeric characters except (, ), and -") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Fixed phone number") ;
		drv.findElement(By.name("hotels_members_phone")).clear();
		drv.findElement(By.name("hotels_members_phone")).sendKeys("1234567890");

		log.info("Sending not-matching emails and passwords") ;
		drv.findElement(By.name("hotels_members_email")).sendKeys("dzaytsev+1@curiousdog.com");
		drv.findElement(By.name("hotels_members_email2")).sendKeys("dzaytsev+2@curiousdog.com");
		drv.findElement(By.name("hotels_pass")).sendKeys("password1");
		drv.findElement(By.name("hotels_pass2")).sendKeys("password2");

		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_register")).click();
		TestUtils.Pause(500);
		
		if ( !TestUtils.findText(drv, "Must match other value") )
		{
			log.error("Missing error message: Must match other value") ;
			return false ;
		}
		log.info("Fixed matching email and password");
		drv.findElement(By.name("hotels_members_email")).clear();
		drv.findElement(By.name("hotels_members_email2")).clear() ;
		drv.findElement(By.name("hotels_pass")).clear() ;
		drv.findElement(By.name("hotels_pass2")).clear() ;		
		TestUtils.Pause(500);
		
		drv.findElement(By.name("hotels_members_email")).sendKeys("dzaytsev+8@curiousdog.com");
		drv.findElement(By.name("hotels_members_email2")).sendKeys("dzaytsev+8@curiousdog.com");
		drv.findElement(By.name("hotels_pass")).sendKeys("password");
		drv.findElement(By.name("hotels_pass2")).sendKeys("password");
		TestUtils.Pause(500);

		drv.findElement(By.id("terms")).click();
		drv.findElement(By.id("type_join")).click();		
		TestUtils.Pause(500);
		
		//Switch to payment form
		SignUpPaymentForm getMoney = new SignUpPaymentForm(drv, bundle) ;
		if ( !getMoney.fillOutForm(drv, bundle) )
		{
			drv.get(homePage) ;
			return false ;
		}
		//Go back to the Main login page.
		
		log.info("Individual sign-up form test is completed");
		TestUtils.Pause(500);
		
		
		return true ;
	}

	/*
	 * 
	 * FROM THIS POINT DOWN NEEDS SOME FIXING UP
	 * 
	 */
	public boolean testIndividualFormHotelLimitedService (WebDriver drv) throws InterruptedException
	{
		//Verify page's URL
		String currentUrl = drv.getCurrentUrl() ;
		log.info("Current URL = " + currentUrl) ;

		//Choose Individual registration:
		WebElement radio1 = drv.findElement(By.id("btype_0")) ;
		radio1.click();
		if ( !TestUtils.verifyTextPresent(drv, "Account Administrator:" ) ) 
		{
			log.error("We loaded something wrong!") ;
		}
		else
		{
			log.info("Correct form for Individual") ;
		}
				
		//Verify labels on the screen
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String[] labels = TestUtils.getStringArray(bundle, "SIGNUP.INDIVIDUAL.LABELS") ;
//		if ( !TestUtils.verifyLabels(drv, labels) )
//		{
//			System.out.println("Form is missing labels. See results above");
//			return false ;
//		}
		//Verify inputs on the screen
		List txtValues= TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.TEXT") , ',') ;
		List txtBoxes = TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.INPUT"), ',') ;		
		if ( !TestUtils.verifyTextBoxes(drv, txtBoxes) )
		{
			log.error("Form is missing input boxes. See results above") ;
			return false ;
		}
		
		log.info("Fill out one field") ;
		
		WebElement hotels_name_autocomplete = drv.findElement(By.name("hotels_name_autocomplete")) ;
		hotels_name_autocomplete.sendKeys("Amish Motel") ;
		//Submit for full (not-trial) membership
		WebElement agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		WebElement submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();
		Thread.sleep(500);
		
		log.info("Verify result of 1 field submission") ;
		//Must display a few error messages:
		boolean allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Please select a location type") )
		{
			log.error("Missing error message: Please select a location type") ;
			allGood = false ;
		}		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		
		Thread.sleep(100);
		
		log.info("Submit 2 fields") ;
		Select dropdown_indtype = new Select(drv.findElement(By.id("ind_type")));
		dropdown_indtype.selectByVisibleText("Hotel - Limited Service");
		
		log.info("Enter incorrect value into number of rooms:") ;
		WebElement hotels_num_rooms = drv.findElement(By.name("hotels_num_rooms")) ;
		hotels_num_rooms.sendKeys("30");
		
		//Submit for full (not-trial) membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();
		Thread.sleep(500);

		log.info("Verifying error messages");
		allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Submit Address") ;
		WebElement hotels_address = drv.findElement(By.name("hotels_address") ) ;
		hotels_address.sendKeys("234 Woodcrest Rd");
		WebElement hotels_city = drv.findElement(By.name("hotels_city") ) ;
		hotels_city.sendKeys("Pottersville");
		Select dropdown_state = new Select(drv.findElement(By.id("hotels_state")));
		dropdown_state.selectByVisibleText("Ohio");
		log.info("Submit non-numeric zip code");
		WebElement hotels_zip = drv.findElement(By.name("hotels_zip") ) ;
		hotels_zip.sendKeys("17345");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();
		Thread.sleep(500);

		log.info("Validate error messages:") ;
		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Enter Personal Information:") ;
		WebElement hotels_members_firstname = drv.findElement(By.name("hotels_members_firstname")) ;
		hotels_members_firstname.sendKeys("Bradly");
		WebElement hotels_members_lastname  = drv.findElement(By.name("hotels_members_lastname")) ;
		hotels_members_lastname.sendKeys("Pitts");
		WebElement hotels_members_title     = drv.findElement(By.name("hotels_members_title")) ;
		hotels_members_title.sendKeys("General Manager");
		log.info("Send incorrect phone number characters:");
		WebElement hotels_members_phone     = drv.findElement(By.name("hotels_members_phone")) ;
		hotels_members_phone.sendKeys("4443332211");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();
		Thread.sleep(500);
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if (!allGood) return false ;

		Thread.sleep(1000);
		
		log.info("Sending not-matching emails and passwords") ;
		
		WebElement hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email.sendKeys("dzaytsev+1@curiousdog.com");
		WebElement hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_members_email2.sendKeys("dzaytsev+2@curiousdog.com");
		WebElement hotels_pass 				= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass.sendKeys("password1");
		WebElement hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		hotels_pass2.sendKeys("password2");
		
		log.info("Submit for trial membership with not-matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();
		log.info("Submitted") ;
		
		Thread.sleep(1000);
		
		if ( !TestUtils.verifyTextPresent(drv, "Must match other value") )
		{
			log.error("Missing error message: Must match other value") ;
			return false ;
		}
		log.info("Fixed matching email and password");
		hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_pass 			= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		
		hotels_members_email.clear() ;
		hotels_members_email2.clear() ;
		hotels_pass.clear() ;
		hotels_pass2.clear() ;
		Thread.sleep(500);
		
		hotels_members_email.sendKeys("dzaytsev+2@curiousdog.com");
		hotels_members_email2.sendKeys("dzaytsev+2@curiousdog.com");
		hotels_pass.sendKeys("password");
		hotels_pass2.sendKeys("password");
		Thread.sleep(1000);
		
		log.info("Submit for Full membership with matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitFullMembership = drv.findElement(By.id("type_join") ) ;
		submitFullMembership.click();

		log.info("FORM TEST IS COMPLETED");

		Thread.sleep(5000);
		
		
		return true ;
	}
	
	public boolean testIndividualFormSpa (WebDriver drv) throws InterruptedException
	{
		//Verify page's URL
		String currentUrl = drv.getCurrentUrl() ;
		log.info("Current URL = " + currentUrl) ;

		//Choose Individual registration:
		WebElement radio1 = drv.findElement(By.id("btype_0")) ;
		radio1.click();
		if ( !TestUtils.verifyTextPresent(drv, "Account Administrator:" ) ) 
		{
			log.error("We loaded something wrong!") ;
		}
		else
		{
			log.info("Correct form for Individual") ;
		}
		
		WebElement trial_code = drv.findElement(By.name("trial_code")) ;
		trial_code.sendKeys("ABC123-ABC123");
		
		//Verify labels on the screen
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		String[] labels = TestUtils.getStringArray(bundle, "SIGNUP.INDIVIDUAL.LABELS") ;
//		if ( !TestUtils.verifyLabels(drv, labels) )
//		{
//			System.out.println("Form is missing labels. See results above");
//			return false ;
//		}
		//Verify inputs on the screen
		List txtValues= TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.TEXT") , ',') ;
		List txtBoxes = TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.INPUT"), ',') ;		
		if ( !TestUtils.verifyTextBoxes(drv, txtBoxes) )
		{
			log.error("Form is missing input boxes. See results above") ;
			return false ;
		}
		
		log.info("Fill out one field") ;
		
		WebElement hotels_name_autocomplete = drv.findElement(By.name("hotels_name_autocomplete")) ;
		hotels_name_autocomplete.sendKeys("Butterworks1") ;
		//Submit for full (not-trial) membership
		WebElement agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		WebElement submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);
		
		log.info("Verify result of 1 field submission") ;
		//Must display a few error messages:
		boolean allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv,  "This trial code is invalid" ) )
		{
			log.error("Missing error message: This trial code is invalid") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Please select a location type") )
		{
			log.error("Missing error message: Please select a location type") ;
			allGood = false ;
		}		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		
		Thread.sleep(100);
		
		log.info("Submit 2 fields") ;
		Select dropdown_indtype = new Select(drv.findElement(By.id("ind_type")));
		dropdown_indtype.selectByVisibleText("Spa - Destination Spa");
		
		log.info("Fixing trial code");
		trial_code = drv.findElement(By.name("trial_code")) ;
		trial_code.clear(); 
		//trial_code.sendKeys("freetrial");
		
		log.info("Enter incorrect value into number of rooms") ;
		WebElement hotels_num_rooms = drv.findElement(By.name("hotels_num_rooms")) ;
		hotels_num_rooms.sendKeys("150");
		
		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);

		log.info("Verifying error messages");
		allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Submit Address") ;
		WebElement hotels_address = drv.findElement(By.name("hotels_address") ) ;
		hotels_address.sendKeys("456 George Bush Jr. Lane");
		WebElement hotels_city = drv.findElement(By.name("hotels_city") ) ;
		hotels_city.sendKeys("Puttsville");
		Select dropdown_state = new Select(drv.findElement(By.id("hotels_state")));
		dropdown_state.selectByVisibleText("Texas");
		log.info("Submit non-numeric zip code");
		WebElement hotels_zip = drv.findElement(By.name("hotels_zip") ) ;
		hotels_zip.sendKeys("57345");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);

		log.info("Validate error messages:") ;
		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Enter Personal Information:") ;
		WebElement hotels_members_firstname = drv.findElement(By.name("hotels_members_firstname")) ;
		hotels_members_firstname.sendKeys("Angelina");
		WebElement hotels_members_lastname  = drv.findElement(By.name("hotels_members_lastname")) ;
		hotels_members_lastname.sendKeys("Jolly");
		WebElement hotels_members_title     = drv.findElement(By.name("hotels_members_title")) ;
		hotels_members_title.sendKeys("Proprietor");
		log.info("Send incorrect phone number characters:");
		WebElement hotels_members_phone     = drv.findElement(By.name("hotels_members_phone")) ;
		hotels_members_phone.sendKeys("5552221212");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if (!allGood) return false ;

		Thread.sleep(1000);
		
		log.info("Sending not-matching emails and passwords") ;
		
		WebElement hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email.sendKeys("dzaytsev+2@curiousdog.com");
		WebElement hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_members_email2.sendKeys("dzaytsev+3@curiousdog.com");
		WebElement hotels_pass 				= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass.sendKeys("password1");
		WebElement hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		hotels_pass2.sendKeys("password2");
		
		log.info("Submit for trial membership with not-matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		log.info("Submitted") ;
		
		Thread.sleep(1000);
		
		if ( !TestUtils.verifyTextPresent(drv, "Must match other value") )
		{
			log.error("Missing error message: Must match other value") ;
			return false ;
		}
		log.info("Fixed matching email and password");
		hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_pass 			= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		
		hotels_members_email.clear() ;
		hotels_members_email2.clear() ;
		hotels_pass.clear() ;
		hotels_pass2.clear() ;
		Thread.sleep(500);
		
		hotels_members_email.sendKeys("dzaytsev+14@curiousdog.com");
		hotels_members_email2.sendKeys("dzaytsev+14@curiousdog.com");
		hotels_pass.sendKeys("password");
		hotels_pass2.sendKeys("password");
		Thread.sleep(1000);
		
		log.info("Submit for trial membership with matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();

		log.info("FORM TEST IS COMPLETED");

		Thread.sleep(5000);
		
		
		return true ;
	}

	public boolean testIndividualFormOther (WebDriver drv) throws InterruptedException
	{
		//Verify page's URL
		String currentUrl = drv.getCurrentUrl() ;
		log.info("Current URL = " + currentUrl) ;

		//Choose Individual registration:
		WebElement radio1 = drv.findElement(By.id("btype_0")) ;
		radio1.click();
		if ( !TestUtils.verifyTextPresent(drv, "Account Administrator:" ) ) 
		{
			log.error("We loaded something wrong!") ;
		}
		else
		{
			log.info("Correct form for Individual") ;
		}
		
		WebElement trial_code = drv.findElement(By.name("trial_code")) ;
		trial_code.sendKeys("ABC123-ABC123");
		
		//Verify labels on the screen
		ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
//		String[] labels = TestUtils.getStringArray(bundle, "SIGNUP.INDIVIDUAL.LABELS") ;
//		if ( !TestUtils.verifyLabels(drv, labels) )
//		{
//			System.out.println("Form is missing labels. See results above");
//			return false ;
//		}
		//Verify inputs on the screen
		List txtValues= TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.TEXT") , ',') ;
		List txtBoxes = TestUtils.splitString(bundle.getString("SIGNUP.INDIVIDUAL.INPUT"), ',') ;		
		if ( !TestUtils.verifyTextBoxes(drv, txtBoxes) )
		{
			log.error("Form is missing input boxes. See results above") ;
			return false ;
		}
		
		log.info("Fill out one field") ;
		
		WebElement hotels_name_autocomplete = drv.findElement(By.name("hotels_name_autocomplete")) ;
		hotels_name_autocomplete.sendKeys("Butterworks2") ;
		//Submit for full (not-trial) membership
		WebElement agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		WebElement submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);
		
		log.info("Verify result of 1 field submission") ;
		//Must display a few error messages:
		boolean allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv,  "This trial code is invalid" ) )
		{
			log.error("Missing error message: This trial code is invalid") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Please select a location type") )
		{
			log.error("Missing error message: Please select a location type") ;
			allGood = false ;
		}		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;
		
		Thread.sleep(100);
		
		log.info("Submit 2 fields") ;
		Select dropdown_indtype = new Select(drv.findElement(By.id("ind_type")));
		dropdown_indtype.selectByVisibleText("Spa - Destination Spa");
		
		log.info("Fixing trial code");
		trial_code = drv.findElement(By.name("trial_code")) ;
		trial_code.clear(); 
		//trial_code.sendKeys("freetrial");
		
		log.info("Enter incorrect value into number of rooms:") ;
		WebElement hotels_num_rooms = drv.findElement(By.name("hotels_num_rooms")) ;
		hotels_num_rooms.sendKeys("150");
		
		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);

		log.info("Verifying error messages");
		allGood = true ;
		if ( !TestUtils.verifyTextPresent(drv, "Please correct the errors below (in red)") )
		{
			log.error("Missing error message: Please correct the errors below (in red)") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Address"))
		{
			log.error("Missing error message: You must enter a value for Address") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for City") )
		{
			log.error("Missing error message: You must enter a value for City") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Zip") )
		{
			log.error("Missing error message: You must enter a value for Zip") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			log.error("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			log.error("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			log.error("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			log.error("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			log.error("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			log.error("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		log.info("Submit Address") ;
		WebElement hotels_address = drv.findElement(By.name("hotels_address") ) ;
		hotels_address.sendKeys("456 George Bush Jr. Lane");
		WebElement hotels_city = drv.findElement(By.name("hotels_city") ) ;
		hotels_city.sendKeys("Puttsville");
		Select dropdown_state = new Select(drv.findElement(By.id("hotels_state")));
		dropdown_state.selectByVisibleText("Texas");
		System.out.println("Submit non-numeric zip code");
		WebElement hotels_zip = drv.findElement(By.name("hotels_zip") ) ;
		hotels_zip.sendKeys("57345");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);

		System.out.println("Validate error messages:") ;
		
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Firstname") )
		{
			System.out.println("Missing error message: You must enter a value for Firstname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Lastname") )
		{
			System.out.println("Missing error message: You must enter a value for Lastname") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a value for Title") )
		{
			System.out.println("Missing error message: You must enter a value for Title") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Phone must end with a number") )
		{
			System.out.println("Missing error message: Phone must end with a number") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			System.out.println("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			System.out.println("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if ( !allGood ) return false ;

		System.out.println("Enter Personal Information:") ;
		WebElement hotels_members_firstname = drv.findElement(By.name("hotels_members_firstname")) ;
		hotels_members_firstname.sendKeys("Angelina");
		WebElement hotels_members_lastname  = drv.findElement(By.name("hotels_members_lastname")) ;
		hotels_members_lastname.sendKeys("Jolly");
		WebElement hotels_members_title     = drv.findElement(By.name("hotels_members_title")) ;
		hotels_members_title.sendKeys("Proprietor");
		System.out.println("Send incorrect phone number characters:");
		WebElement hotels_members_phone     = drv.findElement(By.name("hotels_members_phone")) ;
		hotels_members_phone.sendKeys("5552221212");

		//Submit for trial membership
		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_register") ) ;
		submitTrialMembership.click();
		Thread.sleep(500);
		if ( !TestUtils.verifyTextPresent(drv, "You must enter a properly formatted email") )
		{
			System.out.println("Missing error message: You must enter a properly formatted email") ;
			allGood = false ;
		}
		if ( !TestUtils.verifyTextPresent(drv, "Password must have a minimm of 6 characters") )
		{
			System.out.println("Missing error message: Password must have a minimm of 6 characters") ;
			allGood = false ;
		}
		if (!allGood) return false ;

		Thread.sleep(1000);
		
		System.out.println("Sending not-matching emails and passwords") ;
		
		WebElement hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email.sendKeys("dzaytsev+13@curiousdog.com");
		WebElement hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_members_email2.sendKeys("dzaytsev+13@curiousdog.com");
		WebElement hotels_pass 				= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass.sendKeys("password1");
		WebElement hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		hotels_pass2.sendKeys("password2");
		
		System.out.println("Submit for full membership with not-matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_join") ) ;
		submitTrialMembership.click();
		System.out.println("Submitted") ;
		
		Thread.sleep(1000);
		
		if ( !TestUtils.verifyTextPresent(drv, "Must match other value") )
		{
			System.out.println("Missing error message: Must match other value") ;
			return false ;
		}
		System.out.println("Fixed matching email and password");
		hotels_members_email 	= drv.findElement(By.name("hotels_members_email")) ;
		hotels_members_email2 	= drv.findElement(By.name("hotels_members_email2")) ;
		hotels_pass 			= drv.findElement(By.name("hotels_pass")) ;
		hotels_pass2 			= drv.findElement(By.name("hotels_pass2")) ;
		
		hotels_members_email.clear() ;
		hotels_members_email2.clear() ;
		hotels_pass.clear() ;
		hotels_pass2.clear() ;
		Thread.sleep(500);
		
		hotels_members_email.sendKeys("dzaytsev+3@curiousdog.com");
		hotels_members_email2.sendKeys("dzaytsev+3@curiousdog.com");
		hotels_pass.sendKeys("password");
		hotels_pass2.sendKeys("password");
		Thread.sleep(1000);
		
		System.out.println("Submit for full membership with matching emails and passwords") ;

		agreeToTerms = drv.findElement(By.id("terms")) ;
		agreeToTerms.click();
		submitTrialMembership = drv.findElement(By.id("type_join") ) ;
		submitTrialMembership.click();

		System.out.println("FORM TEST IS COMPLETED");

		Thread.sleep(5000);
		
		
		return true ;
	}
	
	/*
    public String getAlert(WebDriver drv) {
        Alert alert = drv.switchTo().alert();
        String alertText = alert.getText().trim();
        try
        {
        	alert.wait(100);
        }
        catch(Exception ex) { }
        alert.accept();
        return alertText;
    }
	*/
}
