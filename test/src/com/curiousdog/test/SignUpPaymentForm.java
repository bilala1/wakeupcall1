package com.curiousdog.test;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.curiousdog.test.utils.TestUtils;

public class SignUpPaymentForm {

	static Logger log = Logger.getLogger(SignUpPaymentForm.class);

	public SignUpPaymentForm(WebDriver drv, ResourceBundle bundle) {
		// TODO Auto-generated constructor stub
	}

	public boolean fillOutForm(WebDriver drv, ResourceBundle bundle)
	{
		String homePage = bundle.getString("HOME.PAGE") ;
		
		log.info("Loading payment info form") ;
		//Make sure we got to the right place:
		if ( !(homePage+"/full-membership.php").equalsIgnoreCase(drv.getCurrentUrl()) ) 
		{
			log.error("The page did not get redirected to the payment page. Test failed.") ;
			return false ;
		}
		
		log.info("Load payment info from properties file");
		List<String> txtBoxes = TestUtils.splitString(bundle.getString("SIGNUP.BILLING.INPUT"), ',') ;
		List<String> txtValues= TestUtils.splitString(bundle.getString("SIGNUP.BILLING.TEXT"), ',') ;
		for ( int i = 0 ; i < txtBoxes.size() ; i++ )
		{
			log.info("Payment info filled: " + txtBoxes.get(i) + " = " + txtValues.get(i));
			WebElement elm = drv.findElement(By.name( txtBoxes.get(i) ) ) ;
			elm.sendKeys( txtValues.get(i) );
		}
		
		//click dropdowns
		Select dropdown_cardType = new Select(drv.findElement(By.id("members_card_type")));
		dropdown_cardType.selectByVisibleText("Visa");
		Select members_card_expire_month = new Select(drv.findElement(By.id("members_card_expire_month"))) ;
		members_card_expire_month.selectByVisibleText("3");
		Select members_card_expire_year = new Select(drv.findElement(By.id("members_card_expire_year"))) ;
		members_card_expire_year.selectByVisibleText("2020");
		log.info("Submitting payment info form");
		drv.findElement(By.id("discount_code")).submit();
		return true ;
	}
	
	public boolean checkSameAddress(WebDriver drv) throws InterruptedException
	{
		WebElement checkSameAddress = drv.findElement(By.id("same_address")) ;
		checkSameAddress.click();
		
		Thread.sleep(600);
		return false ;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
