package com.curiousdog.test;

import com.curiousdog.test.utils.TestUtils;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class TestBase {

    private static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest");
    private static WebDriver driver = new FirefoxDriver();

    public ResourceBundle getBundle() {
        return bundle;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void login(String userId, String password) {
        WebDriver driver = getDriver();
        String homePage = getBundle().getString("HOME.PAGE");
        driver.get(homePage + "/index.php");
        TestUtils.Pause(TestUtils.PAUSE_SHORT);
        driver.findElement(By.id("login_email")).clear();
        driver.findElement(By.id("login_email")).sendKeys(userId);
        driver.findElement(By.id("login_password")).clear();
        driver.findElement(By.id("login_password")).sendKeys(password);
        driver.findElement(By.id("memberlogin")).submit();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.or(
                ExpectedConditions.urlContains("rss-news.php"),
                ExpectedConditions.urlContains("video.php")));
    }

    protected String formatDate(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/d/yyyy");
        return dateFormat.format(date);
    }

    protected void navigate(String path) {
        getDriver().get(getBundle().getString("HOME.PAGE") + path);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains(path));
    }

    protected void navigateToKey(String urlKey) {
        navigate(getBundle().getString(urlKey));
    }

    protected void assertNavigate(String path) {
        WebDriver driver = getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            wait.until(ExpectedConditions.urlContains(path));
        } catch (Exception e) {
        }
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(path), "Should have navigated to path: " + path + '\n' + "Actual: " + currentUrl);
    }

    protected void assertNavigateToKey(String urlKey) {
        assertNavigate(getBundle().getString(urlKey));
    }

    protected void assertTextExists(String text, String error) {
        WebElement body = getDriver().findElement(By.tagName("body"));
        String bodyText = body.getText();
        if (!bodyText.contains(text)) {
            Assert.fail("Text not found: " + text + '\n' + error);
        }
    }

    protected WebElement safeFindElementById(String id) {
        try {
            return getDriver().findElement(By.id(id));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    protected void setTextbox(String id, String text) {
        setTextbox(safeFindElementById(id), text);
    }

    protected void setTextbox(WebElement element, String text) {
        if (element == null) {
            return;
        }
        element.clear();
        element.sendKeys(text);
    }

    protected void setCheckbox(String id, boolean checked) {
        setCheckbox(safeFindElementById(id), checked);
    }

    protected void setCheckbox(WebElement element, boolean checked) {
        if (element == null) {
            return;
        }
        boolean elementChecked = "checked".equals(element.getAttribute("checked"));
        if (elementChecked != checked) {
            element.click();
        }
    }

    protected void setDropdown(String id, Object value) {
        setDropdown(safeFindElementById(id), value);
    }

    protected void setDropdown(WebElement element, Object value) {
        if (element == null) {
            return;
        }
        Select select = new Select(element);
        select.selectByValue(value.toString());
    }
}
