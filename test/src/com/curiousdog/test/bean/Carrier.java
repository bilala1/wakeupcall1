package com.curiousdog.test.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Carrier 
{
	private String carrierName ;
	private String contactName ;
	private String emailAddress ;
	private boolean noEmail ;
	private String phoneNumber ;
	private String faxNumber ;
	private String policyNumber ;
	private String address ;
	private Date effDate ;
	private Date expDate ;
	private boolean coverageAuto ;
	private boolean coverageBroker ;
	private boolean coverageCrime ;
	private boolean coverageDnO ;
	private boolean coverageEnO ;
	private boolean coverageEPLi ;
	private boolean coverageGL ;
	private boolean coveragePackage ;
	private boolean coverageProperty ;
	private boolean coverageUmbrella ;
	private boolean coverageWC ;
	private boolean coverageOther ;
	
	
	
	
	private List<CarrierLocation> locations ;
	
	public Carrier()
	{
		locations = new ArrayList<CarrierLocation>() ;
		noEmail = false ;
		coverageAuto	= false ;
		coverageBroker	= false ;
		coverageCrime	= false ;
		coverageDnO		= false ;
		coverageEnO		= false ;
		coverageEPLi	= false ;
		coverageGL		= false ;
		coveragePackage	= false ;
		coverageProperty= false ;
		coverageUmbrella= false ;
		coverageWC		= false ;
		coverageOther	= false ;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<CarrierLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<CarrierLocation> locations) {
		this.locations = locations;
	}
	

}
