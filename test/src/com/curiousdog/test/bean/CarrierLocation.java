package com.curiousdog.test.bean;

public class CarrierLocation 
{
	private String locationId ;
	private String locationName ;
	boolean locationCanEdit = false ;
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public boolean getLocationCanEdit() {
		return locationCanEdit;
	}
	public void setLocationCanEdit(boolean locationCanEdit) {
		this.locationCanEdit = locationCanEdit;
	}
	
	
}
