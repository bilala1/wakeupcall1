package com.curiousdog.test.bean;

import com.curiousdog.test.tracking.certificates.Vendor;

public class Certificate 
{
	private Location location = null ;
	private Vendor   vendor   = null ;
	private boolean locationCanView   = false ;
	private boolean locationCanEdit   = false ;
	private String  operationsType    = "" ;
	private String  certFileName      = "" ;
	private String  projectName       = "" ;
	private String  daysBeforeExp     = "" ;
	private boolean sendToVendor      = false ;
	private boolean sendToLocAdmin    = false ;
	private boolean sendToAccAdmin    = false ;
	private boolean sendToAddress     = false ;
	
	private boolean typeGenLiability  = false ;
	private boolean typeUmbrella      = false ;
	private boolean typeWorkComp      = false ;
	private boolean typeAutomotive    = false ;
	private boolean typeCrime         = false ;
	private boolean typeProperty      = false ;
	private boolean typeProfessional  = false ;
	private boolean typeBuildRisk     = false ;
	private boolean typeOther         = false ;
	private String  typeOtherText     = "" ;
	private String  expirationDate    = "" ;
	private String  remindUntilUpd    = "Yes" ;
	private String  id = "" ;
	
	public Certificate()
	{
		location = new Location() ;
		vendor   = new Vendor() ;
	}
	public void setId(String sId)
	{
		this.id = sId ;
	}
	public String getId()
	{
		return this.id ;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public boolean isLocationCanView() {
		return locationCanView;
	}
	public void setLocationCanView(boolean locationCanView) {
		this.locationCanView = locationCanView;
	}
	public boolean isLocationCanEdit() {
		return locationCanEdit;
	}
	public void setLocationCanEdit(boolean locationCanEdit) {
		this.locationCanEdit = locationCanEdit;
	}
	public String getOperationsType() {
		return operationsType;
	}
	public void setOperationsType(String operationsType) {
		this.operationsType = operationsType;
	}
	public String getCertFileName() {
		return certFileName;
	}
	public void setCertFileName(String certFileName) {
		this.certFileName = certFileName;
	}
	public String getDaysBeforeExp() {
		return daysBeforeExp;
	}
	public void setDaysBeforeExp(String daysBeforeExp) {
		this.daysBeforeExp = daysBeforeExp;
	}
	public boolean isSendToVendor() {
		return sendToVendor;
	}
	public void setSendToVendor(boolean sendToVendor) {
		this.sendToVendor = sendToVendor;
	}
	public boolean isSendToLocAdmin() {
		return sendToLocAdmin;
	}
	public void setSendToLocAdmin(boolean sendToLocAdmin) {
		this.sendToLocAdmin = sendToLocAdmin;
	}
	public boolean isSendToAccAdmin() {
		return sendToAccAdmin;
	}
	public void setSendToAccAdmin(boolean sendToAccAdmin) {
		this.sendToAccAdmin = sendToAccAdmin;
	}
	public boolean isSendToAddress() {
		return sendToAddress;
	}
	public void setSendToAddress(boolean sendToAddress) {
		this.sendToAddress = sendToAddress;
	}
	public boolean isTypeGenLiability() {
		return typeGenLiability;
	}
	public void setTypeGenLiability(boolean typeGenLiability) {
		this.typeGenLiability = typeGenLiability;
	}
	public boolean isTypeUmbrella() {
		return typeUmbrella;
	}
	public void setTypeUmbrella(boolean typeUmbrella) {
		this.typeUmbrella = typeUmbrella;
	}
	public boolean isTypeWorkComp() {
		return typeWorkComp;
	}
	public void setTypeWorkComp(boolean typeWorkComp) {
		this.typeWorkComp = typeWorkComp;
	}
	public boolean isTypeAutomotive() {
		return typeAutomotive;
	}
	public void setTypeAutomotive(boolean typeAutomotive) {
		this.typeAutomotive = typeAutomotive;
	}
	public boolean isTypeCrime() {
		return typeCrime;
	}
	public void setTypeCrime(boolean typeCrime) {
		this.typeCrime = typeCrime;
	}
	public boolean isTypeProperty() {
		return typeProperty;
	}
	public void setTypeProperty(boolean typeProperty) {
		this.typeProperty = typeProperty;
	}
	public boolean isTypeProfessional() {
		return typeProfessional;
	}
	public void setTypeProfessional(boolean typeProfessional) {
		this.typeProfessional = typeProfessional;
	}
	public boolean isTypeBuildRisk() {
		return typeBuildRisk;
	}
	public void setTypeBuildRisk(boolean typeBuildRisk) {
		this.typeBuildRisk = typeBuildRisk;
	}
	public boolean isTypeOther() {
		return typeOther;
	}
	public void setTypeOther(boolean typeOther) {
		this.typeOther = typeOther;
	}
	public String getTypeOtherText() {
		return typeOtherText;
	}
	public void setTypeOtherText(String typeOtherText) {
		this.typeOtherText = typeOtherText;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getRemindUntilUpd() {
		return remindUntilUpd;
	}
	public void setRemindUntilUpd(String remindUntilUpd) {
		this.remindUntilUpd = remindUntilUpd;
	}
	
	
	
	
}
