package com.curiousdog.test.bean;

import java.util.Date;

public class Claim 
{
	private Location location ;
	private Carrier  carrier ;
	private boolean  sendFax ;
	private boolean  sendEmail ;
	
	private Recipient recipient ;
	private Date      accidentDate ;
	private Date      notifiedDate ;
	private String    status ;
	private String    claimType ;
	private boolean   typeWC ;
	private boolean   typeGL ;
	private boolean   typeAuto ;
	private boolean   typeYoPro ;
	private boolean   typeOther ;
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Carrier getCarrier() {
		return carrier;
	}
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
	public boolean isSendFax() {
		return sendFax;
	}
	public void setSendFax(boolean sendFax) {
		this.sendFax = sendFax;
	}
	public boolean isSendEmail() {
		return sendEmail;
	}
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}
	public Recipient getRecipient() {
		return recipient;
	}
	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}
	public Date getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}
	public Date getNotifiedDate() {
		return notifiedDate;
	}
	public void setNotifiedDate(Date notifiedDate) {
		this.notifiedDate = notifiedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public boolean isTypeWC() {
		return typeWC;
	}
	public void setTypeWC(boolean typeWC) {
		this.typeWC = typeWC;
	}
	public boolean isTypeGL() {
		return typeGL;
	}
	public void setTypeGL(boolean typeGL) {
		this.typeGL = typeGL;
	}
	public boolean isTypeAuto() {
		return typeAuto;
	}
	public void setTypeAuto(boolean typeAuto) {
		this.typeAuto = typeAuto;
	}
	public boolean isTypeYoPro() {
		return typeYoPro;
	}
	public void setTypeYoPro(boolean typeYoPro) {
		this.typeYoPro = typeYoPro;
	}
	public boolean isTypeOther() {
		return typeOther;
	}
	public void setTypeOther(boolean typeOther) {
		this.typeOther = typeOther;
	}
	
	
}
