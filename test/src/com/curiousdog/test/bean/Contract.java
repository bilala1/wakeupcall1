package com.curiousdog.test.bean;

import java.util.Date;

public class Contract {

    public enum Status {
        Active,
        Inactive,
        Expired,
        MonthToMonth
    }

    private int id;
    private String contractedPartyName;
    private String contactName;
    private String contactPhone;
    private String contactEmail;
    private Date effectiveDate;
    private Date expirationDate;
    private String description;
    private String additionalDetails;
    private Status status;
    private Status statusTransitionTo;
    private boolean notifyExpiration;
    private int notifyLeadDays;
    private boolean locationCanView;
    private boolean locationCanEdit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContractedPartyName() {
        return contractedPartyName;
    }

    public void setContractedPartyName(String contractedPartyName) {
        this.contractedPartyName = contractedPartyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(String additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatusTransitionTo() {
        return statusTransitionTo;
    }

    public void setStatusTransitionTo(Status statusTransitionTo) {
        this.statusTransitionTo = statusTransitionTo;
    }

    public boolean isNotifyExpiration() {
        return notifyExpiration;
    }

    public void setNotifyExpiration(boolean notifyExpiration) {
        this.notifyExpiration = notifyExpiration;
    }

    public int getNotifyLeadDays() {
        return notifyLeadDays;
    }

    public void setNotifyLeadDays(int notifyLeadDays) {
        this.notifyLeadDays = notifyLeadDays;
    }

    public boolean isLocationCanView() {
        return locationCanView;
    }

    public void setLocationCanView(boolean locationCanView) {
        this.locationCanView = locationCanView;
    }

    public boolean isLocationCanEdit() {
        return locationCanEdit;
    }

    public void setLocationCanEdit(boolean locationCanEdit) {
        this.locationCanEdit = locationCanEdit;
    }
}