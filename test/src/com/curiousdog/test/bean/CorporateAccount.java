package com.curiousdog.test.bean;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class CorporateAccount 
{
	private String trial_code ;
	private String corporations_name_autocomplete ; // (by name)
	private String corporations_address ;
	private String corporations_city ;
	private String corporations_state ;
	private String corporations_zip ;
	
	private String members_firstname ;
	private String members_lastname ;
	private String members_title ;
	private String members_phone ;
	private String members_fax ;
	private String members_email ;
	private String members_email2 ;
	private String members_password ;
	private String members_password2 ;
	private boolean corporateOfficeSeparateYes ;
	private boolean corporateOfficeSeparateNo ;
	private String corporate_locations  ;  //number of locations
	
	//Add locations now
	private boolean createLocationYes ;
	private boolean createLocationNo ;

	public CorporateAccount()
	{
		super() ;
	}
	public CorporateAccount(ResourceBundle bundle, String configString)
	{
		this();
		populate(bundle, configString) ;
	}
	public void populate(ResourceBundle bundle, String configString)
	{
		this.trial_code = TestUtils.getProperty(bundle, configString + ".trial_code") ;
		this.corporations_name_autocomplete = TestUtils.getProperty(bundle, configString + ".corporations_name_autocomplete") ;
		this.corporations_address 			= TestUtils.getProperty(bundle, configString + ".corporations_address") ;
		this.corporations_city 				= TestUtils.getProperty(bundle, configString + ".corporations_city") ;
		this.corporations_state 			= TestUtils.getProperty(bundle, configString + ".corporations_state") ;
		this.corporations_zip 				= TestUtils.getProperty(bundle, configString + ".corporations_zip") ;
		this.members_firstname 				= TestUtils.getProperty(bundle, configString + ".members_firstname") ;
		this.members_lastname 				= TestUtils.getProperty(bundle, configString + ".members_lastname") ;
		this.members_title 					= TestUtils.getProperty(bundle, configString + ".members_title") ;
		this.members_phone 					= TestUtils.getProperty(bundle, configString + ".members_phone") ;
		this.members_fax 					= TestUtils.getProperty(bundle, configString + ".members_fax") ;
		this.members_email 					= TestUtils.getProperty(bundle, configString + ".members_email") ;
		this.members_email2 				= TestUtils.getProperty(bundle, configString + ".members_email2") ;
		this.members_password 				= TestUtils.getProperty(bundle, configString + ".members_password") ;
		this.members_password2 				= TestUtils.getProperty(bundle, configString + ".members_password2") ;
		this.corporate_locations 			= TestUtils.getProperty(bundle, configString + ".corporate_locations") ;
		
		String cos1 = TestUtils.getProperty(bundle, configString + ".separate_0") ;
		this.corporateOfficeSeparateYes = ("Y".equalsIgnoreCase(cos1)) ;
		String cos2 = TestUtils.getProperty(bundle, configString + ".separate_1") ;
		this.corporateOfficeSeparateNo  = ("Y".equalsIgnoreCase(cos2)) ;
		
		String cos3 = TestUtils.getProperty(bundle, configString + ".other_loc_0") ;
		this.createLocationYes = ("Y".equalsIgnoreCase(cos3)) ;
		String cos4 = TestUtils.getProperty(bundle, configString + ".other_loc_1") ;
		this.createLocationNo  = ("Y".equalsIgnoreCase(cos4)) ;
		
	}

	
	public boolean isCorporateOfficeSeparateYes() {
		return corporateOfficeSeparateYes;
	}
	public void setCorporateOfficeSeparateYes(boolean corporateOfficeSeparateYes) {
		this.corporateOfficeSeparateYes = corporateOfficeSeparateYes;
	}
	public boolean isCorporateOfficeSeparateNo() {
		return corporateOfficeSeparateNo;
	}
	public void setCorporateOfficeSeparateNo(boolean corporateOfficeSeparateNo) {
		this.corporateOfficeSeparateNo = corporateOfficeSeparateNo;
	}
	public boolean isCreateLocationYes() {
		return createLocationYes;
	}
	public void setCreateLocationYes(boolean createLocationYes) {
		this.createLocationYes = createLocationYes;
	}
	public boolean isCreateLocationNo() {
		return createLocationNo;
	}
	public void setCreateLocationNo(boolean createLocationNo) {
		this.createLocationNo = createLocationNo;
	}
	public String getTrial_code() {
		return trial_code;
	}

	public void setTrial_code(String trial_code) {
		this.trial_code = trial_code;
	}

	public String getCorporations_name_autocomplete() {
		return corporations_name_autocomplete;
	}

	public void setCorporations_name_autocomplete(String corporations_name_autocomplete) {
		this.corporations_name_autocomplete = corporations_name_autocomplete;
	}

	public String getCorporations_address() {
		return corporations_address;
	}

	public void setCorporations_address(String corporations_address) {
		this.corporations_address = corporations_address;
	}

	public String getCorporations_city() {
		return corporations_city;
	}

	public void setCorporations_city(String corporations_city) {
		this.corporations_city = corporations_city;
	}

	public String getCorporations_state() {
		return corporations_state;
	}

	public void setCorporations_state(String corporations_state) {
		this.corporations_state = corporations_state;
	}

	public String getCorporations_zip() {
		return corporations_zip;
	}

	public void setCorporations_zip(String corporations_zip) {
		this.corporations_zip = corporations_zip;
	}

	public String getMembers_firstname() {
		return members_firstname;
	}

	public void setMembers_firstname(String members_firstname) {
		this.members_firstname = members_firstname;
	}

	public String getMembers_lastname() {
		return members_lastname;
	}

	public void setMembers_lastname(String members_lastname) {
		this.members_lastname = members_lastname;
	}

	public String getMembers_title() {
		return members_title;
	}

	public void setMembers_title(String members_title) {
		this.members_title = members_title;
	}

	public String getMembers_phone() {
		return members_phone;
	}

	public void setMembers_phone(String members_phone) {
		this.members_phone = members_phone;
	}

	public String getMembers_fax() {
		return members_fax;
	}

	public void setMembers_fax(String members_fax) {
		this.members_fax = members_fax;
	}

	public String getMembers_email() {
		return members_email;
	}

	public void setMembers_email(String members_email) {
		this.members_email = members_email;
	}

	public String getMembers_email2() {
		return members_email2;
	}

	public void setMembers_email2(String members_email2) {
		this.members_email2 = members_email2;
	}

	public String getMembers_password() {
		return members_password;
	}

	public void setMembers_password(String members_password) {
		this.members_password = members_password;
	}

	public String getMembers_password2() {
		return members_password2;
	}

	public void setMembers_password2(String members_password2) {
		this.members_password2 = members_password2;
	}

	public String getCorporate_locations() {
		return corporate_locations;
	}

	public void setCorporate_locations(String corporate_locations) {
		this.corporate_locations = corporate_locations;
	}

	
}
