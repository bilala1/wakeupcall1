package com.curiousdog.test.bean;

public class Folder 
{
	private String folderId ;
	private String folderName ;
	
	
	public String getFolderId() {
		return folderId;
	}
	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	
	
}
