package com.curiousdog.test.bean;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class IndividualAccount 
{
	private String property_type ;
	
	private String hotels_name_autocomplete ; 
	private String hotels_num_rooms ;
	private String hotels_address ;
	private String hotels_city ;
	private String hotels_state ;
	private String hotels_zip ;

	private String hotels_members_firstname ;
	private String hotels_members_lastname ;
	private String hotels_members_title ;
	private String hotels_members_phone ;
	private String hotels_members_fax ;
	private String hotels_members_email ;
	private String hotels_members_email2 ;
	private String hotels_pass ;
	private String hotels_pass2 ;
	
	public IndividualAccount()
	{
		super() ;
	}
	public IndividualAccount(ResourceBundle bundle, String configString)
	{
		this() ;
		populate(bundle, configString) ;
	}
	
	public void populate(ResourceBundle bundle, String configString)
	{
		this.property_type = TestUtils.getProperty(bundle, configString + ".ind_type") ;
		this.hotels_name_autocomplete = TestUtils.getProperty(bundle, configString + ".hotels_name_autocomplete") ; 
		this.hotels_num_rooms = TestUtils.getProperty(bundle, configString + ".hotels_num_rooms");
		this.hotels_address = TestUtils.getProperty(bundle, configString + ".hotels_address") ;
		this.hotels_city  = TestUtils.getProperty(bundle, configString + ".hotels_city") ;
		this.hotels_state = TestUtils.getProperty(bundle, configString + ".hotels_state") ;
		this.hotels_zip = TestUtils.getProperty(bundle, configString + ".hotels_zip") ;

		this.hotels_members_firstname = TestUtils.getProperty(bundle, configString + ".hotels_members_firstname") ;
		this.hotels_members_lastname = TestUtils.getProperty(bundle, configString + ".hotels_members_lastname") ;
		this.hotels_members_title = TestUtils.getProperty(bundle, configString + ".hotels_members_title") ;
		this.hotels_members_phone = TestUtils.getProperty(bundle, configString + ".hotels_members_phone") ;
		this.hotels_members_fax = TestUtils.getProperty(bundle, configString + ".hotels_members_fax") ;
		this.hotels_members_email = TestUtils.getProperty(bundle, configString + ".hotels_members_email") ;
		this.hotels_members_email2 = TestUtils.getProperty(bundle, configString + ".hotels_members_email2") ;
		this.hotels_pass = TestUtils.getProperty(bundle, configString + ".hotels_pass") ;
		this.hotels_pass2 = TestUtils.getProperty(bundle, configString + ".hotels_pass2") ;
	}
	
	public String getProperty_type() {
		return property_type;
	}
	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}
	public String getHotels_name_autocomplete() {
		return hotels_name_autocomplete;
	}
	public void setHotels_name_autocomplete(String hotels_name_autocomplete) {
		this.hotels_name_autocomplete = hotels_name_autocomplete;
	}
	public String getHotels_num_rooms() {
		return hotels_num_rooms;
	}
	public void setHotels_num_rooms(String hotels_num_rooms) {
		this.hotels_num_rooms = hotels_num_rooms;
	}
	public String getHotels_address() {
		return hotels_address;
	}
	public void setHotels_address(String hotels_address) {
		this.hotels_address = hotels_address;
	}
	public String getHotels_city() {
		return hotels_city;
	}
	public void setHotels_city(String hotels_city) {
		this.hotels_city = hotels_city;
	}
	public String getHotels_state() {
		return hotels_state;
	}
	public void setHotels_state(String hotels_state) {
		this.hotels_state = hotels_state;
	}
	public String getHotels_zip() {
		return hotels_zip;
	}
	public void setHotels_zip(String hotels_zip) {
		this.hotels_zip = hotels_zip;
	}
	public String getHotels_members_firstname() {
		return hotels_members_firstname;
	}
	public void setHotels_members_firstname(String hotels_members_firstname) {
		this.hotels_members_firstname = hotels_members_firstname;
	}
	public String getHotels_members_lastname() {
		return hotels_members_lastname;
	}
	public void setHotels_members_lastname(String hotels_members_lastname) {
		this.hotels_members_lastname = hotels_members_lastname;
	}
	public String getHotels_members_title() {
		return hotels_members_title;
	}
	public void setHotels_members_title(String hotels_members_title) {
		this.hotels_members_title = hotels_members_title;
	}
	public String getHotels_members_phone() {
		return hotels_members_phone;
	}
	public void setHotels_members_phone(String hotels_members_phone) {
		this.hotels_members_phone = hotels_members_phone;
	}
	public String getHotels_members_fax() {
		return hotels_members_fax;
	}
	public void setHotels_members_fax(String hotels_members_fax) {
		this.hotels_members_fax = hotels_members_fax;
	}
	public String getHotels_members_email() {
		return hotels_members_email;
	}
	public void setHotels_members_email(String hotels_members_email) {
		this.hotels_members_email = hotels_members_email;
	}
	public String getHotels_members_email2() {
		return hotels_members_email2;
	}
	public void setHotels_members_email2(String hotels_members_email2) {
		this.hotels_members_email2 = hotels_members_email2;
	}
	public String getHotels_pass() {
		return hotels_pass;
	}
	public void setHotels_pass(String hotels_pass) {
		this.hotels_pass = hotels_pass;
	}
	public String getHotels_pass2() {
		return hotels_pass2;
	}
	public void setHotels_pass2(String hotels_pass2) {
		this.hotels_pass2 = hotels_pass2;
	}

	
}
