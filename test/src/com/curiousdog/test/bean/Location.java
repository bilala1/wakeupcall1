package com.curiousdog.test.bean;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Location 
{
	private String id ;
	private String locationType ;
	private String name ;
	private String address ;
	private String city ;
	private String state ;
	private String zip ;
	private String phone ;
	private String fax ;
	private String noOfRooms ;
	private String discount_code ;
	private String franchise_code ;
	private boolean edit_claims_emails_permission ;
	private boolean edit_certs_emails_permission ;
	
	public Location()
	{
		super() ;
	}
	public Location(ResourceBundle bundle, String configString)
	{
		this() ;
		this.locationType = TestUtils.getProperty(bundle, configString + ".licensed_locations_full_type") ;
		this.name = TestUtils.getProperty(bundle, configString + ".licensed_locations_name") ;
		this.address = TestUtils.getProperty(bundle, configString + ".licensed_locations_address") ;
		this.city = TestUtils.getProperty(bundle, configString + ".licensed_locations_city") ;
		this.state = TestUtils.getProperty(bundle, configString + ".licensed_locations_state") ;
		this.zip = TestUtils.getProperty(bundle, configString + ".licensed_locations_zip") ;
		this.phone = TestUtils.getProperty(bundle, configString + ".licensed_locations_phone") ;
		this.fax = TestUtils.getProperty(bundle, configString + ".licensed_locations_fax") ;
		this.noOfRooms = TestUtils.getProperty(bundle, configString + ".hotels_num_rooms") ;
		this.discount_code = TestUtils.getProperty(bundle, configString + ".discount_code") ;
		this.franchise_code = TestUtils.getProperty(bundle, configString + ".franchise_code") ;
		this.edit_certs_emails_permission = ( "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".edit_certs_emails_permission")));
		this.edit_claims_emails_permission = ( "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".edit_claims_emails_permission")));
	}
	
	
	public String getDiscount_code() {
		return discount_code;
	}
	public void setDiscount_code(String discount_code) {
		this.discount_code = discount_code;
	}
	public String getFranchise_code() {
		return franchise_code;
	}
	public void setFranchise_code(String franchise_code) {
		this.franchise_code = franchise_code;
	}
	public boolean isEdit_claims_emails_permission() {
		return edit_claims_emails_permission;
	}
	public void setEdit_claims_emails_permission(boolean edit_claims_emails_permission) {
		this.edit_claims_emails_permission = edit_claims_emails_permission;
	}
	public boolean isEdit_certs_emails_permission() {
		return edit_certs_emails_permission;
	}
	public void setEdit_certs_emails_permission(boolean edit_certs_emails_permission) {
		this.edit_certs_emails_permission = edit_certs_emails_permission;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getNoOfRooms() {
		return noOfRooms;
	}
	public void setNoOfRooms(String noOfRooms) {
		this.noOfRooms = noOfRooms;
	}
	
}
