package com.curiousdog.test.bean;

public class MyFile 
{
	private String name ;
	private String path ;
	private Folder folder ;
	private boolean isShared ;
	private boolean submitToLibrary ;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Folder getFolder() {
		return folder;
	}
	public void setFolder(Folder folder) {
		this.folder = folder;
	}
	public boolean isShared() {
		return isShared;
	}
	public void setShared(boolean isShared) {
		this.isShared = isShared;
	}
	public boolean isSubmitToLibrary() {
		return submitToLibrary;
	}
	public void setSubmitToLibrary(boolean submitToLibrary) {
		this.submitToLibrary = submitToLibrary;
	}
	
	
	
}
