package com.curiousdog.test.bean;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class PaymentInfo 
{
	private boolean same_address ;
	
	private String members_billing_addr1 ;
	private String members_billing_addr2 ;
	private String members_billing_city ;
	private String members_billing_state ;
	private String members_billing_zip ;

	private String members_card_type ;
	private String members_card_name ;
	private String members_card_num;
	private String members_card_expire_month ;
	private String members_card_expire_year ;
	private String security_code ;
	private String discount_code ;
	private String franchises_code ;
	
	public PaymentInfo()
	{
		super() ;
	}
	
	public PaymentInfo(ResourceBundle bundle, String configString)
	{
		this();
		
		this.same_address		   = ("Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".same_address"))) ;
		this.members_billing_addr1 = TestUtils.getProperty(bundle, configString + ".members_billing_addr1") ;
		this.members_billing_addr2 = TestUtils.getProperty(bundle, configString + ".members_billing_addr2") ;
		this.members_billing_city  = TestUtils.getProperty(bundle, configString + ".members_billing_city") ;
		this.members_billing_state = TestUtils.getProperty(bundle, configString + ".members_billing_state") ;
		this.members_billing_zip   = TestUtils.getProperty(bundle, configString + ".members_billing_zip") ;
		
		this.members_card_type 			= TestUtils.getProperty(bundle, configString + ".members_card_type");
		this.members_card_name 			= TestUtils.getProperty(bundle, configString + ".members_card_name");
		this.members_card_num  			= TestUtils.getProperty(bundle, configString + ".members_card_num");
		this.members_card_expire_month 	= TestUtils.getProperty(bundle, configString + ".members_card_expire_month");
		this.members_card_expire_year 	= TestUtils.getProperty(bundle, configString + ".members_card_expire_year");
		this.security_code 				= TestUtils.getProperty(bundle, configString + ".security_code");
		this.discount_code 				= TestUtils.getProperty(bundle, configString + ".discount_code");
		this.franchises_code 			= TestUtils.getProperty(bundle, configString + ".franchises_code");
	}
	
	
	
	public boolean isSame_address() {
		return same_address;
	}
	public void setSame_address(boolean same_address) {
		this.same_address = same_address;
	}
	public String getMembers_billing_addr1() {
		return members_billing_addr1;
	}
	public void setMembers_billing_addr1(String members_billing_addr1) {
		this.members_billing_addr1 = members_billing_addr1;
	}
	public String getMembers_billing_addr2() {
		return members_billing_addr2;
	}
	public void setMembers_billing_addr2(String members_billing_addr2) {
		this.members_billing_addr2 = members_billing_addr2;
	}
	public String getMembers_billing_city() {
		return members_billing_city;
	}
	public void setMembers_billing_city(String members_billing_city) {
		this.members_billing_city = members_billing_city;
	}
	public String getMembers_billing_state() {
		return members_billing_state;
	}
	public void setMembers_billing_state(String members_billing_state) {
		this.members_billing_state = members_billing_state;
	}
	public String getMembers_billing_zip() {
		return members_billing_zip;
	}
	public void setMembers_billing_zip(String members_billing_zip) {
		this.members_billing_zip = members_billing_zip;
	}
	public String getMembers_card_type() {
		return members_card_type;
	}
	public void setMembers_card_type(String members_card_type) {
		this.members_card_type = members_card_type;
	}
	public String getMembers_card_name() {
		return members_card_name;
	}
	public void setMembers_card_name(String members_card_name) {
		this.members_card_name = members_card_name;
	}
	public String getMembers_card_num() {
		return members_card_num;
	}
	public void setMembers_card_num(String members_card_num) {
		this.members_card_num = members_card_num;
	}
	public String getMembers_card_expire_month() {
		return members_card_expire_month;
	}
	public void setMembers_card_expire_month(String members_card_expire_month) {
		this.members_card_expire_month = members_card_expire_month;
	}
	public String getMembers_card_expire_year() {
		return members_card_expire_year;
	}
	public void setMembers_card_expire_year(String members_card_expire_year) {
		this.members_card_expire_year = members_card_expire_year;
	}
	public String getSecurity_code() {
		return security_code;
	}
	public void setSecurity_code(String security_code) {
		this.security_code = security_code;
	}
	public String getDiscount_code() {
		return discount_code;
	}
	public void setDiscount_code(String discount_code) {
		this.discount_code = discount_code;
	}
	public String getFranchises_code() {
		return franchises_code;
	}
	public void setFranchises_code(String franchises_code) {
		this.franchises_code = franchises_code;
	}

	
}
