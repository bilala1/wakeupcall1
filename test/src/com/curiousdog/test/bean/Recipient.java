package com.curiousdog.test.bean;

import java.util.ArrayList;
import java.util.List;

public class Recipient 
{
	private String carrierName ;
	private String contactName ;
	private String emailAddress ;
	
	private List<CarrierLocation> locations ;
	
	public Recipient()
	{
		locations = new ArrayList<CarrierLocation>() ;
		
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<CarrierLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<CarrierLocation> locations) {
		this.locations = locations;
	}
	
	
}
