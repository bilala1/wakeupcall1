package com.curiousdog.test.bean;

import java.util.ArrayList;
import java.util.List;

public class User 
{
	private String id ;
	private String firstName ;
	private String lastName ;
	private String position ;
	private String email ;
	private String repeatEmail ;
	private String password ;
	private String repeatPassword ;
	
	private boolean isAdmin ;
	private boolean manageAdmins ;
	private boolean hasEmploymentLaw ;
	private boolean hasHR ;
	private boolean hasMessageForum ;
	private boolean hasShareDocuments ;
	
	private List<UserLocationsPermissions> locationPermissions = null ;
	
	public User()
	{
		locationPermissions = new ArrayList<UserLocationsPermissions> () ;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRepeatEmail() {
		return repeatEmail;
	}
	public void setRepeatEmail(String repeatEmail) {
		this.repeatEmail = repeatEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRepeatPassword() {
		return repeatPassword;
	}
	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public boolean isManageAdmins() {
		return manageAdmins;
	}
	public void setManageAdmins(boolean manageAdmins) {
		this.manageAdmins = manageAdmins;
	}
	public boolean isHasEmploymentLaw() {
		return hasEmploymentLaw;
	}
	public void setHasEmploymentLaw(boolean hasEmploymentLaw) {
		this.hasEmploymentLaw = hasEmploymentLaw;
	}
	public boolean isHasHR() {
		return hasHR;
	}
	public void setHasHR(boolean hasHR) {
		this.hasHR = hasHR;
	}
	public boolean isHasMessageForum() {
		return hasMessageForum;
	}
	public void setHasMessageForum(boolean hasMessageForum) {
		this.hasMessageForum = hasMessageForum;
	}
	public boolean isHasShareDocuments() {
		return hasShareDocuments;
	}
	public void setHasShareDocuments(boolean hasShareDocuments) {
		this.hasShareDocuments = hasShareDocuments;
	}
	
	
	
}
