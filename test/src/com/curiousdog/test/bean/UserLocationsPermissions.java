package com.curiousdog.test.bean;

public class UserLocationsPermissions 
{
	private Location location ;
	
	private boolean isLocationAdmin ;
	private boolean worksHere ;
	private boolean hasCertificateTracking ;
	private boolean hasClaimsReporting ;
	private boolean hasContractsManagement ;
	private boolean hasManageUsers ;
	private boolean hasEditLocation ;
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public boolean isLocationAdmin() {
		return isLocationAdmin;
	}
	public void setLocationAdmin(boolean isLocationAdmin) {
		this.isLocationAdmin = isLocationAdmin;
	}
	public boolean isWorksHere() {
		return worksHere;
	}
	public void setWorksHere(boolean worksHere) {
		this.worksHere = worksHere;
	}
	public boolean isHasCertificateTracking() {
		return hasCertificateTracking;
	}
	public void setHasCertificateTracking(boolean hasCertificateTracking) {
		this.hasCertificateTracking = hasCertificateTracking;
	}
	public boolean isHasClaimsReporting() {
		return hasClaimsReporting;
	}
	public void setHasClaimsReporting(boolean hasClaimsReporting) {
		this.hasClaimsReporting = hasClaimsReporting;
	}
	public boolean isHasContractsManagement() {
		return hasContractsManagement;
	}
	public void setHasContractsManagement(boolean hasContractsManagement) {
		this.hasContractsManagement = hasContractsManagement;
	}
	public boolean isHasManageUsers() {
		return hasManageUsers;
	}
	public void setHasManageUsers(boolean hasManageUsers) {
		this.hasManageUsers = hasManageUsers;
	}
	public boolean isHasEditLocation() {
		return hasEditLocation;
	}
	public void setHasEditLocation(boolean hasEditLocation) {
		this.hasEditLocation = hasEditLocation;
	}
	
	
}
