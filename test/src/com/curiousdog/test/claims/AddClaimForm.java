package com.curiousdog.test.claims;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Claim;
import com.curiousdog.test.common.WebForm;

public class AddClaimForm extends WebForm
{
	public AddClaimForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Claim vendor)
	{

	}

	public void navigateToMe()
	{
		driver.findElement(By.partialLinkText("My WAKEUP")).click();
		driver.findElement(By.linkText("Claims Management")).click();
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Claim")).click();
	}
	
	public void submitToCarrierForm()
	{
		this.driver.findElement(By.id("saveAndSubmit")).click() ;
	}
	
	public void saveForRecordsForm()
	{
		this.driver.findElement(By.id("saveOnly")).click() ;
	}

	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Claims List")).click() ;
	}
	
	
}
