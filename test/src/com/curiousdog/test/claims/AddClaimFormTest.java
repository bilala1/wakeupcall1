package com.curiousdog.test.claims;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class AddClaimFormTest 
{
	static AddClaimForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		driver = new FirefoxDriver() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddClaimForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitToCarrierTest()
	{
		form.navigateToMe();
		form.submitToCarrierForm();
		
	}
	@Test
	public void EmptyFormSaveForRecordsTest()
	{
		form.navigateToMe();
		form.saveForRecordsForm();
		
	}

	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	
}
