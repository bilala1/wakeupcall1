package com.curiousdog.test.claims;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Claim;
import com.curiousdog.test.common.WebForm;

public class AddRecipientForm extends WebForm
{
	public AddRecipientForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Claim vendor)
	{

	}

	public void navigateToMe()
	{
		driver.findElement(By.partialLinkText("My WAKEUP")).click();
		driver.findElement(By.linkText("Claims Management")).click();
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Carrier/Recipient")).click();
	}
	public void submitForm()
	{
		this.driver.findElement(By.id("save_button")).submit() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Carrier/Recipient List")).click() ;
	}

}
