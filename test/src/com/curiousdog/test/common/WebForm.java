package com.curiousdog.test.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.curiousdog.test.utils.TestUtils;


public abstract class WebForm
{
	protected WebDriver driver ;//= new FirefoxDriver();
	protected String    appHomePage ;

	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/d/yyyy");
	ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	
	public WebForm(WebDriver drv)
	{
		super() ;
		this.driver = drv ;
		this.appHomePage = bundle.getString("HOME.PAGE") ;
 	}
	
	public WebForm(WebDriver drv, String url)
	{
		this(drv) ;
		this.driver.get(url);
	}
	
	//public abstract void navigateTo() ;
	
	//public abstract void submitForm() ;
	
	//public abstract void cancelForm() ;
	
	protected String getConfigValue(String key)
	{
		String result = null ;
		try
		{
			result = bundle.getString(key) ;
		}
		catch(Exception ex)
		{
			//ignoring exception
		}
		return (result == null) ? "" : result ;
	}
    protected String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/d/yyyy");
        return dateFormat.format(date);
    }

    protected void navigate(String path) {
        driver.get(this.appHomePage + path);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains(path));
    }

    protected void navigateToKey(String urlKey) {
        navigate(bundle.getString(urlKey));
    }

    public void assertNavigate(String path) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            wait.until(ExpectedConditions.urlContains(path));
        } catch (Exception e) {
        }
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(path), "Should have navigated to path: " + path + '\n' + "Actual: " + currentUrl);
    }

    protected void assertNavigateToKey(String urlKey) {
        assertNavigate(bundle.getString(urlKey));
    }

    public void assertTextExists(String text, String error) {
    	//WebDriverWait wait = new WebDriverWait(driver, 15) ;
        WebElement body = driver.findElement(By.tagName("body"));
        String bodyText = body.getText();
        if (!bodyText.contains(text)) {
            Assert.fail("Text not found: " + text + '\n' + error);
        }
    }

    protected WebElement safeFindElementById(String id) {
        try {
            return driver.findElement(By.id(id));
        } catch (NoSuchElementException e) {
            return null;
        }
    }
    protected WebElement safeFindElementByName(String name) {
        try {
            return driver.findElement(By.name(name));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    protected void setTextbox(String id, String text) {
        //setTextbox(safeFindElementById(id), text);
    	WebDriverWait wait = new WebDriverWait(driver, 15) ;
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id))).clear() ;
    	driver.findElement(By.id(id)).sendKeys(text);
    }
    protected void setTextbox(String id, Date date)
    {
    	setTextbox(id, dateFormat.format(date)) ;
    }

    protected void setTextboxByName(String txtBoxName, String text)
    {
    	setTextbox(safeFindElementByName(txtBoxName), text) ;
    }
    
    protected void setTextbox(WebElement element, String text) {
        if (element == null) {
            return;
        }
        element.clear();
        element.sendKeys(text);
    }

    protected void setCheckbox(String id, boolean checked) 
    {
		WebElement checkBox = driver.findElement(By.id(id)) ;
    	if ( checked )
    	{
    		if ( !checkBox.isSelected() ) checkBox.click();
    	}
    	else
    	{
    		if ( checkBox.isSelected() ) checkBox.click();
    	}
    }
    protected void setCheckboxByName(String name, boolean checked) 
    {
		WebElement checkBox = driver.findElement(By.name(name)) ;
    	if ( checked )
    	{
    		if ( !checkBox.isSelected() ) checkBox.click();
    	}
    	else
    	{
    		if ( checkBox.isSelected() ) checkBox.click();
    	}
    }

    protected void setCheckbox(WebElement element, boolean checked) {
        if (element == null) {
            return;
        }
        boolean elementChecked = "checked".equals(element.getAttribute("checked"));
        if (elementChecked != checked) {
            element.click();
        }
    }

    protected void setDropdown(String id, String value) {
        //setDropdown(safeFindElementById(id), value);
    	Select select = new Select(driver.findElement(By.id(id))) ;
    	select.selectByValue(value);
    }

    protected void setDropdown(WebElement element, Object value) {
        if (element == null) {
            return;
        }
        Select select = new Select(element);
        select.selectByValue(value.toString());
    }
    
    public void login(String user, String password)
    {
    	//driver.get(this.appHomePage + "/index.php");
    	driver.get("http://staging.wakeupcall.net/index.php") ;
    	driver.findElement(By.id("login_email")).clear(); 
    	driver.findElement(By.id("login_email")).sendKeys(user) ;
    	driver.findElement(By.id("login_password")).clear(); 
    	driver.findElement(By.id("login_password")).sendKeys(password) ;
    	
    	driver.findElement(By.id("login_email")).submit() ;
    	
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try
        {
	        wait.until(ExpectedConditions.or(
	                ExpectedConditions.urlContains("rss-news.php"),
	                ExpectedConditions.urlContains("video.php")));
        }
        catch(TimeoutException tex)
        {
        	System.out.println("Video screen did not show up. Skipping");
        }
    }
	
    public String getTextboxValue(String id)
    {
    	String result = "" ;
    	try
    	{
    		result = driver.findElement(By.id(id)).getAttribute("value") ;
    	}
    	catch(Exception ex) { }
    	return result ;
    }
    
    public String getSingleSelectChosenValue(String id)
    {
    	WebElement element = driver.findElement(By.id(id)) ;
    	if ( element != null )
    	{
    		Select select = new Select(element) ;
    		List<WebElement> allSelections = select.getAllSelectedOptions() ;
    		return ((WebElement)allSelections.get(0)).getAttribute("value") ;
    	}
    	return null ;
    }
    
    public List<String> getAllSelectChosenValue(String id)
    {
    	List<String> result = new ArrayList<String>() ;
    	WebElement element = driver.findElement(By.id(id)) ;
    	if ( element != null )
    	{
    		Select select = new Select(element) ;
    		List<WebElement> allSelections = select.getAllSelectedOptions() ;
    		for ( int i = 0 ; i < allSelections.size() ; i++ )
    		{
    			WebElement option = (WebElement)allSelections.get(i) ;
    			result.add(option.getAttribute("value"));
    		}
    		return result ;
    	}
    	return null ;
    }
    public void selectCalendarDate(String id, Date date)
    {
		Calendar calDate = new GregorianCalendar() ;
		calDate.setTime(date);
		String calYear = Integer.toString(calDate.get(Calendar.YEAR)) ;
		String calMonth= Integer.toString(calDate.get(Calendar.MONTH)) ;
		String calDay  = Integer.toString(calDate.get(Calendar.DAY_OF_MONTH)) ;
		//System.out.println("Passing " + calMonth + "/" + calDay + "/" + calYear + " to " + id);
		selectCalendarDate(id, calYear, calMonth, calDay) ;
		driver.switchTo().parentFrame() ;
    }
    
    public void selectCalendarDate(String id, String year, String month, String day)
    {
    	WebElement cal = driver.findElement(By.id(id)) ;
    	if (!( cal.isDisplayed() && cal.isEnabled() )) 
    	{
    		System.out.println("Calendar " + id + " is not enabled.");
    		return ;
    	}
    	cal.click();
    	//System.out.println("Found calendar and clicked");
    	driver.switchTo().activeElement() ;
		List<WebElement> elementsList =  driver.findElements(By.className("cal-select"));
		WebElement elm1 = elementsList.get(0);
	    setDropdown(elm1, month);
	    WebElement elm2 = elementsList.get(1);
	    setDropdown(elm2, year);
	    
	    //driver.switchTo().parentFrame() ;
	    
	    List<WebElement> dateWidgets = driver.findElements(By.className("cal-container"));
	    //System.out.println("Found widgets: " + dateWidgets.size());
	    for ( WebElement dateWidget: dateWidgets )
	    {
	    	if ( dateWidget.isDisplayed() && dateWidget.isEnabled() )
	    	{
	    			List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  
	    	        //comparing the text of cell with today's date and clicking it.
	    	        for (WebElement cell : columns)
	    	        {
	    	           if (cell.getText().equals(day))
	    	           {
	    	              cell.click();
	    	              break;
	    	           }
	    	        }
	    		break ;
	    	}
	    }
        driver.switchTo().parentFrame() ;
        
    }
    
    public void setFileChooser(String id, String filePath)
    {
    	if ( filePath == null || filePath.trim().length() == 0 ) return ;
    	try
    	{
    		WebElement fileElement = driver.findElement(By.id(id)) ;
    		fileElement.sendKeys(filePath);
    	}
    	catch(Exception ex)
    	{ 
    		return ; 
    	}
    }
    public void setFileChooserByName(String name, String filePath)
    {
    	if ( filePath == null || filePath.trim().length() == 0 ) return ;
    	try
    	{
    		WebElement fileElement = driver.findElement(By.name(name)) ;
    		fileElement.sendKeys(filePath);
    	}
    	catch(Exception ex)
    	{ 
    		return ; 
    	}
    }
    public void checkText(String text)
    {
    	//Locator locator = new Locator()
    	//WebDriverWait wait = new WebDriverWait(driver, 15) ;
    	//wait.until(ExpectedConditions.);
    	//new WebDriverWait(driver, 15).until(ExpectedConditions.waitForPageToLoad
    }
    public void clickButton(String id)
    {
    	new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.id(id))).click() ;
    }
    
    public void clickTextLink(String text)
    {
    	new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.linkText(text))).click() ;
    }
    
    public void logout()
    {
    	driver.findElement(By.id("logout")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlMatches(appHomePage)) ;
    }
    
    public void clearElement(String id)
    {
    	driver.findElement(By.id(id)).clear();
    }
    public void closeWindow()
    {
    	driver.close();
    }
    
    public void dialogOK()
    {
		driver.switchTo().activeElement() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		WebElement buttonOK = driver.findElement(By.cssSelector("input.btn.ok-btn")) ;
		buttonOK.click();			
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.switchTo().parentFrame() ;
    	
    }
    public void dialogCancel()
    {
		driver.switchTo().activeElement() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		WebElement buttonOK = driver.findElement(By.cssSelector("input.btn")) ;
		buttonOK.click();			
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.switchTo().parentFrame() ;
    	
    }
    
    public boolean clickActionMenuByLinkClass(String className, String url)
    {
    	List<WebElement> menus = driver.findElements(By.className("actionMenu") ) ;
    	for ( int i = 0 ; i < menus.size() ; i++ )
    	{
    		WebElement menu = menus.get(i) ;

    		WebElement item = menu.findElement(By.className(className));
    		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
    		if ( item.getAttribute("href").equalsIgnoreCase(url) ) 
    		{
    			TestUtils.mouseOver(driver, menu);
    			//menu.click();
    			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
    			item.click() ;
    			return true ;
    		}
    	}
    	return false ;
    }
    
    public boolean clickActionLink(String url)
    {
    	List<WebElement> menus = driver.findElements(By.className("actionMenu") ) ;
    	for ( int i = 0 ; i < menus.size() ; i++ )
    	{
    		WebElement menu = menus.get(i) ;
    		//System.out.println("Menu # " + i);
    		
    		List<WebElement> items = menu.findElements(By.tagName("a")) ;
    		for ( int j = 0 ; j < items.size() ; j++ )
    		{
    			WebElement item = items.get(j) ;
    			//System.out.println("LINK URL: " + item.getAttribute("href"));
	    		if ( item.getAttribute("href").equalsIgnoreCase(url) ) 
	    		{
	    			//System.out.println("FOUND!!!") ;
	    			
	    			TestUtils.mouseOver(driver, menu);
	    			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	    			item.click() ;
	    			return true ;
	    		}
    		}
    	}
    	return false ;
    }
    
}
