package com.curiousdog.test.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.curiousdog.test.utils.TestUtils;

public abstract class WebList 
{
	private WebDriver driver = null ;
	private String    appHomePage ;
	private String    objIdName   ;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/d/yyyy");
	ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	
	public WebList(WebDriver drv, String idName) 
	{
		this.driver = drv ;
		this.appHomePage = bundle.getString("HOME.PAGE") ;
		this.objIdName = idName ;
	}
	
	public void clickLink(String linkText, String idValue)
	{
		if ( linkText == null  || linkText.trim().length() == 0 ) return ;
		if ( idValue  == null  || idValue.trim().length()  == 0 ) return ;
		
		List<WebElement> list = this.driver.findElements(By.linkText(linkText)) ;
		if ( list == null || list.size() == 0 ) return ;
		
		String lineId = this.objIdName + "=" + idValue ;
		for ( int i = 0 ; i < list.size() ; i++ )
		{
			WebElement element = list.get(i) ;
			if ( (element.getAttribute("href")).indexOf( lineId ) > 0 )
			{
				element.click();
				break ;
			}
		}
	}
	
	public List<String> getListOfIds(String linkText)
	{
		List<String> ids = new ArrayList<String>() ;
		List<WebElement> list = this.driver.findElements(By.linkText(linkText)) ;
		for (int i = 0 ; i < list.size() ; i++ )
		{
			WebElement element = list.get(i) ;
			String linkUrl = element.getAttribute("href") ;
			String linkId  = TestUtils.getIdString(linkUrl, this.objIdName) ;
			ids.add(linkId) ;
		}
		return ids ;
	}
	
	public void mouseOver(WebElement ele)
	{
		Actions action = new Actions(driver);
		action.moveToElement(ele).build().perform();
	}
	public abstract void navigateTo() ;
	
}
