package com.curiousdog.test.contracts;

import com.curiousdog.test.bean.Contract;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

public class ContractsMultiLocationAdminTest extends ContractTestBase {

    @BeforeClass
    public void setup() {
        this.login("sue+85@wakeupcall.net", "password");
    }

    @Test
    public void createContractTest() {
        navigateToKey("URL.CONTRACTS.CREATE");

        //Assert no location selection
        //Assert no location can view/edit controls

        Contract contract = new Contract();

        contract.setAdditionalDetails("Addl details");
        contract.setContactEmail("automatedtest@wakeupcall.com");
        contract.setContactName("Multi Location Admin Test Contact");
        contract.setContactPhone("(123) 456-7890");
        contract.setContractedPartyName("Multi Location Admin Test Party");
        contract.setDescription("Multi Location Admin Test Description");
        contract.setEffectiveDate(new Date());
        contract.setExpirationDate(new Date());
        contract.setLocationCanEdit(true);
        contract.setLocationCanView(true);
        contract.setNotifyExpiration(true);
        contract.setNotifyLeadDays(30);
        contract.setStatus(Contract.Status.Active);
        contract.setStatusTransitionTo(Contract.Status.Inactive);

        fillContractForm(contract);

        setCheckbox("licensed_locations_90", true);

        submitContractForm();

        //Verify sent back to list
        assertNavigateToKey("URL.CONTRACTS.LIST");

        //Verify new contract is there
        assertContractAddedText();
        assertContractInList(contract);
    }
}
