package com.curiousdog.test.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseUtils 
{
	static
	{
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
	}
	public static void main(String[] args)
	{
		System.out.println("Started db program") ;
		getConnection("a", "b", "c") ;
		System.out.println("Ended db program") ;
	}
	
	public static Connection getConnection(String dbUrl, String dbUser, String dbPass)
	{
		System.out.println("Loading jdbc driver");
        try 
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Driver loaded successfully");
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            return null ;
        }

		Connection conn = null ;
		try 
		{
		    conn = DriverManager.getConnection("jdbc:mysql://216.70.106.180:3306/autotest_db", "autotest_user", "4autotest@wakeup");
		    if ( conn != null )		       

		    {
		    	System.out.println("Looky-looky where we are!") ;
		    }
		    else
		    {
		    	System.out.println("No cigar no bananas!") ;
		    }
		    conn.close();
		} 
		catch (SQLException ex) 
		{
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
		return conn ;
	}
}
