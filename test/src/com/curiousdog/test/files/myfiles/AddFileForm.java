package com.curiousdog.test.files.myfiles;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddFileForm extends WebForm
{
	public AddFileForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void fillOut(MyFile file)
	{
		this.setDropdown("join_members_library_categories_id", file.getFolderName()) ;
		this.setCheckbox("documents_is_corporate_shared", file.isSharedDocument()) ;
		this.setFileChooser("documents_file", file.getFilePath()) ;
		this.setTextbox("documents_title", file.getFileTitle()) ;
		if ( file.isSharedDocument() )
		{
			List<String> locList = file.getLocations() ;
			if ( locList != null && locList.size() > 0 )
			{
				for ( int i = 0 ; i < locList.size() ; i++ )
				{
					String locId = locList.get(i) ;
					if ( locId != null && locId.trim().length() > 0 ) 
					{
						String locLocId = "licensed_locations_" + locId.trim() ;
						driver.findElement(By.id(locLocId)).click();
					}
				}
			}
			
		}
	}
	
	public void navigateTo()
	{
		//TestUtils.chooseMenuItem(driver, "Files", "My Files");
		driver.findElement(By.linkText("Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("My Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Add File")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}

	public void submitForm()
	{
		WebElement titleElement = driver.findElement(By.id("documents_title")) ;
		titleElement.submit();
	}
	
	public void cancelForm()
	{
		WebElement backLink = driver.findElement(By.linkText("Return to Files List")) ;
		backLink.click();
	}
	
}
