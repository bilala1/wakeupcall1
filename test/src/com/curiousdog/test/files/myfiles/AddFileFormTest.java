package com.curiousdog.test.files.myfiles;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddFileFormTest 
{

	static AddFileForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		System.out.println("Starting add file test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		WebDriver driver = TestUtils.openChrome() ;
		form = new AddFileForm(driver) ;
		form.login(user, password);
	}
	
	@AfterClass
	public static void endTest()
	{
		form.logout();
		form.closeWindow();
		
		System.out.println("Test completed");
	}
	
	@Test
	public void goBack()
	{
		System.out.println("Testing going back to file list") ;
		form.navigateTo();
		form.cancelForm();
	}

	@Test
	public void testEmptyForm()
	{
		System.out.println("Submitting empty form and verify error messages");
		form.navigateTo();
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		form.assertNavigate("/members/my-documents/edit.php");
		form.assertTextExists("There are errors below", 			"No error banner at the top of the page") ;
		form.assertTextExists("Please upload a file", 				"No error message for no uploaded file") ;
		form.assertTextExists("You must enter a value for Title", 	"No error message for missing title") ;
	}

	@Test
	public void testSubmitForm()
	{
		System.out.println("Submitting fully filled form");
		MyFile file = new MyFile(bundle, "FILES.MYFILES.ADDFILE.FILE1") ;
		
		form.navigateTo();
		form.fillOut(file);
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
				
		form.assertNavigate("/members/my-documents/index.php?notice=Document+added");
		form.assertTextExists(file.getFileTitle(), "File's title did not appear on the screen");
	}
	
	
}
