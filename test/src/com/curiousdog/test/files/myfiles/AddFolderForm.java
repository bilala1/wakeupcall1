package com.curiousdog.test.files.myfiles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddFolderForm extends WebForm
{
	public AddFolderForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void fillOut(MyFolder folder)
	{
		this.setTextbox("members_library_categories_name", folder.getFolderName()) ;
	}
	
	public void navigateTo()
	{
		driver.findElement(By.linkText("Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("My Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Add Folder")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}

	public void submitForm()
	{
		WebElement nameElement = driver.findElement(By.id("members_library_categories_name")) ;
		nameElement.submit();
	}
	
	public void cancelForm()
	{
		WebElement backLink = driver.findElement(By.linkText("Return to Files List")) ;
		backLink.click();
	}

}
