package com.curiousdog.test.files.myfiles;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddFolderFormTest 
{
	static AddFolderForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		System.out.println("Starting add folder test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		//driver = new FirefoxDriver() ;
		//WebDriver driver = TestUtils.openFirefox() ;
		WebDriver driver = TestUtils.openChrome() ;
		
		form = new AddFolderForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void testSubmitForm()
	{
		System.out.println("Submitting fully filled form");
		MyFolder folder = new MyFolder(bundle, "FILES.MYFILES.ADDFOLDER.FOLDER1") ;
		
		form.navigateTo();
		form.fillOut(folder);
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
				
		form.assertNavigate("/members/my-documents/index.php?&notice=Folder+added");
		form.assertTextExists("Folder added", "No message folder added displayed") ;
		form.assertTextExists(folder.getFolderName(), "File's title did not appear on the screen");
	}
	
	@Test
	public void testEmptyForm()
	{
		System.out.println("Submitting empty form and verify error messages");
		form.navigateTo();
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		form.assertNavigate("/members/my-documents/folders/edit.php");
		form.assertTextExists("There are errors below", 				"No error banner at the top of the page") ;
		form.assertTextExists("You must enter a value for Folder Name", "No error message for missing folder name") ;
	}
	
	@Test
	public void goBack()
	{
		System.out.println("Testing going back to file list") ;
		form.navigateTo();
		form.cancelForm();
	}
	
	@AfterClass
	public static void endTest()
	{
		form.logout();
		form.closeWindow();
		System.out.println("Test completed");
	}
	

}
