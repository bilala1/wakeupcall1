package com.curiousdog.test.files.myfiles;

	import java.net.HttpURLConnection;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

	public class DeleteFileForm extends WebForm 
	{
		public DeleteFileForm(WebDriver driver)
		{
			super(driver) ;
		}
		
		public void navigateTo()
		{
			//TestUtils.chooseMenuItem(driver, "Files", "My Files");
			driver.findElement(By.linkText("Files")).click();
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
			driver.findElement(By.linkText("My Files")).click();
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		}

		public void submit(String id)
		{
			//Go to add new file form
			if ( id != null && id.trim().length() > 0 )
			{
				try
				{
					WebElement deleteLink = TestUtils.getLinkByClassAndId(driver, "deletecheck", appHomePage + "/members/documents/remove-document.php?documents_id", id) ;
					if ( deleteLink != null ) 
					{
						if ( !deleteLink.isEnabled() )
						{
							System.out.println("Disabled link") ;
							return ;
						}
						String url = deleteLink.getAttribute("href") ;
						clickActionMenuByLinkClass("deletecheck", url) ;
					}
					else System.out.println("URL='" + deleteLink + "' is not found.");
				}
				catch(Exception ex)
				{
					System.err.println("Error finding folder id = " + id) ;
					ex.printStackTrace();
				}
			}
			else System.out.println("File ID is empty. Doing nothing...");
		}

}
