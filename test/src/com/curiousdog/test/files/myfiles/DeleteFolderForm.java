package com.curiousdog.test.files.myfiles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class DeleteFolderForm extends WebForm 
{
	public DeleteFolderForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void navigateTo()
	{
		//TestUtils.chooseMenuItem(driver, "Files", "My Files");
		driver.findElement(By.linkText("Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("My Files")).click();
	}

	public void submit(String id)
	{
		//Go to add new file form
		if ( id != null && id.trim().length() > 0 )
		{
			try
			{
				WebElement deleteLink = TestUtils.getLinkElementById(driver, "Delete Folder", "members_library_categories_id", id) ;
				if ( deleteLink != null && deleteLink.isEnabled() && deleteLink.isDisplayed() ) deleteLink.click(); 
				else System.out.println("URL='" + deleteLink + "' is not found.");
			}
			catch(Exception ex)
			{
				System.err.println("Error finding folder id = " + id) ;
			}
		}
		else System.out.println("Folder ID is empty. Doing nothing...");
	}
}
