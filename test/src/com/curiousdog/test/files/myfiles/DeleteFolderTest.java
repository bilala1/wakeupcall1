package com.curiousdog.test.files.myfiles;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class DeleteFolderTest 
{
	static DeleteFolderForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	static MyFolder folder = null ;
	
	@BeforeClass
	public static void startTest()
	{
		System.out.println("Starting delete folder test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
	
		
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
		
		form = new DeleteFolderForm(driver) ;
		form.login(user, password);
	}
	
	@AfterClass
	public static void endTest()
	{
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Logout")).click();
		driver.close();
		
		System.out.println("Test completed");
	}
	
	@Test
	public void testDeleteOK()
	{
		System.out.println("Submitting fully filled form");
		folder = new MyFolder(bundle, "FILES.MYFILES.DELETEFOLDER.FOLDER1") ;
		
		form.navigateTo();
		form.submit(folder.getFolderId());
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.dialogOK();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
				
		//form.assertNavigate("/members/my-documents/index.php?&notice=Folder+updated");
		//form.assertTextExists("Folder updated", "No message folder added displayed") ;
		//form.assertTextExists(folder.getFolderName(), "Folder's name did not appear on the screen");
	}
	
	@Test
	public void testDeleteCancel()
	{
		System.out.println("Submitting empty form and verify error messages");
		folder = new MyFolder(bundle, "FILES.MYFILES.DELETEFOLDER.FOLDER2") ;
		form.navigateTo();
		form.submit(folder.getFolderId());
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.dialogCancel() ;
		//form.assertNavigate("/members/my-documents/folders/edit.php");
		//form.assertTextExists("There are errors below", 				"No error banner at the top of the page") ;
		//form.assertTextExists("You must enter a value for Folder Name", "No error message for missing folder name") ;
	}
	

}
