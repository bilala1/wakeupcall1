package com.curiousdog.test.files.myfiles;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class EditFileTest 
{
	static EditFileForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	static MyFile file = null ;
	
	@BeforeClass
	public static void startTest()
	{
		System.out.println("Starting edit file test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
	
		
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
		
		form = new EditFileForm(driver) ;
		form.login(user, password);
	}
	
	@AfterClass
	public static void endTest()
	{
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Logout")).click();
		driver.close();
		
		System.out.println("Test completed");
	}

	@Test
	public void testChangeFolder()
	{
		file = new MyFile(bundle, "FILES.MYFILES.EDITFILE.FILE3") ;
		form.navigateTo(file.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		form.fillOut(file);
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		form.submitForm();
		form.assertNavigate("members/my-documents/index.php?notice=Document+updated");
		form.assertTextExists("Document updated", "No message that file was updated");
	}
	
	@Test 
	public void testChangeTitleAndFile()
	{
		file = new MyFile(bundle, "FILES.MYFILES.EDITFILE.FILE4") ;
		form.navigateTo(file.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		form.fillOut(file);
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		form.submitForm();
		form.assertNavigate("members/my-documents/index.php?notice=Document+updated");
		form.assertTextExists("Document updated", "No message that file was updated");
	}
/*		
	@Test
	public void testUnshareFile()
	{
		file = new MyFile(bundle, "FILES.MYFILES.EDITFILE.FILE3") ;
		form.navigateTo(file.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
	}
	
	@Test
	public void testShareFile()
	{
		file = new MyFile(bundle, "FILES.MYFILES.EDITFILE.FILE3") ;
		form.navigateTo(file.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
	}
*/
	
	@Test
	public void testCancelForm()
	{
		file = new MyFile(bundle, "FILES.MYFILES.EDITFILE.FILE4") ;
		form.navigateTo(file.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		form.fillOut(file);
		form.cancelForm();
	}

}
