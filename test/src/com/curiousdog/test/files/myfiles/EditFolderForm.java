package com.curiousdog.test.files.myfiles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class EditFolderForm extends WebForm 
{
	public EditFolderForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void fillOut(MyFolder folder)
	{
		System.out.println("Setting text in '" + folder.getFolderId() + "' to: " + folder.getFolderName()) ;
		this.setTextbox("members_library_categories_name", folder.getFolderName()) ;
	}
	
	public void navigateTo(String id)
	{
		//TestUtils.chooseMenuItem(driver, "Files", "My Files");
		driver.findElement(By.linkText("Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("My Files")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Go to add new file form
		if ( id != null && id.trim().length() > 0 )
		{
			try
			{
				WebElement editLink = TestUtils.getLinkElementById(driver, "Edit Folder", "members_library_categories_id", id) ;
				if ( editLink != null && editLink.isEnabled() && editLink.isDisplayed() ) editLink.click(); 
				else System.out.println("URL='" + editLink + "' is not found.");
			}
			catch(Exception ex)
			{
				System.err.println("Error finding folder id = " + id) ;
			}
		}
		else System.out.println("Folder ID is empty. Doing nothing...");
	}

	public void submitForm()
	{
		WebElement nameElement = driver.findElement(By.id("members_library_categories_name")) ;
		nameElement.submit();
	}
	
	public void cancelForm()
	{
		WebElement backLink = driver.findElement(By.linkText("Return to Files List")) ;
		backLink.click();
	}

	public void clearForm()
	{
		driver.findElement(By.id("members_library_categories_name")).clear();
	}
}
