package com.curiousdog.test.files.myfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class MyFile 
{
	private String id ;
	private String folderName ;
	private boolean sharedDocument ;
	private String filePath ;
	private String fileName ;
	private String fileTitle ;
	private List<String> locations ;
	
	public MyFile()
	{
		super() ;
		locations = new ArrayList<String>() ;
	}
	public MyFile(ResourceBundle bundle, String key)
	{
		this() ;
		loadConfigValues(bundle, key) ;
	}
	
	public void loadConfigValues(ResourceBundle bundle, String key)
	{
		this.id       = TestUtils.getProperty(bundle, key + ".id") ;
		this.filePath = TestUtils.getProperty(bundle, key + ".documents_file") ;
		this.fileTitle= TestUtils.getProperty(bundle, key + ".documents_title") ;
		
		String folderId = TestUtils.getProperty(bundle, key + ".join_members_library_categories_id") ;
		if ( folderId == null || folderId.trim().length() == 0 ) this.folderName = "0" ;
		else                                                     this.folderName = folderId ;

		this.sharedDocument = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".documents_is_corporate_shared")) ;
		if ( this.sharedDocument )
		{
			String tLocations 	= TestUtils.getProperty(bundle, key + ".licensed_locations") ;			
			this.locations 		= TestUtils.splitString(tLocations, ',') ;
		}
		
	}
	
	
	public List<String> getLocations() {
		return locations;
	}
	public void setLocations(List<String> locations) {
		this.locations = locations;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public boolean isSharedDocument() {
		return sharedDocument;
	}
	public void setSharedDocument(boolean sharedDocument) {
		this.sharedDocument = sharedDocument;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileTitle() {
		return fileTitle;
	}
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}
	
	
}
