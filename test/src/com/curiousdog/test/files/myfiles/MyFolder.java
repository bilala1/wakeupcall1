package com.curiousdog.test.files.myfiles;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class MyFolder 
{
	private String folderName ;
	private String folderId ;

	public MyFolder()
	{
		super() ;
	}
	public MyFolder(ResourceBundle bundle, String key)
	{
		this();
		loadConfigValues(bundle, key) ;
	}

	public void loadConfigValues(ResourceBundle bundle, String key)
	{
		this.folderId   = TestUtils.getProperty(bundle, key + ".id") ;
		this.folderName = TestUtils.getProperty(bundle, key + ".members_library_categories_name") ;
	}
	
	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public String getFolderId() {
		return folderId;
	}
	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}
	
	
}
