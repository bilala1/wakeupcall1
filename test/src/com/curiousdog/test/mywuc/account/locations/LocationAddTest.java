package com.curiousdog.test.mywuc.account.locations;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.bean.Location;

public class LocationAddTest 
{
	static LocationsForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	

	@BeforeClass
	public static void NavigateToMe()
	{
		driver = new FirefoxDriver() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new LocationsForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void submitIncompleteForm()
	{
		form.navigateToLocationsList();
		
		//go to add location form:
		driver.findElement(By.linkText("Add Location")).click();
		form.submitAddLocationForm(); 
		form.assertTextExists("Please select a location type", "Missing location type error is not displayed") ;
		form.assertTextExists("You must enter a value for Location Name", "Missing location name error is not displayed") ;
		form.assertTextExists("Please enter something", "Missing something error is not displayed") ;
		form.assertTextExists("You must enter a value for Phone", "Missing phone error is not displayed") ;
	}
	
	@Test
	public void submitCompleteForm()
	{
		form.navigateToLocationsList();
		
		//go to add location form:
		driver.findElement(By.linkText("Add Location")).click();
		
		Location location = new Location(bundle, "MYWUC.MYACCOUNT.LOCATIONS.ADD") ;
		form.fillOutForm(location);
		form.submitAddLocationForm(); 
		
		form.assertNavigate("/members/account/hotels/list.php?notice=Hotel+Added");
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
}
