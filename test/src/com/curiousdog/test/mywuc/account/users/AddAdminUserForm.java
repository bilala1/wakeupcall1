package com.curiousdog.test.mywuc.account.users;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.common.WebForm;

public class AddAdminUserForm extends WebForm 
{
	public AddAdminUserForm(WebDriver drv)
	{
		super(drv) ;
	}

	public void fillOutForm(UserAdmin user)
	{
		setTextbox("members_firstname", user.getMembers_firstname());
		setTextbox("members_lastname",  user.getMembers_lastname()) ;
		setTextbox("members_title",     user.getMembers_title()) ;
		setTextbox("members_email",     user.getMembers_email()) ;
		setTextbox("members_email2",    user.getMembers_email2()) ;
		setTextbox("members_password",  user.getMembers_password()) ;
		setTextbox("members_password2", user.getMembers_password2()) ;
		setCheckbox("members_type_0",   user.isAccountAdmin())  ;
		setCheckbox("access[manageAdmins]", user.isManageAdmins()) ;

	}
	
	public void submitForm()
	{
		driver.findElement(By.id("members_title")).submit();
	}
	
	public void navigateToMe()
	{
		this.driver.findElement(By.partialLinkText("My WAKEUP")).click();
		this.driver.findElement(By.linkText("My Account")).click();
		this.driver.findElement(By.linkText("Users")).click();
		
		//Click Add new user link
		this.driver.findElement(By.linkText("Add New User")).click() ;
	}
}
