package com.curiousdog.test.mywuc.account.users;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.common.WebForm;

public class AddRegularUserForm extends WebForm 
{
	public AddRegularUserForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void fillOut(UserRegular user)
	{
		setTextbox("members_firstname", user.getMembers_firstname());
		setTextbox("members_lastname",  user.getMembers_lastname()) ;
		setTextbox("members_title",     user.getMembers_title()) ;
		setTextbox("members_email",     user.getMembers_email()) ;
		setTextbox("members_email2",    user.getMembers_email2()) ;
		setTextbox("members_password",  user.getMembers_password()) ;
		setTextbox("members_password2", user.getMembers_password2()) ;
		setCheckbox("members_type_0",   user.isAccountAdmin())  ;
		//setCheckbox("access[manageAdmins]", user.isManageAdmins()) ;
		
		setCheckbox("access[elaw]", user.isElaw()) ;
		setCheckbox("access[hr]", user.isHr()) ;
		setCheckbox("access[forum]", user.isForum()) ;
		setCheckbox("access[shareDocuments]", user.isShareDocuments()) ;
		
		List<String> locAdminList = user.getLocationAdmin() ;
		if ( locAdminList != null && locAdminList.size() > 0 )
		{
			for ( int i = 0 ; i < locAdminList.size() ; i++ )
			{
				String locId = locAdminList.get(i) ;
				if ( locId != null && locId.trim().length() > 0 ) 
				{
					String locAdminLocId = "loc_access[" + locId.trim() + "][locationAdmin]" ;
					driver.findElement(By.id(locAdminLocId)).click();
				}
			}
		}
		List<String> assoc = user.getAssociated() ;
		if ( assoc != null && assoc.size() > 0 )
		{
			for ( int i = 0 ; i < assoc.size() ; i++ )
			{
				String locId = assoc.get(i) ;
				if ( locId != null && locId.trim().length() > 0 ) 
				{
					String assocLocId = "loc_access[" + locId.trim() + "][associated]" ;
					driver.findElement(By.id(assocLocId)).click();
				}
			}
		}
		List<String> certs = user.getCertificates() ;
		if ( certs != null && certs.size() > 0 )
		{
			for ( int i = 0 ; i < certs.size() ; i++ )
			{
				String locId = certs.get(i) ;
				if ( locId != null && locId.trim().length() > 0 )
				{
					String certLocId = "loc_access[" + locId.trim() + "][certificates]" ;
					driver.findElement(By.id(certLocId)).click() ;
				}
			}
		}
		List<String> claims = user.getClaims() ;
		if ( claims != null && claims.size() > 0 )
		{
			for ( int i = 0 ; i < claims.size() ; i++)
			{
				String locId = claims.get(i) ;
				if ( locId != null && locId.trim().length() > 0 )
				{
					String claimLocId = "loc_access[" + locId.trim() + "][claims]" ;
					driver.findElement(By.id(claimLocId)).click();
				}
			}
		}
		List<String> conts = user.getContracts() ;
		if ( conts != null && conts.size() > 0 )
		{
			for ( int i = 0 ; i < conts.size() ; i++ )
			{
				String locId = conts.get(i) ;
				if ( locId != null && locId.trim().length() > 0 )
				{
					String contsLocId = "loc_access[" + locId.trim() + "][contracts]" ;
					driver.findElement(By.id(contsLocId)).click();
				}
			}
		}
		List<String> manu = user.getManageUsers() ;
		if ( manu != null && manu.size() > 0 )
		{
			for ( int i = 0 ; i < manu.size() ; i++ )
			{
				String locId = manu.get(i) ;
				if ( locId != null && locId.trim().length() > 0 )
				{
					String manuLocId = "loc_access[" + locId.trim() + "][manageUsers]" ;
					driver.findElement(By.id(manuLocId)).click();
				}
			}
		}
		List<String> ediLoc = user.getEditLocations() ;
		if ( ediLoc != null && ediLoc.size() > 0 )
		{
			for ( int i = 0 ; i < ediLoc.size() ; i++ )
			{
				String locId = ediLoc.get(i) ;
				if ( locId != null && locId.trim().length() > 0 )
				{
					String ediLocLocId = "loc_access[" + locId.trim() + "][editLocation]" ;
					driver.findElement(By.id(ediLocLocId)).click();
				}
			}
		}
	}
	
	public void navigateToMe()
	{
		driver.findElement(By.partialLinkText("My WAKEUP")).click();
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Users")).click();
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add New User")).click();
		//Choose a regular user:
		this.setCheckbox("members_type_1", true);
	}

	public void submitForm()
	{
		this.driver.findElement(By.id("members_firstname")).submit() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Users List")).click() ;
	}

	
}
