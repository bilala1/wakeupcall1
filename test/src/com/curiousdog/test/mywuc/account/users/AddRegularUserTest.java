package com.curiousdog.test.mywuc.account.users;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddRegularUserTest 
{
	static AddRegularUserForm form = null ;
	static WebDriver driver = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	
	@BeforeClass
	public static void startTest()
	{
		driver = new FirefoxDriver() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddRegularUserForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void EmptyFormSubmitTest()
	{
		form.navigateToMe();
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Verify test results
		form.assertTextExists("You must enter a value for First Name", "Error message for missing First name is not displayed");
		form.assertTextExists("You must enter a value for Last Name", "Error message for missing Last name is not displayed");
		form.assertTextExists("You must enter a value for Position/Title", "Error message for missing Position is not displayed");
		form.assertTextExists("You must enter a properly formatted email", "Error message for missing Email is not displayed");
		form.assertTextExists("You must enter a value for Repeat Email", "Error message for missing Repeat Email is not displayed");
		form.assertTextExists("You must enter a value for Password", "Error message for missing Password is not displayed");
		form.assertTextExists("You must enter a value for Repeat Password", "Error message for missing Repeat Password is not displayed");
		form.assertTextExists("Please remember to save changes below, after setting permissions", "Error message for missing Permissions is not displayed");
		
	}

	@Test
	public void FullFormSubmitTest()
	{
		UserRegular user = new UserRegular(bundle, "MYWUC.MYACCOUNT.USERS.ADD.REGULAR_USER1") ;
		form.navigateToMe();
		form.fillOut(user);
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Verify test results
		form.assertNavigate("http://staging.wakeupcall.net/members/account/hotels/members/list.php?notice=New+user+added") ;
		form.assertTextExists("New user added", "New user added message did not appear") ;
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	
	
}
