package com.curiousdog.test.mywuc.account.users;

import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class UserRegular 
{
	private String members_firstname ;
	private String members_lastname ;
	private String members_title ;
	private String members_email ;
	private String members_email2 ;
	private String members_password ;
	private String members_password2 ;
	private String members_type_0 ;
	private String members_type_1 ;
	private boolean manageAdmins ;
	private boolean accountAdmin ;
	
	private boolean elaw ;
	private boolean hr ;
	private boolean forum ;
	private boolean shareDocuments ;
	
	//Has comma separated lists of location ids
	private List<String> locationAdmin ;
	private List<String> associated ;
	private List<String> certificates ;
	private List<String> claims ;
	private List<String> contracts ;
	private List<String> manageUsers ;
	private List<String> editLocations ;
	
	
	public UserRegular()
	{
		super() ;
	}
	public UserRegular(ResourceBundle bundle, String configString)
	{
		this.populate(bundle, configString);
	}
	public void populate(ResourceBundle bundle, String configString)
	{
		this.members_firstname = TestUtils.getProperty(bundle, configString + ".members_firstname") ;
		this.members_lastname  = TestUtils.getProperty(bundle, configString + ".members_lastname");
		this.members_title     = TestUtils.getProperty(bundle, configString + ".members_title") ;
		this.members_email     = TestUtils.getProperty(bundle, configString + ".members_email") ;
		this.members_email2	   = TestUtils.getProperty(bundle, configString + ".members_email2") ;
		this.members_password  = TestUtils.getProperty(bundle, configString + ".members_password") ;
		this.members_password2 = TestUtils.getProperty(bundle, configString + ".members_password2") ;
		this.accountAdmin = false ;
		
		String tLocationAdmin 	= TestUtils.getProperty(bundle, configString + ".locationAdmin") ;
		String tAssociated 		= TestUtils.getProperty(bundle, configString + ".associated");
		String tCertificates 	= TestUtils.getProperty(bundle, configString + ".certificates");
		String tClaims 			= TestUtils.getProperty(bundle, configString + ".claims");
		String tContracts		= TestUtils.getProperty(bundle, configString + ".contracts") ;
		String tManageUsers		= TestUtils.getProperty(bundle, configString + ".manageUsers") ;
		String tEditLocations	= TestUtils.getProperty(bundle, configString + ".editLocations") ;
		
		this.locationAdmin 	= TestUtils.splitString(tLocationAdmin, ',') ;
		this.associated		= TestUtils.splitString(tAssociated, ',') ;
		this.certificates	= TestUtils.splitString(tCertificates, ',') ;
		this.claims			= TestUtils.splitString(tClaims, ',') ;
		this.contracts		= TestUtils.splitString(tContracts, ',') ;
		this.manageUsers	= TestUtils.splitString(tManageUsers, ',') ;
		this.editLocations	= TestUtils.splitString(tEditLocations, ',') ;
	}
	
	
	public List<String> getLocationAdmin() {
		return locationAdmin;
	}
	public void setLocationAdmin(List<String> locationAdmin) {
		this.locationAdmin = locationAdmin;
	}
	public List<String> getAssociated() {
		return associated;
	}
	public void setAssociated(List<String> associated) {
		this.associated = associated;
	}
	public List<String> getCertificates() {
		return certificates;
	}
	public void setCertificates(List<String> certificates) {
		this.certificates = certificates;
	}
	public List<String> getClaims() {
		return claims;
	}
	public void setClaims(List<String> claims) {
		this.claims = claims;
	}
	public List<String> getContracts() {
		return contracts;
	}
	public void setContracts(List<String> contracts) {
		this.contracts = contracts;
	}
	public List<String> getManageUsers() {
		return manageUsers;
	}
	public void setManageUsers(List<String> manageUsers) {
		this.manageUsers = manageUsers;
	}
	public List<String> getEditLocations() {
		return editLocations;
	}
	public void setEditLocations(List<String> editLocations) {
		this.editLocations = editLocations;
	}
	public boolean isElaw() {
		return elaw;
	}
	public void setElaw(boolean elaw) {
		this.elaw = elaw;
	}
	public boolean isHr() {
		return hr;
	}
	public void setHr(boolean hr) {
		this.hr = hr;
	}
	public boolean isForum() {
		return forum;
	}
	public void setForum(boolean forum) {
		this.forum = forum;
	}
	public boolean isShareDocuments() {
		return shareDocuments;
	}
	public void setShareDocuments(boolean shareDocuments) {
		this.shareDocuments = shareDocuments;
	}
	public String getMembers_firstname() {
		return members_firstname;
	}
	public void setMembers_firstname(String members_firstname) {
		this.members_firstname = members_firstname;
	}
	public String getMembers_lastname() {
		return members_lastname;
	}
	public void setMembers_lastname(String members_lastname) {
		this.members_lastname = members_lastname;
	}
	public String getMembers_title() {
		return members_title;
	}
	public void setMembers_title(String members_title) {
		this.members_title = members_title;
	}
	public String getMembers_email() {
		return members_email;
	}
	public void setMembers_email(String members_email) {
		this.members_email = members_email;
	}
	public String getMembers_email2() {
		return members_email2;
	}
	public void setMembers_email2(String members_email2) {
		this.members_email2 = members_email2;
	}
	public String getMembers_password() {
		return members_password;
	}
	public void setMembers_password(String members_password) {
		this.members_password = members_password;
	}
	public String getMembers_password2() {
		return members_password2;
	}
	public void setMembers_password2(String members_password2) {
		this.members_password2 = members_password2;
	}
	public String getMembers_type_0() {
		return members_type_0;
	}
	public void setMembers_type_0(String members_type_0) {
		this.members_type_0 = members_type_0;
	}
	public String getMembers_type_1() {
		return members_type_1;
	}
	public void setMembers_type_1(String members_type_1) {
		this.members_type_1 = members_type_1;
	}
	public boolean isManageAdmins() {
		return manageAdmins;
	}
	public void setManageAdmins(boolean manageAdmins) {
		this.manageAdmins = manageAdmins;
	}
	public boolean isAccountAdmin() {
		return accountAdmin;
	}
	public void setAccountAdmin(boolean accountAdmin) {
		this.accountAdmin = accountAdmin;
	}


}
