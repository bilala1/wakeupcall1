package com.curiousdog.test.mywuc.certificate;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;

public class AddCertificateForm extends WebForm
{
	public AddCertificateForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Certificate cert)
	{
		this.setDropdown("licensed_locations_id", cert.getLicensed_locations_id());
		this.setCheckbox("certificates_location_can_view", cert.isCertificates_location_can_view()) ;
		this.setCheckbox("certificates_location_can_edit", cert.isCertificates_location_can_edit()) ;
				
		WebElement vendor = driver.findElement(By.id("join_vendors_id")) ;
		if ( vendor.isDisplayed() && vendor.isEnabled() )
		{
			this.setDropdown("join_vendors_id", cert.getJoin_vendors_id());
		}
		this.setDropdown("certificates_type", cert.getCertificates_type()) ;
		//#add_file
		this.setCheckbox("coverage_gl", cert.isCoverage_gl()) ;
		this.setCheckbox("coverage_umbrella", cert.isCoverage_umbrella()) ;
		this.setCheckbox("coverage_comp", cert.isCoverage_comp()) ;
		this.setCheckbox("coverage_auto", cert.isCoverage_auto()) ;
		this.setCheckbox("coverage_crime", cert.isCoverage_crime()) ;
		this.setCheckbox("coverage_prop", cert.isCoverage_prop()) ;
		this.setCheckbox("coverage_prof", cert.isCoverage_prof()) ;
		this.setCheckbox("coverage_el", cert.isCoverage_el()) ;
		this.setCheckbox("coverage_other", cert.isCoverage_other()) ;
		WebElement otherCoverage = driver.findElement(By.id("certificates_coverage_other")) ;
		if ( otherCoverage.isDisplayed() && otherCoverage.isEnabled() )
		{
			this.setTextbox("certificates_coverage_other", cert.getCertificates_coverage_other()) ;
		}
		this.setCheckbox("certificates_request_cert_to_email_chk", cert.isCertificates_request_cert_to_email_chk()) ;
		WebElement emailAddress = driver.findElement(By.id("certificates_request_cert_to_email")) ;
		if ( emailAddress.isDisplayed() && emailAddress.isEnabled() )
		{
			this.setTextbox("certificates_request_cert_to_email", cert.getCertificates_request_cert_to_email()) ;
		}
		this.setCheckbox("certificates_request_cert_to_fax", cert.isCertificates_request_cert_to_fax()) ;
		this.setCheckbox("certificates_request_cert_to_address", cert.isCertificates_request_cert_to_address()) ;

		this.setCheckbox("request_cert", cert.isRequest_cert()) ;
		WebElement emailSubject = driver.findElement(By.id("subject_line")) ;
		if ( emailSubject.isDisplayed() && emailSubject.isEnabled() )
		{
			this.setTextbox("subject_line", cert.getSubject_line()) ;
		}
		//certificates_expire 
		Calendar expirationDate = new GregorianCalendar() ;
		expirationDate.setTime(cert.getCertificates_expire()) ;
		
		String expYear = Integer.toString(expirationDate.get(Calendar.YEAR)) ;
		String expMonth= Integer.toString(expirationDate.get(Calendar.MONTH)) ;
		String expDay  = Integer.toString(expirationDate.get(Calendar.DAY_OF_MONTH)) ;
		
		selectCalendarDate("certificates_expire", expYear, expMonth, expDay);
		
		this.setDropdown("certificates_email_days", cert.getCertificates_email_days()) ;
		this.setCheckbox("certificates_remind_vendor", cert.isCertificates_remind_vendor()) ;
		List<String> certRemindBefore = cert.getCertificate_remind_members_before_expire() ;
		if ( certRemindBefore != null && certRemindBefore.size() > 0 )
		{
			for ( int i = 0 ; i < certRemindBefore.size() ; i++ )
			{
				String reminder = (String)certRemindBefore.get(i) ;
				if ( reminder != null && reminder.trim().length() > 0 )
				{
					String reminderId = "certificate_remind_members_before_expire_" + reminder ;
					WebElement el = driver.findElement(By.id(reminderId)) ;
					if ( el != null ) el.click();
				}
			}
		}
		this.setCheckbox("certificates_remind_other_chk", cert.isCertificates_remind_other_chk()) ;
		this.setTextbox("certificates_remind_other", cert.getCertificates_remind_other()) ;
		
		this.setDropdown("certificates_remind_member", cert.getCertificates_remind_member()) ;
		this.setCheckbox("certificates_remind_vendor_expired", cert.isCertificates_remind_vendor_expired()) ;
		List<String> certRemindAfter = cert.getCertificate_remind_members_after_expire() ;
		if ( certRemindAfter != null && certRemindAfter.size() > 0 )
		{
			for ( int i = 0 ; i < certRemindAfter.size() ; i++ )
			{
				String reminder = (String)certRemindAfter.get(i) ;
				if ( reminder != null && reminder.trim().length() > 0 )
				{
					String reminderId = "certificate_remind_members_after_expire_" + reminder ;
					WebElement el = driver.findElement(By.id(reminderId)) ;
					if ( el != null ) el.click();
				}
			}
		}
		this.setCheckbox("certificates_remind_other_expired_chk", cert.isCertificates_remind_other_expired_chk()) ;
		WebElement remindOther = driver.findElement(By.id("certificates_remind_other_expired")) ;
		if ( remindOther.isDisplayed() && remindOther.isEnabled() )
		{
			this.setTextbox("certificates_remind_other_expired", cert.getCertificates_remind_other_expired()) ;
		}
	}

	public void navigateToMe()
	{
		driver.findElement(By.partialLinkText("My WAKEUP")).click();
		driver.findElement(By.linkText("Certificate Tracking")).click();
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Certificate")).click();
	}
	public void submitForm()
	{
		this.driver.findElement(By.id("save")).submit() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Certificates List")).click() ;
	}

}
