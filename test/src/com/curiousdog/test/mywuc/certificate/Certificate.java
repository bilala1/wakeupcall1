package com.curiousdog.test.mywuc.certificate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Certificate 
{
	private String licensed_locations_id ;
	private boolean certificates_location_can_view ;
	private boolean certificates_location_can_edit ;
	private String join_vendors_id ;
	private String certificates_type ;
	private String fileName ;
	//#add_file
	private boolean coverage_gl ;
 	private boolean coverage_umbrella ;
	private boolean coverage_comp ;
	private boolean coverage_auto ;
	private boolean coverage_crime ;
	private boolean coverage_prop ;
	private boolean coverage_prof ;
	private boolean coverage_el ;
	private boolean coverage_other ;
	private String certificates_coverage_other ;
	private Date certificates_expire ;

	private boolean certificates_include_additional_insured_request ;
	private String certificates_default_location_name ;
	private String certificate_additional_request_text ;
	
	private boolean certificates_request_cert_to_email_chk ;
	private String certificates_request_cert_to_email ;
	private boolean certificates_request_cert_to_fax ;
	private boolean certificates_request_cert_to_address ;
	
	private boolean request_cert ;
	private String subject_line ;
	
	private String certificates_email_days ;
	private boolean certificates_remind_vendor ;
	List<String> certificate_remind_members_before_expire ;
	private boolean certificates_remind_other_chk ;
	private String certificates_remind_other ;
	
	private String certificates_remind_member ;
	private boolean certificates_remind_vendor_expired ;
	List<String> certificate_remind_members_after_expire ;
	private boolean certificates_remind_other_expired_chk ;
	private String certificates_remind_other_expired ;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy") ;
	
	public Certificate() 
	{
		super() ;
	}
	public Certificate(ResourceBundle bundle, String key)
	{
		this() ;
		populate(bundle, key) ;
	}
	public void populate(ResourceBundle bundle, String key)
	{
		licensed_locations_id = TestUtils.getProperty(bundle, key + ".licensed_locations_id") ;
		certificates_location_can_view = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_location_can_view")) ;
		certificates_location_can_edit = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_location_can_edit")) ;
		join_vendors_id 	= TestUtils.getProperty(bundle, key + ".join_vendors_id") ;
		certificates_type 	= TestUtils.getProperty(bundle, key + ".certificates_type") ;
		fileName 			= TestUtils.getProperty(bundle, key + ".add_file") ;
		coverage_gl 		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_gl")) ;
	 	coverage_umbrella 	= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_umbrella")) ;
		coverage_comp  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_comp")) ;
		coverage_auto  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_auto")) ;
		coverage_crime  	= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_crime")) ;
		coverage_prop  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_prop")) ;
		coverage_prof  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_prof")) ;
		coverage_el  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_el")) ;
		coverage_other  	= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_other")) ;
		certificates_coverage_other = TestUtils.getProperty(bundle, key + ".certificates_coverage_other") ;
		String expirationString 	= TestUtils.getProperty(bundle, key + ".certificates_expire") ;
		try
		{
			certificates_expire = sdf.parse(expirationString);
		}
		catch(Exception ex)
		{
			//Nothing here
		}
		certificates_include_additional_insured_request 
												= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_include_additional_insured_request")) ;
		certificates_default_location_name  	= TestUtils.getProperty(bundle, key + ".certificates_default_location_name") ;
		certificate_additional_request_text  	= TestUtils.getProperty(bundle, key + ".certificate_additional_request_text") ;
		
		certificates_request_cert_to_email_chk 	= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_request_cert_to_email_chk")) ;
		certificates_request_cert_to_email  	= TestUtils.getProperty(bundle, key + ".certificates_request_cert_to_email") ;
		certificates_request_cert_to_fax  		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_request_cert_to_fax"));
		certificates_request_cert_to_address  	= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_request_cert_to_address")) ;
		
		request_cert 		= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".request_cert")) ;
		subject_line 		= TestUtils.getProperty(bundle, key + ".subject_line") ;
		
		certificates_email_days 	= TestUtils.getProperty(bundle, key + ".certificates_email_days") ;
		certificates_remind_vendor  = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_remind_vendor")) ;
		String tRemindMembers 		= TestUtils.getProperty(bundle, key + ".certificate_remind_members_before_expire") ;
		certificate_remind_members_before_expire 
									= TestUtils.splitString(tRemindMembers, ',') ;
		certificates_remind_other_chk 	
									= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_remind_other_chk")) ;
		certificates_remind_other 	= TestUtils.getProperty(bundle, key + ".certificates_remind_other") ;
		
		certificates_remind_member 	= TestUtils.getProperty(bundle, key + ".certificates_remind_member") ;
		certificates_remind_vendor_expired 
									= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_remind_vendor_expired")) ;
		String tRemindAfter 		= TestUtils.getProperty(bundle, key + ".certificate_remind_members_after_expire") ;
		certificate_remind_members_after_expire 
									= TestUtils.splitString(tRemindAfter, ',') ;
		certificates_remind_other_expired_chk 
									= "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".certificates_remind_other_expired_chk")) ;
		certificates_remind_other_expired 
									= TestUtils.getProperty(bundle, key + ".certificates_remind_other_expired") ;
		
	}
	public String getLicensed_locations_id() {
		return licensed_locations_id;
	}
	public void setLicensed_locations_id(String licensed_locations_id) {
		this.licensed_locations_id = licensed_locations_id;
	}
	public boolean isCertificates_location_can_view() {
		return certificates_location_can_view;
	}
	public void setCertificates_location_can_view(boolean certificates_location_can_view) {
		this.certificates_location_can_view = certificates_location_can_view;
	}
	public boolean isCertificates_location_can_edit() {
		return certificates_location_can_edit;
	}
	public void setCertificates_location_can_edit(boolean certificates_location_can_edit) {
		this.certificates_location_can_edit = certificates_location_can_edit;
	}
	public String getJoin_vendors_id() {
		return join_vendors_id;
	}
	public void setJoin_vendors_id(String join_vendors_id) {
		this.join_vendors_id = join_vendors_id;
	}
	public String getCertificates_type() {
		return certificates_type;
	}
	public void setCertificates_type(String certificates_type) {
		this.certificates_type = certificates_type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public boolean isCoverage_gl() {
		return coverage_gl;
	}
	public void setCoverage_gl(boolean coverage_gl) {
		this.coverage_gl = coverage_gl;
	}
	public boolean isCoverage_umbrella() {
		return coverage_umbrella;
	}
	public void setCoverage_umbrella(boolean coverage_umbrella) {
		this.coverage_umbrella = coverage_umbrella;
	}
	public boolean isCoverage_comp() {
		return coverage_comp;
	}
	public void setCoverage_comp(boolean coverage_comp) {
		this.coverage_comp = coverage_comp;
	}
	public boolean isCoverage_auto() {
		return coverage_auto;
	}
	public void setCoverage_auto(boolean coverage_auto) {
		this.coverage_auto = coverage_auto;
	}
	public boolean isCoverage_crime() {
		return coverage_crime;
	}
	public void setCoverage_crime(boolean coverage_crime) {
		this.coverage_crime = coverage_crime;
	}
	public boolean isCoverage_prop() {
		return coverage_prop;
	}
	public void setCoverage_prop(boolean coverage_prop) {
		this.coverage_prop = coverage_prop;
	}
	public boolean isCoverage_prof() {
		return coverage_prof;
	}
	public void setCoverage_prof(boolean coverage_prof) {
		this.coverage_prof = coverage_prof;
	}
	public boolean isCoverage_el() {
		return coverage_el;
	}
	public void setCoverage_el(boolean coverage_el) {
		this.coverage_el = coverage_el;
	}
	public boolean isCoverage_other() {
		return coverage_other;
	}
	public void setCoverage_other(boolean coverage_other) {
		this.coverage_other = coverage_other;
	}
	public String getCertificates_coverage_other() {
		return certificates_coverage_other;
	}
	public void setCertificates_coverage_other(String certificates_coverage_other) {
		this.certificates_coverage_other = certificates_coverage_other;
	}
	public boolean isRequest_cert() {
		return request_cert;
	}
	public void setRequest_cert(boolean request_cert) {
		this.request_cert = request_cert;
	}
	public String getSubject_line() {
		return subject_line;
	}
	public void setSubject_line(String subject_line) {
		this.subject_line = subject_line;
	}
	public boolean isCertificates_request_cert_to_email_chk() {
		return certificates_request_cert_to_email_chk;
	}
	public void setCertificates_request_cert_to_email_chk(boolean certificates_request_cert_to_email_chk) {
		this.certificates_request_cert_to_email_chk = certificates_request_cert_to_email_chk;
	}
	public String getCertificates_request_cert_to_email() {
		return certificates_request_cert_to_email;
	}
	public void setCertificates_request_cert_to_email(String certificates_request_cert_to_email) {
		this.certificates_request_cert_to_email = certificates_request_cert_to_email;
	}
	public boolean isCertificates_request_cert_to_fax() {
		return certificates_request_cert_to_fax;
	}
	public void setCertificates_request_cert_to_fax(boolean certificates_request_cert_to_fax) {
		this.certificates_request_cert_to_fax = certificates_request_cert_to_fax;
	}
	public boolean isCertificates_request_cert_to_address() {
		return certificates_request_cert_to_address;
	}
	public void setCertificates_request_cert_to_address(boolean certificates_request_cert_to_address) {
		this.certificates_request_cert_to_address = certificates_request_cert_to_address;
	}
	public Date getCertificates_expire() {
		return certificates_expire;
	}
	public void setCertificates_expire(Date certificates_expire) {
		this.certificates_expire = certificates_expire;
	}
	public String getCertificates_email_days() {
		return certificates_email_days;
	}
	public void setCertificates_email_days(String certificates_email_days) {
		this.certificates_email_days = certificates_email_days;
	}
	public boolean isCertificates_remind_vendor() {
		return certificates_remind_vendor;
	}
	public void setCertificates_remind_vendor(boolean certificates_remind_vendor) {
		this.certificates_remind_vendor = certificates_remind_vendor;
	}
	public boolean isCertificates_include_additional_insured_request() {
		return certificates_include_additional_insured_request;
	}
	public void setCertificates_include_additional_insured_request(
			boolean certificates_include_additional_insured_request) {
		this.certificates_include_additional_insured_request = certificates_include_additional_insured_request;
	}
	public String getCertificates_default_location_name() {
		return certificates_default_location_name;
	}
	public void setCertificates_default_location_name(String certificates_default_location_name) {
		this.certificates_default_location_name = certificates_default_location_name;
	}
	public String getCertificate_additional_request_text() {
		return certificate_additional_request_text;
	}
	public void setCertificate_additional_request_text(String certificate_additional_request_text) {
		this.certificate_additional_request_text = certificate_additional_request_text;
	}
	public List<String> getCertificate_remind_members_before_expire() {
		return certificate_remind_members_before_expire;
	}
	public void setCertificate_remind_members_before_expire(List<String> certificate_remind_members_before_expire) {
		this.certificate_remind_members_before_expire = certificate_remind_members_before_expire;
	}
	public boolean isCertificates_remind_other_chk() {
		return certificates_remind_other_chk;
	}
	public void setCertificates_remind_other_chk(boolean certificates_remind_other_chk) {
		this.certificates_remind_other_chk = certificates_remind_other_chk;
	}
	public String getCertificates_remind_other() {
		return certificates_remind_other;
	}
	public void setCertificates_remind_other(String certificates_remind_other) {
		this.certificates_remind_other = certificates_remind_other;
	}
	public String getCertificates_remind_member() {
		return certificates_remind_member;
	}
	public void setCertificates_remind_member(String certificates_remind_member) {
		this.certificates_remind_member = certificates_remind_member;
	}
	public boolean isCertificates_remind_vendor_expired() {
		return certificates_remind_vendor_expired;
	}
	public void setCertificates_remind_vendor_expired(boolean certificates_remind_vendor_expired) {
		this.certificates_remind_vendor_expired = certificates_remind_vendor_expired;
	}
	public List<String> getCertificate_remind_members_after_expire() {
		return certificate_remind_members_after_expire;
	}
	public void setCertificate_remind_members_after_expire(List<String> certificate_remind_members_after_expire) {
		this.certificate_remind_members_after_expire = certificate_remind_members_after_expire;
	}
	public boolean isCertificates_remind_other_expired_chk() {
		return certificates_remind_other_expired_chk;
	}
	public void setCertificates_remind_other_expired_chk(boolean certificates_remind_other_expired_chk) {
		this.certificates_remind_other_expired_chk = certificates_remind_other_expired_chk;
	}
	public String getCertificates_remind_other_expired() {
		return certificates_remind_other_expired;
	}
	public void setCertificates_remind_other_expired(String certificates_remind_other_expired) {
		this.certificates_remind_other_expired = certificates_remind_other_expired;
	}
	
	
}
