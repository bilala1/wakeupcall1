package com.curiousdog.test.mywuc.certificate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Vendor;
import com.curiousdog.test.common.WebForm;

public class VendorAddForm extends WebForm
{
	public VendorAddForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Vendor vendor)
	{
		//vendor id can be either "all" for all locations checkbox or a numeric id for a specific location
		setCheckbox("licensed_locations_" + vendor.getLocationId(), true) ;
		
		setCheckbox("vendors_location_can_view"	, vendor.isLocationCanView()) ;
		setCheckbox("vendors_location_can_edit"	, vendor.isLocationCanEdit());
		setTextbox ("vendors_name"				, vendor.getName()) ;
		setTextbox ("vendors_contact_name"		, vendor.getContactName());
		setTextbox ("vendors_email"				, vendor.getEmail());
		setTextbox ("vendors_services"			, vendor.getServices());
		setTextbox ("vendors_phone"				, vendor.getPhone()) ;
		setTextbox ("vendors_street"			, vendor.getAddress()) ;
	}
	
	public void navigateToMe()
	{
		driver.findElement(By.partialLinkText("My WAKEUP")).click();
		driver.findElement(By.linkText("Certificate Tracking")).click();
		driver.findElement(By.linkText("Manage Vendors")).click();
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Vendor")).click();
	}
	
	public void submitForm()
	{
		this.driver.findElement(By.id("vendors_name")).submit() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Vendor List")).click() ;
	}
}
