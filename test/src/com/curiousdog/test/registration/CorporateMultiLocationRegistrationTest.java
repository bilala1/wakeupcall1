package com.curiousdog.test.registration;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.bean.CorporateAccount;
import com.curiousdog.test.bean.Location;
import com.curiousdog.test.bean.PaymentInfo;

public class CorporateMultiLocationRegistrationTest 
{
	static CorporateMultiLocationRegistrationForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass 
	public static void openWindow()
	{
		driver = new FirefoxDriver() ;
		form = new CorporateMultiLocationRegistrationForm(driver) ;
	}
	
	@Test
	public void testCompleteSubmission()
	{
		
		form.navigateToRegistrationForm();
		form.assertNavigate("/signup-direct.php") ;
		
		CorporateAccount account = new CorporateAccount(bundle, "SIGNUP.CORPORATE.SEPARATE.ADD_LOCATION") ;
		Location location = new Location(bundle, "SIGNUP.CORPORATE.LOCATION") ;
		
		form.fillOutForm(account);
		form.fillOutLocationInfo(location);
		form.submitCorporateRegistrationFormJoin();
		
		form.assertNavigate("/full-membership.php");
		
		//Get to payment form and might as well test it while there
		PaymentInfo paymentInfo = new PaymentInfo(bundle, "SIGNUP.CORPORATE.PAYMENT") ;
		RegistrationPaymentForm paymentForm = new RegistrationPaymentForm(driver) ;
		paymentForm.fillOutForm(paymentInfo);
		paymentForm.submitForm() ; 
	}
	
	@Test
	public void testSubmitNotAddLocation()
	{
		
	}
	
	@Test
	public void testSubmitAddLocation()
	{
		
	}
	
	@AfterClass 
	public static void closeWindow()
	{
		form.close();
		System.out.println("Test completed");
	}
	
}
