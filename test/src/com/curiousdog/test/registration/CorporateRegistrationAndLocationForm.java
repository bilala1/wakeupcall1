package com.curiousdog.test.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.CorporateAccount;
import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class CorporateRegistrationAndLocationForm extends WebForm
{
	public CorporateRegistrationAndLocationForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOutForm(CorporateAccount account)
	{
		this.setCheckbox("btype_0", false);
		this.setCheckbox("btype_1", true);

		this.setTextboxByName("corporations_name_autocomplete", account.getCorporations_name_autocomplete());
		this.setTextbox("corporations_address", account.getCorporations_address());
		this.setTextbox("corporations_city", account.getCorporations_city()) ;
		this.setDropdown("corporations_state", account.getCorporations_state());
		this.setTextbox("corporations_zip", account.getCorporations_zip()) ;
		
		this.setTextbox("members_firstname", account.getMembers_firstname());
		this.setTextbox("members_lastname", account.getMembers_lastname());
		this.setTextbox("members_title", account.getMembers_title());
		this.setTextbox("members_phone", account.getMembers_phone());
		this.setTextbox("members_fax", account.getMembers_fax());
		this.setTextbox("members_email", account.getMembers_email());
		this.setTextbox("members_email2", account.getMembers_email2());
		this.setTextbox("members_password", account.getMembers_password());
		this.setTextbox("members_password2", account.getMembers_password2());
		
		this.setCheckbox("separate_0", account.isCorporateOfficeSeparateYes());
		this.setCheckbox("separate_1", account.isCorporateOfficeSeparateNo());
		
		this.setTextbox("corporate_locations", account.getCorporate_locations());
		
		this.setCheckbox("other_loc_0", account.isCreateLocationYes());
		this.setCheckbox("other_loc_1", account.isCreateLocationNo());
		TestUtils.Pause(10000) ;
	}
	
	public void fillOutLocationInfo(FirstLocation location)
	{
		this.setDropdown("first_ind_type", location.getFirst_ind_type()) ;
		this.setTextboxByName("first_hotels_name_autocomplete", location.getFirst_hotels_name_autocomplete()) ;
		this.setTextbox("first_hotels_num_rooms", location.getFirst_hotels_num_rooms()) ;
		this.setTextbox("first_hotels_address", location.getFirst_hotels_address()) ;
		this.setTextbox("first_hotels_city", location.getFirst_hotels_city()) ;
		this.setDropdown("first_hotels_state", location.getFirst_hotels_state()) ;
		this.setTextbox("first_hotels_zip", location.getFirst_hotels_zip()) ;
	}
	
	public void submitCorporateRegistrationFormJoin()
	{
		this.setCheckbox("terms", true);
		this.driver.findElement(By.partialLinkText("Join")).click() ;
	}
	
	public void submitCorporateRegistrationFormRegister()
	{
		this.setCheckbox("terms", true);
		this.driver.findElement(By.partialLinkText("Register")).click();
	}
	
	public void navigateTo()
	{
		this.driver.get(this.appHomePage + "/index.php");
		this.driver.findElement(By.linkText("LET'S GO")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		this.driver.findElement(By.id("btype_1")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
	}
	public void cancelForm()
	{
		
	}
	public void submitForm()
	{
		this.setCheckbox("terms", true);
		this.driver.findElement(By.partialLinkText("Join")).click() ;
	}
	
	public void setText(String id, String text)
	{
		setTextbox(id, text) ;
	}
	public void setCheck(String id, boolean checked)
	{
		setCheckbox(id, checked) ;
	}
}
