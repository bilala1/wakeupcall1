package com.curiousdog.test.registration;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.CorporateAccount;
import com.curiousdog.test.bean.PaymentInfo;
import com.curiousdog.test.utils.TestUtils;

public class CorporateRegistrationNoLocationTest 
{
	static CorporateRegistrationNoLocationForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass 
	public static void openWindow()
	{
		driver = TestUtils.openChrome() ;
		form = new CorporateRegistrationNoLocationForm(driver) ;
	}
	
	@Test
	public void testCompleteSubmission()
	{
		
		form.navigateTo();
		form.assertNavigate("/signup-direct.php") ;
		
		CorporateAccount account = new CorporateAccount(bundle, "SIGNUP.CORPORATE.SEPARATE.NO_LOCATION") ;
		form.fillOut(account);
		form.submitForm();
		
		form.assertNavigate("/full-membership.php");
		
		//Get to payment form and might as well test it while there
		PaymentInfo paymentInfo = new PaymentInfo(bundle, "SIGNUP.CORPORATE.PAYMENT") ;
		RegistrationPaymentForm paymentForm = new RegistrationPaymentForm(driver) ;
		paymentForm.fillOut(paymentInfo);
		paymentForm.submitForm() ; 
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		form.assertNavigate("/thank-you/?notice=You+can+now+login") ;
	}
	
	@Test
	public void submitEmptyFormTest()
	{
		form.navigateTo();
		form.submitForm();
		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Verify that error messages show up on the screen
		form.assertTextExists("Please correct the errors below (in red)", "No message about errors on the page") ;
		form.assertTextExists("You must enter a value for Company Name", "No message about missing company name") ;
		form.assertTextExists("You must enter a value for Address", "No message about missing address") ;
		form.assertTextExists("You must enter a value for City", "No message about missing city") ;
		form.assertTextExists("You must enter a value for Zip", "No message about missing zip") ;
		form.assertTextExists("You must enter a value for First Name", "No message about missing first name") ;
		form.assertTextExists("You must enter a value for Last Name", "No message about missing last name") ;
		form.assertTextExists("You must enter a value for Title", "No message about missing title") ;
		form.assertTextExists("You must enter a value for Phone", "No message about missing phone") ;
		form.assertTextExists("You must enter a properly formatted email", "No message about missing email") ;
		form.assertTextExists("Password must have a minimum of 6 characters", "No message about missing or short password") ;
		form.assertTextExists("Please select yes or no", "No message about needing to choose yes or no") ;
		TestUtils.Pause(TestUtils.PAUSE_BORING) ;
	}
	
	@Test
	public void submitPartialForm()
	{
		form.navigateTo();
		form.setText("members_phone", "ABCD212233");
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.assertTextExists("Phone cannot contain non-numeric characters except (, ), and -", "No message for incorrect phone number");
		
		form.setText("members_email", "ABCDEFG");
		form.setText("members_email2","GFEDCBA");
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.assertTextExists("You must enter a properly formatted email", "No message for incorrect email");
		form.assertTextExists("Must match other value", "No message for not matching values");
		
		form.clearElement("members_email");
		form.clearElement("members_email2");
		form.setText("members_email", "abc@abc.com");
		form.setText("members_email2","abc@abc.com");
		form.setText("members_password", "ABC");
		form.setText("members_password2","CBA");
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.assertTextExists("Password must have a minimum of 6 characters", "No message about missing or short password") ;
		form.assertTextExists("Must match other value", "No message for not matching values");
		
	}
	
	@AfterClass 
	public static void closeWindow()
	{
		TestUtils.close(driver);
		System.out.println("Test is completed") ;
	}
	

}
