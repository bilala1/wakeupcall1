package com.curiousdog.test.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.CorporateAccount;
import com.curiousdog.test.bean.Location;
import com.curiousdog.test.common.WebForm;

public class CorporateSingleLocationRegistrationForm  extends WebForm
{
	public CorporateSingleLocationRegistrationForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOutForm(CorporateAccount account)
	{
		this.setCheckbox("btype_0", false);
		this.setCheckbox("btype_1", true);
		
		this.setTextboxByName("corporations_name_autocomplete", account.getCorporations_name_autocomplete());
		this.setTextbox("corporations_address", account.getCorporations_address());
		this.setTextbox("corporations_city", account.getCorporations_city()) ;
		this.setDropdown("corporations_state", account.getCorporations_state());
		this.setTextbox("corporations_zip", account.getCorporations_zip()) ;
		
		this.setTextbox("members_firstname", account.getMembers_firstname());
		this.setTextbox("members_lastname", account.getMembers_lastname());
		this.setTextbox("members_title", account.getMembers_title());
		this.setTextbox("members_phone", account.getMembers_phone());
		this.setTextbox("members_fax", account.getMembers_fax());
		this.setTextbox("members_email", account.getMembers_email());
		this.setTextbox("members_email2", account.getMembers_email2());
		this.setTextbox("members_password", account.getMembers_password());
		this.setTextbox("members_password2", account.getMembers_password2());
		
		this.setCheckbox("separate_0", account.isCorporateOfficeSeparateYes());
		this.setCheckbox("separate_1", account.isCorporateOfficeSeparateNo());
		
		this.setTextbox("corporate_locations", account.getCorporate_locations());
		
		this.setCheckbox("other_loc_0", account.isCreateLocationYes());
		this.setCheckbox("other_loc_1", account.isCreateLocationNo());
		
	}
		
	public void submitCorporateRegistrationFormJoin()
	{
		this.setCheckbox("terms", true);
		this.driver.findElement(By.partialLinkText("Join")).click() ;
	}
	
	public void submitCorporateRegistrationFormRegister()
	{
		this.setCheckbox("terms", true);
		this.driver.findElement(By.partialLinkText("Register")).click();
	}
	
	public void navigateToRegistrationForm()
	{
		this.driver.get(this.appHomePage + "/index.php");
		this.driver.findElement(By.linkText("LET'S GO")).click();
	}

}
