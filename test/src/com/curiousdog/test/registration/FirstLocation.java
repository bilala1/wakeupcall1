package com.curiousdog.test.registration;

import java.util.ResourceBundle;

public class FirstLocation 
{
	private String first_ind_type ;
	private String first_hotels_name_autocomplete ;
	private String first_hotels_num_rooms ;
	private String first_hotels_address ;
	private String first_hotels_city ;
	private String first_hotels_state ;
	private String first_hotels_zip ;

	public FirstLocation()
	{
		super() ;
	}
	
	public FirstLocation(ResourceBundle bundle, String tag)
	{
		this() ;
		populate(bundle, tag) ;
	}
	
	public void populate(ResourceBundle bundle, String tag)
	{
		first_ind_type = bundle.getString(tag + ".first_ind_type") ;
		first_hotels_name_autocomplete = bundle.getString(tag + ".first_hotels_name_autocomplete") ;
		first_hotels_num_rooms = bundle.getString(tag + ".first_hotels_num_rooms") ;
		first_hotels_address = bundle.getString(tag + ".first_hotels_address") ;
		first_hotels_city = bundle.getString(tag + ".first_hotels_city") ;
		first_hotels_state = bundle.getString(tag + ".first_hotels_state") ;
		first_hotels_zip = bundle.getString(tag + ".first_hotels_zip") ;
		
	}

	public String getFirst_ind_type() {
		return first_ind_type;
	}

	public void setFirst_ind_type(String first_ind_type) {
		this.first_ind_type = first_ind_type;
	}

	public String getFirst_hotels_name_autocomplete() {
		return first_hotels_name_autocomplete;
	}

	public void setFirst_hotels_name_autocomplete(String first_hotels_name_autocomplete) {
		this.first_hotels_name_autocomplete = first_hotels_name_autocomplete;
	}

	public String getFirst_hotels_num_rooms() {
		return first_hotels_num_rooms;
	}

	public void setFirst_hotels_num_rooms(String first_hotels_num_rooms) {
		this.first_hotels_num_rooms = first_hotels_num_rooms;
	}

	public String getFirst_hotels_address() {
		return first_hotels_address;
	}

	public void setFirst_hotels_address(String first_hotels_address) {
		this.first_hotels_address = first_hotels_address;
	}

	public String getFirst_hotels_city() {
		return first_hotels_city;
	}

	public void setFirst_hotels_city(String first_hotels_city) {
		this.first_hotels_city = first_hotels_city;
	}

	public String getFirst_hotels_state() {
		return first_hotels_state;
	}

	public void setFirst_hotels_state(String first_hotels_state) {
		this.first_hotels_state = first_hotels_state;
	}

	public String getFirst_hotels_zip() {
		return first_hotels_zip;
	}

	public void setFirst_hotels_zip(String first_hotels_zip) {
		this.first_hotels_zip = first_hotels_zip;
	}
	
	
}
