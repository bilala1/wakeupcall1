package com.curiousdog.test.registration;

import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Location;
import com.curiousdog.test.common.WebForm;

public class FirstLocationForm extends WebForm 
{
	public FirstLocationForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOutForm(Location location)
	{
		this.setDropdown("first_ind_type", location.getLocationType());
		this.setTextboxByName("first_hotels_name_autocomplete", location.getName());
		this.setTextbox("first_hotels_num_rooms", location.getNoOfRooms());
		this.setTextbox("first_hotels_address", location.getAddress());
		this.setTextbox("first_hotels_city", location.getCity()) ;
		this.setDropdown("first_hotels_state", location.getState());
		this.setTextbox("first_hotels_zip", location.getZip());
	}
	
	public void submitForm()
	{
		
	}
	public void cancelForm() 
	{
		
	}
	public void navigateTo()
	{
		
	}
}
