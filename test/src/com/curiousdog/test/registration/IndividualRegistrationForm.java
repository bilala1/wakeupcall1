package com.curiousdog.test.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.IndividualAccount;
import com.curiousdog.test.common.WebForm;

public class IndividualRegistrationForm extends WebForm
{
	public IndividualRegistrationForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void navigateTo()
	{
		driver.get(this.appHomePage + "/index.php");
		
		//getting to the registration form:
		System.out.println("Going to the registration page...");
		driver.findElement(By.partialLinkText("LET'S GO")).click();
		assertNavigate("/signup-direct.php") ;

		System.out.println("Select individual registration...") ;
		setCheckbox(driver.findElement(By.id("btype_0")), true) ;
	}

	public void fillOut(IndividualAccount account)
	{
		System.out.println("Filling out property info");
		//fill out the rest of the data
		setDropdown		("ind_type", 					account.getProperty_type());  
		setTextboxByName("hotels_name_autocomplete", 	account.getHotels_name_autocomplete()) ; 
		setTextbox 		("hotels_num_rooms", 			account.getHotels_num_rooms()) ;
		setTextbox 		("hotels_address", 				account.getHotels_address()) ;
		setTextbox 		("hotels_city", 				account.getHotels_city()) ;
		setDropdown		("hotels_state",				account.getHotels_state()) ;
		setTextbox 		("hotels_zip", 					account.getHotels_zip()) ;
		
		System.out.println("Filling out member info");
		setTextbox ("hotels_members_firstname", account.getHotels_members_firstname()) ;
		setTextbox ("hotels_members_lastname",	account.getHotels_members_lastname()) ;
		setTextbox ("hotels_members_title",		account.getHotels_members_title()) ;
		setTextbox ("hotels_members_phone",		account.getHotels_members_phone()) ;
		setTextbox ("hotels_members_fax", 		account.getHotels_members_fax()) ;
		setTextbox ("hotels_members_email", 	account.getHotels_members_email()) ;
		setTextbox ("hotels_members_email2",	account.getHotels_members_email2()) ;
		setTextbox ("hotels_pass",				account.getHotels_pass()) ;
		setTextbox ("hotels_pass2", 			account.getHotels_pass2()) ;
		
	}
	
	public void submitForm()
	{
		System.out.println("Agree to terms");
		setCheckbox("terms", true) ;
		
		System.out.println("Leaving form empty and submitting");
		driver.findElement(By.partialLinkText("Join")).click() ;
		
	}
	public void cancelForm()
	{
		
	}
}
