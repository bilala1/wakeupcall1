package com.curiousdog.test.registration;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.IndividualAccount;
import com.curiousdog.test.bean.PaymentInfo;
import com.curiousdog.test.utils.TestUtils;

public class IndividualRegistrationTest 
{
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static IndividualRegistrationForm form = null ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void openBrowser()
	{
		System.out.println("Starting test. Opening browser and navigating to the Registration page");
		driver = TestUtils.openChrome() ;
		form = new IndividualRegistrationForm(driver) ;
		//form.navigateTo();				
	}
	
	@AfterClass
	public static void closeBrowser()
	{
		System.out.println("Test is completed. Closing browser");
		TestUtils.Pause(TestUtils.PAUSE_BORING) ;
		//driver.close();
	}
	
	@Test
	public void incompleteIndividualRegistrationTest()
	{
		form.navigateTo();
		form.submitForm();
		
		form.assertTextExists("Please select a location type", "Error for missing location type is not displayed") ;
		form.assertTextExists("You must enter a value for Location Name", "Error for missing location name is not displayed") ;
		form.assertTextExists("You must enter a value for Address", "Error for missing address is not displayed") ;
		form.assertTextExists("You must enter a value for City", "Error for missing city is not displayed") ;
		form.assertTextExists("You must enter a value for Zip", "Error for missing location zip is not displayed") ;
		form.assertTextExists("You must enter a value for Firstname", "Error for missing first name is not displayed") ;
		form.assertTextExists("You must enter a value for Lastname", "Error for missing last name is not displayed") ;
		form.assertTextExists("You must enter a value for Title", "Error for missing title is not displayed") ;
		form.assertTextExists("You must enter a value for Phone", "Error for missing phone is not displayed") ;
		form.assertTextExists("You must enter a properly formatted email", "Error for missing email is not displayed") ;
		form.assertTextExists("Password must have a minimum of 6 characters", "Error for missing password is not displayed") ;
	}
	
	@Test
	public void completeIndividualRegistrationTest()
	{
		form.navigateTo();
		
		IndividualAccount account = new IndividualAccount(bundle, "SIGNUP.INDIVIDUAL.COMPLETE") ;
		form.fillOut(account);
		form.submitForm(); 
		form.assertNavigate("/full-membership.php");
		
		//account submitted successfully, now entering payment info
		PaymentInfo paymentInfo = new PaymentInfo(bundle, "SIGNUP.INDIVIDUAL.PAYMENT") ;
		//fillPaymentInfo(paymentInfo, "SIGNUP.INDIVIDUAL.PAYMENT") ;

		RegistrationPaymentForm paymentForm = new RegistrationPaymentForm(driver);
		paymentForm.fillOut(paymentInfo);
		paymentForm.submitForm(); 
		
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		TestUtils.close(driver);
	} 
	
	void fillPaymentInfo(PaymentInfo info, String key)
	{
		info.setDiscount_code(TestUtils.getProperty(bundle, key + ".discount_code"));
		info.setFranchises_code(TestUtils.getProperty(bundle, key + ".franchises_code"));
		info.setMembers_card_expire_month(TestUtils.getProperty(bundle, key + ".members_card_expire_month"));
		info.setMembers_card_expire_year(TestUtils.getProperty(bundle, key + ".members_card_expire_year"));
		info.setMembers_card_name(TestUtils.getProperty(bundle, key + ".members_card_name"));		
		info.setMembers_card_num(TestUtils.getProperty(bundle, key + ".members_card_num"));
		info.setMembers_card_type(TestUtils.getProperty(bundle, key + ".members_card_type"));
		info.setSecurity_code(TestUtils.getProperty(bundle, key + ".security_code"));
	}
	
	void fillAccountData(IndividualAccount account, String key)
	{
		account.setHotels_address(TestUtils.getProperty(bundle, key + ".hotels_address")) ;
		account.setHotels_city(TestUtils.getProperty(bundle, key + ".hotels_city")) ;
		account.setHotels_members_email(TestUtils.getProperty(bundle, key + ".hotels_members_email")) ;
		account.setHotels_members_email2(TestUtils.getProperty(bundle, key + ".hotels_members_email2")) ;
		account.setHotels_members_fax(TestUtils.getProperty(bundle, key + ".hotels_members_fax")) ;
		account.setHotels_members_firstname(TestUtils.getProperty(bundle, key + ".hotels_members_firstname")) ;
		account.setHotels_members_lastname(TestUtils.getProperty(bundle, key + ".hotels_members_lastname")) ;
		account.setHotels_members_phone(TestUtils.getProperty(bundle, key + ".hotels_members_phone")) ;
		account.setHotels_members_title(TestUtils.getProperty(bundle, key + ".hotels_members_title")) ;
		account.setHotels_name_autocomplete(TestUtils.getProperty(bundle, key + ".hotels_name_autocomplete")) ;
		account.setHotels_num_rooms(TestUtils.getProperty(bundle, key + ".hotels_num_rooms")) ;
		account.setHotels_pass(TestUtils.getProperty(bundle, key + ".hotels_pass")) ;
		account.setHotels_pass2(TestUtils.getProperty(bundle, key + ".hotels_pass2")) ;
		account.setHotels_state(TestUtils.getProperty(bundle, key + ".hotels_state")) ;
		account.setHotels_zip(TestUtils.getProperty(bundle, key + ".hotels_zip")) ;
		account.setProperty_type(TestUtils.getProperty(bundle, key + ".ind_type")) ;
	}
}
