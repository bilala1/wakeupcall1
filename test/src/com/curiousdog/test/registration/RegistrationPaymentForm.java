package com.curiousdog.test.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.PaymentInfo;
import com.curiousdog.test.common.WebForm;

public class RegistrationPaymentForm extends WebForm
{
	public RegistrationPaymentForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(PaymentInfo info)
	{
		System.out.println("Is same address? - " + info.isSame_address());
		
		this.setCheckbox("same_address", info.isSame_address()) ;
		
		if ( !info.isSame_address() )
		{	
			this.setTextbox("members_billing_addr1", info.getMembers_billing_addr1()) ;
			this.setTextbox("members_billing_addr2", info.getMembers_billing_addr2()) ;
			this.setTextbox("members_billing_city", info.getMembers_billing_city()) ;
			this.setDropdown("members_billing_state", info.getMembers_billing_state()) ;
			this.setTextbox("members_billing_zip", info.getMembers_billing_zip()) ;
		}
		this.setDropdown("members_card_type", info.getMembers_card_type()) ;
		this.setTextbox("members_card_name", info.getMembers_card_name()) ;
		this.setTextbox("members_card_num", info.getMembers_card_num()) ;
		this.setDropdown("members_card_expire_month", info.getMembers_card_expire_month()) ;
		this.setDropdown("members_card_expire_year", info.getMembers_card_expire_year()) ;
		this.setTextbox("security_code", info.getSecurity_code()) ;
		this.setTextbox("discount_code", info.getDiscount_code()) ;
		this.setTextbox("franchises_code", info.getFranchises_code()) ;
	}
	
	public void submitForm()
	{
		this.driver.findElement(By.id("franchises_code")).submit() ;
	}
	public void cancelForm()
	{
		//Not used separately
	}
	public void navigateTo() 
	{
		//Not used separately
	}
	public void recalculate()
	{
		this.driver.findElement(By.id("discount_code_btn")).click() ;
	}
}
