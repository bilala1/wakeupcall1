package com.curiousdog.test.support.contactus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.common.WebForm;

public class ContactUsForm extends WebForm
{
	public ContactUsForm(WebDriver driver)
	{
		super(driver) ;
	}
	
	public void fillOut(Suggestion suggestion)
	{
		setDropdown	("suggestion_type", suggestion.getSuggestionType()) ;
		setTextbox	("suggestion", 		suggestion.getSuggestionText()) ;
	}
	
	public void navigateTo()
	{
		driver.findElement(By.linkText("Support")).click();
		driver.findElement(By.linkText("Contact Us")).click();
	}
	
	public void submitForm()
	{
		driver.findElement(By.id("suggestion")).submit();
	}
}
