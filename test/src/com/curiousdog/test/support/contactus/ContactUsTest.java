package com.curiousdog.test.support.contactus;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class ContactUsTest 
{
	static ContactUsForm form = null ;
	static WebDriver driver = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	
	@BeforeClass
	public static void startTest()
	{
		driver = TestUtils.openChrome() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new ContactUsForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void submitFilledForm()
	{
		Suggestion suggestion = new Suggestion(bundle, "SUPPORT.CONTACTUS") ;
		form.navigateTo();
		form.fillOut(suggestion);
		form.submitForm();
		form.assertTextExists("Thank you for your suggestion", "The thank you note is not displayed");
	}
	@Test
	public void submitEmptyForm()
	{
		form.navigateTo();
		form.submitForm();
		form.assertTextExists("Please type a suggestion.", "No error message for empty suggestion") ;
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}

}
