package com.curiousdog.test.support.contactus;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Suggestion 
{
	private String suggestionType ;
	private String suggestionText ;
	
	public Suggestion()
	{
		super() ;
	}
	public Suggestion(ResourceBundle bundle, String tag)
	{
		this() ;
		fillMe(bundle, tag) ;
	}
	public void fillMe(ResourceBundle bundle, String tag)
	{
		this.suggestionType = TestUtils.getProperty(bundle, tag + ".suggestion_type") ;
		this.suggestionText = TestUtils.getProperty(bundle, tag + ".suggestion") ;
	}
	public String getSuggestionType() {
		return suggestionType;
	}
	public void setSuggestionType(String suggestionType) {
		this.suggestionType = suggestionType;
	}
	public String getSuggestionText() {
		return suggestionText;
	}
	public void setSuggestionText(String suggestionText) {
		this.suggestionText = suggestionText;
	}
	
	
}
