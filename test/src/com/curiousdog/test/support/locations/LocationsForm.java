package com.curiousdog.test.support.locations;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Location;
import com.curiousdog.test.common.WebForm;

public class LocationsForm extends WebForm
{
	public LocationsForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOutForm(Location location)
	{
		this.setDropdown("licensed_locations_full_type", location.getLocationType()) ;
		this.setTextbox("licensed_locations_name", location.getName()) ;
		this.setTextbox("hotels_num_rooms", location.getNoOfRooms()) ;
		this.setTextbox("licensed_locations_address", location.getAddress()) ;
		this.setTextbox("licensed_locations_city", location.getCity()) ;
		this.setDropdown("licensed_locations_state", location.getState()) ;
		this.setTextbox("licensed_locations_zip", location.getZip()) ;
		this.setTextbox("licensed_locations_phone", location.getPhone()) ;
		this.setTextbox("licensed_locations_fax", location.getFax()) ;
		this.setTextbox("discount_code", location.getDiscount_code()) ;
		this.setTextbox("franchise_code", location.getFranchise_code()) ;
		//total-cost
		this.setCheckbox("edit_claims_emails_permission", location.isEdit_claims_emails_permission()) ; ;
		this.setCheckbox("edit_certs_emails_permission", location.isEdit_certs_emails_permission()) ;

	}
	public void submitAddLocationForm()
	{
		this.driver.findElement(By.id("franchise_code")).submit() ;
	}
	
	public void navigateToLocationsList()
	{
		this.driver.findElement(By.linkText("My Account")).click();
		this.driver.findElement(By.linkText("Locations")).click();
		//this.driver.findElement(By.linkText("Add Location")).click();
	}

}
