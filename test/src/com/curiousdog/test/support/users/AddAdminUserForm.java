package com.curiousdog.test.support.users;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddAdminUserForm extends WebForm 
{
	public AddAdminUserForm(WebDriver drv)
	{
		super(drv) ;
	}

	public void fillOutForm(UserAdmin user)
	{
		
		setTextbox("members_firstname", user.getMembers_firstname());
		setTextbox("members_lastname",  user.getMembers_lastname()) ;
		setTextbox("members_title",     user.getMembers_title()) ;
		setTextbox("members_email",     user.getMembers_email()) ;
		setTextbox("members_email2",    user.getMembers_email2()) ;
		setTextbox("members_password",  user.getMembers_password()) ;
		setTextbox("members_password2", user.getMembers_password2()) ;
		setCheckbox("members_type_0",   user.isAccountAdmin())  ;
		setCheckbox("access[manageAdmins]", user.isManageAdmins()) ;
		setCheckbox("sensitive[workers_comp]", user.isWorkers_comp());
		setCheckbox("sensitive[labor_claims]", user.isLabor_claims()) ;
		setCheckbox("sensitive[other_claims]", user.isOther_claims()) ;
		
		//driver.findElement(By.xpath(".//input[@id='members_firstname']")).sendKeys(user.getMembers_firstname());
		//TestUtils.Pause(10) ;
		/*
		new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='members_firstname']"))).sendKeys(user.getMembers_firstname());
		driver.findElement(By.id("members_firstname")).sendKeys();
		driver.findElement(By.id("members_lastname")).sendKeys();
		driver.findElement(By.id("members_title")).sendKeys();
		driver.findElement(By.id("members_email")).sendKeys();
		driver.findElement(By.id("members_email2")).sendKeys();
		driver.findElement(By.id("members_password")).sendKeys();
		driver.findElement(By.id("members_password2")).sendKeys();
		driver.findElement(By.id("members_type_0")).click();
		*/
		
	}
	
	public void submitForm()
	{
		clickButton("save");
	}
	public void cancelForm()
	{
		clickTextLink("Return to Users List") ;
	}
	public void navigateTo()
	{
		clickTextLink("My Account") ;
		clickTextLink("Users") ;
		clickTextLink("Add New User") ;
	}
}
