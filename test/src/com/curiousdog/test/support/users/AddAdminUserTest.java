package com.curiousdog.test.support.users;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddAdminUserTest 
{
	static AddAdminUserForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddAdminUserForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitTest()
	{
		form.navigateTo();
		form.submitForm();
		
		form.assertTextExists("You must enter a value for First Name", "Error message for missing first name not displayed") ;
		form.assertTextExists("You must enter a value for Last Name", "Error message for missing last name not displayed") ;
		form.assertTextExists("You must enter a value for Position/Title", "Error message for missing position not displayed") ;
		form.assertTextExists("You must enter a properly formatted email", "Error message for missing email not displayed") ;
		form.assertTextExists("You must enter a value for Repeat Email", "Error message for missing repeat email not displayed");
		form.assertTextExists("You must enter a value for Password", "Error message for missing password not displayed") ;
		form.assertTextExists("You must enter a value for Repeat Password", "Error message for repeat password not displayed") ;
		form.assertTextExists("Please remember to save changes below, after setting permissions", "Message to save changes not displayed") ;
	}
	
	@Test
	public void SuccessfulFormSubmitTest()
	{
		UserAdmin user = new UserAdmin(bundle, "MYWUC.MYACCOUNT.USERS.ADD.ACCOUNT_ADMIN") ;

		form.navigateTo();
		form.fillOutForm(user);
		form.submitForm();

		form.assertNavigate("/members/account/hotels/members/list.php?notice=New+user+added");
		form.assertTextExists("New user added", "Message 'New user added' not displayed");
		
		form.assertTextExists(user.getMembers_firstname(), "New user's first name is not displayed");
		form.assertTextExists(user.getMembers_lastname(), "New user's last name is not displayed");
		form.assertTextExists(user.getMembers_email(), "New user's email is not displayed");
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}

}
