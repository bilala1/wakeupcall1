package com.curiousdog.test.support.users;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class UserAdmin 
{
	private String members_firstname ;
	private String members_lastname ;
	private String members_title ;
	private String members_email ;
	private String members_email2;
	private String members_password ;
	private String members_password2 ;
	private String members_type_0 ;
	private String members_type_1 ;
	private boolean manageAdmins ;
	private boolean accountAdmin ;
	private boolean workers_comp ;
	private boolean labor_claims ;
	private boolean other_claims ;
	
	public UserAdmin()
	{
		super() ;
	}
	public UserAdmin(ResourceBundle bundle, String configString)
	{
		this.populate(bundle, configString);
	}
	public void populate(ResourceBundle bundle, String configString)
	{
		this.members_firstname = TestUtils.getProperty(bundle, configString + ".members_firstname") ;
		this.members_lastname  = TestUtils.getProperty(bundle, configString + ".members_lastname");
		this.members_title     = TestUtils.getProperty(bundle, configString + ".members_title") ;
		this.members_email     = TestUtils.getProperty(bundle, configString + ".members_email") ;
		this.members_email2	   = TestUtils.getProperty(bundle, configString + ".members_email2") ;
		this.members_password  = TestUtils.getProperty(bundle, configString + ".members_password") ;
		this.members_password2 = TestUtils.getProperty(bundle, configString + ".members_password2") ;
		this.accountAdmin = true ;
		this.workers_comp = ("Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".workers_comp")));
		this.labor_claims = ("Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".labor_claims")));
		this.other_claims = ("Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".other_claims"))); ;
		
	}
	
	
	public boolean isWorkers_comp() {
		return workers_comp;
	}
	public void setWorkers_comp(boolean workers_comp) {
		this.workers_comp = workers_comp;
	}
	public boolean isLabor_claims() {
		return labor_claims;
	}
	public void setLabor_claims(boolean labor_claims) {
		this.labor_claims = labor_claims;
	}
	public boolean isOther_claims() {
		return other_claims;
	}
	public void setOther_claims(boolean other_claims) {
		this.other_claims = other_claims;
	}
	public String getMembers_firstname() {
		return members_firstname;
	}
	public void setMembers_firstname(String members_firstname) {
		this.members_firstname = members_firstname;
	}
	public String getMembers_lastname() {
		return members_lastname;
	}
	public void setMembers_lastname(String members_lastname) {
		this.members_lastname = members_lastname;
	}
	public String getMembers_title() {
		return members_title;
	}
	public void setMembers_title(String members_title) {
		this.members_title = members_title;
	}
	public String getMembers_email() {
		return members_email;
	}
	public void setMembers_email(String members_email) {
		this.members_email = members_email;
	}
	public String getMembers_email2() {
		return members_email2;
	}
	public void setMembers_email2(String members_email) {
		this.members_email2 = members_email;
	}
	public String getMembers_password() {
		return members_password;
	}
	public void setMembers_password(String members_password) {
		this.members_password = members_password;
	}
	public String getMembers_password2() {
		return members_password2;
	}
	public void setMembers_password2(String members_password2) {
		this.members_password2 = members_password2;
	}
	public String getMembers_type_0() {
		return members_type_0;
	}
	public void setMembers_type_0(String members_type_0) {
		this.members_type_0 = members_type_0;
	}
	public String getMembers_type_1() {
		return members_type_1;
	}
	public void setMembers_type_1(String members_type_1) {
		this.members_type_1 = members_type_1;
	}
	public boolean isManageAdmins() {
		return manageAdmins;
	}
	public void setManageAdmins(boolean manageAdmins) {
		this.manageAdmins = manageAdmins;
	}
	public boolean isAccountAdmin() {
		return accountAdmin;
	}
	public void setAccountAdmin(boolean accountAdmin) {
		this.accountAdmin = accountAdmin;
	}

	
}
