package com.curiousdog.test.tracking.business;

import java.util.Date;

public class ItemMilestone 
{
	private String entities_items_milestones_description ;
	private Date entities_items_milestones_date ;
	
	public String getEntities_items_milestones_description() {
		return entities_items_milestones_description;
	}
	public void setEntities_items_milestones_description(String entities_items_milestones_description) {
		this.entities_items_milestones_description = entities_items_milestones_description;
	}
	public Date getEntities_items_milestones_date() {
		return entities_items_milestones_date;
	}
	public void setEntities_items_milestones_date(Date entities_items_milestones_date) {
		this.entities_items_milestones_date = entities_items_milestones_date;
	}
	
	
}
