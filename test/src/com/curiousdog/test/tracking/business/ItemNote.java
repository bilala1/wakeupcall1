package com.curiousdog.test.tracking.business;

public class ItemNote 
{
	private String entities_items_notes_subject ;
	private String entities_items_notes_note ;
	
	public String getEntities_items_notes_subject() {
		return entities_items_notes_subject;
	}
	public void setEntities_items_notes_subject(String entities_items_notes_subject) {
		this.entities_items_notes_subject = entities_items_notes_subject;
	}
	public String getEntities_items_notes_note() {
		return entities_items_notes_note;
	}
	public void setEntities_items_notes_note(String entities_items_notes_note) {
		this.entities_items_notes_note = entities_items_notes_note;
	}

	
}
