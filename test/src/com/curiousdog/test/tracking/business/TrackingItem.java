package com.curiousdog.test.tracking.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class TrackingItem 
{
	private String entities_items_name ;
	private String entities_items_details ;
	private Date entities_items_effective_date ;
	private ArrayList<ItemMilestone> milestones ;
	private ArrayList<String> send_mails ;

	//#by name after clicking "Add recipient"
	private String entities_items_additional_recipients_email ; //limiting to 1 additional recepient
	private ArrayList<String> entities_items_files ;
	private ArrayList<ItemNote> itemNotes ;
	
	public TrackingItem()
	{
		super() ;
	}
	public TrackingItem(ResourceBundle bundle, String tag)
	{
		this() ;
		fill(bundle, tag) ;
	}
	public void fill(ResourceBundle bundle, String tag)
	{
		this.entities_items_name = TestUtils.getProperty(bundle, tag + ".entities_items_name") ;
		this.entities_items_details = TestUtils.getProperty(bundle, tag + ".entities_items_details") ;
		
	}
	public void addMilestone(ItemMilestone milestone)
	{
		if ( this.milestones == null ) this.milestones = new ArrayList<ItemMilestone>() ;
		if ( milestone != null ) this.milestones.add(milestone) ;
	}
	public void addSendMail(String mailAddress)
	{
		if ( this.send_mails == null ) this.send_mails = new ArrayList<String>() ;
		if ( mailAddress != null ) this.send_mails.add(mailAddress) ;
	}
	public void addFile(String fileName)
	{
		if ( this.entities_items_files == null ) this.entities_items_files = new ArrayList<String>() ;
		if ( fileName != null ) this.entities_items_files.add( fileName ) ;
	}
	public void addNote(ItemNote note)
	{
		if ( this.itemNotes == null ) this.itemNotes = new ArrayList<ItemNote>() ;
		if ( note != null ) this.itemNotes.add(note) ;
	}
	public String getEntities_items_name() {
		return entities_items_name;
	}
	public void setEntities_items_name(String entities_items_name) {
		this.entities_items_name = entities_items_name;
	}
	public String getEntities_items_details() {
		return entities_items_details;
	}
	public void setEntities_items_details(String entities_items_details) {
		this.entities_items_details = entities_items_details;
	}
	public Date getEntities_items_effective_date() {
		return entities_items_effective_date;
	}
	public void setEntities_items_effective_date(Date entities_items_effective_date) {
		this.entities_items_effective_date = entities_items_effective_date;
	}
	public ArrayList<ItemMilestone> getMilestones() {
		return milestones;
	}
	public void setMilestones(ArrayList<ItemMilestone> milestones) {
		this.milestones = milestones;
	}
	public ArrayList<String> getSend_mails() {
		return send_mails;
	}
	public void setSend_mails(ArrayList<String> send_mails) {
		this.send_mails = send_mails;
	}
	public String getEntities_items_additional_recipients_email() {
		return entities_items_additional_recipients_email;
	}
	public void setEntities_items_additional_recipients_email(String entities_items_additional_recipients_email) {
		this.entities_items_additional_recipients_email = entities_items_additional_recipients_email;
	}
	public ArrayList<String> getEntities_items_files() {
		return entities_items_files;
	}
	public void setEntities_items_files(ArrayList<String> entities_items_files) {
		this.entities_items_files = entities_items_files;
	}
	public ArrayList<ItemNote> getItemNotes() {
		return itemNotes;
	}
	public void setItemNotes(ArrayList<ItemNote> itemNotes) {
		this.itemNotes = itemNotes;
	}
	

	

}
