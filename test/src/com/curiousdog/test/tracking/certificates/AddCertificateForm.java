package com.curiousdog.test.tracking.certificates;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddCertificateForm extends WebForm
{
	public AddCertificateForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void justChooseLocation(Certificate cert)
	{
		this.setDropdown("licensed_locations_id", cert.getLicensed_locations_id());
		submitLocation() ;
	}
	
	public void fillOut(Certificate cert)
	{
		this.setDropdown("licensed_locations_id", cert.getLicensed_locations_id());
		submitLocation() ;
		
		//if ( TestUtils.findText(driver, cert.getLicensed_locations_name())) ;
		this.setCheckbox("certificates_location_can_view", cert.isCertificates_location_can_view()) ;
		this.setCheckbox("certificates_location_can_edit", cert.isCertificates_location_can_edit()) ;
				
		WebElement vendor = driver.findElement(By.id("join_vendors_id")) ;
		if ( vendor.isDisplayed() && vendor.isEnabled() )
		{
			this.setDropdown("join_vendors_id", cert.getJoin_vendors_id());
		}

		this.setDropdown("certificates_type", cert.getCertificates_type()) ;
		if ( "special".equalsIgnoreCase(cert.getCertificates_type()))
		{
			this.setTextbox("certificates_project_name", cert.getCertificates_project_name());
		}
		//#add_file
		this.setCheckbox("coverage_gl", cert.isCoverage_gl()) ;
		System.out.println("GL Coverage: " + cert.isCoverage_gl());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		if ( cert.isCoverage_gl() )
		{
			this.setTextbox("coverage_gl_amount", cert.getCoverage_gl_amount());
		}
		this.setCheckbox("coverage_comp", cert.isCoverage_comp()) ;
		if ( cert.isCoverage_comp() )
		{
			this.setTextbox("coverage_comp_amount", cert.getCoverage_comp_amount());
			this.setTextbox("coverages_amount2", cert.getCoverages_amount2());
			this.setTextbox("coverages_amount3", cert.getCoverages_amount3());
		}
		this.setCheckbox("coverage_auto", cert.isCoverage_auto()) ;
		if ( cert.isCoverage_auto() )
		{
			this.setTextbox("coverage_auto_amount", cert.getCoverage_auto_amount());
		}
		this.setCheckbox("coverage_umbrella", cert.isCoverage_umbrella()) ;
		if ( cert.isCoverage_umbrella() )
		{
			this.setTextbox("coverage_umbrella_amount", cert.getCoverage_umbrella_amount());
		}
		this.setCheckbox("coverage_el", cert.isCoverage_el()) ;
		if ( cert.isCoverage_el() )
		{
			this.setTextbox("coverage_el_amount", cert.getCoverage_el_amount());
		}
		this.setCheckbox("coverage_crime", cert.isCoverage_crime()) ;
		if ( cert.isCoverage_crime() )
		{
			this.setTextbox("coverage_crime_amount", cert.getCoverage_crime_amount());
		}
		this.setCheckbox("coverage_prop", cert.isCoverage_prop()) ;
		if ( cert.isCoverage_prop() )
		{
			this.setTextbox("coverage_prop_amount", cert.getCoverage_prop_amount());
		}
		this.setCheckbox("coverage_prof", cert.isCoverage_prof()) ;
		if ( cert.isCoverage_prof() )
		{
			this.setTextbox("coverage_prof_amount", cert.getCoverage_prof_amount());
		}
		this.setCheckbox("coverage_other", cert.isCoverage_other()) ;
		WebElement otherCoverage = driver.findElement(By.id("certificates_coverage_other")) ;
		if ( otherCoverage.isDisplayed() && otherCoverage.isEnabled() )
		{
			this.setTextbox("certificates_coverage_other", cert.getCertificates_coverage_other()) ;
		}
		this.setCheckbox("certificates_request_cert_to_email_chk", cert.isCertificates_request_cert_to_email_chk()) ;
		WebElement emailAddress = driver.findElement(By.id("certificates_request_cert_to_email")) ;
		if ( emailAddress.isDisplayed() && emailAddress.isEnabled() )
		{
			this.setTextbox("certificates_request_cert_to_email", cert.getCertificates_request_cert_to_email()) ;
		}
		this.setCheckbox("certificates_request_cert_to_fax", cert.isCertificates_request_cert_to_fax()) ;
		this.setCheckbox("certificates_request_cert_to_address", cert.isCertificates_request_cert_to_address()) ;

		this.setCheckbox("request_cert", cert.isRequest_cert()) ;
		WebElement emailSubject = driver.findElement(By.id("certificates_email_subject")) ;
		if ( emailSubject.isDisplayed() && emailSubject.isEnabled() )
		{
			//this.setTextbox("subject_line", cert.getSubject_line()) ;
		}
		//certificates_expire 
		selectCalendarDate("certificates_expire", cert.getCertificates_expire());
		
		this.setDropdown("certificates_email_days", cert.getCertificates_email_days()) ;
		this.setCheckbox("certificates_remind_vendor", cert.isCertificates_remind_vendor()) ;
		List<String> certRemindBefore = cert.getCertificate_remind_members_before_expire() ;
		if ( certRemindBefore != null && certRemindBefore.size() > 0 )
		{
			for ( int i = 0 ; i < certRemindBefore.size() ; i++ )
			{
				String reminder = (String)certRemindBefore.get(i) ;
				if ( reminder != null && reminder.trim().length() > 0 )
				{
					String reminderId = "certificate_remind_members_before_expire_" + reminder ;
					WebElement el = driver.findElement(By.id(reminderId)) ;
					if ( el != null ) el.click();
				}
			}
		}
		this.setCheckbox("certificates_remind_other_chk", cert.isCertificates_remind_other_chk()) ;
		this.setTextbox("certificates_remind_other", cert.getCertificates_remind_other()) ;
		
		this.setDropdown("certificates_remind_member", cert.getCertificates_remind_member()) ;
		this.setCheckbox("certificates_remind_vendor_expired", cert.isCertificates_remind_vendor_expired()) ;
		List<String> certRemindAfter = cert.getCertificate_remind_members_after_expire() ;
		if ( certRemindAfter != null && certRemindAfter.size() > 0 )
		{
			for ( int i = 0 ; i < certRemindAfter.size() ; i++ )
			{
				String reminder = (String)certRemindAfter.get(i) ;
				if ( reminder != null && reminder.trim().length() > 0 )
				{
					String reminderId = "certificate_remind_members_after_expire_" + reminder ;
					WebElement el = driver.findElement(By.id(reminderId)) ;
					if ( el != null ) el.click();
				}
			}
		}
		this.setCheckbox("certificates_remind_other_expired_chk", cert.isCertificates_remind_other_expired_chk()) ;
		WebElement remindOther = driver.findElement(By.id("certificates_remind_other_expired")) ;
		if ( remindOther.isDisplayed() && remindOther.isEnabled() )
		{
			this.setTextbox("certificates_remind_other_expired", cert.getCertificates_remind_other_expired()) ;
		}
		this.setFileChooserByName("certificates_file[]", cert.getFileName()) ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

	}

	public void submitLocation()
	{
		driver.findElement(By.id("licensed_locations_id")).submit();
	}
	
	public void navigateTo()
	{
		//TestUtils.chooseMenuItem(driver, "Tracking", "Certificates");
		driver.findElement(By.linkText("Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Certificate")).click();
	}
	public void submitForm()
	{
		this.driver.findElement(By.id("save")).click() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Certificates List")).click() ;
	}

}
