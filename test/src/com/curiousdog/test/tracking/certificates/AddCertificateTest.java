package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddCertificateTest 
{
	static AddCertificateForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
				
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddCertificateForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitTest()
	{
		Certificate cert = new Certificate(bundle, "TRACKING.CERTIFICATES.CERTIFICATE.ADD.1") ;
		
		form.navigateTo();
		form.justChooseLocation(cert);
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		//driver.switchTo().activeElement();
		form.assertTextExists("Please select a vendor", "Error message for missing vendor not displayed") ;
		//form.assertTextExists("There are errors below", "Notification about errors did not appear");
		form.assertTextExists("Please select at least one coverage type", "Error message for missing coverage type not displayed") ;
		form.assertTextExists("Please select someone to receive the email", "Error message for missing email addresses not displayed") ;
	}

	@Test
	public void SuccessfulFormSubmitTest()
	{
		Certificate cert = new Certificate(bundle, "TRACKING.CERTIFICATES.CERTIFICATE.ADD.1") ;

		form.navigateTo();
		form.fillOut(cert);
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		form.assertNavigate("/members/my-documents/certificates/index.php?notice=Certificate+added");
		form.assertTextExists("Certificate added!", "Message certificate added is not displayed");
	}

	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}


}
