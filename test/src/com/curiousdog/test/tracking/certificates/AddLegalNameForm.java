package com.curiousdog.test.tracking.certificates;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddLegalNameForm extends WebForm
{
	public AddLegalNameForm(WebDriver driver)
	{
		super(driver) ;
	} 
	
	public void navigateTo()
	{
		driver.findElement(By.linkText("Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.findElement(By.linkText("Certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		driver.findElement(By.linkText("Manage Legal Entity Names")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Add Legal Entity Name")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}
	
	public void fillOut(LegalEntityName entity)
	{
		this.setDropdown("join_locations_id", entity.getLocationId());
		this.setTextbox("legal_entity_names_name", entity.getEntityName());
	}
	
	public void submitForm()
	{
		driver.findElement(By.id("save")).click();
	}
	
	public void cancelForm()
	{
		driver.findElement(By.linkText("Return to Legal Entity List")).click() ;
	}
}
