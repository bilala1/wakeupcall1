package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class AddLegalNameTest 
{
	static AddLegalNameForm form ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;

	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
				
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddLegalNameForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void submitFilledForm()
	{
		LegalEntityName name = new LegalEntityName(bundle, "TRACKING.CERTIFICATES.LEGALNAME.ADD.1") ;
		form.navigateTo();
		form.fillOut(name);
		form.submitForm();
		
		form.assertNavigate("members/my-documents/certificates/legal_names/index.php?notice=Added");
		form.assertTextExists("Added", "No message 'Added' at the top of the form");
		form.assertTextExists(name.getEntityName(), "Name of the new entity did not show up on the screen");
	}

	@Test
	public void submitEmptyForm()
	{
		form.navigateTo();
		form.submitForm();
		
		form.assertTextExists("There are errors below", "There's no message about errors on the form");
		form.assertTextExists("Please enter something", "There's no message 'enter something'") ;
		form.assertTextExists("You must enter a value for Legal Entity Name", "There's no message to enter entity name") ;
	}
	
	@Test
	public void cancelFormTest()
	{
		form.navigateTo();
		form.cancelForm();
		
		form.assertNavigate("members/my-documents/certificates/legal_names/index.php");
	}
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	

}
