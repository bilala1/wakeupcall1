package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class EditLegalNameTest 
{
	static EditLegalNameForm form ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;

	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
				
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new EditLegalNameForm(driver) ;
		form.login(user, password);
	}
	
	@Test
	public void submitFilledForm()
	{
		LegalEntityName name = new LegalEntityName(bundle, "TRACKING.CERTIFICATES.LEGALNAME.EDIT.1") ;
		form.navigateTo(name.getId());
		form.fillOut(name);
		form.submitForm();
		
		form.assertNavigate("members/my-documents/certificates/legal_names/index.php?notice=Updated");
		form.assertTextExists("Updated", "No message 'Updated' at the top of the form");
		form.assertTextExists(name.getEntityName(), "Name of the new entity did not show up on the screen");
	}
	
	@Test
	public void cancelFormTest()
	{
		LegalEntityName name = new LegalEntityName(bundle, "TRACKING.CERTIFICATES.LEGALNAME.EDIT.1") ;
		form.navigateTo(name.getId());
		form.cancelForm();
		
		form.assertNavigate("members/my-documents/certificates/legal_names/index.php");
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	

}
