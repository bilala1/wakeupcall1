package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class LegalEntityName 
{
	private String id ;
	private String locationId ;
	private String entityName ;
	
	public LegalEntityName()
	{
		super() ;
	}
	
	public LegalEntityName(ResourceBundle bundle, String tag)
	{
		this.id             = TestUtils.getProperty(bundle, tag + ".id") ;
		this.locationId		= TestUtils.getProperty(bundle, tag + ".join_locations_id") ;
		this.entityName		= TestUtils.getProperty(bundle, tag + ".legal_entity_names_name") ;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getLocationId() {
		return locationId;
	}
	
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	public String getEntityName() {
		return entityName;
	}
	
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	
	
}
