package com.curiousdog.test.tracking.certificates;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Vendor 
{
	private String id ;
	private String name ;
	private String contactName ;
	private String email ;
	private String phone ;
	private String address ;
	private String services ;
	
	private boolean locationCanView = false ;
	private boolean locationCanEdit = false ;
	
	private List<String> locationId = null ;
	
	public Vendor()
	{
		super() ;
		locationId = new ArrayList<String>() ;
	}
	public Vendor(ResourceBundle bundle, String configString)
	{
		this() ;
		this.id             = TestUtils.getProperty(bundle, configString + ".id") ;
		this.name 			= TestUtils.getProperty(bundle, configString + ".vendors_name") ;
		this.contactName	= TestUtils.getProperty(bundle, configString + ".vendors_contact_name") ;
		this.email			= TestUtils.getProperty(bundle, configString + ".vendors_email") ;
		this.services		= TestUtils.getProperty(bundle, configString + ".vendors_services") ;
		this.phone			= TestUtils.getProperty(bundle, configString + ".vendors_phone") ;
		this.address		= TestUtils.getProperty(bundle, configString + ".vendors_street") ;
		
		String locations    = TestUtils.getProperty(bundle, configString + ".licensed_locations") ;
		this.locationId		= TestUtils.splitString(locations, ',') ;
		this.locationCanEdit = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".vendors_location_can_edit")) ;
		this.locationCanView = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, configString + ".vendors_location_can_view")) ;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public boolean isLocationCanView() {
		return locationCanView;
	}

	public void setLocationCanView(boolean locationCanView) {
		this.locationCanView = locationCanView;
	}

	public boolean isLocationCanEdit() {
		return locationCanEdit;
	}

	public void setLocationCanEdit(boolean locationCanEdit) {
		this.locationCanEdit = locationCanEdit;
	}

	public List<String> getLocationId() {
		return locationId;
	}

	public void setLocationId(List<String> location) {
		this.locationId = location;
	}
	
	
	
}
