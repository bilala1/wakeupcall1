package com.curiousdog.test.tracking.certificates;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class VendorAddForm extends WebForm
{
	public VendorAddForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Vendor vendor)
	{
		List<String> locsList = vendor.getLocationId() ;
		if ( locsList != null && locsList.size() > 0 )
		{
			for ( int i = 0 ; i < locsList.size() ; i++ )
			{
				String locId = locsList.get(i) ;
				if ( locId != null && locId.trim().length() > 0 ) 
				{
					String locLocId = "licensed_locations_" + locId.trim() ;
					this.setCheckbox(locLocId, true);
				}
			}
		}
		
		setCheckbox("vendors_location_can_view"	, vendor.isLocationCanView()) ;
		setCheckbox("vendors_location_can_edit"	, vendor.isLocationCanEdit());
		setTextbox ("vendors_name"				, vendor.getName()) ;
		setTextbox ("vendors_contact_name"		, vendor.getContactName());
		setTextbox ("vendors_email"				, vendor.getEmail());
		setTextbox ("vendors_services"			, vendor.getServices());
		setTextbox ("vendors_phone"				, vendor.getPhone()) ;
		setTextbox ("vendors_street"			, vendor.getAddress()) ;
	}
	
	public void navigateToMe()
	{
		//TestUtils.chooseMenuItem(driver, "Tracking", "Certificates");
		driver.findElement(By.linkText("Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.findElement(By.linkText("Certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		driver.findElement(By.linkText("Manage Vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Add Vendor")).click();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}
	
	public void submitForm()
	{
		this.driver.findElement(By.id("vendors_name")).submit() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Back to Certificates")).click() ;
	}
}
