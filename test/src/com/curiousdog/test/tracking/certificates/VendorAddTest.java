package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;

public class VendorAddTest 
{
	static VendorAddForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;

	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
				
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new VendorAddForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitTest()
	{
		form.navigateToMe();
		form.submitForm();
		
		form.assertTextExists("Please select at least one location", "Message to select location is not displayed");
		form.assertTextExists("Please enter something", "Message to 'enter something' is not displayed") ;
		form.assertTextExists("You must enter a properly formatted email", "Message to enter a proper email is not displayed");
	}

	@Test
	public void SuccessfulFormSubmitTest()
	{
		Vendor vendor = new Vendor(bundle, "TRACKING.CERTIFICATES.VENDOR.ADD.1") ;
		form.navigateToMe();
		form.fillOut(vendor);
		form.submitForm();
		form.assertNavigate("/members/vendors/index.php?notice=Vendor+added") ;
		form.assertTextExists("Vendor added!", "Vendor added message is not displayed") ;
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	
}
