package com.curiousdog.test.tracking.certificates;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class VendorEditForm extends WebForm
{
	public VendorEditForm(WebDriver drv)
	{
		super(drv) ;
	}

	public void navigateTo(String id)
	{
		//TestUtils.chooseMenuItem(driver, "Tracking", "Certificates");
		driver.findElement(By.linkText("Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.findElement(By.linkText("Certificates")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		driver.findElement(By.linkText("Manage Vendors")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		System.out.println("EDIT VENDOR: " + this.appHomePage + "/members/vendors/edit.php?vendors_id=" + id);
		driver.get(this.appHomePage + "/members/vendors/edit.php?vendors_id=" + id) ;		
	}
	
	public void submitForm()
	{
		this.driver.findElement(By.id("vendors_name")).submit() ;
	}

	public void fillOut(Vendor vendor)
	{
		List<WebElement> locations = driver.findElements(By.xpath("//*[contains(@id, 'licensed_locations_')]")) ;
		for ( int i = 0 ; i < locations.size() ; i++ ) setCheckbox(locations.get(i).getAttribute("id"), false) ;
		
		List locIds = vendor.getLocationId() ;
		for ( int j = 0 ; j < locIds.size() ; j++ )
		{
			String temp = "licensed_locations_" + locIds.get(j) ;
			for ( int i = 0 ; i < locations.size() ; i++ )
			{
				if ( temp.equalsIgnoreCase(locations.get(i).getAttribute("id")) ) 
				{
					System.out.println("Found: " + locations.get(i).getAttribute("id"));
					setCheckbox(temp, true) ;
				}
			}
		}
		setCheckbox("vendors_location_can_view"	, vendor.isLocationCanView()) ;
		setCheckbox("vendors_location_can_edit"	, vendor.isLocationCanEdit());
		setTextbox ("vendors_name"				, vendor.getName()) ;
		setTextbox ("vendors_contact_name"		, vendor.getContactName());
		setTextbox ("vendors_email"				, vendor.getEmail());
		setTextbox ("vendors_services"			, vendor.getServices());
		setTextbox ("vendors_phone"				, vendor.getPhone()) ;
		setTextbox ("vendors_street"			, vendor.getAddress()) ;
	}

	public void clearForm()
	{
		List<WebElement> locations = driver.findElements(By.xpath("//*[starts-with(@id,'licensed_locations_')]")) ;
		for ( WebElement el : locations )
		{
			el.clear();
		}
		driver.findElement(By.id("vendors_location_can_view")).clear();
		driver.findElement(By.id("vendors_location_can_edit")).clear();
		driver.findElement(By.id("vendors_name")).clear();
		driver.findElement(By.id("vendors_contact_name")).clear();
		driver.findElement(By.id("vendors_email")).clear();
		driver.findElement(By.id("vendors_services")).clear();
		driver.findElement(By.id("vendors_phone")).clear();
		driver.findElement(By.id("vendors_street")).clear();		
	}
	
	public void cancelForm()
	{
		driver.findElement(By.linkText("Back to Vendor List")).click();
	}
}
