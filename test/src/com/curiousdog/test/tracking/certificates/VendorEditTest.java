package com.curiousdog.test.tracking.certificates;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

public class VendorEditTest 
{
	static VendorEditForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	static Vendor vendor = null ;
	
	@BeforeClass
	public static void startTest()
	{
		System.out.println("Starting edit vendor test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
	
		
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
		
		form = new VendorEditForm(driver) ;
		form.login(user, password);
	}
	
	@AfterClass
	public static void endTest()
	{
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		driver.findElement(By.linkText("Logout")).click();
		driver.close();
		
		System.out.println("Test completed");
	}

	@Test 
	public void testChange()
	{
		vendor = new Vendor(bundle, "TRACKING.CERTIFICATES.VENDOR.EDIT.1") ;
		System.out.println("Location id to edit = " + vendor.getId()) ;
		form.navigateTo(vendor.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		form.fillOut(vendor);
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		form.submitForm();
		form.assertNavigate("/members/vendors/index.php?notice=Vendor+updated");
		form.assertTextExists("Vendor updated!", "No message that vendor was updated");
	}
	
	@Test
	public void testCancelForm()
	{
		vendor = new Vendor(bundle, "TRACKING.CERTIFICATES.VENDOR.EDIT.1") ;
		form.navigateTo(vendor.getId());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		form.fillOut(vendor);
		form.cancelForm();
	}
}
