package com.curiousdog.test.tracking.claims;

import java.text.SimpleDateFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddCarrierForm extends WebForm
{
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy") ;
	
	public AddCarrierForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Carrier carrier)
	{
		
		this.setTextbox("carriers_name", carrier.getCarriers_name()) ; 
		this.setTextbox("carriers_contact", carrier.getCarriers_contact()) ;
		this.setTextbox("carriers_email", carrier.getCarriers_email()) ;
		this.setCheckboxByName("carriers_no_email", carrier.isCarriers_no_email()) ;
		this.setTextbox("carriers_phone", carrier.getCarriers_phone()) ;
		this.setTextbox("carriers_fax", carrier.getCarriers_fax()) ;
		this.setTextbox("carriers_policy_number", carrier.getCarriers_policy_number()) ;
		this.setTextbox("carriers_address", carrier.getCarriers_address()) ;
		//driver.findElement(By.id("carriers_effective_start_date")).click(); 
		//driver.findElement(By.id("carriers_effective_start_date")).sendKeys(sdf.format(carrier.getCarriers_effective_start_date())) ;
		//this.setTextbox("carriers_effective_start_date", carrier.getCarriers_effective_start_date()) ;
		//this.setTextbox("carriers_effective_end_date", carrier.getCarriers_effective_end_date()) ;
		this.selectCalendarDate("carriers_effective_start_date", carrier.getCarriers_effective_start_date());
		System.out.println("Entered the first date: " + carrier.getCarriers_effective_start_date());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//driver.findElement(By.id("carriers_address")).click(); 
		
		//driver.findElement(By.id("carriers_effective_end_date")).click(); 
		//driver.findElement(By.id("carriers_effective_end_date")).sendKeys(sdf.format(carrier.getCarriers_effective_start_date())) ;
		this.selectCalendarDate("carriers_effective_end_date", carrier.getCarriers_effective_end_date()) ;
		System.out.println("Entered the second date: " + carrier.getCarriers_effective_end_date());
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
		//driver.findElement(By.id("carriers_address")).click();
		
		this.setCheckbox("coverage_auto", carrier.isCoverage_auto()) ;
		this.setCheckbox("coverage_broker", carrier.isCoverage_broker()) ;
		this.setCheckbox("coverage_crime", carrier.isCoverage_crime()) ;
		this.setCheckbox("coverage_dampo", carrier.isCoverage_dampo()) ;
		this.setCheckbox("coverage_eampo", carrier.isCoverage_eampo()) ;
		this.setCheckbox("coverage_epli", carrier.isCoverage_epli()) ;
		this.setCheckbox("coverage_general-liability", carrier.isCoverage_gen_liability()) ;
		this.setCheckbox("coverage_package-propertyglcrimeauto", carrier.isCoverage_package_propertyglcrimeauto()) ;
		this.setCheckbox("coverage_property", carrier.isCoverage_property()) ;
		this.setCheckbox("coverage_umbrella", carrier.isCoverage_umbrella()) ;
		this.setCheckbox("coverage_workers-compensation", carrier.isCoverage_workers_compensation()) ;
		this.setCheckbox("coverage_other", carrier.isCoverage_other()) ;
		
		//Clear all carrier checkboxes:
		List<WebElement> carriers = driver.findElements(By.xpath("//*[contains(@name, 'carrier_')]"))  ;
		for ( int i = 0 ; i < carriers.size() ; i++ ) 
		{
			WebElement cbox = carriers.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		//Clear all location can edit
		List<WebElement> canEdits = driver.findElements(By.xpath("//*[contains(@name, 'carrier_location_can_edit_')]"))  ;
		for ( int i = 0 ; i < canEdits.size() ; i++ ) 
		{	
			WebElement cbox = canEdits.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		
		if ( carrier.isCheck_all_box_carrier() )
		{
			this.setCheckbox("check_all_box_carrier", carrier.isCheck_all_box_carrier()) ;
		}
		else
		{
			//by name
			List<String> crs = carrier.getCarrier() ;
			if ( crs != null && crs.size() > 0 )
			{
				for ( int i = 0 ; i < crs.size() ; i++ )
				{
					String boxName = "carrier_" + crs.get(i) ;
					driver.findElement(By.name(boxName)).click();
				}
			}
			//by name
			List<String> crLocs = carrier.getCarrier() ;
			if ( crLocs != null && crLocs.size() > 0 )
			{
				for ( int i = 0 ; i < crLocs.size() ; i++ )
				{
					String boxName = "carrier_location_can_edit_" + crLocs.get(i) ;
					driver.findElement(By.name(boxName)).click();
				}
			}
		}
	}

	public void navigateTo()
	{
		//TestUtils.chooseMenuItem(driver, "Tracking", "Claims");
		driver.findElement(By.linkText("Tracking")).click() ;
		driver.findElement(By.linkText("Claims")).click();
		driver.findElement(By.linkText("Manage Carriers/Recipients")).click() ;
		//Go to add new Carriers/Recipients form
		driver.findElement(By.partialLinkText("Add")).click();
		//Choose carrier (and not recipient)
		this.setDropdown("carriers_recipient", "0");
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
	}
	
	public void submitForm()
	{
		this.driver.findElement(By.id("save_button")).submit() ;
	}
	
	public void cancelForm()
	{
		this.driver.findElement(By.linkText("Return to Carrier/Recipient List")).click() ;
	}
	
}
