package com.curiousdog.test.tracking.claims;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.tracking.certificates.Certificate;
import com.curiousdog.test.utils.TestUtils;

public class AddCarrierTest 
{
	static AddCarrierForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		driver = TestUtils.openChrome() ;

		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddCarrierForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitTest()
	{
		form.navigateTo();
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		form.assertTextExists("There are errors below", "Message about error on the page is not displayed") ;
		form.assertTextExists("Please enter something", "Error message for missing SOMETHING is not displayed") ;
		form.assertTextExists("You must enter a properly formatted email", "Error message for missing email addresses not displayed") ;
		form.assertTextExists("You must enter a value for Fax Number", "Error message for Fax number is not displayed") ;
		form.assertTextExists("You must enter a date", "Error message for missing a date is not displayed") ;
	}
	
	@Test
	public void SuccessfulFormSubmitTest()
	{
		Carrier carrier = new Carrier(bundle, "TRACKING.CLAIMS.CARRIERS.CARRIER.ADD.1") ;

		form.navigateTo();
		form.fillOut(carrier);
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;

		form.assertNavigate("/members/claims/carriers/index.php?notice=Carrier+added");
		form.assertTextExists("Carrier added!", "Message carrier added is not displayed");
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}


	//
	//
	//
	//
	//
}
