package com.curiousdog.test.tracking.claims;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.bean.Claim;
import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddClaimForm extends WebForm
{
	public AddClaimForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Claim vendor)
	{

	}

	public void navigateToMe()
	{
		TestUtils.chooseMenuItem(driver, "Tracking", "Claims");
		driver.findElement(By.linkText("Add Claim")).click();
	}
	
	public void submitToCarrierForm()
	{
		this.driver.findElement(By.id("saveAndSubmit")).click() ;
	}
	
	public void saveForRecordsForm()
	{
		this.driver.findElement(By.id("saveOnly")).click() ;
	}

	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Claims List")).click() ;
	}
	
	
}
