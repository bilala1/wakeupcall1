package com.curiousdog.test.tracking.claims;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.utils.TestUtils;


public class AddClaimFormTest 
{
	static AddClaimForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddClaimForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitToCarrierTest()
	{
		form.navigateToMe();
		form.submitToCarrierForm();
		
	}
	@Test
	public void EmptyFormSaveForRecordsTest()
	{
		form.navigateToMe();
		form.saveForRecordsForm();
		
	}

	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}
	
}
