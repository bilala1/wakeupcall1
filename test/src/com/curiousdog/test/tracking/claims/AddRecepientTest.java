package com.curiousdog.test.tracking.claims;

	import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.curiousdog.test.utils.TestUtils;

	public class AddRecepientTest 
	{
		static AddRecipientForm form = null ;
		static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
		static WebDriver driver = null ;
		
		@BeforeClass
		public static void startTest()
		{
			//driver = new FirefoxDriver() ;
			driver = TestUtils.openChrome() ;

			String user 	= bundle.getString("LOGIN") ;
			String password = bundle.getString("PASSWORD") ;
			
			form = new AddRecipientForm(driver) ;
			form.login(user, password);
		}

		@Test
		public void EmptyFormSubmitTest()
		{
			form.navigateTo();
			form.submitForm();
			TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
			
			form.assertTextExists("Please select atleast one location", "Message 'atleast one location' is not displayed") ;
		}
		
		@Test
		public void SuccessfulFormSubmitTest()
		{
			Recepient recepient = new Recepient(bundle, "TRACKING.CLAIMS.RECEPIENTS.RECEPIENT.ADD.1") ;

			form.navigateTo();
			form.fillOut(recepient);
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
			form.submitForm();
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;

			form.assertNavigate("/members/claims/carriers/index.php?notice=Recipient+added%21");
			form.assertTextExists("Recipient added!", "Message 'Recipient added!' is not displayed");
		}
		
		@AfterClass
		public static void LogoutAndClose()
		{
			driver.findElement(By.linkText("Logout")).click() ;
			driver.close();
		}

}
