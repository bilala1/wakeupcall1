package com.curiousdog.test.tracking.claims;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.bean.Claim;
import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddRecipientForm extends WebForm
{
	public AddRecipientForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Recepient recepient)
	{
		this.setTextbox("carriers_name", recepient.getCarriers_name());
		this.setTextbox("carriers_contact", recepient.getCarriers_contact());
		this.setTextbox("carriers_email", recepient.getCarriers_email());
		this.setCheckbox("checkbox_all_locations", recepient.isCheckbox_all_locations());
		
		//Clear all carrier checkboxes:
		List<WebElement> send_all = driver.findElements(By.xpath("//*[contains(@name, 'recipient_send_all_')]"))  ;
		for ( int i = 0 ; i < send_all.size() ; i++ ) 
		{
			WebElement cbox = send_all.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		
		List<WebElement> on_save = driver.findElements(By.xpath("//*[contains(@name, 'recipient_send_all_on_save_')]"))  ;
		for ( int i = 0 ; i < on_save.size() ; i++ ) 
		{
			WebElement cbox = on_save.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		List<WebElement> on_submission = driver.findElements(By.xpath("//*[contains(@name, 'recipient_send_all_on_submission_')]"))  ;
		for ( int i = 0 ; i < on_submission.size() ; i++ ) 
		{
			WebElement cbox = on_submission.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		List<WebElement> can_edit = driver.findElements(By.xpath("//*[contains(@name, 'recipient_location_can_edit_')]"))  ;
		for ( int i = 0 ; i < can_edit.size() ; i++ ) 
		{
			WebElement cbox = can_edit.get(i) ;
			if ( cbox.isDisplayed() && cbox.isEnabled() && cbox.isSelected() ) cbox.clear(); 
		}
		
		List<String> sendAllBoxes = recepient.getRecipient_send_all() ;
		if ( sendAllBoxes != null && sendAllBoxes.size() > 0 )
		{
			for ( int i = 0 ; i < sendAllBoxes.size() ; i++ )
			{
				String boxName = "recipient_send_all_" + sendAllBoxes.get(i) ;
				this.setCheckboxByName(boxName, true);
			}
		}
		List<String> allOnSaveBoxes = recepient.getRecipient_send_all_on_save() ;
		if ( allOnSaveBoxes != null && allOnSaveBoxes.size() > 0 )
		{
			for ( int i = 0 ; i < allOnSaveBoxes.size() ; i++  )
			{
				String boxName = "recipient_send_all_on_save_" + allOnSaveBoxes.get(i) ;
				this.setCheckboxByName(boxName, true);
			}
		}
		
		List<String> submissionBoxes = recepient.getRecipient_send_all_on_submission() ;
		if ( submissionBoxes != null && submissionBoxes.size() > 0 )
		{
			for ( int i = 0 ; i < submissionBoxes.size() ; i++  )
			{
				String boxName = "recipient_send_all_on_submission_" + submissionBoxes.get(i) ;
				this.setCheckboxByName(boxName, true);
			}
		}

		List<String> canEditBoxes = recepient.getRecipient_location_can_edit() ;
		if ( canEditBoxes != null && canEditBoxes.size() > 0 )
		{
			for ( int i = 0 ; i < canEditBoxes.size() ; i++  )
			{
				String boxName = "recipient_location_can_edit_" + canEditBoxes.get(i) ;
				this.setCheckboxByName(boxName, true);
			}
		}

	}

	public void navigateTo()
	{
		//TestUtils.chooseMenuItem(driver, "Tracking", "Claims");
		driver.findElement(By.linkText("Tracking")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		driver.findElement(By.linkText("Claims")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		driver.findElement(By.linkText("Manage Carriers/Recipients")).click() ;
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Carrier/Recipient")).click();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		//Choose recipient (and not carrier)
		this.setDropdown("carriers_recipient", "1");
		TestUtils.Pause(TestUtils.PAUSE_LONG) ;
		
	}
	public void submitForm()
	{
		this.driver.findElement(By.id("save_button")).click() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Carrier/Recipient List")).click() ;
	}

}
