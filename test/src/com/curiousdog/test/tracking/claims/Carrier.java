package com.curiousdog.test.tracking.claims;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Carrier 
{
	private String carriers_name ;
	private String carriers_contact ;
	private String carriers_email ;
	private boolean carriers_no_email ;
	private String carriers_phone ;
	private String carriers_fax ;
	private String carriers_policy_number ;
	private String carriers_address ;
	private Date carriers_effective_start_date ;
	private Date carriers_effective_end_date ;
	private boolean coverage_auto ;
	private boolean coverage_broker ;
	private boolean coverage_crime ;
 	private boolean coverage_dampo ;
	private boolean coverage_eampo ;
	private boolean coverage_epli ;
	private boolean coverage_gen_liability ;
 	private boolean coverage_package_propertyglcrimeauto ;
	private boolean coverage_property ;
	private boolean coverage_umbrella ;
	private boolean coverage_workers_compensation ;
	private boolean coverage_other ;
	private boolean check_all_box_carrier ;
	//by name
	private List<String> carrier ;
	//by name
	private List<String> carrier_location_can_edit ;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy") ;
	
	public Carrier() 
	{
		super() ;
	}
	public Carrier(ResourceBundle bundle, String key)
	{
		this() ;
		populate(bundle, key) ;
	}
	
	public void populate(ResourceBundle bundle, String key)
	{
		this.carriers_name = TestUtils.getProperty(bundle, key + ".carriers_name") ;
		this.carriers_contact = TestUtils.getProperty(bundle, key + ".carriers_contact") ;
		this.carriers_email = TestUtils.getProperty(bundle, key + ".carriers_email") ;
		this.carriers_no_email = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".carriers_no_email")) ;
		this.carriers_phone = TestUtils.getProperty(bundle, key + ".carriers_phone") ;
		this.carriers_fax = TestUtils.getProperty(bundle, key + ".carriers_fax") ;
		this.carriers_policy_number = TestUtils.getProperty(bundle, key + ".carriers_policy_number") ;
		this.carriers_address = TestUtils.getProperty(bundle, key + ".carriers_address") ;
		String startDate = TestUtils.getProperty(bundle, key + ".carriers_effective_start_date") ;
		try
		{
			this.carriers_effective_start_date = sdf.parse(startDate) ;
		}
		catch(Exception ex) { }
		String endDate = TestUtils.getProperty(bundle, key + ".carriers_effective_end_date") ;
		try
		{
			this.carriers_effective_end_date = sdf.parse(endDate) ;
		}
		catch(Exception ex) { }
		this.coverage_auto = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_auto")) ;
		this.coverage_broker = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_broker")) ;
		this.coverage_crime = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_crime")) ;
		this.coverage_dampo = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_dampo")) ;
		this.coverage_eampo = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_eampo")) ;
		this.coverage_epli = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_epli")) ;
		this.coverage_gen_liability = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_auto")) ;
		this.coverage_package_propertyglcrimeauto = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_package-propertyglcrimeauto")) ;
		this.coverage_property = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_property")) ;
		this.coverage_umbrella = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_umbrella")) ;
		this.coverage_workers_compensation = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_workers-compensation")) ;
		this.coverage_other = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".coverage_other")) ;
		this.check_all_box_carrier="Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".check_all_box_carrier")) ;
		String carrierStr = TestUtils.getProperty(bundle, key + ".carrier") ;
		this.carrier = TestUtils.splitString(carrierStr, ',') ;
		String locationCanEdit = TestUtils.getProperty(bundle, key + ".carrier_location_can_edit") ;
		carrier_location_can_edit = TestUtils.splitString(locationCanEdit, ',') ;	
	}
	public String getCarriers_name() {
		return carriers_name;
	}
	public void setCarriers_name(String carriers_name) {
		this.carriers_name = carriers_name;
	}
	public String getCarriers_contact() {
		return carriers_contact;
	}
	public void setCarriers_contact(String carriers_contact) {
		this.carriers_contact = carriers_contact;
	}
	public String getCarriers_email() {
		return carriers_email;
	}
	public void setCarriers_email(String carriers_email) {
		this.carriers_email = carriers_email;
	}
	public boolean isCarriers_no_email() {
		return carriers_no_email;
	}
	public void setCarriers_no_email(boolean carriers_no_email) {
		this.carriers_no_email = carriers_no_email;
	}
	public String getCarriers_phone() {
		return carriers_phone;
	}
	public void setCarriers_phone(String carriers_phone) {
		this.carriers_phone = carriers_phone;
	}
	public String getCarriers_fax() {
		return carriers_fax;
	}
	public void setCarriers_fax(String carriers_fax) {
		this.carriers_fax = carriers_fax;
	}
	public String getCarriers_policy_number() {
		return carriers_policy_number;
	}
	public void setCarriers_policy_number(String carriers_policy_number) {
		this.carriers_policy_number = carriers_policy_number;
	}
	public String getCarriers_address() {
		return carriers_address;
	}
	public void setCarriers_address(String carriers_address) {
		this.carriers_address = carriers_address;
	}
	public Date getCarriers_effective_start_date() {
		return carriers_effective_start_date;
	}
	public void setCarriers_effective_start_date(Date carriers_effective_start_date) {
		this.carriers_effective_start_date = carriers_effective_start_date;
	}
	public Date getCarriers_effective_end_date() {
		return carriers_effective_end_date;
	}
	public void setCarriers_effective_end_date(Date carriers_effective_end_date) {
		this.carriers_effective_end_date = carriers_effective_end_date;
	}
	public boolean isCoverage_auto() {
		return coverage_auto;
	}
	public void setCoverage_auto(boolean coverage_auto) {
		this.coverage_auto = coverage_auto;
	}
	public boolean isCoverage_broker() {
		return coverage_broker;
	}
	public void setCoverage_broker(boolean coverage_broker) {
		this.coverage_broker = coverage_broker;
	}
	public boolean isCoverage_crime() {
		return coverage_crime;
	}
	public void setCoverage_crime(boolean coverage_crime) {
		this.coverage_crime = coverage_crime;
	}
	public boolean isCoverage_dampo() {
		return coverage_dampo;
	}
	public void setCoverage_dampo(boolean coverage_dampo) {
		this.coverage_dampo = coverage_dampo;
	}
	public boolean isCoverage_eampo() {
		return coverage_eampo;
	}
	public void setCoverage_eampo(boolean coverage_eampo) {
		this.coverage_eampo = coverage_eampo;
	}
	public boolean isCoverage_epli() {
		return coverage_epli;
	}
	public void setCoverage_epli(boolean coverage_epli) {
		this.coverage_epli = coverage_epli;
	}
	public boolean isCoverage_gen_liability() {
		return coverage_gen_liability;
	}
	public void setCoverage_gen_liability(boolean coverage_gen_liability) {
		this.coverage_gen_liability = coverage_gen_liability;
	}
	public boolean isCoverage_package_propertyglcrimeauto() {
		return coverage_package_propertyglcrimeauto;
	}
	public void setCoverage_package_propertyglcrimeauto(boolean coverage_package_propertyglcrimeauto) {
		this.coverage_package_propertyglcrimeauto = coverage_package_propertyglcrimeauto;
	}
	public boolean isCoverage_property() {
		return coverage_property;
	}
	public void setCoverage_property(boolean coverage_property) {
		this.coverage_property = coverage_property;
	}
	public boolean isCoverage_umbrella() {
		return coverage_umbrella;
	}
	public void setCoverage_umbrella(boolean coverage_umbrella) {
		this.coverage_umbrella = coverage_umbrella;
	}
	public boolean isCoverage_workers_compensation() {
		return coverage_workers_compensation;
	}
	public void setCoverage_workers_compensation(boolean coverage_workers_compensation) {
		this.coverage_workers_compensation = coverage_workers_compensation;
	}
	public boolean isCoverage_other() {
		return coverage_other;
	}
	public void setCoverage_other(boolean coverage_other) {
		this.coverage_other = coverage_other;
	}
	public boolean isCheck_all_box_carrier() {
		return check_all_box_carrier;
	}
	public void setCheck_all_box_carrier(boolean check_all_box_carrier) {
		this.check_all_box_carrier = check_all_box_carrier;
	}
	public List<String> getCarrier() {
		return carrier;
	}
	public void setCarrier(List<String> carrier) {
		this.carrier = carrier;
	}
	public List<String> getCarrier_location_can_edit() {
		return carrier_location_can_edit;
	}
	public void setCarrier_location_can_edit(List<String> carrier_location_can_edit) {
		this.carrier_location_can_edit = carrier_location_can_edit;
	}
	
}
