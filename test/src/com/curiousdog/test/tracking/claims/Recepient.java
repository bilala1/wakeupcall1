package com.curiousdog.test.tracking.claims;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

public class Recepient 
{
	private String carriers_name ;
	private String carriers_contact ;
	private String carriers_email ;
	private boolean checkbox_all_locations ;
	//by name
	List<String> recipient_send_all ;
	List<String> recipient_send_all_on_save ;
	List<String> recipient_send_all_on_submission ;
	List<String> recipient_location_can_edit ;

	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy") ;
	
	public Recepient() 
	{
		super() ;
	}
	public Recepient(ResourceBundle bundle, String key)
	{
		this() ;
		populate(bundle, key) ;
	}
	
	public void populate(ResourceBundle bundle, String key)
	{
		this.carriers_name = TestUtils.getProperty(bundle, key + ".carriers_name") ;
		this.carriers_contact = TestUtils.getProperty(bundle, key + ".carriers_contact") ;
		this.carriers_email = TestUtils.getProperty(bundle, key + ".carriers_email") ;
		this.checkbox_all_locations = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".checkbox_all_locations")) ;
		//by name
		String send_all 						= TestUtils.getProperty(bundle, key + ".recipient_send_all") ;
		this.recipient_send_all 				= TestUtils.splitString(send_all, ',') ;
		String on_save 							= TestUtils.getProperty(bundle, key + ".recipient_send_all_on_save") ;
		this.recipient_send_all_on_save 		= TestUtils.splitString(on_save, ',') ;
		String on_submission 					= TestUtils.getProperty(bundle, key + ".recipient_send_all_on_submission") ;
		this.recipient_send_all_on_submission 	= TestUtils.splitString(on_submission, ',') ;
		String location_can_edit 				= TestUtils.getProperty(bundle, key + ".recipient_location_can_edit") ;
		this.recipient_location_can_edit 		= TestUtils.splitString(location_can_edit, ',') ;
	}
	
	public String getCarriers_name() {
		return carriers_name;
	}
	public void setCarriers_name(String carriers_name) {
		this.carriers_name = carriers_name;
	}
	public String getCarriers_contact() {
		return carriers_contact;
	}
	public void setCarriers_contact(String carriers_contact) {
		this.carriers_contact = carriers_contact;
	}
	public String getCarriers_email() {
		return carriers_email;
	}
	public void setCarriers_email(String carriers_email) {
		this.carriers_email = carriers_email;
	}
	public boolean isCheckbox_all_locations() {
		return checkbox_all_locations;
	}
	public void setCheckbox_all_locations(boolean checkbox_all_locations) {
		this.checkbox_all_locations = checkbox_all_locations;
	}
	public List<String> getRecipient_send_all() {
		return recipient_send_all;
	}
	public void setRecipient_send_all(List<String> recipient_send_all) {
		this.recipient_send_all = recipient_send_all;
	}
	public List<String> getRecipient_send_all_on_save() {
		return recipient_send_all_on_save;
	}
	public void setRecipient_send_all_on_save(List<String> recipient_send_all_on_save) {
		this.recipient_send_all_on_save = recipient_send_all_on_save;
	}
	public List<String> getRecipient_send_all_on_submission() {
		return recipient_send_all_on_submission;
	}
	public void setRecipient_send_all_on_submission(List<String> recipient_send_all_on_submission) {
		this.recipient_send_all_on_submission = recipient_send_all_on_submission;
	}
	public List<String> getRecipient_location_can_edit() {
		return recipient_location_can_edit;
	}
	public void setRecipient_location_can_edit(List<String> recipient_location_can_edit) {
		this.recipient_location_can_edit = recipient_location_can_edit;
	}
	
}
