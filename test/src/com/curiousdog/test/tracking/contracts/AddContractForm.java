package com.curiousdog.test.tracking.contracts;

import java.text.SimpleDateFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.curiousdog.test.common.WebForm;
import com.curiousdog.test.utils.TestUtils;

public class AddContractForm extends WebForm
{
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	public AddContractForm(WebDriver drv)
	{
		super(drv) ;
	}
	
	public void fillOut(Contract contract)
	{
		this.setCheckbox("licensed_locations_all", contract.isLicensed_locations_all()) ;
		
		if ( !contract.isLicensed_locations_all() )
		{
			List<String> locations = contract.getLicensed_locations() ;
			for ( int i = 0 ; i < locations.size() ; i++ )
			{
				String locationId = "licensed_locations_" + contract.getLicensed_locations().get(i) ;
				WebElement el = driver.findElement(By.id(locationId)) ;
				if ( el != null && el.isEnabled() ) el.click();
			}
		}
		this.setCheckbox("contracts_location_can_view", contract.isContracts_location_can_view()) ;
		this.setCheckbox("contracts_location_can_edit", contract.isContracts_location_can_edit()) ;
		this.setTextbox("contracts_contracted_party_name", contract.getContracts_contracted_party_name()) ;
		this.setTextbox("contracts_contact_name", contract.getContracts_contact_name()) ;
		this.setTextbox("contracts_contact_phone", contract.getContracts_contact_phone()) ;
		this.setTextbox("contracts_contact_email", contract.getContracts_contact_email()) ;
		this.setTextbox("contracts_description",  contract.getContracts_description()) ;
		this.setTextbox("contracts_additional_details", contract.getContracts_additional_details()) ;
		this.setDropdown("contracts_status", contract.getContracts_status()) ;

		//this.selectCalendarDate("contracts_effective_date", contract.getContracts_effective_date());
		this.setTextbox("contracts_effective_date", sdf.format(contract.getContracts_effective_date()));
		//this.selectCalendarDate("contracts_expiration_date", contract.getContracts_expiration_date());
		this.setTextbox("contracts_expiration_date", sdf.format(contract.getContracts_expiration_date()));
		
		for ( int file_index = 0; file_index < contract.getContracts_file().size() ; file_index++ )
		{
			driver.findElement(By.linkText("Add a file")).click();
			String filePikerName = "contracts_file[" + file_index + "]" ;
			String filePath = contract.getContracts_file().get(file_index) ;
			this.setFileChooserByName(filePikerName, filePath);
		}

		this.setDropdown("contracts_expiration_notify_lead_days", contract.getContracts_expiration_notify_lead_days()) ;
		this.setCheckbox("contracts_expired_notification", contract.isContracts_expired_notification()) ;
		if ( contract.getContracts_additional_recipients_email() != null && contract.getContracts_additional_recipients_email().length() > 0 )
		{
			this.setTextbox("contracts_additional_recipients_email[]", contract.getContracts_additional_recipients_email()) ;
		}

	}
	
	public void navigateToMe()
	{
		TestUtils.chooseMenuItem(driver, "Tracking", "Contracts");
		//Go to add new vendor form
		driver.findElement(By.linkText("Add Contract")).click();
	}
	public void submitForm()
	{
		this.driver.findElement(By.id("save")).click() ;
	}
	
	public void doCancel()
	{
		this.driver.findElement(By.linkText("Return to Contracts List")).click() ;
	}
	
}
