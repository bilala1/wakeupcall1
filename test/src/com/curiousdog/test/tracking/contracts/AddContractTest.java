package com.curiousdog.test.tracking.contracts;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.tracking.contracts.AddContractForm;
import com.curiousdog.test.tracking.contracts.Contract;
import com.curiousdog.test.utils.TestUtils;

public class AddContractTest 
{
	static AddContractForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;
	
	@BeforeClass
	public static void startTest()
	{
		//driver = new FirefoxDriver() ;
		//driver = TestUtils.openFirefox() ;
		driver = TestUtils.openChrome() ;
				
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		form = new AddContractForm(driver) ;
		form.login(user, password);
	}

	@Test
	public void EmptyFormSubmitTest()
	{
		form.navigateToMe();
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;
		
		form.assertTextExists("Please enter the name of the contracted party", "Error message for missing name of the contracted party not displayed") ;
		form.assertTextExists("Please select the status of the contract", "Error message for missing status of the contract not displayed") ;
		form.assertTextExists("Please enter the effective date of the contract", "Error message for missing effective date not displayed") ;
	}
	
	@Test
	public void SuccessfulFormSubmitTest()
	{
		Contract contract = new Contract(bundle, "MYWUC.CONTRACTS.ADD.1") ;

		form.navigateToMe();
		form.fillOut(contract);
		form.submitForm();
		TestUtils.Pause(TestUtils.PAUSE_SHORT) ;

		form.assertNavigate("/members/contracts/index.php?notice=Contract+added");
		form.assertTextExists("Contract added", "Message Contract added is not displayed");
	}
	
	@AfterClass
	public static void LogoutAndClose()
	{
		driver.findElement(By.linkText("Logout")).click() ;
		driver.close();
	}

}
