package com.curiousdog.test.tracking.contracts;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.curiousdog.test.utils.TestUtils;

import java.text.SimpleDateFormat;

public class Contract 
{
	private boolean licensed_locations_all ;
	private List<String> licensed_locations ;
	private boolean contracts_location_can_view ;
	private boolean contracts_location_can_edit ;
	private String contracts_contracted_party_name ; 
	private String contracts_contact_name ;
	private String contracts_contact_phone ;
	private String contracts_contact_email ;
	private String contracts_description ;
	private String contracts_additional_details ;
	private String contracts_status ;
	private Date contracts_effective_date ;
	private Date contracts_expiration_date ;
	private List<String> contracts_file ;
	private String contracts_expiration_notify_lead_days ;
	private boolean contracts_expired_notification ;
	private String contracts_additional_recipients_email ;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy") ;
	
	public Contract() 
	{
		super() ;
	}
	public Contract(ResourceBundle bundle, String key)
	{
		this() ;
		populate(bundle, key) ;
	}
	
	public void populate(ResourceBundle bundle, String key)
	{
		this.licensed_locations_all = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".licensed_locations_all")) ;
		String locations = TestUtils.getProperty(bundle, key + ".licensed_locations") ;
		this.licensed_locations = TestUtils.splitString(locations, ',') ;
		this.contracts_location_can_view = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".contracts_location_can_view")) ;
		this.contracts_location_can_edit = "Y".equalsIgnoreCase(TestUtils.getProperty(bundle, key + ".contracts_location_can_edit")) ;
		this.contracts_contracted_party_name = TestUtils.getProperty(bundle, key + ".contracts_contracted_party_name") ;
		this.contracts_contact_name = TestUtils.getProperty(bundle, key + ".contracts_contact_name") ;
		this.contracts_contact_phone = TestUtils.getProperty(bundle, key + ".contracts_contact_phone") ;
		this.contracts_contact_email = TestUtils.getProperty(bundle, key + ".contracts_contact_email") ;
		this.contracts_description = TestUtils.getProperty(bundle, key + ".contracts_description") ;
		this.contracts_additional_details = TestUtils.getProperty(bundle, key + ".contracts_additional_details") ;
		this.contracts_status = TestUtils.getProperty(bundle, key + ".contracts_status");
		String effDate = TestUtils.getProperty(bundle, key + ".contracts_effective_date") ;
		String expDate = TestUtils.getProperty(bundle, key + ".contracts_expiration_date") ;
		this.contracts_effective_date = TestUtils.makeDate(effDate, "MM/dd/yyyy") ;
		this.contracts_expiration_date= TestUtils.makeDate(expDate, "MM/dd/yyyy") ;
		String files = TestUtils.getProperty(bundle, key + ".contracts_file") ;
		this.contracts_file = TestUtils.splitString(files, ',') ;
		this.contracts_expiration_notify_lead_days = TestUtils.getProperty(bundle, key + ".contracts_expiration_notify_lead_days");
		this.contracts_expired_notification = "Y".equalsIgnoreCase( TestUtils.getProperty(bundle, key + ".contracts_expired_notification") ) ;
		this.contracts_additional_recipients_email = TestUtils.getProperty(bundle, key + ".contracts_additional_recipients_email") ;
	}

	
	public boolean isContracts_location_can_view() {
		return contracts_location_can_view;
	}
	public void setContracts_location_can_view(boolean contracts_location_can_view) {
		this.contracts_location_can_view = contracts_location_can_view;
	}
	public boolean isContracts_location_can_edit() {
		return contracts_location_can_edit;
	}
	public void setContracts_location_can_edit(boolean contracts_location_can_edit) {
		this.contracts_location_can_edit = contracts_location_can_edit;
	}
	public boolean isLicensed_locations_all() {
		return licensed_locations_all;
	}
	public void setLicensed_locations_all(boolean licensed_locations_all) {
		this.licensed_locations_all = licensed_locations_all;
	}
	public List<String> getLicensed_locations() {
		return licensed_locations;
	}
	public void setLicensed_locations(List<String> licensed_locations) {
		this.licensed_locations = licensed_locations;
	}
	public String getContracts_contracted_party_name() {
		return contracts_contracted_party_name;
	}
	public void setContracts_contracted_party_name(String contracts_contracted_party_name) {
		this.contracts_contracted_party_name = contracts_contracted_party_name;
	}
	public String getContracts_contact_name() {
		return contracts_contact_name;
	}
	public void setContracts_contact_name(String contracts_contact_name) {
		this.contracts_contact_name = contracts_contact_name;
	}
	public String getContracts_contact_phone() {
		return contracts_contact_phone;
	}
	public void setContracts_contact_phone(String contracts_contact_phone) {
		this.contracts_contact_phone = contracts_contact_phone;
	}
	public String getContracts_contact_email() {
		return contracts_contact_email;
	}
	public void setContracts_contact_email(String contracts_contact_email) {
		this.contracts_contact_email = contracts_contact_email;
	}
	public String getContracts_description() {
		return contracts_description;
	}
	public void setContracts_description(String contracts_description) {
		this.contracts_description = contracts_description;
	}
	public String getContracts_additional_details() {
		return contracts_additional_details;
	}
	public void setContracts_additional_details(String contracts_additional_details) {
		this.contracts_additional_details = contracts_additional_details;
	}
	public String getContracts_status() {
		return contracts_status;
	}
	public void setContracts_status(String contracts_status) {
		this.contracts_status = contracts_status;
	}
	public Date getContracts_effective_date() {
		return contracts_effective_date;
	}
	public void setContracts_effective_date(Date contracts_effective_date) {
		this.contracts_effective_date = contracts_effective_date;
	}
	public Date getContracts_expiration_date() {
		return contracts_expiration_date;
	}
	public void setContracts_expiration_date(Date contracts_expiration_date) {
		this.contracts_expiration_date = contracts_expiration_date;
	}
	public List<String> getContracts_file() {
		return contracts_file;
	}
	public void setContracts_file(List<String> contracts_file) {
		this.contracts_file = contracts_file;
	}
	public String getContracts_expiration_notify_lead_days() {
		return contracts_expiration_notify_lead_days;
	}
	public void setContracts_expiration_notify_lead_days(String contracts_expiration_notify_lead_days) {
		this.contracts_expiration_notify_lead_days = contracts_expiration_notify_lead_days;
	}
	public boolean isContracts_expired_notification() {
		return contracts_expired_notification;
	}
	public void setContracts_expired_notification(boolean contracts_expired_notification) {
		this.contracts_expired_notification = contracts_expired_notification;
	}
	public String getContracts_additional_recipients_email() {
		return contracts_additional_recipients_email;
	}
	public void setContracts_additional_recipients_email(String contracts_additional_recipients_email) {
		this.contracts_additional_recipients_email = contracts_additional_recipients_email;
	}
	
	
}
