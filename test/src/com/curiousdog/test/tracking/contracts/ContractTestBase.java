package com.curiousdog.test.tracking.contracts;

import com.curiousdog.test.TestBase;
import com.curiousdog.test.bean.Contract;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContractTestBase extends TestBase {

    protected void fillContractForm(Contract contract) {
        setTextbox("contracts_additional_details", contract.getAdditionalDetails());
        setTextbox("contracts_contact_email", contract.getContactEmail());
        setTextbox("contracts_contact_name", contract.getContactName());
        setTextbox("contracts_contact_phone", contract.getContactPhone());
        setTextbox("contracts_contracted_party_name", contract.getContractedPartyName());
        setTextbox("contracts_description", contract.getDescription());
        setTextbox("contracts_effective_date", formatDate(contract.getEffectiveDate()));
        setTextbox("contracts_expiration_date", formatDate(contract.getExpirationDate()));
        setDropdown("contracts_status", contract.getStatus());
        if(Contract.Status.Active == contract.getStatus()) {
            setDropdown("contracts_transition_status_to", contract.getStatusTransitionTo());
            setDropdown("contracts_expiration_notify_lead_days", contract.getNotifyLeadDays());
        }
        setCheckbox("contracts_location_can_view", contract.isLocationCanView());
        setCheckbox("contracts_location_can_edit", contract.isLocationCanEdit());
    }

    protected void submitContractForm() {
        WebDriver driver = getDriver();
        driver.findElement(By.id("save")).submit();
    }

    protected void assertContractAddedText() {
        assertTextExists("Contract added", "'Contract added' text should be on page");
    }

    protected void assertContractInList(Contract contract) {
        assertTextExists(contract.getContactName(), "New contract contact name not in contact list");
        assertTextExists(contract.getContractedPartyName(), "New contracted party not in contact list");
    }
}
