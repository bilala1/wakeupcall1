package com.curiousdog.test.tracking.contracts;

import com.curiousdog.test.bean.Contract;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

public class ContractsSingleLocationUserTest extends ContractTestBase {

    @BeforeClass
    public void setup() {
        login("sue+132@wakeupcall.net", "password");
    }

    @Test
    public void createContractTest() {
        navigateToKey("URL.CONTRACTS.CREATE");

        //Assert no location selection
        //Assert no location can view/edit controls

        Contract contract = new Contract();

        contract.setAdditionalDetails("Addl details");
        contract.setContactEmail("automatedtest@wakeupcall.com");
        contract.setContactName("One Location User Test Contact");
        contract.setContactPhone("(123) 456-7890");
        contract.setContractedPartyName("One Location User Test Party");
        contract.setDescription("One Location User Test Description");
        contract.setEffectiveDate(new Date());
        contract.setExpirationDate(new Date());
        contract.setNotifyExpiration(true);
        contract.setNotifyLeadDays(30);
        contract.setStatus(Contract.Status.Active);
        contract.setStatusTransitionTo(Contract.Status.Inactive);

        fillContractForm(contract);

        submitContractForm();

        //Verify sent back to list
        assertNavigateToKey("URL.CONTRACTS.LIST");

        //Verify new contract is there
        assertContractAddedText();
        assertContractInList(contract);
    }
}
