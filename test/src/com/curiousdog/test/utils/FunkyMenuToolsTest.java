package com.curiousdog.test.utils;

import java.util.List;
import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.curiousdog.test.files.myfiles.AddFileForm;

public class FunkyMenuToolsTest 
{
	static AddFileForm form = null ;
	static ResourceBundle bundle = ResourceBundle.getBundle("WUCTest") ;
	static WebDriver driver = null ;

	@BeforeClass
	public static void getToAList()
	{
		System.out.println("Starting add file test...");
		
		String user 	= bundle.getString("LOGIN") ;
		String password = bundle.getString("PASSWORD") ;
		
		driver = new FirefoxDriver() ;
		form = new AddFileForm(driver) ;
		form.login(user, password);
		
		driver.findElement(By.linkText("Tracking")).click();
		driver.findElement(By.linkText("Certificates")).click() ;
		
	}
	
	@Test
	public void testMenus()
	{
		//Find first Action menu for test purposes
		List<WebElement> list = driver.findElements(By.className("actionMenu")) ;
		if ( list != null && list.size() > 0 )
		{
			WebElement first = list.get(0) ;			
			//first.click();
			TestUtils.mouseOver(driver, first);
			driver.switchTo().activeElement() ;
			TestUtils.Pause(TestUtils.PAUSE_LONG) ;
			driver.findElement(By.linkText("Edit")).click();
			
			driver.switchTo().parentFrame() ;
			driver.findElement(By.id("certificates_location_can_view")).click(); 
		}
		else
		{
			System.out.println("Elements 'Actions...' are not found") ;
		}
	}
	
	@AfterClass
	public static void endTest()
	{
		TestUtils.Pause(TestUtils.PAUSE_BORING) ;
		
	}
}
