package com.curiousdog.test.utils;

import java.util.ArrayList;
import java.util.List;

import com.curiousdog.test.bean.Certificate;
import com.curiousdog.test.bean.Location;
import com.curiousdog.test.bean.MyFile;
import com.curiousdog.test.bean.User;
import com.curiousdog.test.tracking.certificates.Vendor;

public class TestCache 
{
	private static TestCache myInstance = null ;
	
	private User loggedInUser = null ;
	private List<Location> myLocations = null ;
	private List<User>     myUsers     = null ;
	private List<MyFile>   myFiles     = null ;
	private List<Vendor>   myVendors   = null ;
	private String         mySignature = null ;
	private List<Certificate> myCertificates = null ;
	private List<String> stringCache1  = null ;
	private List<String> stringCache2  = null ;
	private List<String> stringCache3  = null ;

			
	protected TestCache()
	{
		loggedInUser = new User() ;
		myLocations = new ArrayList<Location>() ;
		myUsers     = new ArrayList<User>() ;
		myFiles     = new ArrayList<MyFile>() ;
		myVendors   = new ArrayList<Vendor>() ;
		myCertificates = new ArrayList<Certificate>() ;
	}
	
	public static TestCache getInstance()
	{
		if ( myInstance == null ) 
		{
			myInstance = new TestCache() ;
		}
		return myInstance ;
	}

	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public List<Location> getMyLocations() {
		return myLocations;
	}

	public void setMyLocations(List<Location> myLocations) {
		this.myLocations = myLocations;
	}

	public int addLocation(Location location)
	{
		myLocations.add(location) ;
		return myLocations.size() ;
	}
	public int removeLocation(String name)
	{
		boolean found = false ;
		for ( int i = 0 ; i < myLocations.size() ; i++ )
		{
			Location location = myLocations.get(i) ;
			if ( name.equalsIgnoreCase(location.getName())) 
			{	
				found = true ;
				myLocations.remove(i) ;
				break ;
			}
		}
		if ( found ) return myLocations.size() ;
		else         return -1 ;
	}
	
	public List<User> getMyUsers() {
		return myUsers;
	}

	public List<Vendor> getMyVendors() {
		return myVendors ;
	}

	public List<Certificate> getMyCertificates()
	{
		return myCertificates ;
	}
	public void setMyCertificates(List<Certificate> certs)
	{
		this.myCertificates = certs ;
	}
	
	public void setMyUsers(List<User> myUsers) {
		this.myUsers = myUsers;
	}

	public List<MyFile> getMyFiles() {
		return myFiles;
	}

	public void setMyFiles(List<MyFile> myFiles) {
		this.myFiles = myFiles;
	}

	public String getMySignature() {
		return mySignature;
	}

	public void setMySignature(String mySignature) {
		this.mySignature = mySignature;
	}

	public List<String> getStringCache1() {
		return stringCache1;
	}

	public void setStringCache1(List<String> stringCache1) {
		this.stringCache1 = stringCache1;
	}

	public List<String> getStringCache2() {
		return stringCache2;
	}

	public void setStringCache2(List<String> stringCache2) {
		this.stringCache2 = stringCache2;
	}

	public List<String> getStringCache3() {
		return stringCache3;
	}

	public void setStringCache3(List<String> stringCache3) {
		this.stringCache3 = stringCache3;
	}
	
	
	
}
