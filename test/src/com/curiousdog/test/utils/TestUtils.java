package com.curiousdog.test.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TestUtils {

	static Logger log = Logger.getLogger(TestUtils.class);

	public static final int PAUSE_SHORT  =  500 ;
	public static final int PAUSE_LONG   = 1000 ;
	public static final int PAUSE_BORING = 5000 ;
	
	public TestUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static String[] getStringArray(ResourceBundle bundle, String key)
	{
		String elements =  bundle.getString(key) ;
		
		StringTokenizer stok = new StringTokenizer(elements, ",") ;
		String[] txts = new String[stok.countTokens()] ;
		int counter = 0 ;
		while (stok.hasMoreTokens())
		{
			txts[counter++] = stok.nextToken() ; 
		}
		return txts ; 
	}
	
	public static List<String> splitString(String source, char delimiter)
	{
		//int index = 0 ;
		int prev  = 0 ;
		String buff = new String("") ;
		ArrayList<String> listBuffer = new ArrayList<String>() ;
		for (int i = 0 ; i < source.length() ; i++ )
		{			
			if ( (source.charAt(i) == delimiter) )
			{
				buff = source.substring(prev, i) ;
				listBuffer.add(buff) ;
				prev = i+1 ;
				buff = new String("") ;
			}
			else
			{
				buff += source.charAt(i) ;
			}
		}
		listBuffer.add(buff) ;
		return listBuffer ;
	}
	
	public static boolean verifyTextBoxes(WebDriver drv, List textBoxes)
	{
		boolean looksGood = true ;
		WebElement elm = null ;
		for ( int i = 0 ; i < textBoxes.size() ; i++ )
		{
			String txtBoxName = (String)textBoxes.get(i) ;
			System.out.print("Checking TextBox " + txtBoxName);
			try
			{
				elm = drv.findElement(By.name( txtBoxName ) ) ;
			}
			catch( Exception ex )
			{
				System.out.println(" - NOT FOUND. " + ex.getMessage()) ;
				looksGood = false ;
			}
			if ( elm == null )
			{
				System.out.println(" - NOT FOUND. ") ;
				looksGood = false ;
			}
			if ( !elm.isDisplayed() )
			{
				System.out.println(" - NOT VISIBLE. ") ;
				looksGood = false ;
			}
		}
		return looksGood ;
	}
	public static boolean verifyTextPresent(WebDriver driver, String value)
	{
		return driver.getPageSource().contains(value); 
	}

	public static boolean findText(WebDriver drv, String text) {
	    WebElement body = drv.findElement(By.tagName("body"));
	    String bodyText = body.getText();
	    return bodyText.contains(text);
	}
	/*
	public static boolean findText(WebDriver drv, String value)
	{
		int f = drv.getPageSource().indexOf(value) ;
		System.out.println("Searching string: '" + value + "' found index = " + f) ;
		log.info("Label '" + value + "' found at index " + f) ;
		return ( f >= 0 ) ;
	}
	*/
	public static boolean Pause(int timeout)
	{
		try
		{
			Thread.sleep(timeout);
			return true ;
		}
		catch(InterruptedException ie)
		{
			System.err.println("INTERRUPTED EXCEPTION: " + ie.getMessage()) ;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return false ;
	}
	/*
	public static boolean isValid(WebElement e, String eName) {
	    try {
	        WebElementWait wait = new WebElementWait(e, 1);
	        @SuppressWarnings("unused")
	        WebElement el = wait.until(
	        		new Function<WebElement, WebElement>() 
	                {
	                    public WebElement apply(WebElement d) 
	                    {
	                        return d.findElement(By.name(eName)) ;
	                    }
	                });
	        return false;
	    } catch (TimeoutException exception) {
	        return true;
	    }
	}	
	*/
	
	public static WebElement findDynamicElement(WebDriver drv, By by, int timeOut) {
		drv.switchTo().parentFrame() ;
	    WebDriverWait wait = new WebDriverWait(drv, timeOut);
	    WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by)) ;
	    		//.visibilityOfElementLocated(by));
	    return element;
	}
	
	public static void selectItem(WebElement selector, String optionText)
	{
		Select dropdown = new Select(selector);
		dropdown.selectByVisibleText(optionText);
	}
	
	public static List getDropDownItems(WebDriver drv, String id)
	{
		List<String> result = new ArrayList<String>() ;
		WebElement element = drv.findElement(By.id(id)) ;
		Select dropdown = new Select(element) ;
		List options = dropdown.getOptions() ;
		for ( int i = 0 ; i < options.size() ; i++ )
		{
			RemoteWebElement o = (RemoteWebElement)options.get(i) ;
			//System.out.println("getText= '" + o.getText() + "' other shit: '" + o.getAttribute("text") + "' and to string='" + o.toString() + "'") ;
			result.add(o.getText());
			
		}
		return result ;
	}
	
	
	public static WebElement getFirstElement(WebDriver drv, String text)
	{
		List elms = drv.findElements(By.linkText(text) ) ;
		return (WebElement)elms.get(0) ;
	}
	public static String getDocumentId(String url)
	{
		int index = url.indexOf("=") ;
		if ( index < 0 ) return null ;
		else return url.substring(index+1) ;
	}
	
	public static boolean stupidModalWindowTest(WebDriver drv)
	{
		//Verify page's URL
		String currentUrl = drv.getCurrentUrl() ;
		System.out.println("Current URL = " + currentUrl) ;

		WebElement hotels_name_autocomplete = drv.findElement(By.name("hotels_name_autocomplete")) ;
		try
		{
			//hotels_name_autocomplete.click();                         //sendKeys("a") ;
			hotels_name_autocomplete.sendKeys("A") ;
			//String warningText3 = drv.switchTo().window(arg0)
		}
		catch(UnhandledAlertException uae)
		{
			System.out.println("INSIDE CATCH: It threw an exception... What's next?");
		}
		//Here should pop-up a dialog box with error: You must select either Individual or Corporation/Management first
		//String windowsHandle = drv.getWindowHandle() ;
		
		//System.out.println("Window's handle: " + windowsHandle) ;
		Alert alert = drv.switchTo().alert() ;
		String what = alert.getText() ;
		System.out.println( "ALERT SAYS: " + what ) ;
		if (what.equalsIgnoreCase("You must select either Individual or Corporation/Management first") )
		{
			alert.accept();
			try { Thread.sleep(1000) ; } catch(Exception ex) {} ;
			drv.switchTo().parentFrame() ;
			System.out.println( drv.getCurrentUrl() );
			
			return true ;
		}
		System.out.println("WRONG TEXT IN ALERT");
		alert.accept();
		return false ;
	}
	
	public static int findStringInList(List<String> list, String st, boolean ignoreCase)
	{
		for ( int i = 0 ; i < list.size() ; i++ )
		{
			String temp = list.get(i) ;
			if ( ignoreCase ) { if ( temp.equalsIgnoreCase(st) ) return i ; }
			else              { if ( temp.equals(st) ) return i ; }
		}
		return -1 ;
	}
	
	public static List<String> getAddedIds(List<String> oldIDs, List<String> newIDs)
	{
		List<String> result = new ArrayList<String>() ;
		String newElement = null ;
		String oldElement = null ;
		boolean found = false ;
		for ( int i = 0 ; i < newIDs.size() ; i++ )
		{
			newElement = newIDs.get(i) ;
			found = false ;
			for ( int j = 0 ; j < oldIDs.size() ; j++ )
			{
				oldElement = oldIDs.get(j) ;
				if ( oldElement.equalsIgnoreCase(newElement) )
				{
					found = true ; 
				}
			}
			if ( !found ) result.add( newElement ) ;
		}
		log.info(showStringList("Added IDs: ", result)) ;
		return result ;
	}

	public static List<String> getRemovedIds(List<String> oldIDs, List<String> newIDs)
	{
		List<String> result = new ArrayList<String>() ;
		String newElement = null ;
		boolean found = false ;
		for ( int i = 0 ; i < oldIDs.size() ; i++ )
		{
			String oldElement = oldIDs.get(i) ;
			found = false ;
			for ( int j = 0 ; j < newIDs.size() ; j++ )
			{
				newElement = newIDs.get(j) ;
				if ( newElement.equalsIgnoreCase(oldElement) )
				{
					found = true ; break ;
				}
			}
			if ( !found ) result.add( newElement ) ;
		}
		log.info(showStringList("Removed IDs: ", result)) ;
		return result ;
	}
	
	public static String showStringList(String prefix, List<String> list)
	{
		StringBuffer sb = new StringBuffer(prefix) ;
		for ( int i = 0 ; i < list.size() ; i++ ) 
		{	
			if ( i == 0 ) sb.append(list.get(i)) ; 
			else          sb.append(", " + list.get(i)) ;
		}
		return sb.toString() ;
	}
	
	//public static List<WebElement> getElementsByPartialId(WebDriver drv, String idSubString)
	//{
	//	drv.
	//}
	public static WebElement getLinkByClassAndId(WebDriver drv, String className, String url, String id)
	{
		String lookFor = url + "=" + id ;
		System.out.println("Looking for : " + lookFor);

		List<WebElement> elements = drv.findElements(By.className(className)) ;
		if ( elements.size() == 0 )
		{
			System.out.println("No elements with Class '" + className + "' found");
			return null ;
		}
		
		for ( int i = 0 ; i < elements.size() ; i++ )
		{
			WebElement element = elements.get(i) ;
			String href = element.getAttribute("href") ;
			System.out.println("Checking Link " + href);
			if ( href.indexOf(lookFor) == 0 )
			{
				System.out.println("Found Element: " + href);
				return element ;
			}
		}
		System.out.println("NOT FOUND Element: " + lookFor) ;
		return null ;
	}
	public static WebElement getLinkElementById(WebDriver drv, String linkText, String linkTag, String linkId)
	{
		return getLinkElementById(drv, linkText, linkTag, linkId, "") ;
	}
	public static WebElement getLinkElementById(WebDriver drv, String linkText, String linkTag, String linkId, String suffix)
	{
		String lookFor = linkTag + "=" + linkId + suffix;
		System.out.println("LOOK FOR LINK: " + lookFor + ", link text=" + linkText) ;
		List<WebElement> elements = drv.findElements(By.linkText(linkText)) ;
		System.out.println("Found " + elements.size() + "...") ;
		for ( int i = 0 ; i < elements.size() ; i++ )
		{
			WebElement element = elements.get(i) ;
			String href = element.getAttribute("href") ;
			if ( href.indexOf(lookFor) > 0 )
			{
				System.out.println("Found element by link") ;
				return element ;
			}
		}
		return null ;
	}

	public static List<String> getIdStrings(WebDriver drv, List<WebElement> elements, String idName)
	{
		if ( drv == null || elements == null || idName == null ) return null ;
		
		List<String> result = new ArrayList<String>() ;
		for ( int i = 0 ; i < elements.size() ; i++ )
		{
			WebElement el = elements.get(i) ;
			if ( el.isDisplayed() && el.isEnabled() )
			{
				String url = el.getAttribute("href") ;
				System.out.println("Found String: " + url);
				String idStr = getIdString(url, idName) ;
				System.out.println("Extracted id = " + idStr);
				if ( idStr != null ) result.add(idStr) ;
			}
		}
		return result ;
	}
	
	public static String getIdString(String url, String idName)
	{
		if ( url == null || url.trim().length() == 0 )       return null ;
		if ( idName == null || idName.trim().length() == 0 ) return null ;
		
		int startIndex = url.indexOf(idName+"=") ;
		if ( startIndex < 1 ) return null ;
		
		int endIndex = url.indexOf("&", startIndex)  ;
		if ( endIndex > 0 ) 
		{
			return url.substring(startIndex + idName.length() + 1, endIndex) ;
		}
		else
		{
			return url.substring(startIndex + idName.length() + 1) ;
		}
	}
	
	public static boolean isBetweenStrings(WebDriver drv, String targetString, String fromString, String toString)
	{
		int f = drv.getPageSource().indexOf(targetString) ;
		if ( f < 1 ) return false ;
		int fromS = drv.getPageSource().indexOf(fromString) ;
		if ( fromS < 1 ) return false ;
		int toS   = drv.getPageSource().indexOf(toString) ;
		if ( toS < 1 ) return false ;
		return ( f > fromS && f < toS ) ;
	}
	
	public static void waitForUrl(WebDriver drv, String partialUrl, int timeOut)
	{
		WebDriverWait wait = new WebDriverWait(drv, timeOut);
		wait.until(ExpectedConditions.urlContains(partialUrl)) ;                   
	}
	
	public static void calendarDayChoose(WebDriver drv, String className, String year, String month, String day)
	{
		List<WebElement> elementsList =  drv.findElements(By.className("cal-select"));
		//for(WebElement elm:elementsList) {
		//    int i=0;
		    WebElement elm1 = elementsList.get(0);
		    selectItem(elm1, month);
		    WebElement elm2 = elementsList.get(1);
		    selectItem(elm2, year);
		//}
		
		WebElement dateWidget = drv.findElement(By.className(className));  
        List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  

        //comparing the text of cell with today's date and clicking it.
        for (WebElement cell : columns)
        {
           if (cell.getText().equals(day))
           {
              cell.click();
              break;
           }
        }
	}
/*
	public static void calendarByIdDayChoose(WebDriver drv, String calendar, String year, String month, String day)
	{
		WebElement element =  drv.findElement(By.name(calendar));
		element.click();
		
	}
*/
	public static String getFileNameFromPath(String fullPathName, String delimiter)
	{
		if ( fullPathName == null || fullPathName.trim().length() == 0 ) return "" ;
		if ( delimiter == null || delimiter.trim().length() == 0 ) return fullPathName ;
		int ind = fullPathName.lastIndexOf(delimiter) + delimiter.length() ;
		if ( ind > 0 )
		{
			String fname = fullPathName.substring(ind) ;
			return fname ;
		}
		else
		{
			return fullPathName ;
		}
	}
	
	public static int compareStringsByCharacter(String s1, String s2)
	{
		if ( s1 == null && s2 != null ) return -1 ;
		if ( s2 == null && s1 != null ) return -2 ;
		if ( s1.length() != s2.length()) return -3 ;
		
		for ( int i = 0 ; i < s1.length() ; i++ )
		{
			char c1 = s1.charAt(i) ;
			char c2 = s2.charAt(i) ;
			if ( c1 != c2 ) return i ;
		}
		return 0 ;
	}
	
	public static boolean compareDateStrings(String s1, String s2)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("mm/DD/yyyy") ;
		Date d1 = null ;
		Date d2 = null ;
		try
		{
			d1 = sdf.parse(s1) ;
			d2 = sdf.parse(s2) ;
		}
		catch(Exception ex)
		{
			log.error("Exception converting dates. " + ex.getMessage());
			return false ;
		}
		
		return (d1.compareTo(d2) == 0 ) ;
	}
	
	public static String getProperty(ResourceBundle bundle, String key)
	{
		if ( key == null || key.trim().length() == 0 ) return "" ;
		try
		{
			return bundle.getString(key) ;
		}
		catch(Exception ex)
		{
			return "" ;
		}
	}
	
	public static Date makeDate(String dateStr, String pattern)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern) ;
		try
		{
			return sdf.parse(dateStr);
		}
		catch(Exception ex)
		{
			return null ;
		}
		
	}
	
	public static void mouseOver(WebDriver driver, WebElement element)
	{
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		driver.switchTo().activeElement() ;
	}
	
	public static void chooseMenuItem(WebDriver driver, String mainMenuItem, String chooseItem)
	{
		WebElement el1 = driver.findElement(By.linkText(mainMenuItem)) ;
		TestUtils.mouseOver(driver, el1);
		Pause(PAUSE_SHORT) ;
		WebElement el2 = driver.findElement(By.linkText(chooseItem)) ;
		TestUtils.mouseOver(driver, el2);
		el2.click();
		driver.switchTo().parentFrame() ;
	}
	
    public static WebDriver openFirefox()
    {
		//System.setProperty("webdriver.gecko.driver", "C:\\GeckoDriver\\geckodriver.exe");
    	DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    	System.setProperty("webdriver.firefox.bin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
        System.setProperty("webdriver.gecko.driver","C:\\GeckoDriver\\geckodriver.exe");
        capabilities.setCapability("marionette", true);
    	//WebDriver driver = new FirefoxDriver();
		//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		//capabilities.setCapability("marionette", true);
		//driver = new FirefoxDriver(capabilities); 
        return new FirefoxDriver(capabilities);
		//return driver ;
    }
    public static WebDriver openChrome()
    {
    	System.setProperty("webdriver.chrome.driver", "C:\\GeckoDriver\\chromedriver.exe") ;
    	WebDriver driver = new ChromeDriver();
    	return driver ;
    }
    
    public static WebDriver runMarionnete(){
        DesiredCapabilities dc = DesiredCapabilities.firefox();
        System.setProperty("webdriver.firefox.bin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
        System.setProperty("webdriver.gecko.driver","C:\\GeckoDriver\\geckodriver.exe");
        return new MarionetteDriver(dc);
    }
    
    public static void close(WebDriver driver)
    {
    	driver.close();
    }

}
